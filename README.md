# Icarus

![Icarus](icarus.jpg?raw=true=400x)

从 release-2.12 开始，Icarus 使用 [ait-shared](https://gitlab-ce.alauda.cn/frontend/ait-shared) [子模块仓库](http://confluence.alauda.cn/x/La0GB)与 Underlord 复用代码。clone 仓库之后，需初始化子模块仓库：

```shell
$ git submodule update --init --recursive
$ git config submodule.recurse true
```

Icarus 中使用的公共样式有三种来源：

## 公共样式

位于 `app/styles` 文件夹下

```scss
@import 'mixins'; // 常用的mixin
@import 'var';
```

## 全局公共样式

位于 `/static/styles` 文件夹下，包含所有 global 的样式定义

## alauda-ui

样式来自 `@alauda/ui` 组件库，**通常在业务的 scss 文件中不会直接 import 这些文件**

1. aui 中使用的色值，字体，`border`，`height`，`width` 等变量，**包含于 app/\_var.scss**

   ```scss
   @import '~@alauda/ui/theme/base-var';
   ```

2. aui 中常用的 mixin，**包含于 app/\_mixins.scss**

   ```scss
   @import '~@alauda/ui/theme/mixin';
   ```

3. aui 在业务组件中的特殊场景对应的 mixin，以及不同种类的 shadow 定义，**包含于 app/\_mixins.scss**

   ```scss
   @import '~@alauda/ui/theme/pattern';
   ```
