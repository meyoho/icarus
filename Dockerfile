FROM alpine:3.11

WORKDIR /icarus

RUN mkdir -p dist/static

COPY . dist

ARG commit_id=dev
ARG app_version=dev
ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
