import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TerminalComponent } from './terminal.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: TerminalComponent,
      },
    ]),
  ],
})
export class TerminalRoutingModule {}
