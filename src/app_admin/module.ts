import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { LayoutModule } from 'app_admin/layout/layout.module';
import { AppRoutingModule } from 'app_admin/routing.module';

import { HomeComponent } from './home.component';
import { AdminLayoutComponent } from './layout/layout.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [SharedModule, LayoutModule, AppRoutingModule],
  providers: [AdminLayoutComponent],
})
export class AdminModule {}
