import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  template: 'Redirecting ...',
})
export class HomeComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.router.navigate(['load_balancer'], {
      relativeTo: this.activatedRoute,
      replaceUrl: true,
    });
  }
}
