import { InjectionToken } from '@angular/core';

import {
  GenericStatusColor,
  GenericStatusIcon,
  StorageClass,
} from 'app/typings';

import { StorageClassProvisionerType } from './storageclass/util';

export const VOLUME_TYPES = ['io1', 'gp2'];

export enum PV_RECYCLE_STRATEGIES {
  Retain = 'pv_strategy_retain',
  Delete = 'pv_strategy_delete',
}

export const PV_VOLUME_TYPES = ['hostPath', 'cephfs', 'nfs'];

export enum ACCESS_MODES {
  ReadWriteOnce = 'read_write_once',
  ReadOnlyMany = 'read_only_many',
  ReadWriteMany = 'read_write_many',
}
export enum STORAGE_TYPE {
  PV = 'pv',
  STORAGE_CLASS = 'storageclass',
}

export const IS_DEFAULT_CLASS_ANNOTATION =
  'storageclass.kubernetes.io/is-default-class';

export const RESERVED_STORAGECLASS_NAMES = [
  'alauda-system-gfs',
  'alauda-system-ebs',
];

export enum StorageProvisionerType {
  CEPH_FS = 'ceph.com/cephfs',
  NFS = 'cpaas.io/nfs-client',
  CSP = 'cpaas.io/csp',
}

export const DEFAULT_CEPH_SECRET_NAME = 'ceph-secret-admin';
export const DEFAULT_CSP_SECRET_NAME = 'csp-admin-secret';
export const DEFAULT_CEPH_SECRET_NAMESPACE = 'cpaas-system';
export const DEFAULT_CEPH_USER = 'admin';
export const DEFAULT_ROOT_PATH = '/kubernetes';
export const DEFAULT_STORAGE_CLASS_NFS_DATA: StorageClass = {
  apiVersion: 'storage.k8s.io/v1',
  kind: 'StorageClass',
  metadata: {
    name: '',
  },
  provisioner: StorageProvisionerType.NFS,
};

export const DEFAULT_STORAGE_CLASS_CEPHFS_DATA = {
  ...DEFAULT_STORAGE_CLASS_NFS_DATA,
  provisioner: StorageProvisionerType.CEPH_FS,
  parameters: {
    adminId: DEFAULT_CEPH_USER,
    adminSecretName: DEFAULT_CEPH_SECRET_NAME,
    adminSecretNamespace: DEFAULT_CEPH_SECRET_NAMESPACE,
    monitors: '',
    claimRoot: DEFAULT_ROOT_PATH,
  },
};
export const DEFAULT_STORAGE_CLASS_CSP_DATA = {
  ...DEFAULT_STORAGE_CLASS_NFS_DATA,
  provisioner: StorageProvisionerType.CSP,
  parameters: {
    adminId: DEFAULT_CEPH_USER,
    adminSecretName: DEFAULT_CSP_SECRET_NAME,
    adminSecretNamespace: DEFAULT_CEPH_SECRET_NAMESPACE,
    pool: 'cephfs',
    cluster: '',
    claimRoot: '/kubernetes/volumes',
    clientIP: '0.0.0.0/0',
  },
};

export const PROVISIONER_INIT_DATA_MAPPER: Record<
  StorageClassProvisionerType,
  unknown
> = {
  nfs: DEFAULT_STORAGE_CLASS_NFS_DATA,
  cephfs: DEFAULT_STORAGE_CLASS_CEPHFS_DATA,
  csp: DEFAULT_STORAGE_CLASS_CSP_DATA,
} as const;

export const STORAGE_TYPE_TOKEN = new InjectionToken<STORAGE_TYPE>(
  'storage_type',
);

export const STORAGE_HELM_REQUEST_LABEL = 'cpaas.io/provisioner';
export const STORAGE_CLUSTER_LABEL = 'cpaas.io/cluster';

export enum ProvisionerStatus {
  DEPLOY_SUCCEEDED = 'deploy_succeeded',
  DEPLOY_FAILED = 'deploy_failed',
  DEPLOYING = 'deploying',
  UN_DEPLOYED = 'undeployed',
}

export const ProvisionerStatusColorMapper = {
  [ProvisionerStatus.DEPLOY_SUCCEEDED]: GenericStatusColor.Succeeded,
  [ProvisionerStatus.DEPLOY_FAILED]: GenericStatusColor.Failed,
  [ProvisionerStatus.DEPLOYING]: GenericStatusColor.Pending,
  [ProvisionerStatus.UN_DEPLOYED]: GenericStatusColor.Empty,
};

export const ProvisionerStatusIconMapper = {
  [ProvisionerStatus.DEPLOY_SUCCEEDED]: GenericStatusIcon.Check,
  [ProvisionerStatus.DEPLOY_FAILED]: GenericStatusIcon.Error,
  [ProvisionerStatus.DEPLOYING]: GenericStatusIcon.Pending,
  [ProvisionerStatus.UN_DEPLOYED]: GenericStatusIcon.Invalid,
};
