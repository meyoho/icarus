import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
  catchPromise,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import {
  catchError,
  concatMapTo,
  map,
  pluck,
  shareReplay,
} from 'rxjs/operators';

import {
  Namespace,
  PersistentVolume,
  RESOURCE_TYPES,
  Secret,
  StorageClass,
} from 'app/typings';
import { STORAGE_TYPE } from 'app_admin/features/storage/storage.constant';

@Injectable()
export class StorageUtilService {
  constructor(
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  delete(
    type: STORAGE_TYPE,
    resource: PersistentVolume | StorageClass,
    cluster: string,
  ) {
    const resourceType =
      type === STORAGE_TYPE.PV
        ? RESOURCE_TYPES.PV
        : RESOURCE_TYPES.STORAGE_CLASS;
    return catchPromise(
      this.dialogService.confirm({
        title: this.translate.get(`delete_${type}_title`, {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      concatMapTo(
        this.k8sApi.deleteResource({
          type: resourceType,
          cluster,
          resource,
        }),
      ),
    );
  }

  getNameSpaces(cluster: string) {
    return this.k8sApi
      .getResourceList<Namespace>({
        type: RESOURCE_TYPES.NAMESPACE,
        cluster,
      })
      .pipe(
        pluck('items'),
        catchError(() => of<Namespace[]>([])),
        map(items => items.map(this.k8sUtil.getName)),
        shareReplay(1),
      );
  }

  getSecretFromNameSpace(cluster: string, namespace: string) {
    return this.k8sApi
      .getResourceList<Secret>({
        type: RESOURCE_TYPES.SECRET,
        cluster,
        namespace,
      })
      .pipe(
        pluck('items'),
        catchError(() => of<Secret[]>([])),
        map(items => items.map(this.k8sUtil.getName)),
        shareReplay(1),
      );
  }
}
