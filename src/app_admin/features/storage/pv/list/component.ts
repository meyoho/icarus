import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
  TranslateService,
  isAllowed,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, pluck, switchMap, takeUntil } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Environments,
  PersistentVolume,
  PvStatusColorMapper,
  PvStatusIconMapper,
  RESOURCE_TYPES,
} from 'app/typings';
import { ACTION, STATUS, getPvStatus } from 'app/utils';
import { STORAGE_TYPE } from 'app_admin/features/storage/storage.constant';
import { StorageUtilService } from 'app_admin/features/storage/util.service';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvListComponent implements OnDestroy {
  clusterName: string;

  columns = [NAME, STATUS, 'referenced_pvc', 'size', 'created_time', ACTION];
  getPvStatus = getPvStatus;
  PvStatusColorMapper = PvStatusColorMapper;
  PvStatusIconMapper = PvStatusIconMapper;

  private readonly onDestroy$ = new Subject<void>();

  cluster$ = this.layoutComponent
    .getCluster$()
    .pipe(takeUntil(this.onDestroy$));

  params$ = combineLatest([
    this.activatedRoute.queryParamMap,
    this.cluster$,
  ]).pipe(
    map(([params, cluster]) => ({
      cluster: cluster.metadata.name,
      keyword: params.get('keyword'),
    })),
  );

  search$ = this.params$.pipe(pluck('keyword'));

  permissions$ = this.cluster$.pipe(
    switchMap(cluster => {
      this.clusterName = cluster.metadata.name;
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.PV,
        cluster: cluster.metadata.name,
        action: COMMON_WRITABLE_ACTIONS,
      });
    }),
    isAllowed(),
  );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchPVs.bind(this),
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.PV,
            cluster,
          }),
        ),
      ),
  });

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly storageUtil: StorageUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {}

  fetchPVs({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<PersistentVolume>({
      type: RESOURCE_TYPES.PV,
      cluster,
      queryParams,
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  deletePv(pv: PersistentVolume) {
    this.storageUtil
      .delete(STORAGE_TYPE.PV, pv, this.clusterName)
      .subscribe(() => {
        this.message.success(this.translate.get('pv_delete_success'));
      });
  }

  create() {
    this.router.navigate([
      'admin/pv/create',
      {
        cluster: this.clusterName,
      },
    ]);
  }

  update(name: string) {
    this.router.navigate([
      'admin/pv/update',
      name,
      { cluster: this.clusterName },
    ]);
  }

  searchByName(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }
}
