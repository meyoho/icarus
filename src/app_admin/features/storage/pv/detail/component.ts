import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, shareReplay, switchMap, takeUntil } from 'rxjs/operators';

import {
  PersistentVolume,
  PvStatusColorMapper,
  PvStatusIconMapper,
  RESOURCE_TYPES,
} from 'app/typings';
import { getPvStatus } from 'app/utils';
import { StorageUtilService } from 'app_admin/features/storage/util.service';

import {
  ACCESS_MODES,
  PV_RECYCLE_STRATEGIES,
  STORAGE_TYPE,
} from '../../storage.constant';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../../../../app_user/features/app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvDetailComponent implements OnDestroy {
  clusterName: string;

  getPvStatus = getPvStatus;
  PvStatusColorMapper = PvStatusColorMapper;
  PvStatusIconMapper = PvStatusIconMapper;
  accessModes = ACCESS_MODES;
  recycleModesMap = PV_RECYCLE_STRATEGIES;

  params$ = this.activatedRoute.params;

  destroy$ = new Subject<void>();

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.PV,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
        cluster: params.cluster,
        name: params.name,
      }),
    ),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  dataManager = new AsyncDataLoader<{
    resource: PersistentVolume;
    permissions: { update: boolean; delete: boolean };
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly storageUtil: StorageUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  private dataFetcher({ cluster, name }: Params) {
    return combineLatest([
      this.k8sApi.watchResource<PersistentVolume>({
        type: RESOURCE_TYPES.PV,
        cluster,
        name,
      }),
      this.permissions$,
    ]).pipe(
      map(([resource, permissions]) => {
        this.clusterName = cluster;
        return {
          resource,
          permissions,
        };
      }),
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  jumpToListPage = () => {
    this.router.navigateByUrl('/admin/pv');
  };

  update(name: string) {
    this.router.navigate([
      `/admin/pv/update/${name}`,
      { cluster: this.clusterName },
    ]);
  }

  delete(pv: PersistentVolume) {
    this.storageUtil
      .delete(STORAGE_TYPE.PV, pv, this.clusterName)
      .subscribe(() => {
        this.message.success(this.translate.get('pv_delete_success'));
        this.jumpToListPage();
      });
  }
}
