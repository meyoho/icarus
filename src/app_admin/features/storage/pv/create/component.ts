import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PersistentVolume } from 'app/typings';

@Component({
  template: `
    <rc-pv-form-container
      [clusterName]="(activatedRoute.params | async)?.cluster"
      [initResource]="initResource"
    ></rc-pv-form-container>
  `,
})
export class PvCreateComponent {
  initResource: PersistentVolume = {
    apiVersion: 'v1',
    kind: 'PersistentVolume',
    metadata: {
      name: '',
      annotations: {},
      labels: {},
    },
    spec: {
      persistentVolumeReclaimPolicy: 'Delete',
      accessModes: ['ReadWriteOnce'],
      capacity: {
        storage: '',
      },
      hostPath: {
        path: '',
      },
    },
  };

  constructor(public readonly activatedRoute: ActivatedRoute) {}
}
