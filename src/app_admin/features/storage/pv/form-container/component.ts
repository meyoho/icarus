import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';
import { identity } from 'ramda';
import { finalize } from 'rxjs/operators';

import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import { PersistentVolume, RESOURCE_TYPES } from 'app/typings';

const RESOURCE_CREATE_TIP = 'pv_create_success';
const RESOURCE_UPDATE_TIP = 'pv_update_success';

@Component({
  selector: 'rc-pv-form-container',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvFormContainerComponent
  extends BaseFormContainer<PersistentVolume>
  implements OnInit {
  protected fillResource = identity;

  @Input()
  clusterName: string;

  initialized: boolean;

  constructor(
    injector: Injector,
    private readonly translateService: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  submit() {
    if (!this.checkForm()) {
      return;
    }
    this.submitting = true;
    const payload = cloneDeep(this.resource);
    if (!payload) {
      this.submitting = false;
      this.cdr.markForCheck();
      return;
    }
    this._confirm(payload);
  }

  private _confirm(payload: PersistentVolume) {
    const notifyContent = this.isUpdate
      ? RESOURCE_UPDATE_TIP
      : RESOURCE_CREATE_TIP;
    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      cluster: this.clusterName,
      type: RESOURCE_TYPES.PV,
      resource: payload,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.messageService.success(
          this.translateService.get(notifyContent, {
            name: payload.metadata.name,
          }),
        );
        this.router.navigate([
          '/admin/pv/detail',
          payload.metadata.name,
          { cluster: this.clusterName },
        ]);
      });
  }
}
