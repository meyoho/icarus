import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StorageUtilService } from 'app_admin/features/storage/util.service';

import { PvCreateComponent } from './create/component';
import { PvDetailComponent } from './detail/component';
import { PvListComponent } from './list/component';
import { PvUpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: PvListComponent,
  },
  {
    path: 'detail/:name',
    component: PvDetailComponent,
  },
  {
    path: 'create',
    component: PvCreateComponent,
  },
  {
    path: 'update/:name',
    component: PvUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [StorageUtilService],
})
export class PvRoutingModule {}
