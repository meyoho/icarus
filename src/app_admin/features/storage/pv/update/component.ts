import { K8sApiService, NAME } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, switchMap } from 'rxjs/operators';

import { PersistentVolume, RESOURCE_TYPES } from 'app/typings';

@Component({
  template: `
    <rc-pv-form-container
      *ngIf="data$ | async as data"
      [initResource]="data"
      [clusterName]="clusterName"
      [isUpdate]="true"
    ></rc-pv-form-container>
  `,
})
export class PvUpdateComponent {
  clusterName: string;

  data$ = this.activatedRoute.paramMap.pipe(
    switchMap(param => {
      this.clusterName = param.get('cluster');
      return this.k8sApi.getResource<PersistentVolume>({
        type: RESOURCE_TYPES.PV,
        cluster: this.clusterName,
        name: param.get(NAME),
      });
    }),
    catchError(err => {
      this.auiNotificationService.error(err.message);
      return this.router.navigateByUrl('/admin/pv');
    }),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly auiNotificationService: NotificationService,
    private readonly k8sApi: K8sApiService,
  ) {}
}
