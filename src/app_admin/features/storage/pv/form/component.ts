import { KubernetesResource, ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  PersistentVolume,
  PersistentVolumeSpec,
  PersistentVolumeSpecMeta,
} from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  POSITIVE_INT_PATTERN,
  yamlWriteOptions,
} from 'app/utils';
import {
  ACCESS_MODES,
  PV_RECYCLE_STRATEGIES,
  PV_VOLUME_TYPES,
} from 'app_admin/features/storage/storage.constant';

interface PersistentVolumeSpecFormModel extends PersistentVolumeSpecMeta {
  accessModes: string;
}

export interface PersistentVolumeFormModel extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeSpecFormModel;
  status?: {
    phase: string;
  };
}

@Component({
  selector: 'rc-pv-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvFormComponent
  extends BaseResourceFormGroupComponent<
    PersistentVolume,
    PersistentVolumeFormModel
  >
  implements OnInit, OnDestroy {
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  accessModes = Object.keys(ACCESS_MODES);
  recycleModes = Object.keys(PV_RECYCLE_STRATEGIES);
  accessModesMap = ACCESS_MODES;
  recycleModesMap = PV_RECYCLE_STRATEGIES;
  pvVolumeTypes = PV_VOLUME_TYPES;
  pvVolumeType: keyof PersistentVolumeSpecFormModel;
  unit = 'Gi';
  pvVolumeType$$ = new ReplaySubject<string>(1);

  codeEditorOptions = yamlWriteOptions;

  @ObservableInput(true)
  @Input('cluster')
  cluster$: Observable<string>;

  @Input()
  isUpdate: boolean;

  destroy$ = new Subject<void>();

  constructor(public injector: Injector) {
    super(injector);
    this.pvVolumeTypeChange = this.pvVolumeTypeChange.bind(this);
  }

  ngOnInit() {
    super.ngOnInit();
    this.pvVolumeType$$.pipe(takeUntil(this.destroy$)).subscribe(type => {
      this.pvVolumeTypeChange(type);
      this.pvVolumeType = type as keyof PersistentVolumeSpecFormModel;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.resourceNameReg.pattern),
      ]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      accessModes: this.fb.control([]),
      capacity: this.fb.group({
        storage: this.fb.control(''),
      }),
      persistentVolumeReclaimPolicy: this.fb.control(''),
      hostPath: this.fb.group({
        path: this.fb.control(''),
      }),
      nfs: this.fb.group({
        server: this.fb.control(''),
        path: this.fb.control(''),
      }),
      cephfs: this.fb.control(''),
    });

    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: specForm,
    });
  }

  pvVolumeTypeChange(type?: string) {
    this.pvVolumeTypes.forEach(type => {
      this.form.get(`spec.${type}`).disable({
        emitEvent: false,
      });
    });

    if (type && this.form.get(`spec.${type}`)) {
      this.form.get(`spec.${type}`).enable({
        emitEvent: false,
      });
    }
  }

  getDefaultFormModel() {
    return {
      apiVersion: 'v1',
      kind: 'PersistentVolume',
      metadata: {
        name: '',
        annotations: {},
        labels: {},
      },
      spec: {
        persistentVolumeReclaimPolicy: 'Delete',
        accessModes: 'ReadWriteOnce',
        capacity: {
          storage: '',
        },
        hostPath: {
          path: '',
        },
      },
    };
  }

  adaptResourceModel(resource: PersistentVolume): PersistentVolumeFormModel {
    if (resource?.spec) {
      const spec: PersistentVolumeSpecFormModel = {
        ...resource.spec,
        accessModes: resource.spec.accessModes[0] || 'ReadWriteOnce',
        capacity: {
          storage: resource.spec.capacity.storage,
        },
      };
      this.pvVolumeTypes.forEach(item => {
        // eslint-disable-next-line no-prototype-builtins
        if (spec.hasOwnProperty(item)) {
          this.pvVolumeType$$.next(item);
        }
      });
      if (spec.capacity.storage) {
        const res = spec.capacity.storage.match(/[A-Za-z]+|\d+(?:\.\d+|)/g);
        spec.capacity.storage = res[0] || '';
        this.unit = res[1] || 'Gi';
      }
      return { ...resource, spec };
    }
  }

  adaptFormModel(formModel: PersistentVolumeFormModel): PersistentVolume {
    const spec: PersistentVolumeSpec = {
      ...formModel.spec,
      accessModes: [formModel.spec.accessModes],
    };
    // 清除可能由于ui 导致的多余数据
    // TODO: 搞清楚为啥this.form.value 的数据和 formModel 不一致
    this.pvVolumeTypes.forEach(type => {
      delete spec[type as keyof PersistentVolumeSpec];
    });
    // 根据pvVolumeType 设置spec里的属性
    if (formModel.spec[this.pvVolumeType]) {
      Object.assign(spec, {
        [this.pvVolumeType]: formModel.spec[this.pvVolumeType],
      });
    }
    // 拼接capacity.storage
    if (formModel.spec.capacity.storage) {
      formModel.spec.capacity.storage = `${formModel.spec.capacity.storage}${this.unit}`;
    }
    return { ...formModel, spec };
  }
}
