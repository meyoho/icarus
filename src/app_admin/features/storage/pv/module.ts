import { NgModule } from '@angular/core';

import { StorageClassSharedModule } from '../shared.module';
import { STORAGE_TYPE, STORAGE_TYPE_TOKEN } from '../storage.constant';

import { PvCreateComponent } from './create/component';
import { PvDetailComponent } from './detail/component';
import { PvFormContainerComponent } from './form-container/component';
import { PvFormComponent } from './form/component';
import { PvListComponent } from './list/component';
import { PvRoutingModule } from './routing.module';
import { PvUpdateComponent } from './update/component';

@NgModule({
  imports: [StorageClassSharedModule, PvRoutingModule],
  declarations: [
    PvListComponent,
    PvDetailComponent,
    PvCreateComponent,
    PvFormComponent,
    PvFormContainerComponent,
    PvUpdateComponent,
  ],
  providers: [
    {
      provide: STORAGE_TYPE_TOKEN,
      useValue: STORAGE_TYPE.PV,
    },
  ],
})
export class PvModule {}
