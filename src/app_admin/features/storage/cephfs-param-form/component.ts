import { ObservableInput } from '@alauda/common-snippet';
import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { isEqual } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, Subject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  CephFSPersistentVolumeSource,
  StorageClassParameter,
} from 'app/typings';

import {
  DEFAULT_CEPH_SECRET_NAME,
  DEFAULT_CEPH_SECRET_NAMESPACE,
  DEFAULT_CEPH_USER,
  DEFAULT_ROOT_PATH,
  STORAGE_TYPE,
  STORAGE_TYPE_TOKEN,
  StorageProvisionerType,
} from '../storage.constant';
import { monitorPatternValidator } from '../storageclass/util';
import {
  adaptCephParamFormModel,
  adaptCephParamFromResource,
  isCephAuth,
} from '../util';
import { StorageUtilService } from '../util.service';

export interface CephParamFormModel {
  adminId: string;
  adminSecretName: string;
  adminSecretNamespace: string;
  claimRoot: string;
  monitors: string[];
}
@Component({
  selector: 'rc-cephfs-param-form',
  templateUrl: 'template.html',
})
export class CephFsParamFormComponent
  extends BaseResourceFormGroupComponent<
    StorageClassParameter | CephFSPersistentVolumeSource,
    CephParamFormModel
  >
  implements OnInit {
  @Input()
  isUpdate: boolean;

  @ObservableInput(true)
  @Input('cluster')
  cluster$: Observable<string>;

  isAuth = false;

  secretNamespaces$ = this.cluster$.pipe(
    switchMap(cluster => this.utilService.getNameSpaces(cluster)),
  );

  secrets$: Observable<string[]>;
  secretNamespaceChange$: Observable<string>;

  destroy$ = new Subject<void>();
  StorageProvisionerType = StorageProvisionerType;

  constructor(
    public injector: Injector,
    private readonly utilService: StorageUtilService,
    @Inject(STORAGE_TYPE_TOKEN) private readonly type: STORAGE_TYPE,
  ) {
    super(injector);
    this.authChangeHandler = this.authChangeHandler.bind(this);
  }

  ngOnInit() {
    super.ngOnInit();
    // 字段不支持更新，取消认证以及更新页，禁用secret命名空间相关逻辑
    this.secretNamespaceChange$ = this.form
      .get('adminSecretNamespace')
      .valueChanges.pipe(
        filter(_ => this.isAuth && !this.isUpdate),
        takeUntil(this.destroy$),
        distinctUntilChanged(isEqual),
        tap(_ => {
          // 用户主动选择 namespace，才能清空secretName
          if (!this.form.get('adminSecretNamespace').pristine) {
            this.form.get('adminSecretName').setValue('', { emitEvent: false });
          }
        }),
      );
    this.secrets$ = combineLatest([
      this.cluster$,
      this.secretNamespaceChange$.pipe(
        startWith(this.formModel.adminSecretNamespace),
        filter(i => !!i && !this.isUpdate),
      ),
    ]).pipe(
      switchMap(([cluster, namespace]) =>
        this.utilService.getSecretFromNameSpace(cluster, namespace),
      ),
    );
  }

  adaptResourceModel(
    resource: StorageClassParameter | CephFSPersistentVolumeSource,
  ) {
    if (resource) {
      const formModel = adaptCephParamFromResource(resource, this.type);
      // 初始化认证按钮状态，之后皆根据 isAuth 控制表单数据
      this.isAuth = isCephAuth(formModel);
      return formModel;
    }
  }

  adaptFormModel(
    formModel: CephParamFormModel,
  ): StorageClassParameter | CephFSPersistentVolumeSource {
    return adaptCephParamFormModel(formModel, this.type);
  }

  createForm() {
    return this.fb.group({
      adminId: this.fb.control(''),
      adminSecretName: '',
      adminSecretNamespace: '',
      monitors: [[], monitorPatternValidator],
      claimRoot: '',
    });
  }

  getDefaultFormModel(): CephParamFormModel {
    return {
      adminId: DEFAULT_CEPH_USER,
      adminSecretName: DEFAULT_CEPH_SECRET_NAME,
      adminSecretNamespace: DEFAULT_CEPH_SECRET_NAMESPACE,
      monitors: [],
      claimRoot: DEFAULT_ROOT_PATH,
    };
  }

  authChangeHandler(isAuth: boolean) {
    this.form.patchValue(
      isAuth
        ? {
            adminId: '',
            adminSecretName: '',
            adminSecretNamespace: '',
          }
        : {
            adminId: DEFAULT_CEPH_USER,
            adminSecretName: DEFAULT_CEPH_SECRET_NAME,
            adminSecretNamespace: DEFAULT_CEPH_SECRET_NAMESPACE,
          },
    );
  }
}
