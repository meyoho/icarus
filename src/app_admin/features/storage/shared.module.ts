import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { CephFsParamFormComponent } from './cephfs-param-form/component';

@NgModule({
  imports: [SharedModule, EventSharedModule],
  declarations: [CephFsParamFormComponent],
  exports: [SharedModule, EventSharedModule, CephFsParamFormComponent],
})
export class StorageClassSharedModule {}
