import { merge } from 'lodash-es';

import {
  CephFSPersistentVolumeSource,
  HelmRequest,
  RESOURCE_TYPES,
  StorageClassParameter,
  getYamlApiVersion,
} from 'app/typings';

import { CephParamFormModel } from './cephfs-param-form/component';
import { PersistentVolumeFormModel } from './pv/form/component';
import {
  DEFAULT_CEPH_SECRET_NAME,
  DEFAULT_CEPH_SECRET_NAMESPACE,
  DEFAULT_CEPH_USER,
  STORAGE_CLUSTER_LABEL,
  STORAGE_HELM_REQUEST_LABEL,
  STORAGE_TYPE,
} from './storage.constant';
// FIXME: 本版本以固定检查值作为判定是否为开启认证，只影响初始化显示，不影响功能，下版本全部要求认证，可废弃此函数
// TODO: deprecate this!
export function isCephAuth(formModel: CephParamFormModel) {
  if (!formModel) {
    return false;
  }

  const { adminId, adminSecretName, adminSecretNamespace } = formModel;
  return (
    adminId !== DEFAULT_CEPH_USER ||
    adminSecretName !== DEFAULT_CEPH_SECRET_NAME ||
    adminSecretNamespace !== DEFAULT_CEPH_SECRET_NAMESPACE
  );
}
export function adaptCephParamFromResource(
  resource: StorageClassParameter | CephFSPersistentVolumeSource,
  type: STORAGE_TYPE,
): CephParamFormModel {
  if (!resource) {
    return null;
  }
  if (type === STORAGE_TYPE.PV) {
    const {
      monitors,
      path,
      secretRef,
      user,
    } = resource as CephFSPersistentVolumeSource;
    return {
      monitors,
      adminId: user,
      adminSecretName: secretRef?.name,
      adminSecretNamespace: secretRef?.namespace,
      claimRoot: path,
    };
  }
  const {
    adminId,
    adminSecretName,
    adminSecretNamespace,
    claimRoot,
    monitors = '',
  } = resource as StorageClassParameter;
  return {
    adminId,
    adminSecretName,
    adminSecretNamespace,
    claimRoot,
    monitors: monitors.split(','),
  };
}

export function adaptCephParamFormModel(
  formModel: CephParamFormModel,
  type: STORAGE_TYPE,
): StorageClassParameter | CephFSPersistentVolumeSource {
  if (!formModel) {
    return null;
  }
  const {
    adminId,
    adminSecretName,
    adminSecretNamespace,
    claimRoot,
    monitors = [],
  } = formModel;
  if (type === STORAGE_TYPE.STORAGE_CLASS) {
    return { ...formModel, monitors: monitors.join(',') };
  }

  return {
    monitors,
    path: claimRoot,
    secretRef: {
      name: adminSecretName,
      namespace: adminSecretNamespace,
    },
    user: adminId,
  };
}

export function buildNfsHelmRequestPayload({
  cluster,
  server,
  path,
  globalNamespace,
}: {
  cluster: string;
  server: string;
  path: string;
  globalNamespace: string;
}): HelmRequest {
  return merge(genDefaultNfsHelmRequest(globalNamespace, cluster), {
    spec: {
      values: { nfs: { server, path } },
    },
  });
}
export function buildCephFsHelmRequestPayload(
  cluster: string,
  globalNamespace: string,
): HelmRequest {
  return genDefaultCephFsHelmRequest(globalNamespace, cluster);
}

export function buildCspHelmRequestPayload(
  cluster: string,
  globalNamespace: string,
): HelmRequest {
  return genDefaultCspHelmRequest(globalNamespace, cluster);
}
const CEPH_FS_PROVISIONER_NAME = 'cephfs-provisioner';
const CSP_PROVISIONER_NAME = 'csp-provisioner';

const BASE_HELM_REQUEST = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.HELM_REQUEST),
  kind: 'HelmRequest',
  spec: {
    values: {
      serviceAccount: { create: false },
    },
    valuesFrom: [
      {
        configMapKeyRef: {
          key: 'global.config',
          name: 'sentry-cm',
          optional: true,
        },
      },
    ],
  },
};

export function genDefaultCephFsHelmRequest(
  namespace: string,
  clusterName: string,
) {
  return merge({}, BASE_HELM_REQUEST, {
    metadata: {
      name: [clusterName, CEPH_FS_PROVISIONER_NAME].join('-'),
      namespace,
      labels: {
        [STORAGE_HELM_REQUEST_LABEL]: 'cephfs',
        [STORAGE_CLUSTER_LABEL]: clusterName,
      },
    },
    spec: {
      chart: `stable/${CEPH_FS_PROVISIONER_NAME}`,
      clusterName,
      releaseName: CEPH_FS_PROVISIONER_NAME,
      values: {
        fullnameOverride: CEPH_FS_PROVISIONER_NAME,
        storageClass: { create: false },
      },
    },
  });
}

export function genDefaultCspHelmRequest(
  namespace: string,
  clusterName: string,
) {
  return merge({}, BASE_HELM_REQUEST, {
    metadata: {
      name: [clusterName, CSP_PROVISIONER_NAME].join('-'),
      namespace,
      labels: {
        [STORAGE_HELM_REQUEST_LABEL]: 'csp',
        [STORAGE_CLUSTER_LABEL]: clusterName,
      },
    },
    spec: {
      chart: `stable/${CSP_PROVISIONER_NAME}`,
      clusterName,
      releaseName: CSP_PROVISIONER_NAME,
      values: {
        fullnameOverride: CSP_PROVISIONER_NAME,
        storageClass: { create: false },
      },
    },
  });
}

export function genDefaultNfsHelmRequest(
  namespace: string,
  clusterName: string,
) {
  return merge({}, BASE_HELM_REQUEST, {
    metadata: {
      name: [clusterName, 'nfs-client-provisioner'].join('-'),
      namespace,
      labels: {
        [STORAGE_HELM_REQUEST_LABEL]: 'nfs-client',
        [STORAGE_CLUSTER_LABEL]: clusterName,
      },
    },
    spec: {
      chart: 'stable/nfs-client-provisioner',
      clusterName,
      releaseName: 'nfs-client-provisioner',
      values: {
        nfs: { server: '', path: '' },
        storageClass: { create: false, provisionerName: 'cpaas.io/nfs-client' },
      },
    },
  });
}

export function genDefaultPvFormModel(): PersistentVolumeFormModel {
  return {
    apiVersion: 'v1',
    kind: 'PersistentVolume',
    metadata: {
      name: '',
      annotations: {},
      labels: {},
    },
    spec: {
      persistentVolumeReclaimPolicy: 'Delete',
      accessModes: 'ReadWriteOnce',
      capacity: {
        storage: '',
      },
      hostPath: {
        path: '',
      },
    },
  };
}
/**
 *
 * @param source 源复制对象
 * @param other 待合并对象
 * @returns 将待合并对象中 value 为真值的 key-value 合并到 source 中返回新对象
 */
export function mergeIfValueTruthy(
  source: { [key: string]: any },
  other: { [key: string]: any },
) {
  if (!other) {
    return source;
  }
  const obj = { ...source };
  Object.entries(other).forEach(([key, value]) => {
    if (value != null) {
      Object.assign(obj, { [key]: value });
    }
  });
  return obj;
}
