import { K8sApiService, NAME } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, StorageClass } from 'app/typings';

@Component({
  template: `
    <rc-sc-form-container
      *ngIf="data$ | async as data"
      [isUpdate]="true"
      [initResource]="data"
    ></rc-sc-form-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateStorageClassComponent {
  data$ = this.activatedRoute.paramMap.pipe(
    switchMap(param => {
      return this.k8sApi.getResource<StorageClass>({
        type: RESOURCE_TYPES.STORAGE_CLASS,
        cluster: param.get('cluster'),
        name: param.get(NAME),
      });
    }),
    catchError(err => {
      this.auiNotificationService.error(err.message);
      return this.router.navigateByUrl('/admin/storageclass');
    }),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly auiNotificationService: NotificationService,
    private readonly router: Router,
  ) {}
}
