import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { assocPath } from 'ramda';
import { map } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

import { PROVISIONER_INIT_DATA_MAPPER } from '../../storage.constant';
import { StorageClassProvisionerType } from '../util';

@Component({
  template: `
    <rc-sc-form-container
      *ngIf="data$ | async as data"
      [initResource]="data"
    ></rc-sc-form-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateStorageClassComponent {
  data$ = this.activatedRoute.paramMap.pipe(map(this.getInitData.bind(this)));

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {}

  getInitData(paramMap: ParamMap) {
    const defaultData =
      PROVISIONER_INIT_DATA_MAPPER[
        paramMap.get('type') as StorageClassProvisionerType
      ];
    return assocPath(
      ['metadata', 'labels', `project.${this.env.LABEL_BASE_DOMAIN}/name`],
      ASSIGN_ALL,
      defaultData,
    );
  }
}
