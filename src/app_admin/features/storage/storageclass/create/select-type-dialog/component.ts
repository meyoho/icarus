import { K8sApiService, matchLabelsToString } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {
  map,
  pluck,
  shareReplay,
  startWith,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { HelmRequest, HelmRequestPhaseEnum, RESOURCE_TYPES } from 'app/typings';
import {
  ProvisionerStatus,
  ProvisionerStatusColorMapper,
  ProvisionerStatusIconMapper,
  STORAGE_CLUSTER_LABEL,
  STORAGE_HELM_REQUEST_LABEL,
  StorageProvisionerType,
} from 'app_admin/features/storage/storage.constant';
import {
  buildCephFsHelmRequestPayload,
  buildCspHelmRequestPayload,
  buildNfsHelmRequestPayload,
} from 'app_admin/features/storage/util';

import { StorageClassProvisionerType } from '../../util';
import { NfsProvisionerInfoDialogComponent } from '../nfs-provisioner-info-dialog/component';

type SELECT_TYPE = StorageClassProvisionerType | 'yaml';
interface CreateTypeItemModel {
  type: SELECT_TYPE;
  title: string;
  provisioner?: string;
  gate?: string;
}

const CSP_FEATURE_GATE = 'storageclass-csp';

export const CREATE_TYPE_ITEMS: CreateTypeItemModel[] = [
  {
    type: 'cephfs',
    title: 'CephFS',
    provisioner: StorageProvisionerType.CEPH_FS,
  },
  {
    type: 'nfs',
    title: 'NFS',
    provisioner: StorageProvisionerType.NFS,
  },
  {
    type: 'csp',
    title: 'CSP',
    provisioner: StorageProvisionerType.CSP,
    gate: CSP_FEATURE_GATE,
  },
  {
    type: 'yaml',
    title: 'YAML',
  },
];

const HELM_REQUEST_PROVISIONER_STATUS_MAPPER: Record<
  HelmRequestPhaseEnum,
  Exclude<ProvisionerStatus, ProvisionerStatus.UN_DEPLOYED>
> = {
  [HelmRequestPhaseEnum.Failed]: ProvisionerStatus.DEPLOY_FAILED,
  [HelmRequestPhaseEnum.Pending]: ProvisionerStatus.DEPLOYING,
  [HelmRequestPhaseEnum.Synced]: ProvisionerStatus.DEPLOY_SUCCEEDED,
};

const STORAGE_HELM_REQUEST_LABEL_MAPPER = {
  cephfs: 'cephfs',
  nfs: 'nfs-client',
  csp: 'csp',
} as const;

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class StorageClassSelectTypeDialogComponent
  implements OnInit, OnDestroy {
  close = new EventEmitter<void>();

  cluster: string;
  globalNamespace: string;

  CREATE_TYPE_ITEMS = CREATE_TYPE_ITEMS.slice();
  ProvisionerStatusColorMapper = ProvisionerStatusColorMapper;
  ProvisionerStatusIconMapper = ProvisionerStatusIconMapper;

  StorageProvisionerType = StorageProvisionerType;
  ProvisionerStatus = ProvisionerStatus;

  destroy$ = new EventEmitter<void>();
  cephfsHelmRequest$$ = new Subject<HelmRequest>();
  nfsHelmRequest$$ = new Subject<HelmRequest>();
  cspHelmRequest$$ = new Subject<HelmRequest>();
  notSupport: StorageClassProvisionerType[];

  private readonly provisionerHRMapper = {
    nfs: this.nfsHelmRequest$$,
    cephfs: this.cephfsHelmRequest$$,
    csp: this.cspHelmRequest$$,
  } as const;

  constructor(
    @Inject(DIALOG_DATA)
    data: {
      cluster: string;
      globalNamespace: string;
      notSupport: StorageClassProvisionerType[];
    },
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    private readonly dialogService: DialogService,
  ) {
    this.cluster = data.cluster;
    this.globalNamespace = data.globalNamespace;
    this.notSupport = data.notSupport;
    this.switchHelmRequest = this.switchHelmRequest.bind(this);
    this.getProvisionerStatus = this.getProvisionerStatus.bind(this);
  }

  ngOnInit() {
    // 初始化 HelmRequest 信息
    CREATE_TYPE_ITEMS.forEach(item => {
      if (item.type === 'yaml') {
        return;
      }
      if (this.notSupport.includes(item.type)) {
        // 不支持的不予展示,不进行请求
        this.CREATE_TYPE_ITEMS = this.CREATE_TYPE_ITEMS.filter(
          supportItem => supportItem.type !== item.type,
        );
        this.cdr.markForCheck();
        return;
      }
      this.initProvisioner(item.type);
    });
  }

  private initProvisioner(type: StorageClassProvisionerType) {
    this.getHelmRequestStatus(type).subscribe(resource => {
      this.switchHelmRequest(type).next(resource);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private getHelmRequestStatus(
    type: StorageClassProvisionerType,
  ): Observable<HelmRequest> {
    return this.k8sApi
      .getResourceList<HelmRequest>({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: 'global',
        namespace: this.globalNamespace,
        queryParams: {
          labelSelector: matchLabelsToString({
            [STORAGE_HELM_REQUEST_LABEL]:
              STORAGE_HELM_REQUEST_LABEL_MAPPER[type],
            [STORAGE_CLUSTER_LABEL]: this.cluster,
          }),
        },
      })
      .pipe(
        pluck('items'),
        map(items => (!items.length ? null : items[0])),
        shareReplay(1),
      );
  }

  private createHelmRequest(
    resource: HelmRequest,
    type: StorageClassProvisionerType,
  ) {
    this.k8sApi
      .postGlobalResource<HelmRequest>({
        type: RESOURCE_TYPES.HELM_REQUEST,
        resource,
      })
      .pipe(
        switchMap(resource =>
          this.k8sApi.watchGlobalResource<HelmRequest>({
            type: RESOURCE_TYPES.HELM_REQUEST,
            name: resource.metadata.name,
          }),
        ),
        takeUntil(this.destroy$),
      )
      .subscribe(resource => {
        // update HelmRequest
        this.switchHelmRequest(type).next(resource);
      });
  }

  switchHelmRequest(type: StorageClassProvisionerType) {
    return this.provisionerHRMapper[type];
  }

  selectType(type: SELECT_TYPE) {
    this.close.next();
    if (type === 'yaml') {
      return this.router.navigate([
        '/admin/storageclass/create_from_yaml',
        { cluster: this.cluster },
      ]);
    }
    this.router.navigate([
      '/admin/storageclass/create',
      { cluster: this.cluster, type },
    ]);
  }

  deploy(type: StorageClassProvisionerType) {
    if (['cephfs', 'csp'].includes(type)) {
      const resource = (type === 'cephfs'
        ? buildCephFsHelmRequestPayload
        : buildCspHelmRequestPayload)(this.cluster, this.globalNamespace);
      return this.createHelmRequest(resource, type);
    }

    const dialogRef = this.dialogService.open(
      NfsProvisionerInfoDialogComponent,
      {
        data: {
          cluster: this.cluster,
          globalNamespace: this.globalNamespace,
        },
      },
    );
    dialogRef.componentInstance.close.subscribe(
      (info: { server: string; path: string }) => {
        dialogRef.close();
        if (info) {
          const resource = buildNfsHelmRequestPayload({
            cluster: this.cluster,
            globalNamespace: this.globalNamespace,
            ...info,
          });
          this.createHelmRequest(resource, type);
        }
      },
    );
  }

  getImage(type: string) {
    return `icons/storage/${type}.svg`;
  }

  getProvisionerStatus(type: StorageClassProvisionerType | 'yaml') {
    if (type === 'yaml') {
      return null;
    }
    // 根据具体的 HelmRequest 查询对应的状态
    return this.switchHelmRequest(type)
      .pipe(
        map(resource =>
          !resource?.status
            ? ProvisionerStatus.UN_DEPLOYED
            : HELM_REQUEST_PROVISIONER_STATUS_MAPPER[resource.status.phase],
        ),
      )
      .pipe(startWith(this.getHelmRequestStatus(type)));
  }
}
