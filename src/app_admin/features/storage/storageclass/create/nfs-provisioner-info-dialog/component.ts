import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NfsProvisionerInfoDialogComponent {
  @ViewChild('form', { static: false })
  form: NgForm;

  close = new EventEmitter<{ server: string; path: string }>();
  address = '';
  path = '';

  submit() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.close.next({
      server: this.address,
      path: this.path,
    });
  }
}
