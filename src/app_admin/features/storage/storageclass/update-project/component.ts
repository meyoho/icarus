import {
  K8sApiService,
  NAME,
  PROJECT,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
} from '@angular/core';
import { pluck } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, StorageClass } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassUpdateProjectComponent {
  close = new EventEmitter<StorageClass | void>();

  ALL = ASSIGN_ALL;

  allProjects$ = this.projectApi
    .getProjects()
    .pipe(pluck('items'), publishRef());

  project: string = this.k8sUtil.getLabel(
    this.data.storageClass,
    NAME,
    PROJECT,
  );

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly projectApi: ProjectService,
    @Inject(DIALOG_DATA)
    public data: {
      cluster: string;
      storageClass: StorageClass;
    },
  ) {}

  confirm() {
    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.STORAGE_CLASS,
        cluster: this.data.cluster,
        resource: this.data.storageClass,
        part: {
          metadata: {
            labels: {
              [this.k8sUtil.normalizeType(NAME, PROJECT)]: this.project,
            },
          },
        },
      })
      .subscribe(storageClass => this.close.emit(storageClass));
  }
}
