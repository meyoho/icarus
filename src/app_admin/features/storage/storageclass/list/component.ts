import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  PROJECT,
  ResourceListParams,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  isAllowed,
  noop,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  finalize,
  map,
  pluck,
  shareReplay,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { FeatureGateApiService } from 'app/services/api/feature-gate.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, StorageClass } from 'app/typings';
import { ACTION, ASSIGN_ALL, FALSE, TRUE } from 'app/utils';
import { STORAGE_TYPE } from 'app_admin/features/storage/storage.constant';
import { StorageUtilService } from 'app_admin/features/storage/util.service';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import {
  CREATE_TYPE_ITEMS,
  StorageClassSelectTypeDialogComponent,
} from '../create/select-type-dialog/component';
import { StorageClassUpdateProjectComponent } from '../update-project/component';
import {
  StorageClassProvisionerType,
  getProvisionerTypeName,
  isDefaultSc,
} from '../util';

interface StorageClassItem extends StorageClass {
  inAction: boolean;
}

// 设置默认按钮的禁用状态分类
enum SET_DEFAULT_MENU_DISABLED_STATUS {
  NONE_DISABLE = 'none',
  IN_ACTION = 'in_action',
  HAS_DEFAULT = 'has_default',
  NOT_ALL_PROJECT = 'not_all_project',
}

const SET_DEFAULT_DISABLE_TIP_MAPPER: Partial<Record<
  SET_DEFAULT_MENU_DISABLED_STATUS,
  string
>> = {
  [SET_DEFAULT_MENU_DISABLED_STATUS.HAS_DEFAULT]: 'has_default',
  [SET_DEFAULT_MENU_DISABLED_STATUS.NOT_ALL_PROJECT]: 'only_all_project',
};

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassListComponent implements OnDestroy, OnInit {
  columns = [NAME, 'storageclass_type', PROJECT, 'created_time', ACTION];
  clusterName: string;

  isDefaultSc = isDefaultSc;
  getProvisionerTypeName = getProvisionerTypeName;
  ASSIGN_ALL = ASSIGN_ALL;
  SET_DEFAULT_MENU_DISABLED_STATUS = SET_DEFAULT_MENU_DISABLED_STATUS;
  CREATE_TYPE_ITEMS = CREATE_TYPE_ITEMS;
  notSupport: StorageClassProvisionerType[] = [];

  private readonly onDestroy$ = new Subject<void>();
  cluster$ = this.layoutComponent
    .getCluster$()
    .pipe(takeUntil(this.onDestroy$));

  permissions$ = this.cluster$.pipe(
    switchMap(cluster => {
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.STORAGE_CLASS,
        cluster: cluster.metadata.name,
        action: COMMON_WRITABLE_ACTIONS,
      });
    }),
    isAllowed(),
  );

  params$ = combineLatest([
    this.activatedRoute.queryParamMap,
    this.cluster$,
  ]).pipe(
    map(([params, cluster]) => {
      this.clusterName = cluster.metadata.name;
      return {
        cluster: cluster.metadata.name,
        keyword: params.get('keyword'),
        limit: '0',
      };
    }),
  );

  search$ = this.params$.pipe(pluck('keyword'));

  supportGraphic$ = this.featureApi
    .getGlobalGate('storageclass-graphical')
    .pipe(
      map(feature => feature.spec.enabled),
      catchError(_ => of(false)),
    );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchStorageClass.bind(this),
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.STORAGE_CLASS,
            cluster,
          }),
        ),
      ),
  });

  fetchStorageClass({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<StorageClass>({
      type: RESOURCE_TYPES.STORAGE_CLASS,
      cluster,
      queryParams,
    });
  }

  hasDefaultSc$ = this.list.items$.pipe(
    map(items => !!items.find(i => this.isDefaultSc(i))),
    shareReplay(1),
  );

  constructor(
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly storageUtil: StorageUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly dialogService: DialogService,
    private readonly featureApi: FeatureGateApiService,
  ) {
    this.getToggleDefaultStatus = this.getToggleDefaultStatus.bind(this);
  }

  ngOnInit() {
    // 将创建项目的feature gate支持放在列表，以优化创建页效果
    CREATE_TYPE_ITEMS.forEach(item => {
      if (!item.gate) {
        return;
      }
      this.isGateSupport(item.gate).subscribe(enable => {
        if (!enable) {
          this.notSupport.push(item.type as StorageClassProvisionerType);
        }
      });
    });
  }

  private isGateSupport(gate: string) {
    return this.featureApi.getGlobalGate(gate).pipe(
      map(feature => feature.spec.enabled),
      catchError(_ => of(false)),
    );
  }

  toggleDefault(sc: StorageClassItem) {
    const payload = sc;
    payload.metadata.annotations = payload.metadata.annotations || {};
    payload.metadata.annotations[
      'storageclass.kubernetes.io/is-default-class'
    ] = this.isDefaultSc(sc) ? FALSE : TRUE;
    sc.inAction = true;
    this.k8sApi
      .putResource({
        cluster: this.clusterName,
        type: RESOURCE_TYPES.STORAGE_CLASS,
        resource: payload,
      })
      .pipe(
        finalize(() => {
          sc.inAction = false;
        }),
      )
      .subscribe(noop, noop);
  }

  delete(sc: StorageClassItem) {
    sc.inAction = true;
    this.storageUtil
      .delete(STORAGE_TYPE.STORAGE_CLASS, sc, this.clusterName)
      .pipe(
        finalize(() => {
          sc.inAction = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(this.translate.get('storageclass_delete_success'));
      });
  }

  createFromYaml() {
    this.router.navigate([
      '/admin/storageclass/create_from_yaml',
      { cluster: this.clusterName },
    ]);
  }

  create(supportUi: boolean) {
    if (!supportUi) {
      return this.createFromYaml();
    }
    const dialogRef = this.dialogService.open(
      StorageClassSelectTypeDialogComponent,
      {
        data: {
          cluster: this.clusterName,
          globalNamespace: this.globalNamespace,
          notSupport: this.notSupport,
        },
        size: DialogSize.Big,
      },
    );
    dialogRef.componentInstance.close.subscribe(() => {
      dialogRef.close();
    });
  }

  extractStorageClassName(storageClass: StorageClassItem) {
    return storageClass.provisioner.split('/')[1];
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  isSystemSc(item: StorageClassItem) {
    return ['alauda-system-gfs', 'alauda-system-ebs'].includes(
      item.metadata.name,
    );
  }

  searchByName(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  update(name: string) {
    this.router.navigate([
      'admin/storageclass/update',
      name,
      { cluster: this.clusterName },
    ]);
  }

  updateProject(storageClass: StorageClass) {
    const dialogRef = this.dialogService.open(
      StorageClassUpdateProjectComponent,
      {
        data: {
          storageClass,
          cluster: this.clusterName,
        },
      },
    );
    dialogRef.componentInstance.close.subscribe(() => {
      dialogRef.close();
    });
  }

  getToggleDefaultStatus(
    sc: StorageClassItem,
  ): Observable<SET_DEFAULT_MENU_DISABLED_STATUS> {
    return this.hasDefaultSc$.pipe(
      map(hasDefault => {
        if (sc.inAction) {
          return SET_DEFAULT_MENU_DISABLED_STATUS.IN_ACTION;
        }
        if (isDefaultSc(sc)) {
          return SET_DEFAULT_MENU_DISABLED_STATUS.NONE_DISABLE;
        }
        // 优先判定project
        return this.k8sUtil.getLabel(sc, NAME, PROJECT) !== ASSIGN_ALL
          ? SET_DEFAULT_MENU_DISABLED_STATUS.NOT_ALL_PROJECT
          : hasDefault
          ? SET_DEFAULT_MENU_DISABLED_STATUS.HAS_DEFAULT
          : SET_DEFAULT_MENU_DISABLED_STATUS.NONE_DISABLE;
      }),
    );
  }

  formatMenuError(status: SET_DEFAULT_MENU_DISABLED_STATUS) {
    return SET_DEFAULT_DISABLE_TIP_MAPPER[status] || '';
  }
}
