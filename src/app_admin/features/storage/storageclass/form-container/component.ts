import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';
import { identity } from 'ramda';
import { finalize } from 'rxjs/operators';

import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import { RESOURCE_TYPES } from 'app/typings';

import { RESERVED_STORAGECLASS_NAMES } from '../../storage.constant';
import { getProvisionerTypeName } from '../util';

@Component({
  selector: 'rc-sc-form-container',
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassFormContainerComponent extends BaseFormContainer
  implements OnInit {
  protected fillResource = identity;

  constructor(
    injector: Injector,
    private readonly translate: TranslateService,
    private readonly router: Router,
    public readonly activatedRoute: ActivatedRoute,
    private readonly auiNotificationService: NotificationService,
    private readonly k8sApi: K8sApiService,
    private readonly messageService: MessageService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    super(injector);
  }

  getProvisionerTypeName = getProvisionerTypeName;

  ngOnInit() {
    super.ngOnInit();
    if (this.isUpdate) {
      this.switchFormView('YAML');
    }
  }

  submit() {
    if (!this.checkForm()) {
      return;
    }
    const payload = cloneDeep(this.resource);
    // Reserved name or not
    const name = payload.metadata?.name;
    if (RESERVED_STORAGECLASS_NAMES.includes(name)) {
      this.auiNotificationService.error(
        this.translate.get('storageclass_reserved_name_hint', {
          name,
        }),
      );
      return;
    }
    const { cluster } = this.activatedRoute.snapshot.params;
    this.submitting = true;
    this.cdr.markForCheck();
    return this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      cluster,
      type: RESOURCE_TYPES.STORAGE_CLASS,
      resource: payload,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.messageService.success(
          this.translate.get(
            this.isUpdate
              ? 'storageclass_update_success'
              : 'storageclass_create_success',
          ),
        );
        this.router.navigate([
          '/admin/storageclass/detail',
          payload.metadata.name,
          { cluster },
        ]);
      });
  }
}
