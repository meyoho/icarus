import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { LocationStrategy } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import jsyaml, { safeDump, safeLoadAll } from 'js-yaml';
import { finalize } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType, StorageClass } from 'app/typings';
import { createActions, yamlWriteOptions } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateStorageClassFromYamlComponent {
  yaml: string;
  yamlDemo: string;
  codeEditorOptions = yamlWriteOptions;
  actionsConfig = createActions;
  loading = false;

  @ViewChild('form', { static: false })
  private readonly ngForm: NgForm;

  demo = `
  apiVersion: storage.k8s.io/v1
  kind: StorageClass
  metadata:
    name: fast
  provisioner: kubernetes.io/rbd
  parameters:
    monitors: 10.16.153.105:6789
    adminId: kube
    adminSecretName: ceph-secret
    adminSecretNamespace: kube-system
    pool: kube
    userId: kube
    userSecretName: ceph-secret-user
`;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly location: LocationStrategy,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.initYamlDemo();
  }

  private initYamlDemo() {
    this.yamlDemo = safeLoadAll(this.demo)
      .filter(r => !!r)
      .map(r => safeDump(r, { sortKeys: true }))
      .join('---\r\n');
  }

  private yamlToFormModel() {
    try {
      return jsyaml.safeLoad(this.yaml);
    } catch (err) {
      this.notification.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return null;
    }
  }

  useDemoTemplate() {
    this.yaml = this.yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialog.open(template, {
      size: DialogSize.Big,
    });
  }

  create() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    const payload = this.yamlToFormModel() as StorageClass;
    if (payload) {
      this.loading = true;
      const { cluster } = this.activatedRoute.snapshot.params;
      this.k8sApi
        .postResource({
          cluster,
          type: RESOURCE_TYPES.STORAGE_CLASS,
          resource: payload,
        })
        .pipe(
          finalize(() => {
            this.loading = false;
            this.cdr.markForCheck();
          }),
        )
        .subscribe(() => {
          this.router.navigate([
            '/admin/storageclass/detail',
            payload.metadata.name,
            { cluster },
          ]);
        });
    }
  }

  cancel() {
    this.location.back();
  }
}
