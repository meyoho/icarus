import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { get } from 'lodash-es';
import { Subject, combineLatest } from 'rxjs';
import {
  finalize,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { RESOURCE_TYPES, StorageClass } from 'app/typings';
import { ASSIGN_ALL, TRUE } from 'app/utils';
import {
  IS_DEFAULT_CLASS_ANNOTATION,
  STORAGE_TYPE,
  StorageProvisionerType,
} from 'app_admin/features/storage/storage.constant';
import { StorageUtilService } from 'app_admin/features/storage/util.service';

import { isCephAuth } from '../../util';
import { StorageClassUpdateProjectComponent } from '../update-project/component';
import { isDefaultSc } from '../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassDetailComponent implements OnDestroy {
  clusterName: string;
  isDefaultSc = isDefaultSc;
  isCephAuth = isCephAuth;
  inAction = false;
  provisionerType = StorageProvisionerType.NFS;
  StorageProvisionerType = StorageProvisionerType;
  ASSIGN_ALL = ASSIGN_ALL;

  params$ = this.activatedRoute.params;

  destroy$ = new Subject<void>();

  permissions$ = this.params$.pipe(
    switchMap(({ cluster, name }) =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.STORAGE_CLASS,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
        cluster,
        name,
      }),
    ),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  dataManager = new AsyncDataLoader<{
    resource: StorageClass;
    permissions: { update: boolean; delete: boolean };
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly storageUtil: StorageUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly dialogService: DialogService,
  ) {}

  private dataFetcher({ cluster, name }: Params) {
    return combineLatest([
      this.k8sApi
        .watchResource<StorageClass>({
          type: RESOURCE_TYPES.STORAGE_CLASS,
          cluster,
          name,
        })
        .pipe(
          tap(resource => {
            this.provisionerType = resource.provisioner as StorageProvisionerType;
            this.cdr.markForCheck();
          }),
        ),
      this.permissions$,
    ]).pipe(
      map(([resource, permissions]) => {
        this.clusterName = cluster;
        return {
          resource,
          permissions,
        };
      }),
    );
  }

  isDefault(data: StorageClass) {
    return get(data.metadata.annotations, IS_DEFAULT_CLASS_ANNOTATION) === TRUE;
  }

  isSystem(data: StorageClass) {
    return ['alauda-system-gfs', 'alauda-system-ebs'].includes(
      data.metadata.name,
    );
  }

  delete(resource: StorageClass) {
    this.inAction = true;
    this.storageUtil
      .delete(STORAGE_TYPE.STORAGE_CLASS, resource, this.clusterName)
      .pipe(
        finalize(() => {
          this.inAction = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(this.translate.get('storageclass_delete_success'));
        this.jumpToListPage();
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  jumpToListPage = () => {
    this.router.navigate(['/admin/storageclass']);
  };

  update(name: string) {
    this.router.navigate([
      'admin/storageclass/update',
      name,
      { cluster: this.clusterName },
    ]);
  }

  updateProject(storageClass: StorageClass) {
    const dialogRef = this.dialogService.open(
      StorageClassUpdateProjectComponent,
      {
        data: {
          storageClass,
          cluster: this.clusterName,
        },
      },
    );
    dialogRef.componentInstance.close.subscribe(() => {
      dialogRef.close();
    });
  }
}
