import { NgModule } from '@angular/core';

import { StorageClassSharedModule } from '../shared.module';
import { STORAGE_TYPE, STORAGE_TYPE_TOKEN } from '../storage.constant';

import { CreateStorageClassFromYamlComponent } from './create-from-yaml/component';
import { CreateStorageClassComponent } from './create/component';
import { NfsProvisionerInfoDialogComponent } from './create/nfs-provisioner-info-dialog/component';
import { StorageClassSelectTypeDialogComponent } from './create/select-type-dialog/component';
import { StorageClassDetailComponent } from './detail/component';
import { StorageClassFormContainerComponent } from './form-container/component';
import { StorageClassFormComponent } from './form/component';
import { CspStorageFormComponent } from './form/csp-params/component';
import { StorageClassListComponent } from './list/component';
import { StorageClassRoutingModule } from './routing.module';
import { StorageClassUpdateProjectComponent } from './update-project/component';
import { UpdateStorageClassComponent } from './update/component';

@NgModule({
  imports: [StorageClassSharedModule, StorageClassRoutingModule],
  declarations: [
    StorageClassListComponent,
    CreateStorageClassComponent,
    UpdateStorageClassComponent,
    StorageClassDetailComponent,
    StorageClassFormComponent,
    StorageClassFormContainerComponent,
    CreateStorageClassFromYamlComponent,
    StorageClassSelectTypeDialogComponent,
    NfsProvisionerInfoDialogComponent,
    CspStorageFormComponent,
    StorageClassUpdateProjectComponent,
  ],
  entryComponents: [
    StorageClassSelectTypeDialogComponent,
    NfsProvisionerInfoDialogComponent,
    StorageClassUpdateProjectComponent,
  ],
  providers: [
    {
      provide: STORAGE_TYPE_TOKEN,
      useValue: STORAGE_TYPE.STORAGE_CLASS,
    },
  ],
})
export class StorageClassModule {}
