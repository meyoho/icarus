import { StringMap } from '@alauda/common-snippet';
import { AbstractControl } from '@angular/forms';

import { StorageClass } from 'app/typings';
import { IP_ADDRESS_PORT_PATTERN, TRUE } from 'app/utils';

import { IS_DEFAULT_CLASS_ANNOTATION } from '../storage.constant';

export const STORAGE_CLASS_PROVISIONER_TYPES = [
  'cephfs',
  'nfs',
  'csp',
] as const;
export type StorageClassProvisionerType = typeof STORAGE_CLASS_PROVISIONER_TYPES[number];
export const STORAGE_CLASS_PROVISIONER_NAME: StringMap = {
  cephfs: 'CephFS',
  nfs: 'NFS',
  csp: 'CSP',
};

// 仅限制输入，不进行错误提示，故返回 null
export function monitorPatternValidator(control: AbstractControl): null {
  const value: string[] = control.value || [];
  const length = value.length;
  if (length === 0) {
    return null;
  }
  const lastInput = value[length - 1] || '';
  // must be valid input
  if (!IP_ADDRESS_PORT_PATTERN.pattern.test(lastInput)) {
    control.setValue(value.slice(0, length - 1));
  }

  return null;
}

export function isDefaultSc(data: StorageClass) {
  return data.metadata.annotations?.[IS_DEFAULT_CLASS_ANNOTATION] === TRUE;
}

export function getProvisionerTypeName(type: string) {
  return STORAGE_CLASS_PROVISIONER_NAME[type] || type;
}
