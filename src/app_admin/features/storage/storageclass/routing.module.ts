import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StorageUtilService } from 'app_admin/features/storage/util.service';

import { CreateStorageClassFromYamlComponent } from './create-from-yaml/component';
import { CreateStorageClassComponent } from './create/component';
import { StorageClassDetailComponent } from './detail/component';
import { StorageClassListComponent } from './list/component';
import { UpdateStorageClassComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: StorageClassListComponent,
  },
  {
    path: 'create',
    component: CreateStorageClassComponent,
  },
  {
    path: 'create_from_yaml',
    component: CreateStorageClassFromYamlComponent,
  },
  {
    path: 'detail/:name',
    component: StorageClassDetailComponent,
  },
  {
    path: 'update/:name',
    component: UpdateStorageClassComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [StorageUtilService],
})
export class StorageClassRoutingModule {}
