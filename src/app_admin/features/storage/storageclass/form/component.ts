import { ANNOTATIONS, METADATA } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { cloneDeep, isEqual } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { assocPath, identity } from 'ramda';
import { Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  shareReplay,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, StorageClass } from 'app/typings';
import {
  ASSIGN_ALL,
  K8S_RESOURCE_NAME_BASE,
  POSITIVE_INT_PATTERN,
} from 'app/utils';
import {
  DEFAULT_STORAGE_CLASS_CEPHFS_DATA,
  StorageProvisionerType,
} from 'app_admin/features/storage/storage.constant';

import { StorageClassParameterFormModel } from './csp-params/component';

interface StorageClassFormModel extends StorageClass {
  parameters?: StorageClassParameterFormModel;
}

const SUPPORT_STORAGE_RESOURCE_QUOTA: StorageProvisionerType[] = [
  StorageProvisionerType.CSP,
];

@Component({
  selector: 'rc-sc-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassFormComponent
  extends BaseResourceFormGroupComponent<StorageClass, StorageClassFormModel>
  implements OnInit, OnDestroy {
  @Input()
  cluster: string;

  ALL = ASSIGN_ALL;
  unit = 'Gi';

  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  SUPPORT_STORAGE_RESOURCE_QUOTA = SUPPORT_STORAGE_RESOURCE_QUOTA;
  StorageProvisionerType = StorageProvisionerType;
  PROJECT_LABEL_KEY = `project.${this.env.LABEL_BASE_DOMAIN}/name`;
  STORAGE_QUOTA_ANNOTATION_KEY = `quota.${this.env.LABEL_BASE_DOMAIN}/maxCapacity`;
  destroy$ = new Subject<void>();

  allProjects$ = this.projectApi.getProjects().pipe(
    map(list => list.items),
    shareReplay(1),
  );

  protected fillResource = identity;

  constructor(
    public injector: Injector,
    public projectApi: ProjectService,
    public k8sUtil: K8sUtilService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    // UI 创建存储类中，目前仅有 NFS 不支持配置 parameters
    this.form
      .get('provisioner')
      .valueChanges.pipe(
        takeUntil(this.destroy$),
        startWith(this.formModel.provisioner),
        distinctUntilChanged(isEqual),
      )
      .subscribe(provisioner => {
        this.form
          .get('parameters')
          [provisioner !== StorageProvisionerType.NFS ? 'enable' : 'disable']({
            emitEvent: false,
          });

        this.form
          .get(['metadata', 'annotations', this.STORAGE_QUOTA_ANNOTATION_KEY])
          [
            SUPPORT_STORAGE_RESOURCE_QUOTA.includes(provisioner)
              ? 'enable'
              : 'disable'
          ]({
            emitEvent: false,
          });
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.destroy$.next();
  }

  createForm() {
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: this.fb.group({
        name: '',
        labels: this.fb.group({ [this.PROJECT_LABEL_KEY]: '' }),
        annotations: this.fb.group({
          [this.STORAGE_QUOTA_ANNOTATION_KEY]: [
            '',
            [
              Validators.pattern(POSITIVE_INT_PATTERN.pattern),
              Validators.required,
            ],
          ],
        }),
      }),
      // parameters 结构由子 form 定义
      parameters: this.fb.control(''),
      provisioner: '',
    });
  }

  getDefaultFormModel(): StorageClassFormModel {
    return DEFAULT_STORAGE_CLASS_CEPHFS_DATA;
  }

  adaptResourceModel(resource: StorageClass) {
    return this.adaptResourceAnnotations(resource);
  }

  adaptFormModel(formModel: StorageClassFormModel) {
    return this.adaptModelAnnotations(formModel);
  }

  private adaptResourceAnnotations(resource: StorageClass) {
    let resourceModel = cloneDeep(resource);
    const storageQuota = this.k8sUtil.getAnnotation(
      resource,
      'maxCapacity',
      'quota',
    );
    if (storageQuota) {
      resourceModel = assocPath(
        [METADATA, ANNOTATIONS, this.STORAGE_QUOTA_ANNOTATION_KEY],
        storageQuota.match(/\d+/g)[0],
        resourceModel,
      );
    }
    const cspToken = this.k8sUtil.getAnnotation(resource, 'token', 'quota');
    if (cspToken) {
      try {
        const tokenObj = JSON.parse(atob(cspToken));
        resourceModel = assocPath(
          ['parameters', 'adminKeyRing'],
          tokenObj.adminKeyRing,
          resourceModel,
        );
        resourceModel = assocPath(
          ['parameters', 'adminUser'],
          tokenObj.adminUser,
          resourceModel,
        );
      } catch {}
    }
    return resourceModel;
  }

  private adaptModelAnnotations(formModel: StorageClassFormModel) {
    let model = cloneDeep(formModel);
    const storageQuota = this.k8sUtil.getAnnotation(
      formModel,
      'maxCapacity',
      'quota',
    );
    if (storageQuota) {
      model = assocPath(
        [METADATA, ANNOTATIONS, this.STORAGE_QUOTA_ANNOTATION_KEY],
        storageQuota + this.unit,
        model,
      );
    }
    if (
      formModel.provisioner === StorageProvisionerType.CSP &&
      formModel.parameters.cluster
    ) {
      // already hs csp cluster, must have adminUser, adminKeyRing
      const { adminUser, adminKeyRing } = formModel.parameters;
      model = assocPath(
        [METADATA, ANNOTATIONS, `quota.${this.env.LABEL_BASE_DOMAIN}/token`],
        btoa(
          JSON.stringify({
            adminUser,
            adminKeyRing,
            version: '1.0.0',
          }),
        ),
        model,
      );
      delete model.parameters.adminKeyRing;
      delete model.parameters.adminUser;
    }
    return model;
  }
}
