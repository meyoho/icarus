import { K8sApiService } from '@alauda/common-snippet';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, combineLatest } from 'rxjs';
import { distinctUntilChanged, filter, pluck } from 'rxjs/operators';

import { Csp, RESOURCE_TYPES, StorageClassParameter } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';
import { mergeIfValueTruthy } from 'app_admin/features/storage/util';

export interface StorageClassParameterFormModel extends StorageClassParameter {
  adminUser?: string;
  adminKeyRing?: string;
}

@Component({
  selector: 'rc-csp-param-form',
  templateUrl: './template.html',
})
export class CspStorageFormComponent
  extends BaseResourceFormGroupComponent<StorageClassParameterFormModel>
  implements OnInit {
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  @Input()
  cluster: string;

  cspClusters$: Observable<Csp[]>;
  clusterChosen: Csp;

  constructor(
    public injector: Injector,
    private readonly k8sApi: K8sApiService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      cluster: '',
      pool: '',
    });
  }

  getDefaultFormModel(): { [key: string]: any } {
    return {};
  }

  adaptFormModel(formModel: StorageClassParameter) {
    const clusterInfo = this.clusterChosen?.spec.clusterInfo;
    // 回填 cspCluster 信息
    return mergeIfValueTruthy(formModel, {
      adminUser: clusterInfo?.adminUser,
      adminKeyRing: clusterInfo?.adminKeyRing,
      controllerAddress: clusterInfo?.controllerAddress,
      monitors: clusterInfo?.monitors,
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.cspClusters$ = this.k8sApi
      .getGlobalResourceList<Csp>({
        type: RESOURCE_TYPES.CSP,
      })
      .pipe(pluck('items'));
    combineLatest([
      this.form.get('cluster').valueChanges.pipe(
        filter(i => !!i),
        distinctUntilChanged(),
      ),
      this.cspClusters$,
    ]).subscribe(([name, cspClusters]) => {
      // 选中的csp cluster，提交表单时回填信息
      this.clusterChosen = cspClusters.find(
        cluster => cluster.metadata.name === name,
      );
    });
  }
}
