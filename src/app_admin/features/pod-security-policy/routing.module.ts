import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreatePageComponent } from './create-page/component';
import { UpdatePageComponent } from './update-page/component';
import { ViewPageComponent } from './view-page/component';

const routes: Routes = [
  {
    path: '',
    component: ViewPageComponent,
    pathMatch: 'full',
  },
  {
    path: 'create/:cluster',
    component: CreatePageComponent,
  },
  {
    path: 'update/:cluster',
    component: UpdatePageComponent,
  },
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class RoutingModule {}
