import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreatePageComponent } from './create-page/component';
import { PspFormComponent } from './form/component';
import { RoutingModule } from './routing.module';
import { UpdatePageComponent } from './update-page/component';
import { ViewPageComponent } from './view-page/component';

@NgModule({
  imports: [SharedModule, RoutingModule],
  declarations: [
    ViewPageComponent,
    CreatePageComponent,
    UpdatePageComponent,
    PspFormComponent,
  ],
})
export class PodSecurityPolicyModule {}
