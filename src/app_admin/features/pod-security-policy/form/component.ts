import { TranslateService } from '@alauda/common-snippet';
import { CommonFormControl } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  QueryList,
  ViewChildren,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  NG_ASYNC_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgControl,
  ValidationErrors,
} from '@angular/forms';
import { dissoc } from 'ramda';

import { DEFAULT_PSP_NAME } from 'app/services/api/pod-security-policy.service';
import { sleep } from 'app/shared/util';
import {
  PodSecurityPolicy,
  PodSecurityPolicySpec,
  RESOURCE_DEFINITIONS,
  RunAsUser,
  RunAsUserRule,
} from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

@Component({
  selector: 'rc-psp-form',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PspFormComponent),
      multi: true,
    },
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => PspFormComponent),
      multi: true,
    },
  ],
})
export class PspFormComponent extends CommonFormControl<PodSecurityPolicySpec>
  implements AsyncValidator {
  @Input()
  readonly = false;

  @ViewChildren(NgControl)
  controls: QueryList<NgControl>;

  pathPrefixErrorMapper = {
    pattern: this.translate.get('ingress_path_pattern'),
  };

  pathPrefixPattern = /^\/.*$/;

  rowBackgroundColorFn = rowBackgroundColorFn;

  constructor(
    private readonly translate: TranslateService,
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);
  }

  writeValue(value: PodSecurityPolicySpec) {
    this.value$$.next(value);
  }

  async validate(_: AbstractControl): Promise<ValidationErrors | null> {
    await sleep();
    const errors = this.controls.filter(ctl => ctl.invalid);
    return errors.length ? { children_control_error: true } : null;
  }

  onPolicyChange(key: string, value: any) {
    this.emitValueChange({ ...this.snapshot.value, [key]: value });
  }

  onAllowPrivilegeEscalationChange(value: boolean) {
    if (value) {
      this.emitValueChange({
        ...this.snapshot.value,
        allowPrivilegeEscalation: true,
        defaultAllowPrivilegeEscalation: true,
      });
    } else {
      this.emitValueChange(
        dissoc('defaultAllowPrivilegeEscalation', {
          ...this.snapshot.value,
          allowPrivilegeEscalation: false,
        }),
      );
    }
  }

  onAllowedHostPathsChange(value: boolean) {
    if (value) {
      this.emitValueChange({
        ...this.snapshot.value,
        allowedHostPaths: [{ pathPrefix: '', readOnly: false }],
      });
    } else {
      this.emitValueChange({ ...this.snapshot.value, allowedHostPaths: [] });
    }
  }

  addAllowedHostPath() {
    this.emitValueChange({
      ...this.snapshot.value,
      allowedHostPaths: this.snapshot.value.allowedHostPaths.concat({
        pathPrefix: '',
        readOnly: false,
      }),
    });
  }

  removeAllowedHostPath(index: number) {
    this.emitValueChange({
      ...this.snapshot.value,
      allowedHostPaths: this.snapshot.value.allowedHostPaths.filter(
        (_, i) => i !== index,
      ),
    });
  }

  onAllowedHostPathChange(
    key: 'pathPrefix' | 'readOnly',
    index: number,
    value: string | boolean,
  ) {
    const model = this.snapshot.value as any;
    model.allowedHostPaths[index][key] = value;
    this.emitValueChange({
      ...model,
    });
  }

  onHostPortsChange(allAllowed: boolean) {
    if (allAllowed) {
      this.emitValueChange({
        ...this.snapshot.value,
        hostPorts: [{ min: 0, max: 65535 }],
      });
    } else {
      this.emitValueChange({
        ...this.snapshot.value,
        hostPorts: [{ min: null, max: null }],
      });
    }
  }

  addHostPort() {
    this.emitValueChange({
      ...this.snapshot.value,
      hostPorts: this.snapshot.value.hostPorts.concat({
        min: null,
        max: null,
      }),
    });
  }

  removeHostPort(index: number) {
    this.emitValueChange({
      ...this.snapshot.value,
      hostPorts: this.snapshot.value.hostPorts.filter((_, i) => i !== index),
    });
  }

  onHostPortChange(key: 'min' | 'max', index: number, value: number) {
    const model = this.snapshot.value;
    model.hostPorts[index][key] = value;
    this.emitValueChange({ ...model });
  }

  onRunAsUserChange(value: RunAsUserRule) {
    const runAsUser: RunAsUser =
      value === RunAsUserRule.MustRunAs
        ? {
            rule: value,
            ranges: [{ min: null, max: null }],
          }
        : { rule: value };
    this.emitValueChange({ ...this.snapshot.value, runAsUser });
  }

  addUserRange() {
    const model = this.snapshot.value;
    this.emitValueChange({
      ...model,
      runAsUser: {
        ...model.runAsUser,
        ranges: model.runAsUser.ranges.concat({ min: null, max: null }),
      },
    });
  }

  removeUserRange(index: number) {
    const model = this.snapshot.value;
    this.emitValueChange({
      ...model,
      runAsUser: {
        ...model.runAsUser,
        ranges: model.runAsUser.ranges.filter((_, i) => i !== index),
      },
    });
  }

  onUserRangeChange(key: 'min' | 'max', index: number, value: number) {
    const model = this.snapshot.value;
    model.runAsUser.ranges[index][key] = value;
    this.emitValueChange({ ...model });
  }

  isAllHostPortsAllowed(ranges: Array<{ min: number; max: number }> = []) {
    return (
      ranges.length === 1 && ranges[0].min === 0 && ranges[0].max === 65535
    );
  }
}

export function getBasePspResource(): PodSecurityPolicy {
  return {
    apiVersion: `${RESOURCE_DEFINITIONS.POD_SECURITY_POLICY.apiGroup}/${RESOURCE_DEFINITIONS.POD_SECURITY_POLICY.apiVersion}`,
    kind: 'PodSecurityPolicy',
    metadata: {
      name: DEFAULT_PSP_NAME,
    },
  };
}

export function getDefaultPolicy(): PodSecurityPolicySpec {
  return {
    privileged: true,
    allowPrivilegeEscalation: true,
    defaultAllowPrivilegeEscalation: true,
    hostPID: true,
    hostIPC: true,
    hostNetwork: true,
    readOnlyRootFilesystem: false,
    allowedHostPaths: [],
    hostPorts: [{ min: 0, max: 65535 }],
    runAsUser: { rule: RunAsUserRule.RunAsAny },
  };
}
