import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  Reason,
  Status,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Subject, combineLatest, of } from 'rxjs';
import { catchError, map, startWith, switchMap, tap } from 'rxjs/operators';

import {
  DEFAULT_PSP_NAME,
  PodSecurityPolicyService,
} from 'app/services/api/pod-security-policy.service';
import { RESOURCE_TYPES } from 'app/typings';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewPageComponent {
  private readonly refresh$$ = new Subject<void>();
  private readonly params$ = combineLatest([
    this.layout.getCluster$(),
    this.refresh$$.pipe(startWith(null as void)),
  ]).pipe(
    map(([cluster]) => cluster.metadata.name),
    tap(cluster => {
      this.cluster = cluster;
    }),
    publishRef(),
  );

  cluster: string;

  permissions$ = this.params$.pipe(
    switchMap(cluster =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.POD_SECURITY_POLICY,
        cluster,
        name: DEFAULT_PSP_NAME,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    startWith({}),
    publishRef(),
  );

  policy$ = this.params$.pipe(
    switchMap(cluster =>
      this.pspApi.getPolicy(cluster).pipe(
        map(data => ({ status: true, data })),
        catchError((err: Status) => {
          if (err.code === 403) {
            return of({
              status: false,
              reason: Reason.NoPermission,
            });
          }
          return of({
            status: false,
            reason: Reason.NotDeployed,
            title: this.translate.get('pod_security_policy_is_not_deployed'),
            description: this.translate.get(
              'pod_security_policy_is_not_deployed_desc',
            ),
          });
        }),
        startWith(() => ({ status: null })),
      ),
    ),
    publishRef(),
  );

  constructor(
    private readonly layout: AdminLayoutComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly pspApi: PodSecurityPolicyService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
  ) {}

  delete() {
    this.dialog
      .confirm({
        title: this.translate.get('delete_pod_security_policy_confirm', {
          cluster: this.cluster,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.pspApi.deletePolicy(this.cluster).subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.refresh$$.next();
      })
      .catch(noop);
  }
}
