import { AsyncDataLoader } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';

import { PodSecurityPolicyService } from 'app/services/api/pod-security-policy.service';
import { PodSecurityPolicy, PodSecurityPolicySpec } from 'app/typings';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdatePageComponent {
  @ViewChild('ngForm')
  ngForm: NgForm;

  private policy: PodSecurityPolicy;
  model: PodSecurityPolicySpec;
  submitting = false;

  private readonly params$ = this.activatedRoute.paramMap.pipe(
    map(params => params.get('cluster')),
  );

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: cluster =>
      this.pspApi.getPolicy(cluster).pipe(
        tap(res => {
          this.policy = res;
          this.model = res.spec;
        }),
      ),
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    private readonly router: Router,
    private readonly pspApi: PodSecurityPolicyService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    this.pspApi
      .putPolicy(this.activatedRoute.snapshot.paramMap.get('cluster'), {
        ...this.policy,
        spec: this.model,
      })
      .subscribe(
        () => {
          this.router.navigate(['/admin/pod_security_policy']);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.location.back();
  }

  jumpToView() {
    this.router.navigate(['/admin/pod_security_policy']);
  }
}
