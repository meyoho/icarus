import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PodSecurityPolicyService } from 'app/services/api/pod-security-policy.service';

import { getBasePspResource, getDefaultPolicy } from '../form/component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreatePageComponent {
  @ViewChild('ngForm')
  ngForm: NgForm;

  model = getDefaultPolicy();
  submitting = false;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    private readonly router: Router,
    private readonly pspApi: PodSecurityPolicyService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    this.submitting = true;

    this.pspApi
      .postPolicy(this.activatedRoute.snapshot.paramMap.get('cluster'), {
        ...getBasePspResource(),
        spec: this.model,
      })
      .subscribe(
        () => {
          this.router.navigate(['/admin/pod_security_policy']);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.location.back();
  }
}
