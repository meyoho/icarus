import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SubnetDetailComponent } from 'app_admin/features/network/subnet/detail/component';
import { SubnetListComponent } from 'app_admin/features/network/subnet/list/component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: SubnetListComponent,
  },
  {
    path: 'detail/:name',
    component: SubnetDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubnetRoutingModule {}
