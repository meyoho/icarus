import { cloneDeep, get, set } from 'lodash-es';

import { Subnet, SubnetSpec } from 'app/typings';
import {
  GatewayTypeEnum,
  IPIP_MODE_ENUM,
  SubnetCreatePageField,
  SubnetDetailPageModule,
  SubnetFormModel,
  SubnetSupportNetworkType,
  SubnetUpdatePageField,
  UpdatePartialType,
} from 'app_admin/features/network/subnet/subnet.type';
import { ClusterNetworkType } from 'app_admin/features/network/type';

export const KUBE_OVN_ANNOTATION_PREFIX = 'ovn.kubernetes.io';
export const CALICO_ANNOTATION_PREFIX = 'cni.projectcalico.org';

export const IPIP_MODE_LABEL_MAPPER: Record<IPIP_MODE_ENUM, string> = {
  [IPIP_MODE_ENUM.CROSS_SUBNET]: 'Cross Subnet',
  [IPIP_MODE_ENUM.NEVER]: 'Never',
  [IPIP_MODE_ENUM.ALWAYS]: 'Always',
};

export const IPIP_MODE_OPTIONS: Array<{
  label: string;
  value: IPIP_MODE_ENUM;
}> = Object.keys(IPIP_MODE_LABEL_MAPPER).map((key: IPIP_MODE_ENUM) => ({
  label: IPIP_MODE_LABEL_MAPPER[key],
  value: key,
}));

export function adaptModel2Resource(
  model: SubnetFormModel,
  networkType: ClusterNetworkType,
): Subnet {
  const resource = {
    apiVersion: 'kubeovn.io/v1',
    kind: 'Subnet',
    metadata: {
      name: model.name,
    },
    spec: {
      default: false,
      protocol: 'Ipv4',
      namespaces: model.namespaces || [],
      cidrBlock: model.segment,
      natOutgoing: model.natOutgoing,
      gateway: model.gateway,
      excludeIps: model.preserveIp || [],
      gatewayType: model.gatewayType || GatewayTypeEnum.DISTRIBUTED,
      gatewayNode: model.gatewayNode,
      private: model.private,
      allowSubnets: model.whitelist || [],
      ipipMode: model.ipipMode,
    },
  };
  // blockSize only needed in calico
  if (networkType === ClusterNetworkType.CALICO) {
    const cidrPrefix = +resource.spec.cidrBlock.split('/')[1];
    set(resource, 'spec.blockSize', blockSizeMapper(cidrPrefix));
  }
  return resource;
}

export function generateSubnetModel(resource: Subnet): SubnetFormModel {
  return {
    name: get(resource, 'metadata.name', ''),
    segment: get(resource, 'spec.cidrBlock', ''),
    gateway: get(resource, 'spec.gateway', ''),
    preserveIp: get(resource, 'spec.excludeIps', []),
    gatewayType: get(resource, 'spec.gatewayType', GatewayTypeEnum.DISTRIBUTED),
    natOutgoing: get(resource, 'spec.natOutgoing', true),
    gatewayNode: get(resource, 'spec.gatewayNode', ''),
    whitelist: get(resource, 'spec.allowSubnets', []),
    private: get(resource, 'spec.private', false),
    namespaces: get(resource, 'spec.namespaces', []),
    ipipMode: get(resource, 'spec.ipipMode', IPIP_MODE_ENUM.CROSS_SUBNET),
  };
}

export function generateDefaultSubnetModel(): SubnetFormModel {
  return {
    name: '',
    segment: '',
    gateway: '',
    preserveIp: [],
    gatewayType: GatewayTypeEnum.DISTRIBUTED,
    natOutgoing: true,
    gatewayNode: '',
    private: false,
    ipipMode: IPIP_MODE_ENUM.CROSS_SUBNET,
  };
}

export function isKubeOvn(type: ClusterNetworkType) {
  return type === ClusterNetworkType.KUBE_OVN;
}
// refers to: http://confluence.alauda.cn/pages/viewpage.action?pageId=53936889
function blockSizeMapper(cidrSize: number) {
  if (cidrSize <= 16) {
    return 26;
  }
  if (cidrSize <= 19) {
    return 27;
  }
  if (cidrSize === 20) {
    return 28;
  }
  if (cidrSize === 21) {
    return 29;
  }
  if (cidrSize <= 25) {
    return 30;
  }
  if (cidrSize <= 31) {
    return 31;
  }
  return 32;
}

export function extractUpdatePartFromModel(
  rawModel: SubnetFormModel,
  partialType: UpdatePartialType,
  isMacvlan = false,
): { spec: Partial<SubnetSpec> } {
  const model = cloneDeep(rawModel);
  switch (partialType) {
    case UpdatePartialType.GATEWAY: {
      if (!isMacvlan) {
        return {
          spec: {
            gatewayType: model.gatewayType,
            gatewayNode: model.gatewayNode,
            natOutgoing: model.natOutgoing,
          },
        };
      }
      return {
        spec: {
          gateway: model.gateway,
          excludeIps: model.preserveIp,
        },
      };
    }
    case UpdatePartialType.NAMESPACE: {
      return {
        spec: {
          namespaces: model.namespaces,
        },
      };
    }
    case UpdatePartialType.WHITELIST: {
      return {
        spec: {
          private: model.private,
          allowSubnets: model.whitelist,
        },
      };
    }
    case UpdatePartialType.NETWORK_MODE: {
      return {
        spec: {
          natOutgoing: model.natOutgoing,
          ipipMode: model.ipipMode,
        },
      };
    }
  }
}

// 详情页配置
const SUBNET_DETAIL_BASIC_MODULE = [SubnetDetailPageModule.BASIC_INFO];
const KUBE_OVN_SUPPORT_MODULE = [
  ...SUBNET_DETAIL_BASIC_MODULE,
  SubnetDetailPageModule.GATEWAY,
  SubnetDetailPageModule.PRESERVED_IP,
  SubnetDetailPageModule.WHITELIST,
  SubnetDetailPageModule.NAMESPACE,
];
const CALICO_SUPPORT_MODULE = [
  ...SUBNET_DETAIL_BASIC_MODULE,
  SubnetDetailPageModule.NAMESPACE,
];
const MACVLAN_SUPPORT_MODULE = [
  ...SUBNET_DETAIL_BASIC_MODULE,
  SubnetDetailPageModule.PRESERVED_IP,
];

const NETWORK_TYPE_SUPPORT_MODULES_MAPPER: Record<
  SubnetSupportNetworkType,
  SubnetDetailPageModule[]
> = {
  [ClusterNetworkType.KUBE_OVN]: KUBE_OVN_SUPPORT_MODULE,
  [ClusterNetworkType.CALICO]: CALICO_SUPPORT_MODULE,
  [ClusterNetworkType.MACVLAN]: MACVLAN_SUPPORT_MODULE,
};

// 创建页配置
const SUBNET_BASIC_CREATE_FIELDS = [
  SubnetCreatePageField.NAME,
  SubnetCreatePageField.SEGEMENT,
];
const KUBE_OVN_CREATE_FIELDS = [
  ...SUBNET_BASIC_CREATE_FIELDS,
  SubnetCreatePageField.PRESERVE_IP,
  SubnetCreatePageField.SEGEMENT,
  SubnetCreatePageField.NAMESPACE,
  SubnetCreatePageField.GATEWAY_TYPE,
  SubnetCreatePageField.SUBNET_ISOLATION,
  SubnetCreatePageField.NAT_OUTGOING,
];
const CALICO_CREATE_FIELDS = [
  ...SUBNET_BASIC_CREATE_FIELDS,
  SubnetCreatePageField.IPIP_MODE,
  SubnetCreatePageField.NAT_OUTGOING,
];
const MACVLAN_CREATE_FILEDS = [
  ...SUBNET_BASIC_CREATE_FIELDS,
  SubnetCreatePageField.GATEWAY,
  SubnetCreatePageField.PRESERVE_IP,
];
const NETWORK_TYPE_CREATE_FIELDS_MAPPER: Record<
  SubnetSupportNetworkType,
  SubnetCreatePageField[]
> = {
  [ClusterNetworkType.KUBE_OVN]: KUBE_OVN_CREATE_FIELDS,
  [ClusterNetworkType.CALICO]: CALICO_CREATE_FIELDS,
  [ClusterNetworkType.MACVLAN]: MACVLAN_CREATE_FILEDS,
};

// 更新页配置
const KUBE_OVN_UPDATE_FIELDS = [
  SubnetUpdatePageField.UPDATE_GATEWAY,
  SubnetUpdatePageField.UPDATE_NAMESPACE,
  SubnetUpdatePageField.UPDATE_WHITELIST,
];
const CALICO_UPDATE_FIELDS = [
  SubnetUpdatePageField.UPDATE_NAMESPACE,
  SubnetUpdatePageField.UPDATE_NETWORK_MODE,
];
const MACVLAN_UPDATE_FIELDS = [SubnetUpdatePageField.UPDATE_GATEWAY];
const NETWORK_TYPE_UPDATE_FIELDS_MAPPER: Record<
  SubnetSupportNetworkType,
  SubnetUpdatePageField[]
> = {
  [ClusterNetworkType.KUBE_OVN]: KUBE_OVN_UPDATE_FIELDS,
  [ClusterNetworkType.CALICO]: CALICO_UPDATE_FIELDS,
  [ClusterNetworkType.MACVLAN]: MACVLAN_UPDATE_FIELDS,
};

export function isNetworkSupportDetailPageModule(
  networkType: SubnetSupportNetworkType,
  moduleName: SubnetDetailPageModule,
): boolean {
  return (NETWORK_TYPE_SUPPORT_MODULES_MAPPER[networkType] || []).includes(
    moduleName,
  );
}

export function isNetworkSupportCreatePageField(
  networkType: SubnetSupportNetworkType,
  fieldName: SubnetCreatePageField,
): boolean {
  return (NETWORK_TYPE_CREATE_FIELDS_MAPPER[networkType] || []).includes(
    fieldName,
  );
}

export function isNetworkSupportUpdateField(
  networkType: SubnetSupportNetworkType,
  fieldName: SubnetUpdatePageField,
): boolean {
  return (NETWORK_TYPE_UPDATE_FIELDS_MAPPER[networkType] || []).includes(
    fieldName,
  );
}
