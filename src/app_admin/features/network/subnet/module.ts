import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { SubnetCreateComponent } from 'app_admin/features/network/subnet/create/component';
import { BoolTagComponent } from 'app_admin/features/network/subnet/detail/bool-tag/component';
import { SubnetDetailComponent } from 'app_admin/features/network/subnet/detail/component';
import { SubnetIpUsedComponent } from 'app_admin/features/network/subnet/detail/ip-used/component';
import { SubnetListComponent } from 'app_admin/features/network/subnet/list/component';
import { SubnetRoutingModule } from 'app_admin/features/network/subnet/routing.module';
import { SubnetUpdateComponent } from 'app_admin/features/network/subnet/update/component';
import { SubnetUtilService } from 'app_admin/features/network/subnet/util.service';

import { SubnetDetailInfoComponent } from './detail/info/component';

@NgModule({
  imports: [SharedModule, SubnetRoutingModule],
  declarations: [
    SubnetListComponent,
    BoolTagComponent,
    SubnetCreateComponent,
    SubnetUpdateComponent,
    SubnetDetailInfoComponent,
    SubnetIpUsedComponent,
    SubnetDetailComponent,
  ],
  providers: [SubnetUtilService],
})
export class SubnetModule {}
