import { ClusterNetworkType } from '../type';
export enum IPIP_MODE_ENUM {
  CROSS_SUBNET = 'CrossSubnet',
  NEVER = 'Never',
  ALWAYS = 'Always',
}

export interface SubnetFormModel {
  name: string;
  segment: string;
  gateway: string;
  preserveIp: string[];
  gatewayType?: GatewayTypeEnum;
  natOutgoing: boolean;
  gatewayNode: string;
  private: boolean;
  namespaces?: string[];
  whitelist?: string[];
  ipipMode?: IPIP_MODE_ENUM;
}
export enum UpdatePartialType {
  GATEWAY = 'gateway',
  NAMESPACE = 'namespace',
  WHITELIST = 'whitelist',
  NETWORK_MODE = 'network_mode',
}
export enum GatewayTypeEnum {
  DISTRIBUTED = 'distributed',
  CENTRALIZED = 'centralized',
}

export enum SubnetDetailPageModule {
  BASIC_INFO,
  PRESERVED_IP,
  GATEWAY,
  WHITELIST,
  NAMESPACE,
}

export enum SubnetCreatePageField {
  NAME,
  SEGEMENT,
  GATEWAY,
  PRESERVE_IP,
  NAMESPACE,
  GATEWAY_TYPE,
  SUBNET_ISOLATION,
  NAT_OUTGOING,
  IPIP_MODE,
}

export enum SubnetUpdatePageField {
  UPDATE_GATEWAY,
  UPDATE_NAMESPACE,
  UPDATE_WHITELIST,
  UPDATE_NETWORK_MODE,
}

export const ENABLE_SUBNET_NETWORK_TYPES = [
  ClusterNetworkType.CALICO,
  ClusterNetworkType.KUBE_OVN,
  ClusterNetworkType.MACVLAN,
] as const;

// 子网当前支持网络模式的子集
export type SubnetSupportNetworkType = typeof ENABLE_SUBNET_NETWORK_TYPES[number];
