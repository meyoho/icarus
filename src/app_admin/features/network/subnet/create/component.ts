import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Injector,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { curry } from 'ramda';
import { Observable } from 'rxjs';
import { finalize, pluck } from 'rxjs/operators';

import { Node, RESOURCE_TYPES, Subnet } from 'app/typings';
import {
  CIDR_FULL_PATTEN,
  K8S_RESOURCE_NAME_BASE,
  isCidrConflict,
} from 'app/utils';
import { CIDR_IP_PATTERN } from 'app/utils/patterns';

import {
  GatewayTypeEnum,
  IPIP_MODE_ENUM,
  SubnetCreatePageField,
  SubnetSupportNetworkType,
} from '../subnet.type';
import {
  IPIP_MODE_OPTIONS,
  adaptModel2Resource,
  isNetworkSupportCreatePageField,
} from '../util';

// 最大网段位数
const MAXIMUM_SEGMENT_DIGITS = 31;

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetCreateComponent implements OnInit {
  @ViewChild('ngForm', { static: false })
  private readonly ngForm: NgForm;

  @Output()
  confirmed: EventEmitter<void> = new EventEmitter();

  cluster: string;
  nodeList$: Observable<Node[]>;
  confirming: boolean;
  networkType: SubnetSupportNetworkType;
  form = this.createForm();

  usedCidrBlock: string[];
  isFieldSupport: (field: SubnetCreatePageField) => boolean;

  SubnetCreatePageField = SubnetCreatePageField;
  MAXIMUM_SEGMENT_DIGITS = MAXIMUM_SEGMENT_DIGITS;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  CIDR_FULL_PATTEN = CIDR_FULL_PATTEN;
  GatewayTypeEnum = GatewayTypeEnum;
  CIDR_IP_PATTERN = CIDR_IP_PATTERN;
  IPIP_MODE_OPTIONS = IPIP_MODE_OPTIONS;

  constructor(
    readonly injector: Injector,
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      cluster: string;
      networkType: SubnetSupportNetworkType;
      usedCidrBlock: string[];
    },
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly fb: FormBuilder,
  ) {
    this.cluster = this.modalData.cluster;
    this.networkType = this.modalData.networkType;
    this.usedCidrBlock = this.modalData.usedCidrBlock;
    this.submitForm = this.submitForm.bind(this);
    this.isFieldSupport = curry(isNetworkSupportCreatePageField)(
      this.networkType,
    ).bind(this);
  }

  ngOnInit() {
    this.nodeList$ = this.k8sApi
      .getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.cluster,
      })
      .pipe(pluck('items'));
  }

  createForm(): FormGroup {
    return this.fb.group({
      name: '',
      segment: this.fb.control('', {
        validators: [
          this.getSegmentBlkSizeValidateFn(),
          this.getSegmentNonConflictValidateFn(),
        ],
        updateOn: 'blur',
      }),
      gateway: '',
      preserveIp: [[]],
      gatewayType: GatewayTypeEnum.DISTRIBUTED,
      natOutgoing: true,
      gatewayNode: '',
      private: false,
      namespaces: [[]],
      whitelist: [[]],
      ipipMode: IPIP_MODE_ENUM.CROSS_SUBNET,
    });
  }

  submitForm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    this.confirming = true;
    this.cdr.markForCheck();
    this.k8sApi
      .postResource<Subnet>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        resource: adaptModel2Resource(this.form.value, this.networkType),
        cluster: this.cluster,
      })
      .pipe(
        finalize(() => {
          this.confirming = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe((resource: Subnet) => {
        this.confirmed.next();
        this.router.navigate([
          '/admin/subnet/detail',
          resource.metadata.name,
          { cluster: this.cluster },
        ]);
      });
  }

  private getSegmentBlkSizeValidateFn(): ValidatorFn {
    return control => {
      const segement = control.value;
      if (!CIDR_FULL_PATTEN.pattern.test(segement)) {
        return null;
      }
      if (+segement.split('/')[1] > MAXIMUM_SEGMENT_DIGITS) {
        return { blkTooLarge: true };
      }
      return null;
    };
  }

  private getSegmentNonConflictValidateFn(): ValidatorFn {
    return control => {
      const segement = control.value;
      if (!CIDR_FULL_PATTEN.pattern.test(segement)) {
        return null;
      }
      if (
        (this.usedCidrBlock || []).some(usedCidr =>
          isCidrConflict(segement, usedCidr),
        )
      ) {
        return { segmentConflict: true };
      }
      return null;
    };
  }
}
