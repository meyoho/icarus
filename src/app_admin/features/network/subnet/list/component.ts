import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  ResourceListParams,
  TranslateService,
  isAllowed,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  pluck,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, ResourceType, Subnet } from 'app/typings';
import { ACTION } from 'app/utils';
import { SubnetCreateComponent } from 'app_admin/features/network/subnet/create/component';
import { SubnetUtilService } from 'app_admin/features/network/subnet/util.service';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import { ClusterNetworkType } from '../../type';
import {
  ENABLE_SUBNET_NETWORK_TYPES,
  SubnetSupportNetworkType,
} from '../subnet.type';
import { isKubeOvn } from '../util';

const TABLE_COLUMNS_DEFINITION = [
  NAME,
  'network_segment',
  'created_time',
  ACTION,
];
@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetListComponent implements OnDestroy {
  columns = TABLE_COLUMNS_DEFINITION;
  cluster: string;
  initialized = false;
  networkType = ClusterNetworkType.DEFAULT;
  isKubeOvn = isKubeOvn;
  destroy$ = new Subject<void>();
  cluster$ = this.layoutComponent.getCluster$().pipe(takeUntil(this.destroy$));
  usedCidrBlock: string[] = [];

  permissions$ = this.cluster$.pipe(
    switchMap(cluster => {
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        cluster: cluster.metadata.name,
        action: [K8sResourceAction.CREATE, K8sResourceAction.DELETE],
      });
    }),
    isAllowed(),
  );

  isNetworkSupport$ = this.cluster$.pipe(
    filter(_ => !!_),
    distinctUntilChanged(),
    switchMap(cluster => {
      // subscribe by component container, so always got, if adjust structure, may get cluster in extra subscribe
      this.cluster = cluster.metadata.name;
      return this.k8sUtil.getNetworkTypeByCluster$(cluster.metadata.name).pipe(
        tap((type: SubnetSupportNetworkType) => {
          this.initialized = true;
          this.networkType = type;
        }),
      );
    }),
    map((type: SubnetSupportNetworkType) =>
      ENABLE_SUBNET_NETWORK_TYPES.includes(type),
    ),
    shareReplay(1),
  );

  params$ = combineLatest([this.route.queryParamMap, this.cluster$]).pipe(
    map(([params, cluster]) => ({
      cluster: cluster.metadata.name,
      keyword: params.get('keyword'),
      limit: '0',
    })),
  );

  search$ = this.params$.pipe(pluck('keyword'));

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchSubnets.bind(this),
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
            cluster,
          }),
        ),
      ),
  });

  constructor(
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly subnetUtil: SubnetUtilService,
    private readonly k8sUtil: K8sUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  ngOnDestroy() {
    this.destroy$.next();
    this.list.destroy();
  }

  fetchSubnets({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi
      .getResourceList<Subnet>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        cluster,
        queryParams,
      })
      .pipe(
        tap(({ items }) => {
          this.usedCidrBlock = items.map(({ spec }) => spec.cidrBlock);
        }),
      );
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  create() {
    const dialogRef = this.dialogService.open(SubnetCreateComponent, {
      data: {
        cluster: this.cluster,
        networkType: this.networkType,
        usedCidrBlock: this.usedCidrBlock,
      },
    });
    dialogRef.componentInstance.confirmed.pipe(first()).subscribe(() => {
      dialogRef.close();
    });
  }

  deleteSubnet(subnet: Subnet) {
    this.subnetUtil.delete(subnet, this.cluster).subscribe(() => {
      this.message.success(this.translate.get('subnet_delete_success'));
    });
  }
}
