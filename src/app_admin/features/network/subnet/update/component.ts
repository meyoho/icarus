import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize, pluck } from 'rxjs/operators';

import { Namespace, Node, RESOURCE_TYPES, Subnet } from 'app/typings';
import { CIDR_IP_PATTERN } from 'app/utils';
import {
  SubnetFormModel,
  SubnetSupportNetworkType,
  UpdatePartialType,
} from 'app_admin/features/network/subnet/subnet.type';

import { ClusterNetworkType } from '../../type';
import {
  IPIP_MODE_OPTIONS,
  extractUpdatePartFromModel,
  generateSubnetModel,
} from '../util';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetUpdateComponent {
  @ViewChild(NgForm, { static: true })
  private readonly form: NgForm;

  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  cluster: string;
  confirming: boolean;
  model: SubnetFormModel;
  resource: Subnet;
  partialType: UpdatePartialType;
  UpdatePartialType = UpdatePartialType;
  namespaces$: Observable<Namespace[]>;
  nodeList$: Observable<Node[]>;
  networkType: SubnetSupportNetworkType;
  CIDR_IP_PATTERN = CIDR_IP_PATTERN;
  IPIP_MODE_OPTIONS = IPIP_MODE_OPTIONS;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      type: UpdatePartialType;
      info: Subnet;
      cluster: string;
      networkType: SubnetSupportNetworkType;
    },
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.partialType = this.modalData.type;
    this.resource = this.modalData.info;
    this.cluster = this.modalData.cluster;
    this.networkType = this.modalData.networkType;
    this.model = generateSubnetModel(this.resource);
    this.namespaces$ = this.k8sApi
      .getResourceList<Namespace>({
        type: RESOURCE_TYPES.NAMESPACE,
        cluster: this.cluster,
      })
      .pipe(pluck('items'));
    this.nodeList$ = this.k8sApi
      .getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.cluster,
      })
      .pipe(pluck('items'));
  }

  // FIXME 对macvlan进行临时支持，因与其他网络模式差异较大，暂根据模式直接判断
  isMacvlan(networkType: ClusterNetworkType) {
    return networkType === ClusterNetworkType.MACVLAN;
  }

  submitForm() {
    if (this.form.invalid) {
      return;
    }
    this.confirming = true;
    this.k8sApi
      .patchResource<Subnet>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        resource: this.resource,
        part: extractUpdatePartFromModel(
          this.model,
          this.partialType,
          this.isMacvlan(this.networkType),
        ),
        cluster: this.cluster,
      })
      .pipe(
        finalize(() => {
          this.confirming = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(
        () => {
          this.confirmed.next(true);
        },
        () => {
          this.confirmed.next(false);
        },
      );
  }
}
