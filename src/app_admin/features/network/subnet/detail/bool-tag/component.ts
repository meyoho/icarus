import { TagType } from '@alauda/ui';
import { Component, Input } from '@angular/core';

@Component({
  template: `
    <aui-tag [type]="data ? TagType.Success : TagType.Info" size="mini">
      {{ data + '' | translate }}
    </aui-tag>
  `,
  selector: 'rc-bool-tag',
})
export class BoolTagComponent {
  TagType = TagType;

  @Input()
  data: boolean;
}
