import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
  isAllowed,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, shareReplay, switchMap, takeUntil } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, Subnet } from 'app/typings';
import { SubnetUtilService } from 'app_admin/features/network/subnet/util.service';

import { SubnetSupportNetworkType } from '../subnet.type';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetDetailComponent implements OnDestroy {
  params$ = this.activatedRoute.params;
  destroy$ = new Subject<void>();

  permissions$ = this.params$.pipe(
    switchMap(({ cluster, name }) =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
        cluster,
        name,
      }),
    ),
    isAllowed(),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  networkType$ = this.params$.pipe(
    takeUntil(this.destroy$),
    switchMap(
      ({ cluster }) =>
        this.k8sUtil.getNetworkTypeByCluster$(cluster) as Observable<
          SubnetSupportNetworkType
        >,
    ),
    shareReplay(1),
  );

  dataManager = new AsyncDataLoader<{
    resource: Subnet;
    permissions: { update: boolean; delete: boolean };
    networkType: SubnetSupportNetworkType;
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly subnetUtilService: SubnetUtilService,
    private readonly k8sUtil: K8sUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  dataFetcher({ cluster, name }: Params) {
    return combineLatest([
      this.k8sApi.watchResource<Subnet>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        cluster,
        name,
      }),
      this.permissions$,
      this.networkType$,
    ]).pipe(
      map(([resource, permissions, networkType]) => ({
        resource,
        permissions,
        networkType,
      })),
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  delete(resource: Subnet) {
    const { cluster } = this.activatedRoute.snapshot.params;
    this.subnetUtilService.delete(resource, cluster).subscribe(() => {
      this.message.success(this.translate.get('subnet_delete_success'));
      this.jumpToListPage();
    });
  }

  jumpToListPage = () => {
    this.router.navigate(['/admin/subnet']);
  };
}
