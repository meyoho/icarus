import { ObservableInput } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { Subnet } from 'app/typings';
import {
  GatewayTypeEnum,
  IPIP_MODE_ENUM,
  SubnetDetailPageModule,
  SubnetSupportNetworkType,
  SubnetUpdatePageField,
  UpdatePartialType,
} from 'app_admin/features/network/subnet/subnet.type';
import { SubnetUpdateComponent } from 'app_admin/features/network/subnet/update/component';
import {
  IPIP_MODE_LABEL_MAPPER,
  isKubeOvn,
  isNetworkSupportDetailPageModule,
  isNetworkSupportUpdateField,
} from 'app_admin/features/network/subnet/util';
import { ClusterNetworkType } from 'app_admin/features/network/type';

@Component({
  templateUrl: './template.html',
  selector: 'rc-subnet-detail-info',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetDetailInfoComponent {
  @Input()
  cluster: string;

  @Input()
  data: Subnet;

  @Input()
  canUpdate = false;

  @Input()
  canDelete = false;

  @Input()
  networkType: SubnetSupportNetworkType;

  @ObservableInput(true)
  networkType$: Observable<SubnetSupportNetworkType>;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onDelete = new EventEmitter<void>();

  isKubeOvn = isKubeOvn;
  UpdatePartialType = UpdatePartialType;
  GatewayTypeEnum = GatewayTypeEnum;
  ClusterNetworkType = ClusterNetworkType;
  isNetworkSupportDetailPageModule = isNetworkSupportDetailPageModule;
  SubnetDetailPageModule = SubnetDetailPageModule;
  isNetworkSupportUpdateField = isNetworkSupportUpdateField;
  SubnetUpdatePageField = SubnetUpdatePageField;

  constructor(private readonly dialogService: DialogService) {}

  updatePartial(type: UpdatePartialType) {
    const dialogRef = this.dialogService.open(SubnetUpdateComponent, {
      data: {
        type,
        info: this.data,
        cluster: this.cluster,
        networkType: this.networkType,
      },
    });
    dialogRef.componentInstance.confirmed.pipe(first()).subscribe(_ => {
      dialogRef.close();
    });
  }

  ipipModeFormat(ipipMode: IPIP_MODE_ENUM) {
    return IPIP_MODE_LABEL_MAPPER[ipipMode];
  }
}
