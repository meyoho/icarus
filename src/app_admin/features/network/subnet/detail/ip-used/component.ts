import {
  K8SResourceList,
  K8sApiService,
  ResourceListParams,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, pluck } from 'rxjs/operators';

import {
  Cluster,
  RESOURCE_TYPES,
  ResourceType,
  Subnet,
  SubnetIp,
} from 'app/typings';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import { KUBE_OVN_ANNOTATION_PREFIX } from '../../util';

const IP_FIELD_PATH = 'spec.ipAddress';
@Component({
  templateUrl: './template.html',
  selector: 'rc-subnet-ip-used',
})
export class SubnetIpUsedComponent {
  @Input()
  data: Subnet;

  cluster: Cluster;

  columns = ['address', 'node', 'pod'];

  cluster$ = this.layoutComponent.getCluster$();

  params$ = combineLatest([this.route.queryParamMap, this.cluster$]).pipe(
    map(([params, cluster]) => ({
      cluster: cluster.metadata.name,
      labelSelector: matchLabelsToString({
        [`${KUBE_OVN_ANNOTATION_PREFIX}/subnet`]: this.data.metadata.name,
      }),
      keyword: params.get('keyword'),
      field: IP_FIELD_PATH,
    })),
  );

  search$ = this.params$.pipe(pluck('keyword'));

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchSubnetIps.bind(this),
  });

  constructor(
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {}

  fetchSubnetIps({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<SubnetIp>({
      type: RESOURCE_TYPES.SUBNET_KUBE_OVN_IP,
      cluster,
      queryParams,
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  trackByFn(_index: number, item: SubnetIp) {
    return item.metadata.uid;
  }
}
