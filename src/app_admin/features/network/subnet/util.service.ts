import {
  K8sApiService,
  TranslateService,
  catchPromise,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { concatMapTo } from 'rxjs/operators';

import { RESOURCE_TYPES, Subnet } from 'app/typings';

@Injectable()
export class SubnetUtilService {
  constructor(
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
  ) {}

  delete(resource: Subnet, cluster: string) {
    return catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('delete_subnet_title', {
          name: resource.metadata.name,
        }),
        content: this.translate.get('delete_subnet_content'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      concatMapTo(
        this.k8sApi.deleteResource({
          type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
          cluster,
          resource,
        }),
      ),
    );
  }
}
