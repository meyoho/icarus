export enum ClusterNetworkType {
  KUBE_OVN = 'cni-kube-ovn',
  CALICO = 'cni-calico',
  GALAXY = 'cni-galaxy',
  MACVLAN = 'cni-monkey',
  DEFAULT = 'default',
}
