import {
  K8sApiService,
  K8sUtilService,
  KubernetesResource,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef } from '@alauda/ui';
import { ChangeDetectorRef, Component, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReplaySubject, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Domain,
  Environments,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

@Component({
  templateUrl: 'create-domain.component.html',
})
export class CreateDomainComponent {
  @ViewChild(NgForm, { static: true }) private readonly ngForm: NgForm;
  selectedCluster$$ = new ReplaySubject<string | symbol>(1);

  assignAll = ASSIGN_ALL;

  model: {
    kind: 'full' | 'extensive';
    name: string;
    cluster: string;
    project: string;
  } = {
    kind: 'full',
    name: '',
    cluster: '',
    project: '',
  };

  loading = false;

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER,
      namespaced: true,
    })
    .pipe(
      map(list => list.items),
      publishRef(),
    );

  allProjects$ = this.projectApi.getProjects().pipe(
    map(list => list.items),
    publishRef(),
  );

  projects$ = combineLatest([this.allProjects$, this.selectedCluster$$]).pipe(
    map(([allProjects, selectedCluster]) => {
      return allProjects.filter(
        item =>
          item.spec.clusters &&
          item.spec.clusters.find(cluster => cluster.name === selectedCluster),
      );
    }),
    publishRef(),
  );

  name = (item: KubernetesResource) => this.k8sUtil.getName(item);

  label = (item: KubernetesResource) =>
    this.k8sUtil.getDisplayName(item)
      ? `${this.k8sUtil.getName(item)} (${this.k8sUtil.getDisplayName(item)})`
      : this.k8sUtil.getName(item);

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly dialogRef: DialogRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly projectApi: ProjectService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    this.loading = true;
    const seed = new Date(3000, 0).getTime();
    const metadataName = (seed - new Date().getTime() + '').padStart(
      (seed + '').length + 10,
      '0',
    );
    this.k8sApi
      .postGlobalResource<Domain>({
        type: RESOURCE_TYPES.DOMAIN,
        resource: {
          apiVersion: 'crd.alauda.io/v2',
          kind: 'Domain',
          metadata: {
            name: metadataName,
            labels: {
              [`cluster.${this.env.LABEL_BASE_DOMAIN}/name`]: this.model
                .cluster,
              [`project.${this.env.LABEL_BASE_DOMAIN}/name`]:
                this.model.cluster && this.model.project,
            },
          },
          spec: {
            name:
              this.model.kind === 'full'
                ? this.model.name
                : '*.' + this.model.name,
            kind: this.model.kind,
          },
        },
      })
      .subscribe(
        domain => {
          this.dialogRef.close(domain);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }
}
