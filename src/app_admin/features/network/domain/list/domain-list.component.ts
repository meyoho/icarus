import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sUtilService,
  TranslateService,
  matchLabelsToString,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { Domain, RESOURCE_TYPES } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

import { CreateDomainComponent } from '../create/create-domain.component';
import { UpdateDomainComponent } from '../update/update-domain.component';

interface DisplayItem {
  name: string;
  display_name: string;
}

@Component({
  templateUrl: 'domain-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DomainListComponent implements OnInit {
  assignAll = ASSIGN_ALL;
  projects: DisplayItem[] = [];
  clusters: DisplayItem[] = [];

  getDisplayName = (name: string, items: DisplayItem[]) => {
    const item = items.find(it => it.name === name);
    if (item && item.display_name) {
      return `${name} (${item.display_name})`;
    }
    return name;
  };

  list = new K8SResourceList<Domain>({
    fetcher: queryParams =>
      this.k8sApi.getGlobalResourceList<Domain>({
        type: RESOURCE_TYPES.DOMAIN,
        queryParams: {
          fieldSelector: matchLabelsToString({
            'metadata.name': queryParams.keyword,
          }),
        },
      }),
    activatedRoute: this.activatedRoute,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.DOMAIN,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sUtil: K8sUtilService,
    private readonly projectApi: ProjectService,
  ) {}

  ngOnInit() {
    forkJoin([
      this.k8sApi.getGlobalResourceList({
        type: RESOURCE_TYPES.CLUSTER,
        namespaced: true,
      }),
      this.projectApi.getProjects(),
    ])
      .pipe(
        map(([clusterList, projectList]) => [
          clusterList.items.map(item => ({
            name: item.metadata.name,
            display_name: this.k8sUtil.getDisplayName(item),
          })),
          projectList.items.map(item => ({
            name: item.metadata.name,
            display_name: this.k8sUtil.getDisplayName(item),
          })),
        ]),
        publishRef(),
      )
      .subscribe(([clusters, projects]) => {
        this.clusters = clusters;
        this.projects = projects;
        this.cdr.markForCheck();
      });
  }

  create() {
    this.dialog
      .open(CreateDomainComponent)
      .afterClosed()
      .subscribe(data => {
        if (data) {
          this.list.scanItems(items => {
            return [data].concat(items);
          });
        }
      });
  }

  update(data: Domain) {
    this.dialog
      .open(UpdateDomainComponent, { data })
      .afterClosed()
      .subscribe(domain => {
        if (domain) {
          this.list.scanItems(items =>
            items.map(item => {
              if (item.metadata.name === domain.metadata.name) {
                return domain;
              } else {
                return item;
              }
            }),
          );
        }
      });
  }

  delete(data: Domain) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_domain_confirm'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteGlobalResource({
              type: RESOURCE_TYPES.DOMAIN,
              resource: data,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.list.scanItems(items => {
          return items.filter(
            item => item.metadata.name !== data.metadata.name,
          );
        });
      })
      .catch(noop);
  }
}
