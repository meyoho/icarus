import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreateDomainComponent } from './create/create-domain.component';
import { DomainListComponent } from './list/domain-list.component';
import { DomainRoutingModule } from './routing.module';
import { UpdateDomainComponent } from './update/update-domain.component';

@NgModule({
  imports: [SharedModule, DomainRoutingModule],
  declarations: [
    DomainListComponent,
    CreateDomainComponent,
    UpdateDomainComponent,
  ],
})
export class DomainModule {}
