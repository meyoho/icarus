import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DomainListComponent } from 'app_admin/features/network/domain/list/domain-list.component';

const routes: Routes = [
  {
    path: '',
    component: DomainListComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomainRoutingModule {}
