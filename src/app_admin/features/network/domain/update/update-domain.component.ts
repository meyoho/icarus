import {
  CLUSTER,
  K8sApiService,
  KubernetesResource,
  NAME,
  PROJECT,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { Domain, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

@Component({
  templateUrl: 'update-domain.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateDomainComponent {
  @ViewChild(NgForm, { static: true }) private readonly ngForm: NgForm;
  domain = this.data.spec.name;
  model = {
    cluster: this.k8sUtil.getLabel(this.data, NAME, CLUSTER),
    project: this.k8sUtil.getLabel(this.data, NAME, PROJECT),
  };

  selectedCluster$$ = new BehaviorSubject<string>(this.model.cluster);
  loading = false;
  assignAll = ASSIGN_ALL;

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER,
      namespaced: true,
    })
    .pipe(
      map(list => list.items),
      publishRef(),
    );

  allProjects$ = this.projectApi.getProjects().pipe(
    map(list => list.items),
    publishRef(),
  );

  projects$ = combineLatest([this.allProjects$, this.selectedCluster$$]).pipe(
    map(([allProjects, selectedCluster]) => {
      return allProjects.filter(
        item =>
          item.spec.clusters &&
          item.spec.clusters.find(cluster => cluster.name === selectedCluster),
      );
    }),
    publishRef(),
  );

  name = (item: KubernetesResource) => this.k8sUtil.getName(item);

  label = (item: KubernetesResource) =>
    this.k8sUtil.getDisplayName(item)
      ? `${this.k8sUtil.getName(item)} (${this.k8sUtil.getDisplayName(item)})`
      : this.k8sUtil.getName(item);

  constructor(
    @Inject(DIALOG_DATA) private readonly data: Domain,
    private readonly dialogRef: DialogRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly projectApi: ProjectService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    this.loading = true;
    this.k8sApi
      .patchGlobalResource<Domain>({
        type: RESOURCE_TYPES.DOMAIN,
        resource: this.data,
        part: {
          metadata: {
            labels: {
              [this.k8sUtil.normalizeType(NAME, CLUSTER)]: this.model.cluster,
              [this.k8sUtil.normalizeType(NAME, PROJECT)]:
                this.model.cluster && this.model.project,
            },
          },
        },
      })
      .subscribe(
        domain => {
          this.dialogRef.close(domain);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }
}
