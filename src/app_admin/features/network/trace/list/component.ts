import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  Reason,
  ResourceListParams,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, pluck, switchMap, takeUntil, tap } from 'rxjs/operators';

import {
  RESOURCE_TYPES,
  ResourceType,
  TraceJob,
  TraceJobStatusColorMapper,
  TraceJobStatusIconMapper,
} from 'app/typings';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import { canExecute, isBuiltIn } from '../util';
import { TraceJobUtilService } from '../util.service';
const TRACER_COLUMNS_DEFINITION = [
  'name',
  'execute_type',
  'next_trig_time',
  'last_schedule_time',
  'last_schedule_status',
  'created_time',
  'action',
];
@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TraceListComponent implements OnDestroy, OnInit {
  clusterName: string;
  TRACER_COLUMNS_DEFINITION = TRACER_COLUMNS_DEFINITION;
  TraceJobStatusColorMapper = TraceJobStatusColorMapper;
  TraceJobStatusIconMapper = TraceJobStatusIconMapper;
  isBuiltIn = isBuiltIn;
  canExecute = canExecute;
  Reason = Reason;

  destroy$ = new Subject<void>();
  cluster$ = this.layoutComponent.getCluster$().pipe(
    tap(cluster => {
      this.clusterName = cluster.metadata.name;
    }),
    takeUntil(this.destroy$),
  );

  params$ = combineLatest([this.route.queryParamMap, this.cluster$]).pipe(
    map(([params, cluster]) => ({
      cluster: cluster.metadata.name,
      keyword: params.get('keyword'),
    })),
  );

  search$ = this.params$.pipe(pluck('keyword'));

  tracerSupport$ = this.cluster$.pipe(
    takeUntil(this.destroy$),
    switchMap(cluster =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.TRACE_JOB,
        cluster: cluster.metadata.name,
        action: K8sResourceAction.GET,
      }),
    ),
  );

  list: K8SResourceList;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly utilService: TraceJobUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sPermission: K8sPermissionService,
  ) {
    this.executeNow = this.executeNow.bind(this);
  }

  ngOnInit(): void {
    this.list = new K8SResourceList({
      fetchParams$: this.params$,
      fetcher: this.fetchTraceJobs.bind(this),
      watcher: seed =>
        this.params$.pipe(
          switchMap(({ cluster }) =>
            this.k8sApi.watchResourceChange(seed, {
              type: RESOURCE_TYPES.TRACE_JOB,
              cluster,
            }),
          ),
        ),
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.list.destroy();
  }

  fetchTraceJobs({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<TraceJob>({
      type: RESOURCE_TYPES.TRACE_JOB,
      cluster,
      queryParams,
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  create() {
    this.router.navigate([
      '/admin/trace/create',
      {
        cluster: this.clusterName,
      },
    ]);
  }

  deleteTraceJob(traceJob: TraceJob) {
    this.utilService.delete(traceJob, this.clusterName).subscribe(() => {
      this.message.success(this.translate.get('tracejob_delete_success'));
    });
  }

  update(traceJob: TraceJob) {
    this.router.navigate([
      `/admin/trace/update/${traceJob.metadata.name}`,
      {
        cluster: this.clusterName,
      },
    ]);
  }

  executeNow(traceJob: TraceJob) {
    this.utilService.executeNow(traceJob, this.clusterName);
  }
}
