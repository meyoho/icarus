import { K8sApiService } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';
import { finalize } from 'rxjs/operators';

import { RESOURCE_TYPES, TraceJob, getYamlApiVersion } from 'app/typings';

@Component({
  selector: 'rc-trace-job-form-container',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TraceJobFormContainerComponent implements OnInit {
  confirmed = new EventEmitter<void>();
  submitting = false;
  initialized = false;

  @Input()
  resource: TraceJob;

  @Input()
  isUpdate = false;

  @ViewChild('resourceForm', { static: false })
  form: NgForm;

  @Input()
  cluster: string;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.initialized = true;
    this.cdr.markForCheck();
  }

  cancel() {
    this.location.back();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const resource = cloneDeep(this.resource);
    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      type: RESOURCE_TYPES.TRACE_JOB,
      resource: {
        kind: 'TraceJob',
        apiVersion: getYamlApiVersion(RESOURCE_TYPES.TRACE_JOB),
        ...resource,
      },
      cluster: this.cluster,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(traceJob => {
        this.router.navigate([
          `/admin/trace/detail/${traceJob.metadata.name}`,
          { cluster: this.cluster },
        ]);
      });
  }
}
