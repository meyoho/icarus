import {
  AsyncDataLoader,
  K8sApiService,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, TraceJob, TraceJobProbe } from 'app/typings';

import { ProbeProtocolType } from '../constant';
import { canExecute, isBuiltIn } from '../util';
import { TraceJobUtilService } from '../util.service';

const PROBE_DISPLAY_MAPPER: Record<
  ProbeProtocolType,
  (arg: TraceJobProbe) => string
> = {
  ICMP: probe => probe.destination?.ip,
  TCP: probe => [probe.destination?.ip, probe.destination?.port].join(':'),
  DNS: probe => probe.destination?.domain,
  HTTP: probe => probe.destination?.url,
};

@Component({
  templateUrl: './template.html',
})
export class TraceDetailComponent {
  isBuiltIn = isBuiltIn;
  canExecute = canExecute;
  params$ = this.activatedRoute.params;

  dataManager = new AsyncDataLoader<{
    resource: TraceJob;
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly traceUtilService: TraceJobUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.getRecordSelector = this.getRecordSelector.bind(this);
  }

  dataFetcher({ cluster, name }: Params) {
    return this.k8sApi
      .watchResource<TraceJob>({
        type: RESOURCE_TYPES.TRACE_JOB,
        cluster,
        name,
      })
      .pipe(
        map(resource => ({
          resource,
        })),
      );
  }

  delete(resource: TraceJob) {
    const { cluster } = this.activatedRoute.snapshot.params;
    this.traceUtilService.delete(resource, cluster).subscribe(() => {
      this.message.success(this.translate.get('tracejob_delete_success'));
      this.jumpToListPage();
    });
  }

  update(traceJob: TraceJob) {
    const { cluster } = this.activatedRoute.snapshot.params;
    this.router.navigate([
      `/admin/trace/update/${traceJob.metadata.name}`,
      {
        cluster,
      },
    ]);
  }

  jumpToListPage = () => {
    this.router.navigate(['/admin/trace']);
  };

  probeAddressFormat(probe: TraceJobProbe) {
    return PROBE_DISPLAY_MAPPER[probe.protocol as ProbeProtocolType](probe);
  }

  getRecordSelector(jobName: string) {
    return { [this.k8sUtil.normalizeType('jobName', 'tracer')]: jobName };
  }

  executeNow(job: TraceJob) {
    const { cluster } = this.activatedRoute.snapshot.params;
    this.traceUtilService.executeNow(job, cluster);
  }
}
