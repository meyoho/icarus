import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pluck } from 'rxjs/operators';

@Component({
  template: `
    <rc-trace-job-form-container
      [isUpdate]="false"
      [cluster]="cluster$ | async"
    ></rc-trace-job-form-container>
  `,
})
export class CreateTraceJobComponent {
  cluster$ = this.activateRoute.params.pipe(pluck('cluster'));
  constructor(private readonly activateRoute: ActivatedRoute) {}
}
