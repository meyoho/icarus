import { EMPTY, K8sApiService } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, TraceJob } from 'app/typings';

@Component({
  template: `
    <rc-trace-job-form-container
      [isUpdate]="true"
      *ngIf="data$ | async as data"
      [resource]="data"
      [cluster]="cluster"
    ></rc-trace-job-form-container>
  `,
})
export class UpdateTraceJobComponent {
  cluster: string;
  resource: TraceJob;

  data$ = this.activateRoute.params.pipe(
    switchMap(({ cluster, name }) => {
      this.cluster = cluster;
      return this.k8sApi.getResource({
        type: RESOURCE_TYPES.TRACE_JOB,
        cluster,
        name,
      });
    }),
    catchError(err => {
      this.auiNotificationService.error(err);
      this.router.navigateByUrl('/admin/trace');
      return EMPTY;
    }),
  );

  constructor(
    private readonly activateRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly auiNotificationService: NotificationService,
  ) {}
}
