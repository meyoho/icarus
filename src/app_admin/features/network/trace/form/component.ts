import { DISPLAY_NAME } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, TraceJob, TraceJobProbe } from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  NATURAL_NUMBER_PATTERN,
  cronValidator,
} from 'app/utils';

export enum TraceJobExecuteType {
  ONCE = 'manual_trig',
  CRON = 'cron_trig',
}

const EXECUTE_TYPE_OPTIONS: TraceJobExecuteType[] = [
  TraceJobExecuteType.CRON,
  TraceJobExecuteType.ONCE,
];

const BUILT_IN_INSPECT_OPTIONS = ['pod_pod', 'pod_host', 'internal_dns'];

@Component({
  selector: 'rc-trace-job-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TraceJobFormComponent
  extends BaseResourceFormGroupComponent<TraceJob>
  implements OnInit {
  executeType: TraceJobExecuteType;
  EXECUTE_TYPE_OPTIONS = EXECUTE_TYPE_OPTIONS;
  BUILT_IN_INSPECT_OPTIONS = BUILT_IN_INSPECT_OPTIONS;
  NATURAL_NUMBER_PATTERN = NATURAL_NUMBER_PATTERN;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  TraceJobExecuteType = TraceJobExecuteType;
  confirmed = new EventEmitter<void>();
  loading = true;
  displayNameKey = `${this.env.LABEL_BASE_DOMAIN}/${DISPLAY_NAME}`;

  constructor(
    public injector: Injector,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  @Input()
  isUpdate: boolean;

  @Input()
  resource: TraceJob;

  @Input()
  cluster: string;

  @ViewChild('outForm', { static: false })
  ngForm: NgForm;

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.maxLength(63),
        Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
      ]),
      labels: this.fb.control({}),
      annotations: this.fb.group({
        [this.displayNameKey]: this.fb.control(''),
      }),
    });

    const spec = this.fb.group({
      schedule: this.fb.control('', [Validators.required, cronValidator()]),
      builtInProbes: [[]],
      probes: [],
      successfulJobsHistoryLimit: this.fb.control(20, [
        Validators.required,
        Validators.pattern(NATURAL_NUMBER_PATTERN.pattern),
      ]),
      failedJobsHistoryLimit: this.fb.control(20, [
        Validators.required,
        Validators.pattern(NATURAL_NUMBER_PATTERN.pattern),
      ]),
    });

    return this.fb.group({
      metadata,
      spec,
    });
  }

  getDefaultFormModel() {
    return {
      spec: {
        schedule: '',
        builtInProbes: [] as string[],
        probes: [] as TraceJobProbe[],
        successfulJobsHistoryLimit: 20,
        failedJobsHistoryLimit: 20,
      },
    };
  }

  changeExecuteType(type: TraceJobExecuteType) {
    this.form
      .get('spec.schedule')
      [type === TraceJobExecuteType.ONCE ? 'disable' : 'enable']({
        emitEvent: false,
      });
  }

  ngOnInit() {
    super.ngOnInit();
    this.loading = false;
  }

  adaptResourceModel(resource: TraceJob) {
    this.executeType =
      this.isUpdate && !resource?.spec.schedule
        ? TraceJobExecuteType.ONCE
        : TraceJobExecuteType.CRON;
    this.changeExecuteType(this.executeType);
    this.cdr.markForCheck();
    return resource;
  }
}
