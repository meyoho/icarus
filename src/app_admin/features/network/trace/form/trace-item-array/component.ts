import { Component, Injector, Optional } from '@angular/core';
import { ControlContainer, ValidatorFn } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { assoc, dissoc } from 'ramda';

import { TraceJobProbe } from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

import {
  PROBE_PROTOCOL_MAPPER,
  ProbeProtocolType,
  TRACE_JOB_SUPPORT_PROTOCOLS,
} from '../../constant';
import { convertProbeToTraceItem, convertTraceItemToProbe } from '../../util';

export interface TraceItemFormModel {
  protocol: string;
  address: string;
}
const ADDRESS_DUPLICATE_KEY = 'addressDuplicate';
const ADDRESS_PATTERN_KEY = 'addressPattern';
const ADDRESS_REQUIRED_KEY = 'addressRequired';

@Component({
  selector: 'rc-trace-item-form-array',
  templateUrl: 'template.html',
})
export class TraceItemArrayFormComponent extends BaseResourceFormArrayComponent<
  TraceJobProbe,
  TraceItemFormModel
> {
  protocols = TRACE_JOB_SUPPORT_PROTOCOLS;

  rowBackgroundColorFn = rowBackgroundColorFn;

  constructor(
    public injector: Injector,
    @Optional() public controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  getOnFormArrayResizeFn() {
    const validator = this.createValidator();
    return () => {
      const formGroup = this.fb.group(
        {
          protocol: this.fb.control('ICMP'),
          address: this.fb.control(''),
        },
        { validators: [validator] },
      );
      // 此 FormGroup 的 index
      const currentIndex = this.form.controls.length;
      formGroup.valueChanges.subscribe(_ => {
        this.form.controls.forEach((control, ind) => {
          // 每个 control 只关心自身的状态，当前 control 的重复错误状态由 validator 判断
          if (ind !== currentIndex) {
            let errorObj = null;
            const { protocol, address } = control.value;
            // 检查每个其他的 control，是否在数组里重复
            if (
              this.form.controls.some(
                ({ value }, ctrlInd) =>
                  ctrlInd !== ind &&
                  protocol === value.protocol &&
                  address === value.address,
              )
            ) {
              errorObj = assoc(ADDRESS_DUPLICATE_KEY, true, control.errors);
            } else {
              errorObj = dissoc(ADDRESS_DUPLICATE_KEY, control.errors);
            }
            control.setErrors(
              errorObj && Object.keys(errorObj).length > 0 ? errorObj : null,
            );
          }
        });
      });
      return formGroup;
    };
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): TraceItemFormModel[] {
    return [];
  }

  adaptFormModel(formModel: TraceItemFormModel[]) {
    return formModel.map(convertTraceItemToProbe);
  }

  adaptResourceModel(resource: TraceJobProbe[]) {
    if (resource) {
      return resource.map(convertProbeToTraceItem);
    }
  }

  getAddressPlaceholder(protocol: ProbeProtocolType) {
    return PROBE_PROTOCOL_MAPPER[protocol]?.createPlaceholder || '';
  }

  getAddressPattern(protocol: ProbeProtocolType) {
    return PROBE_PROTOCOL_MAPPER[protocol]?.pattern || null;
  }

  private createValidator(): ValidatorFn {
    return control => {
      const index = this.form.controls.indexOf(control);
      const others = [...this.formModel].filter((_, ind) => ind !== index);
      const currentProtocol = control.value.protocol;
      const currentAddress = control.value.address;
      // 三种类型错误是互斥的，在格式校验不通过的情形下，不应当检查重复错误
      if (!currentAddress) {
        return { [ADDRESS_REQUIRED_KEY]: true };
      }
      return !this.getAddressPattern(currentProtocol).test(currentAddress)
        ? {
            [ADDRESS_PATTERN_KEY]: this.getAddressPlaceholder(currentProtocol),
          }
        : others.some(
            ({ protocol, address }) =>
              protocol === currentProtocol && address === currentAddress,
          )
        ? {
            [ADDRESS_DUPLICATE_KEY]: true,
          }
        : null;
    };
  }
}
