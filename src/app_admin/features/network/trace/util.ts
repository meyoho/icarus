import { LABELS, METADATA, TRUE } from '@alauda/common-snippet';
import { get } from 'lodash-es';

import { TraceJob, TraceJobProbe } from 'app/typings';

import { TraceItemFormModel } from './form/trace-item-array/component';

export function convertTraceItemToProbe({
  protocol,
  address,
}: TraceItemFormModel): TraceJobProbe {
  const probe = { protocol };
  switch (protocol) {
    case 'ICMP':
      return { ...probe, destination: { ip: address } };
    case 'TCP': {
      const [ip, port] = address.split(':');
      return { ...probe, destination: { ip, port: +port } };
    }
    case 'DNS':
      return { ...probe, destination: { domain: address } };
    case 'HTTP':
      return { ...probe, destination: { url: address } };
  }
}

export function convertProbeToTraceItem(
  probe: TraceJobProbe,
): TraceItemFormModel {
  const { protocol, destination } = probe;
  const traceItem = { protocol };
  switch (protocol) {
    case 'ICMP': {
      return { ...traceItem, address: destination.ip };
    }
    case 'TCP': {
      return {
        ...traceItem,
        address: [destination.ip, destination.port].join(':'),
      };
    }
    case 'DNS':
      return { ...traceItem, address: destination.domain };
    case 'HTTP':
      return { ...traceItem, address: destination.url };
  }
}

export function isBuiltIn(traceJob: TraceJob) {
  return traceJob.metadata.labels?.['built-in'] === TRUE;
}

export function canExecute(traceJob: TraceJob) {
  return get(traceJob, [METADATA, LABELS, 'force-execute']) !== TRUE;
}
