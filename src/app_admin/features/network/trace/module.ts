import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreateTraceJobComponent } from './create/component';
import { TraceDetailComponent } from './detail/component';
import { TraceJobFormContainerComponent } from './form-container/component';
import { TraceJobFormComponent } from './form/component';
import { TraceItemArrayFormComponent } from './form/trace-item-array/component';
import { TraceListComponent } from './list/component';
import { TracerRoutingModule } from './routing.module';
import { TraceJobStatusComponent } from './status/component';
import { TraceReportDetailComponent } from './trace-report/detail/component';
import { TraceReportListComponent } from './trace-report/list/component';
import { UpdateTraceJobComponent } from './update/component';
import { TraceJobUtilService } from './util.service';

@NgModule({
  imports: [SharedModule, TracerRoutingModule],
  declarations: [
    TraceListComponent,
    TraceDetailComponent,
    TraceReportListComponent,
    TraceReportDetailComponent,
    TraceJobFormComponent,
    TraceJobFormContainerComponent,
    TraceItemArrayFormComponent,
    UpdateTraceJobComponent,
    CreateTraceJobComponent,
    TraceJobStatusComponent,
  ],
  providers: [TraceJobUtilService],
})
export class TraceModule {}
