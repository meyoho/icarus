import {
  K8SResourceList,
  K8sApiService,
  ResourceListParams,
  StringMap,
  matchLabelsToString,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { RESOURCE_TYPES, TraceJobReport } from 'app/typings';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

@Component({
  selector: 'rc-trace-report-list',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TraceReportListComponent implements OnDestroy, OnInit {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly layoutComponent: AdminLayoutComponent,
  ) {}

  ngOnInit(): void {
    this.list = new K8SResourceList({
      fetcher: this.fetchTraceRecords.bind(this),
      fetchParams$: this.cluster$.pipe(
        map(cluster => ({
          cluster: cluster.metadata.name,
        })),
      ),
      watcher: seed =>
        this.k8sApi.watchResourceChange(seed, {
          type: RESOURCE_TYPES.TRACE_JOB_RECORD,
          cluster: this.clusterName,
          queryParams: {
            labelSelector: matchLabelsToString(this.labelSelector),
          },
        }),
    });
  }

  @Input()
  labelSelector: StringMap;

  clusterName: string;

  private readonly onDestroy$ = new Subject<void>();

  cluster$ = this.layoutComponent.getCluster$().pipe(
    tap(cluster => {
      this.clusterName = cluster.metadata.name;
    }),
    takeUntil(this.onDestroy$),
  );

  columns = ['name', 'schedule_status', 'schedule_time', 'complete_time'];

  list: K8SResourceList;

  fetchTraceRecords({ cluster, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<TraceJobReport>({
      type: RESOURCE_TYPES.TRACE_JOB_RECORD,
      cluster,
      queryParams: {
        ...queryParams,
        labelSelector: matchLabelsToString(this.labelSelector),
      },
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  view(item: TraceJobReport) {
    this.router.navigate([
      '/admin/trace/report',
      item.metadata.name,
      { cluster: this.clusterName },
    ]);
  }
}
