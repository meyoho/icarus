import { AsyncDataLoader, K8sApiService } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { RESOURCE_TYPES, TraceJobProbe, TraceJobReport } from 'app/typings';

import {
  PROBE_PROTOCOL_MAPPER,
  ProbeProtocolType,
  TRACE_JOB_SUPPORT_PROTOCOLS,
} from '../../constant';

interface ReportFilter {
  protocol: string;
  onlyFailed: boolean;
}
@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TraceReportDetailComponent {
  params$ = this.activatedRoute.params;
  filterObj: ReportFilter = {
    protocol: null,
    onlyFailed: true,
  };

  reportFilter$ = new BehaviorSubject(this.filterObj);

  showFailFlag = false;

  dataManager = new AsyncDataLoader<{
    resource: TraceJobReport;
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  protocols = TRACE_JOB_SUPPORT_PROTOCOLS;
  detailColumns = [
    'source_pod_ip',
    'source_node',
    'source_mode',
    'type',
    'destination',
    'result',
  ];

  PROBE_PROTOCOL_MAPPER = PROBE_PROTOCOL_MAPPER;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
  ) {}

  dataFetcher({ cluster, name }: Params) {
    return this.k8sApi
      .watchResource<TraceJobReport>({
        type: RESOURCE_TYPES.TRACE_JOB_RECORD,
        cluster,
        name,
      })
      .pipe(
        map(resource => ({
          resource,
        })),
      );
  }

  jumpToListPage = () => {
    this.router.navigate(['/admin/trace']);
  };

  filterFn(filter: ReportFilter, data: TraceJobProbe[]) {
    return data.filter(item =>
      !filter
        ? true
        : (filter.protocol ? item.protocol === filter.protocol : true) &&
          (filter.onlyFailed ? !item.result : true),
    );
  }

  showFail(onlyFailed: boolean) {
    this.filterObj = { ...this.filterObj, onlyFailed };
    this.reportFilter$.next(this.filterObj);
  }

  selectType(protocol: string) {
    this.filterObj = { ...this.filterObj, protocol };
    this.reportFilter$.next(this.filterObj);
  }

  getProbeValue(probe: TraceJobProbe) {
    return PROBE_PROTOCOL_MAPPER[
      probe.protocol as ProbeProtocolType
    ].valueMapper(probe);
  }
}
