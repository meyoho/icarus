import { TraceJobProbe } from 'app/typings';
import {
  DOMAIN_PATTERN,
  HTTP_ADDRESS_PATTERN,
  IP_ADDRESS_PATTERN,
  IP_ADDRESS_PORT_PATTERN,
} from 'app/utils';

interface ProbeProtocolData {
  valueMapper: (value: TraceJobProbe) => string | number;
  createPlaceholder: string;
  pattern: RegExp;
}

export type ProbeProtocolType = 'ICMP' | 'TCP' | 'DNS' | 'HTTP';

export const PROBE_PROTOCOL_MAPPER: Record<
  ProbeProtocolType,
  ProbeProtocolData
> = {
  ICMP: {
    valueMapper: probe => probe.destination?.ip,
    createPlaceholder: 'input_icmp_placeholder',
    pattern: IP_ADDRESS_PATTERN.pattern,
  },
  TCP: {
    valueMapper: probe =>
      [probe.destination?.ip, probe.destination?.port].join(':'),
    createPlaceholder: 'input_tcp_placeholder',
    pattern: IP_ADDRESS_PORT_PATTERN.pattern,
  },
  DNS: {
    valueMapper: probe => probe.destination?.domain,
    createPlaceholder: DOMAIN_PATTERN.tip,
    pattern: DOMAIN_PATTERN.pattern,
  },
  HTTP: {
    valueMapper: probe => probe.destination?.url,
    createPlaceholder: 'input_http_placeholder',
    pattern: HTTP_ADDRESS_PATTERN.pattern,
  },
};

export const TRACE_JOB_SUPPORT_PROTOCOLS = Object.keys(PROBE_PROTOCOL_MAPPER);
