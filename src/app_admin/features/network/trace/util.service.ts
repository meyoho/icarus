import {
  K8sApiService,
  TranslateService,
  catchPromise,
  noop,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { cloneDeep, set } from 'lodash-es';
import { concatMap, concatMapTo } from 'rxjs/operators';

import { RESOURCE_TYPES, TraceJob } from 'app/typings';

@Injectable()
export class TraceJobUtilService {
  constructor(
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
  ) {}

  delete(resource: TraceJob, cluster: string) {
    return catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('delete_tracejob_title', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      concatMapTo(
        this.k8sApi.deleteResource({
          type: RESOURCE_TYPES.TRACE_JOB,
          cluster,
          resource,
        }),
      ),
    );
  }

  executeNow(resource: TraceJob, cluster: string) {
    return catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('trace_job_execute_now_confirm', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('execute_now'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMap(_ => {
          resource = cloneDeep(resource);
          set(resource, 'metadata.labels["force-execute"]', 'true');
          return this.k8sApi.putResource({
            type: RESOURCE_TYPES.TRACE_JOB,
            resource,
            cluster,
          });
        }),
      )
      .subscribe(noop);
  }
}
