import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateTraceJobComponent } from './create/component';
import { TraceDetailComponent } from './detail/component';
import { TraceListComponent } from './list/component';
import { TraceReportDetailComponent } from './trace-report/detail/component';
import { UpdateTraceJobComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: TraceListComponent,
  },
  {
    path: 'create',
    component: CreateTraceJobComponent,
  },
  {
    path: 'update/:name',
    component: UpdateTraceJobComponent,
  },
  {
    path: 'detail/:name',
    component: TraceDetailComponent,
  },
  {
    path: 'job_record/:name',
    component: TraceReportDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TracerRoutingModule {}
