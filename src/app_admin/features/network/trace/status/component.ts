import { Component, Input } from '@angular/core';

import {
  TraceJobStatusColorMapper,
  TraceJobStatusEnum,
  TraceJobStatusIconMapper,
} from 'app/typings';

@Component({
  selector: 'rc-trace-job-status',
  styleUrls: ['./style.scss'],
  template: `
    <div *ngIf="status; else nodata" fxLayout>
      <rc-status-icon
        [status]="status"
        [colorMapper]="TraceJobStatusColorMapper"
        [iconMapper]="TraceJobStatusIconMapper"
        [withText]="false"
        [ngStyle]="{ 'margin-right': 0 }"
      ></rc-status-icon>
      {{ status | lowercase | translate }}
      <aui-icon
        icon="basic:overview"
        *ngIf="reason"
        class="message-tip-icon"
        [auiTooltip]="reason"
        size="14"
        auiTooltipType="info"
        [style.margin-left]="'6px'"
      ></aui-icon>
    </div>

    <ng-template #nodata>-</ng-template>
  `,
})
export class TraceJobStatusComponent {
  @Input()
  status: TraceJobStatusEnum;

  @Input()
  reason: string;

  TraceJobStatusColorMapper = TraceJobStatusColorMapper;
  TraceJobStatusIconMapper = TraceJobStatusIconMapper;
}
