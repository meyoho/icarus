import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
  TOKEN_GLOBAL_NAMESPACE,
  isAllowed,
  matchLabelsToString,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { ChartRepo, RESOURCE_TYPES } from 'app/typings';
import { ACTION, ASSIGN_ALL } from 'app/utils';

import { isChartRepoPending, shouldShowStatus } from '../util';
import { ChartRepoUtilService } from '../util.service';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartRepoListComponent implements OnInit, OnDestroy {
  columns = [NAME, 'type', 'project', 'created_time', ACTION];
  list: K8SResourceList<ChartRepo>;
  permissions$ = this.k8sPermission
    .getAccess({
      type: RESOURCE_TYPES.CHART_REPO,
      namespace: this.globalNamespace,
      action: COMMON_WRITABLE_ACTIONS,
    })
    .pipe(isAllowed());

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  ASSIGN_ALL = ASSIGN_ALL;
  shouldShowStatus = shouldShowStatus;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApiService: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly util: ChartRepoUtilService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  isChartRepoPending = isChartRepoPending;

  ngOnInit() {
    this.list = new K8SResourceList<ChartRepo>({
      fetcher: this.fetchResources.bind(this),
      fetchParams$: this.activatedRoute.queryParams,
      limit: 20,
      watcher: seed =>
        this.k8sApiService.watchGlobalResourceChange(seed, {
          type: RESOURCE_TYPES.CHART_REPO,
          namespaced: true,
        }),
    });
  }

  ngOnDestroy() {
    this.list.destroy();
  }

  fetchResources(queryParams: ResourceListParams) {
    return this.k8sApiService.getGlobalResourceList<ChartRepo>({
      namespaced: true,
      type: RESOURCE_TYPES.CHART_REPO,
      queryParams: {
        fieldSelector: matchLabelsToString({
          'metadata.name': queryParams.keyword,
        }),
      },
    });
  }

  update(item: ChartRepo) {
    this.util.edit(item).subscribe(it => {
      if (it) {
        this.list.update(it);
      }
    });
  }

  delete(item: ChartRepo) {
    this.util.delete(item).subscribe(() => {
      this.list.delete(item);
    });
  }

  addRepo() {
    this.util.edit().subscribe(item => {
      if (item) {
        this.router.navigate([
          'admin',
          'chartrepo',
          'detail',
          item.metadata.name,
        ]);
      }
    });
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }
}
