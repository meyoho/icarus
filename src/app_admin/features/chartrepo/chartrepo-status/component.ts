import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ChartRepoPhaseEnum } from 'app/typings';

@Component({
  selector: 'rc-chartrepo-status',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartRepoStatusComponent {
  @Input() status: ChartRepoPhaseEnum;

  getStatusText(status: ChartRepoPhaseEnum) {
    return status === ChartRepoPhaseEnum.Pending
      ? 'sync_pending'
      : 'sync_failed';
  }

  getStatusIcon(status: ChartRepoPhaseEnum) {
    return status === ChartRepoPhaseEnum.Pending
      ? 'basic:sync_circle_s'
      : 'basic:close_circle_s';
  }

  getStatusColor(status: ChartRepoPhaseEnum) {
    return status === ChartRepoPhaseEnum.Pending ? 'primary' : 'error';
  }

  isPending(status: ChartRepoPhaseEnum) {
    return status === ChartRepoPhaseEnum.Pending;
  }
}
