import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { EMPTY } from 'rxjs';
import { catchError, first, switchMap, tap } from 'rxjs/operators';

import { ForceDeleteComponent } from 'app/shared/components/force-delete/component';
import { ChartRepo, RESOURCE_TYPES } from 'app/typings';

import { ChartRepoFormDialogComponent } from './dialog/chartrepo/component';

@Injectable()
export class ChartRepoUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
  ) {}

  delete(resource: ChartRepo) {
    const name = resource.metadata.name;
    const dialogRef = this.dialog.open(ForceDeleteComponent, {
      data: {
        name,
        title: this.translate.get('delete_chartrepo'),
        content: this.translate.get('delete_chartrepo_confirm', {
          name,
        }),
        confirmText: 'delete',
      },
    });

    return dialogRef.componentInstance.close.pipe(
      switchMap(isConfirm => {
        if (!isConfirm) {
          dialogRef.close();
          return EMPTY;
        }
        return this.k8sApi
          .deleteGlobalResource({
            type: RESOURCE_TYPES.CHART_REPO,
            namespaced: true,
            resource,
          })
          .pipe(
            tap(() => {
              dialogRef.close();
              this.message.success(this.translate.get('delete_success'));
            }),
            catchError(() => {
              return EMPTY;
            }),
          );
      }),
    );
  }

  edit(resource?: ChartRepo) {
    const dialogRef = this.dialog.open(ChartRepoFormDialogComponent, {
      size: DialogSize.Big,
      data: {
        resource,
      },
    });
    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(data => {
        if (data) {
          this.message.success(
            this.translate.get(resource ? 'update_success' : 'add_success'),
          );
        }
        dialogRef.close();
      }),
    );
  }
}
