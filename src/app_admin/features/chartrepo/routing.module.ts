import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChartRepoDetailComponent } from './detail/component';
import { ChartRepoListComponent } from './list/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ChartRepoListComponent,
  },
  {
    path: 'detail/:name',
    component: ChartRepoDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartRepoRoutingModule {}
