import {
  DISPLAY_NAME,
  PROJECT,
  TOKEN_GLOBAL_NAMESPACE,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { get, unset } from 'lodash-es';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Subject, of } from 'rxjs';
import { catchError, pluck, takeUntil } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  ChartRepoCreateSpec,
  ChartRepoType,
  RESOURCE_DEFINITIONS,
  SecretType,
  getYamlApiVersion,
} from 'app/typings';
import { ASSIGN_ALL, K8S_RESOURCE_NAME_BASE } from 'app/utils';

interface ChartRepoCreateSpecFormModel {
  name: string;
  displayName?: string;
  type?: ChartRepoType;
  vcsAddress?: string;
  vcsPath?: string;
  url?: string;
  project?: string;
  secretName?: string;
  username?: string;
  password?: string;
}

@Component({
  selector: 'rc-chartrepo-create-spec-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartRepoCreateSpecFormComponent
  extends BaseResourceFormComponent<
    ChartRepoCreateSpec,
    ChartRepoCreateSpecFormModel
  >
  implements OnInit, OnDestroy {
  @Input() isUpdate: boolean;
  ASSIGN_ALL = ASSIGN_ALL;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  showPassword = false;

  onDestroy$ = new Subject<void>();

  projects$ = this.projectService.getProjects().pipe(
    pluck('items'),
    catchError(() => of([])),
    publishRef(),
  );

  constructor(
    injector: Injector,
    private readonly projectService: ProjectService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((type: string) => {
        if (type === 'Chart') {
          this.form.get('url').enable({ emitEvent: false });
          this.form.get('vcsAddress').disable({ emitEvent: false });
          this.form.get('vcsPath').disable({ emitEvent: false });
        } else {
          this.form.get('url').disable({ emitEvent: false });
          this.form.get('vcsAddress').enable({ emitEvent: false });
          this.form.get('vcsAddress').updateValueAndValidity();
          this.form.get('vcsPath').enable({ emitEvent: false });
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
      ]),
      type: this.fb.control('Chart'),
      displayName: this.fb.control(''),
      url: this.fb.control('', [Validators.required]),
      vcsAddress: this.fb.control('', [Validators.required]),
      vcsPath: this.fb.control('', [Validators.required]),
      project: this.fb.control(''),
      secretName: this.fb.control(''),
      username: this.fb.control(''),
      password: this.fb.control(''),
    });
  }

  getResourceMergeStrategy() {
    return false;
  }

  adaptResourceModel(
    resource: ChartRepoCreateSpec,
  ): ChartRepoCreateSpecFormModel {
    if (!resource) {
      return null;
    }
    const formModel: ChartRepoCreateSpecFormModel = {
      name: resource.chartRepo.metadata.name,
      displayName: this.k8sUtil.getDisplayName(resource.chartRepo) || '',
      url: resource.chartRepo.spec.url,
      project: this.k8sUtil.getLabel(resource.chartRepo, 'project') || '',
      type: resource.chartRepo.spec.type || ChartRepoType.Chart,
      vcsAddress: get(resource, ['chartRepo', 'spec', 'source', 'url'], ''),
      vcsPath: get(resource, ['chartRepo', 'spec', 'source', 'path'], ''),
    };
    if (resource.secret) {
      formModel.secretName = resource.secret.metadata.name;
      formModel.username = atob(resource.secret.data.username);
      if (resource.secret.data.password) {
        formModel.password = atob(resource.secret.data.password);
      }
    }

    return formModel;
  }

  adaptFormModel(formModel: ChartRepoCreateSpecFormModel): ChartRepoCreateSpec {
    const resource: ChartRepoCreateSpec = {
      chartRepo: {
        apiVersion: getYamlApiVersion(RESOURCE_DEFINITIONS.CHART_REPO),
        kind: 'ChartRepo',
        metadata: {
          name: formModel.name || '',
          namespace: this.globalNamespace,
          labels: {},
          annotations: {},
        },
        spec: {
          type: formModel.type,
          url: this.isVcs() ? null : formModel.url,
          source: this.isVcs()
            ? {
                url: formModel.vcsAddress,
                path: formModel.vcsPath,
              }
            : null,
        },
      },
    };
    const projectLabel = this.k8sUtil.normalizeType(PROJECT);
    const displayNameAnnotation = this.k8sUtil.normalizeType(DISPLAY_NAME);
    if (formModel.project) {
      resource.chartRepo.metadata.labels[projectLabel] = formModel.project;
    } else {
      delete resource.chartRepo.metadata.labels[projectLabel];
    }
    if (formModel.displayName) {
      resource.chartRepo.metadata.annotations[displayNameAnnotation] =
        formModel.displayName;
    } else {
      delete resource.chartRepo.metadata.labels[displayNameAnnotation];
    }
    if (formModel.username) {
      const secretName = formModel.secretName || formModel.name;
      resource.chartRepo.spec.secret = {
        name: secretName,
      };
      resource.secret = {
        apiVersion: getYamlApiVersion(RESOURCE_DEFINITIONS.SECRET),
        kind: 'Secret',
        type: SecretType.Opaque,
        metadata: {
          name: secretName,
          namespace: this.globalNamespace,
        },
        data: {
          username: btoa(formModel.username),
          password: formModel.password ? btoa(formModel.password) : '',
        },
      };
    } else {
      delete resource.secret;
      unset(resource, ['chartRepo', 'spec', 'secret']);
    }
    return resource;
  }

  getDefaultFormModel(): ChartRepoCreateSpecFormModel {
    return {
      name: '',
      type: ChartRepoType.Chart,
      vcsPath: '/',
    };
  }

  isVcs() {
    return [ChartRepoType.Git, ChartRepoType.Svn].includes(
      this.form.get('type').value,
    );
  }
}
