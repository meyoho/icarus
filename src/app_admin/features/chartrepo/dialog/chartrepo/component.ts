import { K8sApiService, noop } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import {
  ChartRepo,
  ChartRepoCreate,
  ChartRepoCreateSpec,
  RESOURCE_TYPES,
  Secret,
} from 'app/typings';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartRepoFormDialogComponent implements OnInit {
  formModel: ChartRepoCreateSpec;
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly catalogApi: CatalogApiService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA)
    public dialogData: {
      resource: ChartRepo;
    },
  ) {}

  @Output()
  close = new EventEmitter<ChartRepo>();

  @ViewChild(NgForm, { static: false })
  private readonly form: NgForm;

  submitting = false;
  initialized = false;

  ngOnInit() {
    if (this.dialogData.resource) {
      const { name } = this.dialogData.resource.metadata;
      this.k8sApi
        .getGlobalResource({
          type: RESOURCE_TYPES.SECRET,
          namespaced: true,
          name,
        })
        .pipe(catchError(() => of(null)))
        .subscribe((secret: Secret) => {
          this.formModel = {
            chartRepo: this.dialogData.resource,
            secret,
          };
          this.initialized = true;
          this.cdr.markForCheck();
        });
    } else {
      this.initialized = true;
    }
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const metadata = this.formModel.chartRepo.metadata;
    const payload: ChartRepoCreate = {
      apiVersion: 'v1',
      kind: 'ChartRepoCreate',
      metadata: {
        name: metadata.name,
        namespace: metadata.namespace,
      },
      spec: this.formModel,
    };
    (this.dialogData.resource
      ? this.catalogApi.updateChartRepo(payload)
      : this.catalogApi.createChartRepo(payload)
    )
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe((res: ChartRepo) => this.close.next(res), noop);
  }

  cancel() {
    this.close.next(null);
  }
}
