import { DESCRIPTION } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';
import { get } from 'lodash-es';

import { Chart } from 'app/typings';

import { getChartName } from '../util';

@Component({
  selector: 'rc-helm-chart-card',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmChartCardComponent {
  @Input() chart: Chart;
  @Input() isPlaceholder: boolean;
  @Input() allowCreate: boolean;
  @Input() showAction: boolean;
  @HostBinding('class.invisible')
  get hiddenState() {
    return !!this.isPlaceholder;
  }

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onCreate = new EventEmitter<Chart>();

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onOpenHrDetail = new EventEmitter<string>();

  getChartName = getChartName;

  getChartIconUrl(chart: Chart) {
    return get(chart, ['spec', 'versions', '0', 'icon']);
  }

  getChartDesc(chart: Chart) {
    return get(chart, ['spec', 'versions', '0', DESCRIPTION]);
  }
}
