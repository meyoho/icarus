import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { HelmChartCardComponent } from './chart-card/component';

@NgModule({
  imports: [SharedModule],
  exports: [HelmChartCardComponent],
  declarations: [HelmChartCardComponent],
})
export class ChartSharedModule {}
