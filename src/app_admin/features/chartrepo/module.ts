import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ChartRepoStatusComponent } from './chartrepo-status/component';
import { ChartRepoDetailComponent } from './detail/component';
import { ChartRepoFormDialogComponent } from './dialog/chartrepo/component';
import { ChartRepoCreateSpecFormComponent } from './form/chartrepo-create-spec/component';
import { HelmChartsComponent } from './helm-charts/component';
import { ChartRepoListComponent } from './list/component';
import { ChartRepoRoutingModule } from './routing.module';
import { ChartSharedModule } from './shared.module';
import { ChartRepoUtilService } from './util.service';

@NgModule({
  imports: [SharedModule, ChartSharedModule, ChartRepoRoutingModule],
  declarations: [
    ChartRepoListComponent,
    ChartRepoCreateSpecFormComponent,
    ChartRepoFormDialogComponent,
    ChartRepoDetailComponent,
    ChartRepoStatusComponent,
    HelmChartsComponent,
  ],
  providers: [ChartRepoUtilService],
})
export class ChartRepoModule {}
