import { Chart, ChartRepo, ChartRepoPhaseEnum } from 'app/typings';

export function shouldShowStatus(resource: ChartRepo) {
  return (
    resource.status &&
    resource.status.phase &&
    [ChartRepoPhaseEnum.Failed, ChartRepoPhaseEnum.Pending].includes(
      resource.status.phase,
    )
  );
}

export function isChartRepoPending(resource: ChartRepo) {
  return (
    resource.status && resource.status.phase === ChartRepoPhaseEnum.Pending
  );
}

export function getChartName(chart: Chart) {
  const name = chart.metadata.name;
  return name.includes('.') ? name.split('.')[0] : name;
}
