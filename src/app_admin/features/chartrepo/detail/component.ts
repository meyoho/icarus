import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TOKEN_GLOBAL_NAMESPACE,
  matchLabelsToString,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectorRef, Component, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { combineLatest } from 'rxjs';
import { finalize, map, take } from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import {
  ChartRepo,
  ChartRepoPhaseEnum,
  ChartRepoType,
  RESOURCE_TYPES,
} from 'app/typings';
import { ASSIGN_ALL, STATUS } from 'app/utils';

import { HelmChartsComponent } from '../helm-charts/component';
import { isChartRepoPending, shouldShowStatus } from '../util';
import { ChartRepoUtilService } from '../util.service';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ChartRepoDetailComponent {
  @ViewChild(HelmChartsComponent, { static: false })
  helmChartsComponent: HelmChartsComponent;

  retrySubmitting: boolean;
  ChartRepoPhaseEnum = ChartRepoPhaseEnum;
  ASSIGN_ALL = ASSIGN_ALL;
  totalChart: number;

  params$ = this.activatedRoute.params.pipe(
    map(params => ({
      name: params.name,
    })),
    publishRef(),
  );

  name = this.activatedRoute.snapshot.params.name;
  labelSelector = matchLabelsToString({
    repo: this.name,
  });

  permissions$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.CHART_REPO,
      namespace: this.globalNamespace,
      action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
    })
    .pipe(publishRef());

  dataManager = new AsyncDataLoader<{
    resource: ChartRepo;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.watchGlobalResource<ChartRepo>({
          type: RESOURCE_TYPES.CHART_REPO,
          namespaced: true,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) =>
          resource
            ? {
                resource,
                permissions,
              }
            : null,
        ),
      );
    },
  });

  shouldShowStatus = shouldShowStatus;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly util: ChartRepoUtilService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly catalogApi: CatalogApiService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  isChartRepoPending = isChartRepoPending;

  update(item: ChartRepo) {
    this.util.edit(item).subscribe(it => {
      if (it) {
        this.dataManager.reload();
      }
    });
  }

  delete(item: ChartRepo) {
    this.util.delete(item).subscribe(this.jumpToListPage, noop);
  }

  retry(resource: ChartRepo) {
    this.retrySubmitting = true;
    this.catalogApi
      .triggerRepoSync(resource)
      .pipe(
        finalize(() => {
          this.retrySubmitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.dataManager.reload();
      });
    this.subscribeForRefresh();
  }

  refresh() {
    this.dataManager.reload();
    this.subscribeForRefresh();
  }

  getLastSyncTime = (resource: ChartRepo) => {
    return get(resource, ['metadata', 'annotations', 'cpaas.io/last-sync-at']);
  };

  getFailedReason(resource: ChartRepo) {
    return get(resource, [STATUS, 'reason']);
  }

  refreshTotalCharts(total: number) {
    this.totalChart = total;
  }

  getStatus(resource: ChartRepo) {
    return resource.status ? resource.status.phase : ChartRepoPhaseEnum.Pending;
  }

  isVcs(resource: ChartRepo) {
    return [ChartRepoType.Git, ChartRepoType.Svn].includes(resource.spec.type);
  }

  private subscribeForRefresh() {
    this.dataManager.data$.pipe(take(1)).subscribe(data => {
      if (
        data.resource.status &&
        data.resource.status.phase === ChartRepoPhaseEnum.Synced &&
        this.helmChartsComponent
      ) {
        this.helmChartsComponent.refresh();
      }
    });
  }

  jumpToListPage = () => {
    this.router.navigate(['admin', 'chartrepo']);
  };
}
