import { K8sApiService, publishRef } from '@alauda/common-snippet';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Subject, merge, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  pluck,
  scan,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { Chart, RESOURCE_TYPES } from 'app/typings';

import { getChartName } from '../util';

type ChartsScanner = (state: ChartsState) => ChartsState;
interface ChartsState {
  charts: Chart[];
  filterKey: string;
  loading: boolean;
}

@Component({
  selector: 'rc-helm-charts',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmChartsComponent implements OnChanges, AfterViewInit {
  @Input() labelSelector: string;
  @Output() total = new EventEmitter<number>();
  chartsInit$ = new Subject<void>();
  filterChange$ = new Subject<ChartsScanner>();
  chartsState$ = merge(
    this.chartsInit$.pipe(
      switchMap(() =>
        this.getCharts$().pipe(
          map(charts => (state: ChartsState) => ({
            ...state,
            charts,
            loading: false,
          })),
          startWith((state: ChartsState) => ({
            ...state,
            loading: true,
          })),
          debounceTime(0),
        ),
      ),
    ),
    this.filterChange$,
  ).pipe(
    scan((state: ChartsState, action: ChartsScanner) => action(state), {
      charts: [],
      filterKey: '',
      loading: false,
    }),
    tap((state: ChartsState) => {
      if (state && state.charts) {
        this.total.next(state.charts.length);
      }
    }),
    publishRef(),
  );

  charts$ = this.chartsState$.pipe(
    map(state => {
      return state.filterKey
        ? state.charts.filter(c => getChartName(c).includes(state.filterKey))
        : state.charts;
    }),
  );

  chartsLength$ = this.chartsState$.pipe(map(state => state.charts.length));
  chartsLoading$ = this.chartsState$.pipe(map(state => state.loading));

  constructor(private readonly k8sApi: K8sApiService) {}

  ngAfterViewInit() {
    this.chartsInit$.next();
  }

  ngOnChanges({ labelSelector }: SimpleChanges) {
    if (
      labelSelector &&
      labelSelector.currentValue &&
      !labelSelector.firstChange
    ) {
      this.refresh();
    }
  }

  filterKeyChange(filterKey: string) {
    this.filterChange$.next((state: ChartsState) => {
      return {
        ...state,
        filterKey,
      };
    });
  }

  getCharts$() {
    return this.k8sApi
      .getGlobalResourceList<Chart>({
        type: RESOURCE_TYPES.CHART,
        namespaced: true,
        queryParams: {
          labelSelector: this.labelSelector,
        },
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      );
  }

  refresh() {
    this.chartsInit$.next();
  }

  trackByUid(_index: number, chart: Chart) {
    return chart.metadata.uid;
  }
}
