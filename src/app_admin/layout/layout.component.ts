import {
  FeatureGateService,
  Reason,
  createRecursiveFilter,
  publishRef,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Inject, Injector } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/shared/layout/base-layout-component';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Cluster, Environments } from 'app/typings';

/**
 * 白名单内的 url 路径会展示集群选择组件
 * 目前只在 feature 列表页响应集群变化事件
 * feature 的列表页路由需要是 xxx/list 格式
 */
const CLUSTER_FEATURE_URL_WHITELIST = [
  '/alarm/list',
  '/subnet/list',
  '/trace/list',
  '/load_balancer/list',
  '/pv/list',
  '/storageclass/list',
  '/cluster_addon/list',
  '/monitor',
  '/event',
  '/pod_security_policy',
];

@Component({
  selector: 'rc-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['../../app/shared/layout/layout.common.scss'],
  animations: [
    trigger('scaleInOut', [
      state(
        '0',
        style({ transform: 'scale(0.8)', opacity: '0', display: 'none' }),
      ),
      state('1', style({ transform: 'scale(1)', opacity: '1' })),
      transition('0 => 1', [
        animate('.1s', style({ transform: 'scale(1.1)', opacity: '1' })),
        animate('.1s', style({ transform: 'scale(1)' })),
      ]),
      transition('1 => 0', animate('.1s')),
    ]),
  ],
})
export class AdminLayoutComponent extends BaseLayoutComponent {
  routerStateName: Observable<string>;
  titleText = '';
  shouldShowClusterBadge = true;
  clusterName: string;
  navConfigAddress = 'custom/navconfig-admin.yaml';
  private readonly cluster$$ = new BehaviorSubject<Cluster>(null);
  private readonly cluster$ = this.cluster$$.asObservable().pipe(
    filter(_ => !!_),
    distinctUntilChanged(),
    publishRef(),
  );

  reason = Reason;

  constructor(
    injector: Injector,
    private readonly fgService: FeatureGateService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {
    super(injector);
    this.initRouterObserver();
    // 确保在页面没有其他订阅的情况下，保持第一次获得的值
    this.cluster$.pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  filterFg = (items: NavItemConfig[]) => {
    return this.fgService.filterEnabled(
      items,
      item => item.gate,
      '',
      createRecursiveFilter(
        item => item.children,
        (item, children) => ({ ...item, children }),
      ),
    );
  };

  getCluster$() {
    return this.cluster$;
  }

  getCluster() {
    return this.cluster$$.value;
  }

  onClusterChange(cluster: Cluster) {
    this.cluster$$.next(cluster);
  }

  buildUrl(href: string) {
    return '/admin/' + href;
  }

  private initRouterObserver() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        const currentUrl = urlAfterRedirects.split(/[#?]/)[0];
        if (
          CLUSTER_FEATURE_URL_WHITELIST.find(
            url => currentUrl === '/admin' + url,
          )
        ) {
          this.shouldShowClusterBadge = true;
        } else {
          this.shouldShowClusterBadge = false;
        }
      });
  }
}
