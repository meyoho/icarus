import {
  K8sUtilService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { BehaviorSubject, Observable, Subject, combineLatest } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import * as clusterActions from 'app/store/actions/clusters.actions';
import { ClustersFacadeService } from 'app/store/facades/clusters.facade';
import { Cluster } from 'app/typings';

const CLUSTER_KEY = 'CLUSTER';

@Component({
  selector: 'rc-cluster-badge',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterBadgeComponent implements OnInit, OnDestroy {
  selectedCluster: Cluster;

  clusters$: Observable<Cluster[]>;
  clusterLength$: Observable<number>;
  onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject('');
  filteredClusters$: Observable<Cluster[]>;

  @Output() clusterChange = new EventEmitter<Cluster>();

  constructor(
    private readonly clustersFacade: ClustersFacadeService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.clustersFacade.dispatch(new clusterActions.GetClusters());
    this.clusters$ = this.clustersFacade.getClusters$();
    this.clusterLength$ = this.clusters$.pipe(map(clusters => clusters.length));
    this.filteredClusters$ = combineLatest([this.clusters$, this.search$]).pipe(
      takeUntil(this.onDestroy$),
      map(([clusters, filter]) => {
        return clusters.filter(cluster =>
          cluster.metadata.name.startsWith(filter),
        );
      }),
    );
    this.clusters$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((clusters: Cluster[]) => {
        const name = window.sessionStorage.getItem(CLUSTER_KEY);
        const cluster = clusters.find(c => c.metadata.name === name);
        if (cluster) {
          this.onClusterSelected(cluster);
        } else if (clusters.length > 0) {
          this.onClusterSelected(clusters[0]);
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  name = (item: KubernetesResource) => this.k8sUtil.getName(item);

  displayName = (item: KubernetesResource) => this.k8sUtil.getDisplayName(item);

  onClusterSelected(cluster: Cluster) {
    this.selectedCluster = cluster;
    window.sessionStorage.setItem(CLUSTER_KEY, cluster.metadata.name);
    this.clusterChange.emit(cluster);
  }

  trackByName(_: number, cluster: Cluster) {
    return cluster.metadata.name;
  }

  onSearch(name: string) {
    this.search$.next(name);
  }

  searchName(ev: Event) {
    // prevent closing dialog
    ev.stopPropagation();
  }

  getDisplayNameLabel(name: string) {
    return `${this.translate.get('cluster')}:${name}`;
  }
}
