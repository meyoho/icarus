import { PageModule, PlatformNavModule } from '@alauda/ui';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { ClusterBadgeComponent } from './cluster-badge/component';
import { AdminLayoutComponent } from './layout.component';

/**
 * For fundamental page Layout.
 */
@NgModule({
  providers: [],
  imports: [
    RouterModule,
    ScrollingModule,
    SharedModule,
    PageModule,
    SharedLayoutModule,
    PlatformNavModule,
  ],
  exports: [AdminLayoutComponent],
  declarations: [AdminLayoutComponent, ClusterBadgeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LayoutModule {}
