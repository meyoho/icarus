import { FeatureGuard } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import { HomeComponent } from './home.component';

/**
 * Define base level App base routes.
 */
export const appBaseRoutes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full',
      },
      {
        path: 'load_balancer',
        canActivate: [FeatureGuard],
        data: { featureName: 'alb2' },
        loadChildren: () =>
          import('app/features-shared/load_balancer/module').then(
            M => M.LoadBalancerModule,
          ),
      },
      {
        path: 'trace',
        loadChildren: () =>
          import('./features/network/trace/module').then(M => M.TraceModule),
      },
      {
        path: 'subnet',
        loadChildren: () =>
          import('./features/network/subnet/module').then(M => M.SubnetModule),
      },
      {
        path: 'pv',
        loadChildren: () =>
          import('./features/storage/pv/module').then(M => M.PvModule),
      },
      {
        path: 'storageclass',
        loadChildren: () =>
          import('./features/storage/storageclass/module').then(
            M => M.StorageClassModule,
          ),
      },
      {
        path: 'pod_security_policy',
        canActivate: [FeatureGuard],
        data: { featureName: 'psp' },
        loadChildren: () =>
          import('./features/pod-security-policy/module').then(
            M => M.PodSecurityPolicyModule,
          ),
      },
      {
        path: 'domain',
        loadChildren: () =>
          import('./features/network/domain/module').then(M => M.DomainModule),
      },
      {
        path: 'chartrepo',
        canActivate: [FeatureGuard],
        data: { featureName: 'app-market' },
        loadChildren: () =>
          import('./features/chartrepo/module').then(M => M.ChartRepoModule),
      },
      {
        path: '**',
        component: HomeComponent,
      },
    ],
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [RouterModule.forChild(appBaseRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
