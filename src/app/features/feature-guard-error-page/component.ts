import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rc-feature-guard-error',
  styleUrls: ['../../shared/layout/layout.common.scss'],
  templateUrl: 'template.html',
})
export class FeatureGuardErrorComponent {
  featureName: string;
  constructor(private readonly route: ActivatedRoute) {
    this.featureName = this.route.snapshot.paramMap.get('name');
  }
}
