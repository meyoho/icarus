import { PageModule } from '@alauda/ui';
import { NgModule } from '@angular/core';

import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { FeatureGuardErrorComponent } from './component';

@NgModule({
  imports: [PageModule, SharedModule, SharedLayoutModule],
  declarations: [FeatureGuardErrorComponent],
})
export class FeatureGuardErrorPageModule {}
