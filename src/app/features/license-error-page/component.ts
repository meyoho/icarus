import { Component } from '@angular/core';

@Component({
  template: `
    <acl-license-error>
      <rc-page-header>
        <div fxFlex></div>
        <acl-platform-center-nav></acl-platform-center-nav>
      </rc-page-header>
    </acl-license-error>
  `,
})
export class LicenseErrorPageComponent {}
