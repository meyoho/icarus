import { LicenseErrorModule } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';

import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { LicenseErrorPageComponent } from './component';

@NgModule({
  imports: [SharedModule, SharedLayoutModule, LicenseErrorModule],
  declarations: [LicenseErrorPageComponent],
})
export class LicenseErrorPageModule {}
