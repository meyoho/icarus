let dayjs = require('dayjs');

dayjs = dayjs.default || dayjs;

dayjs.extend((_, Dayjs) => {
  const proto = Dayjs.prototype;
  if (!proto.tz) {
    dayjs.tz = proto.tz = function (value, timeZone) {
      return dayjs(
        (value == null
          ? new Date()
          : new Date(value)
        ).toLocaleString(this.locale(), { timeZone }),
      );
    };
  }

  if (!proto.hours) {
    dayjs.hours = proto.hours = proto.hour;
  }
});

module.exports = dayjs;
