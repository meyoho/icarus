import {
  AuthorizationGuardService,
  LICENSE_PRODUCT_NAME_TOKEN,
  LicenseGuardService,
  LicenseProductName,
} from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DelayPreloadingStrategy } from './delay-preloading-strategy';
import { FeatureGuardErrorComponent } from './features/feature-guard-error-page/component';
import { FeatureGuardErrorPageModule } from './features/feature-guard-error-page/module';
import { LicenseErrorPageComponent } from './features/license-error-page/component';
import { LicenseErrorPageModule } from './features/license-error-page/module';
import { ProjectNameGuard } from './project-name.guard';

/**
 * Define base level App base routes.
 */
export const appBaseRoutes: Routes = [
  {
    path: 'workspace',
    loadChildren: () =>
      import('app_user/workspace/module').then(M => M.WorkspaceModule),
  },
  {
    path: 'home',
    loadChildren: () => import('app_user/home/module').then(M => M.HomeModule),
    canActivate: [LicenseGuardService, ProjectNameGuard],
  },
  {
    path: 'admin',
    loadChildren: () => import('../app_admin/module').then(M => M.AdminModule),
  },
  {
    path: 'terminal',
    loadChildren: () =>
      import('../terminal/terminal.module').then(M => M.TerminalModule),
  },
  {
    path: 'license-error',
    component: LicenseErrorPageComponent,
  },
  {
    path: 'feature-guard-error/:name',
    component: FeatureGuardErrorComponent,
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

appBaseRoutes.forEach(route => {
  if (route.redirectTo) {
    return;
  }
  if (!route.canActivate) {
    route.canActivate = [];
  }
  if (!route.canActivateChild) {
    route.canActivateChild = [];
  }
  route.canActivate.unshift(AuthorizationGuardService);
  if (route.path === 'license-error') {
    return;
  }
  if (!route.canActivate.includes(LicenseGuardService)) {
    route.canActivate.push(LicenseGuardService);
  }
  route.canActivateChild.push(LicenseGuardService);
});

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [
    RouterModule.forRoot(appBaseRoutes, {
      preloadingStrategy: DelayPreloadingStrategy,
    }),
    LicenseErrorPageModule,
    FeatureGuardErrorPageModule,
  ],
  exports: [RouterModule],
  providers: [
    DelayPreloadingStrategy,
    {
      provide: LICENSE_PRODUCT_NAME_TOKEN,
      useValue: LicenseProductName.CONTAINER_PLATFORM,
    },
  ],
})
export class AppRoutingModule {}
