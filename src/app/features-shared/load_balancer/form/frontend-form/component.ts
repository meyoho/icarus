import { K8sUtilService, NAME } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Frontend, Secret, Service } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-front-end-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerFrontEndFormComponent
  extends BaseResourceFormGroupComponent<Frontend>
  implements OnInit {
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  inWorkspace: boolean;

  @Input()
  namespaceList: string[];

  @Input()
  serviceList: Service[];

  @Input()
  secretList: Secret[];

  @Input()
  loadBalancerName: string;

  @Output()
  protocolChange = new EventEmitter<string>();

  PROTOCOLS = ['HTTPS', 'HTTP', 'TCP'];
  sessionAffinityTypes = ['', 'sip-hash', 'cookie'];

  constructor(
    public injector: Injector,
    private readonly k8sUtil: K8sUtilService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.get('spec.protocol').valueChanges.subscribe(value => {
      this.protocolChange.next(value);
      if (value === 'https') {
        this.form.get('spec.certificate_name').enable({ emitEvent: false });
      } else {
        this.form.get('spec.certificate_name').disable({ emitEvent: false });
      }
    });
  }

  getDefaultFormModel(): Frontend {
    return {
      apiVersion: 'crd.alauda.io/v1',
      kind: 'Frontend',
      metadata: {
        name: '',
        namespace: '',
        labels: {
          [this.k8sUtil.normalizeType(NAME, 'alb2')]: '',
        },
      },
      spec: {
        port: '',
        protocol: '',
        certificate_name: '',
        serviceGroup: {
          session_affinity_attribute: '',
          session_affinity_policy: '',
          services: [],
        },
      },
    };
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: [''],
      namespace: [''],
      labels: [{}],
    });
    const specForm = this.fb.group({
      port: ['', Validators.required],
      protocol: ['', Validators.required],
      serviceGroup: [''],
      certificate_name: ['', Validators.required],
    });
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: metadataForm,
      spec: specForm,
    });
  }

  adaptFormModel(formModel: Frontend): Frontend {
    if (formModel.spec.port) {
      // port格式为5位，前置补0，10 => 00010
      const portPadded = (formModel.spec.port + '').padStart(5, '0');
      formModel.metadata.name = `${this.loadBalancerName}-${portPadded}`;
    }
    return formModel;
  }
}
