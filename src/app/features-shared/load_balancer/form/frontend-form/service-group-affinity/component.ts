import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Service, ServiceGroup } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-service-group-with-affinity',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerServiceGroupWithAffinityComponent
  extends BaseResourceFormGroupComponent
  implements OnChanges {
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  inWorkspace: boolean;

  @Input()
  namespaceList: string[];

  @Input()
  serviceList: Service[];

  @Input()
  protocol: string;

  sessionAffinityTypes = ['', 'sip-hash', 'cookie'];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnChanges({ protocol }: SimpleChanges) {
    if (protocol && protocol.currentValue) {
      this.sessionAffinityTypes =
        protocol.currentValue.toUpperCase() === 'TCP'
          ? ['', 'sip-hash']
          : ['', 'sip-hash', 'cookie'];
      if (this.form) {
        this.form.get('session_affinity_policy').setValue('');
        this.form.get('session_affinity_attribute').setValue('');
      }
    }
  }

  getDefaultFormModel(): ServiceGroup {
    return {
      session_affinity_policy: '',
      session_affinity_attribute: '',
      services: [],
    };
  }

  createForm() {
    return this.fb.group({
      session_affinity_attribute: [''],
      session_affinity_policy: [''],
      services: [''],
    });
  }
}
