import {
  K8sApiService,
  NAME,
  NAMESPACE,
  TOKEN_GLOBAL_NAMESPACE,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest, forkJoin, of } from 'rxjs';
import {
  finalize,
  map,
  pluck,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  Domain,
  Frontend,
  LoadBalancer,
  RESOURCE_TYPES,
  ResourceType,
  Secret,
  SecretType,
  Service,
  WorkspaceDetailParams,
  getYamlApiVersion,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AlbResourceType } from '../../util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerAddFrontendComponent implements OnInit, OnDestroy {
  confirming: boolean;

  loadBalancer: LoadBalancer;
  frontend: Frontend;

  namespaceList: string[] = [];
  serviceList: Service[] = [];
  secretList: Secret[] = [];
  domainList: Domain[] = [];

  destroy$$ = new Subject<void>();

  baseParams: WorkspaceDetailParams;

  baseParams$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspace ? this.workspace.baseParams : of(null),
  ]).pipe(
    takeUntil(this.destroy$$),
    map(([params, baseParams]) => {
      const cluster = params.get('cluster') || baseParams.cluster;
      const namespace = params.get(NAMESPACE) || baseParams.namespace;
      const name = params.get(NAME);

      return (this.baseParams = {
        cluster,
        namespace,
        name,
      });
    }),
  );

  loadBalancer$ = this.baseParams$.pipe(
    switchMap(params =>
      this.k8sApi.getResource<LoadBalancer>({
        ...params,
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        namespace: this.globalNamespace,
      }),
    ),
    tap(loadBalancer => (this.loadBalancer = loadBalancer)),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    @Optional() public workspace: WorkspaceComponent,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  genFrontend(): Frontend {
    return {
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.FRONTEND),
      kind: 'Frontend',
      metadata: {
        name: '',
        namespace: this.k8sUtil.getNamespace(this.loadBalancer),
        ownerReferences: [
          {
            apiVersion: getYamlApiVersion(RESOURCE_TYPES.ALAUDA_LOADBALANCER2),
            kind: AlbResourceType.ALB,
            name: this.loadBalancer.metadata.name,
            uid: this.loadBalancer.metadata.uid,
          },
        ],
        labels: {
          [this.k8sUtil.normalizeType(NAME, 'alb2')]: this.k8sUtil.getName(
            this.loadBalancer,
          ),
        },
      },
      spec: {
        port: '',
        protocol: '',
        serviceGroup: {
          session_affinity_attribute: '',
          session_affinity_policy: '',
          services: [],
        },
      },
    };
  }

  ngOnInit() {
    // loadBalancer$ is required for `genFrontend`
    combineLatest([this.baseParams$, this.loadBalancer$])
      .pipe(
        switchMap(([{ cluster, namespace }]) =>
          forkJoin([
            this.k8sApi
              .getResourceList<Service>({
                type: RESOURCE_TYPES.SERVICE,
                cluster,
                namespace,
              })
              .pipe(pluck('items')),
            this.k8sApi
              .getResourceList<Secret>({
                type: RESOURCE_TYPES.SECRET,
                cluster,
                namespace,
                queryParams: {
                  fieldSelector: matchLabelsToString({
                    type: SecretType.TLS,
                  }),
                },
              })
              .pipe(pluck('items')),
          ]),
        ),
      )
      .subscribe(
        ([services, secrets]) => {
          this.frontend = this.genFrontend();
          services.forEach(({ metadata: { namespace } }) => {
            if (namespace && !this.namespaceList.includes(namespace)) {
              this.namespaceList.push(namespace);
            }
          });
          this.serviceList = services;
          this.secretList = secrets;
        },
        () => {
          this.router.navigate(['..'], {
            relativeTo: this.activatedRoute,
          });
        },
      );
  }

  ngOnDestroy() {
    this.destroy$$.next();
  }

  onSubmit() {
    this.confirming = true;

    this.k8sApi
      .postResource({
        type: RESOURCE_TYPES.FRONTEND,
        cluster: this.baseParams.cluster,
        resource: this.frontend,
      })
      .pipe(
        takeUntil(this.destroy$$),
        finalize(() => (this.confirming = false)),
      )
      .subscribe(frontend => {
        this.router.navigate(['../port', frontend.metadata.name], {
          relativeTo: this.activatedRoute,
        });
      });
  }

  cancel() {
    this.location.back();
  }
}
