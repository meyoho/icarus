import {
  K8sApiService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { finalize, pluck } from 'rxjs/operators';

import {
  Frontend,
  RESOURCE_TYPES,
  ResourceType,
  Secret,
  SecretType,
} from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerUpdateDefaultCertificateComponent {
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  confirming: boolean;

  frontend: Frontend;

  secretList$ = this.k8sApi
    .getResourceList<Secret>({
      type: RESOURCE_TYPES.SECRET,
      cluster: this.modalData.cluster,
      namespace: this.modalData.namespace,
      queryParams: {
        fieldSelector: matchLabelsToString({
          type: SecretType.TLS,
        }),
      },
    })
    .pipe(pluck('items'), publishRef());

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    @Inject(DIALOG_DATA)
    public modalData: {
      cluster: string;
      namespace: string;
      frontend: Frontend;
    },
  ) {
    this.frontend = cloneDeep(this.modalData.frontend);
  }

  onSubmit() {
    this.confirming = true;
    this.cdr.markForCheck();

    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.FRONTEND,
        cluster: this.modalData.cluster,
        resource: this.frontend,
        part: {
          spec: {
            certificate_name: this.frontend.spec.certificate_name,
          },
        },
      })
      .pipe(
        finalize(() => {
          this.confirming = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => this.confirmed.next(true));
  }
}
