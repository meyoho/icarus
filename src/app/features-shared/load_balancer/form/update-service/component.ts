import { K8sApiService, noop, publishRef } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { map, pluck } from 'rxjs/operators';

import {
  Frontend,
  RESOURCE_TYPES,
  ResourceType,
  Service,
  ServiceGroup,
} from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerPortUpdateServiceComponent {
  @Output()
  confirmed = new EventEmitter<Frontend>();

  frontend: Frontend;

  serviceList$ = this.k8sApi
    .getResourceList<Service>({
      type: RESOURCE_TYPES.SERVICE,
      cluster: this.modalData.cluster,
      namespace: this.modalData.namespace,
      queryParams: {
        limit: '0',
      },
    })
    .pipe(pluck('items'), publishRef());

  namespaceList$ = this.serviceList$.pipe(
    map(services => {
      const namespaces = new Set<string>();
      services.forEach(({ metadata: { namespace } }) =>
        namespaces.add(namespace),
      );
      return [...namespaces];
    }),
    publishRef(),
  );

  confirming: boolean;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    @Inject(DIALOG_DATA)
    public modalData: {
      cluster: string;
      namespace: string;
      frontend: Frontend;
      inWorkspace: boolean;
    },
  ) {
    const { spec } = (this.frontend = cloneDeep(this.modalData.frontend));

    if (!spec.serviceGroup) {
      spec.serviceGroup = this.getDefaultServiceGroup();
    }
  }

  getDefaultServiceGroup(): ServiceGroup {
    return {
      session_affinity_attribute: '',
      session_affinity_policy: '',
      services: [
        {
          name: '',
          namespace: this.modalData.inWorkspace ? this.modalData.namespace : '',
          port: '',
          weight: 100,
        },
      ],
    };
  }

  onSubmit() {
    this.confirming = true;
    this.cdr.markForCheck();
    this.k8sApi
      .patchResource<Frontend>({
        type: RESOURCE_TYPES.FRONTEND,
        cluster: this.modalData.cluster,
        resource: this.frontend,
        part: {
          spec: {
            serviceGroup: this.frontend.spec.serviceGroup,
          },
        },
      })
      .subscribe(
        frontend => this.confirmed.next(frontend),
        noop,
        () => {
          this.confirming = false;
          this.cdr.markForCheck();
        },
      );
  }
}
