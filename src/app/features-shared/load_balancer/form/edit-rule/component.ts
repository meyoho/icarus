import {
  CLUSTER,
  K8sApiService,
  K8sResourceAction,
  NAME,
  NAMESPACE,
  PROJECT,
  TOKEN_GLOBAL_NAMESPACE,
  matchExpressionsToString,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { clone, uniqBy } from 'lodash-es';
import { Observable, Subject, combineLatest, forkJoin, of } from 'rxjs';
import {
  finalize,
  map,
  pluck,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { v4 } from 'uuid';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  Domain,
  Frontend,
  LoadBalancer,
  RESOURCE_TYPES,
  ResourceType,
  Rule,
  Secret,
  SecretType,
  Service,
  getYamlApiVersion,
} from 'app/typings';
import { ASSIGN_ALL, delay } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { LoadBalancerPortParams } from '../../detail/port/component';
import { AlbResourceType, LbType } from '../../util';

interface RuleModel {
  value: Rule;
  action?:
    | K8sResourceAction.CREATE
    | K8sResourceAction.UPDATE
    | K8sResourceAction.DELETE;
}

const ACTION_MAPPER = {
  [K8sResourceAction.CREATE]: 'postResource',
  [K8sResourceAction.UPDATE]: 'putResource',
  [K8sResourceAction.DELETE]: 'deleteResource',
} as const;

export interface LoadBalancerEditRuleParams extends LoadBalancerPortParams {
  rule?: string;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerEditRuleComponent implements OnInit, OnDestroy {
  loadBalancer: LoadBalancer;
  frontend: Frontend;
  serviceList: Service[] = [];
  secretList: Service[] = [];
  domainList: Domain[] = [];
  ruleList: RuleModel[] = [];
  namespaceList: string[] = [];

  params: LoadBalancerEditRuleParams;

  params$: Observable<LoadBalancerEditRuleParams> = combineLatest([
    this.activatedRoute.paramMap,
    this.workspace ? this.workspace.baseParams : of(null),
  ]).pipe(
    map(([params, baseParams]) => ({
      project: baseParams.project,
      cluster: params.get('cluster') || baseParams.cluster,
      namespace: params.get(NAMESPACE) || baseParams.namespace,
      name: params.get(NAME),
      port: params.get('port'),
      rule: params.get('rule'),
    })),
    tap(params => (this.params = params)),
    publishRef(),
  );

  loadBalancer$ = this.params$.pipe(
    switchMap(params =>
      this.k8sApi.getResource<LoadBalancer>({
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        cluster: params.cluster,
        namespace: this.globalNamespace,
        name: params.name,
      }),
    ),
    tap(loadBalancer => (this.loadBalancer = loadBalancer)),
  );

  confirming$$ = new Subject<boolean>();
  destroy$$ = new Subject<void>();

  updatingRule: Rule;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    @Optional() public workspace: WorkspaceComponent,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  ngOnInit() {
    combineLatest([this.params$, this.loadBalancer$])
      .pipe(
        takeUntil(this.destroy$$),
        map(([params, loadBalancer]) => ({
          ...params,
          lbNamespace: this.k8sUtil.getNamespace(loadBalancer),
        })),
        switchMap(({ project, cluster, lbNamespace, namespace, port, rule }) =>
          forkJoin([
            this.k8sApi.getResource<Frontend>({
              type: RESOURCE_TYPES.FRONTEND,
              cluster,
              namespace: lbNamespace,
              name: port,
            }),
            rule
              ? this.k8sApi.getResource<Rule>({
                  type: RESOURCE_TYPES.RULE,
                  cluster,
                  namespace: lbNamespace,
                  name: rule,
                })
              : of(null),
            this.k8sApi
              .getResourceList<Service>({
                type: RESOURCE_TYPES.SERVICE,
                cluster,
                namespace,
              })
              .pipe(pluck('items')),
            this.k8sApi
              .getResourceList<Secret>({
                type: RESOURCE_TYPES.SECRET,
                cluster,
                namespace,
                queryParams: {
                  fieldSelector: matchLabelsToString({
                    type: SecretType.TLS,
                  }),
                },
              })
              .pipe(pluck('items')),
            this.k8sApi
              .getGlobalResourceList<Domain>({
                type: RESOURCE_TYPES.DOMAIN,
                queryParams: {
                  labelSelector: matchExpressionsToString([
                    {
                      key: this.k8sUtil.normalizeType(NAME, CLUSTER),
                      operator: 'in',
                      values: [ASSIGN_ALL, cluster],
                    },
                    {
                      key: this.k8sUtil.normalizeType(NAME, PROJECT),
                      operator: 'in',
                      values: [ASSIGN_ALL, project],
                    },
                  ]),
                },
              })
              .pipe(pluck('items')),
          ]),
        ),
      )
      .subscribe(
        ([frontend, rule, services, secrets, domains]) => {
          this.frontend = frontend;
          this.ruleList.length = 0;
          this.addRule(rule);
          this.updatingRule = rule;
          this.serviceList = services;
          this.secretList = secrets;
          this.domainList = uniqBy(domains, 'spec.name');
          const namespaces = new Set<string>();
          services.forEach(({ metadata: { namespace } }) =>
            namespaces.add(namespace),
          );
          this.namespaceList = [...namespaces];
          this.cdr.markForCheck();
        },
        () => {
          this.router.navigate(this.params.rule ? ['../../..'] : ['..'], {
            relativeTo: this.activatedRoute,
          });
        },
      );
  }

  ngOnDestroy() {
    this.destroy$$.next();
  }

  canAddRule() {
    const { protocol } = this.frontend.spec;
    return (
      protocol &&
      protocol !== 'tcp' &&
      this.loadBalancer.spec.type !== LbType.ELB
    );
  }

  genDefaultRule(): Rule {
    const lbName = this.loadBalancer.metadata.name;
    const frontendName = this.frontend.metadata.name;
    return {
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.RULE),
      kind: 'Rule',
      metadata: {
        name: [frontendName, v4()].join('-'),
        namespace: this.loadBalancer.metadata.namespace,
        labels: {
          [this.k8sUtil.normalizeType(NAME, 'alb2')]: lbName,
          [this.k8sUtil.normalizeType('frontend', 'alb2')]: frontendName,
        },
      },
      spec: {
        domain: '',
        url: '',
        dsl: '',
        description: '',
        serviceGroup: {
          session_affinity_policy: '',
          session_affinity_attribute: '',
          services: [
            {
              name: '',
              namespace: this.workspace ? this.params.namespace : '',
              port: '',
              weight: 100,
            },
          ],
        },
        backendProtocol: 'HTTP',
      },
    };
  }

  addRule(rule?: Rule) {
    if (
      this.frontend.spec.protocol === 'tcp' ||
      this.loadBalancer.spec.type === LbType.ELB
    ) {
      return;
    }
    this.ruleList.push({ value: rule || this.genDefaultRule() });
  }

  moveRule(from: number, to: number) {
    const { ruleList } = this;
    const temp = ruleList[to];
    ruleList[to] = ruleList[from];
    ruleList[from] = temp;
  }

  async onSubmit(ngForm: NgForm) {
    await delay();

    if (ngForm.invalid) {
      if (process.env.NODE_ENV === 'development') {
        console.log(ngForm);
      }
      return;
    }

    let ruleList = clone(this.ruleList);

    this.confirming$$.next(true);
    const { name, uid } = this.frontend.metadata;

    if (this.updatingRule) {
      // tslint:disable-next-line: no-shadowed-variable
      const { name } = this.updatingRule.metadata;
      const ruleModel = ruleList.find(
        ({ value }) => name === value.metadata.name,
      );
      if (ruleModel) {
        ruleModel.action = K8sResourceAction.UPDATE;
      } else {
        ruleList = ruleList.concat({
          value: this.updatingRule,
          action: K8sResourceAction.DELETE,
        });
      }
    }

    forkJoin(
      ruleList.map(({ value: rule, action = K8sResourceAction.CREATE }) => {
        if (uid) {
          rule.metadata.ownerReferences = [
            {
              apiVersion: 'crd.alauda.io/v1',
              kind: AlbResourceType.FRONTEND,
              name,
              uid,
            },
          ];
        }
        rule.spec.serviceGroup.services = rule.spec.serviceGroup.services.filter(
          service => service.namespace,
        );
        // reference http://jira.alaudatech.com/browse/ACP-171?focusedCommentId=88251&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-88251
        delete rule.spec.source;
        // @ts-ignore
        return this.k8sApi[ACTION_MAPPER[action]]({
          type: RESOURCE_TYPES.RULE,
          cluster: this.params.cluster,
          resource: rule,
        });
      }),
    )
      .pipe(
        takeUntil(this.destroy$$),
        finalize(() => this.confirming$$.next(false)),
      )
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(() => {
        this.router.navigate(this.params.rule ? ['../../..'] : ['..'], {
          relativeTo: this.activatedRoute,
        });
      });
  }

  cancel() {
    this.location.back();
  }
}
