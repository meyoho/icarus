import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

import { Secret } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-certificate-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerCertificateFormComponent
  extends BaseResourceFormComponent<string>
  implements OnInit {
  @Input()
  secretList: Secret[];

  @Input()
  namespace: string;

  @Input()
  required: boolean;

  namespaceSecretMap: Record<string, Secret[]> = {};

  constructor(public cdr: ChangeDetectorRef, injector: Injector) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return false;
  }

  ngOnInit() {
    super.ngOnInit();
    const originalNamespace = (this.form.value || '').split('_')[0];
    this.secretList.forEach(secret => {
      const { namespace } = secret.metadata;
      if (![this.namespace, originalNamespace].includes(namespace)) {
        return;
      }
      if (this.namespaceSecretMap[namespace]) {
        this.namespaceSecretMap[namespace].push(secret);
      } else {
        this.namespaceSecretMap[namespace] = [secret];
      }
    });
    this.cdr.markForCheck();
  }

  getDefaultFormModel() {
    return '';
  }

  createForm() {
    return this.fb.control('');
  }

  adaptResourceModel(resource: string) {
    return resource;
  }
}
