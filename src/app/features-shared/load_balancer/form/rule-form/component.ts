import {
  K8sUtilService,
  KubernetesResource,
  NAME,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormArray, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  Domain,
  LoadBalancer,
  Rule,
  RuleIndicator,
  RuleSpec,
  Secret,
  Service,
} from 'app/typings';

import {
  LbType,
  MatchType,
  RuleType,
  parseDSL,
  stringifyDSL,
} from '../../util';

interface RuleSpecForm extends RuleSpec {
  ruleIndicators: RuleIndicator[];
}

interface RuleForm extends KubernetesResource {
  spec: RuleSpecForm;
}

const HTTP_REDIRECT_STATUS_CODES = [301, 302, 307];

@Component({
  selector: 'rc-load-balancer-rule-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRuleFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  inWorkspace: boolean;

  @Input()
  namespaceList: string[];

  @Input()
  serviceList: Service[];

  @Input()
  secretList: Secret[];

  @Input()
  domainList: Domain[];

  @Input()
  loadBalancer: LoadBalancer;

  @Input()
  frontendName: string;

  @Input()
  showCertificate = false;

  HTTP_REDIRECT_STATUS_CODES = HTTP_REDIRECT_STATUS_CODES;

  sessionAffinityTypes = ['', 'sip-hash', 'cookie'];
  urlRewrite = false;
  certificateEnabled = false;
  redirect = false;

  // ordered by adding priority
  ruleTypes = [
    RuleType.HOST,
    RuleType.URL,
    RuleType.SRC_IP,
    RuleType.HEADER,
    RuleType.COOKIE,
    RuleType.PARAM,
  ];

  constructor(
    injector: Injector,
    private readonly k8sUtil: K8sUtilService,
    private readonly translateService: TranslateService,
  ) {
    super(injector);
  }

  getDefaultFormModel(): RuleForm {
    return {
      apiVersion: 'crd.alauda.io/v1',
      kind: 'Rule',
      metadata: {
        name: '',
        namespace: this.namespace,
        labels: {
          [this.k8sUtil.normalizeType(NAME, 'alb2')]: '',
          [this.k8sUtil.normalizeType('frontend', 'alb2')]: '',
        },
      },
      spec: {
        domain: '',
        url: '',
        ruleIndicators: [{ values: [] } as RuleIndicator],
        dsl: '',
        description: '',
        certificate_name: '',
        serviceGroup: {
          session_affinity_policy: '',
          session_affinity_attribute: '',
          services: [
            {
              name: '',
              namespace: this.namespace,
              port: '',
              weight: 100,
            },
          ],
        },
        backendProtocol: 'HTTP',
      },
    };
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: [''],
      namespace: [this.namespace || ''],
      labels: [''],
    });
    const specForm = this.fb.group({
      domain: [''],
      url: [''],
      dsl: [''],
      ruleIndicators: this.fb.array([]),
      description: [''],
      certificate_name: [''],
      serviceGroup: this.fb.group({
        session_affinity_attribute: [''],
        session_affinity_policy: [''],
        services: [[]],
      }),
      rewrite_target: ['', Validators.required],
      backendProtocol: ['', Validators.required],
      redirectCode: [''],
      redirectURL: ['', Validators.required],
      enableCORS: [''],
    });
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getRuleTypeDisplay(type: string) {
    return this.translateService.get('rule_' + type.toLocaleLowerCase());
  }

  get ruleIndicatorsForm() {
    return this.form.get('spec.ruleIndicators') as FormArray;
  }

  removeRuleIndicator(index: number) {
    this.ruleIndicatorsForm.removeAt(index);
  }

  addRuleIndicator() {
    let addType = '';
    // all type added,just return
    if (this.ruleIndicatorsForm.controls.length === this.ruleTypes.length) {
      return;
    }
    // find first item in ruleTypes,which not exist in ruleIndicatorsForm.controls
    if (this.ruleIndicatorsForm.controls.length > 0) {
      addType = this.ruleTypes.find(
        type =>
          !this.ruleIndicatorsForm.controls.find(
            item => item.value.type === type,
          ),
      );
    } else {
      addType = this.ruleTypes[0];
    }
    this.ruleIndicatorsForm.push(
      this.fb.control(this.genDefaultFromType(addType), control => {
        const index = this.ruleIndicatorsForm.controls.indexOf(control);
        const previousTypes = this.ruleIndicatorsForm.controls
          .slice(0, index)
          .map(control => control.value.type)
          .filter(_ => !!_);
        const { type } = control.value;
        if (previousTypes.includes(type)) {
          return {
            duplicateType: type,
          };
        } else {
          return null;
        }
      }),
    );
  }

  genDefaultFromType(type: string): RuleIndicator {
    switch (type) {
      case RuleType.HOST: {
        return {
          type: RuleType.HOST,
          key: '',
          values: [[null, '']],
        };
      }
      case RuleType.URL: {
        return {
          type: RuleType.URL,
          key: '',
          values: [[MatchType.REGEX, '']],
        };
      }
      case RuleType.SRC_IP: {
        return {
          type: RuleType.SRC_IP,
          key: '',
          values: [[MatchType.EQ, '']],
        };
      }
      case RuleType.HEADER: {
        return {
          type: RuleType.HEADER,
          key: '',
          values: [[MatchType.EQ, '']],
        };
      }
      case RuleType.COOKIE: {
        return {
          type: RuleType.COOKIE,
          key: '',
          values: [[MatchType.EQ, '']],
        };
      }
      case RuleType.PARAM: {
        return {
          type: RuleType.PARAM,
          key: '',
          values: [[MatchType.EQ, '']],
        };
      }
      default:
        return null;
    }
  }

  urlRewriteChange(value: boolean) {
    this.form.get('spec.rewrite_target')[value ? 'enable' : 'disable']();
  }

  redirectChange(value: boolean) {
    const redirectCodeCtrl = this.form.get('spec.redirectCode');
    redirectCodeCtrl[value ? 'enable' : 'disable']();
    if (value && !redirectCodeCtrl.value) {
      redirectCodeCtrl.setValue(HTTP_REDIRECT_STATUS_CODES[0]);
    }
    this.form.get('spec.redirectURL')[value ? 'enable' : 'disable']();
  }

  certificateEnabledChange(value: boolean) {
    if (!value) {
      this.form.get('spec.certificate_name').setValue('');
    }
  }

  adaptResourceModel(resource: Rule): RuleForm {
    if (resource) {
      const spec: RuleSpecForm = {
        ...resource.spec,
        backendProtocol: resource.spec.backendProtocol || 'HTTP',
        ruleIndicators: [],
      };
      this.urlRewrite = !!resource.spec.rewrite_target;
      this.redirect = !!resource.spec.redirectURL;
      this.certificateEnabled = !!resource.spec.certificate_name;
      this.urlRewriteChange(this.urlRewrite);
      this.redirectChange(this.redirect);
      spec.ruleIndicators = parseDSL(resource);
      return { ...resource, spec };
    }
  }

  adaptFormModel(formModel: RuleForm): Rule {
    formModel.metadata.labels[
      this.k8sUtil.normalizeType('frontend', 'alb2')
    ] = this.frontendName;
    const { ruleIndicators, ...modelSpec } = formModel.spec;
    return {
      ...formModel,
      spec: this.parseDSL(ruleIndicators, {
        ...modelSpec,
        rewrite_target: this.urlRewrite ? modelSpec.rewrite_target : '',
        redirectCode: this.redirect ? modelSpec.redirectCode : undefined,
        redirectURL: this.redirect ? modelSpec.redirectURL : '',
      }),
    };
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private parseDSL(ruleIndicators: RuleIndicator[], spec: RuleSpec) {
    ruleIndicators.forEach(indicator => {
      const { type, values } = indicator;
      values.forEach(v => {
        const [matchType, value] = v;
        if (
          type === RuleType.URL &&
          matchType === MatchType.STARTS_WITH &&
          !value.startsWith('/')
        ) {
          v[1] = '/' + value;
        }
      });
    });

    let domain = '';
    let url = '';
    if (LbType[this.loadBalancer.spec.type as keyof typeof LbType]) {
      ruleIndicators.forEach(indicator => {
        const { type, values } = indicator;
        if (values[0]) {
          const [matchType, value] = values[0];
          switch (type) {
            case RuleType.HOST: {
              domain = value;
              break;
            }
            case RuleType.URL: {
              let prefix = '';
              if (matchType === MatchType.REGEX && !/^[*.]/.test(value)) {
                prefix = '^';
              }
              url = prefix + value;
              break;
            }
          }
        }
      });

      if (this.loadBalancer.spec.type === 'clb') {
        if (!domain) {
          throw new Error(this.translateService.get('domain_required_in_clb'));
        } else {
          // url可以为空，以/开头的url地址，满足一定条件的正则
          const domainValid =
            (/[\w-.]/.test(domain) || /^\*\.|\.\*$/.test(domain)) &&
            !/[ "';`{}]/.test(domain);
          const urlValid =
            (url &&
              (/^\/[\w-.]/.test(url) ||
                (!/[ "';`{}]/.test(url) && url.startsWith('^')))) ||
            !url;

          if (!domainValid || !urlValid) {
            throw new Error(
              this.translateService.get('domain_or_url_invalid_in_clb'),
            );
          }
        }
      }
    }
    spec.domain = domain;
    spec.url = url;
    spec.dsl = stringifyDSL(ruleIndicators);
    spec.dslx = ruleIndicators;
    return spec;
  }
}
