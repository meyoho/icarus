import {
  NAME,
  NAMESPACE,
  ObservableInput,
  ValueHook,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup,
  Validators,
} from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { RuleService, Service, ServicePort } from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

const MODEL_KEYS = [NAME, NAMESPACE, 'port', 'weight'] as const;

interface RuleServiceForm extends RuleService {
  nameList: string[];
  portList: number[];
}

@Component({
  selector: 'rc-load-balancer-service-group-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerServiceGroupFormComponent
  extends BaseResourceFormArrayComponent<RuleService, RuleServiceForm>
  implements OnInit, OnDestroy {
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  inWorkspace: boolean;

  @Input()
  namespaceList: string[];

  destroy$$ = new Subject<void>();

  @ObservableInput(true)
  serviceList$: Observable<Service[]>;

  @ValueHook(Boolean)
  @Input()
  serviceList: Service[] = [];

  servicePortMap: Record<string, ServicePort[]> = {};

  rowBackgroundColorFn = rowBackgroundColorFn;

  constructor(
    public injector: Injector,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.serviceList$
      .pipe(takeUntil(this.destroy$$))
      .subscribe((serviceList: Service[]) => {
        if (!serviceList) {
          return;
        }

        serviceList.forEach(item => {
          const { namespace, name } = item.metadata;
          this.servicePortMap[`${namespace}_${name}`] = item.spec.ports;
          return item;
        });

        this.form.controls.forEach((control: FormGroup) => {
          this.namespaceChange(control.get(NAMESPACE).value, control);
        });
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    const namespace = this.inWorkspace ? this.namespace : '';
    return this.fb.group({
      name: ['', Validators.required],
      namespace: [
        {
          value: namespace,
          disabled: this.inWorkspace,
        },
        Validators.required,
      ],
      nameList: [this.namespaceChange(namespace)],
      portList: [],
      port: ['', Validators.required],
      weight: [
        100,
        [Validators.required, Validators.max(100), Validators.min(0)],
      ],
    });
  }

  namespaceChange(namespace: string, control?: AbstractControl) {
    let services: string[] = [];
    if (namespace) {
      const serviceList = this.serviceList.filter(
        item => item.metadata.namespace === namespace,
      );
      services = serviceList.map(item => item.metadata.name);
    }
    if (control) {
      control.get('nameList').setValue(services);
    }
    return services;
  }

  serviceChange(
    service: string,
    control?: AbstractControl,
    namespace?: string,
  ) {
    let list: number[] = [];
    if (service) {
      const namespaceValue = control ? control.get(NAMESPACE).value : namespace;
      const portList = this.servicePortMap[`${namespaceValue}_${service}`];
      list = portList && portList.map(item => item.port);
    }
    if (control) {
      control.get('portList').setValue(list);
      const portCtrl = control.get('port');
      if (!list.includes(portCtrl.value)) {
        portCtrl.setValue(list[0]);
      }
    }
    return list;
  }

  getWeightsPercent(weight: number) {
    const total = this.form.controls.reduce(
      (acc, control) => acc + (control.get('weight').value || 0),
      0,
    );
    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  adaptResourceModel(resource: RuleService[]): RuleServiceForm[] {
    if (resource && resource.length > 0) {
      return resource.map((item, index) => {
        const nameList = this.namespaceChange(item.namespace);
        const portList = this.serviceChange(item.name, null, item.namespace);
        const namespace = item.namespace;

        if (namespace !== this.namespace) {
          setTimeout(() => {
            const group = this.form.controls[index] as FormGroup;
            Object.keys(group.controls).forEach(name =>
              group.get(name).disable(),
            );
          });
        }

        return {
          name: item.name,
          nameList,
          namespace,
          port: item.port,
          portList,
          weight: item.weight,
        };
      });
    }
  }

  adaptFormModel(_formModel: RuleServiceForm[]): RuleService[] {
    return this.form.controls.map(group =>
      MODEL_KEYS.reduce(
        (acc, key) =>
          Object.assign(acc, {
            [key]: group.get(key).value,
          }),
        {} as RuleService,
      ),
    );
  }
}
