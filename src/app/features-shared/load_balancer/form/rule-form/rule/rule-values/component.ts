import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup,
  Validators,
} from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MATCH_TYPES, MatchType } from 'app/features-shared/load_balancer/util';
import { Domain } from 'app/typings';
import { TYPE } from 'app/utils';

interface RuleIndicatorValue {
  type: string;
  value?: string;
  startValue?: string;
  endValue?: string;
  hostPrefix?: string;
  hostSuffix?: string;
}

const HOST_TYPES = ['hostPrefix', 'hostSuffix'] as const;
const NON_HOST_TYPES = ['value', 'startValue', 'endValue'] as const;

@Component({
  selector: 'rc-load-balancer-rule-values-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRuleValuesFormComponent
  extends BaseResourceFormArrayComponent<string[], RuleIndicatorValue>
  implements OnInit, OnDestroy {
  @Input()
  ruleIndicatorType: string;

  @Input()
  lbType: string;

  @Input()
  domains: Domain[] = [];

  @Input()
  matchTypesMap: Map<
    string,
    {
      key: boolean;
      matchTypes: MatchType[];
    }
  >;

  MATCH_TYPES = MATCH_TYPES;

  onDestroy$ = new Subject<void>();

  PATTERNS = {
    SRC_IP: /^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})$/,
    NUMBER: /^\d+$/,
  };

  constructor(
    public injector: Injector,
    @Optional() public controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  typeChange(type?: string, control?: AbstractControl) {
    if (!type || this.isHost) {
      const hostSuffix = control.get('hostSuffix').value;
      if (this.isHost && hostSuffix != null) {
        const isExtensiveDomain = hostSuffix.startsWith('*');
        // 全域名不能编辑前缀
        this.toggleCtrl(
          control,
          isExtensiveDomain ? [] : ['hostPrefix'],
          HOST_TYPES,
        );
        if (!isExtensiveDomain) {
          control.get('hostPrefix').patchValue('', {
            emitEvent: false,
          });
        }
      }
      return;
    }

    if (type === MatchType.RANGE) {
      this.toggleCtrl(control, ['value']);
    } else {
      this.toggleCtrl(control, ['startValue', 'endValue']);
    }
  }

  private toggleCtrl(
    group: AbstractControl,
    disabledNames: string[],
    types: readonly string[] = NON_HOST_TYPES,
  ) {
    types.forEach(name => {
      const ctrl = group.get(name);
      if (!ctrl) {
        return;
      }
      ctrl[disabledNames.includes(name) ? 'disable' : 'enable']({
        emitEvent: false,
      });
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.form.controls.forEach(control => {
        const typeValue = control.get(TYPE).value;
        this.typeChange(typeValue, control);
      });
    });

    // init form (更新时存在 controls，根据每个control里type的值设置input是否disable)
    // todo: 没有真正解决子表单状态更新后父表单更新滞后的问题
    this.form.controls.forEach(control => {
      const typeValue = control.get(TYPE).value;
      this.typeChange(typeValue, control);
    });

    if (this.isHost) {
      this.form.controls.forEach((group: FormGroup) => {
        Object.keys(group.controls).forEach(name => {
          const control = group.get(name);
          if (HOST_TYPES.includes(name as typeof HOST_TYPES[number])) {
            control.setValidators(Validators.required);
          } else {
            control.setValidators(null);
          }
        });
      });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  get isHost() {
    return this.ruleIndicatorType === 'HOST';
  }

  private createNewControl() {
    const control = this.isHost
      ? [
          {
            value: '',
            disabled: true,
          },
        ]
      : ['', Validators.required];
    return this.fb.group({
      type: control,
      value: control,
      startValue: control,
      endValue: control,
      hostPrefix: this.isHost ? control : null,
      hostSuffix: this.isHost ? [''] : null,
    });
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  adaptResourceModel(resource: string[][]): RuleIndicatorValue[] {
    const isPossibleHost = !this.ruleIndicatorType || this.isHost;

    if (resource && resource.length > 0) {
      return resource.map((item, index) => {
        if (item[0] === MatchType.RANGE) {
          return {
            type: item[0],
            startValue: item[1] || '',
            endValue: item[2] || '',
          };
        }
        const value = item[1] || '';

        if (isPossibleHost) {
          setTimeout(() => {
            // ensure it's host
            if (!this.isHost) {
              return;
            }
            const matchedDomain = this.domains.find(
              ({ spec: { kind, name } }) =>
                kind === 'full'
                  ? value === name
                  : value.endsWith(name.slice(1)),
            );
            const hostSuffix = matchedDomain ? matchedDomain.spec.name : value;
            const hostPrefix = hostSuffix.startsWith('*')
              ? value.slice(0, 1 - hostSuffix.length)
              : '';

            this.form.controls[index].patchValue({ hostPrefix, hostSuffix });
          });
        }

        return {
          type: item[0],
          value,
        };
      });
    }
  }

  adaptFormModel(formModel: RuleIndicatorValue[]): string[][] {
    return formModel.map((item, index) => {
      if (item.type === MatchType.RANGE) {
        return [item.type, item.startValue, item.endValue];
      }
      const hostPrefix = this.form.controls[index].get('hostPrefix').value;
      const hostSuffix = item.hostSuffix || '';
      return [
        this.isHost ? MatchType.IN : item.type,
        this.isHost
          ? hostPrefix + hostSuffix.slice(+hostSuffix.startsWith('*'))
          : item.value,
      ];
    });
  }

  trackByFn(_index: number, ctrl: AbstractControl) {
    return ctrl;
  }
}
