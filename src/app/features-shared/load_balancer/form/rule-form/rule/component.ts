import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { AbstractControl, ControlContainer, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Domain, RuleIndicator } from 'app/typings';
import { TYPE } from 'app/utils';

import { LbType, MatchType, RuleType } from '../../../util';

@Component({
  selector: 'rc-load-balancer-rule-part-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRulePartFormComponent
  extends BaseResourceFormGroupComponent<RuleIndicator>
  implements OnInit, OnDestroy {
  @Input()
  lbType: string;

  @Input()
  domains: Domain[];

  @Input()
  chosenControls: AbstractControl[];

  onDestroy$ = new Subject<void>();

  matchTypesMap = new Map<
    string,
    {
      key: boolean;
      matchTypes: MatchType[];
    }
  >();

  ruleTypes: Array<{ display: string; value: string }>;

  constructor(
    injector: Injector,
    private readonly translateService: TranslateService,
    @Optional() public controlContainer: ControlContainer,
  ) {
    super(injector);
    this.matchTypesMap
      .set(RuleType.HOST, {
        key: false,
        matchTypes: [MatchType.IN],
      })
      .set(RuleType.URL, {
        key: false,
        matchTypes:
          this.lbType === 'slb'
            ? [MatchType.STARTS_WITH]
            : [MatchType.REGEX, MatchType.STARTS_WITH],
      })
      .set(RuleType.SRC_IP, {
        key: false,
        matchTypes: [MatchType.EQ, MatchType.RANGE],
      })
      .set(RuleType.HEADER, {
        key: true,
        matchTypes: [MatchType.EQ, MatchType.RANGE, MatchType.REGEX],
      })
      .set(RuleType.COOKIE, {
        key: true,
        matchTypes: [MatchType.EQ],
      })
      .set(RuleType.PARAM, {
        key: true,
        matchTypes: [MatchType.EQ, MatchType.RANGE],
      });

    const ruleTypes = [];
    switch (this.lbType) {
      case LbType.SLB:
      case LbType.CLB:
        ruleTypes.push(RuleType.HOST, RuleType.URL);
        break;
      default:
        ruleTypes.push(
          RuleType.HOST,
          RuleType.URL,
          RuleType.SRC_IP,
          RuleType.HEADER,
          RuleType.COOKIE,
          RuleType.PARAM,
        );
        break;
    }

    this.ruleTypes = ruleTypes.map(value => ({
      display: this.translateService.get('rule_' + value.toLocaleLowerCase()),
      value,
    }));
  }

  isOccurInGroup(value: string) {
    // can select current label
    if (
      !Array.isArray(this.chosenControls) ||
      this.chosenControls.length === 0 ||
      value === this.form.get(TYPE).value
    ) {
      return false;
    }
    return !!this.chosenControls.find(e => e.value.type === value);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get(TYPE)
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(_ => this.onIndicatorTypeChange(_));
    const type = this.form.get(TYPE).value;
    if (!this.matchTypesMap.get(type).key) {
      this.form.get('key').disable({
        emitEvent: false,
      });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  getDefaultFormModel(): RuleIndicator {
    return {
      type: null,
      values: [],
      key: '',
    };
  }

  createForm() {
    return this.fb.group({
      type: ['', Validators.required],
      values: [[]],
      key: ['', Validators.required],
    });
  }

  showIndicatorKey() {
    const typeValue = this.form.get(TYPE).value;
    return typeValue && this.matchTypesMap.get(typeValue).key;
  }

  onIndicatorTypeChange(_type: string) {
    this.form.get('values').setValue([]);
    if (!this.showIndicatorKey()) {
      this.form.get('key').setValue('');
      this.form.get('key').disable({
        emitEvent: false,
      });
    } else {
      this.form.get('key').enable({
        emitEvent: false,
      });
    }
  }
}
