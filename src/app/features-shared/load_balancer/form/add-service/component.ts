import { K8sApiService, publishRef } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { finalize, map, pluck } from 'rxjs/operators';

import {
  RESOURCE_TYPES,
  Rule,
  RuleIndicator,
  RuleService,
  Service,
} from 'app/typings';

import { getPartialIndicators, parseDSL } from '../..//util';

interface ModalData {
  title: string;
  lbName: string;
  protocol: string;
  port: number;
  rule: Rule;
  ruleList: Rule[];
  type: string;
  cluster: string;
  namespace: string;
  inWorkspace: boolean;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerPortAddServiceComponent {
  @Output()
  confirmed = new EventEmitter<boolean>();

  ruleServices: RuleService[];
  ruleIndicators: RuleIndicator[];
  type: string;
  selectedRuleIndex = -1;
  rule: Rule;
  confirming: boolean;

  serviceList$ = this.k8sApi
    .getResourceList<Service>({
      type: RESOURCE_TYPES.SERVICE,
      cluster: this.modalData.cluster,
      namespace: this.modalData.namespace,
    })
    .pipe(pluck('items'), publishRef());

  namespaceList$ = this.serviceList$.pipe(
    map(serviceList => {
      const namespaces = new Set<string>();
      serviceList.forEach(({ metadata: { namespace } }) => {
        if (namespace) {
          namespaces.add(namespace);
        }
      });
      return [...namespaces];
    }),
    publishRef(),
  );

  getPartialIndicators = getPartialIndicators;
  parseDSL = parseDSL;

  constructor(
    @Inject(DIALOG_DATA) public modalData: ModalData,
    private readonly k8sApi: K8sApiService,
  ) {
    this.type = this.modalData.type || 'add';

    if (this.type === 'create') {
      return;
    }

    const { rule } = this.modalData;
    this.rule = rule;
    this.ruleServices = cloneDeep(rule.spec.serviceGroup.services);

    if (this.ruleServices.length === 0) {
      this.addService();
    }

    this.ruleIndicators = parseDSL(rule);
  }

  ruleSelectedChanged(va: boolean, index: number) {
    if (va) {
      this.selectedRuleIndex = index;
      this.ruleServices =
        cloneDeep(this.modalData.ruleList[index]).spec.serviceGroup.services ||
        [];
      this.rule = cloneDeep(this.modalData.ruleList[index]);
    } else if (this.selectedRuleIndex === index) {
      this.selectedRuleIndex = -1;
    }
  }

  showServiceGroup() {
    return !(this.type === 'create' && this.selectedRuleIndex < 0);
  }

  addService() {
    if (!this.showServiceGroup()) {
      return;
    }
    this.ruleServices.push({
      name: '',
      namespace: this.modalData.inWorkspace ? this.modalData.namespace : '',
      port: '',
      weight: 100,
    });
  }

  submitForm(formRef: NgForm) {
    formRef.onSubmit(null);

    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;
    this.rule.spec.serviceGroup.services = this.ruleServices;
    delete this.rule.spec.source;

    this.k8sApi
      .putResource({
        type: RESOURCE_TYPES.RULE,
        cluster: this.modalData.cluster,
        resource: this.rule,
      })
      .pipe(finalize(() => (this.confirming = false)))
      .subscribe(() => this.confirmed.next(true));
  }
}
