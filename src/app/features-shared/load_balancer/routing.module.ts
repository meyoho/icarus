import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoadBalancerDetailComponent } from './detail/component';
import { LoadBalancerPortComponent } from './detail/port/component';
import { LoadBalancerAddFrontendComponent } from './form/add-frontend/component';
import { LoadBalancerEditRuleComponent } from './form/edit-rule/component';
import { LoadBalancerListComponent } from './list/component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: LoadBalancerListComponent,
  },
  {
    path: 'detail/:name',
    component: LoadBalancerDetailComponent,
  },
  {
    path: 'detail/:name/add-frontend',
    component: LoadBalancerAddFrontendComponent,
  },
  {
    path: 'detail/:name/port/:port',
    component: LoadBalancerPortComponent,
  },
  {
    path: 'detail/:name/port/:port/add-rule',
    component: LoadBalancerEditRuleComponent,
  },
  {
    path: 'detail/:name/port/:port/rule/:rule/update',
    component: LoadBalancerEditRuleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoadbalancerRoutingModule {}
