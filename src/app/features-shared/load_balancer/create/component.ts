import {
  K8sApiService,
  StringMap,
  TOKEN_BASE_DOMAIN,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { find } from 'lodash-es';
import { Subject, interval, of } from 'rxjs';
import {
  concatMap,
  finalize,
  first,
  map,
  pluck,
  retry,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  AcpApiService,
  Label,
  NodeLabels,
} from 'app/services/api/acp-api.service';
import { ProjectService } from 'app/services/api/project.service';
import {
  HelmRequest,
  LoadBalancer,
  LoadBalancerFormModel,
  RESOURCE_TYPES,
  ResourceType,
  getYamlApiVersion,
} from 'app/typings';
import {
  ASSIGN_ALL,
  HOST_OR_IP_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerCreateComponent implements OnInit, OnDestroy {
  @Output()
  confirmed = new EventEmitter<LoadBalancer>();

  created: HelmRequest;

  ALL = ASSIGN_ALL;
  HOST_OR_IP_PATTERN = HOST_OR_IP_PATTERN;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  loading$ = new Subject<boolean>();
  confirming$ = new Subject<boolean>();
  destroy$$ = new Subject<void>();

  allProjects$ = this.projectApi
    .getProjects()
    .pipe(pluck('items'), publishRef());

  projects$ = this.allProjects$.pipe(
    map(projects =>
      projects.filter(project =>
        find(project.spec.clusters, ({ name }) => name === this.clusterName),
      ),
    ),
    publishRef(),
  );

  clusterName: string;
  project = ASSIGN_ALL;
  labels: Label[] = [];
  nodeSelector: Label[] = [];
  matchNodes: string[] = [];
  nodeLabels: NodeLabels;
  alb: LoadBalancerFormModel = {
    loadbalancerName: '',
    address: '',
    replicas: 1,
    nodeSelector: {},
  };

  type = 'standalone';
  minReplicas = 1;

  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly acpApi: AcpApiService,
    private readonly projectApi: ProjectService,
    @Inject(TOKEN_BASE_DOMAIN)
    private readonly baseDomain: string,
    @Inject(TOKEN_GLOBAL_NAMESPACE)
    private readonly globalNamespace: string,
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      cluster: string;
    },
  ) {
    this.clusterName = this.modalData.cluster;
  }

  ngOnInit() {
    this.loading$.next(true);
    let allLabels: Label[] = [];
    this.acpApi
      .getClusterNodesLabels(this.clusterName)
      .pipe(
        finalize(() => {
          this.loading$.next(false);
        }),
      )
      .subscribe(labels => {
        this.nodeLabels = labels;
        Object.values(this.nodeLabels).forEach(labels => {
          allLabels = allLabels.concat(labels);
        });
        this.labels = allLabels.filter(
          (label, index, self) =>
            index ===
            self.findIndex(t => t.key === label.key && t.value === label.value),
        );
      });
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  typeChange(type: string) {
    if (type === 'standalone') {
      this.alb.replicas = 1;
      this.minReplicas = 1;
      return;
    }
    this.alb.replicas = 3;
    this.minReplicas = 2;
  }

  nodeSelectorChange(nodeSelector: Label[]) {
    this.matchNodes = this.acpApi.checkNodeByLabels(
      this.nodeLabels,
      nodeSelector,
    );
  }

  disabledNodeLabel(label: Label) {
    return (
      this.nodeSelector.findIndex(
        t => t.key === label.key && t.value !== label.value,
      ) >= 0
    );
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid) {
      return;
    }

    const selector: StringMap = {};
    this.nodeSelector.forEach(item => {
      selector[item.key] = item.value;
    });
    this.alb.nodeSelector = selector;

    if (this.matchNodes.length < this.alb.replicas) {
      // 点击取消返回上一层，不是直接关创建的弹窗
      try {
        await this.dialog.confirm({
          title: this.translate.get('not_match_replicas_dialog_tip', {
            nodes: this.matchNodes.length,
            replicas: this.alb.replicas,
          }),
          confirmText: this.translate.get('continue_to_create'),
          cancelText: this.translate.get('cancel'),
        });
      } catch (e) {
        return;
      }
    }

    this.confirming$.next(true);

    (this.created
      ? of(this.created)
      : this.k8sApi
          .postGlobalResource<HelmRequest>({
            type: RESOURCE_TYPES.HELM_REQUEST,
            resource: {
              apiVersion: getYamlApiVersion(RESOURCE_TYPES.HELM_REQUEST),
              kind: 'HelmRequest',
              metadata: {
                finalizers: ['captain.' + this.baseDomain],
                name: [this.clusterName, this.alb.loadbalancerName].join('-'),
                namespace: this.globalNamespace,
              },
              spec: {
                clusterName: this.clusterName,
                chart: 'stable/alauda-alb2',
                namespace: this.globalNamespace,
                values: {
                  ...this.alb,
                  global: {
                    namespace: this.globalNamespace,
                  },
                  project: this.project,
                },
                valuesFrom: [
                  {
                    configMapKeyRef: {
                      name: 'sentry-cm',
                      key: 'global.config',
                      optional: true,
                    },
                  },
                ],
              },
            },
          })
          .pipe(
            tap(helmRequest => {
              this.created = helmRequest;
            }),
          )
    )
      .pipe(
        switchMap(() =>
          interval(5000).pipe(
            concatMap(() =>
              this.k8sApi.getResource<LoadBalancer>({
                type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
                cluster: this.clusterName,
                namespace: this.globalNamespace,
                name: this.alb.loadbalancerName,
              }),
            ),
            retry(3),
            first(),
          ),
        ),
        finalize(() => this.confirming$.next(false)),
        takeUntil(this.destroy$$),
      )
      .subscribe(loadBalancer => {
        this.confirmed.emit(loadBalancer);
        if (loadBalancer) {
          this.created = null;
        }
      });
  }
}
