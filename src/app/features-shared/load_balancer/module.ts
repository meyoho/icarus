import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { LoadBalancerCreateComponent } from './create/component';
import { LoadBalancerDetailComponent } from './detail/component';
import { LoadBalancerDetailInfoComponent } from './detail/info/component';
import { PortDetailComponent } from './detail/port-detail/component';
import { LoadBalancerPortInfoComponent } from './detail/port-info/component';
import { PortMonitorComponent } from './detail/port-monitor/component';
import { LoadBalancerPortComponent } from './detail/port/component';
import { LoadBalancerServiceWeightDisplayComponent } from './detail/service-weight-display/component';
import { LoadBalancerAddFrontendComponent } from './form/add-frontend/component';
import { LoadBalancerPortAddServiceComponent } from './form/add-service/component';
import { LoadBalancerCertificateFormComponent } from './form/certificate/component';
import { LoadBalancerEditRuleComponent } from './form/edit-rule/component';
import { LoadBalancerFrontEndFormComponent } from './form/frontend-form/component';
import { LoadBalancerServiceGroupWithAffinityComponent } from './form/frontend-form/service-group-affinity/component';
import { LoadBalancerRuleFormComponent } from './form/rule-form/component';
import { LoadBalancerRulePartFormComponent } from './form/rule-form/rule/component';
import { LoadBalancerRuleValuesFormComponent } from './form/rule-form/rule/rule-values/component';
import { LoadBalancerServiceGroupFormComponent } from './form/rule-form/service-group/component';
import { LoadBalancerUpdateDefaultCertificateComponent } from './form/update-default-certificate/component';
import { LoadBalancerPortUpdateServiceComponent } from './form/update-service/component';
import { LoadBalancerListComponent } from './list/component';
import { LoadbalancerRoutingModule } from './routing.module';
import { LoadBalancerUpdateProjectComponent } from './update-project/component';

@NgModule({
  imports: [SharedModule, LoadbalancerRoutingModule],
  declarations: [
    LoadBalancerListComponent,
    LoadBalancerDetailComponent,
    LoadBalancerPortComponent,
    LoadBalancerServiceGroupFormComponent,
    LoadBalancerRuleFormComponent,
    LoadBalancerRulePartFormComponent,
    LoadBalancerRuleValuesFormComponent,
    LoadBalancerFrontEndFormComponent,
    LoadBalancerServiceGroupWithAffinityComponent,
    LoadBalancerDetailInfoComponent,
    LoadBalancerPortInfoComponent,
    LoadBalancerAddFrontendComponent,
    LoadBalancerEditRuleComponent,
    LoadBalancerCertificateFormComponent,
    LoadBalancerServiceWeightDisplayComponent,
    LoadBalancerCreateComponent,
    LoadBalancerPortUpdateServiceComponent,
    LoadBalancerPortAddServiceComponent,
    LoadBalancerUpdateDefaultCertificateComponent,
    LoadBalancerUpdateProjectComponent,
    PortDetailComponent,
    PortMonitorComponent,
  ],
})
export class LoadBalancerModule {}
