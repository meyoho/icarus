import { takeWhile } from 'lodash-es';

import { Rule, RuleIndicator } from 'app/typings';

export enum LbType {
  nginx = 'nginx',
  SLB = 'slb',
  ELB = 'elb',
  CLB = 'clb',
}

export enum AlbResourceType {
  ALB = 'alaudaloadbalancer2',
  FRONTEND = 'frontends',
  RULE = 'rules',
}

export enum MatchType {
  AND = 'AND',
  EXIST = 'EXIST',
  EQ = 'EQ',
  IN = 'IN',
  OR = 'OR',
  RANGE = 'RANGE',
  REGEX = 'REGEX',
  STARTS_WITH = 'STARTS_WITH',
}

export const MATCH_TYPES = {
  EXIST: 'Exist',
  EQ: 'Equal',
  IN: 'In',
  RANGE: 'Range',
  REGEX: 'RegEx',
  STARTS_WITH: 'StartsWith',
} as Record<MatchType, string>;

export enum RuleType {
  HOST = 'HOST',
  URL = 'URL',
  SRC_IP = 'SRC_IP',
  HEADER = 'HEADER',
  COOKIE = 'COOKIE',
  PARAM = 'PARAM',
}

const KEY_TYPES = new Set([RuleType.COOKIE, RuleType.HEADER, RuleType.PARAM]);

const RULE_INDICATORS_CACHE: Record<string, RuleIndicator[]> = {};
const PARTIAL_RULE_INDICATORS_CACHE: Record<
  string,
  {
    more: boolean;
    indicators: RuleIndicator[];
  }
> = {};

function tokenizer(rawDsl: string): string[] {
  const tokens: string[] = [];
  let nextTokenBeg = 0;
  rawDsl.split('').forEach((char, i) => {
    if (char === '(') {
      tokens.push('(');
      nextTokenBeg = i + 1;
    } else if (char === ' ') {
      const token = rawDsl.slice(nextTokenBeg, i);
      if (token && token !== ' ' && token !== ')') {
        tokens.push(token);
      }
      nextTokenBeg = i + 1;
    } else if (char === ')') {
      const token = rawDsl.slice(nextTokenBeg, i);
      if (token && token !== ' ' && token !== ')') {
        tokens.push(token);
      }
      tokens.push(')');
      nextTokenBeg = i + 1;
    }
  });
  return tokens;
}

function parseTokens(tokens: string[]): string | any[] {
  if (tokens.length === 0) {
    return null;
  }

  const token = tokens.shift();

  if (token !== '(') {
    return token;
  }

  const exp = [];
  while (tokens[0] !== ')') {
    exp.push(parseTokens(tokens));
  }
  tokens.shift();
  return exp;
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function parseDSL(dslOrRule: string | Rule): RuleIndicator[] {
  let rawDSL;

  if (typeof dslOrRule === 'string') {
    rawDSL = dslOrRule;
  } else if (dslOrRule.spec.dslx) {
    return dslOrRule.spec.dslx;
  } else if (dslOrRule.spec.dsl) {
    rawDSL = dslOrRule.spec.dsl;
  } else {
    // tslint:disable-next-line: no-shadowed-variable
    const indicators: RuleIndicator[] = [];

    if (dslOrRule.spec.domain) {
      indicators.push({
        type: RuleType.HOST,
        key: null,
        values: [[MatchType.IN, dslOrRule.spec.domain]],
      });
    } else if (dslOrRule.spec.url) {
      indicators.push({
        type: RuleType.URL,
        key: null,
        values: [
          [
            dslOrRule.spec.url.startsWith('/')
              ? MatchType.STARTS_WITH
              : MatchType.REGEX,
            dslOrRule.spec.url,
          ],
        ],
      });
    } else {
      return [];
    }

    rawDSL = stringifyDSL(indicators);
  }

  if (RULE_INDICATORS_CACHE[rawDSL]) {
    return RULE_INDICATORS_CACHE[rawDSL];
  }

  const tokens = parseTokens(tokenizer(rawDSL)) as any[];

  if (!tokens) {
    return [];
  }

  const first = tokens[0];

  const indicators = first === MatchType.AND ? tokens.slice(1) : tokens;

  const ruleIndicators: RuleIndicator[] = [];

  (Array.isArray(indicators[0]) ? indicators : [indicators]).forEach(
    (indicator: [string, ...Array<string[] | string[][]>]) => {
      let matchType = indicator[0] as MatchType;
      const values = indicator.slice(1);

      const ruleIndicator = { values: [] } as RuleIndicator;

      if (matchType === MatchType.OR) {
        (values as string[][]).forEach(value => {
          matchType = value.shift() as MatchType;
          const ruleType = value.shift();

          if (!ruleIndicator.type) {
            ruleIndicator.type = ruleType as RuleType;
          } else if (ruleIndicator.type !== ruleType) {
            throw new TypeError(
              'invalid dsl, rule type should be same in a single OR block',
            );
          }

          if (KEY_TYPES.has(ruleIndicator.type)) {
            const key = value.shift();

            if (!ruleIndicator.key) {
              ruleIndicator.key = key;
            } else if (ruleIndicator.key !== key) {
              throw new TypeError(
                'invalid dsl, rule key should be same in a single OR block',
              );
            }
          }

          ruleIndicator.values.push([matchType, ...value]);
        });
      } else {
        const ruleType = values.shift() as RuleType;

        ruleIndicator.type = ruleType;

        if (KEY_TYPES.has(ruleIndicator.type)) {
          ruleIndicator.key = values.shift() as string;
        }

        if (ruleType === RuleType.HOST) {
          (values as string[]).forEach(value => {
            ruleIndicator.values.push([matchType, value]);
          });
        } else {
          ruleIndicator.values.push([matchType, ...(values as string[])]);
        }
      }

      ruleIndicators.push(ruleIndicator);
    },
  );

  return (RULE_INDICATORS_CACHE[rawDSL] = ruleIndicators);
}

export function stringifyDSL(ruleIndicators: RuleIndicator[]): string {
  let result = '';

  ruleIndicators.forEach(({ key, type, values }) => {
    if (values.length === 0) {
      return;
    }

    if (type === RuleType.HOST) {
      result += ` (IN HOST ${values.map(valueArr => valueArr[1]).join(' ')})`;
      return;
    }

    const temp = values.reduce((t, [matchType, ...args]) => {
      if (key) {
        args.unshift(key);
      }

      args.unshift(matchType, type);

      t += ` (${args.join(' ')})`;
      return t;
    }, '');

    result += values.length > 1 ? ` (OR${temp})` : temp;
  });

  return result && `(AND${result})`;
}

export function getPartialIndicators(
  rawDslOrIndicators: string | Rule | RuleIndicator[],
): { more: boolean; indicators: RuleIndicator[] } {
  let ruleIndicators: RuleIndicator[];

  if (Array.isArray(rawDslOrIndicators)) {
    ruleIndicators = rawDslOrIndicators;
  } else if (typeof rawDslOrIndicators !== 'string') {
    ruleIndicators = rawDslOrIndicators.spec.dslx;
    if (!ruleIndicators) {
      rawDslOrIndicators = rawDslOrIndicators.spec.dsl;
    }
  }

  if (typeof rawDslOrIndicators === 'string') {
    if (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators]) {
      return PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators];
    }
    ruleIndicators = parseDSL(rawDslOrIndicators);
  }

  const indicators: RuleIndicator[] = [];

  let i = 0;
  let more: boolean;
  for (let index = 0, len = ruleIndicators.length; index < len; index++) {
    const { key, type, values } = ruleIndicators[index];
    const newValues = takeWhile(values, () => {
      more = i++ > 2;
      return !more;
    });

    if (newValues.length > 0) {
      indicators.push({ key, type, values: newValues });
    }
    // more === true would also exec once
    if (more) {
      break;
    }
  }

  const result = {
    more,
    indicators,
  };

  if (typeof rawDslOrIndicators === 'string') {
    return (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators] = result);
  }

  return result;
}
