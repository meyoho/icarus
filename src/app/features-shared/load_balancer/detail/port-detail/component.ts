import {
  COMMON_WRITABLE_ACTIONS,
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sUtilService,
  NAME,
  ObservableInput,
  TranslateService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';

import { Frontend, RESOURCE_TYPES, ResourceType, Rule } from 'app/typings';
import { ACTION, STATUS } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { LoadBalancerUpdateDefaultCertificateComponent } from '../../form/update-default-certificate/component';
import { LoadBalancerPortUpdateServiceComponent } from '../../form/update-service/component';
import { getPartialIndicators, parseDSL } from '../../util';
import { LoadBalancerPortParams } from '../port/component';

@Component({
  selector: 'alk-alb-port-detail',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortDetailComponent {
  @Input()
  baseParams: LoadBalancerPortParams;

  @Input()
  params: LoadBalancerPortParams;

  @ObservableInput(true)
  params$: Observable<LoadBalancerPortParams>;

  @Input()
  frontend: Frontend;

  @ObservableInput(true)
  frontend$: Observable<Frontend>;

  @Output()
  frontendChange = new EventEmitter<void>();

  columns = [DESCRIPTION, 'rule', 'k8s_service_group', ACTION];

  parseDSL = parseDSL;
  getPartialIndicators = getPartialIndicators;

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchRules.bind(this),
  });

  permissions$ = this.frontend$.pipe(
    withLatestFrom(this.params$),
    switchMap(([frontend, { cluster }]) =>
      this.k8sPermission.isAllowed({
        resource: frontend,
        cluster,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    publishRef(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    @Optional() public readonly workspace: WorkspaceComponent,
  ) {}

  conflictInstances(frontend: Frontend) {
    return Object.entries(get(frontend, [STATUS, 'instances'], {})).reduce<
      string[]
    >((acc, [instance, { conflict }]) => {
      if (conflict) {
        acc.push(instance);
      }
      return acc;
    }, []);
  }

  fetchRules({
    cluster,
    namespace,
    name,
    port,
    ...queryParams
  }: LoadBalancerPortParams) {
    return this.k8sApi.getResourceList<Rule>({
      type: RESOURCE_TYPES.RULE,
      cluster,
      namespace,
      queryParams: {
        ...queryParams,
        labelSelector: matchLabelsToString({
          [this.k8sUtil.normalizeType(NAME, 'alb2')]: name,
          [this.k8sUtil.normalizeType('frontend', 'alb2')]: port,
        }),
      },
    });
  }

  updateDefaultService(frontend: Frontend) {
    const dialogRef = this.dialogService.open(
      LoadBalancerPortUpdateServiceComponent,
      {
        size: DialogSize.Big,
        data: {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          frontend,
          inWorkspace: !!this.workspace,
        },
      },
    );

    dialogRef.componentInstance.confirmed.subscribe((isConfirm: boolean) => {
      if (isConfirm) {
        this.frontendChange.emit();
      }
      dialogRef.close();
    });
  }

  updateDefaultCertificate(frontend: Frontend) {
    const dialogRef = this.dialogService.open(
      LoadBalancerUpdateDefaultCertificateComponent,
      {
        data: {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          frontend,
        },
      },
    );

    // eslint-disable-next-line sonarjs/no-identical-functions
    dialogRef.componentInstance.confirmed.subscribe((isConfirm: boolean) => {
      if (isConfirm) {
        this.frontendChange.emit();
      }
      dialogRef.close();
    });
  }

  async deleteFrontend(frontend: Frontend) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_port'),
        content: this.translate.get('confirm_delete_port'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.k8sApi
      .deleteResource({
        type: RESOURCE_TYPES.FRONTEND,
        cluster: this.params.cluster,
        resource: frontend,
      })
      .subscribe(() => {
        this.router.navigate(['../..'], {
          relativeTo: this.route,
        });
      });
  }

  async deleteRule(rule: Rule) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_rule'),
        content: this.translate.get('confirm_delete_rule'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.k8sApi
      .deleteResource({
        type: RESOURCE_TYPES.RULE,
        cluster: this.params.cluster,
        resource: rule,
      })
      .subscribe(() => this.list.delete(rule));
  }

  trackByFn(_index: number, rule: Rule) {
    return rule.metadata.uid;
  }
}
