import { Component, EventEmitter, Input, Output } from '@angular/core';

import { LoadBalancer } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

@Component({
  selector: 'rc-load-balancer-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerDetailInfoComponent {
  ALL = ASSIGN_ALL;

  @Input()
  inWorkspace: boolean;

  @Input()
  data: LoadBalancer;

  @Input()
  cluster: string;

  @Input()
  canUpdate: boolean;

  @Input()
  canDelete: boolean;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onUpdateProject = new EventEmitter<void>();

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onDelete = new EventEmitter<void>();
}
