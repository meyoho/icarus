import {
  API_GATEWAY,
  Dictionary,
  FALSE,
  K8sApiService,
  NOTIFY_ON_ERROR_HEADER,
  ObservableInput,
  TranslateService,
  matchLabelsToString,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import Highcharts from 'highcharts';
import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { Frontend, RESOURCE_TYPES } from 'app/typings';
import { shortNum } from 'app/utils';

import { LoadBalancerPortParams } from '../port/component';

export interface MetricQueryItem<Normalized = false> {
  metric: {
    __query_id__: string;
  };
  values: Array<[number, Normalized extends true ? number : string]>;
}

const TIME_RANGES = [
  {
    type: 'last_15_minutes',
    offset: 15 * 60,
    step: 30,
  },
  {
    type: 'last_hour',
    offset: 60 * 60,
    step: 60,
  },
  {
    type: 'last_day',
    offset: 24 * 60 * 60,
    step: 30 * 60,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 60 * 60,
    step: 4 * 60 * 60,
  },
  {
    type: 'last_30_days',
    offset: 30 * 24 * 60 * 60,
    step: 12 * 60 * 60,
  },
];

const FLOW_UNITS = [
  {
    type: 'bps',
    value: 1,
  },
  {
    type: 'Kbps',
    value: 1000,
  },
  {
    type: 'Mbps',
    value: 1000 * 1000,
  },
  {
    type: 'Gbps',
    value: 1000 * 1000 * 1000,
  },
];

const COLORS: Dictionary<string> = {
  income_flow: '#1fc0cc',
  output_flow: '#006eff',
  total_requests: '#006eff',
  status_code_2xx: '#47cc50',
  status_code_3xx: '#1fc0cc',
  status_code_4xx: '#f8ac58',
  status_code_5xx: '#d0021b',
};

@Component({
  selector: 'alk-alb-port-monitor',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortMonitorComponent {
  @ObservableInput()
  @Input('baseParams')
  baseParams$: Observable<LoadBalancerPortParams>;

  @ObservableInput()
  @Input('frontend')
  frontend$: Observable<Frontend>;

  pageGuard$ = combineLatest([this.frontend$, this.baseParams$]).pipe(
    switchMap(([frontend, { cluster }]) => {
      const { protocol } = frontend.spec;

      const protocolAllowed = /^https?$/.test(frontend.spec.protocol);

      if (!protocolAllowed) {
        return of({
          allowed: false,
          reason: this.translate.get('monitor_not_supported', {
            protocol,
          }),
        });
      }

      return this.k8sApi
        .getResourceList({
          type: RESOURCE_TYPES.FEATURE,
          cluster,
          queryParams: {
            labelSelector: matchLabelsToString({
              instanceType: 'prometheus',
            }),
          },
        })
        .pipe(
          map(({ items }) => ({
            allowed: !!items.length,
            reason: '',
          })),
        );
    }),
    publishRef(),
  );

  TIME_RANGES = TIME_RANGES;

  FLOW_UNITS = FLOW_UNITS;

  timeRange$$ = new BehaviorSubject(TIME_RANGES[0].type);
  flowUnit$$ = new BehaviorSubject(FLOW_UNITS[2].type);

  refresh$$ = new BehaviorSubject<void>(null);

  Highcharts = Highcharts;

  flowData$ = combineLatest([
    this.baseParams$,
    this.frontend$,
    this.timeRange$$,
    this.flowUnit$$,
    this.refresh$$,
  ]).pipe(
    switchMap(([{ cluster, name }, frontend, timeRange, flowUnit]) => {
      const { port } = frontend.spec;
      return this.fetchMetrics({
        cluster,
        timeRange,
        flowUnit: FLOW_UNITS.find(({ type }) => flowUnit === type).value,
        queries: [
          {
            name: 'income_flow',
            expr: `nginx_http_request_size_bytes{job="alb2-${name}",port="${port}"}`,
          },
          {
            name: 'output_flow',
            expr: `nginx_http_response_size_bytes{job="alb2-${name}", port="${port}"}`,
          },
        ],
      });
    }),
  );

  requestData$ = combineLatest([
    this.baseParams$,
    this.frontend$,
    this.timeRange$$,
    this.refresh$$,
  ]).pipe(
    switchMap(([{ cluster, name }, frontend, timeRange]) => {
      const { port } = frontend.spec;
      return this.fetchMetrics({
        cluster,
        timeRange,
        queries: [
          {
            name: 'total_requests',
            expr: `nginx_http_status{job="alb2-${name}",port="${port}",status=~"[0-9]{3}"}`,
          },
          {
            name: 'status_code_2xx',
            expr: `nginx_http_status{job="alb2-${name}",port="${port}",status=~"2[0-9]{2}"}`,
          },
          {
            name: 'status_code_3xx',
            expr: `nginx_http_status{job="alb2-${name}",port="${port}",status=~"3[0-9]{2}"}`,
          },
          {
            name: 'status_code_4xx',
            expr: `nginx_http_status{job="alb2-${name}",port="${port}",status=~"4[0-9]{2}"}`,
          },
          {
            name: 'status_code_5xx',
            expr: `nginx_http_status{job="alb2-${name}",port="${port}",status=~"5[0-9]{2}"}`,
          },
        ],
      });
    }),
  );

  constructor(
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
    private readonly translate: TranslateService,
  ) {
    this.getChartOptions = this.getChartOptions.bind(this);
  }

  fetchMetrics({
    cluster,
    timeRange,
    queries,
    flowUnit = 1,
  }: {
    cluster: string;
    timeRange: string;
    queries: Array<{
      name: string;
      expr: string;
    }>;
    flowUnit?: number;
  }) {
    const range = TIME_RANGES.find(({ type }) => type === timeRange);
    const end = Math.ceil(Date.now() / 1000);
    const initData = queries.map(({ name }) => ({
      metric: {
        __query_id__: name,
      },
      values: this.buildEmptyValues(range),
    }));
    const { offset, step } = range;
    return this.http
      .post<MetricQueryItem[]>(
        `${API_GATEWAY}/v1/metrics/${cluster}/query_range`,
        {
          start: end - offset,
          end,
          step,
          queries: queries.map(({ name, expr }) => ({
            id: name,
            range: 0,
            labels: [
              {
                name: '__name__',
                value: 'custom',
              },
              {
                name: 'expr',
                value: `sum by(port)(rate(${expr}[${
                  step < 5 * 60 ? step + 's' : '5m'
                }])) or on() vector(0)`,
              },
            ],
          })),
        },
        {
          headers: {
            [NOTIFY_ON_ERROR_HEADER]: FALSE,
          },
        },
      )
      .pipe(
        map(items =>
          items
            .reduce<MetricQueryItem[]>((acc, item) => {
              const type = item.metric.__query_id__;
              const exists = acc.find(i => i.metric.__query_id__ === type);
              if (exists) {
                exists.values.push(...item.values);
              } else {
                acc.push(item);
              }
              return acc;
            }, [])
            .map(item => ({
              ...item,
              values: item.values
                .reduce<Array<[number, number]>>((acc, [x, y]) => {
                  const exists = acc.find(i => i[0] === x);
                  if (exists) {
                    exists[1] += +y;
                  } else {
                    acc.push([x, +y]);
                  }
                  return acc;
                }, [])
                .map(([x, y]) => [x * 1000, y / flowUnit])
                .sort((a, b) => a[0] - b[0]),
            }))
            .sort((a, b) => {
              const aIndex = queries.findIndex(
                x => x.name === a.metric.__query_id__,
              );
              const bIndex = queries.findIndex(
                x => x.name === b.metric.__query_id__,
              );
              return aIndex - bIndex;
            }),
        ),
        startWith(initData),
        skipError(initData),
      );
  }

  buildEmptyValues({ offset, step }: { offset: number; step: number }) {
    const now = Math.ceil(Date.now() / 1000);
    const values: Array<[number, number]> = [];
    let currOffset = 0;
    while (currOffset <= offset) {
      values.unshift([(now - currOffset) * 1000, 0]);
      currOffset += step;
    }
    return values;
  }

  getChartOptions(
    items: Array<MetricQueryItem<true>>,
    {
      allowDecimals = true,
      unit = '',
    }: { allowDecimals: boolean; unit: string },
  ): Highcharts.Options {
    return {
      title: {
        text: '',
      },
      series: items.map(item => {
        const type = item.metric.__query_id__;
        return {
          type: 'line',
          name: this.translate.get(type),
          data: item.values,
          color: COLORS[type],
          lineColor: COLORS[type],
          marker: {
            enabled: false,
          },
        };
      }),
      tooltip: {
        xDateFormat: '%Y-%m-%d %H:%M:%S',
        pointFormatter() {
          return `<span style="color:${this.color}">●</span> ${
            this.series.name
          }: <b>${
            allowDecimals ? shortNum(this.y) : this.y.toFixed(0)
          }${unit}</b><br/>`;
        },
      },
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
          day: '%m-%d %H:%M',
        },
      },
      yAxis: {
        title: {
          text: '',
        },
        allowDecimals,
        min: 0,
        max:
          items.reduce(
            (max, item) =>
              Math.max(
                max,
                item.values.reduce(
                  (innerMax, [_, value]) => Math.max(innerMax, value),
                  0,
                ),
              ),
            0,
          ) * 2 || 1,
      },
      legend: {
        align: 'left',
        verticalAlign: 'top',
      },
      credits: {
        enabled: false,
      },
    };
  }
}
