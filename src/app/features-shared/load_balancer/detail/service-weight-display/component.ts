import { Component, Input } from '@angular/core';
import { memoize } from 'lodash-es';

import { RuleService } from 'app/typings';
@Component({
  selector: 'rc-load-balancer-service-weight-display',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerServiceWeightDisplayComponent {
  @Input()
  services: RuleService[];

  getTotalWeights = memoize((services: RuleService[]) =>
    services.reduce((total, curr) => total + curr.weight, 0),
  );

  getWeightsPercent(services: RuleService[], weight: number) {
    return ((weight / this.getTotalWeights(services)) * 100).toFixed(2) + '%';
  }
}
