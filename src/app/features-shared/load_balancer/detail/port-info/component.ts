// tslint:disable: no-output-on-prefix
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { Frontend } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-detail-port-info',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerPortInfoComponent {
  @Input()
  data: Frontend;

  @Input()
  cluster: string;

  @Input()
  inWorkspace: boolean;

  @Input()
  canDelete: boolean;

  @Input()
  canUpdate: boolean;

  @Output()
  onUpdateDefaultService = new EventEmitter<void>();

  @Output()
  onUpdateDefaultCertificate = new EventEmitter<void>();

  @Output()
  onDelete = new EventEmitter<void>();
}
