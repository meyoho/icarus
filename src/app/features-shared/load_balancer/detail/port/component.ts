import {
  AsyncDataLoader,
  K8sApiService,
  NAME,
  NAMESPACE,
  TOKEN_GLOBAL_NAMESPACE,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest, of } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  Frontend,
  RESOURCE_TYPES,
  WorkspaceDetailParams,
  WorkspaceListParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

export interface LoadBalancerPortParams
  extends WorkspaceDetailParams,
    WorkspaceListParams {
  port: string;
}

@Component({
  templateUrl: 'template.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerPortComponent {
  baseParams$: Observable<LoadBalancerPortParams> = combineLatest([
    this.route.paramMap,
    this.workspace ? this.workspace.baseParams : of(null),
  ]).pipe(
    map(([params, baseParams]) => ({
      cluster: params.get('cluster') || baseParams.cluster,
      namespace: params.get(NAMESPACE) || baseParams.namespace,
      name: params.get(NAME),
      port: params.get('port'),
    })),
    publishRef(),
  );

  params$ = this.baseParams$.pipe(
    map(baseParams => ({
      ...baseParams,
      namespace: this.globalNamespace,
    })),
  );

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    @Optional() public readonly workspace: WorkspaceComponent,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  dataFetcher({ cluster, namespace, port }: LoadBalancerPortParams) {
    return this.k8sApi.getResource<Frontend>({
      type: RESOURCE_TYPES.FRONTEND,
      cluster,
      namespace,
      name: port,
    });
  }
}
