import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  KubernetesResource,
  NAME,
  NAMESPACE,
  ResourceListParams,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  isAllowed,
  matchLabelsToString,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Component, Inject, OnDestroy, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { EMPTY, Subject, combineLatest, forkJoin, of } from 'rxjs';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  first,
  map,
  pluck,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { ForceDeleteComponent } from 'app/shared/components/force-delete/component';
import {
  Frontend,
  LoadBalancer,
  RESOURCE_TYPES,
  ResourceType,
  Rule,
  WorkspaceDetailParams,
  getYamlApiVersion,
} from 'app/typings';
import { ACTION, STATUS } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { LoadBalancerPortUpdateServiceComponent } from '../form/update-service/component';
import { LoadBalancerUpdateProjectComponent } from '../update-project/component';

interface FrontendAndRules extends KubernetesResource {
  frontend: Frontend;
  rules: Rule[];
}

const ACTIONS = [K8sResourceAction.UPDATE, K8sResourceAction.DELETE] as const;

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerDetailComponent implements OnDestroy {
  private readonly destroy$$ = new Subject<void>();

  loadBalancer: LoadBalancer;

  private readonly loading$$ = new Subject<boolean>();
  loading$ = this.loading$$
    .asObservable()
    .pipe(startWith(true), distinctUntilChanged());

  columns = this.workspace
    ? ['port', 'protocol', 'rule', 'default_k8s_service', ACTION]
    : ['port', 'protocol', 'default_k8s_service'];

  baseParams: WorkspaceDetailParams;

  baseParams$ = combineLatest([
    this.route.paramMap,
    this.workspace ? this.workspace.baseParams : of(null),
  ]).pipe(
    takeUntil(this.destroy$$),
    map(([params, baseParams]) => ({
      cluster: params.get('cluster') || baseParams.cluster,
      namespace: params.get(NAMESPACE) || baseParams.namespace,
      name: params.get(NAME),
    })),
    tap(baseParams => (this.baseParams = baseParams)),
    publishRef(),
  );

  reload$$ = new Subject<void>();

  loadBalancer$ = combineLatest([
    this.baseParams$,
    this.reload$$.pipe(startWith(null as void)),
  ]).pipe(
    tap(() => this.loading$$.next(true)),
    switchMap(([{ cluster, name }]) =>
      this.k8sApi.getResource<LoadBalancer>({
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        cluster,
        namespace: this.globalNamespace,
        name,
      }),
    ),
    tap(loadBalancer => {
      this.loadBalancer = loadBalancer;
      this.loading$$.next(false);
    }),
    publishRef(),
  );

  params: WorkspaceDetailParams;

  params$ = combineLatest([this.baseParams$, this.loadBalancer$]).pipe(
    map(([baseParams, loadBalancer]) => ({
      ...baseParams,
      namespace: this.k8sUtil.getNamespace(loadBalancer),
    })),
    tap(params => (this.params = params)),
    publishRef(),
  );

  albPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        action: ACTIONS,
        ...params,
      }),
    ),
    isAllowed(),
  );

  frontendPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.FRONTEND,
        action: ACTIONS,
        ...params,
      }),
    ),
    isAllowed(),
  );

  list = new K8SResourceList<FrontendAndRules>({
    fetchParams$: this.params$,
    fetcher: this.fetchFrontend.bind(this),
  });

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    @Optional() public workspace: WorkspaceComponent,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  fetchFrontend({
    cluster,
    namespace,
    name,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi
      .getResourceList<Frontend>({
        type: RESOURCE_TYPES.FRONTEND,
        cluster,
        namespace,
        queryParams: {
          ...queryParams,
          labelSelector: matchLabelsToString({
            [this.k8sUtil.normalizeType(NAME, 'alb2')]: name,
          }),
        },
      })
      .pipe(
        switchMap(({ kind, items, metadata }) =>
          items.length > 0
            ? forkJoin(
                items.map(frontend => this.getFrontendAndRules(frontend)),
              ).pipe(
                // tslint:disable-next-line: no-shadowed-variable
                map(items => ({
                  kind,
                  items,
                  metadata,
                })),
              )
            : of({
                kind,
                items: [] as FrontendAndRules[],
                metadata,
              }),
        ),
      );
  }

  normalizeCertificate(certificateName: string) {
    return certificateName.split('_').join('/');
  }

  conflictInstances(frontend: Frontend) {
    return Object.entries(get(frontend, [STATUS, 'instances'], {})).reduce<
      string[]
    >((acc, [instance, { conflict }]) => {
      if (conflict) {
        acc.push(instance);
      }
      return acc;
    }, []);
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  async deleteFrontend(row: FrontendAndRules) {
    try {
      await this.dialog.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('load_balancer_delete_frontend_confirm', {
          port: row.frontend.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.k8sApi
      .deleteResource({
        type: RESOURCE_TYPES.FRONTEND,
        cluster: this.baseParams.cluster,
        resource: row.frontend,
      })
      .pipe(skipError())
      .subscribe(() =>
        this.list.scanItems(items =>
          items.filter(
            item => item.frontend.metadata.name !== row.frontend.metadata.name,
          ),
        ),
      );
  }

  updateDefaultService(row: FrontendAndRules) {
    const dialogRef = this.dialog.open(LoadBalancerPortUpdateServiceComponent, {
      size: DialogSize.Big,
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        inWorkspace: !!this.workspace,
        frontend: row.frontend,
      },
    });

    dialogRef.componentInstance.confirmed.pipe(first()).subscribe(frontend => {
      if (frontend) {
        this.list.scanItems(items => {
          return items.map(item => {
            if (item.frontend.metadata.name === frontend.metadata.name) {
              return {
                ...row,
                frontend,
              };
            } else {
              return item;
            }
          });
        });
      }
      dialogRef.close();
    });
  }

  onUpdateProject() {
    const dialogRef = this.dialog.open(LoadBalancerUpdateProjectComponent, {
      data: {
        cluster: this.baseParams.cluster,
        loadBalancer: this.loadBalancer,
      },
    });

    dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((loadBalancer?: LoadBalancer) => {
        dialogRef.close();
        if (loadBalancer) {
          this.loadBalancer = loadBalancer;
        }
      });
  }

  deleteLB() {
    const { name } = this.loadBalancer.metadata;
    const dialogRef = this.dialog.open(ForceDeleteComponent, {
      data: {
        name,
        title: this.translate.get('delete_load_balancer'),
        content: this.translate.get('delete_load_balancer_content', {
          name,
        }),
      },
    });
    dialogRef.componentInstance.close
      .pipe(
        concatMap(isConfirm => {
          if (!isConfirm) {
            dialogRef.close();
            return EMPTY;
          }
          return this.k8sApi
            .deleteGlobalResource({
              type: RESOURCE_TYPES.HELM_REQUEST,
              resource: {
                ...this.loadBalancer,
                apiVersion: getYamlApiVersion(RESOURCE_TYPES.HELM_REQUEST),
                metadata: {
                  ...this.loadBalancer.metadata,
                  name: [
                    this.baseParams.cluster,
                    this.k8sUtil.getName(this.loadBalancer),
                  ].join('-'),
                },
              },
            })
            .pipe(
              catchError(e => {
                if ([e.code, e.status].includes(404)) {
                  return of(null);
                }
                return EMPTY;
              }),
            );
        }),
        concatMap(() =>
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
              cluster: this.baseParams.cluster,
              resource: this.loadBalancer,
            })
            .pipe(
              // eslint-disable-next-line sonarjs/no-identical-functions
              catchError(e => {
                if ([e.code, e.status].includes(404)) {
                  return of(null);
                }
                return EMPTY;
              }),
            ),
        ),
        takeUntil(this.destroy$$),
      )
      .subscribe(() => {
        dialogRef.close();
        this.router.navigate(['/admin/load_balancer']);
      });
  }

  getFrontendAndRules(frontend: Frontend) {
    return this.k8sApi
      .getResourceList<Rule>({
        type: RESOURCE_TYPES.RULE,
        cluster: this.baseParams.cluster,
        namespace: this.params.namespace,
        queryParams: {
          labelSelector: matchLabelsToString({
            [this.k8sUtil.normalizeType(NAME, 'alb2')]: this.baseParams.name,
            [this.k8sUtil.normalizeType('frontend', 'alb2')]: frontend.metadata
              .name,
          }),
        },
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        map(rules => ({ frontend, rules })),
      );
  }

  trackByFn(_index: number, item: FrontendAndRules) {
    return item.frontend.metadata.uid;
  }
}
