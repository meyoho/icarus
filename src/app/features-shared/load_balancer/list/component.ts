import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  NAMESPACE,
  PROJECT,
  ResourceListParams,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  isAllowed,
  matchExpressionsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Component, Inject, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, Observable, combineLatest, of } from 'rxjs';
import {
  catchError,
  concatMap,
  first,
  map,
  switchMap,
  tap,
} from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { ForceDeleteComponent } from 'app/shared/components/force-delete/component';
import {
  LoadBalancer,
  RESOURCE_TYPES,
  ResourceType,
  WorkspaceBaseParams,
  getYamlApiVersion,
} from 'app/typings';
import { ACTION, ASSIGN_ALL } from 'app/utils';
import { AdminLayoutComponent } from 'app_admin/layout/layout.component';

import { LoadBalancerCreateComponent } from '../create/component';
import { LoadBalancerUpdateProjectComponent } from '../update-project/component';

const COMMON_COLUMNS = [NAME, 'address', 'project', 'created_time'];
@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class LoadBalancerListComponent {
  ALL = ASSIGN_ALL;

  filterKey = '';

  isAdmin = !!this.layoutComponent;

  columns = this.isAdmin ? [...COMMON_COLUMNS, ACTION] : COMMON_COLUMNS;

  baseParams: WorkspaceBaseParams;

  baseParams$: Observable<WorkspaceBaseParams> = (this.isAdmin
    ? this.layoutComponent.getCluster$().pipe(
        map(({ metadata: { name } }) => ({
          project: null,
          cluster: name,
          namespace: null,
        })),
      )
    : this.route.parent.parent.paramMap.pipe(
        map(params => ({
          project: params.get('project'),
          cluster: params.get('cluster'),
          namespace: params.get(NAMESPACE),
        })),
      )
  ).pipe(
    tap(baseParams => (this.baseParams = baseParams)),
    publishRef(),
  );

  permissions$ = this.baseParams$.pipe(
    switchMap(baseParams => {
      const { cluster, namespace } = baseParams;
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        cluster,
        namespace,
        action: COMMON_WRITABLE_ACTIONS,
      });
    }),
    isAllowed(),
  );

  params$ = combineLatest([this.route.queryParamMap, this.baseParams$]).pipe(
    map(([queryParams, { project, cluster, namespace }]) => ({
      cluster,
      namespace,
      keyword: queryParams.get('keyword'),
      labelSelector: this.isAdmin
        ? undefined
        : matchExpressionsToString([
            {
              key: this.k8sUtil.normalizeType(NAME, PROJECT),
              operator: 'in',
              values: [ASSIGN_ALL, project],
            },
          ]),
    })),
  );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchLoadBalancers.bind(this),
  });

  constructor(
    private readonly dialog: DialogService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly translate: TranslateService,
    @Optional()
    private readonly layoutComponent: AdminLayoutComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  fetchLoadBalancers({
    cluster,
    namespace: _namespace,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi.getResourceList<LoadBalancer>({
      type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
      cluster,
      namespace: this.globalNamespace,
      queryParams,
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  create() {
    const { cluster } = this.baseParams;
    const dialogRef = this.dialog.open(LoadBalancerCreateComponent, {
      data: {
        cluster,
      },
    });
    dialogRef.componentInstance.confirmed
      .pipe(first())
      .subscribe(loadBalancer => {
        dialogRef.close();
        if (loadBalancer) {
          this.router.navigate(
            [
              '../detail/' + this.k8sUtil.getName(loadBalancer),
              {
                cluster,
                namespace: this.globalNamespace,
              },
            ],
            {
              relativeTo: this.route,
            },
          );
        }
      });
  }

  deleteLoadBalancer(loadBalancer: LoadBalancer) {
    const name = this.k8sUtil.getName(loadBalancer);

    const dialogRef = this.dialog.open(ForceDeleteComponent, {
      data: {
        name,
        title: this.translate.get('delete_load_balancer'),
        content: this.translate.get('delete_load_balancer_content', {
          name,
        }),
      },
    });

    dialogRef.componentInstance.close
      .pipe(
        concatMap(isConfirm => {
          if (!isConfirm) {
            dialogRef.close();
            return EMPTY;
          }
          return this.k8sApi
            .deleteGlobalResource({
              type: RESOURCE_TYPES.HELM_REQUEST,
              resource: {
                ...loadBalancer,
                apiVersion: getYamlApiVersion(RESOURCE_TYPES.HELM_REQUEST),
                metadata: {
                  ...loadBalancer.metadata,
                  name: [this.baseParams.cluster, name].join('-'),
                },
              },
            })
            .pipe(
              catchError(e => {
                if ([e.code, e.status].includes(404)) {
                  return of(null);
                }
                return EMPTY;
              }),
            );
        }),
        concatMap(() =>
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
              cluster: this.baseParams.cluster,
              resource: loadBalancer,
            })
            .pipe(
              // eslint-disable-next-line sonarjs/no-identical-functions
              catchError(e => {
                if ([e.code, e.status].includes(404)) {
                  return of(null);
                }
                return EMPTY;
              }),
            ),
        ),
      )
      .subscribe(() => {
        dialogRef.close();
        this.list.reload();
      });
  }

  updateProject(loadBalancer: LoadBalancer) {
    const dialogRef = this.dialog.open(LoadBalancerUpdateProjectComponent, {
      data: {
        cluster: this.baseParams.cluster,
        loadBalancer,
      },
    });

    dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((alb?: LoadBalancer) => {
        dialogRef.close();
        if (alb) {
          this.list.update(alb);
        }
      });
  }

  trackByFn(_index: number, item: LoadBalancer) {
    return item.metadata.uid;
  }
}
