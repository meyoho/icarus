import {
  CLUSTER,
  K8sApiService,
  NAME,
  PROJECT,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
} from '@angular/core';
import { find } from 'lodash-es';
import { Subject } from 'rxjs';
import { finalize, map, pluck } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { LoadBalancer, RESOURCE_TYPES } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerUpdateProjectComponent {
  close = new EventEmitter<LoadBalancer | void>();

  ALL = ASSIGN_ALL;

  loading$ = new Subject<boolean>();

  allProjects$ = this.projectApi
    .getProjects()
    .pipe(pluck('items'), publishRef());

  projects$ = this.allProjects$.pipe(
    map(projects =>
      projects.filter(project =>
        find(project.spec.clusters, ({ name }) => name === this.data.cluster),
      ),
    ),
    publishRef(),
  );

  project: string = this.k8sUtil.getLabel(
    this.data.loadBalancer,
    NAME,
    PROJECT,
  );

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly projectApi: ProjectService,
    @Inject(DIALOG_DATA)
    public data: {
      cluster: string;
      loadBalancer: LoadBalancer;
    },
  ) {}

  confirm() {
    this.loading$.next(true);
    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.ALAUDA_LOADBALANCER2,
        cluster: this.data.cluster,
        resource: this.data.loadBalancer,
        part: {
          metadata: {
            labels: {
              [this.k8sUtil.normalizeType(NAME, CLUSTER)]: this.data.cluster,
              [this.k8sUtil.normalizeType(NAME, PROJECT)]: this.project,
            },
          },
        },
      })
      .pipe(finalize(() => this.loading$.next(false)))
      .subscribe(loadBalancer => this.close.emit(loadBalancer));
  }
}
