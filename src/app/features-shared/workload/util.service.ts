import {
  ANNOTATIONS,
  K8sApiService,
  METADATA,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Inject, Injectable } from '@angular/core';
import { toNumber } from 'lodash-es';
import { assocPath, compose, max, path, pathOr, toString } from 'ramda';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, Workload, WorkloadType } from 'app/typings';

import { WORKLOAD_TYPE } from './inject-tokens';

@Injectable()
export class UtilService {
  specReplicasPath = ['spec', 'replicas'];
  legacyLastReplicasPath = [
    METADATA,
    ANNOTATIONS,
    `${this.workloadType.toLowerCase()}.${
      this.env.LABEL_BASE_DOMAIN
    }/last-replica`,
  ];

  lastReplicasPath = [
    METADATA,
    ANNOTATIONS,
    `app.${this.env.LABEL_BASE_DOMAIN}/last-replicas`,
  ];

  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    @Inject(WORKLOAD_TYPE) private readonly workloadType: WorkloadType,
  ) {}

  start({ cluster, resource }: { cluster: string; resource: Workload }) {
    const lastReplicas = path(this.lastReplicasPath, resource);

    const legacyLastReplicas = compose(
      max(1),
      toNumber,
      pathOr(1, this.legacyLastReplicasPath),
    )(resource);
    const part = {
      spec: {
        replicas: lastReplicas ? toNumber(lastReplicas) : legacyLastReplicas,
      },
    };
    return this.dialog.confirm({
      title: this.translate.get('start'),
      content: this.translate.get(
        `${this.workloadType.toLowerCase()}_start_confirm`,
        {
          name: resource.metadata.name,
        },
      ),
      confirmText: this.translate.get('confirm'),
      cancelText: this.translate.get('cancel'),
      beforeConfirm: (resolve, reject) => {
        this.k8sApi
          .patchResource({
            type: this.workloadType,
            cluster,
            resource,
            part,
          })
          .subscribe(resolve, reject);
      },
    });
  }

  stop({ cluster, resource }: { cluster: string; resource: Workload }) {
    const part = assocPath(
      this.lastReplicasPath,
      compose(toString, pathOr(1, ['spec', 'replicas']))(resource),
      { spec: { replicas: 0 } },
    );
    return this.dialog.confirm({
      title: this.translate.get('stop'),
      content: this.translate.get(
        `${this.workloadType.toLowerCase()}_stop_confirm`,
        {
          name: resource.metadata.name,
        },
      ),
      confirmText: this.translate.get('confirm'),
      cancelText: this.translate.get('cancel'),
      // eslint-disable-next-line sonarjs/no-identical-functions
      beforeConfirm: (resolve, reject) => {
        this.k8sApi
          .patchResource({
            type: this.workloadType,
            cluster,
            resource,
            part,
          })
          .subscribe(resolve, reject);
      },
    });
  }

  delete({ cluster, resource }: { cluster: string; resource: Workload }) {
    return this.dialog.confirm({
      title: this.translate.get(
        `${this.workloadType.toLowerCase()}_delete_confirm`,
        {
          name: resource.metadata.name,
        },
      ),
      confirmText: this.translate.get('delete'),
      cancelText: this.translate.get('cancel'),
      beforeConfirm: (resolve, reject) => {
        this.k8sApi
          .deleteResource({
            type: this.workloadType,
            cluster,
            resource,
          })
          .subscribe(resolve, reject);
      },
    });
  }
}
