import {
  KubernetesResource,
  LABELS,
  METADATA,
  StringMap,
} from '@alauda/common-snippet';
import { get } from 'lodash-es';
import {
  assocPath,
  compose,
  converge,
  dissoc,
  identity,
  lensPath,
  pathOr,
  set,
} from 'ramda';

import {
  DaemonSet,
  Deployment,
  GenericWorkloadStatus,
  RESOURCE_TYPES,
  StatefulSet,
  Workload,
  WorkloadKind,
  WorkloadStatusEnum,
  WorkloadType,
} from 'app/typings';
import { STATUS } from 'app/utils';

export function parseDeploymentStatus(res: Deployment) {
  const status = res.status;
  if (
    larger(res.spec.replicas, 0) &&
    equal(status.updatedReplicas, res.spec.replicas) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.availableReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation)
  ) {
    return WorkloadStatusEnum.running;
  } else if (
    equal(res.spec.replicas, 0) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.updatedReplicas, res.spec.replicas) &&
    equal(status.availableReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation)
  ) {
    return WorkloadStatusEnum.stopped;
  } else {
    return WorkloadStatusEnum.pending;
  }
}

export function parseDaemonSetStatus(res: DaemonSet) {
  const status = res.status;
  if (
    larger(status.desiredNumberScheduled, 0) &&
    largerEqual(status.observedGeneration, res.metadata.generation) &&
    equal(status.desiredNumberScheduled, status.currentNumberScheduled) &&
    equal(status.numberReady, status.desiredNumberScheduled) &&
    equal(status.numberAvailable, status.desiredNumberScheduled) &&
    (res.spec.updateStrategy.type !== 'RollingUpdate' ||
      equal(status.updatedNumberScheduled, status.desiredNumberScheduled))
  ) {
    return WorkloadStatusEnum.running;
  } else if (
    equal(status.desiredNumberScheduled, 0) &&
    largerEqual(status.observedGeneration, res.metadata.generation) &&
    equal(status.desiredNumberScheduled, status.desiredNumberScheduled) &&
    equal(status.numberReady, status.desiredNumberScheduled) &&
    equal(status.numberAvailable, status.desiredNumberScheduled) &&
    equal(status.currentNumberScheduled, status.desiredNumberScheduled) &&
    (res.spec.updateStrategy.type !== 'RollingUpdate' ||
      equal(status.updatedNumberScheduled, status.desiredNumberScheduled))
  ) {
    return WorkloadStatusEnum.stopped;
  } else {
    return WorkloadStatusEnum.pending;
  }
}

export function parseStatefulSetStatus(res: StatefulSet) {
  const status = res.status;
  if (
    larger(res.spec.replicas, 0) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.readyReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation) &&
    (res.spec.updateStrategy.type !== 'RollingUpdate' ||
      (equal(status.currentRevision, status.updateRevision) &&
        equal(status.currentReplicas, res.spec.replicas)))
  ) {
    return WorkloadStatusEnum.running;
  } else if (
    equal(res.spec.replicas, 0) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.readyReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation) &&
    (res.spec.updateStrategy.type !== 'RollingUpdate' ||
      (equal(status.currentRevision, status.updateRevision) &&
        equal(status.currentReplicas, res.spec.replicas)))
  ) {
    return WorkloadStatusEnum.stopped;
  } else {
    return WorkloadStatusEnum.pending;
  }
}

export function parseToWorkloadStatus(data: Deployment): GenericWorkloadStatus {
  switch (data.kind) {
    case WorkloadKind.Deployment:
      return {
        status: parseDeploymentStatus(data),
        desired: get(data, ['spec', 'replicas'], 0),
        current: get(data, [STATUS, 'availableReplicas'], 0),
      };
    case WorkloadKind.DaemonSet:
      return {
        status: parseDaemonSetStatus(data),
        desired: get(data, [STATUS, 'desiredNumberScheduled'], 0),
        current: get(data, [STATUS, 'numberReady'], 0),
      };
    case WorkloadKind.StatefulSet:
      return {
        status: parseStatefulSetStatus(data),
        desired: get(data, ['spec', 'replicas'], 0),
        current: get(data, [STATUS, 'readyReplicas'], 0),
      };
    default:
      return {
        status: WorkloadStatusEnum.stopped,
        desired: 0,
        current: 0,
      };
  }
}

export interface FlattenAffinity {
  isAnti: boolean;
  classification: 'Preferred' | 'Required';
  weight?: number;
  topologyKey: string;
  matchLabels: StringMap;
}

export interface RequiredAffinity {
  topologyKey: string;
  labelSelector: {
    matchLabels: StringMap;
  };
}

export interface PreferredAffinity {
  weight: number;
  podAffinityTerm: {
    labelSelector: {
      matchLabels: StringMap;
    };
    topologyKey: string;
  };
}

export interface PodAffinity {
  requiredDuringSchedulingIgnoredDuringExecution?: RequiredAffinity[];
  preferredDuringSchedulingIgnoredDuringExecution?: PreferredAffinity[];
}

export interface Affinity {
  podAffinity?: PodAffinity;
  podAntiAffinity?: PodAffinity;
}

export function flatAffinity(affinity: Affinity): FlattenAffinity[] {
  if (!affinity) {
    return [];
  }
  return [
    ...flatPodAffinity(affinity.podAffinity || {}),
    ...flatPodAntiAffinity(affinity.podAntiAffinity || {}),
  ];
}

export function flatPodAffinity({
  requiredDuringSchedulingIgnoredDuringExecution: required,
  preferredDuringSchedulingIgnoredDuringExecution: preferred,
}: PodAffinity) {
  return [
    ...(required || []).map(flatRequiredAffinity),
    ...(preferred || []).map(flatPreferredAffinity),
  ].map(item => ({
    isAnti: false,
    ...item,
  }));
}

export function flatPodAntiAffinity({
  requiredDuringSchedulingIgnoredDuringExecution: required,
  preferredDuringSchedulingIgnoredDuringExecution: preferred,
}: PodAffinity) {
  return [
    ...(required || []).map(flatRequiredAffinity),
    ...(preferred || []).map(flatPreferredAffinity),
  ].map(item => ({
    isAnti: true,
    ...item,
  }));
}

export function flatPreferredAffinity(data: PreferredAffinity) {
  return {
    classification: 'Preferred' as 'Preferred',
    weight: data.weight,
    topologyKey: data.podAffinityTerm.topologyKey,
    matchLabels: data.podAffinityTerm.labelSelector.matchLabels,
  };
}

export function flatRequiredAffinity(data: RequiredAffinity) {
  return {
    classification: 'Required' as 'Required',
    topologyKey: data.topologyKey,
    matchLabels: data.labelSelector.matchLabels,
  };
}

export const serviceSetter = (baseDomain: string) => {
  const lens = lensPath([
    'spec',
    'template',
    METADATA,
    LABELS,
    `service.${baseDomain}/name`,
  ]);
  return converge(set(lens), [
    (model: Workload) =>
      `${model.kind.toLocaleLowerCase()}-${model.metadata.name}`,
    identity,
  ]);
};

export const projectSetter = (baseDomain: string, project: string) => {
  return assocPath(
    ['spec', 'template', METADATA, LABELS, `project.${baseDomain}/name`],
    project,
  );
};

export function getWorkloadKind(type: WorkloadType) {
  switch (type) {
    case RESOURCE_TYPES.DEPLOYMENT:
      return 'Deployment';
    case RESOURCE_TYPES.DAEMONSET:
      return 'DaemonSet';
    case RESOURCE_TYPES.STATEFULSET:
      return 'StatefulSet';
  }
}

export const dropRollingUpdate = dissoc('rollingUpdate');

export function numberOrPercent(x: number | string) {
  if (!x && x !== 0) {
    return '25%';
  }
  if (typeof x === 'number' || x.endsWith('%')) {
    return x;
  } else {
    return parseInt(x, 10);
  }
}

export function getBelongingApp(resource: KubernetesResource) {
  const refs = resource.metadata.ownerReferences;
  const app = refs ? refs.find(item => item.kind === 'Application') : null;
  return app ? app.name : null;
}

function equal(x: number | string, y: number | string) {
  return defaultZero(x) === defaultZero(y);
}

function largerEqual(x: number, y: number) {
  return defaultZero(x) >= defaultZero(y);
}

function larger(x: number, y: number) {
  return defaultZero(x) > defaultZero(y);
}

function defaultZero(x: number | string) {
  return x || 0;
}

export const copyMatchLabels: (baseDomain: string) => <T>(r: T) => T = (
  baseDomain: string,
) => {
  const labelsPath = ['spec', 'template', METADATA, LABELS];
  const matchLabelsPath = ['spec', 'selector', 'matchLabels'];
  const servicePath = [`service.${baseDomain}/name`];
  const projectPath = [`project.${baseDomain}/name`];
  const copyService = converge(assocPath(matchLabelsPath.concat(servicePath)), [
    pathOr('', labelsPath.concat(servicePath)),
    identity,
  ]);
  const copyProject = converge(assocPath(matchLabelsPath.concat(projectPath)), [
    pathOr('', labelsPath.concat(projectPath)),
    identity,
  ]);
  return compose(copyProject, copyService);
};

export function getVolumes(podController: Workload) {
  return get(podController, ['spec', 'template', 'spec', 'volumes']);
}
