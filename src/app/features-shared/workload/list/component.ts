import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  KubernetesResource,
  catchPromise,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { Workload, WorkloadType } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { WORKLOAD_TYPE } from '../inject-tokens';
import { getBelongingApp, parseToWorkloadStatus } from '../util';
import { UtilService } from '../util.service';

@Component({
  selector: 'rc-workload-list',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent implements OnDestroy {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermission: K8sPermissionService,
    private readonly workspace: WorkspaceComponent,
    private readonly appSharedService: AppSharedService,
    private readonly utilService: UtilService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(WORKLOAD_TYPE) public workloadType: WorkloadType,
  ) {
    this.list = new K8SResourceList<Workload>({
      fetcher: queryParams =>
        this.k8sApi.getResourceList({
          type: this.workloadType,
          cluster: workspace.baseParamsSnapshot.cluster,
          namespace: workspace.baseParamsSnapshot.namespace,
          queryParams,
        }),
      activatedRoute,
      watcher: seed =>
        this.k8sApi.watchResourceChange(seed, {
          type: this.workloadType,
          cluster: workspace.baseParamsSnapshot.cluster,
          namespace: workspace.baseParamsSnapshot.namespace,
        }),
    });
  }

  list: K8SResourceList<Workload>;
  baseParams = this.workspace.baseParamsSnapshot;
  getBelongingApp = getBelongingApp;

  permissions$ = this.k8sPermission.isAllowed({
    type: this.workloadType,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  parseToWorkloadStatus = parseToWorkloadStatus;

  ngOnDestroy() {
    this.list.destroy();
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  create() {
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['create'], {
            relativeTo: this.activatedRoute,
            queryParams: res,
          });
        }
      });
  }

  createFromYaml() {
    this.router.navigate(['create_from_yaml'], {
      relativeTo: this.activatedRoute,
    });
  }

  update(resource: Workload) {
    this.router.navigate(['update', resource.metadata.name], {
      relativeTo: this.activatedRoute,
    });
  }

  delete(resource: Workload) {
    catchPromise(
      this.utilService.delete({
        cluster: this.baseParams.cluster,
        resource,
      }),
    ).subscribe(() => this.list.delete(resource));
  }

  isFederated = (resource: KubernetesResource) => {
    return this.k8sUtil.isFederatedResource(resource);
  };
}
