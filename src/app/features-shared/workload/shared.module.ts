import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { AlarmSharedModule } from '../alarm/shared.module';
import { AppSharedModule } from '../app/shared.module';
import { EventSharedModule } from '../event/shared.module';
import { ImageSharedModule } from '../image/shared.module';
import { PodSharedModule } from '../pod/shared.module';

import { CreateFromYamlComponent } from './create-from-yaml/component';
import { CreateComponent } from './create/component';
import { FormContainerComponent } from './create/form-container/component';
import { FormComponent } from './create/form/component';
import { DaemonSetStrategyFormComponent } from './create/strategy-form/daemonset-strategy-form/component';
import { DeploymentStrategyFormComponent } from './create/strategy-form/deployment-strategy-form/component';
import { StatefulSetStrategyFormComponent } from './create/strategy-form/statefulset-strategy-form/component';
import { DetailComponent } from './detail/component';
import { DetailInfoComponent } from './detail/detail-info/component';
import { ListComponent } from './list/component';
import { UpdateComponent } from './update/component';

@NgModule({
  imports: [
    SharedModule,
    AppSharedModule,
    AlarmSharedModule,
    ImageSharedModule,
    EventSharedModule,
    PodSharedModule,
  ],
  declarations: [
    ListComponent,
    DetailComponent,
    DetailInfoComponent,
    CreateFromYamlComponent,
    CreateComponent,
    UpdateComponent,
    FormContainerComponent,
    FormComponent,
    DeploymentStrategyFormComponent,
    DaemonSetStrategyFormComponent,
    StatefulSetStrategyFormComponent,
  ],
  exports: [
    AppSharedModule,
    AlarmSharedModule,
    ImageSharedModule,
    PodSharedModule,
    ListComponent,
    DetailComponent,
    CreateFromYamlComponent,
    CreateComponent,
    UpdateComponent,
    FormComponent,
  ],
})
export class WorkloadSharedModule {}
