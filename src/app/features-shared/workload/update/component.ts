import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { WorkloadType } from 'app/typings';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { WORKLOAD_TYPE } from '../inject-tokens';

@Component({
  selector: 'rc-workload-update',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent {
  resource$ = this.activatedRoute.paramMap.pipe(
    map(param => param.get(NAME)),
    switchMap(name =>
      this.k8sApi.getResource({
        type: this.workloadType,
        cluster: this.workspace.baseParamsSnapshot.cluster,
        namespace: this.workspace.baseParamsSnapshot.namespace,
        name,
      }),
    ),
    map(removeDirtyDataBeforeUpdate),
    publishRef(),
  );

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    @Inject(WORKLOAD_TYPE) private readonly workloadType: WorkloadType,
  ) {}
}
