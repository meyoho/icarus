import {
  K8sApiService,
  K8sResourceAction,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { compose } from '@ngrx/store';
import { pathOr } from 'ramda';
import { first, tap } from 'rxjs/operators';

import { HpaConfigResource } from 'app/features-shared/app/detail/hpa/type';
import { UpdateWorkloadLabelsDialogComponent } from 'app/features-shared/app/dialog/update-workload-labels/component';
import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import {
  FlattenAffinity,
  flatAffinity,
  getBelongingApp,
  getVolumes,
  parseToWorkloadStatus,
} from 'app/features-shared/workload/util';
import { UtilService } from 'app/features-shared/workload/util.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  DaemonSet,
  Deployment,
  RESOURCE_TYPES,
  StatefulSet,
  Workload,
  WorkloadType,
} from 'app/typings';
import {
  CALICO_ANNOTATION_PREFIX,
  KUBE_OVN_ANNOTATION_PREFIX,
} from 'app_admin/features/network/subnet/util';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { WORKLOAD_TYPE } from '../../inject-tokens';

@Component({
  selector: 'rc-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailInfoComponent implements OnChanges {
  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
    private readonly util: UtilService,
    private readonly k8sApi: K8sApiService,
    private readonly dialog: DialogService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly terminal: TerminalService,
    private readonly appUtilService: AppUtilService,
    private readonly translate: TranslateService,
    private readonly notification: NotificationService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(WORKLOAD_TYPE) public workloadType: WorkloadType,
  ) {
    this.getIpPool = this.getIpPool.bind(this);
  }

  @Input()
  resource: Workload;

  @Input()
  autoScaler: HpaConfigResource;

  @Input()
  permissions: Partial<Record<K8sResourceAction, boolean>> = {};

  @Output()
  stateChange = new EventEmitter<{
    resource: Workload;
    autoScaler?: HpaConfigResource;
  } | void>();

  updateStrategy: {
    type?: string;
    rollingUpdate?: {
      maxSurge?: string | number;
      maxUnavailable?: string | number;
      partition?: string | number;
    };
  };

  baseParams = this.workspace.baseParamsSnapshot;
  isDaemonSet = this.workloadType === RESOURCE_TYPES.DAEMONSET;

  getVolumes = getVolumes;
  getBelongingApp = getBelongingApp;

  parseToWorkloadStatus = parseToWorkloadStatus;

  path = pathOr('-');

  ngOnChanges({ resource }: SimpleChanges) {
    if (resource && resource.currentValue) {
      const res = resource.currentValue;
      if (this.workloadType === RESOURCE_TYPES.DEPLOYMENT) {
        this.updateStrategy = (res as Deployment).spec.strategy;
      } else {
        this.updateStrategy = (res as
          | DaemonSet
          | StatefulSet).spec.updateStrategy;
      }
    }
  }

  start() {
    this.util
      .start({
        cluster: this.baseParams.cluster,
        resource: this.resource,
      })
      .then(resource => this.stateChange.emit({ resource }), noop);
  }

  stop() {
    this.util
      .stop({
        cluster: this.baseParams.cluster,
        resource: this.resource,
      })
      .then(resource => this.stateChange.emit({ resource }), noop);
  }

  update() {
    this.router.navigate(
      [this.workloadType.toLowerCase(), 'update', this.resource.metadata.name],
      {
        relativeTo: this.workspace.baseActivatedRoute,
      },
    );
  }

  delete() {
    this.util
      .delete({
        cluster: this.baseParams.cluster,
        resource: this.resource,
      })
      .then(() => {
        this.router.navigate([this.workloadType.toLowerCase()], {
          relativeTo: this.workspace.baseActivatedRoute,
        });
      }, noop);
  }

  updateReplicas(num: number) {
    this.k8sApi
      .patchResource({
        type: this.workloadType,
        cluster: this.baseParams.cluster,
        resource: this.resource,
        part: { spec: { replicas: num } },
      })
      .subscribe(resource => this.stateChange.emit({ resource }));
  }

  updateLabels() {
    const dialogRef = this.dialog.open(UpdateWorkloadLabelsDialogComponent, {
      size: DialogSize.Big,
      data: {
        podController: this.resource,
        baseParams: {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
        },
      },
    });
    return dialogRef.componentInstance.close
      .pipe(
        first(),
        tap(() => {
          dialogRef.close();
        }),
      )
      .subscribe(res => {
        if (res) {
          this.notification.success(this.translate.get('update_success'));
          this.stateChange.emit();
        }
      });
  }

  getNodeSelector(resource: Workload) {
    return pathOr({}, ['spec', 'template', 'spec', 'nodeSelector'], resource);
  }

  getIpPool(resource: Workload) {
    const { template } = resource.spec;
    const ipList: string[] = (
      this.k8sUtil.getAnnotation(template, {
        type: 'ip_pool',
        baseDomain: KUBE_OVN_ANNOTATION_PREFIX,
      }) || ''
    )
      .split(',')
      .filter(i => i);
    if (ipList.length > 0) {
      return ipList;
    }
    return JSON.parse(
      this.k8sUtil.getAnnotation(template, {
        type: 'ipAddrs',
        baseDomain: CALICO_ANNOTATION_PREFIX,
      }) || '[]',
    );
  }

  getImagePullSecrets(resource: Workload) {
    return pathOr(
      [],
      ['spec', 'template', 'spec', 'imagePullSecrets'],
      resource,
    ).map(({ name }) => name);
  }

  getAffinityInfo(resource: Workload): FlattenAffinity[] {
    const items = compose(
      flatAffinity,
      pathOr({}, ['spec', 'template', 'spec', 'affinity']),
    )(resource);
    return items.length > 0 ? items : null;
  }

  updateImageTag(containerName: string) {
    this.appUtilService
      .updateImageTag({
        podController: this.resource,
        containerName,
        baseParams: this.baseParams,
      })
      .subscribe(res => {
        if (res) {
          this.stateChange.emit();
        }
      });
  }

  updateResourceSize(containerName: string) {
    this.appUtilService
      .updateResourceSize({
        podController: this.resource,
        containerName,
        baseParams: this.baseParams,
      })
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(res => {
        if (res) {
          this.stateChange.emit();
        }
      });
  }

  openTerminal(params: {
    pods: string;
    selectedPod: string;
    container: string;
  }) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (data) {
        this.terminal.openTerminal({
          ctl: this.resource.metadata.name,
          pods: params.pods,
          selectedPod: params.selectedPod,
          container: params.container,
          namespace: this.baseParams.namespace,
          cluster: this.baseParams.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  openLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 4,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  updateHpa(resource: HpaConfigResource) {
    if (resource) {
      this.stateChange.emit();
    }
  }

  isHpaAllowed(workloadType: WorkloadType) {
    return ['DEPLOYMENT', 'TAPP'].includes(workloadType);
  }
}
