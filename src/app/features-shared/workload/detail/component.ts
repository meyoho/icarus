import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  NAME,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { equals } from 'ramda';
import { combineLatest, of } from 'rxjs';
import { catchError, distinctUntilChanged, first, map } from 'rxjs/operators';

import { EnvUpdateParams } from 'app/features-shared/app/detail/env/component';
import { HpaConfigResource } from 'app/features-shared/app/detail/hpa/type';
import { UpdateEnvDialogComponent } from 'app/features-shared/app/dialog/update-env/component';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { MetricService } from 'app/services/api/metric.service';
import { Workload, WorkloadType } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { WORKLOAD_TYPE } from '../inject-tokens';
import { getBelongingApp } from '../util';

@Component({
  selector: 'rc-workload-detail',
  templateUrl: 'template.html',
  styleUrls: ['../../../../app_user/features/app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailComponent {
  @Output()
  redirect = new EventEmitter();

  baseParams = this.workspace.baseParamsSnapshot;
  tabIndex$ = this.activatedRoute.queryParamMap.pipe(
    map(params => parseInt(params.get('tabIndex'), 10) || 0),
    distinctUntilChanged(),
  );

  logInitialState$ = this.activatedRoute.queryParamMap.pipe(
    map(params => ({
      pod: params.get('pod'),
      container: params.get('container'),
    })),
  );

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspace.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{ name: string; cluster: string; namespace: string }>(
      equals,
    ),
    publishRef(),
  );

  showMetricTab$ = this.metricService.canUseMetric(this.baseParams.cluster);

  getBelongingApp = getBelongingApp;

  dataManager = new AsyncDataLoader<{
    resource: Workload;
    autoScaler: HpaConfigResource;
  }>({
    params$: this.params$,
    fetcher: params => {
      const hasHpa = this.workloadType === 'DEPLOYMENT';
      const hpa$ = hasHpa
        ? this.acpApi
            .getHpaResource({
              workloadType: this.workloadType.toLowerCase(),
              cluster: params.cluster,
              namespace: params.namespace,
              workloadName: params.name,
            })
            .pipe(
              catchError(() => {
                return of(null);
              }),
            )
        : of(null);

      const permissions$ = this.k8sPermission.isAllowed({
        type: this.workloadType,
        cluster: params.cluster,
        namespace: params.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      });

      return combineLatest([
        this.k8sApi.watchResource({
          type: this.workloadType,
          cluster: params.cluster,
          namespace: params.namespace,
          name: params.name,
        }),
        hpa$,
        permissions$,
      ]).pipe(
        map(([resource, autoScaler, permissions]) =>
          resource
            ? {
                resource,
                autoScaler,
                permissions,
              }
            : null,
        ),
      );
    },
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly workspace: WorkspaceComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly acpApi: AcpApiService,
    private readonly metricService: MetricService,
    @Inject(WORKLOAD_TYPE) private readonly workloadType: WorkloadType,
  ) {}

  tabIndexChanged(index: number) {
    const currentQueryParams = this.activatedRoute.snapshot.queryParams;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...currentQueryParams,
        tabIndex: index,
      },
    });
  }

  stateChanged(data: { resource: Workload; autoScaler?: HpaConfigResource }) {
    if (data) {
      this.dataManager.reload({ ...this.dataManager.snapshot.data, ...data });
    } else {
      this.dataManager.reload();
    }
  }

  viewLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 4,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  updateEnv(params: EnvUpdateParams) {
    const dialogRef = this.dialog.open(UpdateEnvDialogComponent, {
      size: params.type === 'env' ? DialogSize.Big : DialogSize.Medium,
      data: {
        podController: this.dataManager.snapshot.data.resource,
        baseParams: this.baseParams,
        ...params,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.message.success(this.translate.get('update_success'));
          this.dataManager.reload();
        }
        dialogRef.close();
      });
  }

  jumpToListPage() {
    this.redirect.next();
  }
}
