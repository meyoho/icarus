import { InjectionToken } from '@angular/core';

import { ResourceType } from 'app/typings';

export const WORKLOAD_TYPE = new InjectionToken<ResourceType>('WORKLOAD_TYPE');
