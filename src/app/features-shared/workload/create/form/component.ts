import {
  DISPLAY_NAME,
  KubernetesResource,
  ObservableInput,
  TOKEN_BASE_DOMAIN,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { compose, pathOr } from 'ramda';
import { Observable, combineLatest } from 'rxjs';
import { debounceTime, filter, map, startWith } from 'rxjs/operators';

import {
  copyMatchLabels,
  getWorkloadKind,
  projectSetter,
  serviceSetter,
} from 'app/features-shared/workload/util';
import {
  ConfigMap,
  RESOURCE_TYPES,
  Secret,
  Workload,
  WorkloadOption,
  WorkloadType,
} from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

@Component({
  selector: 'rc-workload-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent extends BaseResourceFormGroupComponent<Workload>
  implements OnInit {
  @Input()
  isUpdate = false;

  @Input()
  project: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  appName: string;

  @Input()
  workloadType: WorkloadType;

  @ObservableInput(true) otherWorkloads$: Observable<Workload[]>;
  @Input() otherWorkloads: Workload[];
  @Input() configMapOptions: ConfigMap[];
  @Input() secretOptions: Secret[];

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  displayNameKey = `${this.baseDomain}/${DISPLAY_NAME}`;

  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      prefixPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  workloadOptions$: Observable<WorkloadOption[]>;

  matchLabels: Record<string, string>;

  private get updateStrategyKey() {
    return this.workloadType === RESOURCE_TYPES.DEPLOYMENT
      ? 'strategy'
      : 'updateStrategy';
  }

  private setupInitialMatchLabels: (
    r: KubernetesResource,
  ) => KubernetesResource;

  constructor(
    injector: Injector,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.setupInitialMatchLabels = compose(
      copyMatchLabels(this.baseDomain),
      projectSetter(this.baseDomain, this.project),
      serviceSetter(this.baseDomain),
    );

    this.workloadOptions$ = combineLatest([
      this.otherWorkloads$.pipe(
        startWith([] as Workload[]),
        filter(r => !!r),
        map(
          list =>
            list.map(r => ({
              name: r.metadata.name,
              kind: r.kind,
            })) as WorkloadOption[],
        ),
      ),
      this.form.get('metadata.name').valueChanges.pipe(
        debounceTime(300),
        map(
          (name: string) =>
            ({
              kind: getWorkloadKind(this.workloadType),
              name: this.appName ? `${this.appName}-${name}` : name,
            } as WorkloadOption),
        ),
        startWith(null as WorkloadOption),
      ),
    ]).pipe(
      map(([options, option]) => (option ? [...options, option] : options)),
      publishRef(),
    );
  }

  createForm() {
    const specGroup = {
      template: this.fb.control({}),
      [this.updateStrategyKey]: this.fb.control({}),
    };
    if (this.workloadType !== RESOURCE_TYPES.DAEMONSET) {
      specGroup.replicas = this.fb.control(1, [
        Validators.required,
        Validators.min(0),
      ]);
    }

    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control('', [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ]),
        labels: this.fb.control({}),
        annotations: this.fb.group({
          [this.displayNameKey]: this.fb.control(''),
        }),
      }),
      spec: this.fb.group(specGroup),
    });
  }

  getDefaultFormModel(): Workload {
    return {};
  }

  adaptResourceModel(resource: Workload) {
    if (!this.matchLabels && resource) {
      if (this.isUpdate) {
        this.matchLabels = pathOr(
          {},
          ['spec', 'selector', 'matchLabels'],
          resource,
        );
      } else {
        this.matchLabels = {};
      }
    }
    return resource;
  }

  adaptFormModel(formModel: Workload) {
    return this.isUpdate ? formModel : this.setupInitialMatchLabels(formModel);
  }
}
