import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { pathOr } from 'ramda';

import {
  dropRollingUpdate,
  numberOrPercent,
} from 'app/features-shared/workload/util';
import { DaemonSetUpdateStrategy } from 'app/typings';

import { INTEGER_OR_PERCENTAGE_PATTERN } from '../deployment-strategy-form/component';

@Component({
  selector: 'rc-daemonset-strategy-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetStrategyFormComponent extends BaseResourceFormGroupComponent<
  DaemonSetUpdateStrategy,
  DaemonSetUpdateStrategy
> {
  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      type: this.fb.control('RollingUpdate'),
      rollingUpdate: this.fb.group({
        maxUnavailable: this.fb.control('', [
          Validators.pattern(INTEGER_OR_PERCENTAGE_PATTERN),
        ]),
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: DaemonSetUpdateStrategy) {
    if (!formModel.type || formModel.type === 'RollingUpdate') {
      return {
        type: 'RollingUpdate',
        rollingUpdate: {
          maxUnavailable: numberOrPercent(
            pathOr(null, ['rollingUpdate', 'maxUnavailable'], formModel),
          ),
        },
      };
    } else {
      return dropRollingUpdate(formModel);
    }
  }
}
