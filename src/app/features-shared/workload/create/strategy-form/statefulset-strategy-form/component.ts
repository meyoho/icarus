import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { pathOr } from 'ramda';

import { dropRollingUpdate } from 'app/features-shared/workload/util';
import { StatefulSetUpdateStrategy } from 'app/typings';

@Component({
  selector: 'rc-statefulset-strategy-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetStrategyFormComponent extends BaseResourceFormGroupComponent<
  StatefulSetUpdateStrategy,
  StatefulSetUpdateStrategy
> {
  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      type: this.fb.control('RollingUpdate'),
      rollingUpdate: this.fb.group({
        partition: this.fb.control(null, [Validators.min(0)]),
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: StatefulSetUpdateStrategy) {
    if (!formModel.type || formModel.type === 'RollingUpdate') {
      return {
        type: 'RollingUpdate',
        rollingUpdate: {
          partition: pathOr(0, ['rollingUpdate', 'partition'], formModel),
        },
      };
    } else {
      return dropRollingUpdate(formModel);
    }
  }
}
