import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { pathOr } from 'ramda';

import {
  dropRollingUpdate,
  numberOrPercent,
} from 'app/features-shared/workload/util';
import { DeploymentStrategy } from 'app/typings';

export const INTEGER_OR_PERCENTAGE_PATTERN = /^(0|[1-9]\d*|(100|[1-9]?\d(\.\d\d?)?)%)$/;

@Component({
  selector: 'rc-deployment-strategy-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentStrategyFormComponent extends BaseResourceFormGroupComponent<
  DeploymentStrategy,
  DeploymentStrategy
> {
  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      type: this.fb.control('RollingUpdate'),
      rollingUpdate: this.fb.group({
        maxUnavailable: this.fb.control('', [
          Validators.pattern(INTEGER_OR_PERCENTAGE_PATTERN),
        ]),
        maxSurge: this.fb.control('', [
          Validators.pattern(INTEGER_OR_PERCENTAGE_PATTERN),
        ]),
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: DeploymentStrategy) {
    if (!formModel.type || formModel.type === 'RollingUpdate') {
      return {
        type: 'RollingUpdate',
        rollingUpdate: {
          maxSurge: numberOrPercent(
            pathOr(null, ['rollingUpdate', 'maxSurge'], formModel),
          ),
          maxUnavailable: numberOrPercent(
            pathOr(null, ['rollingUpdate', 'maxUnavailable'], formModel),
          ),
        },
      };
    } else {
      return dropRollingUpdate(formModel);
    }
  }
}
