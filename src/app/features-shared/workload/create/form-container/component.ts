import { K8sApiService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
} from '@angular/core';
import { Router } from '@angular/router';
import { identity } from 'ramda';

import { ResourceUpdateService } from 'app/services/update-resource.service';
import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import { Workload, WorkloadType } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { WORKLOAD_TYPE } from '../../inject-tokens';

@Component({
  selector: 'rc-workload-form-container',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormContainerComponent extends BaseFormContainer<Workload> {
  baseParams = this.workspace.baseParamsSnapshot;

  protected fillResource = identity;

  constructor(
    injector: Injector,
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(WORKLOAD_TYPE) public workloadType: WorkloadType,
    private readonly updateService: ResourceUpdateService,
  ) {
    super(injector);
  }

  submit() {
    if (!this.checkForm()) {
      return;
    }

    this.submitting = true;
    const resource = this.isUpdate
      ? this.resource
      : this.fillResource(this.resource);
    const params = {
      type: this.workloadType,
      cluster: this.baseParams.cluster,
      resource,
    };

    (this.isUpdate
      ? this.updateService.retryPutResource(params)
      : this.k8sApi.postResource(params)
    ).subscribe(
      () => {
        this.router.navigate(
          [this.workloadType.toLowerCase(), 'detail', resource.metadata.name],
          {
            relativeTo: this.workspace.baseActivatedRoute,
            replaceUrl: true,
          },
        );
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }
}
