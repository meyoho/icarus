import {
  K8sPermissionService,
  K8sResourceAction,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { combineLatest } from 'rxjs';

import { FeatureGateApiService } from 'app/services/api/feature-gate.service';
import { RESOURCE_TYPES } from 'app/typings';

export interface ExecInfo {
  user: string;
  command: string;
}

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExecCommandDialogComponent {
  @Output()
  close = new EventEmitter<ExecInfo>();

  @ViewChild(NgForm, { static: true })
  form: NgForm;

  loading = false;
  userEnabled = false;
  isAdmin = true;
  title = 'EXEC';
  user = 'root';
  command = '/bin/sh';

  rootPattern = /^(?!root$)/;

  constructor(
    @Inject(DIALOG_DATA)
    { title, cluster, namespace }: ExecCommandDialogParams,
    featureGateApi: FeatureGateApiService,
    k8sPermission: K8sPermissionService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    if (title) {
      this.title = title;
    }

    this.loading = true;
    combineLatest([
      featureGateApi.getClusterGate(cluster, 'non-root-exec'),
      k8sPermission.isAllowed({
        type: RESOURCE_TYPES.PODS_ROOT_EXEC,
        cluster,
        namespace,
        action: K8sResourceAction.CREATE,
      }),
    ]).subscribe(
      ([gate, isAdmin]) => {
        this.userEnabled = gate.status.enabled;
        this.isAdmin = isAdmin;

        if (this.userEnabled && !this.isAdmin) {
          this.user = '';
        }

        this.loading = false;
        this.cdr.markForCheck();
      },
      () => {
        this.loading = false;
        this.cdr.markForCheck();
      },
    );
  }

  confirm() {
    this.form.onSubmit(null);

    if (!this.form.valid) {
      return;
    }

    this.close.next({
      user: this.user,
      command: this.command,
    });
  }

  cancel() {
    this.close.next(null);
  }
}

export interface ExecCommandDialogParams {
  cluster: string;
  namespace: string;
  title?: string;
}
