import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ExecCommandDialogComponent } from './exec-command/component';

const EXPORT_COMPONENTS = [ExecCommandDialogComponent];

@NgModule({
  imports: [SharedModule],
  exports: EXPORT_COMPONENTS,
  declarations: EXPORT_COMPONENTS,
})
export class ExecSharedModule {}
