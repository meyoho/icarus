import {
  API_GATEWAY,
  K8sApiService,
  NAMESPACE,
  ObservableInput,
  StringMap,
  TranslateService,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import dayjs from 'dayjs';
import { find, mergeWith, reduce } from 'lodash-es';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  combineLatest,
  of,
  timer,
} from 'rxjs';
import {
  catchError,
  filter,
  map,
  pluck,
  skipUntil,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { AdvanceApiService } from 'app/services/api/advance.service';
import { Log, Namespace, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { ACTION, TYPE, viewActions } from 'app/utils';

import { K8sEvent, K8sEventsResult } from './event.types';
import {
  COMMON_SUGGESTIONS,
  DATE_FORMAT,
  TIMESTAMP_OPTIONS,
} from './k8s-event.constant';

interface TimeRangeOption {
  name: string;
  type: string;
  offset: number;
}
@Component({
  selector: 'rc-k8s-event-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class K8sEventListComponent implements OnInit {
  @Input()
  kind: string;

  @Input()
  names: string;

  @Input()
  namespace: string;

  @ObservableInput()
  @Input('cluster')
  cluster$: Observable<string>;

  @Input()
  embedded = false;

  @Input()
  fuzzy = false;

  isDeployed$ = this.advanceApiService.getDiagnoseInfo().pipe(publishRef());

  loadingSuggestions = true;
  suggestions$ = this.cluster$.pipe(
    skipUntil(this.isDeployed$),
    tap(() => {
      this.updated$.next(null);
      this.loadingSuggestions = true;
    }),
    switchMap(cluster =>
      this.k8sApi
        .getResourceList<Namespace>({
          type: RESOURCE_TYPES.NAMESPACE,
          cluster,
        })
        .pipe(
          pluck('items'),
          catchError(() => of<Namespace[]>([])),
          map(items => items.map(item => `namespace: ${item.metadata.name}`)),
        ),
    ),
    map(clusterNamespaces => {
      return [
        ...clusterNamespaces,
        ...COMMON_SUGGESTIONS.map(type => `kind: ${type}`),
      ];
    }),
    tap(() => {
      this.loadingSuggestions = false;
      this.cdr.markForCheck();
    }),
  );

  loading = false;
  actionsConfigView = viewActions;
  timeRangeOptions: TimeRangeOption[] = TIMESTAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: this.translate.get(option.type),
    };
  });

  timeRangeOption = 'last_30_minutes';
  currentTime = dayjs();
  startTime = this.currentTime
    .startOf('minute')
    .subtract(30, 'minute')
    .format(DATE_FORMAT);

  endTime = this.currentTime.format(DATE_FORMAT);

  calendarOptions = {
    maxDate: this.currentTime.clone().endOf('day').valueOf(),
    minDate: this.currentTime
      .clone()
      .startOf('day')
      .subtract(6, 'day')
      .valueOf(),
    dateFormat: 'Y-m-d H:i:S',
    enableTime: true,
    enableSeconds: true,
  };

  columns: string[] = [
    NAMESPACE,
    TYPE,
    'resource_name',
    'time_range',
    'count',
    'reason',
    'message',
    ACTION,
  ];

  pagination = {
    count: 0,
    size: 20,
    current: 1,
    pages: 0,
  };

  conditions: string[] = [];
  queryCondition = {};

  eventDetail = {};

  updated$ = new BehaviorSubject(null);
  events$ = combineLatest([
    this.cluster$.pipe(startWith(null as string)),
    this.updated$.pipe(startWith(null as string)),
    timer(0, 60000).pipe(
      filter(
        () =>
          this.timeRangeOption !== 'custom_time_range' &&
          !this.conditions.length,
      ),
      tap(() => {
        this.resetTimeRange();
      }),
    ),
  ]).pipe(
    skipUntil(this.isDeployed$),
    switchMap(([cluster]) => {
      const startTime = this.dateValue(this.startTime);
      const endTime = this.dateValue(this.endTime);
      if (!startTime || !endTime) {
        this.auiNotificationService.warning(
          this.translate.get('log_query_timerange_required'),
        );
        return EMPTY;
      }
      if (
        this.timeRangeOption === 'custom_time_range' &&
        startTime >= endTime
      ) {
        this.auiNotificationService.warning(
          this.translate.get('log_query_timerange_warning'),
        );
        return EMPTY;
      }
      const conditions = this.generateCondition();
      this.loading = true;
      return this.httpClient.get<K8sEventsResult>(`${API_GATEWAY}/v4/events`, {
        params: {
          cluster,
          fuzzy: String(this.fuzzy),
          start_time: String(startTime),
          end_time: String(endTime),
          ...conditions,
          pageno: String(this.pagination.current),
          size: String(this.pagination.size),
        },
      });
    }),
    tap(res => {
      this.pagination.count = res.total_items > 10000 ? 10000 : res.total_items;
      this.pagination.pages = res.total_page;
    }),
    map(res => {
      if (Array.isArray(res) || !res.items) {
        return [];
      }
      return res.items.map(item => item.spec);
    }),
    tap(() => {
      this.loading = false;
    }),
    skipError(),
  );

  constructor(
    private readonly advanceApiService: AdvanceApiService,
    private readonly httpClient: HttpClient,
    private readonly translate: TranslateService,
    private readonly dialogService: DialogService,
    private readonly auiNotificationService: NotificationService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}

  ngOnInit() {
    if (this.namespace) {
      this.columns.splice(0, 1);
    }
    this.k8sApi
      .getGlobalResource<Log>({
        type: RESOURCE_TYPES.LOG,
        namespaced: true,
        name: 'alauda-es-config',
      })
      .subscribe(logConfigs => {
        if (logConfigs.spec.EVENT_TTL) {
          this.calendarOptions.minDate = this.currentTime
            .clone()
            .startOf('day')
            .subtract(logConfigs.spec.EVENT_TTL, 'day')
            .valueOf();
        }
      });
  }

  trackByFn(_index: number, item: K8sEvent) {
    return item.detail.event.metadata.uid;
  }

  onTimeRangeChanged(value: string) {
    const option: TimeRangeOption = find(this.timeRangeOptions, {
      type: value,
    });
    if (option.type !== 'custom_time_range') {
      this.resetTimeRange();
      this.pagination.current = 1;
      this.updated$.next(null);
    }
  }

  reloadEvent() {
    if (this.timeRangeOption !== 'custom_time_range') {
      this.resetTimeRange();
      this.updated$.next(null);
    }
  }

  private resetTimeRange() {
    const timeRange = TIMESTAMP_OPTIONS.find(
      option => option.type === this.timeRangeOption,
    );
    this.endTime = dayjs().format(DATE_FORMAT);
    this.startTime = dayjs()
      .startOf('second')
      .subtract(timeRange.offset, 'ms')
      .format(DATE_FORMAT);
  }

  onStartTimeSelect(time: string) {
    this.startTime = time;
    this.pagination.current = 1;
    this.updated$.next(null);
  }

  onEndTimeSelect(time: string) {
    this.endTime = time;
    this.pagination.current = 1;
    this.updated$.next(null);
  }

  setConditions(_conditions: string[]) {
    this.pagination.current = 1;
    this.updated$.next(null);
  }

  private generateCondition(): {
    kind?: string;
    namespace?: string;
    name?: string;
  } {
    const conditionMap = this.conditions.map(condition => {
      const param: StringMap = {};
      const match = /^(.+):\s(.+)$/.exec(condition);
      if (match && match[0]) {
        const value = (match[2] || '').trim();
        param[match[1]] = value;
      } else {
        param.name = condition;
      }
      return param;
    });
    const ret = reduce(conditionMap, (result, param) =>
      mergeWith(result, param, (pre, cur) => {
        if (pre) {
          return [pre, cur].join(',');
        } else {
          return cur;
        }
      }),
    );
    return ret ? this.addParams(ret) : this.addParams({});
  }

  private addParams(ret: StringMap) {
    if (this.kind) {
      ret.kind = ret.kind ? `${ret.kind},${this.kind}` : this.kind;
    }
    if (this.names) {
      ret.name = ret.name ? `${ret.name},${this.names}` : this.names;
    }
    if (this.namespace) {
      ret.namespace = ret.namespace
        ? `${ret.namespace},${this.namespace}`
        : this.namespace;
    }
    return ret;
  }

  currentPageChange(_currentPage: number) {
    this.updated$.next(null);
  }

  pageSizeChange(size: number) {
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.updated$.next(null);
  }

  showDetailTemplate(item: K8sEvent, template: TemplateRef<any>) {
    this.eventDetail = item;
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private dateValue(date: string) {
    return dayjs(date).valueOf() / 1000;
  }
}
