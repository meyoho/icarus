/**
 * K8s event
 *
 * @export
 * @interface K8sEvent
 */
export interface K8sEvent {
  resource_id: string;
  log_level: string;
  detail: {
    source: string;
    cluster_name: string;
    operator: string;
    cluster_uuid: string;
    operation: string;
    event: {
      count: string;
      firstTimestamp: string;
      lastTimestamp: string;
      source: {
        host: string;
        component: string;
      };
      reason: string;
      involvedObject: {
        kind: string;
        name: string;
        namespace: string;
        apiVersion: string;
        fieldPath: string;
        resourceVersion: string;
        uid: string;
      };
      message: string;
      type: string;
      metadata: {
        uid: string;
        namespace: string;
        resourceVersion: string;
        creationTimestamp: string;
        selfLink: string;
        name: string;
      };
    };
  };
  resource_type: string;
  time: string;
}

/**
 * K8s event query
 *
 * @export
 * @interface K8sEventQuery
 */
export interface K8sEventQuery {
  start_time: number;
  end_time: number;
  cluster: string;
  kind?: string;
  name?: string;
  fuzzy?: boolean;
  namespace?: string;
  pageno?: number;
  size?: number;
  filters?: string;
}

export interface K8sEventsResult {
  count: number;
  total_page: number;
  page_size: number;
  next: string;
  previous: string;
  total_items: number;
  items: Array<{
    spec: K8sEvent;
  }>;
}
