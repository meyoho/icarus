import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { K8sEventListComponent } from './k8s-event-list/component';
@NgModule({
  imports: [SharedModule],
  declarations: [K8sEventListComponent],
  exports: [K8sEventListComponent],
})
export class EventSharedModule {}
