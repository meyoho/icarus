import { TopologyResponse } from '../../typings/backend-api';

export function findRefs(
  graph: TopologyResponse,
  source: { kind: string; name: string },
) {
  const pair = Object.entries(graph.nodes).find(([_, value]) => {
    return value.kind === source.kind && value.metadata.name === source.name;
  });

  if (!pair) {
    return [];
  }

  const [id] = pair;
  return graph.edges
    .filter(({ type, to }) => type === 'Reference' && to === id)
    .map(({ from }) => {
      return graph.nodes[from];
    });
}
