import { KubernetesResource } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';

import { SafeDeleteParams } from '../types';
import { findRefs } from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RefsWarningComponent {
  refs: KubernetesResource[] = [];
  handleDelete: () => Observable<void | KubernetesResource>;
  resource: KubernetesResource;

  loading = false;

  constructor(
    @Inject(DIALOG_DATA)
    { cluster, resource, handleDelete }: SafeDeleteParams,
    acpApi: AcpApiService,
    private readonly dialogRef: DialogRef,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.loading = true;
    acpApi
      .getTopologyByResource({
        cluster,
        namespace: resource.metadata.namespace,
        name: resource.metadata.name,
        kind: resource.kind.toLowerCase(),
      })
      .pipe(
        map(graph =>
          findRefs(graph, {
            kind: resource.kind,
            name: resource.metadata.name,
          }),
        ),
      )
      .subscribe(
        refs => {
          this.refs = refs;
          this.loading = false;
          this.cdr.markForCheck();
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );

    this.resource = resource;
    this.handleDelete = handleDelete;
  }

  delete() {
    this.loading = true;
    this.handleDelete().subscribe(
      () => {
        this.dialogRef.close(true);
      },
      () => {
        this.loading = false;
        this.cdr.markForCheck();
      },
    );
  }
}
