import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { RefsWarningComponent } from './refs-warning/component';
import { RefsService } from './refs.service';

@NgModule({
  imports: [SharedModule],
  declarations: [RefsWarningComponent],
  providers: [RefsService],
  exports: [RefsWarningComponent],
})
export class ResourceReferenceSharedModule {}
