import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { RefsWarningComponent } from './refs-warning/component';
import { SafeDeleteParams } from './types';

@Injectable()
export class RefsService {
  constructor(private readonly dialog: DialogService) {}

  safeDelete(params: SafeDeleteParams) {
    return this.dialog
      .open<RefsWarningComponent, SafeDeleteParams>(RefsWarningComponent, {
        data: params,
      })
      .afterClosed()
      .pipe(
        switchMap(deleted => {
          return deleted ? of(null) : throwError(null);
        }),
      );
  }
}
