import { KubernetesResource } from '@alauda/common-snippet';
import { Observable } from 'rxjs';

export interface SafeDeleteParams<T = KubernetesResource> {
  cluster: string;
  resource: T;
  handleDelete: () => Observable<void | T>;
}
