import { NAME } from '@alauda/common-snippet';

import { Volume, VolumeTypeEnum } from 'app/typings';

export const AVAILABLE_VOLUME_TYPES = Object.keys(VolumeTypeEnum);

export function getVolumeType(volume: Volume) {
  return Object.keys(volume).find(key => key !== NAME) as VolumeTypeEnum;
}

export function getVolumeTypeTranslateKey(type: VolumeTypeEnum) {
  const keyMap = {
    [VolumeTypeEnum.emptyDir]: 'empty_dir',
    [VolumeTypeEnum.configMap]: 'configmap',
    [VolumeTypeEnum.secret]: 'secret',
    [VolumeTypeEnum.hostPath]: 'host_path',
    [VolumeTypeEnum.persistentVolumeClaim]: 'persistentvolumeclaim',
  };
  return keyMap[type];
}
