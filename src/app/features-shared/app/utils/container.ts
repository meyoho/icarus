import { StringMap, TranslateService } from '@alauda/common-snippet';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { endsWith, get } from 'lodash-es';

import {
  Container,
  CronJob,
  ResourceRequirements,
  Workload,
} from 'app/typings';

export function getContainerNamesValidatorFn(
  namesFn: () => string[],
): ValidatorFn {
  return (control: AbstractControl) => {
    const names = namesFn();
    if (names && control.value && names.includes(control.value)) {
      return {
        name_existed: true,
      };
    } else {
      return null;
    }
  };
}

export function getDefaultContainerConfig(params: {
  name: string;
  image: string;
  defaultLimit: StringMap;
}): Container {
  return {
    name: params.name,
    image: params.image,
    resources: {
      limits: params.defaultLimit,
    },
  };
}

export function formatContainerCPU(
  rawData: string,
  translateService: TranslateService,
) {
  if (!rawData) {
    return '-';
  }
  return endsWith(rawData, 'm')
    ? rawData
    : rawData + translateService.get('unit_core');
}

export function getResourceRequirementsAsLabels(
  resource: ResourceRequirements,
  translate: TranslateService,
) {
  const labels: StringMap = {
    [translate.get('cpu')]: formatContainerCPU(
      get(resource, 'limits.cpu'),
      translate,
    ),
    [translate.get('memory')]: get(resource, 'limits.memory', '-'),
  };
  Object.keys(get(resource, 'limits', {}))
    .filter(key => !['cpu', 'memory'].includes(key))
    .forEach(key => {
      labels[key] = resource.limits[key];
    });
  return labels;
}

export function getContainerPath(
  resource: CronJob | Workload,
  podSpecPath: string,
  containerName: string,
) {
  const podSpec = get(resource, podSpecPath);
  return (podSpec.initContainers || []).some(
    (c: Container) => c.name === containerName,
  )
    ? [...podSpecPath.split('.'), 'initContainers'].join('.')
    : [...podSpecPath.split('.'), 'containers'].join('.');
}
