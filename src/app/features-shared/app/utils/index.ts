import { KubernetesResource, StringMap } from '@alauda/common-snippet';

import { ConfigMap, Secret, WorkspaceBaseParams } from 'app/typings';

export * from './container';
export * from './env-var';
export * from './env-from';
export * from './monitor';
export * from './pod-controller';
export * from './volume';

export const SupportedLinkResourceType = [
  'Deployment',
  'StatefulSet',
  'DaemonSet',
  'ConfigMap',
  'Secret',
  'Ingress',
  'Service',
  'CronJob',
  'PersistentVolumeClaim',
];

export const ResourceKindToRouteMap: StringMap = {
  CronJob: 'cron_job',
  PersistentVolumeClaim: 'pvc',
};

export function getConfigMapKeys(item: ConfigMap) {
  return item ? Object.keys(Object.assign({}, item.data, item.binaryData)) : [];
}

export function getSecretKeys(item: Secret) {
  return item ? Object.keys(Object.assign({}, item.data, item.stringData)) : [];
}

export function unionMultiRegex(...regexStrs: string[]) {
  return bracketString(regexStrs.join('|'));
}
export function bracketString(str: string) {
  return `(${str})`;
}

export function getMergedResourceOptions(
  items: KubernetesResource[],
  options: KubernetesResource[],
) {
  const newItems = items.slice();
  if (options) {
    options.forEach(option => {
      if (!newItems.find(r => r.metadata.name === option.metadata.name)) {
        newItems.push(option);
      }
    });
  }
  return newItems;
}

export function isResourceLinkSupported(resource: KubernetesResource) {
  return SupportedLinkResourceType.includes(resource.kind);
}

export function getResourceRouterLink(
  resource: KubernetesResource,
  baseParams: WorkspaceBaseParams,
) {
  const links = ['/workspace', baseParams];
  if (SupportedLinkResourceType.includes(resource.kind)) {
    if (ResourceKindToRouteMap[resource.kind]) {
      return [
        ...links,
        ResourceKindToRouteMap[resource.kind],
        'detail',
        resource.metadata.name,
      ];
    } else {
      return [
        ...links,
        resource.kind.toLowerCase(),
        'detail',
        resource.metadata.name,
      ];
    }
  } else {
    return null;
  }
}
