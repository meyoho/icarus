import {
  ConfigMapKeyRef,
  EnvVar,
  EnvVarSource,
  ObjectFieldSelector,
  ResourceFieldSelector,
  Secret,
  SecretKeyRef,
  SecretType,
} from 'app/typings';

export const ENV_ENABLED_SECRET_TYPES = [
  SecretType.Opaque,
  SecretType.BasicAuth,
];

// refers to: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#envvarsource-v1-core
export const FIELD_REF_FIELDS = [
  'metadata.name',
  'metadata.namespace',
  'spec.nodeName',
  'spec.serviceAccountName',
  'status.hostIP',
  'status.podIP',
];

export const RESOURCE_FIELD_REF_FIELDS = [
  'limits.cpu',
  'limits.memory',
  'requests.cpu',
  'requests.memory',
];

export type SupportedEnvVarSourceType = 'configMapKeyRef' | 'secretKeyRef';
export type SupportedEnvVarSourceKind = 'Secret' | 'ConfigMap';

export const SUPPORTED_ENV_SOURCE_KEY_TYPES = [
  'configMapKeyRef',
  'secretKeyRef',
  'fieldRef',
  'resourceFieldRef',
];

export const KIND_TO_ENV_VAR_SOURCE_TYPE: {
  [key: string]: SupportedEnvVarSourceType;
} = {
  Secret: 'secretKeyRef',
  ConfigMap: 'configMapKeyRef',
};

export const ENV_VAR_SOURCE_TYPE_TO_KIND: {
  [key: string]: SupportedEnvVarSourceKind;
} = {
  secretKeyRef: 'Secret',
  configMapKeyRef: 'ConfigMap',
};

export function isEnvVarSourceMode(envVar: EnvVar) {
  return !!envVar && !!envVar.valueFrom;
}

export function isEnvVarSourceSupported(envVar: EnvVar) {
  if (isEnvVarSourceMode(envVar)) {
    if (getEnvVarSourceType(envVar.valueFrom)) {
      return SUPPORTED_ENV_SOURCE_KEY_TYPES.includes(
        getEnvVarSourceType(envVar.valueFrom),
      );
    } else {
      return true;
    }
  } else {
    return false;
  }
}

export function getEnvVarSourceType(
  envVarSource: EnvVarSource,
): SupportedEnvVarSourceType {
  return Object.keys(envVarSource)[0] as SupportedEnvVarSourceType;
}

export function getEnvVarSourceKind(
  envVarSource: EnvVarSource,
): SupportedEnvVarSourceKind {
  return ENV_VAR_SOURCE_TYPE_TO_KIND[getEnvVarSourceType(envVarSource)];
}

export function getEnvVarSource(
  envVar: EnvVar,
): // tslint:disable-next-line: max-union-size
ConfigMapKeyRef | SecretKeyRef | ObjectFieldSelector | ResourceFieldSelector {
  return (
    envVar.valueFrom && envVar.valueFrom[getEnvVarSourceType(envVar.valueFrom)]
  );
}

export function getEnvSecrets(items: Secret[]): Secret[] {
  return items
    ? items.filter(item => ENV_ENABLED_SECRET_TYPES.includes(item.type))
    : [];
}

export function getEnvVarSourceObj(field: string, key: string) {
  if (field === 'fieldRef') {
    return {
      fieldPath: key,
    };
  } else if (field === 'resourceFieldRef') {
    return {
      resource: key,
      divisor: getResourceFieldRefDivisor(key),
    };
  } else {
    return {};
  }
}

function getResourceFieldRefDivisor(key: string) {
  if (['limits.cpu', 'requests.cpu'].includes(key)) {
    return '1m';
  } else if (['limits.memory', 'requests.memory'].includes(key)) {
    return '1Mi';
  } else {
    return '1';
  }
}
