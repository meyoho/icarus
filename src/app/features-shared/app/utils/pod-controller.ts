import {
  DaemonSetUpdateStrategyType,
  DeploymentStrategyType,
  RollingUpdateDaemonSet,
  RollingUpdateDeployment,
  RollingUpdateStatefulSetStrategy,
  StatefulSetUpdateStrategyType,
} from 'app/typings';

export enum PodControllerKindEnum {
  Deployment = 'Deployment',
  StatefulSet = 'StatefulSet',
  DaemonSet = 'DaemonSet',
}

export enum PodControllerUpdateStrategyEnum {
  OnDelete = 'OnDelete',
  RollingUpdate = 'RollingUpdate',
}

export const PodControllerKinds = Object.keys(PodControllerKindEnum);
export const PodControllerUpdateStrategyOptions = Object.keys(
  PodControllerUpdateStrategyEnum,
);

export type PodControllerUpdateStrategyType =
  | DeploymentStrategyType
  | DaemonSetUpdateStrategyType
  | StatefulSetUpdateStrategyType;

export type PodControllerRollingUpdateType =
  | RollingUpdateDeployment
  | RollingUpdateDaemonSet
  | RollingUpdateStatefulSetStrategy;
