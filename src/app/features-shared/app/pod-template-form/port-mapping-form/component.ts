import { ObservableInput, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { ControlContainer, ValidatorFn, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { ContainerPort, SERVICE_PORT_BASE_PROTOCOLS } from 'app/typings';
import { POSITIVE_INT_PATTERN, rowBackgroundColorFn } from 'app/utils';

const DUPLICATE_ERROR_MSGS = [
  'duplicate_container_port_error_message',
  'duplicate_host_port_error_message',
];

@Component({
  selector: 'rc-port-mapping-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortMappingComponent
  extends BaseResourceFormArrayComponent<ContainerPort>
  implements OnInit, OnDestroy {
  private readonly onDestroy$ = new Subject<void>();
  rowBackgroundColorFn = rowBackgroundColorFn;
  SERVICE_PORT_BASE_PROTOCOLS = SERVICE_PORT_BASE_PROTOCOLS;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  constructor(
    public injector: Injector,
    private readonly translate: TranslateService,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  @Input()
  hostNetwork: boolean;

  @ObservableInput(true)
  hostNetwork$: Observable<boolean>;

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  ngOnInit() {
    super.ngOnInit();
    combineLatest([this.hostNetwork$, this.form.valueChanges])
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(([hostNetwork, _]) => {
        if (hostNetwork) {
          this.form.controls.forEach(control => {
            control.get('hostPort').disable({ emitEvent: false });
          });
        } else {
          this.form.controls.forEach(control => {
            control.get('hostPort').enable({ emitEvent: false });
          });
        }
      });
  }

  adaptFormModel(formModel: ContainerPort[]) {
    if (this.hostNetwork) {
      return formModel.map(port => ({
        protocol: port.protocol,
        containerPort: +port.containerPort,
      }));
    }
    return formModel.map(port =>
      port.hostPort
        ? {
            protocol: port.protocol,
            hostPort: +port.hostPort,
            containerPort: +port.containerPort,
          }
        : { protocol: port.protocol, containerPort: +port.containerPort },
    );
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): ContainerPort[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    const duplicateContainerPortValidator = this.createDuplicateValidator();
    return () =>
      this.fb.group(
        {
          protocol: this.fb.control('', [Validators.required]),
          containerPort: this.fb.control('', [Validators.required]),
          hostPort: this.fb.control(''),
        },
        {
          validators: [duplicateContainerPortValidator],
        },
      );
  }

  private createDuplicateValidator(): ValidatorFn {
    return control => {
      const index = this.form.controls.indexOf(control);
      let duplicateContainerPort = false;
      let duplicateHostPort = false;
      const previousPorts = this.formModel.slice(0, index);
      // '' / null => NaN，让未输入字段彼此不判定重复
      const containerPort = parseInt(control.value.containerPort);
      const hostPort = parseInt(control.value.hostPort);
      previousPorts.forEach(port => {
        if (+port.containerPort === containerPort) {
          duplicateContainerPort = true;
        }
        if (+port.hostPort === hostPort) {
          duplicateHostPort = true;
        }
      });
      const hasError = duplicateContainerPort || duplicateHostPort;
      const errMsgs$ = this.translate.locale$.pipe(
        map(_ =>
          [duplicateContainerPort, duplicateHostPort]
            .map((val, index) => {
              if (val) {
                return this.translate.get(DUPLICATE_ERROR_MSGS[index]);
              }
              return null;
            })
            .filter(i => !!i),
        ),
        takeUntil(this.onDestroy$),
      );
      return hasError
        ? {
            duplicateError: errMsgs$,
          }
        : null;
    };
  }
}
