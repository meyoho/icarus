import { noop } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { get } from 'lodash-es';
import { first } from 'rxjs/operators';

import { HTTPGetAction, Probe } from 'app/typings';

import { HealthcheckDialogComponent } from './container-healthcheck-dialog.component';

interface ContainerProbes {
  livenessProbe?: Probe;
  readinessProbe?: Probe;
}

interface ProbeFormModel {
  protocol: string;
  initialDelaySeconds: number;
  periodSeconds: number;
  timeoutSeconds: number;
  successThreshold: number;
  failureThreshold: number;
  scheme: string;
  port: number;
  path: string;
  headers: Array<{
    name?: string;
    value?: string;
  }>;
  commands: Array<{
    command: string;
  }>;
}
@Component({
  selector: 'rc-healthcheck-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HealthcheckFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HealthcheckFormComponent implements ControlValueAccessor {
  private readonly cdr: ChangeDetectorRef;
  private readonly dialogService: DialogService;
  probesEntities: ContainerProbes = {};
  probeTypes = ['livenessProbe', 'readinessProbe'];

  constructor(private readonly injector: Injector) {
    // tslint:disable-next-line: deprecation
    this.cdr = this.injector.get(ChangeDetectorRef);
    this.dialogService = this.injector.get(DialogService);
  }

  onChange() {
    this.cdr.markForCheck();
    this.onCvaChange({ ...this.probesEntities });
  }

  onCvaChange = noop;
  onCvaTouched = noop;
  onValidatorChange = noop;

  registerOnChange(fn: (value: ContainerProbes) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_resource: ContainerProbes) {
    this.probesEntities = _resource || {};
    this.cdr.markForCheck();
  }

  updateItem(type: keyof ContainerProbes) {
    const dialogRef = this.dialogService.open(HealthcheckDialogComponent, {
      size: DialogSize.Big,
      data: {
        formModel: this.getContainerProbeFormModel(this.probesEntities[type]),
      },
    });
    return dialogRef.componentInstance.finish
      .pipe(first())
      .subscribe((data: ProbeFormModel) => {
        dialogRef.close();
        if (data) {
          this.probesEntities[type] = this.getContainerProbeDefinition(data);
        }
        this.onChange();
      });
  }

  removeItem(type: keyof ContainerProbes) {
    this.probesEntities[type] = null;
    this.onChange();
  }

  addItem(type: keyof ContainerProbes) {
    const dialogRef = this.dialogService.open(HealthcheckDialogComponent, {
      size: DialogSize.Big,
      data: {
        type,
      },
    });
    return (
      dialogRef.componentInstance.finish
        .pipe(first())
        // eslint-disable-next-line sonarjs/no-identical-functions
        .subscribe((data: ProbeFormModel) => {
          dialogRef.close();
          if (data) {
            this.probesEntities[type] = this.getContainerProbeDefinition(data);
          }
          this.onChange();
        })
    );
  }

  trackByType(_index: number, type: string) {
    return type;
  }

  private getContainerProbeDefinition(item: ProbeFormModel): Probe {
    const data: Probe = {
      initialDelaySeconds: item.initialDelaySeconds,
      periodSeconds: item.periodSeconds,
      timeoutSeconds: item.timeoutSeconds,
      successThreshold: item.successThreshold,
      failureThreshold: item.failureThreshold,
    };

    switch (item.protocol) {
      case 'HTTP': {
        delete item.commands;
        // filter invalid header before output
        // headers: [{name: '', value: ''}, {name: '', value: 'no-name'}] --> headers: []
        item.headers = item.headers && item.headers.filter(({ name }) => name);
        const httpGet: HTTPGetAction = {
          path: item.path,
          scheme: item.scheme,
          port: item.port,
        };
        if (item.headers && item.headers.length > 0) {
          httpGet.httpHeaders = item.headers;
        }
        data.httpGet = httpGet;
        break;
      }
      case 'TCP': {
        delete item.scheme;
        delete item.path;
        delete item.headers;
        delete item.commands;
        data.tcpSocket = {
          port: item.port,
        };
        break;
      }
      case 'EXEC': {
        delete item.scheme;
        delete item.path;
        delete item.port;
        delete item.headers;
        if (item.commands && item.commands.length > 0) {
          data.exec = {
            command: item.commands.map(
              ({ command }: { command: string }) => command,
            ),
          };
        }
        break;
      }
    }
    return data;
  }

  private getContainerProbeFormModel(data: Probe): ProbeFormModel {
    if (!data) {
      return null;
    }
    const hc: any = {
      initialDelaySeconds: data.initialDelaySeconds,
      periodSeconds: data.periodSeconds,
      timeoutSeconds: data.timeoutSeconds,
      successThreshold: data.successThreshold || 0,
      failureThreshold: data.failureThreshold || 0,
    };
    if (data.tcpSocket) {
      hc.protocol = 'TCP';
      hc.port = data.tcpSocket.port;
    } else if (data.httpGet) {
      hc.protocol = 'HTTP';
      hc.port = data.httpGet.port;
      hc.scheme = data.httpGet.scheme;
      hc.path = data.httpGet.path;
      hc.headers = get(data, 'httpGet.httpHeaders', []);
    } else {
      hc.protocol = 'EXEC';
      hc.commands = (get(data, 'exec.command', []) as string[]).map(
        command => ({
          command,
        }),
      );
    }
    return hc;
  }
}
