import { noop } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import jsyaml from 'js-yaml';
import { cloneDeep, get, isEqual, remove, uniqWith } from 'lodash-es';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import { catchError, first, map, startWith } from 'rxjs/operators';

import {
  Affinity,
  AffinityTerm,
  PodAffinityTerm,
  PodAffinityType,
  PodSpecAffinity,
  WeightedPodAffinityTerm,
  WorkloadOption,
} from 'app/typings';

import { PodAffinityDialogComponent } from '../affinity-dialog/component';

interface AffinityItem {
  isAnti: boolean;
  val: AffinityTerm;
}

@Component({
  selector: 'rc-affinity-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AffinityContainerFormComponent),
      multi: true,
    },
  ],
})
export class AffinityContainerFormComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input() workloadOptions: WorkloadOption[] = [];

  affinity: PodSpecAffinity = {
    podAntiAffinity: {
      requiredDuringSchedulingIgnoredDuringExecution: [],
      preferredDuringSchedulingIgnoredDuringExecution: [],
    },
    podAffinity: {
      requiredDuringSchedulingIgnoredDuringExecution: [],
      preferredDuringSchedulingIgnoredDuringExecution: [],
    },
  };

  expLabelsViewerOptions = {
    language: 'yaml',
    folding: false,
    minimap: { enabled: false },
    readOnly: true,
  };

  expLabelsViewerActionsConfig = {
    diffMode: false,
    clear: false,
    recover: false,
    import: false,
    copy: false,
    find: false,
    export: false,
    fullscreen: false,
    theme: false,
  };

  podAffinities$: Observable<AffinityItem[]>;
  affinitySubject$ = new Subject<Affinity>();
  antiAffinitySubject$ = new Subject<Affinity>();

  constructor(
    private readonly dialogService: DialogService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  onChange() {
    this.onCvaChange(this.affinity);
    this.updateAffinityTable();
  }

  onCvaChange = noop;
  onCvaTouched = noop;
  onValidatorChange = noop;

  registerOnChange(fn: (value: PodSpecAffinity) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(resource: PodSpecAffinity) {
    if (resource) {
      this.affinity = resource;
    }
    if (this.affinity && this.affinity.podAffinity) {
      this.affinitySubject$.next(this.affinity.podAffinity);
    }
    if (this.affinity && this.affinity.podAntiAffinity) {
      this.antiAffinitySubject$.next(this.affinity.podAntiAffinity);
    }
    this.updateAffinityTable();
    this.cdr.markForCheck();
  }

  ngOnInit() {
    this.podAffinities$ = combineLatest([
      this.affinitySubject$,
      this.antiAffinitySubject$,
    ])
      .pipe(
        map(([affinity, antiAffinity]) => [
          ...(affinity[PodAffinityType.required] || []).map(val => ({
            val,
            isAnti: false,
          })),
          ...(affinity[PodAffinityType.preferred] || []).map(val => ({
            val,
            isAnti: false,
          })),
          ...(antiAffinity[PodAffinityType.required] || []).map(val => ({
            val,
            isAnti: true,
          })),
          ...(antiAffinity[PodAffinityType.preferred] || []).map(val => ({
            val,
            isAnti: true,
          })),
        ]),
        startWith([]),
      )
      .pipe(catchError(() => of([])));
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  podAffinity(
    mode = 'add',
    rawData: {
      type: string;
      isAnti: boolean;
      val: AffinityTerm;
    },
  ) {
    const dialogRef = this.dialogService.open(PodAffinityDialogComponent, {
      size: DialogSize.Big,
      data: {
        mode,
        workloadOptions: this.workloadOptions,
        cluster: this.cluster,
        namespace: this.namespace,
        ...rawData,
      },
    });
    dialogRef.componentInstance.finish.pipe(first()).subscribe(res => {
      dialogRef.close();
      const affinityType = get(res.data, 'weight')
        ? PodAffinityType.preferred
        : PodAffinityType.required;
      const type = res.type === 'affinity' ? 'podAffinity' : 'podAntiAffinity';
      if (!this.affinity[type]) {
        this.affinity[type] = {
          [affinityType]: [],
        };
      } else if (!this.affinity[type][affinityType]) {
        this.affinity[type][affinityType] = [];
      }
      let val = cloneDeep(this.affinity[type][affinityType]) as Array<
        PodAffinityTerm | WeightedPodAffinityTerm
      >; // 已存在的items
      const isExist = val.some(oldItem => isEqual(oldItem, res.data));
      if (mode === 'add') {
        if (!isExist) {
          this.affinity[type][affinityType] = [
            ...val,
            res.data,
          ] as PodAffinityTerm[] & WeightedPodAffinityTerm[];
        } else {
          return;
        }
      } else {
        val.forEach((item, index) => {
          if (isEqual(item, rawData.val)) {
            val[index] = res.data;
          }
        });
        if (isExist) {
          // 如果有值重复，去重
          val = uniqWith(val, isEqual);
        }
        this.affinity[type][affinityType] = val as PodAffinityTerm[] &
          WeightedPodAffinityTerm[];
      }
      this.onChange();
    });
  }

  updateAffinityTable() {
    this.affinitySubject$.next(this.affinity.podAffinity || {});
    this.antiAffinitySubject$.next(this.affinity.podAntiAffinity || {});
  }

  private formToYaml(formModel: unknown) {
    try {
      return jsyaml.safeDump(formModel);
    } catch (err) {}
  }

  getExpLabels(rowData: AffinityItem) {
    return this.formToYaml(
      get(rowData.val as WeightedPodAffinityTerm, 'weight')
        ? (rowData.val as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
            .matchExpressions
        : (rowData.val as PodAffinityTerm).labelSelector.matchExpressions,
    );
  }

  trackByFn(index: number) {
    return index;
  }

  getLabels(rowData: AffinityItem) {
    return Object.entries(
      get(
        rowData.val,
        get(rowData.val, 'weight')
          ? 'podAffinityTerm.labelSelector.matchLabels'
          : 'labelSelector.matchLabels',
        [],
      ),
    );
  }

  affinityCanUpdate(rowData: AffinityItem) {
    return get(rowData.val as WeightedPodAffinityTerm, 'weight')
      ? !(rowData.val as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
          .matchExpressions
      : !(rowData.val as PodAffinityTerm).labelSelector.matchExpressions;
  }

  removeAffinity(rowData: AffinityItem) {
    const val = cloneDeep(this.affinity);
    remove(
      val[rowData.isAnti ? 'podAntiAffinity' : 'podAffinity'][
        get(rowData.val, 'weight')
          ? PodAffinityType.preferred
          : PodAffinityType.required
      ] as AffinityTerm[],
      item => isEqual(rowData.val, item),
    );
    this.affinity = val;
    this.onChange();
  }

  trackByIndex(index: number) {
    return index;
  }
}
