import {
  KubernetesResource,
  ObservableInput,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { OptionComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';

import {
  ConfigMap,
  ConfigMapKeyRef,
  EnvVar,
  ObjectFieldSelector,
  ResourceFieldSelector,
  Secret,
  WorkspaceBaseParams,
} from 'app/typings';
import { envVarNameValidator } from 'app/utils';
import { rowBackgroundColorFn } from 'app/utils/helpers';
import { K8S_ENV_VARIABLE_NAME } from 'app/utils/patterns';

import {
  ENV_VAR_SOURCE_TYPE_TO_KIND,
  FIELD_REF_FIELDS,
  KIND_TO_ENV_VAR_SOURCE_TYPE,
  RESOURCE_FIELD_REF_FIELDS,
  SupportedEnvVarSourceKind,
  SupportedEnvVarSourceType,
  getEnvSecrets,
  getEnvVarSource,
  getEnvVarSourceObj,
  getEnvVarSourceType,
  isEnvVarSourceMode,
  isEnvVarSourceSupported,
} from '../../utils/env-var';

interface EnvRefObj {
  kind?: SupportedEnvVarSourceKind; // One of Secret, configMap
  name: string;
}

interface EnvVarFormModel extends EnvVar {
  refObj?: EnvRefObj;
  refObjKey?: string;
}

const keyPatternValidator: ValidatorFn = control => {
  const nameControl = (control as FormGroup).get('name');
  if (
    nameControl.invalid &&
    nameControl.errors &&
    nameControl.errors.envVarNamePattern
  ) {
    return {
      keyPatternInvalid: true,
    };
  } else {
    return null;
  }
};

const valueRequiredValidator: ValidatorFn = control => {
  const { name, valueFrom, refObjKey } = control.value;
  if (name && !!valueFrom && !refObjKey) {
    return {
      refObjKeyMissing: true,
    };
  } else {
    return null;
  }
};

const missingKeyValidator: ValidatorFn = control => {
  const { name, value, refObj } = control.value;

  if (!name && (value || (refObj && refObj.name))) {
    return {
      keyIsMissing: true,
    };
  } else {
    return null;
  }
};

@Component({
  selector: 'rc-env-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFormComponent extends BaseResourceFormArrayComponent<
  EnvVar,
  EnvVarFormModel
> {
  @ObservableInput(true) baseParams$: Observable<WorkspaceBaseParams>;
  @Input()
  baseParams: WorkspaceBaseParams;

  @Input() configMaps: ConfigMap[];

  @Input() secrets: Secret[];

  getEnvSecrets = getEnvSecrets;
  K8S_ENV_VARIABLE_NAME = K8S_ENV_VARIABLE_NAME;

  constructor(
    private readonly translateService: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  rowBackgroundColorFn = rowBackgroundColorFn;

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [] as any;
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(envVars: EnvVar[]) {
    if (!envVars || envVars.length === 0) {
      envVars = this.getDefaultFormModel();
    }

    // Fill in keyRefObj when applied:
    return envVars.map((envVar: EnvVar) => {
      if (isEnvVarSourceSupported(envVar)) {
        const envVarSource = getEnvVarSource(envVar);
        const envVarSourceType = getEnvVarSourceType(envVar.valueFrom);
        return {
          ...envVar,
          refObj: {
            kind: ['configMapKeyRef', 'secretKeyRef'].includes(envVarSourceType)
              ? ENV_VAR_SOURCE_TYPE_TO_KIND[envVarSourceType]
              : null,
            name: ['configMapKeyRef', 'secretKeyRef'].includes(envVarSourceType)
              ? (envVarSource as ConfigMapKeyRef).name
              : envVarSourceType,
          },
          refObjKey:
            (envVarSource as ConfigMapKeyRef).key ||
            (envVarSource as ObjectFieldSelector).fieldPath ||
            (envVarSource as ResourceFieldSelector).resource,
        };
      }
      return envVar;
    });
  }

  adaptFormModel(envVars: EnvVarFormModel[]): EnvVar[] {
    if (envVars) {
      envVars = envVars.filter(
        ({ name, value, valueFrom }) => name || value || valueFrom,
      );
    }
    return envVars
      .filter(envVar => !!envVar.name)
      .map(envVar => {
        if (envVar.refObj && envVar.refObj.name) {
          let envVarSource;
          if (envVar.refObj.kind) {
            const refKind: SupportedEnvVarSourceType =
              KIND_TO_ENV_VAR_SOURCE_TYPE[envVar.refObj.kind];
            envVarSource = {
              [refKind]: {
                name: envVar.refObj.name,
                key: envVar.refObjKey,
              },
            };
          } else {
            envVarSource = {
              [envVar.refObj.name]: getEnvVarSourceObj(
                envVar.refObj.name,
                envVar.refObjKey,
              ),
            };
          }
          envVar = {
            name: envVar.name,
            valueFrom: envVarSource,
          };
        } else {
          envVar = {
            name: envVar.name,
            value: envVar.value,
          };
        }
        return envVar;
      });
  }

  envVarViewMode(envVar: EnvVar): 'value' | 'valueFrom' | 'yaml' {
    if (!isEnvVarSourceMode(envVar)) {
      return 'value';
    } else if (isEnvVarSourceSupported(envVar)) {
      return 'valueFrom';
    } else {
      return 'yaml';
    }
  }

  // Overwrite add so that we could have different types of controls
  add(index = this.length, withRef = false) {
    const control = this.createNewControl();
    if (withRef) {
      control.get('valueFrom').reset({});
    }
    this.form.insert(index, control);
    this.cdr.markForCheck();
  }

  getRefObj(obj: ConfigMap | Secret): EnvRefObj {
    return {
      name: obj.metadata.name,
      kind: obj.kind as SupportedEnvVarSourceKind,
    };
  }

  getRefObjLabel(refObj: ConfigMap | Secret) {
    return refObj && refObj.kind
      ? this.translateService.get(refObj.kind.toLowerCase()) +
          ': ' +
          refObj.metadata.name
      : '';
  }

  trackFn = (refObj: EnvRefObj) => {
    return refObj && refObj.kind
      ? refObj.kind.toLowerCase() + ': ' + refObj.name
      : refObj.name || '';
  };

  filterFn = (filterString: string, option: OptionComponent) => {
    return (option.value.name || option.value).includes(filterString);
  };

  getRefObjKeys(control: FormGroup): Observable<string[]> {
    const refObjControl = control.get('refObj');
    return refObjControl.valueChanges.pipe(
      startWith(refObjControl.value),
      map(refObj => {
        if (refObj.kind) {
          const objs =
            refObj.kind === 'Secret' ? this.secrets : this.configMaps;
          const selectedObj = objs
            ? ((objs as KubernetesResource[]).find(
                (obj: Secret | ConfigMap) => obj.metadata.name === refObj.name,
              ) as ConfigMap | Secret)
            : null;
          return selectedObj ? Object.keys(selectedObj.data || {}) : [];
        } else {
          if (refObj.name === 'fieldRef') {
            return FIELD_REF_FIELDS;
          } else if (refObj.name === 'resourceFieldRef') {
            return RESOURCE_FIELD_REF_FIELDS;
          }
        }
      }),
      tap(keys => {
        const keyControl = control.get('refObjKey');
        const enableKeyControl = keys && keys.length > 0;
        if (enableKeyControl) {
          keyControl.enable({ emitEvent: false });
        } else {
          keyControl.disable({ emitEvent: false });
        }
      }),
    );
  }

  valueFromChange(control: AbstractControl) {
    control.get('refObjKey').setValue('');
  }

  getRowErrorMessage = (errors: StringMap) => {
    if (errors.keyPatternInvalid) {
      return `“${this.translateService.get('key')}” ${this.translateService.get(
        K8S_ENV_VARIABLE_NAME.tip,
      )}`;
    } else if (errors.refObjKeyMissing) {
      return this.translateService.get('ref_field_required_message');
    } else if (errors.keyIsMissing) {
      return this.translateService.get('key_is_missing_error_message');
    } else if (errors.duplicateKey) {
      return this.translateService.get('duplicate_key_error_message', {
        key: errors.duplicateKey,
      });
    }
  };

  private getPreviousKeys(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ name }) => name)
      .filter(name => !!name);
  }

  private createNewControl() {
    return this.fb.group(
      {
        name: ['', [envVarNameValidator]],
        value: [],
        valueFrom: [],

        // The followings are view only controls and will be filtered out later
        refObj: [{}],
        refObjKey: [],
      },
      {
        validator: [
          keyPatternValidator,
          valueRequiredValidator,
          missingKeyValidator,
          (control: AbstractControl) => {
            const index = this.form.controls.indexOf(control);
            const previousKeys = this.getPreviousKeys(index);

            const { name } = control.value;

            if (previousKeys.includes(name)) {
              return {
                duplicateKey: name,
              };
            } else {
              return null;
            }
          },
        ],
      },
    );
  }
}
