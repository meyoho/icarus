import { NAME, StringMap, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { AbstractControl, FormArray, Validators } from '@angular/forms';
import { get, merge, set } from 'lodash-es';
import {
  BaseResourceFormGroupComponent,
  PathParam,
} from 'ng-resource-form-util';
import { dissocPath } from 'ramda';
import { withLatestFrom } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { ContainerFormComponent } from 'app/features-shared/app/pod-template-form/container-form/component';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  ConfigMap,
  ContainerFormModel,
  PodNetworkFormModel,
  PodTemplateSpec,
  PodTemplateSpecFormModel,
  Secret,
  WorkloadOption,
  WorkspaceBaseParams,
} from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  k8sResourceLabelKeyValidator,
} from 'app/utils';
import {
  CALICO_ANNOTATION_PREFIX,
  KUBE_OVN_ANNOTATION_PREFIX,
} from 'app_admin/features/network/subnet/util';
import { ClusterNetworkType } from 'app_admin/features/network/type';
import { adaptRepoNameToContainerName } from 'app_user/features/app/util';

import { getDefaultContainerConfig } from '../utils';

import {
  IP_RECYCLE_STRATEGY_IMMUTABLE,
  IP_RECYCLE_STRATEGY_NEVER,
  IpRecycleStrategyEnum,
} from './pod-network-form/util';

const FLOATING_IP_SUPPORT_PLUGINS = new Set([
  'galaxy-k8s-vlan',
  'galaxy-k8s-sriov',
]);
// 对支持浮动IP的pod，需在每个container中的limits和requests中配置本字段
const FLOATING_IP_CONTAINER_CONSTRAIN = 'tke.cloud.tencent.com/eni-ip';
const FLOATING_IP_PLUGINS_BASE_DOMAIN = 'k8s.v1.cni.cncf.io';
const FLOATING_IP_RECYCLE_BASE_DOMAIN = 'k8s.v1.cni.galaxy.io';

interface ContainerControl {
  control: AbstractControl[];
  isInitContainer: boolean;
}

@Component({
  selector: 'rc-pod-template-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodTemplateFormComponent extends BaseResourceFormGroupComponent<
  PodTemplateSpec,
  PodTemplateSpecFormModel
> {
  @ViewChildren(ContainerFormComponent)
  containerForms: QueryList<ContainerFormComponent>;

  @Input() workloadOptions: WorkloadOption[];
  @Input() configMapOptions: ConfigMap[];
  @Input() secretOptions: Secret[];

  @Input() baseParams: WorkspaceBaseParams;

  @Input() matchLabels: StringMap; // 更新 workload 时，禁止用户修改和 selector 中相同的 labels

  activeContainerIndex = 0;

  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      prefixPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  constructor(
    injector: Injector,
    private readonly appSharedService: AppSharedService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  get initContainersFormArray(): FormArray {
    return this.form.get('spec.initContainers') as FormArray;
  }

  get containersFormArray(): FormArray {
    return this.form.get('spec.containers') as FormArray;
  }

  get allContainersControls(): ContainerControl[] {
    return [
      ...(this.form.get('spec.initContainers') as FormArray).controls.map(
        control => ({
          isInitContainer: true,
          control: [control],
        }),
      ),
      ...(this.form.get('spec.containers') as FormArray).controls.map(
        control => ({
          isInitContainer: false,
          control: [control],
        }),
      ),
    ];
  }

  getCurrentContainerControl() {
    return this.allContainersControls[this.activeContainerIndex].control[0];
  }

  createForm() {
    const metadataForm = this.fb.group({
      labels: this.fb.control({}),
    });
    const specForm = this.fb.group({
      hostNetwork: this.fb.control(false),
      initContainers: this.fb.array([]),
      containers: this.fb.array([]),
      nodeSelector: this.fb.control([]),
      affinity: this.fb.control({}),
      volumes: this.fb.control([]),
      imagePullSecrets: this.fb.control([]),
    });
    return this.fb.group({
      network: this.fb.control({}),
      spec: specForm,
      metadata: metadataForm,
    });
  }

  adaptResourceModel(resource: PodTemplateSpec): PodTemplateSpecFormModel {
    // Makes sure user will not accidentally remove the last container:
    if (!resource || !resource.spec || !resource.spec.containers) {
      return null;
    }
    const network = this.getNetworkFromSpec(resource);
    return {
      ...resource,
      network,
    } as PodTemplateSpecFormModel;
  }

  adaptFormModel(formModel: PodTemplateSpecFormModel): PodTemplateSpec {
    formModel = this.adaptAnnotationsForSpec(formModel);
    const metadata = formModel.metadata || {};
    if (!get(formModel, 'spec.imagePullSecrets.length')) {
      delete formModel.spec.imagePullSecrets;
    }
    return {
      metadata,
      spec: {
        ...formModel.spec,
      },
    };
  }

  getNetworkFromSpec(resource: PodTemplateSpec) {
    const network: PodNetworkFormModel = {};
    merge(network, {
      fixIp: this.getFixIpsFromSpec(resource),
      subnet: this.k8sUtil.getAnnotation(resource, NAME, 'subnet'),
    });
    const floatingIpPlugin = this.k8sUtil.getAnnotation(resource, {
      type: 'networks',
      baseDomain: FLOATING_IP_PLUGINS_BASE_DOMAIN,
    });
    if (FLOATING_IP_SUPPORT_PLUGINS.has(floatingIpPlugin)) {
      set(network, 'floatingIp', true);
      const releasePolicy = this.k8sUtil.getAnnotation(resource, {
        type: 'release-policy',
        baseDomain: FLOATING_IP_RECYCLE_BASE_DOMAIN,
      });
      let ipRecycleStrategy: IpRecycleStrategyEnum;
      // 未传递/空字符串为随时回收
      if (!releasePolicy) {
        ipRecycleStrategy = IpRecycleStrategyEnum.ANYTIME;
      } else if (releasePolicy === IP_RECYCLE_STRATEGY_NEVER) {
        ipRecycleStrategy = IpRecycleStrategyEnum.NEVER;
      } else if (releasePolicy === IP_RECYCLE_STRATEGY_IMMUTABLE) {
        ipRecycleStrategy = IpRecycleStrategyEnum.IMMUTABLE;
      }
      if (ipRecycleStrategy) {
        set(network, 'ipRecycleStrategy', ipRecycleStrategy);
      }
    }

    return network;
  }

  private adaptAnnotationsForSpec(
    formModel: PodTemplateSpecFormModel,
  ): PodTemplateSpecFormModel {
    const { networkType } = formModel.network;
    let annotations = get(formModel, 'metadata.annotations', {});
    annotations = this.setFixIpAnnotation(
      formModel.network,
      annotations,
      networkType,
    );

    if (formModel.network.networkType === ClusterNetworkType.GALAXY) {
      const { floatingIp, ipRecycleStrategy } = formModel.network;
      if (floatingIp) {
        annotations = this.assocFloatingIpAnnotation(
          ipRecycleStrategy,
          annotations,
          formModel,
        );
      } else {
        annotations = dissocPath(
          [`${FLOATING_IP_PLUGINS_BASE_DOMAIN}/networks`],
          annotations,
        );
        annotations = dissocPath(
          [`${FLOATING_IP_RECYCLE_BASE_DOMAIN}/release-policy`],
          annotations,
        );
        formModel.spec.containers = formModel.spec.containers.map(container => {
          container = dissocPath(
            ['resources', 'limits', FLOATING_IP_CONTAINER_CONSTRAIN],
            container,
          );
          return dissocPath(
            ['resources', 'requests', FLOATING_IP_CONTAINER_CONSTRAIN],
            container,
          );
        });
      }
    }
    return { ...formModel, metadata: { ...formModel.metadata, annotations } };
  }

  private setFixIpAnnotation(
    network: PodNetworkFormModel,
    annotations: StringMap,
    networkType: ClusterNetworkType,
  ) {
    const { fixIp } = network;
    if (networkType === ClusterNetworkType.MACVLAN) {
      if (network.subnet) {
        set(
          annotations,
          [this.k8sUtil.normalizeType(NAME, 'subnet')],
          network.subnet,
        );
      } else {
        annotations = dissocPath(
          [this.k8sUtil.normalizeType(NAME, 'subnet')],
          annotations,
        );
      }
    }
    if (fixIp && fixIp.length > 0) {
      if (networkType === ClusterNetworkType.KUBE_OVN) {
        set(
          annotations,
          [`${KUBE_OVN_ANNOTATION_PREFIX}/ip_pool`],
          fixIp.join(','),
        );
      } else if (networkType === ClusterNetworkType.CALICO) {
        set(
          annotations,
          [`${CALICO_ANNOTATION_PREFIX}/ipAddrs`],
          JSON.stringify(fixIp),
        );
      } else if (networkType === ClusterNetworkType.MACVLAN) {
        set(
          annotations,
          [this.k8sUtil.normalizeType('ipAddrs', 'subnet')],
          fixIp.join(','),
        );
      }
    } else {
      // 无数据，同时清空
      annotations = dissocPath(
        [`${CALICO_ANNOTATION_PREFIX}/ipAddrs`],
        annotations,
      );
      annotations = dissocPath(
        [`${KUBE_OVN_ANNOTATION_PREFIX}/ip_pool`],
        annotations,
      );
      annotations = dissocPath(
        [this.k8sUtil.normalizeType('ipAddrs', 'subnet')],
        annotations,
      );
    }
    return annotations;
  }

  // impure function: affect containers annotation
  private assocFloatingIpAnnotation(
    ipRecycleStrategy: IpRecycleStrategyEnum,
    annotations: StringMap,
    formModel: PodTemplateSpecFormModel,
  ) {
    set(
      annotations,
      [`${FLOATING_IP_PLUGINS_BASE_DOMAIN}/networks`],
      'galaxy-k8s-vlan',
    );
    let recycleStrategy: string;
    if (ipRecycleStrategy === IpRecycleStrategyEnum.IMMUTABLE) {
      recycleStrategy = IP_RECYCLE_STRATEGY_IMMUTABLE;
    } else if (ipRecycleStrategy === IpRecycleStrategyEnum.NEVER) {
      recycleStrategy = IP_RECYCLE_STRATEGY_NEVER;
    }
    if (recycleStrategy) {
      set(
        annotations,
        [`${FLOATING_IP_RECYCLE_BASE_DOMAIN}/release-policy`],
        recycleStrategy,
      );
    } else {
      annotations = dissocPath(
        [`${FLOATING_IP_RECYCLE_BASE_DOMAIN}/release-policy`],
        annotations,
      );
    }
    const { containers } = formModel.spec;
    if (Array.isArray(containers)) {
      containers.forEach(container => {
        set(
          container,
          ['resources', 'limits', FLOATING_IP_CONTAINER_CONSTRAIN],
          '1',
        );
        set(
          container,
          ['resources', 'requests', FLOATING_IP_CONTAINER_CONSTRAIN],
          '1',
        );
      });
    }
    return annotations;
  }

  private getFixIpsFromSpec(resource: PodTemplateSpec) {
    // 同一时刻只有一种网络类型，至多只有一种
    const fixIpOvn = (
      this.k8sUtil.getAnnotation(resource, {
        type: 'ip_pool',
        baseDomain: KUBE_OVN_ANNOTATION_PREFIX,
      }) || ''
    )
      .split(',')
      .filter((i: string) => !!i);

    const fixIpCalico: string[] = JSON.parse(
      this.k8sUtil.getAnnotation(resource, {
        type: 'ipAddrs',
        baseDomain: CALICO_ANNOTATION_PREFIX,
      }) || '[]',
    );

    const fixIpMacvlan: string[] = (
      this.k8sUtil.getAnnotation(resource, 'ipAddrs', 'subnet') || ''
    )
      .split(',')
      .filter(i => !!i);

    if (fixIpOvn.length > 0) {
      return fixIpOvn;
    } else if (fixIpCalico.length > 0) {
      return fixIpCalico;
    } else if (fixIpMacvlan.length > 0) {
      return fixIpMacvlan;
    } else {
      return [];
    }
  }

  getDefaultFormModel(): PodTemplateSpecFormModel {
    return {
      spec: {
        hostNetwork: false,
        containers: [],
        initContainers: [],
        nodeSelector: {},
        affinity: {},
      },
      metadata: {},
    };
  }

  addContainer(isInitContainer = false) {
    const containerForms = this.containerForms.toArray();
    containerForms[this.activeContainerIndex].triggerSubmit();
    if (!containerForms[this.activeContainerIndex].isValid) {
      return;
    }

    this.appSharedService
      .selectImage({
        cluster: this.baseParams.cluster,
        project: this.baseParams.project,
        namespace: this.baseParams.namespace,
      })
      .pipe(
        withLatestFrom(
          this.appSharedService.getDefaultLimitResources(
            {
              cluster: this.baseParams.cluster,
              namespace: this.baseParams.namespace,
            },
            true,
          ),
        ),
      )
      .subscribe(([res, defaultLimit]) => {
        if (res) {
          const formArray = isInitContainer
            ? this.initContainersFormArray
            : this.containersFormArray;
          formArray.push(
            this.fb.control(
              getDefaultContainerConfig({
                name: adaptRepoNameToContainerName(res.repository_name || ''),
                image: `${res.full_image_name}:${res.tag}`,
                defaultLimit,
              }),
            ),
          );
          this.activeContainerIndex = isInitContainer
            ? this.initContainersFormArray.length - 1
            : this.initContainersFormArray.length +
              this.containersFormArray.length -
              1;
          this.cdr.markForCheck();
        }
      });
  }

  removeContainer(index: number, isInitContainer = false) {
    const formArray = isInitContainer
      ? this.initContainersFormArray
      : this.containersFormArray;
    formArray.removeAt(index);
  }

  getOnFormArrayResizeFn() {
    return (path: PathParam) => this.getNewContainerFormControl(path);
  }

  getExistedContainerNames(index: number) {
    const formModels = [
      ...this.initContainersFormArray.value,
      ...this.containersFormArray.value,
    ] as ContainerFormModel[];
    return formModels
      .filter((_value, _index: number) => {
        return _index !== index;
      })
      .map((c: ContainerFormModel) => c.name);
  }

  getNewContainerFormControl(path?: PathParam) {
    let index =
      this.initContainersFormArray.length + this.containersFormArray.length;
    if (path) {
      index = +path[path.length - 1];
    }
    return this.fb.control({ name: `container-${index}`, image: '' });
  }

  trackByFn(index: number) {
    return index;
  }

  getSelectorKeys(matchLabels: StringMap) {
    return matchLabels ? Object.keys(matchLabels) : [];
  }
}
