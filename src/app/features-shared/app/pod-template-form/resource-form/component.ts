import {
  K8sApiService,
  ObservableInput,
  StringMap,
  TOKEN_BASE_DOMAIN,
  TranslateService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  pluck,
  switchMap,
  take,
  takeUntil,
} from 'rxjs/operators';

import {
  ConfigMap,
  ErrorMapper,
  LimitRange,
  RESOURCE_TYPES,
  ResourceRequirements,
  ResourceRequirementsFormModel,
  ResourceType,
} from 'app/typings';
import { KUBE_PUBLIC_NAMESPACE, POSITIVE_INT_PATTERN } from 'app/utils';

import { formatCPU, formatMemory, getCpu, getMemory } from './utils';

const REQUESTS_FROM_LIMITS = 'fromLimits';
const AVAILABLE_RESOURCE_TYPES = ['cpu', 'memory'] as const;
type AvailableResourceType = typeof AVAILABLE_RESOURCE_TYPES[number];
type ResourceKind = keyof ResourceRequirements;
interface LimitRangeConfig extends StringMap {
  cpu: string;
  memory: string;
}

interface ResourceControl {
  value: string;
  unit: string;
}

@Component({
  selector: 'rc-resources-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourcesFormComponent
  extends BaseResourceFormGroupComponent<
    ResourceRequirements,
    ResourceRequirementsFormModel
  >
  implements OnInit, OnDestroy {
  @Input() cluster: string;
  @ObservableInput(true)
  private readonly cluster$: Observable<string>;

  @Input() namespace: string;
  @ObservableInput(true)
  private readonly namespace$: Observable<string>;

  private readonly onDestroy$ = new Subject<void>();
  private readonly limitRange$: Observable<LimitRange>;
  _adaptedResource: ResourceRequirementsFormModel; // Hack: adaptedResource is private in base class, we need it to initialize dynamic form control value
  errorMapper: ErrorMapper;
  resourceUnits = {
    cpu: [
      {
        value: 'm',
        label: 'm',
      },
      {
        value: 'c',
        label: this.translate.get('core'),
      },
    ],
    memory: ['Mi', 'Gi'],
  };

  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;

  customResourceConfigs: StringMap[] = [];
  lang$ = this.translate.locale$.pipe(publishRef());

  constructor(
    injector: Injector,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {
    super(injector);
    this.limitRange$ = combineLatest([
      this.cluster$.pipe(filter(i => !!i)),
      this.namespace$.pipe(filter(i => !!i)),
    ]).pipe(
      switchMap(([cluster, namespace]) =>
        this.k8sApi
          .getResource<LimitRange>({
            cluster,
            namespace,
            type: RESOURCE_TYPES.LIMIT_RANGE,
            name: 'default',
          })
          .pipe(catchError(() => of(null))),
      ),
      publishRef(),
    );

    this.limitRange$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(res => this.setupErrorMapper(res));
  }

  ngOnInit() {
    super.ngOnInit();
    this.setupCustomResourceFormGroup();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getResourceMergeStrategy() {
    return true;
  }

  get limits() {
    return this.form.get('limits');
  }

  getResourceByType(type: AvailableResourceType) {
    return this.limits.get(type) as FormGroup;
  }

  createForm() {
    const cpuAsyncValidatorFn = this.getAsyncValidatorFn('cpu');
    const memoryAsyncValidatorFn = this.getAsyncValidatorFn('memory');

    return this.fb.group({
      limits: this.fb.group({
        cpu: this.fb.group(
          {
            value: [
              '',
              [
                Validators.required,
                Validators.pattern(POSITIVE_INT_PATTERN.pattern),
              ],
            ],
            unit: [],
          },
          {
            asyncValidator: cpuAsyncValidatorFn,
          },
        ),
        memory: this.fb.group(
          {
            value: [
              '',
              [
                Validators.required,
                Validators.pattern(POSITIVE_INT_PATTERN.pattern),
              ],
            ],
            unit: [],
          },
          {
            asyncValidator: memoryAsyncValidatorFn,
          },
        ),
      }),
      customResource: this.fb.group({}),
    });
  }

  private getAsyncValidatorFn(type: AvailableResourceType): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.limitRange$.pipe(
        map((resource: LimitRange) =>
          this.validateControl(resource, control, type),
        ),
        take(1),
      );
    };
  }

  private validateControl(
    resource: LimitRange,
    control: AbstractControl,
    type: AvailableResourceType,
  ) {
    if (!resource || !control.value || !control.value.value) {
      return null;
    } else {
      const max = get(resource, 'spec.limits[0].max') as LimitRangeConfig;
      return this.limitLessThanMax(max, control.value, type)
        ? null
        : { [`${type}_max`]: true };
    }
  }

  private setupErrorMapper(resource: LimitRange) {
    const defaultRequest = get(
      resource,
      'spec.limits[0].defaultRequest',
    ) as LimitRangeConfig;
    const max = get(resource, 'spec.limits[0].max') as LimitRangeConfig;
    if (max && defaultRequest) {
      this.errorMapper = {
        cpu_max: this.translate.get('cpu_should_not_greater_than', {
          maxCPU: max.cpu.endsWith('m')
            ? max.cpu
            : max.cpu + this.translate.get('unit_core'),
        }),
        memory_max: this.translate.get('memory_should_not_greater_than', {
          maxMemory: max.memory,
        }),
      };
    }
  }

  /**
   * limit值不能大于max值
   */
  private limitLessThanMax(
    max: LimitRangeConfig,
    limit: ResourceControl,
    type: AvailableResourceType,
  ): boolean {
    if (type === 'cpu') {
      return formatCPU(max.cpu) >= getCpu(parseFloat(limit.value), limit.unit);
    } else {
      return (
        formatMemory(max.memory) >=
        getMemory(parseFloat(limit.value), limit.unit)
      );
    }
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  adaptFormModel(
    formModel: ResourceRequirementsFormModel,
  ): ResourceRequirements {
    const limits = Object.keys(formModel.limits).reduce(
      (item, type: AvailableResourceType) => {
        const value = this.getResourceValue(formModel.limits[type]);
        if (value) {
          item[type] = value;
        }
        return item;
      },
      {} as Record<AvailableResourceType, string>,
    );
    const resource: ResourceRequirements = {
      requests: Object.assign({}, limits),
      limits: limits,
    };
    // http://confluence.alaudatech.com/pages/viewpage.action?pageId=50829241
    if (formModel.customResource) {
      Object.keys(formModel.customResource).forEach(key => {
        const limitsValue = formModel.customResource[key];
        if (limitsValue) {
          const config = this.getCustomResourceConfigByKey(key);
          if (!resource.limits) {
            resource.limits = {};
          }
          resource.limits[key] = limitsValue;
          if (config && config.requests === REQUESTS_FROM_LIMITS) {
            if (!resource.requests) {
              resource.requests = {};
            }
            resource.requests[key] = limitsValue;
          }
        }
      });
    }
    return resource;
  }

  adaptResourceModel(
    resources: ResourceRequirements,
  ): ResourceRequirementsFormModel {
    const resourcesModel = {} as ResourceRequirementsFormModel;
    if (resources) {
      Object.keys(resources).forEach(kind => {
        const resourceKind = kind as ResourceKind;
        resourcesModel[resourceKind] = Object.keys(resources[resourceKind])
          .filter(key =>
            AVAILABLE_RESOURCE_TYPES.includes(key as AvailableResourceType),
          )
          .reduce((item, type) => {
            const resourceType = type as AvailableResourceType;
            item[resourceType] = this.getResourceViewModel(
              resources[resourceKind][resourceType],
              resourceType,
            );
            return item;
          }, {} as Record<AvailableResourceType, ResourceControl>);
      });
      if (resources.limits) {
        Object.keys(resources.limits)
          .filter(
            key =>
              !AVAILABLE_RESOURCE_TYPES.includes(key as AvailableResourceType),
          )
          .forEach(key => {
            if (!resourcesModel.customResource) {
              resourcesModel.customResource = {};
            }
            resourcesModel.customResource[key] = resources.limits[key];
          });
      }
    }
    this._adaptedResource = resourcesModel;
    return resourcesModel;
  }

  getDefaultFormModel(): ResourceRequirementsFormModel {
    return {
      limits: {
        cpu: {
          value: '',
          unit: 'm',
        },
        memory: {
          value: '',
          unit: 'Mi',
        },
      },
    };
  }

  getCustomResourceTooltip(lang: 'en' | 'zh', config: StringMap) {
    return lang === 'zh' ? config.descriptionZh : config.descriptionEn;
  }

  private getResourceViewModel(
    data: string,
    type: AvailableResourceType,
  ): ResourceControl {
    // 对于 '2' 的cpu大小，视图上展示为 '2c'
    if (type === 'cpu' && !isNaN(+data)) {
      data = `${data}c`;
    }
    const reg = /^(\d?\.?\d+)(\w+)$/;
    const res = {
      value: '',
      unit: '',
    };
    if (!isNaN(parseFloat(data))) {
      const _res = reg.exec(data);
      res.value = _res[1] || '';
      switch (type) {
        case 'cpu':
          res.unit = _res[2] || 'm';
          break;
        case 'memory':
          res.unit = _res[2] || '';
          break;
      }
    }
    return res;
  }

  private getResourceValue(data: ResourceControl | string) {
    if (typeof data === 'string') {
      return data;
    } else {
      if (data.value) {
        if (data.unit === 'c') {
          return data.value;
        } else {
          return `${data.value}${data.unit}`;
        }
      } else {
        return '';
      }
    }
  }

  private setupCustomResourceFormGroup() {
    this.k8sApi
      .getResourceList<ConfigMap>({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster: this.cluster,
        namespace: KUBE_PUBLIC_NAMESPACE,
        queryParams: {
          labelSelector: matchLabelsToString({
            [`features.${this.baseDomain}/type`]: 'CustomResourceLimitation',
          }),
        },
      })
      .pipe(
        pluck('items'),
        catchError(() => of([] as ConfigMap[])),
      )
      .subscribe(configmaps => {
        this.customResourceConfigs = configmaps.map(c => c.data);
        const customResourceFormGroup = this.form.get(
          'customResource',
        ) as FormGroup;
        this.customResourceConfigs.forEach(config => {
          customResourceFormGroup.addControl(
            config.key,
            this.fb.control(
              this.getFormModelCustomResourceValue(
                config.key,
                // FIXME: 由于无法区分 初始状态和 更新一个空值的情况，暂时去掉自动填充的默认值
                '', // config.defaultValue
              ),
              [Validators.pattern(POSITIVE_INT_PATTERN.pattern)],
            ),
          );
        });
        this.cdr.markForCheck();
      });
  }

  private getFormModelCustomResourceValue(key: string, defaultValue: string) {
    return get(this._adaptedResource, ['customResource', key], defaultValue);
  }

  private getCustomResourceConfigByKey(key: string) {
    return this.customResourceConfigs.find(i => i.key === key);
  }
}
