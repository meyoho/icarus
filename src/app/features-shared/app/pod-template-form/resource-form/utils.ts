const reg = /^(\d?\.?\d+)(\w+)*$/;

export function formatMemory(mem: string) {
  const base = parseFloat(mem);
  const res = reg.exec(mem);
  const unit = res[2];
  if (unit) {
    return getMemory(base, unit);
  } else {
    return base;
  }
}

export function getMemory(base: number, unit: string) {
  switch (unit) {
    case 'K':
      return base * 1e3;
    case 'Ki':
      return base * 1024;
    case 'M':
      return base * 1e6;
    case 'Mi':
      return base * 1024 ** 2;
    case 'G':
      return base * 1e9;
    case 'Gi':
      return base * 1024 ** 3;
    case 'T':
      return base * 1e12;
    case 'Ti':
      return base * 1024 ** 4;
    case 'P':
      return base * 1e15;
    case 'Pi':
      return base * 1024 ** 5;
    case 'E':
      return base * 1e18;
    case 'Ei':
      return base * 1024 ** 6;
    default:
      return base;
  }
}

export function formatCPU(cpu: string) {
  const base = parseFloat(cpu);
  const res = reg.exec(cpu);
  const unit = res[2];
  return getCpu(base, unit);
}

export function getCpu(base: number, unit: string) {
  switch (unit) {
    case 'm':
      return base;
      break;
    case 'c':
      return base * 1e3;
      break;
    case 'k':
      return base * 1e6;
      break;
    case 'M':
      return base * 1e9;
      break;
    case 'G':
      return base * 1e12;
      break;
    case 'T':
      return base * 1e15;
      break;
    default:
      return base * 1e3;
      break;
  }
}
