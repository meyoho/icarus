import {
  K8sApiService,
  ObservableInput,
  matchLabelsToString,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Observable, combineLatest, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  pluck,
  startWith,
  switchMap,
} from 'rxjs/operators';

import {
  LocalObjectReference,
  RESOURCE_TYPES,
  Secret,
  SecretType,
  WorkspaceBaseParams,
} from 'app/typings';

import { getMergedResourceOptions } from '../../utils';

@Component({
  selector: 'rc-image-credential-form',
  templateUrl: 'template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
// 镜像凭据组件, formModel只需要为string[]
export class ImageCredentialComponent extends BaseResourceFormComponent<
  LocalObjectReference[],
  string[]
> {
  allCredentialNames$: Observable<string[]>;

  @ObservableInput(true) secretOptions$: Observable<Secret[]>;
  @Input() secretOptions: Secret[];

  @ObservableInput(true) baseParams$: Observable<WorkspaceBaseParams>;
  @Input() baseParams: WorkspaceBaseParams;

  constructor(injector: Injector, private readonly k8sApi: K8sApiService) {
    super(injector);
    this.allCredentialNames$ = combineLatest([
      this.baseParams$
        .pipe(
          filter(params => !!params),
          switchMap(baseParams =>
            this.k8sApi.getResourceList<Secret>({
              cluster: baseParams.cluster,
              namespace: baseParams.namespace,
              type: RESOURCE_TYPES.SECRET,
              queryParams: {
                fieldSelector: matchLabelsToString({
                  type: SecretType.DockerConfigJson,
                }),
              },
            }),
          ),
        )
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
      this.secretOptions$.pipe(
        startWith([] as Secret[]),
        filter(options => !!options),
        map(options =>
          options.filter(option => option.type === SecretType.DockerConfigJson),
        ),
      ),
    ]).pipe(
      map(([items, options]) =>
        getMergedResourceOptions(items, options).map(
          item => item.metadata.name,
        ),
      ),
    );
  }

  getResourceMergeStrategy(): boolean {
    return false;
  }

  createForm() {
    return new FormControl([]);
  }

  getDefaultFormModel(): string[] {
    return [];
  }

  adaptFormModel(formModel: string[] = []): LocalObjectReference[] {
    return formModel.map(name => {
      return { name };
    });
  }

  adaptResourceModel(resource: LocalObjectReference[] = []): string[] {
    return resource.map(({ name }) => name);
  }

  trackByFn(index: number) {
    return index;
  }
}
