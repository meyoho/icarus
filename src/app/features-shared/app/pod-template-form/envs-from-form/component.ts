import { ObservableInput, TranslateService } from '@alauda/common-snippet';
import { OptionComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';

import {
  EnvFromSourceFormModel,
  KIND_TO_SUPPORTED_ENV_FROM_TYPES,
  SupportedEnvVarSourceKind,
  adaptEnvFromSourceResource,
  getEnvSecrets,
} from 'app/features-shared/app/utils';
import {
  ConfigMap,
  EnvFromSource,
  Secret,
  WorkspaceBaseParams,
} from 'app/typings';

@Component({
  selector: 'rc-env-from-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFromFormComponent extends BaseResourceFormComponent<
  EnvFromSource[],
  EnvFromSourceFormModel[]
> {
  @ObservableInput(true) baseParams$: Observable<WorkspaceBaseParams>;
  @Input()
  baseParams: WorkspaceBaseParams;

  @Input() configMaps: ConfigMap[];

  @Input() secrets: Secret[];

  getEnvSecrets = getEnvSecrets;

  constructor(
    injector: Injector,
    private readonly translateService: TranslateService,
  ) {
    super(injector);
  }

  createForm() {
    return new FormControl([]);
  }

  getResourceMergeStrategy() {
    return false;
  }

  adaptResourceModel(
    envFromSources: EnvFromSource[],
  ): EnvFromSourceFormModel[] {
    if (!envFromSources || envFromSources.length === 0) {
      return this.getDefaultFormModel();
    }

    return adaptEnvFromSourceResource(envFromSources);
  }

  adaptFormModel(
    envFromSourceFormModels: EnvFromSourceFormModel[],
  ): EnvFromSource[] {
    return envFromSourceFormModels.map(({ kind, name }) => {
      const refType = KIND_TO_SUPPORTED_ENV_FROM_TYPES[kind];

      return {
        [refType]: {
          name,
        },
      };
    });
  }

  getDefaultFormModel(): EnvFromSourceFormModel[] {
    return [];
  }

  getRefObj(obj: ConfigMap | Secret): EnvFromSourceFormModel {
    return {
      name: obj.metadata.name,
      kind: obj.kind as SupportedEnvVarSourceKind,
    };
  }

  getRefObjLabel(refObj: ConfigMap | Secret) {
    return refObj && refObj.kind
      ? this.translateService.get(refObj.kind.toLowerCase()) +
          ': ' +
          refObj.metadata.name
      : '';
  }

  trackFn = (refObj: EnvFromSourceFormModel) => {
    return refObj && refObj.kind
      ? refObj.kind.toLowerCase() + ': ' + refObj.name
      : '';
  };

  filterFn = (filterString: string, option: OptionComponent) => {
    return option.value.name.includes(filterString);
  };
}
