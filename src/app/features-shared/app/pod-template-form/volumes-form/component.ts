import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { ConfigMap, Secret, Volume } from 'app/typings';

import {
  AVAILABLE_VOLUME_TYPES,
  getVolumeType,
  getVolumeTypeTranslateKey,
} from '../../utils';
import { VolumeFormDialogComponent } from '../volume-form-dialog/component';

@Component({
  selector: 'rc-volumes-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumesFormComponent extends BaseResourceFormArrayComponent<
  Volume
> {
  constructor(private readonly dialog: DialogService, injector: Injector) {
    super(injector);
  }

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input() configMapOptions: ConfigMap[];
  @Input() secretOptions: Secret[];

  getVolumeType = getVolumeType;
  getVolumeTypeTranslateKey = getVolumeTypeTranslateKey;

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): Volume[] {
    return [];
  }

  getVolumeAttributes(volume: Volume): any {
    return volume[getVolumeType(volume)];
  }

  isBuiltIn(volume: Volume) {
    return AVAILABLE_VOLUME_TYPES.includes(getVolumeType(volume));
  }

  addVolume() {
    const afterAdd = new Subject<Volume>(); // could be subscribed in container volume mounts form
    this.dialog
      .open(VolumeFormDialogComponent, {
        size: DialogSize.Big,
        data: {
          cluster: this.cluster,
          namespace: this.namespace,
          existedNames: this.form.controls.map(c => c.value.name),
          existedPvcNames: this.getExistedPvcNames(),
          configMapOptions: this.configMapOptions,
          secretOptions: this.secretOptions,
        },
      })
      .afterClosed()
      .pipe(first())
      .subscribe(newVolume => {
        if (newVolume) {
          afterAdd.next(newVolume);
          afterAdd.complete();
          const control = this.fb.control(newVolume);
          this.form.push(control);
          this.cdr.markForCheck();
        }
      });
    return afterAdd;
  }

  editVolume(index: number) {
    const volume = this.form.controls[index].value;
    this.dialog
      .open(VolumeFormDialogComponent, {
        size: DialogSize.Big,
        data: {
          volume,
          cluster: this.cluster,
          namespace: this.namespace,
          existedNames: this.form.controls
            .map(c => c.value.name)
            .filter(n => n !== volume.name),
          existedPvcNames: this.getExistedPvcNames(volume),
          configMapOptions: this.configMapOptions,
          secretOptions: this.secretOptions,
        },
      })
      .afterClosed()
      .pipe(first())
      .subscribe(newVolume => {
        if (newVolume) {
          this.form.get([index]).setValue(newVolume);
          this.cdr.markForCheck();
        }
      });
  }

  private getExistedPvcNames(currentVolume?: Volume) {
    const names = this.form.controls
      .filter(c => c.value.persistentVolumeClaim)
      .map(c => c.value.persistentVolumeClaim.claimName);
    return currentVolume
      ? names.filter(
          name =>
            !currentVolume.persistentVolumeClaim ||
            currentVolume.persistentVolumeClaim.claimName !== name,
        )
      : names;
  }
}
