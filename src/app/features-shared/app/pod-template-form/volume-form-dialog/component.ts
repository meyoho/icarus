import {
  K8sApiService,
  NAME,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { setFormByResource, setResourceByForm } from 'ng-resource-form-util';
import { Observable, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  pluck,
  startWith,
  tap,
} from 'rxjs/operators';

import {
  ConfigMap,
  PersistentVolumeClaim,
  PvcStatusEnum,
  RESOURCE_TYPES,
  Secret,
  Volume,
  VolumeTypeEnum,
} from 'app/typings';
import { K8S_RESOURCE_NAME_BASE, TYPE, getPvcStatus } from 'app/utils';

import {
  AVAILABLE_VOLUME_TYPES,
  getMergedResourceOptions,
  getVolumeTypeTranslateKey,
} from '../../utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeFormDialogComponent implements OnInit {
  volume: Volume = {
    name: 'new-volume',
  };

  volumeTypes = AVAILABLE_VOLUME_TYPES;
  form: FormGroup;
  attributeForms: FormGroup;

  pvcNames$: Observable<string[]>;
  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;

  getVolumeTypeTranslateKey = getVolumeTypeTranslateKey;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: DialogRef<VolumeFormDialogComponent>,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      volume: Volume;
      cluster: string;
      namespace: string;
      configMapOptions?: ConfigMap[]; // 允许选择应用中声明的 ConfigMap
      secretOptions?: Secret[]; // 允许选择应用中声明的 Secret
      existedNames: string[];
      existedPvcNames: string[]; // PVC 不允许重复声明 volume
    },
  ) {
    if (this.dialogData.volume) {
      this.volume = this.dialogData.volume;
    }
  }

  onConfirm() {
    const name = this.form.get(NAME).value;
    if (
      this.dialogData.existedNames &&
      this.dialogData.existedNames.includes(name)
    ) {
      this.message.error(this.translate.get('volume_name_already_existed'));
      return;
    }

    const type = this.form.get(TYPE).value as VolumeTypeEnum;
    const volume = {
      name,

      // Use setResourceByForm to make sure hidden fields are not overridden
      [type]: setResourceByForm(
        this.attributeForms.get(type),
        this.volume[type],
      ),
    };

    this.dialogRef.close(volume);
  }

  get type() {
    return this.form.get(TYPE).value;
  }

  get confirmDisabled() {
    return this.form.invalid || this.attributeForms.get(this.type).invalid;
  }

  ngOnInit() {
    const type =
      // Default is persistentVolumeClaim
      this.getVolumeTypeFromVolume(this.volume) ||
      (AVAILABLE_VOLUME_TYPES[0] as VolumeTypeEnum);
    const attributes = this.volume[type];
    const configs = this.getFormControlConfigs();

    this.attributeForms = this.fb.group(configs);

    this.form = this.fb.group({
      name: [
        this.volume.name,
        [Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern)],
      ],
      type: [type],
    });

    if (attributes) {
      setFormByResource(this.attributeForms.get(type), attributes);
    }

    this.setupPvcForm();
    this.setupConfigMapForm();
    this.setupSecretForm();

    // Set up type observable:
    this.form
      .get(TYPE)
      .valueChanges.pipe(
        startWith(null as string),
        map(() => this.type),
        distinctUntilChanged(),
      )
      .subscribe(_type => {
        if (_type && !this.form.get(NAME).value) {
          this.form.get(NAME).setValue(_type);
        }
      });
  }

  isPvcOptionDisabled(name: string) {
    return (
      this.dialogData.existedPvcNames &&
      this.dialogData.existedPvcNames.includes(name)
    );
  }

  private setupPvcForm() {
    const pvcForm = this.attributeForms.get('persistentVolumeClaim');

    this.pvcNames$ = this.k8sApi
      .getResourceList<PersistentVolumeClaim>({
        cluster: this.dialogData.cluster,
        namespace: this.dialogData.namespace,
        type: RESOURCE_TYPES.PVC,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        map((list: PersistentVolumeClaim[]) =>
          list
            .filter(r => getPvcStatus(r) === PvcStatusEnum.bound)
            .map(r => r.metadata.name),
        ),
        tap(pvcNames => {
          const existedPvcNames = this.dialogData.existedPvcNames || [];
          const availableNames = pvcNames.filter(
            name => !existedPvcNames.includes(name),
          );
          if (!pvcForm.value.claimName && availableNames.length > 0) {
            pvcForm.get('claimName').setValue(availableNames[0]);
          }
        }),
        publishRef(),
      );
  }

  private setupConfigMapForm() {
    const configMapForm = this.attributeForms.get('configMap');

    this.configMaps$ = this.k8sApi
      .getResourceList<ConfigMap>({
        cluster: this.dialogData.cluster,
        namespace: this.dialogData.namespace,
        type: RESOURCE_TYPES.CONFIG_MAP,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        map(items =>
          getMergedResourceOptions(items, this.dialogData.configMapOptions),
        ),
        tap(configMaps => {
          if (
            !configMapForm.value.name &&
            configMaps &&
            configMaps.length > 0
          ) {
            configMapForm.get(NAME).setValue(configMaps[0].metadata.name);
          }
        }),
        publishRef(),
      );
  }

  private setupSecretForm() {
    const secretForm = this.attributeForms.get('secret');

    this.secrets$ = this.k8sApi
      .getResourceList<Secret>({
        cluster: this.dialogData.cluster,
        namespace: this.dialogData.namespace,
        type: RESOURCE_TYPES.SECRET,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        map(items =>
          getMergedResourceOptions(items, this.dialogData.secretOptions),
        ),
        tap(secrets => {
          if (!secretForm.value.secretName && secrets && secrets.length > 0) {
            secretForm.get('secretName').setValue(secrets[0].metadata.name);
          }
        }),
        publishRef(),
      );
  }

  private getVolumeTypeFromVolume(volume: Volume) {
    return Object.keys(volume).find(key => key !== NAME) as VolumeTypeEnum;
  }

  private getFormControlConfigs() {
    // For now we list all type whether or not they are used.
    return {
      emptyDir: [{}], // No controls yet
      persistentVolumeClaim: this.fb.group({
        claimName: this.fb.control(''),
      }),
      hostPath: this.fb.group({
        path: ['/'],
      }),
      configMap: this.fb.group({
        name: [''],
      }),
      secret: this.fb.group({
        secretName: [''],
      }),
    };
  }
}
