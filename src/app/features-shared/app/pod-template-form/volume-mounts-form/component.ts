import { K8sApiService, publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { groupBy, omit } from 'ramda';
import { Subject, of } from 'rxjs';
import { catchError, map, takeUntil } from 'rxjs/operators';

import {
  ConfigMap,
  RESOURCE_TYPES,
  Secret,
  Volume,
  VolumeMount,
  VolumeTypeEnum,
} from 'app/typings';
import { K8S_VOLUME_MOUNT_SUB_PATH, rowBackgroundColorFn } from 'app/utils';

import {
  getConfigMapKeys,
  getSecretKeys,
  getVolumeType,
  getVolumeTypeTranslateKey,
} from '../../utils';
import { VolumesFormComponent } from '../volumes-form/component';

interface VolumeOption {
  name: string;
  type: VolumeTypeEnum;
}
@Component({
  selector: 'rc-volume-mounts-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeMountsComponent
  extends BaseResourceFormArrayComponent<VolumeMount>
  implements OnInit, OnDestroy {
  constructor(injector: Injector, private readonly k8sApi: K8sApiService) {
    super(injector);
  }

  get volumes() {
    return this.volumesForm && this.volumesForm.formModel;
  }

  get noVolumes() {
    return !this.volumes || this.volumes.length === 0;
  }

  @Input() cluster: string;
  @Input() namespace: string;
  @Input()
  volumesForm: VolumesFormComponent;

  private readonly onDestroy$ = new Subject<void>();
  K8S_VOLUME_MOUNT_SUB_PATH = K8S_VOLUME_MOUNT_SUB_PATH;

  rowBackgroundColorFn = rowBackgroundColorFn;

  getVolumeType = getVolumeType;
  getVolumeTypeTranslateKey = getVolumeTypeTranslateKey;

  ngOnInit() {
    if (this.volumesForm) {
      this.volumesForm.form.valueChanges
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(() => {
          this.cdr.markForCheck();
        });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  adaptFormModel(formModel: VolumeMount[]) {
    return formModel.map(v => {
      if (!v.subPath) {
        return omit(['subPath'], v);
      }
      return v;
    });
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): VolumeMount[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewMountControl();
  }

  add(index = this.length) {
    if (!this.noVolumes) {
      super.add(index);
    }
  }

  addVolumeAndMount(index = this.length) {
    this.volumesForm.addVolume().subscribe(newVolume => {
      this.form.insert(index, this.createNewMountControl(newVolume.name));
      this.cdr.markForCheck();
    });
  }

  getGroupedVolumes(volumes: Volume[]) {
    const groupByFn = groupBy((item: VolumeOption) => item.type);
    return volumes.length > 0
      ? groupByFn(
          volumes.map(
            item =>
              ({
                name: item.name,
                type: getVolumeType(item),
              } as VolumeOption),
          ),
        )
      : {};
  }

  volumeTrackByFn(_index: number, item: Volume) {
    return item.name;
  }

  volumeNameValueChange(control: FormControl) {
    control.get('subPath').setValue('', {
      emitEvent: false,
    });
  }

  shouldShowSubPathOption = (volumeName: string) => {
    const volume = this.getVolumeByName(volumeName);
    return volume
      ? [VolumeTypeEnum.configMap, VolumeTypeEnum.secret].includes(
          getVolumeType(volume),
        )
      : false;
  };

  getSubPathOptions$ = (volumeName: string) => {
    const volume = this.getVolumeByName(volumeName);
    let option: ConfigMap | Secret;
    switch (getVolumeType(volume)) {
      case VolumeTypeEnum.secret:
        option = (this.volumesForm.secretOptions || []).find(
          o => o.metadata.name === volume.secret.secretName,
        );
        return option
          ? of(getConfigMapKeys(option))
          : this.k8sApi
              .getResource<Secret>({
                type: RESOURCE_TYPES.SECRET,
                cluster: this.cluster,
                namespace: this.namespace,
                name: volume.secret.secretName,
              })
              .pipe(
                map(getSecretKeys),
                catchError(() => of([])),
                publishRef(),
              );
      case VolumeTypeEnum.configMap:
        option = (this.volumesForm.configMapOptions || []).find(
          o => o.metadata.name === volume.configMap.name,
        );
        return option
          ? of(getConfigMapKeys(option))
          : this.k8sApi
              .getResource<ConfigMap>({
                type: RESOURCE_TYPES.CONFIG_MAP,
                cluster: this.cluster,
                namespace: this.namespace,
                name: volume.configMap.name,
              })
              .pipe(
                map(getConfigMapKeys),
                catchError(() => of([])),
                publishRef(),
              );
    }
  };

  private getVolumeByName(name: string) {
    return this.volumes.find(v => v.name === name);
  }

  private getPreviousMountPaths(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ mountPath }) => mountPath)
      .filter(mountPath => !!mountPath);
  }

  private createNewMountControl(volumeName?: string) {
    const defaultName =
      volumeName ||
      (this.volumes && this.volumes.length && this.volumes[0].name);
    return this.fb.group(
      {
        name: [defaultName],
        mountPath: ['', [Validators.required]],
        subPath: ['', [Validators.pattern(K8S_VOLUME_MOUNT_SUB_PATH.pattern)]],
        readOnly: [false],
      },
      {
        validator: (control: AbstractControl) => {
          const index = this.form.controls.indexOf(control);
          const previousMountPaths = this.getPreviousMountPaths(index);

          const { mountPath } = control.value;

          if (previousMountPaths.includes(mountPath)) {
            return {
              duplicateMountPath: mountPath,
            };
          } else {
            return null;
          }
        },
      },
    );
  }
}
