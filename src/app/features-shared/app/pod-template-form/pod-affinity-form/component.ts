import { TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { groupBy, uniqBy } from 'lodash-es';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  AffinityTerm,
  PodAffinityFormModel,
  PodAffinityTerm,
  WeightedPodAffinityTerm,
  WorkloadOption,
} from 'app/typings';
import { TYPE } from 'app/utils';

@Component({
  selector: 'rc-pod-affinity-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodAffinityFormComponent
  extends BaseResourceFormComponent<AffinityTerm, PodAffinityFormModel>
  implements OnInit, OnDestroy {
  @Input() workloadOptions: WorkloadOption[] = [];

  @Input()
  type: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  mode: string;

  method = 'basic';

  workloads: {
    [key: string]: WorkloadOption[];
  } = {};

  workload: WorkloadOption;

  onDestroy$ = new Subject<void>();

  constructor(
    injector: Injector,
    private readonly acpApi: AcpApiService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return true;
  }

  get isUpdate() {
    return this.mode === 'update';
  }

  get isAdvanced() {
    return this.method === 'advanced';
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.mode !== 'update') {
      this.acpApi
        .getWorkloads({
          cluster: this.cluster,
          namespace: this.namespace,
        })
        .subscribe(workloads => {
          const workloadsOptions = Object.values(workloads)
            .flat()
            .map(r => ({
              kind: r.kind,
              name: r.metadata.name,
            }));
          this.workloads = groupBy(
            uniqBy(
              workloadsOptions.concat(this.workloadOptions || []),
              r => `${r.kind}${r.name}`,
            ),
            r => r.kind,
          );
        });
    } else {
      this.method = 'advanced';
      this.methodChange('advanced');
    }

    this.form
      .get(TYPE)
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((type: string) => {
        type === 'required'
          ? this.form.get('weight').disable({ emitEvent: false })
          : this.form.get('weight').enable({ emitEvent: false });
      });

    this.form
      .get('workload')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(workload => {
        this.workload = workload;
      });
  }

  methodChange(method: string) {
    if (method === 'advanced') {
      this.form.get(TYPE).value === 'required'
        ? this.form.get('weight').disable({ emitEvent: false })
        : this.form.get('weight').enable({ emitEvent: false });
      this.form.get('weight').setValue(1);
      this.form.get('workload').disable({ emitEvent: false });
    } else {
      this.form.get('weight').disable({ emitEvent: false });
      this.form.get('workload').enable({ emitEvent: false });
    }
    if (this.mode !== 'update') {
      // TO DO: 可能不是最优解
      // 仅在添加时，切换模式重新验证一下表单以更新errorsTooltip状态
      this.ngFormGroupDirective.onSubmit(null);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      workload: this.fb.control([]),
      type: this.fb.control('', [Validators.required]), // required, preferred
      weight: this.fb.control('', [
        Validators.min(1),
        Validators.max(100),
        Validators.required,
      ]), // only preferred
      topologyKey: this.fb.control('', [Validators.required]),
      labelSelector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
    });
  }

  getDefaultFormModel() {
    return {
      type: 'required',
      weight: 1,
      topologyKey: 'kubernetes.io/hostname',
      labelSelector: { matchLabels: {} },
    };
  }

  adaptResourceModel(resource: AffinityTerm): PodAffinityFormModel {
    if (resource) {
      const weight = (resource as WeightedPodAffinityTerm).weight;
      const topologyKey = weight
        ? (resource as WeightedPodAffinityTerm).podAffinityTerm.topologyKey
        : (resource as PodAffinityTerm).topologyKey;
      const labelSelector = weight
        ? (resource as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
        : (resource as PodAffinityTerm).labelSelector;
      return {
        type: weight ? 'preferred' : 'required',
        weight,
        topologyKey,
        labelSelector: { ...labelSelector },
      };
    } else {
      // 添加，初始无数据
      return {} as PodAffinityFormModel;
    }
  }

  adaptFormModel(formModel: PodAffinityFormModel): AffinityTerm {
    if (this.mode === 'add' && !this.isAdvanced) {
      // 添加一个，基本模式，直接根据workload的labels拼接。
      return {
        topologyKey: 'kubernetes.io/hostname',
        labelSelector: this.workload
          ? {
              matchLabels: {
                [`service.${this.baseDomain}/name`]: `${this.workload.kind.toLowerCase()}-${
                  this.workload.name
                }`,
              },
            }
          : {},
      } as PodAffinityTerm;
    } else {
      // 高级模式 或 更新，转换formModel为resource类型
      const resource: any = {};
      const podAffinityTerm: PodAffinityTerm = {
        topologyKey: formModel.topologyKey,
        labelSelector: formModel.labelSelector,
      };
      if (formModel.type === 'required') {
        Object.assign(resource, podAffinityTerm);
        return resource as PodAffinityTerm;
      } else {
        Object.assign(resource, {
          podAffinityTerm,
          weight: formModel.weight,
        });
        return resource as WeightedPodAffinityTerm;
      }
    }
  }

  triggerSubmit() {
    this.ngFormGroupDirective.onSubmit(null);
  }

  trackByFn(index: number) {
    return index;
  }

  keys(obj: {}) {
    return Object.keys(obj);
  }
}
