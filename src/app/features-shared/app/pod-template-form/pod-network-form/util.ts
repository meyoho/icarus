import { RESOURCE_TYPES, WorkloadType } from 'app/typings';
import { CIDR_IP_PATTERN } from 'app/utils';
import { ClusterNetworkType } from 'app_admin/features/network/type';

export const SUBNET_NAME_NONE = '$$none';

export enum IpRecycleStrategyEnum {
  ANYTIME = 'recycle_anytime',
  NEVER = 'recycle_never',
  IMMUTABLE = 'recycle_immutable',
}

export const IP_RECYCLE_STRATEGY_IMMUTABLE = 'immutable';
export const IP_RECYCLE_STRATEGY_NEVER = 'never';

export function fixIpEnable(
  type: ClusterNetworkType,
  workloadType: WorkloadType,
) {
  return (
    [
      ClusterNetworkType.KUBE_OVN,
      ClusterNetworkType.CALICO,
      ClusterNetworkType.MACVLAN,
    ].includes(type) && workloadType !== RESOURCE_TYPES.TAPP
  );
}

export function floatingIpEnable(
  type: ClusterNetworkType,
  workloadType: WorkloadType,
) {
  return (
    type === ClusterNetworkType.GALAXY &&
    workloadType !== RESOURCE_TYPES.DAEMONSET
  );
}

export function subnetChosenEnable(type: ClusterNetworkType) {
  return type === ClusterNetworkType.MACVLAN;
}
// 提前修正输入格式，确保在_validIpSet中是合法的IP,只需要调整新加入的即可
// 10.001.01.003 => 10.1.1.3
export function formatIpsPattern(ips: string[]) {
  if (!ips || ips.length === 0) {
    return [];
  }
  const len = ips.length;
  const lastInput = ips[len - 1];
  if (!CIDR_IP_PATTERN.pattern.test(lastInput)) {
    return ips;
  }
  const validInput = lastInput
    .split('.')
    .map(ip => +ip)
    .join('.');
  return ips.slice(0, -1).concat(validInput);
}
