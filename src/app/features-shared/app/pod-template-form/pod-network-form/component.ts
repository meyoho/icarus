import {
  K8sApiService,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  pluck,
  shareReplay,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { WORKLOAD_TYPE } from 'app/features-shared/workload/inject-tokens';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  Namespace,
  PodNetworkFormModel,
  PodNetworkTypeEnum,
  RESOURCE_TYPES,
  Subnet,
  SubnetIp,
  WorkloadType,
} from 'app/typings';
import { isBelongCidr, isBroadcastAddr, isValidIpPattern } from 'app/utils';
import { SubnetSupportNetworkType } from 'app_admin/features/network/subnet/subnet.type';
import { KUBE_OVN_ANNOTATION_PREFIX } from 'app_admin/features/network/subnet/util';
import { ClusterNetworkType } from 'app_admin/features/network/type';

import {
  IpRecycleStrategyEnum,
  SUBNET_NAME_NONE,
  fixIpEnable,
  floatingIpEnable,
  formatIpsPattern,
  subnetChosenEnable,
} from './util';

enum FixIpInputStatus {
  VALID = 'valid',
  INVALID = 'invalid',
  USED = 'used',
}
@Component({
  selector: 'rc-pod-network-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodNetworkFormComponent
  extends BaseResourceFormGroupComponent<PodNetworkFormModel>
  implements OnInit, OnDestroy {
  IpRecycleStrategyEnum = IpRecycleStrategyEnum;
  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input()
  hostNetwork: boolean;

  private readonly onDestroy$ = new Subject<void>();
  networkType$: Observable<SubnetSupportNetworkType>;
  networkType: SubnetSupportNetworkType;
  private readonly _validIpSet = new Set();
  subnetCidr = '';
  private excludeIps: string[] = [];
  private subnetName = '';
  subnetsList$: Observable<Subnet[]>;
  subnetsList: Subnet[];
  // 后端返回subnetName，内部维护相应Subnet Entity与之对应
  subnetChosen: Subnet;
  fixIpEnable = fixIpEnable;
  floatingIpEnable = floatingIpEnable;
  subnetChosenEnable = subnetChosenEnable;
  formatIpsPattern = formatIpsPattern;
  ngOnInit() {
    super.ngOnInit();
    const fixIpControl = this.form.get('fixIp');
    // TODO 之后支持tagInput的输入前校验
    fixIpControl.valueChanges
      .pipe(
        takeUntil(this.onDestroy$),
        filter(value => !!value),
        switchMap(value => {
          const formattedValue = this.formatIpsPattern(value);
          return this.validateFixIp(formattedValue).pipe(
            // 这里加入as any是由于下方subscribe时，会认为其中的每一项的类型为 A|B
            map(status => [formattedValue as any, status]),
          );
        }),
      )
      .subscribe(([value, status]) => {
        const length = value.length;
        if (length === 0) {
          return;
        }
        const lastInput = value[length - 1];
        if (status === FixIpInputStatus.VALID) {
          fixIpControl.setValue([...value.slice(0, length - 1), lastInput], {
            emitEvent: false,
          });
          this._validIpSet.add(lastInput);
        } else {
          fixIpControl.setValue([...value.slice(0, length - 1)], {
            emitEvent: false,
          });
          this.message.warning(
            this.translate.get(
              status === FixIpInputStatus.USED
                ? 'ip_used_hint'
                : 'ip_invalid_hint',
            ),
          );
        }
      });

    this.networkType$ = (this.k8sUtil.getNetworkTypeByCluster$(
      this.cluster,
    ) as Observable<SubnetSupportNetworkType>).pipe(
      tap(networkType => {
        this.networkType = networkType;
      }),
    );

    // 仅在macvlan拉取子网列表
    this.subnetsList$ = this.k8sApi
      .getResourceList<Subnet>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN,
        cluster: this.cluster,
      })
      .pipe(
        pluck('items'),
        tap(subnets => {
          this.subnetsList = subnets;
        }),
        catchError(() => of([])),
        shareReplay(1),
      );

    // 初始化子网相关数据
    this.networkType$
      .pipe(
        switchMap(networkType => {
          if (this.subnetChosenEnable(networkType)) {
            return combineLatest([
              this.form
                .get('subnet')
                .valueChanges.pipe(startWith(this.formModel.subnet)),
              this.subnetsList$,
            ]).pipe(
              map(([subnetName, subnets]) => {
                if (!subnetName) {
                  this.form.get('fixIp').disable();
                  return null;
                } else {
                  this.form.get('fixIp').enable();
                  const subnet = (subnets || []).find(
                    subnet => subnet.metadata.name === subnetName,
                  );
                  return !subnet
                    ? null
                    : {
                        subnetName: subnet.metadata.name,
                        subnetCidr: get(subnet, ['spec', 'cidrBlock']),
                        excludeIps: get(subnet, ['spec', 'excludeIps']),
                      };
                }
              }),
              takeUntil(this.onDestroy$),
            );
          }
          return this.k8sApi
            .getResource({
              type: RESOURCE_TYPES.NAMESPACE,
              name: this.namespace,
              cluster: this.cluster,
            })
            .pipe(
              map((value: Namespace) => ({
                subnetCidr: this.k8sUtil.getAnnotation(value, {
                  type: 'cidr',
                  baseDomain: KUBE_OVN_ANNOTATION_PREFIX,
                }),
                subnetName: this.k8sUtil.getAnnotation(value, {
                  type: 'logical_switch',
                  baseDomain: KUBE_OVN_ANNOTATION_PREFIX,
                }),
                excludeIps: (
                  this.k8sUtil.getAnnotation(value, {
                    type: 'exclude_ips',
                    baseDomain: KUBE_OVN_ANNOTATION_PREFIX,
                  }) || ''
                )
                  .split(',')
                  .filter((i: string) => !!i),
              })),
            );
        }),
      )
      .subscribe(result => {
        if (result) {
          this.subnetCidr = result.subnetCidr;
          this.subnetName = result.subnetName;
          this.excludeIps = result.excludeIps;
          this.cdr.markForCheck();
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  constructor(
    injector: Injector,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(WORKLOAD_TYPE) @Optional() public workloadType: WorkloadType,
  ) {
    super(injector);
    this.fixIpEnable = this.fixIpEnable.bind(this);
  }

  getDefaultFormModel() {
    return {
      networkType: ClusterNetworkType.DEFAULT,
      ipRecycleStrategy: IpRecycleStrategyEnum.ANYTIME,
      floatingIp: false,
      subnet: '',
    };
  }

  adaptFormModel(formModel: PodNetworkFormModel) {
    const networkType = this.networkType;
    if (this.hostNetwork) {
      // 主机模式下不支持固定IP，浮动IP
      return { networkType, hostNetwork: this.hostNetwork };
    }
    if (this.subnetChosenEnable(this.networkType) && !this.formModel.subnet) {
      return { networkType, hostNetwork: this.hostNetwork };
    }
    return { networkType, ...formModel };
  }

  adaptResourceModel(resource: PodNetworkFormModel) {
    // input fixIp from outer form
    if (resource && Array.isArray(resource.fixIp)) {
      resource.fixIp.forEach(ip => {
        this._validIpSet.add(ip);
      });
    }
    return {
      subnetName: SUBNET_NAME_NONE,
      ...resource,
      type:
        resource && this.hostNetwork
          ? PodNetworkTypeEnum.host
          : PodNetworkTypeEnum.cluster,
    };
  }

  createForm() {
    return this.fb.group({
      fixIp: this.fb.control([]),
      floatingIp: false,
      ipRecycleStrategy: this.fb.control(IpRecycleStrategyEnum.ANYTIME),
      subnet: this.fb.control(''),
    });
  }

  validateFixIp(value: string[]): Observable<FixIpInputStatus> {
    const length = value.length;
    const lastInput = get(value, length - 1, '');
    // 由于10.001.01.1会转化为10.1.1.1，需手动去重
    if (length && value.indexOf(lastInput) !== length - 1) {
      return of(FixIpInputStatus.INVALID);
    }
    if (length === 0 || this._validIpSet.has(lastInput)) {
      return of(FixIpInputStatus.VALID);
    }
    // invalid ip pattern，not broadcast ip
    if (
      !isValidIpPattern(lastInput) ||
      isBroadcastAddr(lastInput, this.subnetCidr)
    ) {
      return of(FixIpInputStatus.INVALID);
    }

    // calico网络不支持excludeIps
    const excludeIps =
      this.networkType !== ClusterNetworkType.CALICO ? this.excludeIps : [];
    // in ip segment,not exist in excludesIp
    if (
      (!!this.subnetCidr && !isBelongCidr(this.subnetCidr, lastInput)) ||
      excludeIps.includes(lastInput)
    ) {
      return of(FixIpInputStatus.INVALID);
    }
    // not exist in usedIP
    return this.validateExistIP({
      ip: lastInput,
    });
  }

  private validateExistIP(params: { ip: string }) {
    return this.k8sApi
      .getResourceList<SubnetIp>({
        type: RESOURCE_TYPES.SUBNET_KUBE_OVN_IP,
        cluster: this.cluster,
        queryParams: {
          field: 'spec.ipAddress',
          keyword: params.ip,
          labelSelector: matchLabelsToString({
            [`${KUBE_OVN_ANNOTATION_PREFIX}/subnet`]: this.subnetName,
          }),
        },
      })
      .pipe(
        pluck('items'),
        map(arr =>
          arr.filter(subnetIp => subnetIp.spec.ipAddress === params.ip)
            .length === 0
            ? FixIpInputStatus.VALID
            : FixIpInputStatus.USED,
        ),
        catchError(() => {
          return of(FixIpInputStatus.VALID);
        }),
      );
  }
}
