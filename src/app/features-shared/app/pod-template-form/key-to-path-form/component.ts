import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { KeyToPath } from 'app/typings';
import { K8S_VOLUME_MOUNT_SUB_PATH } from 'app/utils';

@Component({
  selector: 'rc-key-to-path-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyToPathFormComponent extends BaseResourceFormArrayComponent<
  KeyToPath
> {
  @Input()
  availableKeys: string[];

  K8S_VOLUME_MOUNT_SUB_PATH = K8S_VOLUME_MOUNT_SUB_PATH;

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): KeyToPath[] {
    return [
      {
        key: '',
        path: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group(
      {
        key: [''],
        path: ['', [Validators.pattern(K8S_VOLUME_MOUNT_SUB_PATH.pattern)]],
      },
      {
        validator: [
          (control: AbstractControl) => {
            const { key, path } = control.value;
            return (!key && !path) || (key && path) ? null : { error: true };
          },
        ],
      },
    );
  }
}
