import {
  K8sApiService,
  NAME,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { isEqual, remove } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, combineLatest, of } from 'rxjs';
import { catchError, map, pluck, startWith, switchMap } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import {
  getContainerNamesValidatorFn,
  getMergedResourceOptions,
} from 'app/features-shared/app/utils';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import {
  ConfigMap,
  Container,
  ContainerFormModel,
  EXCLUDE_LOG_PATH_KEY,
  EnvVar,
  LOG_PATH_KEY,
  RESOURCE_TYPES,
  Secret,
  WorkspaceBaseParams,
} from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

import { VolumesFormComponent } from '../volumes-form/component';

const DEFAULT_CONTAINER: Container = {
  name: '',
  image: '',
  ports: [],
};

@Component({
  selector: 'rc-container-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerFormComponent
  extends BaseResourceFormGroupComponent<ContainerFormModel>
  implements OnChanges, AfterViewInit {
  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true) baseParams$: Observable<WorkspaceBaseParams>;

  @Input() volumesForm: VolumesFormComponent;

  @Input()
  existedContainerNames: string[];

  @Input()
  hostNetwork: boolean;

  @Input() isInitContainer: boolean;

  @Input() configMapOptions: ConfigMap[];
  @ObservableInput(true) configMapOptions$: Observable<ConfigMap[]>;

  @Input() secretOptions: Secret[];
  @ObservableInput(true) secretOptions$: Observable<Secret[]>;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  private readonly namespacedConfigMaps$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sApi
        .getResourceList<ConfigMap>({
          type: RESOURCE_TYPES.CONFIG_MAP,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
    ),
  );

  private readonly namespacedSecrets$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sApi
        .getResourceList<Secret>({
          type: RESOURCE_TYPES.SECRET,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
    ),
  );

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.form.get(NAME).updateValueAndValidity();
    this.cdr.markForCheck();
  }

  configMaps$ = combineLatest([
    this.namespacedConfigMaps$,
    this.configMapOptions$.pipe(startWith(null as ConfigMap[])),
  ]).pipe(
    map(([items, options]) => getMergedResourceOptions(items, options)),
    publishRef(),
  );

  secrets$ = combineLatest([
    this.namespacedSecrets$,
    this.secretOptions$.pipe(startWith(null as Secret[])),
  ]).pipe(
    map(([items, options]) => getMergedResourceOptions(items, options)),
    publishRef(),
  );

  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService,
    private readonly appSharedService: AppSharedService,
  ) {
    super(injector);
  }

  ngOnChanges({
    existedContainerNames,
  }: {
    existedContainerNames: SimpleChange;
  }) {
    if (this.form && existedContainerNames) {
      const {
        currentValue,
        previousValue,
        firstChange,
      } = existedContainerNames;
      if (!firstChange && !isEqual(currentValue, previousValue)) {
        this.form.get(NAME).updateValueAndValidity();
        this.cdr.markForCheck();
      }
    }
  }

  createForm() {
    return this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
          getContainerNamesValidatorFn(() => {
            return this.existedContainerNames;
          }),
        ],
      ],
      image: [],
      volumes: [],
      volumeMounts: [],
      env: [],
      envFrom: [],
      args: [],
      command: [],
      resources: [],
      probes: [],
      logPaths: [],
      excludeLogPaths: [],
      ports: [],
    });
  }

  getDefaultFormModel() {
    return DEFAULT_CONTAINER;
  }

  triggerSubmit() {
    this.ngFormGroupDirective.onSubmit(null);
  }

  get isValid() {
    return this.form.valid;
  }

  adaptFormModel(formModel: ContainerFormModel) {
    const resource = { ...formModel };
    if (resource.args && resource.args.length === 0) {
      delete resource.args;
    }
    // 健康检查
    const probes = resource.probes;
    if (probes.livenessProbe) {
      resource.livenessProbe = { ...probes.livenessProbe };
    } else {
      delete resource.livenessProbe;
    }
    if (probes.readinessProbe) {
      resource.readinessProbe = { ...probes.readinessProbe };
    } else {
      delete resource.readinessProbe;
    }
    delete resource.probes;
    // 日志文件
    if (resource.logPaths) {
      resource.env = this.getReplacedEnvVarsWithLogPaths(
        resource.env,
        LOG_PATH_KEY,
        resource.logPaths,
      );
      delete resource.logPaths;
    }
    if (resource.excludeLogPaths) {
      resource.env = this.getReplacedEnvVarsWithLogPaths(
        resource.env,
        EXCLUDE_LOG_PATH_KEY,
        resource.excludeLogPaths,
      );
      delete resource.excludeLogPaths;
    }
    return resource;
  }

  adaptResourceModel(resource: ContainerFormModel) {
    const formModel = { ...resource };
    // 健康检查
    formModel.probes = {
      livenessProbe: formModel.livenessProbe
        ? { ...formModel.livenessProbe }
        : null,
      readinessProbe: formModel.readinessProbe
        ? { ...formModel.readinessProbe }
        : null,
    };
    // 日志文件
    if (formModel.env && formModel.env.length > 0) {
      const logPathEnvVar = formModel.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === LOG_PATH_KEY;
      });
      const excludeLogPathEnvVar = formModel.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === EXCLUDE_LOG_PATH_KEY;
      });
      if (logPathEnvVar && logPathEnvVar.value) {
        formModel.logPaths = logPathEnvVar.value.split(',');
      }
      if (excludeLogPathEnvVar && excludeLogPathEnvVar.value) {
        formModel.excludeLogPaths = excludeLogPathEnvVar.value.split(',');
      }
    }
    return formModel;
  }

  selectImage() {
    this.appSharedService
      .selectImage({
        cluster: this.baseParams.cluster,
        project: this.baseParams.project,
        namespace: this.baseParams.namespace,
      })
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.form.get('image').setValue(`${res.full_image_name}:${res.tag}`);
        }
      });
  }

  getRequiredValidator() {
    return [Validators.required];
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private getReplacedEnvVarsWithLogPaths(
    envs: EnvVar[],
    name: string,
    paths: string[],
  ) {
    if (!envs || envs.length === 0) {
      if (paths.length > 0) {
        return [
          {
            name,
            value: paths.join(','),
          },
        ];
      } else {
        return [];
      }
    } else {
      const target = envs.find((item: EnvVar) => {
        return item.name === name;
      });
      if (paths.length > 0) {
        const newEnvVar = {
          name,
          value: paths.join(','),
        };
        if (target) {
          target.value = paths.join(',');
        } else {
          envs.push(newEnvVar);
        }
      } else {
        if (target) {
          remove(envs, env => env.name === name && !env.valueFrom);
        }
      }
      return envs;
    }
  }
}
