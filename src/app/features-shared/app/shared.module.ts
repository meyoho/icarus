import { NgModule } from '@angular/core';

import { AppStatusComponent } from 'app/features-shared/app/app-status/component';
import { SharedModule } from 'app/shared/shared.module';
import { ServiceSharedModule } from 'app_user/features/service/shared.module';

import { AlarmSharedModule } from '../alarm/shared.module';

import { AppSharedService } from './app-shared.service';
import { PodControllerContainersComponent } from './detail/containers/component';
import { WorkloadDetailEnvComponent } from './detail/env/component';
import { ContainerHealthcheckDisplayComponent } from './detail/healthcheck/component';
import { HpaComponent } from './detail/hpa/component';
import { UpdateHpaComponent } from './detail/hpa/update-hpa/dialog';
import { HpaCronRulesFormComponent } from './detail/hpa/update-hpa/form/cron-rule/component';
import { HpaCronTriggerRuleComponent } from './detail/hpa/update-hpa/form/cron-rule/cron-trigger/component';
import { HpaIndicatorFormComponent } from './detail/hpa/update-hpa/form/indicator/component';
import { IndicatorMetricsFormComponent } from './detail/hpa/update-hpa/form/indicator/metrics/component';
import { PodsLogComponent } from './detail/log/component';
import { ContainerPortsPreviewComponent } from './detail/port/component';
import { VolumeAttributesComponent } from './detail/volume-attributes/component';
import { VolumeMountsPreviewComponent } from './detail/volume-mounts/component';
import { VolumesPreviewComponent } from './detail/volumes/component';
import { AddServiceFormDialogComponent } from './dialog/add-service/dialog';
import { AddServiceFormComponent } from './dialog/add-service/form';
import { UpdateEnvDialogComponent } from './dialog/update-env/component';
import { UpdateImageTagDialogComponent } from './dialog/update-image-tag/component';
import { UpdateResourceSizeDialogComponent } from './dialog/update-resource-size/component';
import { UpdateWorkloadLabelsDialogComponent } from './dialog/update-workload-labels/component';
import { ContainerFormHiddenFieldDirective } from './directives/container-form-hidden-field.directive';
import { PodSpecFormHiddenFieldDirective } from './directives/pod-spec-form-hidden-field.directive';
import { PodControllerDetailMonitorComponent } from './monitor/component';
import { PodAffinityDialogComponent } from './pod-template-form/affinity-dialog/component';
import { AffinityContainerFormComponent } from './pod-template-form/affinity-form/component';
import { PodTemplateFormComponent } from './pod-template-form/component';
import { ContainerFormComponent } from './pod-template-form/container-form/component';
import { EnvFormComponent } from './pod-template-form/envs-form/component';
import { EnvFromFormComponent } from './pod-template-form/envs-from-form/component';
import { HealthcheckFormComponent } from './pod-template-form/healthcheck-form/component';
import { HealthcheckDialogComponent } from './pod-template-form/healthcheck-form/container-healthcheck-dialog.component';
import { ImageCredentialComponent } from './pod-template-form/image-credential-form/component';
import { KeyToPathFormComponent } from './pod-template-form/key-to-path-form/component';
import { NodeSelectorComponent } from './pod-template-form/node-selector/component';
import { PodAffinityFormComponent } from './pod-template-form/pod-affinity-form/component';
import { PodNetworkFormComponent } from './pod-template-form/pod-network-form/component';
import { PortMappingComponent } from './pod-template-form/port-mapping-form/component';
import { ResourcesFormComponent } from './pod-template-form/resource-form/component';
import { VolumeFormDialogComponent } from './pod-template-form/volume-form-dialog/component';
import { VolumeMountsComponent } from './pod-template-form/volume-mounts-form/component';
import { VolumesFormComponent } from './pod-template-form/volumes-form/component';
import { ResourceLabelComponent } from './resource-label/component';
import { WorkloadStatusIconComponent } from './workload-status-icon/component';
import { WorkloadStatusComponent } from './workload-status/component';

const EXPORT_COMPONENTS = [
  AffinityContainerFormComponent,
  ContainerFormComponent,
  ContainerHealthcheckDisplayComponent,
  EnvFormComponent,
  EnvFromFormComponent,
  HealthcheckDialogComponent,
  HealthcheckFormComponent,
  NodeSelectorComponent,
  ImageCredentialComponent,
  PodAffinityDialogComponent,
  PodAffinityFormComponent,
  PodControllerContainersComponent,
  WorkloadDetailEnvComponent,
  PodControllerDetailMonitorComponent,
  PodsLogComponent,
  PodNetworkFormComponent,
  PodTemplateFormComponent,
  ResourcesFormComponent,
  UpdateImageTagDialogComponent,
  UpdateResourceSizeDialogComponent,
  UpdateEnvDialogComponent,
  PodSpecFormHiddenFieldDirective,
  ContainerFormHiddenFieldDirective,
  KeyToPathFormComponent,
  VolumesFormComponent,
  VolumeFormDialogComponent,
  VolumeMountsComponent,
  VolumeAttributesComponent,
  VolumeMountsPreviewComponent,
  VolumesPreviewComponent,
  PortMappingComponent,
  WorkloadStatusComponent,
  WorkloadStatusIconComponent,
  HpaComponent,
  UpdateHpaComponent,
  HpaIndicatorFormComponent,
  HpaCronRulesFormComponent,
  IndicatorMetricsFormComponent,
  UpdateWorkloadLabelsDialogComponent,
  HpaCronTriggerRuleComponent,
  AddServiceFormComponent,
  AddServiceFormDialogComponent,
  AppStatusComponent,
  ResourceLabelComponent,
  ContainerPortsPreviewComponent,
];

@NgModule({
  imports: [SharedModule, AlarmSharedModule, ServiceSharedModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [AppSharedService],
})
export class AppSharedModule {}
