import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import {
  GenericWorkloadStatus,
  WorkloadStatusColorMapper,
  WorkloadStatusIconMapper,
} from 'app/typings';
import { getWorkloadStatus } from 'app/utils';
@Component({
  selector: 'rc-workload-status-icon',
  templateUrl: './template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
/**
 * http://confluence.alaudatech.com/pages/viewpage.action?pageId=27178273
 */
export class WorkloadStatusIconComponent {
  @Input()
  workloadStatus: GenericWorkloadStatus;

  getWorkloadStatus = getWorkloadStatus;
  WorkloadStatusColorMapper = WorkloadStatusColorMapper;
  WorkloadStatusIconMapper = WorkloadStatusIconMapper;

  getInstancesText(status: GenericWorkloadStatus) {
    return `(${status.current || 0}/${status.desired || 0})`;
  }
}
