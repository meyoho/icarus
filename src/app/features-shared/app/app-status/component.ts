import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';

import {
  Application,
  ApplicationStatusColorMapper,
  ApplicationStatusIconMapper,
} from 'app/typings';
import { STATUS, getApplicationStatus } from 'app/utils';
@Component({
  selector: 'rc-app-status',
  templateUrl: 'template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppStatusComponent {
  @Input()
  application: Application;

  getApplicationStatus = getApplicationStatus;
  ApplicationStatusColorMapper = ApplicationStatusColorMapper;
  ApplicationStatusIconMapper = ApplicationStatusIconMapper;

  getText(application: Application) {
    const total = (
      get(application, [STATUS, 'workloadsStatus', 'workloads']) || []
    ).length;
    const running = get(application, [STATUS, 'workloadsStatus', 'ready'], 0);
    return `(${running}/${total})`;
  }
}
