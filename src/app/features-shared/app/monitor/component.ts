import {
  K8sApiService,
  NAME,
  NAMESPACE,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import Highcharts, { SeriesLineOptions } from 'highcharts';
import { get } from 'lodash-es';
import { of } from 'rxjs';
import { catchError, map, pluck } from 'rxjs/operators';
import { v4 } from 'uuid';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  MODES,
  QueryMetric,
  QueryMetrics,
  getMetricNumericOptions,
  getMetricPercentOptions,
  parseMetricsResponse,
} from 'app/features-shared/app/utils';
import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';
import { Pod, RESOURCE_TYPES, ResourceType, Workload } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  selector: 'rc-pod-controller-detail-monitor',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerDetailMonitorComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  podController: Workload;

  @Input()
  pod: Pod;

  baseParams = this.workspaceComponent.baseParamsSnapshot;

  private intervalTimer: number;
  private pods: string[];

  componentName: string;
  componentType: string;
  modes: Array<{ [index: string]: string }>;
  aggregators = AGGREGATORS;
  selectedGroupMode: string;
  selectedAggregator = this.aggregators[0].key;
  Highcharts = Highcharts;
  cpuChartOptions: Highcharts.Options = getMetricPercentOptions();
  memChartOptions: Highcharts.Options = getMetricPercentOptions();
  sentBytesChartOptions: Highcharts.Options = getMetricNumericOptions();
  receivedBytesChartOptions: Highcharts.Options = getMetricNumericOptions();
  queryMetrics = QueryMetrics;
  queryIdMap = new Map<string, QueryMetric>();

  constructor(
    private readonly cdr: ChangeDetectorRef,
    auiMessageService: MessageService,
    translateService: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly metricService: MetricService,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {
    super(auiMessageService, translateService);
  }

  ngOnInit() {
    const isPod = !!this.pod;
    const resource = this.pod || this.podController;
    this.modes = MODES.slice(isPod ? 1 : 0);
    this.selectedGroupMode = this.modes[0].key;
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    this.sentBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.receivedBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.componentName = get(resource, 'metadata.name');
    this.componentType = get(resource, 'kind');
    (isPod
      ? of([resource.metadata.name])
      : this.k8sApi
          .getResourceList<Pod>({
            type: RESOURCE_TYPES.POD,
            cluster: this.baseParams.cluster,
            namespace: this.baseParams.namespace,
            queryParams: {
              labelSelector: matchLabelsToString(
                get(resource, 'spec.selector.matchLabels', {}),
              ),
            },
          })
          .pipe(
            pluck('items'),
            catchError(() => of<Pod[]>([])),
            map(items => items.map(item => item.metadata.name)),
          )
    ).subscribe(pods => {
      this.pods = pods;
      this.resetTimeRange();
      this.loadCharts();
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.chartLoading = true;
          this.cdr.markForCheck();
          this.loadCharts();
        }
      }
    }, 60000);
  }

  loadCharts() {
    const args = {
      mode: this.selectedGroupMode,
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 1800) {
      this.step = 60;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.endTime = args.end;
    const params: MetricQuery = {
      start,
      end: args.end,
      step: this.step,
    };
    let podName = '';
    if (this.pods.length > 0) {
      podName = this.pods.join('|');
    }
    const queries = this.queryMetrics.map(query => {
      const uuid = v4();
      const labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: `${args.mode}.${query.metric}`,
        },
        {
          type: 'EQUAL',
          name: NAMESPACE,
          value: this.baseParams.namespace,
        },
        {
          type: 'EQUAL',
          name: 'kind',
          value: this.componentType,
        },
        {
          type: 'EQUAL',
          name: NAME,
          value: this.componentName,
        },
      ];
      if (this.podController) {
        labels.push({
          type: 'IN',
          name: 'pod_name',
          value: podName,
        });
      }
      this.queryIdMap.set(uuid, query);
      const metricQuery: MetricQueries = {
        aggregator: args.aggregator,
        id: uuid,
        labels,
      };
      switch (args.mode) {
        case 'pod':
          metricQuery.group_by = 'pod_name';
          break;
        case 'container':
          metricQuery.group_by = 'container_name';
      }
      return metricQuery;
    });
    params.queries = queries;
    this.metricService
      .queryMetrics(this.baseParams.cluster, params)
      .then((results: Metric[]) => {
        const chartData = new Map<string, Metric[]>([
          ['cpu.utilization', []],
          ['memory.utilization', []],
          ['network.transmit_bytes', []],
          ['network.receive_bytes', []],
        ]);
        results.forEach(result => {
          const { metric } = this.queryIdMap.get(result.metric.__query_id__);
          chartData.set(metric, [result, ...chartData.get(metric)]);
        });
        chartData.forEach(values => {
          const { series, unit } = this.queryIdMap.get(
            values[0].metric.__query_id__,
          );
          if (this.selectedGroupMode === 'container') {
            values = values.filter(
              el => el.metric.container_name || el.metric.container,
            );
          }
          if (unit) {
            this[series].series = parseMetricsResponse(
              values,
              this.getName.bind(this),
              this.endTime,
              this.step,
              100,
            ) as SeriesLineOptions[];
          } else {
            this[series].series = parseMetricsResponse(
              values,
              this.getName.bind(this),
              this.endTime,
              this.step,
            ) as SeriesLineOptions[];
          }
        });
      })
      .catch(error => {
        this.cpuChartOptions.series = [];
        this.memChartOptions.series = [];
        this.sentBytesChartOptions.series = [];
        this.receivedBytesChartOptions.series = [];
        throw error;
      })
      .finally(() => {
        this.chartLoading = false;
        this.cdr.markForCheck();
      });
  }

  private getName(el: Metric) {
    let name = this.componentName;
    if (this.selectedGroupMode === 'pod') {
      name = el.metric.pod_name || el.metric.pod;
    } else if (this.selectedGroupMode === 'container') {
      name = `${el.metric.pod_name || el.metric.pod} ${
        el.metric.container_name || el.metric.container
      }`;
    }
    return name;
  }
}
