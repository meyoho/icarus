import { K8sApiService, publishRef } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, get, set } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, pluck } from 'rxjs/operators';

import {
  ConfigMap,
  Container,
  CronJob,
  EnvFromSource,
  EnvVar,
  RESOURCE_TYPES,
  ResourceType,
  Secret,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateEnvDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm, { static: true })
  private readonly form: NgForm;

  private defaultEnvs: EnvVar[];
  private containerPath: string;
  private resource: Workload | CronJob;
  private resourceType: ResourceType;

  submitting = false;
  filteredEnvs: EnvVar[];
  envFrom: EnvFromSource[];

  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA)
    public data: {
      podController?: Workload;
      cronJob?: CronJob;
      baseParams: WorkspaceBaseParams;
      type: 'env' | 'envFrom';
      containerName: string;
      isInitContainer: boolean;
    },
  ) {
    this.configMaps$ = this.k8sApi
      .getResourceList<ConfigMap>({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster: this.data.baseParams.cluster,
        namespace: this.data.baseParams.namespace,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        publishRef(),
      );

    this.secrets$ = this.k8sApi
      .getResourceList<Secret>({
        type: RESOURCE_TYPES.SECRET,
        cluster: this.data.baseParams.cluster,
        namespace: this.data.baseParams.namespace,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        publishRef(),
      );
  }

  ngOnInit(): void {
    let podSpecPath: string;
    if (this.data.podController) {
      podSpecPath = 'spec.template.spec';
      this.resource = cloneDeep(this.data.podController);
      this.resourceType = this.data.podController.kind.toUpperCase() as ResourceType;
    } else if (this.data.cronJob) {
      podSpecPath = 'spec.jobTemplate.spec.template.spec';
      this.resource = cloneDeep(this.data.cronJob);
      this.resourceType = RESOURCE_TYPES.CRON_JOB;
    }
    this.containerPath = `${podSpecPath}.${
      this.data.isInitContainer ? 'initContainers' : 'containers'
    }`;
    const containers: Container[] = get(this.resource, this.containerPath, []);
    const container = containers.find(
      ({ name }) => name === this.data.containerName,
    );
    const envs = container.env || [];
    // 不允许编辑默认的env，也不能删掉
    this.filteredEnvs = envs.filter((item: EnvVar) => {
      return !this.isDefaultEnv(item.name);
    });
    this.defaultEnvs = envs.filter((item: EnvVar) => {
      return this.isDefaultEnv(item.name);
    });
    this.envFrom = container.envFrom || [];
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const part = this.getResourceUpdatePart(this.resource);
    this.updateResource(part)
      .pipe(catchError(() => of(null)))
      .subscribe(response => {
        if (response) {
          this.close.next(true);
        }
        this.submitting = false;
        this.cdr.markForCheck();
      });
  }

  cancel() {
    this.close.next(false);
  }

  private isDefaultEnv(name: string) {
    return [
      '__ALAUDA_FILE_LOG_PATH__',
      '__ALAUDA_EXCLUDE_LOG_PATH__',
      '__ALAUDA_SERVICE_ID__',
      '__ALAUDA_SERVICE_NAME__',
      '__ALAUDA_APP_NAME__',
    ].includes(name);
  }

  private getResourceUpdatePart(res: Workload | CronJob) {
    const containers = get(res, this.containerPath, []).map(
      (item: Container) => {
        if (item.name !== this.data.containerName) {
          return item;
        } else {
          const arr =
            this.data.type === 'env'
              ? [...this.defaultEnvs, ...this.filteredEnvs]
              : [...this.envFrom];
          return {
            ...item,
            [this.data.type]: arr,
          };
        }
      },
    );
    return set({}, this.containerPath, containers);
  }

  private updateResource(part: Workload | CronJob) {
    return this.k8sApi.patchResource({
      cluster: this.data.baseParams.cluster,
      type: this.resourceType,
      resource: this.resource,
      part,
    });
  }
}
