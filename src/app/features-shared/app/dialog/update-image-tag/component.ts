import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, get, set } from 'lodash-es';
import { of } from 'rxjs';
import { catchError, finalize, map, pluck } from 'rxjs/operators';

import { ImageService } from 'app/features-shared/image/image.service';
import {
  Container,
  CronJob,
  RESOURCE_TYPES,
  ResourceType,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';

import { getContainerPath } from '../../utils';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateImageTagDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();

  @ViewChild(NgForm, { static: true })
  private readonly form: NgForm;

  private targetContainer: Container;
  private image: string;
  private containerPath: string;
  private resourceType: ResourceType;

  selectedImageTag = '';
  imageTagOptions: string[] = [];
  imageName: string;
  submitting = false;
  initialized = false;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly imageService: ImageService,
    @Inject(DIALOG_DATA)
    public data: {
      podController?: Workload;
      cronJob?: CronJob;
      containerName: string;
      baseParams: WorkspaceBaseParams;
    },
  ) {}

  ngOnInit() {
    let resource: Workload | CronJob;
    let podSpecPath = 'spec.template.spec';
    if (this.data.podController) {
      this.resourceType = this.data.podController.kind.toUpperCase() as ResourceType;
      resource = this.data.podController;
    } else if (this.data.cronJob) {
      podSpecPath = 'spec.jobTemplate.spec.template.spec';
      this.resourceType = RESOURCE_TYPES.CRON_JOB;
      resource = this.data.cronJob;
    }
    this.containerPath = getContainerPath(
      resource,
      podSpecPath,
      this.data.containerName,
    );
    this.targetContainer = cloneDeep(
      get(resource, this.containerPath, []).find(
        (c: Container) => c.name === this.data.containerName,
      ),
    );
    this.image = this.targetContainer.image;
    const imageParts = this.image.split(':');
    this.selectedImageTag = imageParts.pop();
    this.imageName = imageParts.join(':');
    this.imageService
      .findRepositoriesByProject(this.data.baseParams.project, null)
      .pipe(
        pluck('items'),
        map(items => {
          const tags = items.find(
            item => [item.endpoint, item.image].join('/') === this.imageName,
          )?.tags;
          return tags ? tags.map(tag => tag.name) : [];
        }),
        // 不支持devops，返回空，直接输入
        catchError(_ => {
          return of([] as string[]);
        }),
        finalize(() => {
          this.initialized = true;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(tags => {
        this.imageTagOptions = tags;
      });
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    this.targetContainer.image = [this.imageName, this.selectedImageTag].join(
      ':',
    );

    this.updateResource()
      .pipe(catchError(() => of(null)))
      .subscribe(response => {
        if (response) {
          this.close.next(true);
        }
        this.submitting = false;
        this.cdr.markForCheck();
      });
  }

  cancel() {
    this.close.next(null);
  }

  private updateResource() {
    const resource = this.data.podController || this.data.cronJob;
    const containers = get(resource, this.containerPath, []).map(
      (item: Container) => {
        if (item.name !== this.data.containerName) {
          return item;
        } else {
          return this.targetContainer;
        }
      },
    );
    const part = set({}, this.containerPath, containers);
    return this.k8sApi.patchResource({
      cluster: this.data.baseParams.cluster,
      type: this.resourceType,
      resource,
      part,
    });
  }
}
