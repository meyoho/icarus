import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Service, Workload } from 'app/typings';
import { getDefaultService } from 'app_user/features/service/util';

@Component({
  templateUrl: 'dialog.html',
})
export class AddServiceFormDialogComponent {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  service: Service;
  close = new EventEmitter<Service>();
  workloads: Workload[] = [];

  isUpdate: boolean;
  isUpdateApp: boolean;

  constructor(
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(DIALOG_DATA)
    public readonly dialogData: {
      data?: Service;
      namespace: string;
      workloads?: Workload[];
      existedNames?: string[]; // 创建/更新应用，根据应用内已存在的 Service 做重名校验
      isUpdateApp: boolean; // 更新应用时，不允许更新某些字段
      defaultName?: string; // 应用-快速创建，默认使用 workload 名称作为 Service 名称
      disableWorkloadSelect?: boolean; // 应用-快速创建，不需要选择 workload，需要禁用对应 formControl
      disableHeadless?: boolean; // 禁止关闭 “虚拟IP” 选项
    },
  ) {
    this.isUpdate = !!dialogData.data;
    this.isUpdateApp = !!dialogData.isUpdateApp;
    this.service =
      dialogData.data ||
      getDefaultService(dialogData.namespace, dialogData.defaultName || '');
    this.workloads = dialogData.workloads || [];
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    if (
      !this.isUpdateApp &&
      this.dialogData.existedNames &&
      this.dialogData.existedNames.includes(this.service.metadata.name)
    ) {
      this.message.error(this.translate.get('resource_name_already_existed'));
      return;
    }
    this.close.next(this.service);
  }

  cancel() {
    this.close.next();
  }
}
