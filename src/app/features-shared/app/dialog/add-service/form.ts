import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { groupBy } from 'ramda';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Environments,
  Service,
  ServiceFormModel,
  ServiceTypeEnum,
  ServiceTypeMeta,
  Workload,
} from 'app/typings';
import { K8S_SERVICE_NAME, TYPE } from 'app/utils';

interface WorkloadOption {
  name: string;
  value: string;
  kind: string;
}
interface GroupedWorkloadOptions {
  [key: string]: WorkloadOption[];
}
@Component({
  selector: 'rc-k8s-service-form',
  templateUrl: 'form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddServiceFormComponent
  extends BaseResourceFormGroupComponent<Service, ServiceFormModel>
  implements OnChanges, OnInit {
  @ValueHook(function (items: Workload[]) {
    this.workloadOptions = this.getWorkloadNameOptions(items);
  })
  @Input()
  workloads: Workload[];

  @Input() isUpdate: boolean;
  @Input() disableWorkloadSelect: boolean;
  @Input() disableHeadless: boolean;

  resourceNamePattern = K8S_SERVICE_NAME;
  workloadOptions: GroupedWorkloadOptions = {};
  private readonly onDestroy$ = new Subject<void>();

  constructor(
    injector: Injector,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('virtualIp')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(value => {
        const internetAccess = this.form.get('internetAccess').value;
        if (value === false && internetAccess === true) {
          this.form.get('internetAccess').setValue(false, {
            emitEvent: true,
          });
        }
      });
    this.form
      .get('workloadName')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(value => {
        if (!!value && !this.form.get('metadata.name').value) {
          this.form
            .get('metadata.name')
            .setValue(value.slice(Math.max(0, value.indexOf('-') + 1)), {
              emitEvent: true,
            });
        }
      });
  }

  ngOnChanges({ disableWorkloadSelect }: SimpleChanges) {
    if (
      this.form &&
      disableWorkloadSelect &&
      disableWorkloadSelect.currentValue === true
    ) {
      this.form.get('workloadName').disable({
        emitEvent: false,
      });
    }
  }

  workloadTrackByFn(item: WorkloadOption) {
    return item.value;
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel(): ServiceFormModel {
    return {
      virtualIp: true,
      internetAccess: false,
      sessionPersistence: false,
      spec: {
        type: ServiceTypeEnum.ClusterIP,
        ports: [],
      },
    };
  }

  adaptFormModel(formModel: ServiceFormModel): Service {
    const {
      workloadName,
      virtualIp,
      internetAccess,
      sessionPersistence,
      ...resource
    } = formModel;
    const selectorKey = `service.${this.env.LABEL_BASE_DOMAIN}/name`;
    if (workloadName) {
      if (resource.spec.selector) {
        resource.spec.selector[selectorKey] = workloadName;
      } else {
        resource.spec.selector = {
          [selectorKey]: workloadName,
        };
      }
    } else if (resource.spec.selector) {
      delete resource.spec.selector[selectorKey];
    }

    if (virtualIp) {
      delete resource.spec.clusterIP;
    } else {
      resource.spec.clusterIP = 'None';
    }

    if (internetAccess) {
      resource.spec.type = 'NodePort';
    } else {
      resource.spec.type = 'ClusterIP';
    }

    if (sessionPersistence) {
      resource.spec.sessionAffinity = 'ClientIP';
    } else {
      resource.spec.sessionAffinity = 'None';
    }

    return Object.assign({}, ServiceTypeMeta, resource);
  }

  adaptResourceModel(resource: Service): ServiceFormModel {
    if (!resource) {
      return {};
    }
    const workloadName = get(
      resource,
      ['spec', 'selector', `service.${this.env.LABEL_BASE_DOMAIN}/name`],
      '',
    );

    return {
      ...resource,
      virtualIp: get(resource, ['spec', 'clusterIP']) !== 'None',
      internetAccess: get(resource, ['spec', TYPE]) === 'NodePort',
      sessionPersistence:
        get(resource, ['spec', 'sessionAffinity']) === 'ClientIP',
      workloadName,
    };
  }

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control(
        {
          value: '',
        },
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern(K8S_SERVICE_NAME.pattern),
        ],
      ),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const spec = this.fb.group({
      ports: this.fb.control(null),
      selector: [],
    });

    return this.fb.group({
      virtualIp: this.fb.control([true]),
      internetAccess: this.fb.control([false]),
      sessionPersistence: this.fb.control([false]),
      workloadName: this.fb.control(
        {
          value: '',
          disabled: this.disableWorkloadSelect,
        },
        [Validators.required],
      ),
      metadata,
      spec,
    });
  }

  getWorkloadNameOptions(workloads: Workload[]): GroupedWorkloadOptions {
    const groupByKind = groupBy((item: WorkloadOption) => item.kind);
    return workloads.length > 0
      ? groupByKind(
          workloads.map(item => ({
            name: item.metadata.name,
            value: `${item.kind.toLowerCase()}-${item.metadata.name}`,
            kind: item.kind.toLowerCase(),
          })),
        )
      : {};
  }
}
