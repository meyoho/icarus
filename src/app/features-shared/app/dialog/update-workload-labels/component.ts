import {
  JSONPatchOp,
  K8sApiService,
  PatchOperation,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ResourceType, Workload } from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

/**
 * 更新容器组标签：{Deployment}.spec.template.metadata.labels
 */
@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateWorkloadLabelsDialogComponent {
  @Output()
  close = new EventEmitter<boolean>();

  @ViewChild(NgForm, { static: true })
  form: NgForm;

  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      prefixPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  submitting = false;
  labels: StringMap;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    @Inject(DIALOG_DATA)
    private readonly data: {
      podController: Workload;
      baseParams: {
        cluster: string;
        namespace: string;
      };
    },
    private readonly translate: TranslateService,
  ) {
    this.labels = cloneDeep(this.data.podController?.metadata?.labels ?? {});
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;

    this.k8sApi
      .patchResource({
        cluster: this.data.baseParams.cluster,
        type: this.data.podController.kind.toUpperCase() as ResourceType,
        resource: this.data.podController,
        operation: PatchOperation.JSON,
        part: [
          {
            op: JSONPatchOp.Replace,
            path: '/metadata/labels',
            value: this.labels,
          },
        ],
      })
      .pipe(catchError(() => of(null)))
      .subscribe(res => {
        if (res) {
          this.close.next(true);
        }
        this.submitting = false;
        this.cdr.markForCheck();
      });
  }

  cancel() {
    this.close.next(null);
  }
}
