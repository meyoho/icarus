import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep, get, set } from 'lodash-es';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {
  Container,
  CronJob,
  RESOURCE_TYPES,
  ResourceType,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';

import { getContainerPath } from '../../utils';

@Component({
  templateUrl: './template.html',
})
export class UpdateResourceSizeDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();

  @ViewChild(NgForm, { static: true })
  private readonly form: NgForm;

  submitting = false;
  targetContainer: Container;

  get cluster() {
    return this.data.baseParams.cluster;
  }

  get namespace() {
    return this.data.baseParams.namespace;
  }

  private containerPath: string;
  private resourceType: ResourceType;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA)
    public data: {
      podController?: Workload;
      cronJob?: CronJob;
      containerName: string;
      baseParams: WorkspaceBaseParams;
    },
  ) {}

  ngOnInit() {
    let resource: Workload | CronJob;
    let podSpecPath = 'spec.template.spec';
    if (this.data.podController) {
      this.resourceType = this.data.podController.kind.toUpperCase() as ResourceType;
      resource = this.data.podController;
    } else if (this.data.cronJob) {
      this.resourceType = RESOURCE_TYPES.CRON_JOB;
      resource = this.data.cronJob;
      podSpecPath = 'spec.jobTemplate.spec.template.spec';
    }
    this.containerPath = getContainerPath(
      resource,
      podSpecPath,
      this.data.containerName,
    );
    this.targetContainer = cloneDeep(
      get(resource, this.containerPath, []).find(
        (c: Container) => c.name === this.data.containerName,
      ),
    );
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;

    this.updateResource()
      .pipe(catchError(() => of(null)))
      .subscribe(response => {
        if (response) {
          this.close.next(true);
        }
        this.submitting = false;
        this.cdr.markForCheck();
      });
  }

  cancel() {
    this.close.next(null);
  }

  private updateResource() {
    const resource = this.data.podController || this.data.cronJob;
    const containers = get(resource, this.containerPath, []).map(
      (item: Container) => {
        if (item.name !== this.data.containerName) {
          return item;
        } else {
          return this.targetContainer;
        }
      },
    );
    const part = set({}, this.containerPath, containers);
    return this.k8sApi.patchResource({
      cluster: this.data.baseParams.cluster,
      type: this.resourceType,
      resource,
      part,
    });
  }
}
