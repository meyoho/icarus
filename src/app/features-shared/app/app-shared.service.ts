import { K8sApiService, StringMap, publishRef } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, first, map, tap } from 'rxjs/operators';

import { ImageDialogComponent } from 'app/features-shared/image/dialog/component';
import { ImageService } from 'app/features-shared/image/image.service';
import { ImageSelectorContext } from 'app/features-shared/image/image.type';
import { LocalImageSelectorDataContext } from 'app/features-shared/image/local-image-selector-data-context';
import { LimitRange, RESOURCE_TYPES, WorkspaceBaseParams } from 'app/typings';

@Injectable()
export class AppSharedService {
  private defaultLimit$: Observable<StringMap>;
  private imageSelectorDataContext: ImageSelectorContext;
  constructor(
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService,
    private readonly imageService: ImageService,
  ) {}

  selectImage(workspaceParams: WorkspaceBaseParams) {
    this.imageSelectorDataContext = new LocalImageSelectorDataContext(
      this.imageService,
    );
    this.imageSelectorDataContext.params = workspaceParams;
    const dialogRef = this.dialogService.open(ImageDialogComponent, {
      size: DialogSize.Big,
      data: { context: this.imageSelectorDataContext },
    });

    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  getDefaultLimitResources(
    params: {
      cluster: string;
      namespace: string;
    },
    cached = false,
  ) {
    if (cached && this.defaultLimit$) {
      return this.defaultLimit$;
    }
    this.defaultLimit$ = this.k8sApi
      .getResource({
        type: RESOURCE_TYPES.LIMIT_RANGE,
        cluster: params.cluster,
        namespace: params.namespace,
        name: 'default',
      })
      .pipe(
        map((res: LimitRange) => {
          return res.spec.limits[0].default;
        }),
        catchError(() =>
          of({
            cpu: '1',
            memory: '512Mi',
          }),
        ),
        publishRef(),
      );
    return this.defaultLimit$;
  }
}
