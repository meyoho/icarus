import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { VolumeMount } from 'app/typings';

@Component({
  selector: 'rc-volume-mounts-preview',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeMountsPreviewComponent {
  @Input() volumeMounts: VolumeMount[] = [];
}
