import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ContainerPort } from 'app/typings';

@Component({
  selector: 'rc-container-ports-preview',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerPortsPreviewComponent {
  @Input() ports: ContainerPort[] = [];
}
