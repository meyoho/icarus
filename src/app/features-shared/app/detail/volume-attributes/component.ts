import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { safeDump } from 'js-yaml';

import { Volume, VolumeTypeEnum } from 'app/typings';

import { AVAILABLE_VOLUME_TYPES, getVolumeType } from '../../utils';
@Component({
  selector: 'rc-volume-attributes',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeAttributesComponent {
  @Input() volume: Volume;
  getVolumeType = getVolumeType;
  VolumeTypeEnum = VolumeTypeEnum;

  getNameOrPath(volume: Volume) {
    switch (getVolumeType(volume)) {
      case VolumeTypeEnum.persistentVolumeClaim:
        return volume[VolumeTypeEnum.persistentVolumeClaim].claimName;
      case VolumeTypeEnum.hostPath:
        return volume[VolumeTypeEnum.hostPath].path;
      case VolumeTypeEnum.configMap:
        return volume[VolumeTypeEnum.configMap].name;
      case VolumeTypeEnum.secret:
        return volume[VolumeTypeEnum.secret].secretName;
      default:
        return '';
    }
  }

  hasConfigItems(volume: Volume) {
    return (
      (volume[VolumeTypeEnum.configMap] &&
        volume[VolumeTypeEnum.configMap].items) ||
      (volume[VolumeTypeEnum.secret] && volume[VolumeTypeEnum.secret].items)
    );
  }

  allowDisplay(volume: Volume) {
    return AVAILABLE_VOLUME_TYPES.includes(getVolumeType(volume));
  }

  getYaml(volume: Volume) {
    return safeDump(volume[getVolumeType(volume)]).trim();
  }

  getConfigItems(volume: Volume) {
    if (volume[VolumeTypeEnum.configMap]) {
      return volume[VolumeTypeEnum.configMap].items;
    } else if (volume[VolumeTypeEnum.secret]) {
      return volume[VolumeTypeEnum.secret].items;
    } else {
      return [];
    }
  }
}
