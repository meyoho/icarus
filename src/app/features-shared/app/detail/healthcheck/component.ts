import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { get } from 'lodash-es';

import { Probe } from 'app/typings';

// TODO: refactor

enum ProbeKinds {
  livenessProbe = 'livenessProbe',
  readinessProbe = 'readinessProbe',
}
@Component({
  selector: 'rc-container-healthcheck-display',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerHealthcheckDisplayComponent implements OnChanges {
  @Input()
  probe: Probe;

  @Input()
  kind: ProbeKinds;

  viewModel: any;

  constructor(private readonly cdr: ChangeDetectorRef) {}

  ngOnChanges({ probe }: SimpleChanges): void {
    if (probe) {
      try {
        this.viewModel = this.adaptViewModel(this.probe);
        this.cdr.markForCheck();
      } catch (e) {
        // parse error ?

        console.error(e);
        this.viewModel = null;
      }
    }
  }

  private adaptViewModel(rawProbe: Probe) {
    if (!rawProbe) {
      return null;
    }
    const viewModel: any = {
      initialDelaySeconds: rawProbe.initialDelaySeconds,
      periodSeconds: rawProbe.periodSeconds,
      timeoutSeconds: rawProbe.timeoutSeconds,
      successThreshold: rawProbe.successThreshold || 0,
      failureThreshold: rawProbe.failureThreshold || 0,
    };
    if (rawProbe.tcpSocket) {
      viewModel.protocol = 'TCP';
      viewModel.port = rawProbe.tcpSocket.port;
    } else if (rawProbe.httpGet) {
      viewModel.protocol = 'HTTP';
      viewModel.port = rawProbe.httpGet.port;
      viewModel.scheme = rawProbe.httpGet.scheme;
      viewModel.path = rawProbe.httpGet.path;
      viewModel.headers = get(rawProbe, 'httpGet.httpHeaders', []);
    } else {
      viewModel.protocol = 'EXEC';
      viewModel.commands = (get(rawProbe, 'exec.command', []) as string[]).map(
        command => ({
          command,
        }),
      );
    }
    return viewModel;
  }

  getResourceNameByType(type: string) {
    switch (type) {
      case 'livenessProbe':
        return 'k8s_service_liveness_health_check';
      case 'readinessProbe':
        return 'k8s_service_readiness_health_check';
      default:
        return 'data';
    }
  }
}
