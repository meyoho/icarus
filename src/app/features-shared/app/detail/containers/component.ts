// tslint:disable: no-output-on-prefix
import {
  K8sApiService,
  StringMap,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { find, get } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, pluck } from 'rxjs/operators';

import { getPodShortName } from 'app/features-shared/pod/util';
import { TerminalService } from 'app/services/api/terminal.service';
import {
  Container,
  ContainerStatus,
  EXCLUDE_LOG_PATH_KEY,
  EnvVar,
  JobTemplateSpec,
  LOG_PATH_KEY,
  Pod,
  RESOURCE_TYPES,
  ResourceType,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { getContainerResourceLimit } from 'app_user/features/pod/util';

import { getResourceRequirementsAsLabels } from '../../utils';

interface ContainerOverall extends Container {
  isInit?: boolean;
}

@Component({
  selector: 'rc-pod-controller-containers',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  providers: [DetailDataService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerContainersComponent implements OnChanges, OnInit {
  @Input()
  isUpdateAllowed = false;

  // 是否有对当前 container 所在 workload 的 update 权限
  @Input()
  podController: Workload | JobTemplateSpec;

  // pod详情页，传入此数据，据此做一些逻辑区分
  @Input()
  readonly pod: Pod;

  // 允许暴露的操作，包括 更新镜像版本、更新资源配额、查看日志、exec，默认显示全部
  @Input()
  actions: string[] = ['image', 'resource', 'log', 'exec'];

  @Input()
  baseParams: WorkspaceBaseParams;

  @Output()
  onUpdateImageTag = new EventEmitter<string>();

  @Output()
  onUpdateResourceSize = new EventEmitter<string>();

  @Output()
  onOpenTerminal = new EventEmitter<{
    pods: string; // pod names separated by ;
    selectedPod: string;
    container: string;
  }>();

  @Output() onOpenLog = new EventEmitter<{
    pod: string;
    container: string;
  }>();

  private matchLabels: StringMap;

  execPermissions$: Observable<Record<string, boolean>>;

  containers: ContainerOverall[];
  selectedContainer: Container;
  selectedTabIndex = 0;

  initialized = false;
  pods: Pod[];
  podsLoading: boolean;
  podsLoaded: boolean;

  getPodShortName = getPodShortName;
  LOG_PATH_KEY = LOG_PATH_KEY;
  EXCLUDE_LOG_PATH_KEY = EXCLUDE_LOG_PATH_KEY;
  getResourceRequirementsAsLabels = getResourceRequirementsAsLabels;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly terminal: TerminalService,
    public translate: TranslateService,
  ) {
    this.getContainerStatus = this.getContainerStatus.bind(this);
  }

  ngOnChanges({ podController, pod }: SimpleChanges): void {
    if (podController?.currentValue || pod?.currentValue) {
      this.onInputChange();
    }
  }

  ngOnInit() {
    this.execPermissions$ = this.terminal.getPermissions({
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
    });
  }

  private onInputChange() {
    const containers = this.pod
      ? get(this.pod, 'spec.containers', [])
      : get(this.podController, 'spec.template.spec.containers', []);
    const initContainers: Container[] = this.pod
      ? get(this.pod, 'spec.initContainers', [])
      : get(this.podController, 'spec.template.spec.initContainers', []);

    this.containers = [
      ...initContainers.map(container => ({ isInit: true, ...container })),
      ...containers,
    ];
    this.matchLabels = get(this.podController, 'spec.selector.matchLabels', {});
    this.selectedContainer = this.containers[this.selectedTabIndex];
    this.initialized = true;
  }

  getPods() {
    const labelSelector = matchLabelsToString(this.matchLabels);
    this.podsLoading = true;
    this.k8sApi
      .getResourceList<Pod>({
        type: RESOURCE_TYPES.POD,
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        queryParams: {
          labelSelector,
        },
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      )
      .subscribe(items => {
        this.pods = items;
        this.podsLoaded = true;
        this.podsLoading = false;
        this.cdr.markForCheck();
      });
  }

  shouldShowAction(action: string) {
    return this.actions.includes(action);
  }

  getLogPathContext(c: Container) {
    const envs = c.env || [];
    const logPathEnv = envs.find((item: EnvVar) => {
      return !item.valueFrom && item.name === LOG_PATH_KEY;
    });
    const logPath =
      logPathEnv && logPathEnv.value ? logPathEnv.value.split(',') : [];
    const excludeLogPathEnv = envs.find((item: EnvVar) => {
      return !item.valueFrom && item.name === EXCLUDE_LOG_PATH_KEY;
    });
    const excludeLogPath =
      excludeLogPathEnv && excludeLogPathEnv.value
        ? excludeLogPathEnv.value.split(',')
        : [];
    return {
      logPath,
      excludeLogPath,
    };
  }

  // ===== actions =====

  canEXEC(pod: Pod, container: Container): boolean {
    const podStatus = get(pod, 'status.containerStatuses', null);
    if (!podStatus) {
      return false;
    }
    const containerStatus = find(podStatus, co => co.name === container.name);

    return !!get(containerStatus, 'state.running', null);
  }

  canNotEXECAll(pods: Pod[], container: Container): boolean {
    if (pods) {
      return pods.every(pod => {
        return !this.canEXEC(pod, container);
      });
    }
  }

  openTerminal(pods: Pod[], container: Container, selected?: Pod) {
    const podNames = pods.map(pod => get(pod, 'metadata.name', '')).join(';');
    const selectedName = get(selected, 'metadata.name', '');
    const containerName = container.name;
    this.onOpenTerminal.emit({
      pods: podNames,
      selectedPod: selectedName || '',
      container: containerName,
    });
  }

  openLog(pod: Pod, container: Container) {
    this.onOpenLog.emit({
      pod: pod.metadata.name,
      container: container.name,
    });
  }

  updateImageTag(container: Container) {
    this.onUpdateImageTag.emit(container.name);
  }

  updateResourceSize(container: Container) {
    this.onUpdateResourceSize.emit(container.name);
  }

  trackByFn(_i: number, c: Container) {
    return c.name;
  }

  onTabIndexChange(index: number) {
    this.selectedTabIndex = index;
    this.selectedContainer = this.containers[index];
  }

  onMenuShow() {
    this.getPods();
  }

  onMenuHide() {
    this.podsLoaded = false;
  }

  getContainerStatus(container: Container) {
    const { status } = this.pod;
    const { containerStatuses = [], initContainerStatuses = [] } = status;
    const statuses = containerStatuses.concat(...initContainerStatuses);
    return statuses.find(status => status.name === container.name) || {};
  }

  getContainerResourceLimit = getContainerResourceLimit;

  getContainerStatusState(status: ContainerStatus) {
    const state = get(status, 'state', {});
    const [stateKey] = Object.keys(state);
    if (!stateKey) {
      return {};
    }
    return {
      state: stateKey,
      reason: get(state, [stateKey, 'reason']),
      message: get(state, [stateKey, 'message']),
    };
  }

  statusStateFormat({ state, reason }: { state: string; reason?: string }) {
    return state ? state + (reason ? `(${reason})` : '') : '';
  }
}
