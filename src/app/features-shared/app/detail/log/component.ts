import { CodeEditorIntl } from '@alauda/code-editor';
import {
  K8sApiService,
  ObservableInput,
  StringMap,
  TranslateService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { get, isEqual } from 'lodash-es';
import { editor } from 'monaco-editor';
import { MonacoProviderService } from 'ng-monaco-editor';
import { Observable, ReplaySubject, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  mergeScan,
  pluck,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';
import { FormatUtcStrPipe } from 'app/shared/pipes/format-utc-str.pipe';
import {
  Container,
  ContainerLog,
  ContainerLogParams,
  ContainerStatus,
  LogInfo,
  LogLine,
  LogSelection,
  Pod,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { FALSE, STATUS, TRUE } from 'app/utils';

export interface LogSelectionParams {
  logFilePosition: string;
  referenceTimestamp: string;
  referenceLineNum: number | string;
  offsetFrom: number;
  offsetTo: number;
}

interface ContainerOption {
  initContainer?: boolean; // 是否是初始化容器
  name: string;
  state: string; // https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstate-v1-core
  // one of 'running', 'terminated', 'waiting'
}

type LogSelectionParamScanner = (prev: LogSelection) => LogSelectionParams;

const logsPerView = 100;
const maxLogSize = 2e9;
// Load logs from the beginning of the log file. This matters only if the log file is too large to
// be loaded completely.
const beginningOfLogFile = 'beginning';
// Load logs from the end of the log file. This matters only if the log file is too large to be
// loaded completely.
const endOfLogFile = 'end';
const oldestTimestamp = 'oldest';
const newestTimestamp = 'newest';

@Component({
  selector: 'rc-pods-log',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PodsLogComponent implements OnDestroy, OnInit {
  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true)
  private readonly baseParams$: Observable<WorkspaceBaseParams>;

  @Input() matchLabels: StringMap;
  @ObservableInput(true)
  private readonly matchLabels$: Observable<StringMap>;

  @Input()
  initialState: {
    pod: string;
    container: string;
  };

  // labelSelector 与 fieldSelector 传任意一个即可，优先按照 fieldSelector 处理
  @Input() fieldSelector: string;
  @ObservableInput(true, null)
  readonly fieldSelector$: Observable<string>;

  private readonly labelSelector$ = this.matchLabels$.pipe(
    filter(o => !!o),
    distinctUntilChanged(isEqual),
    map(matchLabels => matchLabelsToString(matchLabels)),
    startWith(''),
  );

  private readonly fetchParams$ = combineLatest([
    this.baseParams$,
    this.labelSelector$,
    this.fieldSelector$,
  ]).pipe(
    map(([baseParams, labelSelector, fieldSelector]) => {
      return {
        ...baseParams,
        queryParams: fieldSelector ? { fieldSelector } : { labelSelector },
      };
    }),
  );

  private intervalTimer: any;
  private editor: editor.IStandaloneCodeEditor;
  private logResponse$: Observable<ContainerLog>;
  private logResponse: ContainerLog; // Latest fetched log detail
  private readonly logSelectionParamScanner$ = new ReplaySubject<
    LogSelectionParamScanner
  >(1);

  selectedPod: Pod;
  selectedContainer: ContainerOption;

  pods: Pod[];
  containers: ContainerOption[];

  logs$: Observable<string>;
  logInfo$: Observable<LogInfo>;
  logsReady$: Observable<boolean>;
  onDestroy$ = new Subject<void>();

  monacoOptions: editor.IEditorConstructionOptions = {
    wordWrap: 'on',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  pollNewest = true;
  initialized: boolean;

  ngOnInit() {
    this.fetchParams$
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(({ cluster, namespace, queryParams }) => {
          return this.k8sApi
            .getResourceList<Pod>({
              type: RESOURCE_TYPES.POD,
              cluster,
              namespace,
              queryParams,
            })
            .pipe(
              pluck('items'),
              catchError(() => of([])),
            );
        }),
      )
      .subscribe(pods => {
        this.pods = pods;
        this.setupLogResponse$();
        this.setupInitialState();
        this.setupTimer();
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    clearInterval(this.intervalTimer);
    this.onDestroy$.next();
  }

  onPodNameSelect(name: string) {
    this.selectedPod = this.pods.find(
      (item: Pod) => item.metadata.name === name,
    );
    this.resetContainerNamesOptions();
    if (this.containers.length > 0) {
      this.selectedContainer = this.containers[0];
    }
    this.loadNewest();
  }

  onContainerNameSelect(containerName: string) {
    this.selectedContainer = this.containers.find((item: ContainerOption) => {
      return item.name === containerName;
    });
    this.loadNewest();
  }

  loadNewest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: endOfLogFile,
      referenceTimestamp: newestTimestamp,
      referenceLineNum: 0,
      offsetFrom: maxLogSize,
      offsetTo: maxLogSize + logsPerView,
    }));
  }

  loadOldest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: beginningOfLogFile,
      referenceTimestamp: oldestTimestamp,
      referenceLineNum: 0,
      offsetFrom: -maxLogSize - logsPerView,
      offsetTo: -maxLogSize,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the past.
   * @export
   */
  loadOlder() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetFrom - logsPerView,
      offsetTo: prev.offsetFrom,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the future.
   * @export
   */
  loadNewer() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetTo,
      offsetTo: prev.offsetTo + logsPerView,
    }));
    this.pollNewest = false;
  }

  onFindClicked() {
    return this.checkActionAndRun('actions.find');
  }

  onMonacoEditorChanged(codeEditor: editor.IStandaloneCodeEditor) {
    this.editor = codeEditor;
    this.scrollToBottom();
  }

  filterContainerOption(options: ContainerOption[], isInitContainer: boolean) {
    const filteredOptions = options
      ? options.filter(c => !!c.initContainer === isInitContainer)
      : [];
    return filteredOptions.length ? filteredOptions : null;
  }

  private resetContainerNamesOptions() {
    this.containers = this.selectedPod
      ? this.getContainerOptions(this.selectedPod)
      : [];
  }

  private getContainerOptions(pod: Pod) {
    const initContainers = (pod.spec.initContainers || []).map(
      (c: Container) => ({
        initContainer: true,
        name: c.name,
        state: this.getContainerState(this.selectedPod, c.name, true),
      }),
    );
    const containers = pod.spec.containers.map(c => ({
      name: c.name,
      state: this.getContainerState(this.selectedPod, c.name),
    }));

    return [...initContainers, ...containers];
  }

  private getContainerState(
    pod: Pod,
    containerName: string,
    isInitContainer = false,
  ) {
    const status: ContainerStatus = get(
      pod,
      [STATUS, isInitContainer ? 'initContainerStatuses' : 'containerStatuses'],
      [],
    ).find(s => s.name === containerName);
    return status ? Object.keys(status)[0] : 'waiting';
  }

  private checkActionAndRun(actionName: string) {
    const action = this.editor && this.editor.getAction(actionName);
    return action && action.run();
  }

  private formatAllLogs(logs: LogLine[]) {
    if (!logs || logs.length === 0) {
      logs = [
        {
          timestamp: new Date().toUTCString(),
          content: this.translate.get('zero_state_hint', {
            resourceName: this.translate.get('logs'),
          }),
        },
      ];
    }
    return logs.map(line => this.formatLine(line));
  }

  /**
   * Formats the given log line as raw HTML to display to the user.
   */
  private formatLine(line: LogLine) {
    const formatter = new FormatUtcStrPipe();
    return `${formatter.transform(line.timestamp)} ${line.content}`;
  }

  private scrollToBottom() {
    this.zone.runOutsideAngular(() => {
      setTimeout(() => {
        if (this.editor && this.logResponse && this.logResponse.logs) {
          this.editor.revealLine(this.logResponse.logs.length);
        }
      });
    });
  }

  private setupTimer() {
    this.intervalTimer = setInterval(() => {
      if (this.pollNewest) {
        this.loadNewest();
      }
    }, 10000);
  }

  private setupInitialState() {
    if (this.pods.length > 0) {
      this.selectedPod =
        this.initialState && this.initialState.pod
          ? this.pods.find(
              (item: Pod) => item.metadata.name === this.initialState.pod,
            )
          : this.pods[0];
      this.resetContainerNamesOptions();
      this.selectedContainer =
        this.initialState && this.initialState.container
          ? this.containers.find(
              (item: ContainerOption) =>
                item.name === this.initialState.container,
            )
          : this.containers[0];
      this.loadNewest();
    }
  }

  private setupLogResponse$() {
    this.logResponse$ = this.logSelectionParamScanner$.pipe(
      takeUntil(this.onDestroy$),
      mergeScan<LogSelectionParamScanner, ContainerLog>(
        (prev, scannerFn) => {
          const params: ContainerLogParams = {
            previous:
              this.selectedContainer.state === 'terminated' ? TRUE : FALSE,
          };
          const selectionParams: LogSelectionParams = scannerFn(
            prev && prev.selection,
          );
          Object.keys(selectionParams).forEach(
            (key: keyof LogSelectionParams) => {
              if (selectionParams[key]) {
                params[key] = '' + selectionParams[key];
              }
            },
          );
          return this.acpApi.getContainerLog({
            cluster: this.baseParams.cluster,
            namespace: this.baseParams.namespace,
            pod: this.selectedPod.metadata.name,
            container: this.selectedContainer.name,
            queryParams: params,
          });
        },
        null,
        1,
      ),
      tap(logDetails => {
        this.logResponse = logDetails;
      }),
      publishRef(),
    );

    this.logs$ = this.logResponse$.pipe(
      map(res => this.formatAllLogs(res.logs).join('\n')),
    );

    this.logInfo$ = this.logResponse$.pipe(map(res => res.info));

    this.logsReady$ = this.logInfo$.pipe(
      filter(logInfo => !!logInfo),
      map(logInfo => logInfo.fromDate && logInfo.fromDate !== '0'),
    );
  }

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly zone: NgZone,
    private readonly translate: TranslateService,
    private readonly acpApi: AcpApiService,
    private readonly k8sApi: K8sApiService,
    public codeEditorIntl: CodeEditorIntl,
    public monacoProvider: MonacoProviderService,
  ) {
    this.pods = [];
    this.containers = [];
  }
}
