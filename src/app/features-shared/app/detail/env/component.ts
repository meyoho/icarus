import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { get } from 'lodash-es';

import {
  EnvFromSourceFormModel,
  adaptEnvFromSourceResource,
} from 'app/features-shared/app/utils';
import {
  Container,
  EnvVar,
  PodTemplateSpec,
  WorkspaceBaseParams,
} from 'app/typings';

interface ContainerViewModel {
  isInitContainer?: boolean;
  name: string;
  envVars: EnvVar[];
  envFroms: EnvFromSourceFormModel[];
}

export interface EnvUpdateParams {
  containerName: string;
  isInitContainer: boolean;
  type: 'env' | 'envFrom';
}

@Component({
  selector: 'rc-workload-detail-env',
  templateUrl: './template.html',
  styles: [
    `
      h3 {
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 14px;
        font-weight: 500;
        margin-bottom: 12px;
      }
      h3:not(:first-child) {
        margin-top: 12px;
      }
    `,
  ],
})
export class WorkloadDetailEnvComponent implements OnChanges {
  // 是否有更新权限
  @Input()
  canUpdate = true;

  // 隐藏更新按钮(任务历史页)
  @Input()
  hideUpdate = false;

  @Input()
  podTemplate: PodTemplateSpec;

  @Input()
  baseParams: WorkspaceBaseParams;

  @Input()
  isFederatedResource = false;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onUpdateEnv = new EventEmitter<{
    containerName: string;
    isInitContainer: boolean;
    type: 'env' | 'envFrom';
  }>();

  containers: ContainerViewModel[];

  ngOnChanges({ podTemplate }: SimpleChanges): void {
    if (podTemplate && podTemplate.currentValue) {
      const initContainers = get(
        this.podTemplate,
        'spec.initContainers',
        [],
      ).map((co: Container) => ({
        isInitContainer: true,
        name: co.name,
        envVars: this.getContainerEnvvarViewModelFromYaml(co),
        envFroms: adaptEnvFromSourceResource(co.envFrom || []),
      }));
      const containers = get(this.podTemplate, 'spec.containers', []).map(
        (co: Container) => ({
          name: co.name,
          envVars: this.getContainerEnvvarViewModelFromYaml(co),
          envFroms: adaptEnvFromSourceResource(co.envFrom || []),
        }),
      );
      this.containers = [...initContainers, ...containers];
    }
  }

  getContainerEnvvarViewModelFromYaml(container: Container) {
    return (container.env || []).filter(
      item =>
        ![
          '__ALAUDA_FILE_LOG_PATH__',
          '__ALAUDA_EXCLUDE_LOG_PATH__',
          '__ALAUDA_SERVICE_ID__',
          '__ALAUDA_SERVICE_NAME__',
          '__ALAUDA_APP_NAME__',
        ].includes(item.name),
    );
  }

  updateEnv(container: ContainerViewModel, type: 'env' | 'envFrom' = 'env') {
    this.onUpdateEnv.emit({
      containerName: container.name,
      isInitContainer: container.isInitContainer,
      type,
    });
  }
}
