import { TranslateKey, TypeMeta } from '@alauda/common-snippet';

import { TimePickrModel } from 'app/shared/components/timepickr/component';
import {
  CronHorizontalPodAutoscaler,
  CronHorizontalPodAutoscalerRule,
  HorizontalPodAutoscaler,
} from 'app/typings';

export enum HpaTypeEnum {
  HPA = 'HorizontalPodAutoscaler',
  CronHpa = 'CronHpa',
  None = 'None',
}

export const CronHorizontalPodAutoscalerMeta: TypeMeta = {
  apiVersion: 'tkestack.io/v1',
  kind: 'CronHPA',
};
export interface HpaConfigResource {
  hpa: HorizontalPodAutoscaler;
  cronHpa: CronHorizontalPodAutoscaler;
}
export interface CronHpaModel extends CronHorizontalPodAutoscalerRule {
  type: CronHpaTriggerRuleEnum;
}
export enum CronHpaTriggerRuleEnum {
  CRON = 'cron',
  CUSTOM = 'custom',
}
export interface CronHpaTriggerRuleModel {
  daysChosen?: WeekDayValueType[];
  timeChosen?: TimePickrModel;
  customRule?: string;
}

export const WEEK_DAY_VALUES = ['0', '1', '2', '3', '4', '5', '6', '*'];
export const WEEK_DAY_LABELS = [
  'sunday',
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'everyday',
];
export type WeekDayValueType = typeof WEEK_DAY_VALUES[number];

export type HpaV2ConfigTargetType = 'Utilization' | 'AverageValue' | 'Value';

export interface HpaV2Config {
  name: string;
  type: string;
  placeholder: TranslateKey;
  description: TranslateKey;
  target: HpaV2ConfigTarget;
}

export interface HpaV2ConfigTarget {
  type: HpaV2ConfigTargetType;
  key: string;
  defaultUnit: string;
  displayUnit?: string;
  pattern: string;
}
