import {
  K8sApiService,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { safeLoad } from 'js-yaml';
import { Observable, of } from 'rxjs';
import {
  catchError,
  first,
  map,
  pluck,
  shareReplay,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import {
  HpaConfigResource,
  HpaV2Config,
  WEEK_DAY_LABELS,
} from 'app/features-shared/app/detail/hpa/type';
import { FeatureGateApiService } from 'app/services/api/feature-gate.service';
import {
  ConfigMap,
  RESOURCE_TYPES,
  Workload,
  WorkloadType,
  WorkspaceBaseParams,
} from 'app/typings';
import { KUBE_PUBLIC_NAMESPACE } from 'app/utils';

import { UpdateHpaComponent } from './update-hpa/dialog';
import {
  HPA_CRON_CONDITION_REGEXP,
  HPA_CRON_DAYTIME_CHOSEN_REGEXP,
  HPA_CRON_TIME_CHOSEN_REGEXP,
  getMetricDescription,
  getMetricDisplayUnit,
  getMetricName,
  getMetricValue,
} from './util';

@Component({
  selector: 'rc-hpa',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HpaComponent implements OnInit, OnDestroy {
  @Input()
  autoScaler: HpaConfigResource;

  @Input()
  canUpdate: boolean;

  @Input()
  permissions: {
    update?: boolean;
  } = {};

  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true)
  baseParams$: Observable<WorkspaceBaseParams>;

  @Input()
  podController: Workload;

  @Input()
  workloadType: WorkloadType;

  @Output()
  update = new EventEmitter<HpaConfigResource>();

  hpaV2Config: HpaV2Config[];
  isCronSupport: boolean;
  isHpaV2: boolean;
  onDestroy$ = new EventEmitter<void>();

  getMetricName = getMetricName;
  getMetricValue = getMetricValue;
  getMetricDescription = getMetricDescription;
  getMetricDisplayUnit = getMetricDisplayUnit;

  isHpaV2$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.featureGateApi
        .getClusterGate(baseParams.cluster, 'hpav2')
        .pipe(pluck('status', 'enabled')),
    ),
    shareReplay(1),
    takeUntil(this.onDestroy$),
  );

  hpaV2Config$ = this.isHpaV2$.pipe(
    switchMap(isHpaV2 => {
      if (!isHpaV2) {
        return of([] as HpaV2Config[]);
      }
      return this.k8sApi
        .getResource<ConfigMap>({
          type: RESOURCE_TYPES.CONFIG_MAP,
          cluster: 'global',
          namespace: KUBE_PUBLIC_NAMESPACE,
          name: 'hpav2-config',
        })
        .pipe(
          map(resource => safeLoad(resource.data.config) as HpaV2Config[]),
          catchError(_ => of([] as HpaV2Config[])),
          shareReplay(1),
        );
    }),
    takeUntil(this.onDestroy$),
  );

  hasHpaResource(autoScaler: HpaConfigResource) {
    return !!(autoScaler?.cronHpa || autoScaler?.hpa);
  }

  constructor(
    public readonly translate: TranslateService,
    public readonly k8sApi: K8sApiService,
    private readonly featureGateApi: FeatureGateApiService,
    private readonly dialog: DialogService,
  ) {
    this.formatSchedule = this.formatSchedule.bind(this);
  }

  ngOnInit() {
    this.hpaV2Config$.subscribe(hpaV2Config => {
      this.hpaV2Config = hpaV2Config;
    });

    this.featureGateApi
      .getClusterGate(this.baseParams.cluster, 'cron-hpa')
      .subscribe(feature => {
        this.isCronSupport = feature.status.enabled;
      });

    this.isHpaV2$.subscribe(isHpaV2 => {
      this.isHpaV2 = isHpaV2;
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  formatSchedule(schedule: string): string {
    if (!HPA_CRON_CONDITION_REGEXP.test(schedule)) {
      return schedule;
    }
    const scheduleParts = schedule.split(' ').filter(i => !!i);
    const minuteFormatted = scheduleParts[0].padStart(2, '0');
    const hourFormatted = scheduleParts[1].padStart(2, '0');
    const daysInWeek = scheduleParts[4] || '';
    if (HPA_CRON_TIME_CHOSEN_REGEXP.test(schedule)) {
      return this.translate.get('trigger_everyday_with_time', {
        time: `${hourFormatted}:${minuteFormatted}`,
      });
    }
    if (HPA_CRON_DAYTIME_CHOSEN_REGEXP.test(schedule)) {
      const daysInWeekFormatted = daysInWeek
        .split(',')
        .sort((a, b) => {
          return +a - +b;
        })
        .map(val => {
          return this.translate.get(WEEK_DAY_LABELS[+val]);
        });
      return this.translate.get('trigger_with_daytime', {
        time: `${hourFormatted}:${minuteFormatted}`,
        days: daysInWeekFormatted,
      });
    }
    return schedule;
  }

  updateHpa() {
    const dialogRef = this.dialog.open(UpdateHpaComponent, {
      size: DialogSize.Large,
      data: {
        original: this.autoScaler,
        podController: this.podController,
        baseParams: this.baseParams,
        workloadType: this.workloadType,
        isCronSupport: this.isCronSupport,
        isHpaV2: this.isHpaV2,
        hpaV2Config: this.hpaV2Config,
      },
    });
    // eslint-disable-next-line sonarjs/no-identical-functions
    return dialogRef.componentInstance.finish.pipe(first()).subscribe(res => {
      dialogRef.close();
      this.update.next(res);
    });
  }
}
