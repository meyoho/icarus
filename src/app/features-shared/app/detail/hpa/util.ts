import { TranslateService } from '@alauda/common-snippet';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { get } from 'lodash-es';

import {
  HpaConfigResource,
  HpaTypeEnum,
  HpaV2Config,
} from 'app/features-shared/app/detail/hpa/type';
import {
  CronHorizontalPodAutoscaler,
  HorizontalPodAutoscaler,
  MetricSpec,
  RESOURCE_TYPES,
  WorkloadType,
} from 'app/typings';

import { unionMultiRegex } from '../../utils';

export function buildHpaUpdatePayload(params: {
  kind: HpaTypeEnum;
  hpaPayload: HorizontalPodAutoscaler;
  cronHpaPayload: CronHorizontalPodAutoscaler;
}): HpaConfigResource {
  switch (params.kind) {
    case HpaTypeEnum.CronHpa: {
      return {
        hpa: null,
        cronHpa: params.cronHpaPayload,
      };
    }
    case HpaTypeEnum.HPA: {
      return {
        hpa: params.hpaPayload,
        cronHpa: null,
      };
    }
    case HpaTypeEnum.None: {
      return { hpa: null, cronHpa: null };
    }
  }
}

const HPA_CRON_CONDITION_PREFIX = '[0-5]?[0-9] ([0-9]|1[0-9]|2[0-3]) \\* \\*';
const HPA_CRON_DAYTIME_SUFFIX = '([0-6,]*[0-6])';
const HPA_CRON_TIME_SUFFIX = '\\*';
export const HPA_CRON_CONDITION_REGEXP = new RegExp(
  `^${HPA_CRON_CONDITION_PREFIX} ${unionMultiRegex(
    HPA_CRON_TIME_SUFFIX,
    HPA_CRON_DAYTIME_SUFFIX,
  )}$`,
);
export const HPA_CRON_TIME_CHOSEN_REGEXP = new RegExp(
  `^${HPA_CRON_CONDITION_PREFIX} ${HPA_CRON_TIME_SUFFIX}$`,
);
export const HPA_CRON_DAYTIME_CHOSEN_REGEXP = new RegExp(
  `^${HPA_CRON_CONDITION_PREFIX} ${HPA_CRON_DAYTIME_SUFFIX}$`,
);

const TAPP_RESOURCE_PREFIX = 'tapp-';

// 当前仅需要在TApp时增加前缀
// reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=56985773
export function adaptHpaResourceName(
  workloadName: string,
  workloadType: WorkloadType,
) {
  return workloadType !== RESOURCE_TYPES.TAPP
    ? workloadName
    : TAPP_RESOURCE_PREFIX + workloadName;
}

// get metric unit from metric config
export function getUnitFromConfig(config: HpaV2Config) {
  return config?.target?.defaultUnit;
}

// get name from metric
export function getMetricName(metric: MetricSpec) {
  const type = metric.type.toLowerCase();
  return type === 'resource'
    ? metric.resource.name
    : get(metric, [type, 'metric', 'name']);
}

// value key decided by metric.target.type
export const METRIC_TARGET_TYPE_KEY_MAPPER: StringMap = {
  AverageValue: 'averageValue',
  Value: 'value',
  Utilization: 'averageUtilization',
};

// get value from metric, value should include unit
export function getMetricValue(metric: MetricSpec) {
  const { target } = get(metric, [metric.type.toLowerCase()]);
  return get(target, [METRIC_TARGET_TYPE_KEY_MAPPER[target.type]]) + '';
}

// return null when no placeholder
export function getMetricPlaceholder(
  metricName: string,
  metricConfigs: HpaV2Config[],
) {
  return (
    metricConfigs?.find(metric => metric.name === metricName)?.placeholder ||
    null
  );
}

export function getMetricDescription(
  metricName: string,
  [metricConfigs, translate]: [HpaV2Config[], TranslateService],
): string {
  const description = metricConfigs?.find(metric => metric.name === metricName)
    ?.description;
  return description ? translate.get(description) : metricName;
}

export function getMetricDisplayUnit(
  metricName: string,
  metricConfigs: HpaV2Config[],
) {
  return (
    metricConfigs?.find(metric => metric.name === metricName)?.target
      ?.displayUnit || ''
  );
}
