import { ObjectMeta, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { get } from 'lodash-es';

import {
  CronHorizontalPodAutoscalerMeta,
  HpaConfigResource,
  HpaTypeEnum,
  HpaV2Config,
} from 'app/features-shared/app/detail/hpa/type';
import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  CronHorizontalPodAutoscaler,
  CronHorizontalPodAutoscalerRule,
  HorizontalPodAutoscaler,
  Workload,
  WorkloadType,
  WorkspaceBaseParams,
} from 'app/typings';

import { adaptHpaResourceName, buildHpaUpdatePayload } from '../util';

@Component({
  templateUrl: 'dialog.html',
  styleUrls: ['dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateHpaComponent {
  // 指标调节form
  @ViewChild('indicatorForm', { static: true })
  indicatorForm: NgForm;

  // 定时调节form
  @ViewChild('cronForm', { static: false })
  cronForm: NgForm;

  submitting: boolean;
  podController: Workload;
  cluster: string;
  namespace: string;
  hpaFormModel: HorizontalPodAutoscaler;
  cronHpaRulesFormModel: CronHorizontalPodAutoscalerRule[];
  finish = new EventEmitter<HpaConfigResource>();
  updateType: HpaTypeEnum;
  HpaTypeEnum = HpaTypeEnum;
  workloadName: string;
  workloadType: WorkloadType;
  isCronSupport: boolean;
  isHpaV2: boolean;
  hpaV2Config: HpaV2Config[];

  constructor(
    @Inject(DIALOG_DATA)
    dialogData: {
      original: HpaConfigResource;
      podController: Workload;
      baseParams: WorkspaceBaseParams;
      workloadType: WorkloadType;
      isCronSupport: boolean;
      isHpaV2: boolean;
      hpaV2Config: HpaV2Config[];
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly acpApi: AcpApiService,
    private readonly messageService: MessageService,
    private readonly translateService: TranslateService,
  ) {
    this.podController = dialogData.podController;
    this.cluster = dialogData.baseParams.cluster;
    this.namespace = dialogData.baseParams.namespace;
    this.workloadName = dialogData.podController.metadata.name;
    this.workloadType = dialogData.workloadType;
    this.isCronSupport = dialogData.isCronSupport;
    this.isHpaV2 = dialogData.isHpaV2;
    this.hpaV2Config = dialogData.hpaV2Config;

    // 优先读取HPA
    // reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=56985773
    const hpaModel = get(dialogData, ['original', 'hpa']);
    const cronHpaModel = get(dialogData, ['original', 'cronHpa']);
    if (hpaModel) {
      this.hpaFormModel = hpaModel;
      this.updateType = HpaTypeEnum.HPA;
    } else if (cronHpaModel && this.isCronSupport) {
      this.cronHpaRulesFormModel = get(cronHpaModel, ['spec', 'crons']);
      this.updateType = HpaTypeEnum.CronHpa;
    } else {
      this.updateType = HpaTypeEnum.None;
    }
  }

  confirm = () => {
    if (!this.validateForm()) {
      return;
    }
    this.updateHpaResource(
      buildHpaUpdatePayload({
        kind: this.updateType,
        hpaPayload: this.hpaFormModel,
        cronHpaPayload: this.buildCronHpaPayloadFromCrons(
          this.cronHpaRulesFormModel,
        ),
      }),
    );
  };

  private validateForm(): boolean {
    if (this.updateType === HpaTypeEnum.HPA) {
      this.indicatorForm.onSubmit(null);
      return (
        this.checkMaxMinReplicas(this.hpaFormModel) &&
        this.checkMetricsNotNull(this.hpaFormModel) &&
        this.indicatorForm.valid
      );
    }
    if (this.updateType === HpaTypeEnum.CronHpa) {
      this.cronForm.onSubmit(null);
      return (
        this.cronForm.valid &&
        this.checkExistCronCondition(this.cronHpaRulesFormModel)
      );
    }
    return true;
  }

  private buildCronHpaPayloadFromCrons(
    crons: CronHorizontalPodAutoscalerRule[],
  ): CronHorizontalPodAutoscaler {
    const { name, namespace } = this.podController.metadata;
    const { apiVersion, kind } = this.podController;
    const metadata: ObjectMeta = {
      name: adaptHpaResourceName(name, this.workloadType),
      namespace,
    };
    const scaleTargetRef = {
      apiVersion,
      kind,
      name,
    };
    return {
      ...CronHorizontalPodAutoscalerMeta,
      metadata,
      spec: {
        crons,
        scaleTargetRef,
      },
    };
  }

  private checkMaxMinReplicas(resource: HorizontalPodAutoscaler) {
    if (resource.spec.maxReplicas < resource.spec.minReplicas) {
      this.messageService.error(
        this.translateService.get('hpa_replicas_check_error'),
      );
      return false;
    }
    return true;
  }

  private checkMetricsNotNull(resource: HorizontalPodAutoscaler) {
    if (this.isHpaV2 && !resource.spec.metrics?.length) {
      this.messageService.error(
        this.translateService.get('hpa_v2_metrics_null_error'),
      );
      return false;
    }
    return true;
  }

  private checkExistCronCondition(crons: CronHorizontalPodAutoscalerRule[]) {
    if (!Array.isArray(crons) || crons.length === 0) {
      this.messageService.error(
        this.translateService.get('cron_rules_cannot_empty'),
      );
      return false;
    }
    return true;
  }

  private updateHpaResource(payload: HpaConfigResource) {
    this.submitting = true;
    return this.acpApi
      .updateHpaResource({
        cluster: this.cluster,
        namespace: this.namespace,
        workloadName: this.workloadName,
        workloadType: this.workloadType.toLowerCase(),
        payload,
      })
      .subscribe(
        resource => {
          this.messageService.success(
            this.translateService.get('update_success'),
          );
          this.finish.emit(resource);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }
}
