import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  CronHpaTriggerRuleEnum,
  CronHpaTriggerRuleModel,
  WEEK_DAY_LABELS,
  WEEK_DAY_VALUES,
} from 'app/features-shared/app/detail/hpa/type';
import { cronValidator } from 'app/utils';

import { HPA_CRON_CONDITION_REGEXP } from '../../../../util';

const WEEK_DAY_OPTIONS = WEEK_DAY_LABELS.slice(0, -1).map((day, index) => ({
  value: WEEK_DAY_VALUES[index],
  label: day,
}));

@Component({
  selector: 'rc-hpa-cron-trigger-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HpaCronTriggerRuleComponent
  extends BaseResourceFormGroupComponent<string, CronHpaTriggerRuleModel>
  implements OnInit, OnDestroy {
  CronHpaTriggerRuleEnum = CronHpaTriggerRuleEnum;
  WEEK_DAY_OPTIONS = WEEK_DAY_OPTIONS;
  destroy$ = new Subject<void>();

  @Output()
  errors = new EventEmitter();

  @Input()
  ruleType: CronHpaTriggerRuleEnum;

  @ObservableInput(true)
  ruleType$: Observable<CronHpaTriggerRuleEnum>;

  constructor(public injector: Injector) {
    super(injector);
  }

  setDisabledState(isDisabled: boolean): void {
    this.form[isDisabled ? 'disable' : 'enable']();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.initForm();
  }

  private initForm() {
    this.form
      .get('daysChosen')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (
          this.ruleType === CronHpaTriggerRuleEnum.CRON &&
          value.length > 1 &&
          value.includes('*')
        ) {
          this.form.patchValue({ daysChosen: ['*'] }, { emitEvent: false });
        }
      });

    this.ruleType$.pipe(takeUntil(this.destroy$)).subscribe(ruleType => {
      if (ruleType === CronHpaTriggerRuleEnum.CRON) {
        this.form.get('daysChosen').enable({ emitEvent: false });
        this.form.get('timeChosen').enable({ emitEvent: false });
        this.form.get('customRule').disable({ emitEvent: false });
      } else {
        this.form.get('daysChosen').disable({ emitEvent: false });
        this.form.get('timeChosen').disable({ emitEvent: false });
        this.form.get('customRule').enable({ emitEvent: false });
      }
    });

    // expose error to parent form
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      const errorObj = Object.create(null);
      if (this.form.get('daysChosen').errors) {
        errorObj.daysChosen = this.form.get('daysChosen').errors;
      }
      if (this.form.get('customRule').errors) {
        errorObj.customRule = this.form.get('customRule').errors;
      }
      this.errors.emit(Object.keys(errorObj).length ? errorObj : null);
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.destroy$.next();
  }

  createForm() {
    return this.fb.group({
      daysChosen: this.fb.control(['*'], Validators.required),
      timeChosen: this.fb.control({ hour: 0, minute: 0 }),
      customRule: this.fb.control('', cronValidator()),
    });
  }

  adaptResourceModel(resource: string): CronHpaTriggerRuleModel {
    if (HPA_CRON_CONDITION_REGEXP.test(resource)) {
      const cronDataArr = resource.split(' ');
      const minute = +cronDataArr[0];
      const hour = +cronDataArr[1];
      const daysInWeek = cronDataArr[4];
      return {
        daysChosen: daysInWeek.split(','),
        timeChosen: { minute, hour },
      };
    }
    return {
      customRule: resource,
    };
  }

  adaptFormModel(formModel: CronHpaTriggerRuleModel) {
    if (this.ruleType === CronHpaTriggerRuleEnum.CUSTOM) {
      return formModel.customRule;
    }
    const { daysChosen = [], timeChosen } = formModel;
    return `${timeChosen.minute} ${timeChosen.hour} * * ${daysChosen.join(
      ',',
    )}`;
  }

  getDefaultFormModel() {
    return {
      daysChosen: ['*'],
      timeChosen: { hour: 0, minute: 0 },
      customRule: '',
    };
  }
}
