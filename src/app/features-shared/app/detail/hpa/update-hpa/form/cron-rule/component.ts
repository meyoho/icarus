import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Optional,
} from '@angular/core';
import { ControlContainer, ValidationErrors, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { CronHorizontalPodAutoscalerRule, Workload } from 'app/typings';
import { NATURAL_NUMBER_PATTERN, rowBackgroundColorFn } from 'app/utils';

import { CronHpaModel, CronHpaTriggerRuleEnum } from '../../../type';
import { HPA_CRON_CONDITION_REGEXP } from '../../../util';
@Component({
  selector: 'rc-hpa-cron-rules-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      .timing-form__action-col {
        align-self: flex-start;
        margin-top: 4px;
      }
    `,
  ],
})
export class HpaCronRulesFormComponent extends BaseResourceFormArrayComponent<
  CronHorizontalPodAutoscalerRule
> {
  rowBackgroundColorFn = rowBackgroundColorFn;
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.form[isDisabled ? 'disable' : 'enable']();
  }

  @Input()
  podController: Workload;

  constructor(
    public injector: Injector,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  setSchecduleErrors(errors: ValidationErrors, index: number) {
    this.form.get(`${index}.schedule`).setErrors(errors);
  }

  CronHpaTriggerRuleEnum = CronHpaTriggerRuleEnum;
  NATURAL_NUMBER_PATTERN = NATURAL_NUMBER_PATTERN;

  adaptFormModel(formModel: CronHpaModel[]): CronHorizontalPodAutoscalerRule[] {
    return formModel.map(({ schedule = '', targetReplicas }) => ({
      schedule: schedule,
      targetReplicas,
    }));
  }

  adaptResourceModel(
    resource: CronHorizontalPodAutoscalerRule[],
  ): CronHpaModel[] {
    return (resource || []).map(({ targetReplicas, schedule }) => {
      return {
        targetReplicas,
        schedule,
        type: HPA_CRON_CONDITION_REGEXP.test(schedule)
          ? CronHpaTriggerRuleEnum.CRON
          : CronHpaTriggerRuleEnum.CUSTOM,
      };
    });
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group({
        type: CronHpaTriggerRuleEnum.CRON,
        schedule: ['0 0 * * *'],
        targetReplicas: [
          '',
          [Validators.pattern(NATURAL_NUMBER_PATTERN.pattern)],
        ],
      });
  }
}
