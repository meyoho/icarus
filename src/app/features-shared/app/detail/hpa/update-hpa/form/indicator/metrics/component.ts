import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { get, isEqual } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { distinctUntilChanged, filter } from 'rxjs/operators';

import { MetricSpec } from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

import { HpaV2Config } from '../../../../type';
import {
  METRIC_TARGET_TYPE_KEY_MAPPER,
  getMetricDescription,
  getMetricDisplayUnit,
  getMetricName,
  getMetricPlaceholder,
  getMetricValue,
  getUnitFromConfig,
} from '../../../../util';

interface MetricModel {
  name: string;
  value: string | number;
  unit: string;
  targetType: string;
  type: string;
}
@Component({
  selector: 'rc-indicator-metrics-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndicatorMetricsFormComponent extends BaseResourceFormArrayComponent<
  MetricSpec,
  MetricModel
> {
  getMetricDescription = getMetricDescription;
  getMetricPlaceholder = getMetricPlaceholder;
  getMetricDisplayUnit = getMetricDisplayUnit;
  rowBackgroundColorFn = rowBackgroundColorFn;

  @Input()
  metricConfig: HpaV2Config[] = [];

  constructor(injector: Injector, public readonly translate: TranslateService) {
    super(injector);
    this.getAvailableOption = this.getAvailableOption.bind(this);
  }

  adaptResourceModel(resource: MetricSpec[]) {
    // 根据 pattern，将数据提取出来，分割为value和单位
    return (resource || []).map(metric => {
      const name = getMetricName(metric);
      const resourceValue = getMetricValue(metric);
      const pattern = (this.metricConfig || []).find(
        config => config.name === name,
      )?.target.pattern;
      // 无 pattern，采用原值，无单位
      let value = resourceValue;
      let unit = '';
      if (pattern) {
        // 去除末尾 $ 进行贪婪匹配
        value = new RegExp(pattern.slice(0, -1)).exec(resourceValue)[0];
        unit = resourceValue.slice(value.length);
      }
      return {
        name,
        value,
        unit,
        type: metric.type,
        targetType: get(metric, [metric.type.toLowerCase(), 'target', 'type']),
      };
    });
  }

  adaptFormModel(formModel: MetricModel[]): MetricSpec[] {
    return formModel.map(model => {
      const { name, value, unit, type, targetType } = model;
      return {
        [type.toLowerCase()]: {
          ...(type === 'Resource'
            ? { name }
            : {
                metric: {
                  name,
                },
              }),
          target: {
            type: targetType,
            // 仅为百分比时，无需向后端提供单位
            [METRIC_TARGET_TYPE_KEY_MAPPER[targetType]]:
              targetType === 'Utilization' ? +value : value + unit,
          },
        },
        type,
      };
    });
  }

  existMetricOption(name: string, formModel: MetricModel[]) {
    return formModel.some(metric => metric.name === name);
  }

  getAvailableOption(
    formModel: MetricModel[],
    metricConfig: HpaV2Config[],
  ): FormGroup {
    const config = this.getUnusedMetricConfig(formModel, metricConfig);
    return config
      ? this.fb.group({
          name: config.name,
          value: null,
          unit: getUnitFromConfig(config),
          targetType: config.target.type,
          type: config.type,
        })
      : null;
  }

  getUnusedMetricConfig(formModel: MetricModel[], metricConfig: HpaV2Config[]) {
    return (metricConfig || []).find(config => {
      return !formModel.some(metric => metric.name === config.name);
    });
  }

  getConfigPattern(name: string, metricConfig: HpaV2Config[]) {
    const pattern = (metricConfig || []).find(config => config.name === name)
      ?.target.pattern;
    return pattern ? new RegExp(pattern) : null;
  }

  getOnFormArrayResizeFn() {
    return () => {
      const fg =
        this.getAvailableOption(this.formModel, this.metricConfig) ||
        this.fb.group({
          name: '',
          value: null,
          unit: null,
          targetType: '',
          type: '',
        });
      fg.get('name')
        .valueChanges.pipe(
          distinctUntilChanged(isEqual),
          filter(_ => !fg.disabled && fg.dirty),
        )
        .subscribe(name => {
          const config = this.metricConfig.find(metric => metric.name === name);
          fg.patchValue(
            {
              value: null,
              unit: getUnitFromConfig(config),
              type: config.type,
              targetType: config.target.type,
            },
            { emitEvent: false },
          );
        });

      return fg;
    };
  }

  setDisabledState(disabled: boolean) {
    this.form[disabled ? 'disable' : 'enable']();
    this.cdr.markForCheck();
  }
}
