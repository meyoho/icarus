import { ObjectMeta, ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';

import {
  HorizontalPodAutoscaler,
  HorizontalPodAutoscalerMeta,
  HorizontalPodAutoscalerSpec,
  Workload,
  WorkloadType,
} from 'app/typings';
import { POSITIVE_INT_PATTERN } from 'app/utils';

import { HpaV2Config } from '../../../type';
import { adaptHpaResourceName } from '../../../util';

const HPA_V2BETA2_API_VERSION = 'autoscaling/v2beta2';

@Component({
  selector: 'rc-hpa-indicator-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HpaIndicatorFormComponent
  extends BaseResourceFormGroupComponent<
    HorizontalPodAutoscaler,
    HorizontalPodAutoscalerSpec
  >
  implements OnInit {
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;

  @Input()
  isHpaV2: boolean;

  @ObservableInput(true)
  isHpaV2$: Observable<boolean>;

  @Input() podController: Workload;
  @Input() workloadType: WorkloadType;
  @Input() cluster: string;
  @Input() hpaV2Config: HpaV2Config[];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.isHpaV2$.subscribe(isHpaV2 => {
      this.form
        .get('targetCPUUtilizationPercentage')
        [isHpaV2 ? 'disable' : 'enable']({ emitEvent: false });
    });
  }

  getDefaultFormModel() {
    return {
      minReplicas: 1,
      maxReplicas: 1,
    };
  }

  adaptFormModel(formModel: HorizontalPodAutoscalerSpec) {
    const { name, namespace } = this.podController.metadata;
    const { kind, apiVersion } = this.podController;
    const metadata: ObjectMeta = {
      name: adaptHpaResourceName(name, this.workloadType),
      namespace,
    };
    const scaleTargetRef = {
      apiVersion,
      kind,
      name,
    };
    const spec = {
      ...formModel,
      scaleTargetRef,
    };
    return {
      ...HorizontalPodAutoscalerMeta,
      ...(this.isHpaV2 ? { apiVersion: HPA_V2BETA2_API_VERSION } : {}),
      metadata,
      spec,
    };
  }

  adaptResourceModel(resource: HorizontalPodAutoscaler) {
    return resource?.spec;
  }

  createForm() {
    return this.fb.group({
      maxReplicas: ['', [Validators.pattern(POSITIVE_INT_PATTERN.pattern)]],
      minReplicas: ['', [Validators.pattern(POSITIVE_INT_PATTERN.pattern)]],
      targetCPUUtilizationPercentage: [
        '',
        [Validators.pattern(POSITIVE_INT_PATTERN.pattern)],
      ],
      metrics: [],
    });
  }

  setDisabledState(disabled: boolean) {
    this.form[disabled ? 'disable' : 'enable']();
  }
}
