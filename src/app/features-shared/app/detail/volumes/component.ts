import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Volume } from 'app/typings';

import {
  AVAILABLE_VOLUME_TYPES,
  getVolumeType,
  getVolumeTypeTranslateKey,
} from '../../utils';

@Component({
  selector: 'rc-volumes-preview',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumesPreviewComponent {
  @Input() volumes: Volume[] = [];

  getVolumeType = getVolumeType;
  getVolumeTypeTranslateKey = getVolumeTypeTranslateKey;

  isBuiltIn(volume: Volume) {
    return AVAILABLE_VOLUME_TYPES.includes(getVolumeType(volume));
  }
}
