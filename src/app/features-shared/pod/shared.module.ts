import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { AppSharedModule } from '../app/shared.module';
import { ExecSharedModule } from '../exec/shared.module';

import { PodImagesComponent } from './images/component';
import { PodListComponent } from './list/component';
import { PodStatusComponent } from './status/component';

@NgModule({
  imports: [SharedModule, ExecSharedModule, AppSharedModule],
  declarations: [PodListComponent, PodImagesComponent, PodStatusComponent],
  exports: [PodListComponent, PodStatusComponent],
})
export class PodSharedModule {}
