/**
 * Deployment 和 CronJob 对应的Pod 的全名 podFullName 规则如下：
 * Deployment : {Deployment名字}-{随机 ReplicaSet名字}-{随机串}
 * CronJob: {CronJob名字}-{执行后的Job的名字}-{随机串}
 * 只需要取最后一部分展示
 * @param podFullName
 */
export function getPodShortName(name: string) {
  const names = name.split('-');
  return names[names.length - 1];
}
