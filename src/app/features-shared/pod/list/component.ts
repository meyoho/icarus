import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  KubernetesResourceList,
  NAME,
  NAMESPACE,
  ObservableInput,
  StringMap,
  TranslateService,
  isAllowed,
  matchLabelsToString,
  noop,
  normalizeParams,
  publishRef,
  viewActions,
  yamlReadOptions,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { get, isEqual } from 'lodash-es';
import { EMPTY, Observable, combineLatest, of } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';

import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import { TerminalService } from 'app/services/api/terminal.service';
import { CodeDisplayDialogComponent } from 'app/shared/components/code-display-dialog/component';
import {
  Container,
  ContainerStatus,
  Pod,
  PodStatus,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
  WorkspaceListParams,
} from 'app/typings';
import { ACTION, STATUS, getPodStatus, getResourceYaml } from 'app/utils';
import { getPodResourceLimit } from 'app_user/features/pod/util';
import { PodUtilService } from 'app_user/features/pod/util.service';

const EMPTY_POD_LIST: KubernetesResourceList<Pod> = {
  apiVersion: 'v1',
  kind: 'PodList',
  metadata: {},
  items: [],
};

@Component({
  selector: 'rc-pod-list',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodListComponent implements OnInit, OnDestroy {
  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true)
  private readonly baseParams$: Observable<WorkspaceBaseParams>;

  @Input() matchLabels: StringMap;
  @ObservableInput(true)
  private readonly matchLabels$: Observable<StringMap>;

  @Input() matchFields: StringMap;
  @ObservableInput(true)
  private readonly matchFields$: Observable<StringMap>;

  @Input() hideLog = false;
  @Input() isAdmin = false;
  @Input() showTitle = false;
  @Input() tappFeatures = false;
  @Input() allowUpdateTapp = false;

  // embedded in workload or tapp..., false is given when pod list page
  @Input() embedded = true;
  @ObservableInput()
  embedded$: Observable<true>;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onViewLog = new EventEmitter<{
    pod: string;
    container: string;
  }>();

  @Output()
  deleted = new EventEmitter<void>();

  @Output()
  killPod = new EventEmitter<{ pod: Pod; callback: () => void }>();

  @Output()
  updateBlueGreen = new EventEmitter<{ pod: Pod; callback: () => void }>();

  private readonly labelSelector$ = this.matchLabels$.pipe(
    filter(o => !!o),
    distinctUntilChanged(isEqual),
    map(matchLabels => matchLabelsToString(matchLabels)),
  );

  private readonly fieldSelector$ = this.matchFields$.pipe(
    filter(o => !!o),
    distinctUntilChanged(isEqual),
    map(matchFields => matchLabelsToString(matchFields)),
  );

  private readonly fetchParams$ = combineLatest([
    this.baseParams$,
    this.labelSelector$.pipe(startWith('')),
    this.fieldSelector$.pipe(startWith('')),
    this.activatedRoute.queryParams,
  ]).pipe(
    map(([baseParams, labelSelector, fieldSelector, queryParams]) => ({
      ...baseParams,
      labelSelector,
      fieldSelector,
      ...queryParams,
    })),
  );

  columns: string[] = [];

  list: K8SResourceList<Pod>;

  getPodStatus = getPodStatus;
  editorActions = viewActions;
  editorOptions = yamlReadOptions;
  yamlInputValue = '';

  permissions$ = this.baseParams$.pipe(
    switchMap(baseParams => {
      return this.k8sPermission
        .getAccess({
          type: RESOURCE_TYPES.POD,
          action: K8sResourceAction.DELETE,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
        })
        .pipe(isAllowed());
    }),
    publishRef(),
  );

  execPermissions$: Observable<Record<string, boolean>>;

  constructor(
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly terminal: TerminalService,
    public readonly translate: TranslateService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly podService: PodUtilService,
  ) {}

  ngOnDestroy(): void {
    this.list.destroy();
  }

  ngOnInit() {
    const allColumns = [
      {
        name: NAME,
      },
      {
        name: 'image',
        hide: !this.tappFeatures,
      },
      {
        name: STATUS,
      },
      {
        name: 'resource_limit',
      },
      {
        name: 'restart_count',
      },
      {
        name: NAMESPACE,
        hide: !this.isAdmin,
      },
      {
        name: 'pod_ip',
      },
      {
        name: 'node_name',
        hide: this.isAdmin,
      },
      {
        name: 'created_time',
      },
      {
        name: ACTION,
      },
    ];

    this.columns = allColumns.filter(item => !item.hide).map(item => item.name);

    this.execPermissions$ = this.terminal.getPermissions({
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
    });

    this.list = new K8SResourceList<Pod>({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetchPodResourceList.bind(this),
      watcher: seed =>
        this.fetchParams$.pipe(
          switchMap(
            ({
              cluster,
              namespace,
              name: _name,
              project: _project,
              ...params
            }: WorkspaceListParams) => {
              if (!seed) {
                return EMPTY;
              }
              const queryParams = normalizeParams(params);
              return this.k8sApi.watchResourceChange(seed, {
                type: RESOURCE_TYPES.POD,
                cluster,
                namespace,
                queryParams,
              });
            },
          ),
        ),
    });
  }

  delete(pod: Pod) {
    this.podService.deletePod(pod, this.baseParams.cluster).subscribe(_ => {
      this.deleted.emit();
      this.message.success(this.translate.get('delete_success'));
    });
  }

  killTappPod(pod: Pod) {
    this.killPod.emit({ pod, callback: noop });
  }

  tappBlueGreen(pod: Pod) {
    this.updateBlueGreen.emit({
      pod,
      callback: noop,
    });
  }

  canExec(container: Container, pod: Pod) {
    const containerStatuses = get(
      pod,
      [STATUS, 'containerStatuses'],
      [] as ContainerStatus[],
    );
    return !!get(
      containerStatuses.find(status => status.name === container.name),
      ['state', 'running'],
    );
  }

  onExec(pod: Pod, container: Container) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      this.podService.handleExecInfo(pod, container, this.baseParams, data);
    });
  }

  viewLog(pod: Pod, container: Container) {
    this.onViewLog.emit({
      pod: pod.metadata.name,
      container: container.name,
    });
  }

  // reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=61913591
  getContainerRestartCountAcc(status: PodStatus) {
    const { containerStatuses = [], initContainerStatuses = [] } = status;
    const statuses = containerStatuses.concat(...initContainerStatuses);

    return statuses.length > 0
      ? statuses
          .map(i => i.restartCount)
          .reduce((a, b) => {
            return a + b;
          })
      : '';
  }

  openPodYaml(pod: Pod) {
    this.dialog.open(CodeDisplayDialogComponent, {
      size: DialogSize.Big,
      data: {
        code: getResourceYaml(pod),
        title: pod.metadata.name,
      },
    });
  }

  private fetchPodResourceList({
    cluster,
    namespace,
    name: _name,
    project: _project,
    ...queryParams
  }: WorkspaceListParams) {
    if (
      this.embedded &&
      (!queryParams ||
        (!queryParams.labelSelector && !queryParams.fieldSelector))
    ) {
      return of(EMPTY_POD_LIST);
    } else {
      return this.k8sApi.getResourceList<Pod>({
        type: RESOURCE_TYPES.POD,
        cluster,
        namespace,
        queryParams,
      });
    }
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  viewResource(pod: KubernetesResource, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(pod, {
      lineWidth: 9999,
      sortKeys: true,
    });
    this.dialog.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: pod.metadata.name,
      },
    });
  }

  getPodResourceLimit = getPodResourceLimit;
}
