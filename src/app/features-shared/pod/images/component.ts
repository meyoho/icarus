import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Pod } from 'app/typings';

@Component({
  selector: 'rc-pod-images',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodImagesComponent {
  @Input()
  pod: Pod;

  get images() {
    return this.pod.spec.containers.map(item => `${item.name}: ${item.image}`);
  }
}
