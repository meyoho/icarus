import { ImagePullDialogData } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { RcImageSelection } from 'app/features-shared/image/image.type';
import { formatValue } from 'app/features-shared/image/utils';

@Component({
  templateUrl: './component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageDialogComponent {
  @ViewChild('ngform', { static: false })
  form: NgForm;

  formModel = this.buildForm();

  @Output()
  close = new EventEmitter<RcImageSelection>();

  constructor(@Inject(DIALOG_DATA) public data: ImagePullDialogData) {}

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.close.next(formatValue(this.formModel));
  }

  buildForm() {
    return {
      type: 'select' as const,
    };
  }
}
