import {
  ImageRepositoryOption,
  ImageSelectorDataContext,
  ObservableInput,
  SelectedImageRepository,
  withLoadState,
} from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { EMPTY, Observable, Subject, combineLatest, merge, of } from 'rxjs';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';

import { ImageSelectFormModel } from '../image.type';

@Component({
  templateUrl: './template.html',
  selector: 'rc-image-select',
  styleUrls: ['./component.scss'],
})
export class ImageSelectComponent extends BaseResourceFormGroupComponent<
  ImageSelectFormModel,
  ImageSelectFormModel
> {
  @ObservableInput(true) form$: Observable<FormGroup>;

  @Output()
  error = new EventEmitter<void>();

  @ObservableInput(true)
  context$$: Observable<ImageSelectorDataContext>;

  @Input()
  context: ImageSelectorDataContext;

  repositoryNullError$$ = new Subject<boolean>();

  imageRepositories$ = this.context$$.pipe(
    filter(context => !!context),
    switchMap(() =>
      withLoadState(
        this.context.getImageRepositories().pipe(
          catchError(_ => {
            this.error.next();
            return EMPTY;
          }),
        ),
        [],
      ),
    ),
  );

  createForm(): FormGroup {
    return this.fb.group(
      {
        repository: null,
        tag: null,
      },
      { validators: [this.formValidator] },
    );
  }

  formValidator: ValidatorFn = (group: FormGroup) => {
    const errors: { repoRequired?: boolean } = { repoRequired: false };
    // FIXME: aui-select暂不支持原生required校验
    if (!group.get('repository').value) {
      group
        .get('repository')
        .setErrors(Validators.required(group.get('repository')));
      errors.repoRequired = true;
    }
    this.repositoryNullError$$.next(errors.repoRequired);
    return errors.repoRequired ? errors : null;
  };

  getDefaultFormModel(): ImageSelectFormModel {
    return {
      repository: null,
      tag: '',
    };
  }

  constructor(public injector: Injector) {
    super(injector);
  }

  private readonly selectedImageRepository$: Observable<
    SelectedImageRepository
  > = merge(
    this.form$.pipe(
      filter(form => !!form),
      concatMap(form => form.valueChanges),
      map(value => {
        return (value.repository && value.repository.repositoryPath) || '';
      }),
      map((value: string) => ({ value, isChange: true })),
    ),
    this.form$.pipe(
      filter(form => !!form),
      take(1),
      map(
        form =>
          (form.value.repository && form.value.repository.repositoryPath) || '',
      ),
      map((value: string) => ({ value, isChange: false })),
    ),
  ).pipe(
    distinctUntilChanged((a, b) => a.value === b.value),
    // clean selected tag when repository change
    tap(
      ({ value, isChange }) =>
        (isChange || !value) && this.form.patchValue({ tag: '' }),
    ),
  );

  tags$ = combineLatest([
    this.selectedImageRepository$,
    this.imageRepositories$,
  ]).pipe(
    switchMap(([{ value }, { loading, data }]) => {
      if (!data || data.length === 0) {
        return of({
          loading,
          data: [],
        });
      }

      const selected = data.find(item => item.repositoryPath === value);

      if (!selected) {
        return of({
          loading,
          data: [],
        });
      }

      return withLoadState(
        this.context.getTags(selected).pipe(catchError(_ => of([]))),
        [],
      );
    }),
  );

  getImage(name: string) {
    return `icons/tool-chain/${(name || 'docker').toLowerCase()}.svg`;
  }

  repositoryTracker(repository: Partial<ImageRepositoryOption>) {
    return (repository && repository.repositoryPath) || '';
  }
}
