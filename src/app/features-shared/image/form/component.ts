import { ImageSelectorDataContext } from '@alauda/common-snippet';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map, shareReplay, startWith, tap } from 'rxjs/operators';

import { TYPE } from 'app/utils';

import { ImageDialogFormModel } from '../image.type';

@Component({
  selector: 'rc-image-form',
  templateUrl: './template.html',
})
export class ImageDialogFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  context: ImageSelectorDataContext;

  isSelect$: Observable<boolean>;
  canSelect = true;

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.isSelect$ = this.form.get(TYPE).valueChanges.pipe(
      map(value => value === 'select'),
      startWith(this.formModel.type === 'select'),
      tap(isSelect => {
        this._refreshValidControl(isSelect);
      }),
      shareReplay(1),
    );
  }

  createForm(): FormGroup {
    return this.fb.group({
      type: 'select',
      repoSelect: {
        tag: this.fb.control(''),
        repository: this.fb.control({}),
      },
      fullPath: '',
    });
  }

  getDefaultFormModel(): ImageDialogFormModel {
    return {
      type: 'select',
      repoSelect: { tag: '', repository: null },
      fullPath: '',
    };
  }

  private _refreshValidControl(isSelect: boolean) {
    if (isSelect) {
      this.form.get('fullPath').disable();
      this.form.get('repoSelect').enable();
    } else {
      this.form.get('fullPath').enable();
      this.form.get('repoSelect').disable();
    }
  }

  errorHandler() {
    if (this.canSelect) {
      this.canSelect = false;
      this.form.patchValue({ type: 'input' });
      this.cdr.markForCheck();
    }
  }
}
