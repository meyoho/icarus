import { ImageRepositoryOption, ObservableInput } from '@alauda/common-snippet';
import { Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { ImageService } from 'app/features-shared/image/image.service';
import {
  ImageParams,
  ImageRepository,
  ImageSelectorContext,
  ListResult,
} from 'app/features-shared/image/image.type';

export class LocalImageSelectorDataContext implements ImageSelectorContext {
  @ObservableInput(true)
  params$: Observable<ImageParams>;

  @Input()
  params: ImageParams;

  constructor(private readonly imageService: ImageService) {}

  getImageRepositories(mode?: string): Observable<ImageRepositoryOption[]> {
    return this.params$.pipe(
      switchMap(({ project }) => {
        let registryApiResult$: Observable<ListResult<ImageRepository>>;
        if (mode && mode === 'create') {
          registryApiResult$ = this.imageService.findRepositoryProjects(
            project,
            null,
          );
        } else {
          registryApiResult$ = this.imageService.findRepositoriesByProject(
            project,
            null,
          );
        }
        return registryApiResult$.pipe(
          map(data => data.items),
          map(items =>
            items.map(
              ({
                endpoint,
                type,
                secretName,
                secretNamespace,
                tags,
                image,
                name,
              }) => {
                const imageSegments = image.split('/');
                const [imageProject, imageName] =
                  imageSegments.length > 1 ? imageSegments : [image, ''];
                return {
                  name,
                  type,
                  endpoint,
                  image,
                  repositoryPath: `${endpoint}/${image}`,
                  projectPath: `${endpoint}/${imageProject}`,
                  imageName,
                  tags,
                  secret: {
                    name: secretName,
                    namespace: secretNamespace,
                  },
                };
              },
            ),
          ),
        );
      }),
    );
  }

  getTags(repository: ImageRepositoryOption) {
    return this.params$.pipe(
      switchMap(({ project }) =>
        this.imageService
          .findRepositoryTags(project, repository.name, null)
          .pipe(map(list => list.items || [])),
      ),
    );
  }
}
