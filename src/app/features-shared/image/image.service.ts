import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ImageRepository,
  ImageTag,
  ListResult,
} from 'app/features-shared/image/image.type';
import {
  mapDataToRepoTag,
  mapFindRepositoriesResponseToList,
} from 'app/features-shared/image/utils';

const REPOSITORIES_URL = '{{API_GATEWAY}}/devops/api/v1/imagerepository';
const REPOSITORIES_PROJECT_URL =
  '{{API_GATEWAY}}/devops/api/v1/imagerepositoryproject';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  constructor(private readonly http: HttpClient) {}

  findRepositoriesByProject(
    project: string,
    query: { [key: string]: string },
  ): Observable<ListResult<ImageRepository>> {
    return this.http
      .get(`${REPOSITORIES_URL}/${project}`, { params: query })
      .pipe(map(mapFindRepositoriesResponseToList));
  }

  findRepositoryProjects(
    project: string,
    query: { [key: string]: string },
  ): Observable<ListResult<ImageRepository>> {
    return this.http
      .get(`${REPOSITORIES_PROJECT_URL}/${project}`, { params: query })
      .pipe(map(mapFindRepositoriesResponseToList));
  }

  findRepositoryTags(
    project: string,
    repo: string,
    query: { [key: string]: string },
  ): Observable<ListResult<ImageTag>> {
    return this.http
      .get(`${REPOSITORIES_URL}/${project}/${repo}/tags`, {
        params: query,
      })
      .pipe(
        map((res: any) => ({
          total: (res.tags && res.tags.length) || 0,
          items: (res.tags || []).map(mapDataToRepoTag),
          errors: res.errors,
        })),
      );
  }
}
