import {
  ImageRepositoryOption,
  ImageRepositoryValue,
} from '@alauda/common-snippet';
import { get } from 'lodash-es';

import {
  DevOpsK8sResource,
  ImageDialogFormModel,
  ImageRepository,
  ImageTag,
  ListResult,
  RcImageSelection,
} from 'app/features-shared/image/image.type';

export function mapFindRepositoriesResponseToList(
  res: any,
): ListResult<ImageRepository> {
  return {
    total: get(res, 'listMeta.totalItems', 0),
    items: res.imagerepositories.map(mapResourceToRepository),
    errors: res.errors,
  };
}
export function mapResourceToRepository(
  res: DevOpsK8sResource,
): ImageRepository {
  const meta = res.objectMeta || res.metadata;

  return {
    name: meta.name,
    namespace: meta.namespace,
    creationTimestamp: meta.creationTimestamp,
    image: get(res, 'spec.image', ''),
    endpoint: get(meta, 'annotations.imageRegistryEndpoint', ''),
    type: get(meta, 'annotations.imageRegistryType', ''),
    link: get(meta, 'annotations.imageRepositoryLink', ''),
    host: get(res, 'objectMeta.annotations.imageRegistryEndpoint', ''),
    status: {
      phase: get(res, 'status.phase', ''),
      message: get(res, 'status.message', ''),
    },
    tags: (get(res, 'status.tags') || []).map(mapDataToRepoTag),
    secretName: get(meta, 'annotations.secretName', ''),
    secretNamespace: get(meta, 'annotations.secretNamespace', ''),
  };
}

export function mapDataToRepoTag(data: any): ImageTag {
  const { created_at, ...rest } = data;
  return {
    ...rest,
    createdAt: created_at,
  };
}

export function formatValue({
  type,
  fullPath,
  repoSelect,
}: ImageDialogFormModel): ImageRepositoryValue | RcImageSelection {
  if (type === 'input') {
    return getImageSelectionParamsFromInput(fullPath);
  }
  return getImageSelectionParamsFromSelect(
    repoSelect.repository,
    repoSelect.tag,
  );
}

function getImageSelectionParamsFromInput(
  imageAddressInput: string,
): RcImageSelection {
  const parts = imageAddressInput.split('/');
  let fullImageName = '';
  const length = parts.length;
  const image = parts[length - 1];
  const imageParts = image.split(':');
  // repository may contain :, last : refer to split repository and tag
  let name, tag;
  if (imageParts.length > 1) {
    // exist tag
    tag = imageParts[imageParts.length - 1];
    name = imageParts.slice(0, -1).join(':');
  } else {
    name = imageParts[0];
  }
  if (tag) {
    parts[length - 1] = name;
    fullImageName = parts.join('/');
  } else {
    fullImageName = imageAddressInput;
  }
  return {
    registry_endpoint: parts[0],
    is_public_registry: true,
    registry_name: '',
    project_name: '',
    repository_name: name,
    tag: tag || 'latest',
    full_image_name: fullImageName,
    is_third: false,
  };
}

function getImageSelectionParamsFromSelect(
  repository: Partial<ImageRepositoryOption>,
  tag: string,
): RcImageSelection {
  return {
    registry_endpoint: repository.endpoint,
    is_public_registry: true,
    registry_name: '',
    project_name: '',
    repository_name: repository.imageName,
    tag: tag || 'latest',
    full_image_name: repository.repositoryPath,
    is_third: false,
    secret: repository.secret.name,
  };
}
