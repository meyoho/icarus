import { Component, Injector } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

import { IMAGE_REPO_PATTERN } from 'app/utils';

@Component({
  selector: 'rc-image-input',
  templateUrl: './template.html',
  preserveWhitespaces: false,
})
export class ImageInputComponent extends BaseResourceFormComponent<string> {
  demoRepositoryAddress = 'index.docker.io/library/ubuntu:latest';
  IMAGE_REPO_PATTERN = IMAGE_REPO_PATTERN;

  constructor(public injector: Injector) {
    super(injector);
  }

  getResourceMergeStrategy(): boolean {
    return false;
  }

  createForm(): FormControl {
    return this.fb.control('');
  }

  getDefaultFormModel() {
    return '';
  }

  setDemoAddress(ev: Event) {
    ev.preventDefault();
    this.form.patchValue(this.demoRepositoryAddress);
  }
}
