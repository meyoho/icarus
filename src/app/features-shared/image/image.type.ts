import {
  ImageRepositoryOption,
  ImageRepositoryValue,
  KubernetesResource,
  ScanStatus,
  Severity,
  TagOption,
} from '@alauda/common-snippet';
import { Observable } from 'rxjs';

export interface DevOpsK8sResource extends KubernetesResource {
  objectMeta?: K8SMetaData;
  spec: {
    [key: string]: any;
  };
  status?: {
    phase: string;
    message: string;
  };
}

interface K8SMetaData {
  name: string;
  namespace?: string;
  description?: string;
  creationTimestamp?: string;
  labels?: { [key: string]: string };
  annotations?: { [key: string]: string };
}

export interface ListResult<T> {
  total: number;
  items: T[];
  errors: any[];
}

export interface ImageRepository {
  name: string;
  namespace: string;
  host: string;
  creationTimestamp: string;
  image: string;
  endpoint: string;
  type: string;
  link: string;
  tags: ImageTag[];
  status: ImageStatus;
  secretName?: string;
  secretNamespace?: string;
}

export interface ImageTag {
  name: string;
  digest: string;
  createdAt: string;
  size?: string;
  level?: number;
  scanStatus: ScanStatus;
  severity: string;
  message?: string;
  summary?: ScanSummary[];
  author: string;
}

export interface ImageSelectorContext {
  params: ImageParams;
  getImageRepositories: (mode?: string) => Observable<ImageRepositoryOption[]>;
  getTags: (repository: ImageRepositoryOption) => Observable<TagOption[]>;
}

export interface ImageParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}

interface ScanSummary {
  severity: Severity;
  count: number;
}

interface ImageStatus {
  phase: string;
  message: string;
}

export declare interface RcImageSelection {
  is_public_registry?: boolean;
  registry_name?: string;
  registry_endpoint?: string;
  project_name?: string;
  repository_name?: string;
  full_image_name?: string;
  tag?: string;
  is_third?: boolean;
  secret?: string;
}

export interface ImageSelectFormModel {
  repository: ImageRepositoryOption;
  tag: string;
}

export type ImageDialogFormModel = Pick<ImageRepositoryValue, 'type'> & {
  fullPath?: string;
  repoSelect?: ImageSelectFormModel;
};
