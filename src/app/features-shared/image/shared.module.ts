import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ImageDialogComponent } from 'app/features-shared/image/dialog/component';
import { ImageInputComponent } from 'app/features-shared/image/image-input/component';
import { ImageSelectComponent } from 'app/features-shared/image/image-select/component';
import { SharedModule } from 'app/shared/shared.module';

import { ImageDialogFormComponent } from './form/component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule],
  declarations: [
    ImageDialogComponent,
    ImageInputComponent,
    ImageSelectComponent,
    ImageDialogFormComponent,
  ],
  exports: [ImageDialogComponent, ImageInputComponent, ImageSelectComponent],
})
export class ImageSharedModule {}
