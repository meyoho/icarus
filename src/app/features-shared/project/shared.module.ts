import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { ProjectListDialogComponent } from './list-dialog/component';

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: [ProjectListDialogComponent],
  exports: [ProjectListDialogComponent],
})
export class ProjectSharedModule {}
