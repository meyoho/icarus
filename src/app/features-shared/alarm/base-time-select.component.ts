import { TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import dayjs from 'dayjs';

import { QUERY_TIME_STAMP_OPTIONS } from 'app/features-shared/app/utils';
import { dateNumToStr, dateStrToNum, getTimeRangeByRangeType } from 'app/utils';

/**
 * 日期选择器基础类，用于监控告警图表的日期选择
 */
export abstract class BaseTimeSelectComponent {
  chartLoading = true;
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });

  selectedTimeStampOption = this.timeStampOptions[0].type;
  currentTime = dayjs();
  startTime = this.currentTime.clone().startOf('day').subtract(6, 'day');

  step: number;
  endTime: number;
  queryDates = {
    start_time: this.startTime.valueOf(),
    end_time: this.currentTime.valueOf(),
  };

  queryDatesShown = {
    start_time: dateNumToStr(this.queryDates.start_time),
    end_time: dateNumToStr(this.queryDates.end_time),
  };

  dateTimeOptions = {
    maxDate: this.currentTime.clone().endOf('day').valueOf(),
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };

  constructor(
    private readonly auiMessageService: MessageService,
    protected translateService: TranslateService,
  ) {}

  abstract loadCharts(): void;

  protected resetTimeRange() {
    this.queryDates = getTimeRangeByRangeType(this.selectedTimeStampOption);
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(startTime: number, endTime: number) {
    this.queryDatesShown.start_time = dateNumToStr(startTime);
    this.queryDatesShown.end_time = dateNumToStr(endTime);
  }

  onStartTimeSelect(event: any) {
    this.queryDates.start_time = dateStrToNum(event);
  }

  onEndTimeSelect(event: any) {
    this.queryDates.end_time = dateStrToNum(event);
  }

  checkTimeRange() {
    if (
      this.floorTime(this.queryDates.end_time) <=
      this.floorTime(this.queryDates.start_time)
    ) {
      this.auiMessageService.warning({
        content: this.translateService.get('metric_timerange_invalid'),
      });
      return false;
    }
    return true;
  }

  floorTime(time: number) {
    return Math.floor(time / 1000);
  }

  onTimeStampOptionSelect() {
    if (this.selectedTimeStampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    this.onSearch();
  }

  onSearch() {
    if (this.checkTimeRange()) {
      this.chartLoading = true;
      this.loadCharts();
    }
  }
}
