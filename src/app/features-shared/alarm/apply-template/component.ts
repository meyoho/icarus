import {
  K8sApiService,
  NAME,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { upperFirst } from 'lodash-es';
import md5 from 'md5';
import { of } from 'rxjs';
import { catchError, pluck } from 'rxjs/operators';

import { IndicatorType } from 'app/services/api/metric.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AlertTemplate,
  AlertTemplateItem,
  ClusterFeature,
  DeploymentTypeMeta,
  PrometheusRule,
  PrometheusRuleItem,
  PrometheusRuleItemLabel,
  PrometheusRuleMeta,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/typings';
import { LOOSE_K8S_RESOURCE_NAME_BASE } from 'app/utils';

import {
  getAlarmTypeTitle,
  getRule,
  parseAlertList,
  parseGolangTemplate,
  parseNotifications,
  randomString,
} from '../alarm.util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkloadAlarmTemplateApplyComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  @Output()
  close: EventEmitter<PrometheusRule | boolean> = new EventEmitter();

  loading = true;
  initialized = false;
  submitting = false;

  resourceNameReg = LOOSE_K8S_RESOURCE_NAME_BASE;
  name: string;
  templates: AlertTemplate[];
  template: AlertTemplate;
  prometheusName: string;
  parseAlertList = parseAlertList;
  parseNotifications = parseNotifications;

  getRule = getRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      metricType: IndicatorType[];
      componentName: string;
      clusterName: string;
      namespaceName: string;
      projectName: string;
      appName: string;
      kind: string;
      uid: string;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<AlertTemplate>({
        type: RESOURCE_TYPES.ALERT_TEMPLATE,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      )
      .subscribe(alarmTemplates => {
        this.templates = alarmTemplates.filter(el => {
          return this.k8sUtil.getLabel(el, 'kind') === 'workload';
        });
        this.loading = false;
        this.initialized = true;
        this.cdr.markForCheck();
      });
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.modalData.clusterName,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .subscribe(features => {
        if (features.items.length > 0) {
          this.prometheusName = features.items[0].spec.accessInfo.name;
        }
      });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;

    const rules = this.template.spec.templates.map(template =>
      this.getPrometheusRule(template),
    );
    this.k8sApi
      .postResource<PrometheusRule>({
        type: RESOURCE_TYPES.PROMETHEUS_RULE,
        cluster: this.modalData.clusterName,
        resource: {
          apiVersion: PrometheusRuleMeta.apiVersion,
          kind: PrometheusRuleMeta.kind,
          metadata: {
            name: this.name,
            namespace: this.modalData.namespaceName,
            annotations: {
              [this.k8sUtil.normalizeType(
                'notifications',
                'alert',
              )]: this.k8sUtil.getAnnotation(
                this.template,
                'notifications',
                'alert',
              ),
            },
            labels: {
              [this.k8sUtil.normalizeType('application', 'alert')]: this
                .modalData.appName,
              [this.k8sUtil.normalizeType('cluster', 'alert')]: this.modalData
                .clusterName,
              [this.k8sUtil.normalizeType('kind', 'alert')]: upperFirst(
                this.modalData.kind,
              ),
              [this.k8sUtil.normalizeType(NAME, 'alert')]: this.modalData
                .componentName,
              [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this.modalData
                .namespaceName,
              [this.k8sUtil.normalizeType('project', 'alert')]: this.modalData
                .projectName,
              [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
              prometheus: this.prometheusName,
            },
            ownerReferences: [
              {
                apiVersion: DeploymentTypeMeta.apiVersion,
                controller: true,
                blockOwnerDeletion: true,
                kind: this.modalData.kind,
                name: this.modalData.componentName,
                uid: this.modalData.uid,
              },
            ],
          },
          spec: {
            groups: [
              {
                name: 'general',
                rules,
              },
            ],
          },
        },
      })
      .subscribe(
        rule => {
          this.messageService.success(
            this.translate.get('alarm_template_apply_success'),
          );
          this.close.next(rule);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  getPrometheusRule(template: AlertTemplateItem): PrometheusRuleItem {
    let alertIndicator = '';
    let alertIndicatorUnit: string;
    let customExpr: string;
    let expr: string;
    let alertIndicatorQuery: string;

    const resourceName = `ns-${this.modalData.namespaceName}.${this.modalData.componentName}.rules`;
    const groupName = `${this.modalData.kind}.${this.modalData.componentName}`;
    const firstQuery = template.metric.queries[0];
    firstQuery.labels.forEach((label: { name: string; value: string }) => {
      switch (label.name) {
        case '__name__': {
          alertIndicator = label.value;
          const found = this.modalData.metricType.find(el => {
            return el.name === label.value;
          });
          if (found) {
            alertIndicatorUnit = found.unit;
          }
          break;
        }
        case 'expr':
          customExpr = label.value;
          break;
        case 'query':
          alertIndicatorQuery = label.value;
          break;
      }
    });
    if (alertIndicator === 'custom') {
      alertIndicatorUnit = template.unit;
    }
    const labels = {
      ...template.labels,
      severity: template.labels.severity,
      application: this.modalData.appName,
      alert_name: '',
      alert_involved_object_kind: upperFirst(this.modalData.kind),
      alert_involved_object_name: this.modalData.componentName,
      alert_involved_object_namespace: this.modalData.namespaceName,
      alert_cluster: this.modalData.clusterName,
      alert_project: this.modalData.projectName,
      alert_indicator: alertIndicator,
      alert_indicator_aggregate_range: String(firstQuery.range),
      alert_indicator_aggregate_function: firstQuery.aggregator,
      alert_indicator_comparison: template.compare,
      alert_indicator_threshold: String(template.threshold),
      alert_indicator_query: alertIndicatorQuery,
      alert_indicator_unit: alertIndicatorUnit,
    } as PrometheusRuleItemLabel;
    const annotations = {
      ...template.annotations,
      alert_current_value: '{{ $value }}',
    };
    if (alertIndicator && alertIndicator !== 'custom') {
      const metric = this.modalData.metricType.find(
        ({ name }) => name === alertIndicator,
      );
      if (metric.query) {
        let query = metric.query;
        switch (upperFirst(this.modalData.kind)) {
          case 'Deployment':
            query = metric.query
              // eslint-disable-next-line sonarjs/no-duplicate-string
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[a-z0-9]{7,10}-[a-z0-9]{5}');
            break;
          case 'Daemonset':
            query = metric.query
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[a-z0-9]{5}');
            break;
          case 'Statefulset':
            query = metric.query
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[0-9]{1,3}');
            break;
        }
        expr = parseGolangTemplate(query, {
          ...labels,
        });
      }
    } else {
      expr = customExpr;
    }
    const alertName = `${labels.alert_indicator}-${randomString()}`;
    labels.alert_name = alertName;
    return {
      alert: `${alertName}-${md5(resourceName + groupName + expr)}`,
      annotations,
      expr:
        expr +
        labels.alert_indicator_comparison +
        labels.alert_indicator_threshold,
      for: (template.wait || 60) + 's',
      labels,
    };
  }

  cancel() {
    this.close.next(false);
  }
}
