import { LABELS, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import Highcharts, { SeriesLineOptions } from 'highcharts';
import { get } from 'lodash-es';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  getMetricNumericOptions,
  getMetricPercentOptions,
  parseMetricsResponse,
} from 'app/features-shared/app/utils';
import { Alarm } from 'app/services/api/alarm.service';
import {
  IndicatorType,
  Metric,
  MetricService,
} from 'app/services/api/metric.service';
import { PrometheusRuleItem } from 'app/typings';

import {
  getPrometheusExpr,
  getThreshold,
  getUnit,
  getUnitType,
  parseChartOptions,
} from '../alarm.util';

@Component({
  selector: 'rc-alarm-rule-detail-chart',
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
})
export class PrometheusDetailChartComponent extends BaseTimeSelectComponent
  implements OnInit {
  @Input()
  rule: PrometheusRuleItem;

  @Input()
  metricType: IndicatorType[];

  Highcharts = Highcharts;
  chartPercentOptions: Highcharts.Options = getMetricPercentOptions(false);

  chartNumericOptions: Highcharts.Options = getMetricNumericOptions(false);

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly metricService: MetricService,
    translate: TranslateService,
    messageService: MessageService,
  ) {
    super(messageService, translate);
  }

  ngOnInit() {
    const rule = {
      metric_name: this.rule.labels.alert_indicator,
      unit: this.rule.labels.alert_indicator_unit,
      threshold: parseFloat(this.rule.labels.alert_indicator_threshold),
    };
    parseChartOptions(this.chartPercentOptions);
    parseChartOptions(this.chartNumericOptions);
    const threshold = this.getThreshold(rule);
    this.addPlotLines(this.chartPercentOptions, threshold);
    this.addPlotLines(this.chartNumericOptions, threshold);
    const unit = this.getUnit(rule);
    this.chartNumericOptions.tooltip.valueSuffix = unit;
    this.resetTimeRange();
    this.loadCharts();
  }

  getUnit(item: Alarm) {
    return getUnit(item, this.metricType);
  }

  getThreshold(item: Alarm) {
    return getThreshold(item, this.metricType);
  }

  private getUnitType(metric: string) {
    return getUnitType(metric, this.metricType);
  }

  private percentFlag() {
    return (
      get(this.rule, [LABELS, 'alert_indicator_unit']) === '%' ||
      this.getUnitType(get(this.rule, [LABELS, 'alert_indicator'])) === '%'
    );
  }

  loadCharts() {
    const args = {
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    const cluster = get(this.rule, [LABELS, 'alert_cluster']);
    const name = get(this.rule, [LABELS, 'alert_involved_object_name']);
    const range = parseInt(
      get(this.rule, [LABELS, 'alert_indicator_aggregate_range']),
      10,
    );
    const aggregator = get(this.rule, [
      LABELS,
      'alert_indicator_aggregate_function',
    ]);
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 900) {
      this.step = 30;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.endTime = args.end;
    this.chartLoading = true;
    this.chartPercentOptions.series = [];
    this.chartNumericOptions.series = [];
    this.metricService
      .queryMetrics(cluster, {
        start,
        end: args.end,
        queries: [
          {
            aggregator,
            range,
            labels: [
              {
                name: '__name__',
                value: 'custom',
              },
              {
                name: 'expr',
                value: getPrometheusExpr(this.rule),
              },
            ],
          },
        ],
        step: this.step,
      })
      .then(
        (result: Metric[]) => {
          if (result) {
            if (this.percentFlag()) {
              this.chartPercentOptions.series = parseMetricsResponse(
                result,
                name,
                this.endTime,
                this.step,
                100,
              ) as SeriesLineOptions[];
              this.chartNumericOptions.series = [];
            } else {
              this.chartNumericOptions.series = parseMetricsResponse(
                result,
                name,
                this.endTime,
                this.step,
              ) as SeriesLineOptions[];
              this.chartPercentOptions.series = [];
            }
            this.chartLoading = false;
            this.cdr.markForCheck();
          }
        },
        error => {
          this.chartLoading = false;
          this.cdr.markForCheck();
          throw error;
        },
      );
  }

  private addPlotLines(chartOption: any, value: number) {
    chartOption.chart.backgroundColor = 'transparent';
    chartOption.chart.plotBackgroundColor = '#ffffff';
    chartOption.yAxis.plotLines = [
      {
        color: '#ed615f',
        dashStyle: 'dash',
        width: 2,
        value,
        zIndex: 3,
      },
    ];
  }
}
