import { NAME, NAMESPACE, TranslateService } from '@alauda/common-snippet';
import { AxisOptions, Options } from 'highcharts';
import { get } from 'lodash-es';

import { Alarm } from 'app/services/api/alarm.service';
import { IndicatorType } from 'app/services/api/metric.service';
import { PrometheusRuleItem, PrometheusRuleItemLabel } from 'app/typings';
import { formatNumber } from 'app/utils';
export function getUnit(item: Alarm, metricType: IndicatorType[]) {
  // 如果是自定义告警，返回自定义的单位
  if (item.metric_name === 'custom') {
    return item.unit;
  }
  if (!metricType) {
    return '';
  }
  // 如果不是自定义告警，返回与告警指标匹配的单位
  const found = metricType.find(el => {
    return el.name === item.metric_name;
  });
  return found ? found.unit : undefined;
}

export function getThreshold(item: Alarm, metricType: IndicatorType[] = []) {
  if (!metricType) {
    return item.threshold;
  }
  const found = metricType.find(el => {
    return el.name === item.metric_name;
  });
  if ((found && found.unit === '%') || (item.unit && item.unit === '%')) {
    return item.threshold * 100;
  }
  return item.threshold;
}

export function getUnitType(metric: string, metricType: IndicatorType[] = []) {
  if (!metricType) {
    return '';
  }
  const found = metricType.find(el => {
    return el.name === metric;
  });
  if (found) {
    return found.unit;
  }
  return undefined;
}

export function getRule(
  this: { translate: TranslateService },
  item: Alarm,
  metricType: IndicatorType[] = [],
) {
  let metric = item.metric_name;
  const threshold = formatNumber(getThreshold(item, metricType), [1, 0, 4]);
  const translateKey =
    // eslint-disable-next-line sonarjs/no-duplicate-string
    metric === 'workload.log.keyword.count'
      ? 'alarm_log_list_rule'
      : // eslint-disable-next-line sonarjs/no-duplicate-string
      metric === 'workload.event.reason.count'
      ? 'alarm_event_list_rule'
      : '';
  if (translateKey) {
    return this.translate.get(translateKey, {
      time: item.metric.queries[0].range / 60,
      query: item.query,
      compare: item.compare,
      threshold,
    });
  }
  if (item.metric_name === 'custom') {
    if (item.expr.length <= 100) {
      metric = item.expr;
    } else {
      metric = `${item.expr.slice(0, 50)}...${item.expr.slice(-50)}`;
    }
  }
  return `${metric} ${item.compare} ${threshold}${getUnit(
    item,
    metricType,
  )} ${this.translate.get('alarm_wait_time_last', { time: item.wait / 60 })}`;
}

export function getAlarmTypeTitle(metric: string) {
  if (metric === 'custom') {
    return 'metric_custom';
  } else if (metric === 'workload.log.keyword.count') {
    return 'log_alarm';
  } else if (metric === 'workload.event.reason.count') {
    return 'event_alarm';
  } else {
    return 'metric_alarm';
  }
}

export function getPrometheusRule(
  this: { translate: TranslateService },
  item: PrometheusRuleItem,
  metricType: IndicatorType[] = [],
) {
  let metric = item.labels.alert_indicator;
  const rule = {
    metric_name: metric,
    unit: item.labels.alert_indicator_unit,
    threshold: parseFloat(item.labels.alert_indicator_threshold),
  };
  const threshold = formatNumber(getThreshold(rule, metricType), [1, 0, 4]);
  const translateKey =
    metric === 'workload.log.keyword.count'
      ? 'alarm_log_list_rule'
      : metric === 'workload.event.reason.count'
      ? 'alarm_event_list_rule'
      : '';
  if (translateKey) {
    return this.translate.get(translateKey, {
      time: parseInt(item.labels.alert_indicator_aggregate_range, 10) / 60,
      query: item.labels.alert_indicator_query,
      compare: item.labels.alert_indicator_comparison,
      threshold,
    });
  }
  const expr = getPrometheusExpr(item);
  if (metric === 'custom') {
    if (expr.length <= 100) {
      metric = expr;
    } else {
      metric = `${expr.slice(0, 50)}...${expr.slice(-50)}`;
    }
  }
  if (!metric) {
    return '-';
  }
  let time = parseInt(item.for, 10);
  if (item.for.endsWith('s')) {
    time = time / 60;
  } else if (item.for.endsWith('h')) {
    time = time * 60;
  }
  return `${metric} ${
    item.labels.alert_indicator_comparison
  } ${threshold}${getUnit(rule, metricType)} ${this.translate.get(
    'alarm_wait_time_last',
    {
      time,
    },
  )}`;
}

export function getPrometheusExpr(rule: PrometheusRuleItem) {
  const match = new RegExp(
    '(.*)' +
      rule.labels.alert_indicator_comparison +
      '(.*)' +
      rule.labels.alert_indicator_threshold,
  ).exec(rule.expr);
  return match && match[1] ? match[1] : rule.expr;
}

/**
 * 在告警中，图表有灰色背景，增加网格线
 */
export function parseChartOptions(chartOption: Options) {
  chartOption.chart.plotBackgroundColor = '#FCFCFC';
  (chartOption.yAxis as AxisOptions).gridLineWidth = 1;
  (chartOption.xAxis as AxisOptions).gridLineWidth = 1;
  (chartOption.yAxis as AxisOptions).tickPosition = 'inside';
}

/**
 * 解析后端返回的告警，补全参数用于列表和详情页。
 * @param list API返回的告警列表
 * @param search 根据告警名称过滤
 * @param METRICS_TYPE 预设的指标数组，包含指标名称、类型、查询表达式和单位
 * @param clusterName 在用于补全集群名称
 */
export function parseAlertList(
  list: Alarm[],
  search: string,
  METRICS_TYPE: IndicatorType[] = [],
  clusterName?: string,
): Alarm[] {
  if (!list) {
    return [];
  }
  list = list.map((item: Alarm) => {
    let displayName = '';
    let metricName = '';
    let metricTooltip = '';
    let namespace = '';
    let type = '';
    let unit = '';
    let expr = '';
    let kind = '';
    let query = '';
    // 在告警中，metric.queries里有且仅有一个元素
    item.metric.queries[0].labels.forEach(
      (label: { name: string; value: string }) => {
        // http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864640
        // __name__表示监控指标的名字, eg: workload.cpu.utilization/custom/workload.log.keyword.count
        // 非自定义告警时，指标单位需要通过METRICS_TYPE的键值对解析出来
        switch (label.name) {
          case '__name__': {
            metricName = label.value;
            const found = METRICS_TYPE.find(el => {
              return el.name === label.value;
            });
            if (found) {
              unit = found.unit;
            }
            break;
          }
          // kind可以是Cluster、Node、Deployment、DaemonSet和StatefulSet
          case 'kind': {
            kind = label.value;
            break;
          }
          // 非自定义告警时，name表示告警对象的名字
          // 自定义告警时，name为空
          case NAME: {
            displayName = label.value;
            metricTooltip = label.value;
            break;
          }
          // 非自定义告警且是组件告警时，namespace表示组件所在命名空间的名字
          case NAMESPACE:
            namespace = label.value;
            break;
          // expr表示prometheus查询表达式的名字，仅在自定义告警时生效
          case 'expr': {
            expr = label.value;
            break;
          }
          // query表示日志查询条件，在日志告警起作用
          case 'query': {
            query = label.value;
            break;
          }
        }
      },
    );
    // http://confluence.alaudatech.com/pages/viewpage.action?pageId=27171757
    // item.resource_name遵循链接文档的规则，可以通过前缀判断是集群/主机/组件的告警
    // metric_tooltip是拼装好的告警资源，用于展示在告警列表中
    // type可以直接通过kind判断，kind的首字母均为小写
    switch (kind.toLocaleLowerCase()) {
      case 'node':
        type = 'node';
        break;
      case 'cluster':
        type = 'cluster';
        metricTooltip = clusterName;
        break;
      case 'deployment':
      case 'daemonset':
      case 'statefulset':
        type = 'workload';
        break;
      case 'other':
        type = 'other';
        metricTooltip = '-';
        break;
      default:
        type = 'unknown';
        break;
    }
    // 所有自定义告警的阈值单位都直接取unit字段
    if (metricName === 'custom') {
      unit = item.unit;
    }
    // 组件的告警时，告警资源会写成[命名空间.组件名称]的形式
    if (namespace) {
      metricTooltip = `${namespace}.${metricTooltip}`;
    }
    return {
      ...item,
      display_name: displayName,
      metric_name: metricName,
      namespace,
      metric_tooltip: metricTooltip,
      kind,
      type,
      unit,
      expr,
      query,
    };
  });
  if (search) {
    list = list.filter((item: Alarm) => {
      return item.name.includes(search.trim());
    });
  }
  return list;
}

export function randomString() {
  return Math.random().toString(36).slice(2, 7);
}

// eslint-disable-next-line sonarjs/cognitive-complexity
function replaceCondition(str: string, variables: any): string {
  const regex = /{{\s*if\s*(.+?)\s*}}(.*?)({{\s*else\s*if\s*(.+?)\s*}}(.*?))*{{\s*else\s*}}(.*?){{\s*end\s*}}/;
  let result = str;
  let m: RegExpMatchArray | null;

  // tslint:disable-next-line: no-conditional-assignment
  while ((m = regex.exec(result)) !== null) {
    let conditionResult: string;

    const all = m[0];
    const condition = m[1];
    const onTrue = m[2];
    const onIfElse = m[5];
    const onFalse = m[6];

    if (condition.startsWith('.')) {
      let conditionResultState = false;
      const value = get(variables, condition.slice(1));
      if (value === null || value === undefined) {
        conditionResultState = false;
      } else if (typeof value === 'string') {
        conditionResultState = value.length > 0;
      } else if (Array.isArray(value)) {
        conditionResultState = value.length > 0;
      } else if (typeof value === 'boolean') {
        conditionResultState = value;
      } else {
        throw new TypeError(
          `Unexpected type for variable ${condition}: ${typeof value}`,
        );
      }

      conditionResult = conditionResultState ? onTrue : onFalse;
    } else if (condition.startsWith('eq')) {
      const kind = get(variables, 'Kind', '').toLowerCase();
      if (kind === 'deployment') {
        conditionResult = onTrue;
      } else if (kind === 'daemonset') {
        conditionResult = onIfElse;
      } else {
        conditionResult = onFalse;
      }
    } else {
      throw new Error('Functionality not implemented');
    }

    result = result.replace(all, conditionResult);
  }

  return result;
}

function replaceVariable(str: string, variables: any): string {
  const regex = /{{\s*(\..+?)\s*}}/;
  let result = str;
  let m: RegExpMatchArray | null;

  // tslint:disable-next-line: no-conditional-assignment
  while ((m = regex.exec(result)) !== null) {
    const all = m[0];
    const prop = m[1];

    const value = get(variables, prop.slice(1));
    result = result.replace(all, value);
  }

  return result;
}

/**
 * parse template and insert variables
 * @param str golang style template
 * @param variables object of variables to insert
 *
 * 由于 replaceCondition 方法无法处理嵌套的 if..else.. 结构
 * 所以对于 node.resource.request.cpu.utilization 和 node.resource.request.memory.utilization 这两个指标
 * 直接用 Function 和 AliasName 判断，选取对应的模板进行渲染
 * 如果后端对这两个指标的模板进行更改或者添加了新的具有嵌套的 if..else.. 结构的模板，需要更新这个方法的逻辑
 */
export function parseGolangTemplate(
  str: string,
  {
    alert_indicator,
    alert_involved_object_kind,
    alert_involved_object_name,
    alert_involved_object_namespace,
    alert_indicator_aggregate_range,
    alert_indicator_aggregate_function,
    alert_indicator_query,
    alert_cluster,
    hostname,
    application,
  }: PrometheusRuleItemLabel,
): string {
  const labels = {
    Indicator: alert_indicator,
    Kind: alert_involved_object_kind,
    Name: alert_involved_object_name,
    Range: alert_indicator_aggregate_range,
    // eslint-disable-next-line @typescript-eslint/camelcase
    Function: alert_indicator_aggregate_function || '',
    // eslint-disable-next-line @typescript-eslint/camelcase
    Query: alert_indicator_query || '',
    ClusterName: alert_cluster,
    Namespace: alert_involved_object_namespace,
    AliasName: hostname || '',
    Application: application || '',
  };
  return replaceVariable(replaceCondition(str, labels), labels);
}

export function parseNotifications(notifications: string) {
  try {
    return JSON.parse(notifications).map(
      (notification: { name: string }) => notification.name,
    );
  } catch {
    return [];
  }
}

export const eventReasonList = [
  // Container event reason list
  'Created',
  'Started',
  'Failed',
  'Killing',
  'Preempting',
  'BackOff',
  'ExceededGracePeriod',

  // Pod event reason list
  'FailedKillPod',
  'FailedCreatePodContainer',
  'NetworkNotReady',

  // Image event reason list
  'Pulling',
  'Pulled',
  'InspectFailed',
  'ErrImageNeverPull',
  'BackOff',

  // kubelet event reason list
  'NodeReady',
  'NodeNotReady',
  'NodeSchedulable',
  'NodeNotSchedulable',
  'Starting',
  'KubeletSetupFailed',
  'FailedAttachVolume',
  'FailedMount',
  'VolumeResizeFailed',
  'VolumeResizeSuccessful',
  'FileSystemResizeFailed',
  'FileSystemResizeSuccessful',
  'FailedMapVolume',
  'AlreadyMountedVolume',
  'SuccessfulAttachVolume',
  'SuccessfulMountVolume',
  'Rebooted',
  'ContainerGCFailed',
  'ImageGCFailed',
  'FailedNodeAllocatableEnforcement',
  'NodeAllocatableEnforced',
  'SandboxChanged',
  'FailedCreatePodSandBox',
  'FailedPodSandBoxStatus',

  // Image manager event reason list
  'InvalidDiskCapacity',
  'FreeDiskSpaceFailed',

  // Probe event reason list
  'Unhealthy',
  'ProbeWarning',

  // Pod worker event reason list
  'FailedSync',

  // Config event reason list
  'FailedValidation',

  // Lifecycle hooks
  'FailedPostStartHook',
  'FailedPreStopHook',
];
