import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { parseNotifications } from 'app/features-shared/alarm/alarm.util';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  PrometheusRule,
  RESOURCE_TYPES,
  ResourceType,
  WorkspaceBaseParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmDetailComponent {
  name: string;
  workloadKind: string;
  workloadName: string;
  workloadUid: string;
  appName: string;
  baseParams: WorkspaceBaseParams;
  parseNotifications = parseNotifications;

  dataManager = new AsyncDataLoader<{
    resource: PrometheusRule;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    fetcher: () => {
      return combineLatest([
        this.k8sApi.getResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.baseParams.cluster,
          name: this.name,
          namespace: this.baseParams.namespace,
        }),
        this.k8sPermission.isAllowed({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
        }),
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly messageService: MessageService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {
    this.name = this.route.snapshot.params.name;
    this.workloadKind = this.route.snapshot.queryParams.kind;
    this.workloadName = this.route.snapshot.queryParams.name;
    this.workloadUid = this.route.snapshot.queryParams.uid;
    this.appName = this.route.snapshot.queryParams.appName;
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  update(rule: PrometheusRule) {
    this.router.navigate(['../../', 'update', rule.metadata.name], {
      relativeTo: this.route,
      queryParams: {
        kind: this.workloadKind,
        name: this.workloadName,
        appName: this.appName,
      },
    });
  }

  delete(rule: PrometheusRule) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: rule.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.PROMETHEUS_RULE,
              cluster: this.k8sUtil.getLabel(rule, 'cluster', 'alert'),
              resource: rule,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(this.navigateToList)
      .catch(noop);
  }

  navigateToList = () => {
    switch (this.workloadKind) {
      case 'application': {
        this.router.navigate(['app', 'detail', this.appName], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
          queryParams: {
            tabIndex: 8,
          },
        });
        break;
      }
      case 'deployment':
      case 'daemonset':
      case 'statefulset': {
        this.router.navigate([this.workloadKind, 'detail', this.workloadName], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
          queryParams: {
            tabIndex: 7,
          },
        });
        break;
      }
    }
  };
}
