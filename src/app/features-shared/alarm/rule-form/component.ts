import { Component, Injector, Input, OnInit } from '@angular/core';
import { upperFirst } from 'lodash-es';
import md5 from 'md5';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { AlarmRule } from 'app/features-shared/alarm/rule-base-form/component';
import { IndicatorType } from 'app/services/api/metric.service';
import { PrometheusRuleItem, PrometheusRuleItemLabel } from 'app/typings';

import {
  getPrometheusExpr,
  parseGolangTemplate,
  randomString,
} from '../alarm.util';

@Component({
  selector: 'rc-prometheus-alarm-rule-form',
  templateUrl: 'template.html',
})
export class PrometheusAlarmRuleFormComponent
  extends BaseResourceFormGroupComponent<
    PrometheusRuleItem,
    { rule: AlarmRule }
  >
  implements OnInit {
  @Input()
  modalData: {
    kind: string;
    metricType: IndicatorType[];
    cluster: string;
    namespace: string;
    podName: string;
    isOCP: boolean;
    node: string;
    hostname: string;
    application: string;
    project: string;
  };

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getDefaultFormModel() {
    return {
      rule: {
        alarm_type: 'default',
        annotations: {},
        data_type: false,
        labels: {},
      },
    };
  }

  createForm() {
    return this.fb.group({
      rule: [{}],
    });
  }

  adaptResourceModel(resource: PrometheusRuleItem): { rule: AlarmRule } {
    if (!resource) {
      return;
    }
    const {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      alert_current_value,
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      alert_notifications,
      ...annotations
    } = resource.annotations;

    let wait = parseInt(resource.for, 10);
    if (resource.for.endsWith('m')) {
      wait = wait * 60;
    } else if (resource.for.endsWith('h')) {
      wait = wait * 3600;
    }

    return {
      rule: {
        annotations,
        aggregate_function: resource.labels.alert_indicator_aggregate_function,
        aggregate_range: Number(
          resource.labels.alert_indicator_aggregate_range,
        ),
        alarm_type:
          resource.labels.alert_indicator === 'custom'
            ? 'custom'
            : resource.labels.alert_indicator === 'workload.log.keyword.count'
            ? 'log'
            : resource.labels.alert_indicator === 'workload.event.reason.count'
            ? 'event'
            : 'default',
        compare: resource.labels.alert_indicator_comparison,
        data_type: !!resource.labels.alert_indicator_aggregate_function,
        expr:
          resource.labels.alert_indicator === 'custom'
            ? getPrometheusExpr(resource)
            : null,
        labels: resource.labels,
        query: resource.labels.alert_indicator_query,
        metric: resource.labels.alert_indicator,
        severity: resource.labels.severity,
        threshold: Number(resource.labels.alert_indicator_threshold),
        unit: resource.labels.alert_indicator_unit,
        wait,
      },
    };
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  adaptFormModel(formModel: { rule: AlarmRule }): PrometheusRuleItem {
    const rule = formModel.rule;
    let resourceName: string;
    let groupName: string;
    if (this.modalData.podName) {
      resourceName = `ns-${this.modalData.namespace}.${this.modalData.podName}.rules`;
      groupName = `${this.modalData.kind}.${this.modalData.podName}`;
    } else if (this.modalData.node) {
      resourceName = `node-${this.modalData.node}.rules`;
      groupName = `node.${this.modalData.node}`;
    } else {
      resourceName = `cluster-${this.modalData.cluster}.rules`;
      groupName = `cluster.${this.modalData.cluster}`;
    }
    const labels = {
      ...rule.labels,
      severity: rule.severity,
      alert_involved_object_kind: upperFirst(this.modalData.kind),
      alert_involved_object_name:
        this.modalData.podName || this.modalData.node || this.modalData.cluster,
      alert_involved_object_namespace: this.modalData.namespace || '',
      alert_cluster: this.modalData.cluster,
      alert_project: '',
      alert_indicator: rule.alarm_type === 'custom' ? 'custom' : rule.metric,
      alert_indicator_aggregate_range: rule.aggregate_range
        ? String(rule.aggregate_range)
        : '0',
      alert_indicator_aggregate_function: rule.aggregate_function,
      alert_indicator_comparison: rule.compare,
      alert_indicator_threshold: String(rule.threshold),
      alert_indicator_query: rule.query,
      alert_indicator_unit: rule.unit,
    } as PrometheusRuleItemLabel;
    const annotations = {
      ...rule.annotations,
      alert_current_value: '{{ $value }}',
    };
    if (this.modalData.application) {
      labels.application = this.modalData.application;
    }
    if (this.modalData.project) {
      labels.alert_project = this.modalData.project;
    }
    let expr;
    if (rule.metric && rule.metric !== 'custom') {
      const metric = this.modalData.metricType.find(
        ({ name }) => name === rule.metric,
      );
      if (metric.query) {
        let nodeName = '';
        if (this.modalData.kind === 'node') {
          if (this.modalData.isOCP) {
            nodeName = this.modalData.hostname;
          } else {
            nodeName = this.modalData.node + ':.*';
          }
          if (labels.alert_involved_object_name === '0.0.0.0') {
            nodeName = '.*';
          }
        }
        let query = metric.query;
        switch (upperFirst(this.modalData.kind)) {
          case 'Deployment':
            query = metric.query
              // eslint-disable-next-line sonarjs/no-duplicate-string
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[a-z0-9]{7,10}-[a-z0-9]{5}');
            break;
          case 'Daemonset':
            query = metric.query
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[a-z0-9]{5}');
            break;
          case 'Statefulset':
            query = metric.query
              .split('{{.PodNamePattern}}')
              .join('{{.Name}}-[0-9]{1,3}');
            break;
        }
        expr = parseGolangTemplate(query, {
          ...labels,
          alert_involved_object_name:
            nodeName || labels.alert_involved_object_name,
          hostname:
            labels.alert_involved_object_name === '0.0.0.0'
              ? '.*'
              : this.modalData.hostname,
        });
      }
    } else {
      expr = rule.expr;
    }
    const alertName = `${labels.alert_indicator}-${randomString()}`;
    labels.alert_name = alertName;
    return {
      alert: `${alertName}-${md5(resourceName + groupName + expr)}`,
      annotations,
      expr:
        expr +
        labels.alert_indicator_comparison +
        labels.alert_indicator_threshold,
      for: (rule.wait || 60) + 's',
      labels,
    };
  }
}
