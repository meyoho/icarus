import { TranslateService } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { PrometheusAlarmRuleDialogComponent } from 'app/features-shared/alarm/rule-dialog/component';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { PrometheusRuleItem } from 'app/typings';

import { getAlarmTypeTitle, getPrometheusRule } from '../alarm.util';

@Component({
  selector: 'rc-alarm-rule-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PrometheusAlarmRuleListComponent),
      multi: true,
    },
  ],
})
export class PrometheusAlarmRuleListComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  kind: string;

  @Input()
  cluster: string;

  @Input()
  isOCP: boolean;

  @Input()
  node: string;

  @Input()
  hostname: string;

  @Input()
  application: string;

  @Input()
  project: string;

  @Input()
  namespace: string;

  @Input()
  podName: string;

  templates: PrometheusRuleItem[];
  metricType: IndicatorType[] = [];

  getPrometheusRule = getPrometheusRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  onChange: (templates: PrometheusRuleItem[]) => void;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
    private readonly metricService: MetricService,
    public translate: TranslateService,
  ) {}

  async ngOnInit() {
    this.metricType = await this.metricService.getIndicators(this.cluster);
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(
      PrometheusAlarmRuleDialogComponent,
      {
        data: {
          kind: this.kind,
          cluster: this.cluster,
          isOCP: this.isOCP,
          node: this.node,
          hostname: this.hostname,
          metricType: this.metricType,
          application: this.application,
          project: this.project,
          namespace: this.namespace,
          podName: this.podName,
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.close.subscribe((el: PrometheusRuleItem) => {
      if (el) {
        this.templates.push(el);
        this.onChange(this.templates);
      }
      dialogRef.close();
    });
  }

  edit(index: number) {
    const dialogRef = this.dialogService.open(
      PrometheusAlarmRuleDialogComponent,
      {
        data: {
          kind: this.kind,
          cluster: this.cluster,
          isOCP: this.isOCP,
          node: this.node,
          hostname: this.hostname,
          metricType: this.metricType,
          application: this.application,
          project: this.project,
          namespace: this.namespace,
          podName: this.podName,
          data: this.templates[index],
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.close.subscribe((el: PrometheusRuleItem) => {
      if (el) {
        this.templates[index] = el;
        this.onChange(this.templates);
      }
      dialogRef.close();
    });
  }

  remove(index: number) {
    this.templates.splice(index, 1);
    this.onChange(this.templates);
  }

  writeValue(templates: PrometheusRuleItem[]): void {
    if (templates && this.templates !== templates) {
      this.templates = templates;
      this.cdr.markForCheck();
    }
  }

  registerOnChange(fn: (templates: PrometheusRuleItem[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void): void {
    //
  }
}
