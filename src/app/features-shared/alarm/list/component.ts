import {
  COMMON_WRITABLE_ACTIONS,
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  NAMESPACE,
  ResourceListParams,
  TranslateService,
  matchLabelsToString,
  noop,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { upperFirst } from 'lodash-es';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { PrometheusRule, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { ACTION, STATUS } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { parseNotifications } from '../alarm.util';
import { WorkloadAlarmTemplateApplyComponent } from '../apply-template/component';

@Component({
  selector: 'rc-alarm-list',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkloadAlarmListComponent implements OnInit {
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  @Input()
  listParam = {
    workload_kind: '',
    workload_name: '',
    workload_uid: '',
    app_name: '',
  };

  parseNotifications = parseNotifications;
  columns = [NAME, STATUS, 'notification', DESCRIPTION, 'created_time', ACTION];

  metricType: IndicatorType[];
  alertState: Array<{
    name: string;
    state: string;
  }>;

  params$ = this.activatedRoute.queryParamMap.pipe(
    distinctUntilChanged((p, q) => p.get('keyword') === q.get('keyword')),
    map(params => {
      let labelSelector = {
        [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
      };
      switch (this.listParam.workload_kind) {
        case 'deployment':
        case 'daemonset':
        case 'statefulset':
          labelSelector = {
            ...labelSelector,
            [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this.baseParams
              .namespace,
            [this.k8sUtil.normalizeType('kind', 'alert')]: upperFirst(
              this.listParam.workload_kind,
            ),
            [this.k8sUtil.normalizeType(NAME, 'alert')]: this.listParam
              .workload_name,
          };
          break;
        case 'application':
          labelSelector = {
            ...labelSelector,
            [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this.baseParams
              .namespace,
            [this.k8sUtil.normalizeType('application', 'alert')]: this.listParam
              .workload_name,
          };
      }
      return {
        cluster: this.baseParams.cluster,
        keyword: params.get('keyword'),
        labelSelector: matchLabelsToString(labelSelector),
        namespace: this.baseParams.namespace,
      };
    }),
  );

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  list = new K8SResourceList({
    fetcher: this.fetchAlarmList.bind(this),
    fetchParams$: this.params$,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.PROMETHEUS_RULE,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    public cdr: ChangeDetectorRef,
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly messageService: MessageService,
    private readonly metricService: MetricService,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {}

  ngOnInit() {
    if (this.listParam.workload_kind === 'application') {
      this.columns = [
        NAME,
        'workload',
        STATUS,
        'notification',
        DESCRIPTION,
        'created_time',
        ACTION,
      ];
    }
    this.metricService
      .getIndicators(this.baseParams.cluster)
      .then(metricType => {
        this.metricType = metricType;
      });
    this.metricService
      .getPrometheusMetricLabels(this.baseParams.cluster, 'ALERTS{}')
      .then(labels => {
        this.alertState = labels.map(el => ({
          name: el.metric.alertname,
          state: el.metric.alertstate,
        }));
        this.cdr.markForCheck();
      });
  }

  trackByFn(_: number, item: PrometheusRule) {
    return item.metadata.uid;
  }

  fetchAlarmList({ cluster, namespace, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<PrometheusRule>({
      type: RESOURCE_TYPES.PROMETHEUS_RULE,
      cluster,
      namespace,
      queryParams,
    });
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  createAlarmTemplate() {
    const dialogRef = this.dialogService.open(
      WorkloadAlarmTemplateApplyComponent,
      {
        data: {
          metricType: this.metricType,
          componentName: this.listParam.workload_name,
          clusterName: this.baseParams.cluster,
          namespaceName: this.baseParams.namespace,
          appName: this.listParam.app_name || '',
          projectName: this.baseParams.project,
          kind: this.listParam.workload_kind,
          uid: this.listParam.workload_uid,
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.close.subscribe((rule: PrometheusRule) => {
      dialogRef.close();
      this.alarmDetail(rule);
    });
  }

  createAlarm() {
    this.router.navigate(['alert', 'create'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        kind: this.listParam.workload_kind,
        name: this.listParam.workload_name,
        uid: this.listParam.workload_uid,
        appName: this.listParam.app_name,
      },
    });
  }

  updateAlarm(rule: PrometheusRule) {
    const kind = this.k8sUtil.getLabel(rule, 'kind', 'alert').toLowerCase();
    const podName = this.k8sUtil.getLabel(rule, NAME, 'alert');
    this.router.navigate(
      [kind, 'detail', podName, 'alert', 'update', rule.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
        queryParams: {
          kind: this.listParam.workload_kind,
          name: this.listParam.workload_name,
          appName: this.listParam.app_name,
        },
      },
    );
  }

  deleteAlarm(rule: PrometheusRule) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: rule.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.PROMETHEUS_RULE,
              cluster: this.k8sUtil.getLabel(rule, 'cluster', 'alert'),
              resource: rule,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => this.list.delete(rule))
      .catch(noop);
  }

  alarmDetail(rule: PrometheusRule) {
    const kind = this.k8sUtil.getLabel(rule, 'kind', 'alert').toLowerCase();
    const podName = this.k8sUtil.getLabel(rule, NAME, 'alert');
    this.router.navigate(
      [kind, 'detail', podName, 'alert', 'detail', rule.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
        queryParams: {
          kind: this.listParam.workload_kind,
          name: this.listParam.workload_name,
          appName: this.listParam.app_name,
        },
      },
    );
  }

  workloadDetail(rule: PrometheusRule) {
    const kind = this.k8sUtil.getLabel(rule, 'kind', 'alert').toLowerCase();
    const podName = this.k8sUtil.getLabel(rule, NAME, 'alert');
    this.router.navigate([kind, 'detail', podName], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
