import {
  Locale,
  StringMap,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject, from, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  takeUntil,
} from 'rxjs/operators';

import { AGGREGATORS, LEVELS, UNITS } from 'app/features-shared/app/utils';
import { AdvanceApiService } from 'app/services/api/advance.service';
import { AlarmService } from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import {
  ALARM_LABEL_KEY_NAME_PATTERN,
  ALARM_METRIC_PATTERN,
  ALARM_THRESHOLD_PATTERN,
  NATURAL_NUMBER_PATTERN,
  formatNumber,
} from 'app/utils';

import { eventReasonList, getUnitType } from '../alarm.util';

const ALARM_TIME_STAMP_OPTIONS = [1, 2, 3, 5, 10, 30].map(el => ({
  type: el,
  offset: el * 60,
}));

interface BaseRule {
  aggregate_function?: string;
  aggregate_range?: number;
  alarm_type: string;
  annotations: StringMap;
  compare?: string;
  data_type: boolean;
  expr?: string;
  labels: StringMap;
  metric?: string;
  severity?: string;
  unit?: string;
  wait?: number;
}

export interface AlarmRuleFormModel extends BaseRule {
  query?: string[];
  threshold: string;
}

export interface AlarmRule extends BaseRule {
  query?: string;
  threshold?: number;
}

@Component({
  selector: 'rc-base-alarm-rule-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseAlarmRuleFormComponent
  extends BaseResourceFormGroupComponent<AlarmRule, AlarmRuleFormModel>
  implements OnInit, OnDestroy {
  @Input()
  metricType: IndicatorType[];

  @Input()
  kind: string;

  @Input()
  clusterName: string;

  @Input()
  node: string;

  eventReasonList = eventReasonList;

  isDeployed$ = this.advanceApiService.getDiagnoseInfo().pipe(publishRef());

  metrics: IndicatorType[];
  timeStampOptions = ALARM_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type + this.translate.get('minutes'),
    };
  });

  units = UNITS;
  aggregators = AGGREGATORS;
  levels = LEVELS;
  compareOptions = ['>', '>=', '==', '<=', '<', '!='];
  aggregationEnabled = true;
  prometheusMetrics: string[];
  functionNames: string[];
  prometheusMetricsSuggestion: string[];
  functionNamesSuggestion: string[];
  prometheusMetricLabels: string[];
  metricReg = ALARM_METRIC_PATTERN;
  thresholdTip = ALARM_THRESHOLD_PATTERN.tip;
  labelValidator = {
    key: [Validators.pattern(ALARM_LABEL_KEY_NAME_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      pattern: this.translate.get(ALARM_LABEL_KEY_NAME_PATTERN.tip),
    },
  };

  onDestroy$ = new Subject<void>();
  expressionChanged$ = new Subject<string>();

  constructor(
    injector: Injector,
    private readonly advanceApiService: AdvanceApiService,
    private readonly alarmService: AlarmService,
    private readonly auiNotificationService: NotificationService,
    private readonly metricService: MetricService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.initFormControls();
    this.setUpListener();
    this.setUpAutoComplete();
  }

  private initFormControls() {
    let unit = '';
    const metricValue = this.form.get('metric').value;
    const threshold = this.form.get('threshold');
    switch (this.form.get('alarm_type').value) {
      case 'default':
        this.filterMetrics(this.kind, !metricValue);
        unit = getUnitType(metricValue, this.metricType);
        if (unit === '%') {
          threshold.setValue(formatNumber(threshold.value * 100, [1, 0, 4]));
        }
        break;
      case 'log':
      case 'event':
        this.form.get('query').enable();
        this.form.patchValue({
          unit: 'items',
        });
        break;
      case 'custom':
        this.form.get('metric').disable();
        this.form.get('expr').enable();
        break;
    }
    if (this.form.get('data_type').value) {
      this.form.get('aggregate_function').enable();
    }
    if (
      // eslint-disable-next-line sonarjs/no-duplicate-string
      this.form.get('metric').value === 'workload.log.keyword.count' ||
      // eslint-disable-next-line sonarjs/no-duplicate-string
      this.form.get('metric').value === 'workload.event.reason.count'
    ) {
      this.thresholdTip = NATURAL_NUMBER_PATTERN.tip;
      this.form.get('wait').disable();
      this.form
        .get('threshold')
        .setValidators([
          Validators.required,
          Validators.pattern(NATURAL_NUMBER_PATTERN.pattern),
        ]);
    }
    this.form.get('threshold').updateValueAndValidity();
  }

  setQuery(query: string) {
    this.form.get('query').setValue(query);
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private setUpListener() {
    this.expressionChanged$
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(300),
        distinctUntilChanged(),
      )
      .subscribe(expression => {
        this.predictExpression(expression);
      });
    this.form
      .get('alarm_type')
      .valueChanges.pipe(
        takeUntil(this.onDestroy$),
        filter(value => !!value),
      )
      .subscribe(alarmType => {
        if (alarmType === 'custom') {
          this.form.get('metric').disable();
          this.form.get('expr').enable();
          this.form.patchValue({
            data_type: false,
            metric: '',
          });
        } else {
          this.form.get('metric').enable();
          this.form.get('expr').disable();
          this.form.patchValue({
            unit: '',
          });
        }
        if (alarmType === 'log') {
          this.form.get('query').enable();
          this.form.patchValue({
            data_type: false,
            unit: 'items',
            metric: 'workload.log.keyword.count',
            query: '',
          });
        } else if (alarmType === 'event') {
          this.form.get('query').enable();
          this.form.patchValue({
            data_type: false,
            unit: 'items',
            metric: 'workload.event.reason.count',
            query: '',
          });
        } else {
          this.form.get('query').disable();
          this.form.patchValue({
            unit: '',
          });
        }
        if (alarmType === 'default') {
          this.filterMetrics(this.kind, true);
        }
      });
    this.form
      .get('data_type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(dataType => {
        if (dataType) {
          this.form.get('aggregate_function').enable();
          this.form.patchValue({
            aggregate_range: this.timeStampOptions[0].offset,
            aggregate_function: this.aggregators[0].key,
          });
        } else {
          this.form.patchValue({
            aggregate_range: 0,
            aggregate_function: '',
          });
          if (
            this.form.get('alarm_type').value === 'log' ||
            this.form.get('alarm_type').value === 'event'
          ) {
            this.form.patchValue({
              aggregate_range: this.timeStampOptions[0].offset,
            });
          }
          this.form.get('aggregate_function').disable();
        }
      });
    this.form
      .get('metric')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(metric => {
        if (
          metric === 'workload.log.keyword.count' ||
          metric === 'workload.event.reason.count'
        ) {
          this.thresholdTip = NATURAL_NUMBER_PATTERN.tip;
          this.form.get('wait').disable();
          this.form
            .get('threshold')
            .setValidators([
              Validators.required,
              Validators.pattern(NATURAL_NUMBER_PATTERN.pattern),
            ]);
        } else {
          this.thresholdTip = ALARM_THRESHOLD_PATTERN.tip;
          this.form.get('wait').enable();
          this.form
            .get('threshold')
            .setValidators([
              Validators.required,
              Validators.pattern(ALARM_THRESHOLD_PATTERN.pattern),
            ]);
        }
        const selectedMetric = this.metricType.find(el => el.name === metric);
        this.aggregationEnabled = true;
        if (
          selectedMetric &&
          selectedMetric.aggregation_enabled !== undefined &&
          !selectedMetric.aggregation_enabled
        ) {
          this.aggregationEnabled = false;
          this.form.patchValue({
            data_type: false,
          });
        }
        this.form.get('threshold').updateValueAndValidity();
      });
  }

  private setUpAutoComplete() {
    if (this.kind === 'node') {
      this.updatePredictExpression(this.node);
    }
    Promise.all([
      this.metricService
        .getPrometheusMetrics(this.clusterName)
        .then(prometheusMetrics => {
          this.prometheusMetrics = prometheusMetrics;
        }),
      this.alarmService.getPrometheusFunctions().then(functionNames => {
        this.functionNames = functionNames;
      }),
    ]);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      alarm_type: ['default'],
      metric: ['', Validators.required],
      expr: [
        { value: '', disabled: true },
        {
          validators: [Validators.required],
          asyncValidators: [this.prometheusMetricValidate.bind(this)],
          updateOn: 'blur',
        },
      ],
      unit: [''],
      aggregate_function: [{ value: '', disabled: true }, Validators.required],
      data_type: [false],
      aggregate_range: [0],
      query: [
        { value: [''], disabled: true },
        {
          validators: [Validators.required],
          updateOn: 'blur',
        },
      ],
      severity: ['', Validators.required],
      compare: ['', Validators.required],
      threshold: [
        '',
        [
          Validators.required,
          Validators.pattern(ALARM_THRESHOLD_PATTERN.pattern),
        ],
      ],
      wait: [0, Validators.required],
      labels: [{}],
      annotations: [{}],
    });
  }

  getDefaultFormModel(): AlarmRuleFormModel {
    return {
      alarm_type: 'default',
      annotations: {},
      compare: this.compareOptions[0],
      data_type: false,
      labels: {},
      metric: '',
      severity: this.levels[2],
      threshold: '',
      wait: this.timeStampOptions[0].offset,
    };
  }

  adaptFormModel(formModel: AlarmRuleFormModel): AlarmRule {
    const { query, threshold, ...remaining } = formModel;
    return {
      ...remaining,
      query: query
        ? query
            .map(str => {
              // 将多个空格转换为一个空格并且删除字符串两端的空白字符
              return str.replace(/\s+/g, ' ').trim();
            })
            .join(' ')
        : '',
      threshold:
        this.getUnit() === '%'
          ? (Number(threshold) * 10000) / 1000000
          : Number(threshold),
    };
  }

  adaptResourceModel(resource: AlarmRule): AlarmRuleFormModel {
    if (resource) {
      const unit =
        resource.alarm_type === 'default'
          ? getUnitType(resource.metric, this.metricType)
          : resource.unit;
      const { query, labels, threshold, ...resourceRemaining } = resource;
      let labelsEditable: StringMap;
      if (labels) {
        const {
          /* eslint-disable @typescript-eslint/no-unused-vars */
          severity,
          application,
          alert_name,
          alert_involved_object_kind,
          alert_involved_object_name,
          alert_involved_object_namespace,
          alert_cluster,
          alert_project,
          alert_creator,
          alert_indicator,
          alert_indicator_aggregate_range,
          alert_indicator_aggregate_function,
          alert_indicator_comparison,
          alert_indicator_threshold,
          alert_indicator_query,
          alert_indicator_unit,
          ...labelsRemaining
        } = labels;
        labelsEditable = labelsRemaining;
      }
      return {
        ...resourceRemaining,
        labels: labelsEditable,
        query:
          resource.metric === 'workload.log.keyword.count' ||
          resource.metric === 'workload.event.reason.count'
            ? query.split(' ')
            : null,
        threshold:
          threshold !== undefined
            ? unit === '%'
              ? formatNumber(threshold * 100, [1, 0, 4])
              : String(threshold)
            : null,
      };
    }
  }

  onExpressionInput(event: Event) {
    this.expressionChanged$.next((event.target as HTMLInputElement).value);
  }

  /**
   * 初始化表单和告警类型切换成指标告警时调用，修改指标(Morgans预设指标)下拉框中的选项
   * @param kind 用于过滤出metric_type中集群、主机或者计算组件的指标
   * @param patchValue 是否自动选中下拉框中的第一项
   */
  private filterMetrics(kind: string, patchValue = true) {
    if (['deployment', 'daemonset', 'statefulset'].find(el => el === kind)) {
      kind = 'workload';
    }
    this.metrics = this.metricType.filter(el => {
      return (
        el.kind === kind &&
        el.type === 'metric' &&
        (el.alert_enabled || el.alert_enabled === undefined)
      );
    });
    if (
      patchValue &&
      !this.metrics.find(
        element => element.name === this.form.get('metric').value,
      ) &&
      this.metrics.length > 0
    ) {
      this.form.patchValue({
        metric: this.metrics[0].name,
      });
    }
  }

  /**
   * 监控类型为主机、告警类型为自定义告警时设置指标(Prometheus表达式)自动补全下拉菜单的值
   * @param nodeIp 主机实例的IP
   */
  private updatePredictExpression(nodeIp: string) {
    this.metricService
      .getPrometheusMetricLabels(this.clusterName, `{instance=~"${nodeIp}:.*"}`)
      .then(labels => {
        const set = new Set(labels.map(el => el.metric.__name__));
        this.prometheusMetrics = [...set];
      });
  }

  /**
   * 告警类型为自定义告警时设置指标(Prometheus表达式)自动补全下拉菜单的值
   * 见http://confluence.alaudatech.com/x/gIIyAg 最下方的辅助输入API
   * @param exp 指标输入框的值
   */
  private predictExpression(exp: string) {
    const found = exp.match(/([\w-])+$/g);
    if (found && found[0]) {
      const match = found[0].toLowerCase();
      // 监控指标补全
      this.prometheusMetricsSuggestion = this.prometheusMetrics
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      // 函数名补全
      this.functionNamesSuggestion = this.functionNames
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      this.cdr.markForCheck();
    }
    // label的可用值补全
    let foundLabel = exp.match(/([\w-])+=([\w-])+$/g);
    if (foundLabel && foundLabel[0]) {
      foundLabel = foundLabel[0].split('=');
      this.metricService
        .getPrometheusMetricLabels(this.clusterName, foundLabel[0])
        .then(labels => {
          this.prometheusMetricLabels = labels
            .filter(el => {
              return el.value[1].startsWith(foundLabel[1]);
            })
            .map(el => {
              return exp + el.value[1].split(foundLabel[1])[1];
            });
          this.cdr.markForCheck();
        });
    }
  }

  /**
   * 告警类型为自定义告警时指标(Prometheus表达式)异步校验
   */
  private prometheusMetricValidate(ctrl: AbstractControl) {
    return from(
      this.metricService.getPrometheusMetricLabels(
        this.clusterName,
        ctrl.value,
      ),
    ).pipe(
      map(() => {
        return null;
      }),
      catchError(err => {
        if (err.code === 400) {
          this.auiNotificationService.error({
            title: this.translate.get(ALARM_METRIC_PATTERN.tip),
            content: err.message,
          });
          return of({
            metric: this.translate.get(ALARM_METRIC_PATTERN.tip),
          });
        } else {
          return of(null);
        }
      }),
    );
  }

  /**
   * 获取当前告警规则表单的单位
   * 告警类型为指标告警时为和Morgans预设指标对应的单位
   * 告警类型为自定义告警时为unit表单项的值
   */
  getUnit() {
    if (this.form.get('alarm_type').value === 'default') {
      return getUnitType(this.form.get('metric').value, this.metricType);
    }
    return this.form.get('unit').value;
  }

  getAnnotations = (metric: IndicatorType) => {
    if (metric.annotations) {
      return this.translate.locale === Locale.EN
        ? metric.annotations.en
        : metric.annotations.cn;
    }
    return metric.name;
  };
}
