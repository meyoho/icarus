import { TranslateService } from '@alauda/common-snippet';
import { Status, StatusType } from '@alauda/ui';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { IndicatorType } from 'app/services/api/metric.service';
import { PrometheusRuleItem } from 'app/typings';
import { getAlarmStatus } from 'app/utils';

import { getAlarmTypeTitle, getPrometheusRule } from '../alarm.util';

@Component({
  selector: 'rc-alarm-status-bar',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class AlarmStatusBarComponent implements OnInit, OnChanges {
  @Input()
  rules: PrometheusRuleItem[];

  @Input()
  alarmState: Array<{
    name: string;
    state: string;
  }> = [];

  @Input()
  metricType: IndicatorType[];

  alarmStatus: Status[];

  getAlarmStatus = getAlarmStatus;
  getPrometheusRule = getPrometheusRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    this.setStatusBar();
  }

  ngOnChanges({ alarmState }: SimpleChanges) {
    if (alarmState && alarmState.currentValue) {
      this.setStatusBar();
    }
  }

  setStatusBar() {
    this.rules.forEach(rule => {
      const found = this.alarmState
        ? this.alarmState.find(el => {
            return el.name === rule.alert;
          })
        : null;
      rule.labels = {
        ...rule.labels,
        alarm_status: found ? found.state : '',
      };
    });
    const firingRules = this.getRuleStateLength('firing');
    const pendingRules = this.getRuleStateLength('pending');
    this.alarmStatus = [
      {
        scale: firingRules,
        type: StatusType.Error,
      },
      {
        scale: pendingRules,
        type: StatusType.Pending,
      },
      {
        scale: this.rules.length - firingRules - pendingRules,
        type: StatusType.Success,
      },
    ];
  }

  getRuleStateLength(status: string) {
    return this.rules.filter(el => el.labels.alarm_status === status).length;
  }
}
