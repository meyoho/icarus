import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { AlarmActionFormComponent } from './action-form/component';
import { AlarmActionListComponent } from './action-list/component';
import { WorkloadAlarmTemplateApplyComponent } from './apply-template/component';
import { AlarmDetailComponent } from './detail/component';
import { AlarmFormComponent } from './form/component';
import { WorkloadAlarmListComponent } from './list/component';
import { AlarmMetricChartComponent } from './metric-chart/component';
import { BaseAlarmRuleFormComponent } from './rule-base-form/component';
import { PrometheusDetailChartComponent } from './rule-chart/component';
import { PrometheusDetailRuleListComponent } from './rule-detail-list/component';
import { PrometheusAlarmRuleDialogComponent } from './rule-dialog/component';
import { PrometheusAlarmRuleFormComponent } from './rule-form/component';
import { PrometheusAlarmRuleListComponent } from './rule-list/component';
import { AlarmStatusBarComponent } from './status-bar/component';

const components = [
  AlarmActionFormComponent,
  AlarmActionListComponent,
  AlarmFormComponent,
  AlarmDetailComponent,
  BaseAlarmRuleFormComponent,
  AlarmStatusBarComponent,
  AlarmMetricChartComponent,
  WorkloadAlarmListComponent,
  WorkloadAlarmTemplateApplyComponent,
  PrometheusDetailChartComponent,
  PrometheusDetailRuleListComponent,
  PrometheusAlarmRuleDialogComponent,
  PrometheusAlarmRuleFormComponent,
  PrometheusAlarmRuleListComponent,
];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
})
export class AlarmSharedModule {}
