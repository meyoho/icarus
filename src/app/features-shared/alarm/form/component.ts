import {
  DESCRIPTION,
  K8sApiService,
  NAME,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { upperFirst } from 'lodash-es';
import { EMPTY, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AlarmAction,
  ClusterFeature,
  DeploymentTypeMeta,
  PrometheusRule,
  PrometheusRuleItem,
  PrometheusRuleMeta,
  RESOURCE_TYPES,
  ResourceType,
  WorkspaceBaseParams,
} from 'app/typings';
import { LOOSE_K8S_RESOURCE_NAME_BASE, hasDuplicateBy } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

interface PrometheusRuleModel {
  name: string;
  description: string;
  rules: PrometheusRuleItem[];
  actions: AlarmAction[];
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmFormComponent implements OnInit, OnDestroy {
  @ViewChild('alarmForm', { static: false })
  form: NgForm;

  name: string;

  workloadKind: string;
  workloadName: string;
  workloadUid: string;
  baseParams: WorkspaceBaseParams;
  appName: string;
  prometheusName: string;
  resource: PrometheusRule;
  formSubmitText = 'create';
  submitting = false;
  resourceNameReg = LOOSE_K8S_RESOURCE_NAME_BASE;

  viewModel: PrometheusRuleModel = {
    name: '',
    description: '',
    rules: [],
    actions: [],
  };

  onDestroy$ = new Subject<void>();

  constructor(
    private readonly auiNotificationService: NotificationService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly location: Location,
    private readonly messageService: MessageService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {
    this.name = this.route.snapshot.params.alarmname;
    this.workloadKind = this.route.snapshot.queryParams.kind;
    this.workloadName = this.route.snapshot.queryParams.name;
    this.workloadUid = this.route.snapshot.queryParams.uid;
    this.appName = this.route.snapshot.queryParams.appName;
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  ngOnInit() {
    if (this.name) {
      this.k8sApi
        .getResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          name: this.name,
        })
        .pipe(
          catchError(() => {
            return EMPTY;
          }),
        )
        .subscribe(result => {
          if (result) {
            this.formSubmitText = 'update';
            this.resource = result;
            let actions: AlarmAction[];
            try {
              actions = JSON.parse(
                this.k8sUtil.getAnnotation(result, 'notifications', 'alert'),
              );
            } catch {
              actions = [];
            }
            this.workloadName = this.k8sUtil.getLabel(result, NAME, 'alert');
            this.workloadKind = this.k8sUtil
              .getLabel(result, 'kind', 'alert')
              .toLowerCase();
            this.viewModel = {
              name: result.metadata.name,
              description: this.k8sUtil.getDescription(result),
              rules: result.spec.groups[0].rules,
              actions,
            };
          }
          this.cdr.markForCheck();
        });
    }
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.baseParams.cluster,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .subscribe(features => {
        if (features.items.length > 0) {
          this.prometheusName = features.items[0].spec.accessInfo.name;
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.viewModel.rules.length === 0) {
      this.auiNotificationService.error(
        this.translate.get('alarm_rules_error'),
      );
      return;
    }
    const duplicate = hasDuplicateBy(this.viewModel.rules, 'expr');
    if (duplicate) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_duplicate'),
      );
      return;
    }
    this.viewModel.actions = this.viewModel.actions || [];
    this.viewModel.rules.forEach(rule => {
      rule.annotations.alert_notifications = JSON.stringify(
        this.viewModel.actions,
      );
    });
    this.submitting = true;
    if (this.resource) {
      const updateTime = new Date();
      this.k8sApi
        .patchResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.baseParams.cluster,
          resource: this.resource,
          part: {
            metadata: {
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'updated-at',
                )]: updateTime.toISOString(),
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
            },
            spec: {
              groups: [
                {
                  name: 'general',
                  rules: this.viewModel.rules,
                },
              ],
            },
          },
        })
        .subscribe(
          () => {
            this.cancel();
            this.messageService.success(
              this.translate.get('alarm_update_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    } else {
      this.k8sApi
        .postResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.baseParams.cluster,
          resource: {
            apiVersion: PrometheusRuleMeta.apiVersion,
            kind: PrometheusRuleMeta.kind,
            metadata: {
              name: this.viewModel.name,
              namespace: this.baseParams.namespace,
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
              labels: {
                [this.k8sUtil.normalizeType('application', 'alert')]: this
                  .appName,
                [this.k8sUtil.normalizeType('cluster', 'alert')]: this
                  .baseParams.cluster,
                [this.k8sUtil.normalizeType('kind', 'alert')]: upperFirst(
                  this.workloadKind,
                ),
                [this.k8sUtil.normalizeType(NAME, 'alert')]: this.workloadName,
                [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this
                  .baseParams.namespace,
                [this.k8sUtil.normalizeType('project', 'alert')]: this
                  .baseParams.project,
                [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
                prometheus: this.prometheusName,
              },
              ownerReferences: [
                {
                  apiVersion: DeploymentTypeMeta.apiVersion,
                  controller: true,
                  blockOwnerDeletion: true,
                  kind: this.workloadKind,
                  name: this.workloadName,
                  uid: this.workloadUid,
                },
              ],
            },
            spec: {
              groups: [
                {
                  name: 'general',
                  rules: this.viewModel.rules,
                },
              ],
            },
          },
        })
        .subscribe(
          () => {
            this.router.navigate(
              ['../../', 'alert', 'detail', this.viewModel.name],
              {
                relativeTo: this.route,
                queryParams: {
                  kind: this.workloadKind,
                  name: this.workloadName,
                  appName: this.appName,
                },
              },
            );
            this.messageService.success(
              this.translate.get('alarm_create_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  cancel() {
    this.location.back();
  }
}
