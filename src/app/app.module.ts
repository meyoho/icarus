import { CodeEditorIntl, CodeEditorModule } from '@alauda/code-editor';
import {
  CommonLayoutModule,
  DEFAULT_CODE_EDITOR_OPTIONS,
  DEFAULT_REMOTE_URL,
  TranslateModule,
  TranslateService,
} from '@alauda/common-snippet';
import {
  IconModule,
  MessageModule,
  NotificationModule,
  StatusBarModule,
} from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { MonacoProviderService } from 'ng-monaco-editor';

import { SharedModule } from 'app/shared/shared.module';
import { effects, metaReducers, reducers } from 'app/store';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { en } from './i18n/en.i18n';
import { zh } from './i18n/zh.i18n';
import { CustomCodeEditorIntl } from './services/custom-monaco-provider/custom-code-editor-intl.service';
import { ServicesModule } from './services/module';

const imports = [
  BrowserModule,
  BrowserAnimationsModule,
  HttpClientModule,
  SharedModule,
  CommonLayoutModule,
  EffectsModule.forRoot(effects),
  StoreModule.forRoot(reducers, { metaReducers }),
  NotificationModule,
  MessageModule,
  IconModule,
  StatusBarModule,
  CodeEditorModule.forRoot({
    baseUrl: 'monaco_lib/v1',
    defaultOptions: DEFAULT_CODE_EDITOR_OPTIONS,
  }),
  TranslateModule.forRoot({
    loose: true,
    translations: { zh, en },
    remoteUrl: DEFAULT_REMOTE_URL,
  }),
  ServicesModule,
  AppRoutingModule,
];

if (process.env.NODE_ENV === 'development') {
  imports.push(StoreDevtoolsModule.instrument({ maxAge: 25 }));
}

@NgModule({
  imports,
  bootstrap: [AppComponent],
  declarations: [AppComponent],
  providers: [{ provide: CodeEditorIntl, useClass: CustomCodeEditorIntl }],
})
export class AppModule {
  constructor(
    monacoProvider: MonacoProviderService,
    translate: TranslateService,
  ) {
    monacoProvider.initMonaco();
    translate.locale$.subscribe(lang => {
      document.documentElement.setAttribute('lang', lang);
    });
  }
}
