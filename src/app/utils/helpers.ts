import { KubernetesResource } from '@alauda/common-snippet';
import { isDevMode } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import dayjs from 'dayjs';
import { safeDump } from 'js-yaml';

import { QUERY_TIME_STAMP_OPTIONS } from 'app/features-shared/app/utils';
import { Domain, HostModel } from 'app/typings';

export const getResourceYaml = <T extends KubernetesResource>(resource: T) =>
  safeDump(resource, { sortKeys: true });

/**
 * 判断一个数组中的某个key的值是否重复
 * @param arr 一个对象数组
 * @param key 需要判断的key
 * @return 如重复，则返回重复的值，否则返回false
 */
export const hasDuplicateBy = <T>(arr: T[], key: keyof T) => {
  const values = new Set<T[keyof T]>();
  if (arr.some(item => values.size === values.add(item[key]).size)) {
    return values.values().next().value;
  }
  return false;
};

/**
 * Function to generate a promise to be resolved after the given timeout.
 */
export const delay = (ms?: number) =>
  new Promise<void>(resolve => setTimeout(resolve, ms));

export const rowBackgroundColorFn = ({ invalid }: AbstractControl) =>
  invalid ? '#fdefef' : '';

export const sortDomains = (domains: Domain[]) =>
  (domains || [])
    .slice()
    .sort(({ spec: { name: aName } }, { spec: { name: bName } }) => {
      if (aName.startsWith('*') && bName.startsWith('*')) {
        return aName.length - bName.length;
      }
      return aName > bName ? 1 : aName < bName ? -1 : 0;
    });

export const parseHost = (host = '', domains: Domain[]): HostModel => {
  if (host.startsWith('*') && isDevMode()) {
    console.error("It seems you're passing incorrect host!");
  }

  const domain = sortDomains(domains).find(
    ({ spec: { kind, name } }) =>
      (name === host && kind === 'full') || host.endsWith(name.slice(1)),
  );

  if (!domain) {
    return {
      prefix: '',
      name: '',
      value: host,
    };
  }

  const {
    metadata: { name },
    spec: { kind, name: value },
  } = domain;
  const prefix =
    kind === 'full' ? '' : host.slice(0, host.lastIndexOf(value.slice(1)));
  return {
    prefix: prefix === '*' ? '' : prefix,
    name,
    value,
  };
};

export const stringifyHostModel = (host: HostModel) =>
  host.prefix ? host.prefix + host.value.slice(1) : host.value;

export const getTimeRangeByRangeType = (type: string) => {
  let startTime;
  const endTime = new Date().getTime();
  const offset =
    QUERY_TIME_STAMP_OPTIONS.find(option => option.type === type).offset ||
    QUERY_TIME_STAMP_OPTIONS[0].offset;
  if (offset > 7 * 24 * 3600 * 1000) {
    const endDate = new Date(endTime);
    startTime =
      new Date(
        endDate.getFullYear(),
        endDate.getMonth(),
        endDate.getDate(),
      ).getTime() -
      offset +
      24 * 3600 * 1000;
  } else {
    startTime = endTime - offset;
  }
  return {
    start_time: startTime,
    end_time: endTime,
  };
};

export const dateNumToStr = (num: number) => {
  return dayjs(num).format('YYYY-MM-DD HH:mm:ss');
};

export const dateStrToNum = (str: string) => {
  return dayjs(str, 'YYYY-MM-DD HH:mm:ss').valueOf();
};
