export const K8S_RESOURCE_NAME_BASE = {
  pattern: /^[\da-z]([\da-z-]*[\da-z])?$/,
  tip: 'regexp_tip_k8s_resource_name_base',
};

export const LOOSE_K8S_RESOURCE_NAME_BASE = {
  pattern: /^[\da-z]([\da-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*$/,
  tip: 'regexp_tip_k8s_resource_name_loose',
};

export const K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN = {
  pattern: /^([\da-z]([\da-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*)?$/,
  tip: 'regexp_tip_resource_label_key_prefix',
};

export const K8S_RESOURCE_LABEL_KEY_NAME_PATTERN = {
  pattern: /^(([\dA-Za-z][\w.-]*)?[\dA-Za-z])$/,
  tip: 'regexp_tip_resource_label_key_name',
};

export const K8S_RESOURCE_LABEL_VALUE_PATTERN = {
  pattern: /^(([\dA-Za-z][\w.-]*)?[\dA-Za-z])?$/,
  tip: 'regexp_tip_resource_label_value',
};

export const K8S_CONFIGMAP_KEY = {
  pattern: /^[\w-][\w.-]*$/,
  tip: 'regexp_tip_k8s_configmap_key',
};

export const K8S_ENV_VARIABLE_NAME = {
  pattern: /^[.A-Z_a-z-][\w.-]*$/,
  tip: 'regexp_tip_k8s_env_variable_name',
};

export const K8S_SERVICE_NAME = {
  pattern: /^[a-z]([\da-z-]*[\da-z])?$/,
  tip: 'regexp_tip_k8s_service_name',
};

export const K8S_VOLUME_MOUNT_SUB_PATH = {
  pattern: /^([\w.][\w.-]*\/?)*$/,
  tip: 'regexp_tip_sub_path',
};

export const INT_PATTERN = {
  pattern: /^-?\d+$/,
  tip: 'integer_pattern',
};

export const POSITIVE_INT_PATTERN = {
  pattern: /^[1-9]\d*$/,
  tip: 'positive_integer_pattern',
};

export const NATURAL_NUMBER_PATTERN = {
  pattern: /^\d+$/,
  tip: 'natural_number_pattern',
};

export const REGISTRY_PATH = {
  pattern: /^[\da-z]([\d._a-z-]*[\da-z])?$/,
  tip: 'registry_path_rule',
};

export const INGRESS_PATH_PATTERN = {
  pattern: /^\/.*/,
  tip: 'ingress_path_pattern',
};

export const IP_ADDRESS_PATTERN = {
  pattern: /^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})$/,
  tip: 'regexp_tip_ip_address',
};

export const IP_HTTP_HOSTNAME_PATTERN = {
  pattern: /^(([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])$|^(https?:\/{2})?([\dA-Za-z]\.)*[\dA-Za-z]$/,
  tip: 'regexp_tip_ip_address_or_hostname',
};

export const IP_ADDRESS_PORT_PATTERN = {
  pattern: /^(?:(?:[1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:[1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])(?::\d+)?$/,
  tip: 'regexp_tip_ip_address',
};

export const IP_ADDRESS_HOSTNAME_PATTERN = {
  pattern: /^((\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])$|^(([A-Za-z]|[A-Za-z][\dA-Za-z-]*[\dA-Za-z])\.)*([A-Za-z]|[A-Za-z][\dA-Za-z-]*[\dA-Za-z])$/,
  tip: 'regexp_tip_ip_address_or_hostname',
};

export const DOMAIN_PATTERN = {
  pattern: /^(?:[\da-z](?:[\da-z-]{0,61}[\da-z])?\.)+[\da-z][\da-z-]{0,61}[\da-z]$/,
  tip: 'regexp_tip_domain',
};

export const CIDR_IP_PATTERN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3}$/,
};

export const CIDR_FULL_PATTEN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3}(\/(\d|[12]\d|3[0-2]))$/,
  tip: 'regexp_cidr_pattern',
};

export const CIDR_PATTERN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3}(\/(\d|[12]\d|3[0-2]))?$/,
  tip: 'regexp_cidr_pattern',
};

export const RESERVED_IP_PATTERN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3}(\.\.((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3})?$/,
};

export const RESERVED_IP_SEGMENT_PATTERN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3}(\.\.((2(5[0-5]|[0-4]\d))|1\d{2}|[1-9]\d|\d)(\.(((2(5[0-5]|[0-4]\d))|1\d{1,2}|[1-9]\d|\d))){3})$/,
};

export const ALARM_LABEL_KEY_NAME_PATTERN = {
  pattern: /^[A-Z_a-z]\w*$/,
  tip: 'regexp_tip_alert_label_key_name',
};

export const ALARM_THRESHOLD_PATTERN = {
  pattern: /^(?!0\d)\d{0,17}(\.\d{1,4})?$/,
  tip: 'regexp_tip_alert_threshold',
};

export const ALARM_METRIC_PATTERN = {
  pattern: /^[\u0020-\u007E]*$/,
  tip: 'regexp_tip_alert_metric',
};

export const EMAIL_PATTERN = {
  pattern: /^[\w!#$%&'*+/=?^`{|}~-]+(?:\.[\w!#$%&'*+/=?^`{|}~-]+)*@(?:[\dA-Za-z](?:[\dA-Za-z-]*[\dA-Za-z])?\.)+[\dA-Za-z](?:[\dA-Za-z-]*[\dA-Za-z])?$/,
  tip: 'email_format_limit',
};

export const PHONE_PATTERN = {
  pattern: /^\+{0,1}\d{7,15}$/,
  tip: 'phone_format_limit',
};

export const HTTP_ADDRESS_PATTERN = {
  pattern: /^(http|https):\/\/([\w.]+\/?)\S*$/,
  tip: 'http_address_format_limit',
};

export const HOST_OR_IP_PATTERN = {
  pattern: /^[^/:]+$/,
  tip: 'host_or_ip_pattern_hint',
};

export const IMAGE_REPO_PATTERN = {
  pattern: /^([\da-z][\d./:_a-z-]*)?[\da-z](:[\w.-]+)?$/,
  tip: 'image_repo_pattern_hint',
};

export const CERT_FILE_PATTERN = {
  pattern: /^-{5}BEGIN CERTIFICATE-{5}[\d\n+/=A-Za-z]*-{5}END CERTIFICATE-{5}$/,
  tip: 'cert_file_pattern_hint',
};
