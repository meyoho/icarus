import { numToStr, shortNum } from './commons';

test('numToStr', () => {
  expect(numToStr()).toEqual('');
  expect(numToStr(0)).toEqual('0');
  expect(numToStr(1)).toEqual('1');
  expect(numToStr(1.1)).toEqual('1.1');
  expect(numToStr(1.01)).toEqual('1.01');
  expect(numToStr(0.01953125)).toEqual('0.01953125');
  expect(numToStr(0.00002)).toEqual('0.00002');
  expect(numToStr(10)).toEqual('10');
  expect(numToStr(1000)).toEqual('1000');
  expect(numToStr(123456.7891011)).toEqual('123456.7891011');
  expect(numToStr(1000.00001)).toEqual('1000.00001');
  expect(numToStr(1000.0000100020003)).toEqual('1000.0000100020003');
  expect(numToStr(1e-10)).toEqual('0.0000000001');
});

test('shortNum', () => {
  expect(shortNum()).toEqual('');
  expect(shortNum(0)).toEqual('0');
  expect(shortNum(1)).toEqual('1');
  expect(shortNum(1.1)).toEqual('1.1');
  expect(shortNum(1.01)).toEqual('1.01');
  expect(shortNum(0.01953125)).toEqual('0.019');
  expect(shortNum(0.00002)).toEqual('0.00002');
  expect(shortNum(10)).toEqual('10');
  expect(shortNum(1000)).toEqual('1000');
  expect(shortNum(123456.7891011)).toEqual('123456.78');
  expect(shortNum(1000.00002)).toEqual('1000.00002');
  expect(shortNum(1000.0000100020003)).toEqual('1000.00001');
  expect(shortNum(1e-10)).toEqual('0.0000000001');
});
