import { AbstractControl, ValidationErrors } from '@angular/forms';
import parser from 'cron-parser';

import {
  CIDR_IP_PATTERN,
  K8S_ENV_VARIABLE_NAME,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
} from './patterns';
const CRON_FORMAT_ERROR = 'cronFormatError';
const CRON_MIN_INTERVAL_ERROR = 'cronMinIntervalError';

export function k8sResourceLabelKeyValidator(
  control: AbstractControl,
): ValidationErrors | null {
  if (!control.value) {
    return null;
  }

  const keyPartition = control.value.split('/');
  const [prefix, ...nameParts] = keyPartition;
  let checkPrefix = true;
  let checkName = true;
  if (keyPartition.length > 1) {
    checkPrefix = K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.pattern.test(prefix);
    checkName = K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(
      nameParts.join('/'),
    );
  } else {
    checkName = K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(
      keyPartition[0],
    );
  }
  if (!checkPrefix) {
    return { prefixPattern: true };
  }
  if (!checkName) {
    return { namePattern: true };
  }

  return null;
}

export function isValidIpPattern(ipStr: string) {
  if (!CIDR_IP_PATTERN.pattern.test(ipStr)) {
    return false;
  }
  return !ipStr.split('.').find(ipPart => {
    return +ipPart > 255;
  });
}

export function envVarNameValidator(control: AbstractControl) {
  return !control.value ||
    (K8S_ENV_VARIABLE_NAME.pattern.test(control.value) &&
      !/^\.+$/.test(control.value))
    ? null
    : {
        envVarNamePattern: true,
      };
}

// CRON_FORMAT_ERROR: cronFormatError 标识格式错误
// CRON_MIN_INTERVAL_ERROR: cronMinIntervalError 标识最小间隔错误
export function cronValidator(minInterval?: number) {
  return (control: AbstractControl): ValidationErrors | null => {
    const { value } = control;
    if (!value) {
      return null;
    }
    if (!isFiledCountCorrect(value)) {
      return {
        [CRON_FORMAT_ERROR]: true,
      };
    }
    try {
      const iterator = parser.parseExpression(value);

      let minIntervalValidity = true;

      if (minInterval) {
        const next1 = iterator.next() as any;
        const next2 = iterator.next() as any;

        // minute
        if ((next2._date - next1._date) / 1000 / 60 < minInterval) {
          minIntervalValidity = false;
        }
      }

      return minIntervalValidity
        ? null
        : {
            [CRON_MIN_INTERVAL_ERROR]: true,
          };
    } catch (e) {
      return {
        [CRON_FORMAT_ERROR]: true,
      };
    }
  };
}

function isFiledCountCorrect(exp: string) {
  const spaceCount = exp.split('').filter(val => val === ' ').length;
  const fieldCount = exp.split(' ').filter(val => val).length;
  return spaceCount === 4 && fieldCount === 5;
}
