export const isBlank = (str?: unknown) =>
  str == null ||
  Number.isNaN(str as number) ||
  !(str as object).toString().trim();

export const numToStr = (num?: number) => {
  if (num === 0) {
    return '0';
  }

  if (!num) {
    return '';
  }

  return num
    .toLocaleString('fullwide', {
      minimumFractionDigits: 20,
      useGrouping: false,
    })
    .replace(/([^.])\.?0+$/, '$1');
};

export const shortNum = (num?: number | string) =>
  (typeof num === 'string' ? num : numToStr(num)).replace(
    /(\.0*[1-9]{1,2})\d*/,
    '$1',
  );
