export const RESOURCE_ROUTE_MAPPER = {
  Application: 'app',
};

export const STANDALONE_NG_MODEL = {
  standalone: true,
};

export const ASSIGN_ALL = 'ALL_ALL';
export const KUBE_PUBLIC_NAMESPACE = 'kube-public';
export const DEFAULT = 'default';
export const TRUE = 'true';
export const FALSE = 'false';
export const TYPE = 'type';
export const ACTION = 'action';
export const STATUS = 'status';
