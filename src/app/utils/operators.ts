import { EMPTY, Observable, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

export const skipError = <T>(mapped: Observable<T> = EMPTY) =>
  catchError(() => mapped);

export const catchPromise = <T, R>(
  promise: Promise<T>,
  mapped?: Observable<R>,
) =>
  of(null).pipe(
    switchMap(() => promise),
    skipError(mapped),
  );
