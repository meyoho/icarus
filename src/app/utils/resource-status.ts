import { get } from 'lodash-es';

import {
  AlarmStatusEnum,
  Application,
  ApplicationPhaseEnum,
  ApplicationStateEnum,
  ApplicationStatusEnum,
  ContainerStatus,
  GenericWorkloadStatus,
  HelmRequest,
  HelmRequestPhaseEnum,
  HelmRequestStatusEnum,
  Job,
  JobStatusEnum,
  PersistentVolume,
  PersistentVolumeClaim,
  Pod,
  PodStatusEnum,
  PrometheusRuleItem,
  PvStatusEnum,
  PvcStatusEnum,
  WorkloadStatusEnum,
} from 'app/typings';

export function getAlarmStatus(rule: PrometheusRuleItem): AlarmStatusEnum {
  if (rule.labels.alarm_status === 'pending') {
    return AlarmStatusEnum.pending;
  } else if (rule.labels.alarm_status === 'firing') {
    return AlarmStatusEnum.firing;
  }
  return AlarmStatusEnum.normal;
}
// reference: https://bitbucket.org/mathildetech/link/src/c81161f02f981904b716462a305e09414e5617d3/src/backend/resource/common/pod.go#lines-179
export function getPodStatus(pod: Pod): string {
  let reason = pod.status.phase || pod.status.reason;
  const { initializing, reason: initReason } = getPodInitStatus(pod, reason);
  if (!initializing) {
    reason = getPodContainerStatus(pod, reason);
  } else {
    reason = initReason;
  }

  if (pod.metadata.deletionTimestamp && pod.status.reason === 'NodeLost') {
    reason = 'Unknown';
  } else if (pod.metadata.deletionTimestamp) {
    reason = 'Terminating';
  }
  return reason;
}

function getPodContainerStatus(pod: Pod, reason: string): string {
  const containerStatuses = get(pod, ['status', 'containerStatuses'], []);
  for (let i = 0, len = containerStatuses.length; i < len; i++) {
    const container = containerStatuses[i];
    let waitingReason, terminatedReason;
    if ((waitingReason = get(container, ['state', 'waiting', 'reason']))) {
      return waitingReason;
    } else if (
      (terminatedReason = get(container, ['state', 'terminated', 'reason']))
    ) {
      return terminatedReason;
    } else if (get(container, ['state', 'terminated'])) {
      const { signal, exitCode } = container.state.terminated;
      if (signal !== 0) {
        return `Signal:${signal}`;
      } else {
        return `ExitCode:${exitCode}`;
      }
    }
  }
  return reason;
}

function getPodInitStatus(pod: Pod, reason: string) {
  let initializing = false;
  const initContainerStatuses = get(
    pod,
    ['status', 'initContainerStatuses'],
    [] as ContainerStatus[],
  );
  for (let i = initContainerStatuses.length - 1; i >= 0; i--) {
    const status = initContainerStatuses[i];
    if (get(status, ['state', 'terminated', 'exitCode']) === 0) {
      continue;
    }
    let waitingReason;
    const terminated = get(status, ['state', 'terminated']);
    if (terminated) {
      reason = terminated.reason
        ? `Init:${terminated.reason}`
        : terminated.signal !== 0
        ? `Init:Signal:${terminated.signal}`
        : `Init:ExitCode:${terminated.exitCode}`;
    } else if (
      (waitingReason = get(status, ['state', 'waiting', reason])) &&
      waitingReason !== 'PodInitializing'
    ) {
      reason = `Initing:${waitingReason}`;
    } else {
      reason = `Initing:${i}/${pod.spec.initContainers.length}`;
    }
    initializing = true;
    break;
  }
  return { initializing, reason };
}

// 将 getPodStatus 得到的多种状态映射到标签类型
// reference: https://bitbucket.org/mathildetech/link/src/c81161f02f981904b716462a305e09414e5617d3/src/backend/resource/common/podinfo.go#lines-68
export function getPodAggregatedStatus(status: string): PodStatusEnum {
  if (
    status.split(':')[0] === 'Initing' ||
    [
      'Initing',
      'Pending',
      'PodInitializing',
      'ContainerCreating',
      'Terminating',
    ].includes(status)
  ) {
    return PodStatusEnum.pending;
  }

  if (status === 'Completed') {
    return PodStatusEnum.completed;
  }
  if (status === 'Running') {
    return PodStatusEnum.running;
  }

  return PodStatusEnum.error;
}

export function getJobStatus(data: Job): JobStatusEnum {
  const status = data.status;
  if (
    status.conditions &&
    status.conditions.find(_item => _item.type === 'Failed')
  ) {
    return JobStatusEnum.EXECUTE_FAILED;
  }
  // when Complete and succeeded, judge to execute_succeeded
  if (
    status.conditions &&
    status.conditions.find(_item => _item.type === 'Complete')
  ) {
    if (status.succeeded) {
      return JobStatusEnum.EXECUTE_SUCCEEDED;
    }
    if (status.failed) {
      return JobStatusEnum.EXECUTE_FAILED;
    }
  }
  // no condition and no Complete when running
  if (status.active) {
    return JobStatusEnum.EXECUTING;
  }
  return JobStatusEnum.UNKNOWN;
}

export function getWorkloadStatus(status: GenericWorkloadStatus) {
  return status.status.toLowerCase() as WorkloadStatusEnum;
}

export function getApplicationStatus(application: Application) {
  const status = application.status;
  const phase = application.spec.assemblyPhase;

  if (phase === ApplicationPhaseEnum.Succeeded) {
    switch (status.state) {
      case ApplicationStateEnum.Stopped:
        return ApplicationStatusEnum.STOPPED;
      case ApplicationStateEnum.Pending:
        return ApplicationStatusEnum.PENDING;
      case ApplicationStateEnum.Running:
        return ApplicationStatusEnum.RUNNING;
      case ApplicationStateEnum.PartialRunning:
        return ApplicationStatusEnum.PARTIAL_RUNNING;
      case ApplicationStateEnum.Empty:
        return ApplicationStatusEnum.NO_WORKLOAD;
      default:
        return ApplicationStatusEnum.UNKNOWN;
    }
  }

  switch (phase) {
    case ApplicationPhaseEnum.Pending:
      return ApplicationStatusEnum.PENDING;
    case ApplicationPhaseEnum.Failed:
      return ApplicationStatusEnum.FAILED;
  }
}

export function getPvcStatus(pvc: PersistentVolumeClaim): PvcStatusEnum {
  switch (pvc.status.phase.toLowerCase()) {
    case 'pending':
      return PvcStatusEnum.matching;
    case 'bound':
      return PvcStatusEnum.bound;
    case 'lost':
      return PvcStatusEnum.invalid;
  }
}

export function getPvStatus(pv: PersistentVolume): PvStatusEnum {
  switch (pv.status.phase.toLowerCase()) {
    case 'available':
      return PvStatusEnum.AVAILABLE;
    case 'bound':
      return PvStatusEnum.BOUND;
    case 'released':
      return PvStatusEnum.WAITING_RECYCLE;
    case 'failed':
      return PvStatusEnum.RECYCLE_FAILED;
  }
}

export function getHelmRequestStatus(hr: HelmRequest): HelmRequestStatusEnum {
  if (hr.status) {
    switch (hr.status.phase) {
      case HelmRequestPhaseEnum.Synced:
        return HelmRequestStatusEnum.DEPLOY_SUCCEEDED;
      case HelmRequestPhaseEnum.Failed:
        return HelmRequestStatusEnum.DEPLOY_FAILED;
      case HelmRequestPhaseEnum.Pending:
        return HelmRequestStatusEnum.DEPLOYING;
    }
  }

  return HelmRequestStatusEnum.DEPLOYING;
}
