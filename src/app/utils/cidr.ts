/**
 * @param {string} ip specific ip, e.g. 10.16.1.1
 * @return {string} ip formatted in 32bits binary string,e.g. 00001010000100000000000100000001
 */
export function getIPBinaryString(ip: string) {
  const numArray = ip.split('.');
  let returnIpStr = '';
  for (let i = 0; i < 4; i++) {
    const currNum = numArray[i];
    let numberBin = parseInt(currNum, 10).toString(2);
    numberBin = numberBin.padStart(8, '0');
    returnIpStr += numberBin;
  }
  return returnIpStr;
}

/**
 * @param {string} cidr cidrBlock, e.g. 10.16.0.0/24
 * @param {string} ip specific ip, e.g. 10.16.1.2
 * @return {boolean} whether ip belongs to cidr
 */
export function isBelongCidr(cidr: string, ip: string) {
  if (!cidr) {
    return false;
  }
  const [cidrIp, digit] = cidr.split('/');
  const cidrPrefix = getIPBinaryString(cidrIp).slice(0, Math.max(0, +digit));
  const ipInDigits = getIPBinaryString(ip);
  return ipInDigits.startsWith(cidrPrefix);
}

/**
 * @param {string} ip specific ip, e.g. 10.16.1.2
 * @param {string} cidr specific cidr which ip may belongs to, e.g. 10.16.0.0/24
 * @return {boolean} whether ip is broadcast address
 */
export function isBroadcastAddr(ip: string, cidr?: string) {
  if (!cidr) {
    return ip === '255.255.255.255';
  }
  const digit = cidr.split('/')[1];
  const cidrIpBinStr = getIPBinaryString(ip);
  const hostDigits = 32 - +digit;
  // 广播地址为cidrIp的最后 32-digit 位为1
  return cidrIpBinStr.endsWith(''.padStart(hostDigits, '1'));
}

/**
 * @param {string} cidrA cidrBlock A, e.g. 10.16.0.0/24
 * @param {string} cidrB cidrBlock B, e.g. 10.10.0.0/16
 * @return {boolean} whether 2 cidr blocks may conflict, judge by whether ip belongs to cidrBlock A may also belongs to B
 */
export function isCidrConflict(cidrA: string, cidrB: string) {
  const [ipA, digitA] = cidrA.split('/');
  const [ipB, digitB] = cidrB.split('/');
  const commonDigits = Math.min(+digitA, +digitB);
  const cidrAIpBinStr = getIPBinaryString(ipA);
  const cidrBIpBinStr = getIPBinaryString(ipB);
  return cidrAIpBinStr.startsWith(cidrBIpBinStr.slice(0, commonDigits));
}
