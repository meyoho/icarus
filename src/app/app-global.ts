import { baseDomain, globalNamespace } from '@alauda/common-snippet';

export const globalEnvironments = {
  LABEL_BASE_DOMAIN: baseDomain,
  GLOBAL_NAMESPACE: globalNamespace,
  DEBUG: false,
};
