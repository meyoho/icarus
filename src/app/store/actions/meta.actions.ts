// metadata reducer related Action

import { Action } from '@ngrx/store';

export const BATCH = `[global] BATCH`;

export class BatchAction implements Action {
  readonly type = BATCH;

  constructor(public payload: Action[]) {}
}

export type MetaActions = BatchAction;
