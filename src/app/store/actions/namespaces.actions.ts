// tslint:disable: max-classes-per-file
import { Action } from '@ngrx/store';

import { ErrorResponse } from 'app/services/http.service';
import { Namespace } from 'app/typings';

export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT =
  '[Namespace] get by cluster and project';
export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC =
  '[Namespace] get by cluster and project success';
export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL =
  '[Namespace] get by cluster and project failed';
export const UPDATE_NAMESPACE_BY_CLUSTER_AND_PROJECT =
  '[Namespace] update by cluster and project';
export class GetNamespacesByClusterAndProject implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT;
  constructor(public payload: { project: string; cluster: string }) {}
}

export class GetNamespacesByClusterAndProjectSucc implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC;
  constructor(
    public payload: {
      project: string;
      cluster: string;
      namespaces: Namespace[];
    },
  ) {}
}

export class GetNamespacesByClusterAndProjectFail implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL;
  constructor(public payload: ErrorResponse) {}
}

export class UpdateNamespaceByClusterAndProject implements Action {
  readonly type = UPDATE_NAMESPACE_BY_CLUSTER_AND_PROJECT;
  constructor(
    public payload: { project: string; cluster: string; namespace: Namespace },
  ) {}
}

export type NamespaceAction =
  | GetNamespacesByClusterAndProject
  | GetNamespacesByClusterAndProjectSucc
  | GetNamespacesByClusterAndProjectFail
  | UpdateNamespaceByClusterAndProject;
