// tslint:disable: max-classes-per-file
import { Action } from '@ngrx/store';

import { Project } from 'app/typings';

export const LOAD_PROJECTS = '[Projects] Load Projects';
export const LOAD_PROJECTS_SUCCESS = '[Projects] Load Projects Success';
export const LOAD_PROJECTS_FAIL = '[Projects] Load Projects Fail';

export const LOAD_PROJECT_DETAIL = '[Projects] Load Project Detail';
export const LOAD_PROJECT_DETAIL_SUCCESS =
  '[Projects] Load Project Detail Success';
export const LOAD_PROJECT_DETAIL_FAIL = '[Projects] Load Project Detail Fail';
export const SET_CURRENT_PROJECT_NAME = '[Projects] Set Current Project Name';

export class LoadProjects implements Action {
  readonly type = LOAD_PROJECTS;
}

export class LoadProjectsSuccess implements Action {
  readonly type = LOAD_PROJECTS_SUCCESS;
  constructor(public payload: Project[]) {}
}

export class LoadProjectsFail implements Action {
  readonly type = LOAD_PROJECTS_FAIL;
  constructor(public payload: any) {}
}

export type ProjectAction =
  | LoadProjects
  | LoadProjectsSuccess
  | LoadProjectsFail;
