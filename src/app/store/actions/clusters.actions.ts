// tslint:disable: max-classes-per-file
import { Action } from '@ngrx/store';

import { ErrorResponse } from 'app/services/http.service';
import { Cluster } from 'app/typings';

export const GET_CLUSTERS = '[Clusters] Load Clusters';
export const GET_CLUSTERS_SUCCESS = '[Clusters] Load Clusters Success';
export const GET_CLUSTERS_FAIL = '[Clusters] Load Clusters Fail';

export const GET_CLUSTER_BY_NAME = '[Cluster] get by name';
export const GET_CLUSTER_BY_NAME_SUCCESS = '[Cluster] get by name success';
export const GET_CLUSTER_BY_NAME_FAIL = '[Cluster] get by name failed';
export const CREATE_CLUSTER = '[Cluster] create';
export const CREATE_CLUSTER_SUCCESS = '[Cluster] create success';
export const CREATE_CLUSTER_FAIL = '[Cluster] create failed';
export const DELETE_CLUSTER = '[Cluster] delete by name';
export const DELETE_CLUSTER_SUCCESS = '[Cluster] delete by name success';
export const DELETE_CLUSTER_FAIL = '[Cluster] delete by name failed';

export class GetClusters implements Action {
  readonly type = GET_CLUSTERS;
}

export class GetClustersSuccess implements Action {
  readonly type = GET_CLUSTERS_SUCCESS;
  constructor(public payload: Cluster[]) {}
}

export class GetClustersFail implements Action {
  readonly type = GET_CLUSTERS_FAIL;
  constructor(public payload: any) {}
}

export class GetClusterByName implements Action {
  readonly type = GET_CLUSTER_BY_NAME;
  constructor(public payload: string) {}
}
export class GetClusterByNameSuccess implements Action {
  readonly type = GET_CLUSTER_BY_NAME_SUCCESS;
  constructor(public payload: Cluster) {}
}

export class GetClusterByNameFail implements Action {
  readonly type = GET_CLUSTER_BY_NAME_FAIL;
  constructor(public payload: string) {}
}

export class CreateClusterSuccess implements Action {
  readonly type = CREATE_CLUSTER_SUCCESS;
  constructor(public payload: Cluster) {}
}

export class CreateClusterFail implements Action {
  readonly type = CREATE_CLUSTER_FAIL;
  payload: string[];
  constructor(public rawError: ErrorResponse) {
    this.payload = rawError.errors
      ? rawError.errors.map(err => err.message)
      : [];
  }
}

export class CreateCluster implements Action {
  readonly type = CREATE_CLUSTER;
  constructor(public payload: Cluster) {}
}

export class DeleteClusterSuccess implements Action {
  readonly type = DELETE_CLUSTER_SUCCESS;

  constructor(public payload: Cluster) {}
}

export class DeleteClusterFail implements Action {
  readonly type = DELETE_CLUSTER_FAIL;

  constructor(
    public payload: {
      error: any;
    },
  ) {}
}

export class DeleteCluster implements Action {
  readonly type = DELETE_CLUSTER;

  constructor(public payload: Cluster) {}
}

export type ClusterAction =
  | GetClusters
  | GetClustersSuccess
  | GetClustersFail
  | GetClusterByName
  | GetClusterByNameSuccess
  | GetClusterByNameFail
  | CreateCluster
  | CreateClusterSuccess
  | CreateClusterFail
  | DeleteCluster
  | DeleteClusterSuccess
  | DeleteClusterFail;
