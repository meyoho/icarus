import { publishRef } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { AppState } from 'app/store';
import { ProjectAction } from 'app/store/actions/projects.actions';
import * as fromProject from 'app/store/selectors/projects.selectors';
import { Project } from 'app/typings';

@Injectable({
  providedIn: 'root',
})
export class ProjectsFacadeService {
  projects$: Observable<Project[]>;

  constructor(private readonly store: Store<AppState>) {
    this.projects$ = this.store.select(fromProject.getAllProjects);
  }

  selectProjectByName$(name: string) {
    return this.projects$.pipe(
      map(projects => projects.find(project => project.metadata.name === name)),
      filter(p => !!p),
      publishRef(),
    );
  }

  dispatch(action: ProjectAction) {
    this.store.dispatch(action);
  }
}
