import { publishRef } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

import { AppState } from 'app/store';
import { ClusterAction } from 'app/store/actions/clusters.actions';
import * as fromCluster from 'app/store/selectors/clusters.selectors';

@Injectable({
  providedIn: 'root',
})
export class ClustersFacadeService {
  constructor(private readonly store: Store<AppState>) {}

  getClusters$() {
    return this.store.pipe(
      select(fromCluster.getAllClusters),
      filter(r => !!r),
      publishRef(),
    );
  }

  getClusterByName$(name: string) {
    return this.getClusters$().pipe(
      map(clusters => {
        return clusters.find(c => c.metadata.name === name);
      }),
    );
  }

  dispatch(action: ClusterAction) {
    this.store.dispatch(action);
  }
}
