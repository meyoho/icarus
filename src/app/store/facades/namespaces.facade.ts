import { publishRef } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { flatten } from 'lodash-es';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { AppState } from 'app/store';
import { NamespaceAction } from 'app/store/actions/namespaces.actions';

import * as fromFeature from '../reducers';
import {
  ClusterNamespaceMappings,
  NamespaceState,
} from '../reducers/namespaces.reducers';
import {
  NamespaceEntity,
  getNamespaces,
} from '../selectors/namespaces.selectors';

@Injectable({
  providedIn: 'root',
})
export class NamespacesFacadeService {
  constructor(private readonly store: Store<AppState>) {}

  getNamespaceState$() {
    return this.store.pipe(
      select(fromFeature.getNamespacesState),
      publishRef(),
    );
  }

  getNamespaces$() {
    return this.store.pipe(
      select(getNamespaces),
      filter(r => !!r),
      publishRef(),
    );
  }

  getNamespacesByProject$(project: string): Observable<NamespaceEntity[]> {
    return this.getNamespaceState$().pipe(
      map((state: NamespaceState) => {
        const mapping: ClusterNamespaceMappings = state.mappings[project] || {};
        return flatten(
          Object.keys(mapping).reduce((acc, cluster) => {
            const entities = mapping[cluster].entities;
            acc.push(
              Object.values(entities).map(namespace => ({
                namespace,
                cluster,
                project,
              })),
            );
            return acc;
          }, []),
        );
      }),
      filter(r => !!r),
      publishRef(),
    );
  }

  getNamespacesByProjectAndCluster$(
    project: string,
    cluster: string,
  ): Observable<NamespaceEntity[]> {
    return this.getNamespacesByProject$(project).pipe(
      map(entities => {
        return entities.filter(entity => entity.cluster === cluster);
      }),
    );
  }

  dispatch(action: NamespaceAction) {
    this.store.dispatch(action);
  }
}
