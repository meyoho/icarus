import { Project } from 'app/typings';

import * as fromProject from '../actions/projects.actions';

export interface ProjectState {
  entities: { [name: string]: Project };
  entitiesDetail: { [name: string]: Project };
  loaded: boolean;
  loading: boolean;
  currentProjectName: string;
}

export const initialState: ProjectState = {
  entities: {},
  entitiesDetail: {},
  loaded: false,
  loading: false,
  currentProjectName: '',
};

export function reducer(
  state = initialState,
  action: fromProject.ProjectAction,
) {
  switch (action.type) {
    case fromProject.LOAD_PROJECTS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromProject.LOAD_PROJECTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        entities: action.payload.reduce<Record<string, Project>>(
          (acc, curr) => {
            return {
              ...acc,
              [curr.metadata.name]: curr,
            };
          },
          {},
        ),
      };
    }
    case fromProject.LOAD_PROJECTS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }
  }

  return state;
}

export const getProjectEntities = (state: ProjectState) => state.entities;
export const getProjectEntitiesDetail = (state: ProjectState) =>
  state.entitiesDetail;
export const getProjectLoading = (state: ProjectState) => state.loading;
export const getProjectLoaded = (state: ProjectState) => state.loaded;
