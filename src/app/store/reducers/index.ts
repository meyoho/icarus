import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer,
  createFeatureSelector,
} from '@ngrx/store';

import * as metaActions from '../actions/meta.actions';

import * as fromCluster from './clusters.reducers';
import * as fromNamespace from './namespaces.reducers';
import * as fromProject from './projects.reducers';

export interface AppState {
  projects?: fromProject.ProjectState;
  clusters?: fromCluster.ClusterState;
  namespaces?: fromNamespace.NamespaceState;
}

export const reducers = {
  projects: fromProject.reducer,
  clusters: fromCluster.reducer,
  namespaces: fromNamespace.reducer,
} as ActionReducerMap<AppState>;

export const getProjectsState = createFeatureSelector<
  AppState,
  fromProject.ProjectState
>('projects');

export const getClustersState = createFeatureSelector<
  AppState,
  fromCluster.ClusterState
>('clusters');

export const getNamespacesState = createFeatureSelector<
  AppState,
  fromNamespace.NamespaceState
>('namespaces');

export function batch(
  reducer: ActionReducer<AppState>,
): ActionReducer<AppState> {
  return (state: AppState, action: any): AppState => {
    if ((action as metaActions.BatchAction).type === metaActions.BATCH) {
      return action.payload.reduce(reducer, state);
    }
    return reducer(state, action);
  };
}

export const metaReducers: Array<MetaReducer<AppState>> = [batch];
