import { Cluster } from 'app/typings';

import * as fromCluster from '../actions/clusters.actions';

export interface ClusterState {
  entities: { [name: string]: Cluster };
  loaded: boolean;
  loading: boolean;
}

export const initialState: ClusterState = {
  entities: {},
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromCluster.ClusterAction,
) {
  switch (action.type) {
    case fromCluster.GET_CLUSTERS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromCluster.GET_CLUSTERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        entities: action.payload.reduce<Record<string, Cluster>>(
          (acc, curr) => ({
            ...acc,
            [curr.metadata.name]: curr,
          }),
          {},
        ),
      };
    }
    case fromCluster.GET_CLUSTERS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }
    case fromCluster.GET_CLUSTER_BY_NAME_SUCCESS:
    case fromCluster.CREATE_CLUSTER_SUCCESS: {
      const cluster = action.payload;
      const newEntities = {
        ...state.entities,
        [cluster.metadata.name]: cluster,
      };
      return {
        ...state,
        entities: newEntities,
      };
    }
    case fromCluster.DELETE_CLUSTER_SUCCESS: {
      const clusterDeleted = action.payload;
      const {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        [clusterDeleted.details.name]: removed,
        ...entities
      } = state.entities;
      return {
        ...state,
        entities,
      };
    }
  }

  return state;
}

export const getClusterEntities = (state: ClusterState) => state.entities;
export const getClusterLoading = (state: ClusterState) => state.loading;
export const getClusterLoaded = (state: ClusterState) => state.loaded;
