import { Namespace } from 'app/typings';

import * as namespaceActions from '../actions/namespaces.actions';

export interface NamespaceEntities {
  [name: string]: Namespace;
}

export interface ClusterNamespaceMappings {
  [cluster: string]: {
    entities: NamespaceEntities;
  };
}

export interface ProjectClusterMappings {
  [project: string]: ClusterNamespaceMappings;
}

export interface NamespaceState {
  mappings: ProjectClusterMappings;
  loading: boolean;
}

const initialState: NamespaceState = {
  mappings: {},
  loading: false,
};

function getNamespacesEntities(namespaces: Namespace[]): NamespaceEntities {
  return namespaces.reduce(
    (entities: { [name: string]: Namespace }, namespace: Namespace) => {
      return {
        ...entities,
        [namespace.metadata.name]: namespace,
      };
    },
    {},
  );
}

export function reducer(
  state: NamespaceState = initialState,
  action: namespaceActions.NamespaceAction,
) {
  switch (action.type) {
    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT: {
      return {
        ...state,
        loading: true,
      };
    }

    /**
     * payload: {
     *  project: string;
     *  cluster: string;
     *  namespaces: Namespace[];
     * }
     */
    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC: {
      const { project, cluster, namespaces } = action.payload;
      const mappings = {
        ...state.mappings,
        [project]: {
          ...state.mappings[project],
          [cluster]: {
            entities: getNamespacesEntities(namespaces),
          },
        },
      };
      return {
        ...state,
        mappings,
        loading: false,
      };
    }

    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL: {
      return {
        ...state,
        loading: false,
      };
    }
    case namespaceActions.UPDATE_NAMESPACE_BY_CLUSTER_AND_PROJECT: {
      const { project, cluster, namespace } = action.payload;
      const mappings = {
        ...state.mappings,
        [project]: {
          ...state.mappings[project],
          [cluster]: Object.assign(
            {},
            {
              entities: {
                ...state.mappings[project][cluster].entities,
                [namespace.metadata.name]: namespace,
              },
            },
          ),
        },
      };
      return {
        ...state,
        mappings,
        loading: false,
      };
    }
  }
  return state;
}

export const getProjectClusterMappings = (state: NamespaceState) =>
  state.mappings;
export const getNamespaceLoading = (state: NamespaceState) => state.loading;
