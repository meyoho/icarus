import { ClusterEffects } from './clusters.effects';
import { NamespacesEffects } from './namespaces.effects';
import { ProjectEffects } from './projects.effects';

export const effects: any[] = [
  ProjectEffects,
  ClusterEffects,
  NamespacesEffects,
];

export * from './projects.effects';
export * from './clusters.effects';
export * from './namespaces.effects';
