import { K8sApiService, KubernetesResourceList } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, pluck, switchMap } from 'rxjs/operators';

import { Cluster, RESOURCE_TYPES, ResourceType } from 'app/typings';

import * as clusterActions from '../actions/clusters.actions';

@Injectable()
export class ClusterEffects {
  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly actions$: Actions,
  ) {}

  @Effect()
  getClusters$ = this.actions$.pipe(
    ofType(clusterActions.GET_CLUSTERS),
    switchMap(() =>
      this.k8sApi
        .getGlobalResourceList({
          type: RESOURCE_TYPES.CLUSTER,
          namespaced: true,
        })
        .pipe(
          map(
            (clusterList: KubernetesResourceList<Cluster>) =>
              new clusterActions.GetClustersSuccess(clusterList.items),
          ),
          catchError(error => of(new clusterActions.GetClustersFail(error))),
        ),
    ),
  );

  @Effect()
  getByName$ = this.actions$.pipe(
    ofType(clusterActions.GET_CLUSTER_BY_NAME),
    pluck('payload'),
    mergeMap((name: string) =>
      this.k8sApi
        .getGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          name,
          namespaced: true,
        })
        .pipe(
          map(cluster => new clusterActions.GetClusterByNameSuccess(cluster)),
          catchError(() => of(new clusterActions.GetClusterByNameFail(name))),
        ),
    ),
  );

  @Effect()
  createCluster$ = this.actions$.pipe(
    ofType(clusterActions.CREATE_CLUSTER),
    pluck('payload'),
    switchMap((payload: Cluster) =>
      this.k8sApi
        .postGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          resource: payload,
          namespaced: true,
        })
        .pipe(
          map(cluster => new clusterActions.CreateClusterSuccess(cluster)),
          catchError(rawError =>
            of(new clusterActions.CreateClusterFail(rawError)),
          ),
        ),
    ),
  );

  @Effect()
  deleteCluster$ = this.actions$.pipe(
    ofType(clusterActions.DELETE_CLUSTER),
    pluck('payload'),
    switchMap((payload: Cluster) =>
      this.k8sApi
        .deleteGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          resource: payload,
          namespaced: true,
        })
        .pipe(
          map(cluster => new clusterActions.DeleteClusterSuccess(cluster)),
          catchError(rawError =>
            of(
              new clusterActions.DeleteClusterFail({
                error: rawError,
              }),
            ),
          ),
        ),
    ),
  );
}
