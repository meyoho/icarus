import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {
  catchError,
  groupBy,
  map,
  mergeMap,
  pluck,
  switchMap,
} from 'rxjs/operators';

import { Namespace } from 'app/typings';

import { ProjectService } from '../../services/api/project.service';
import * as namespaceActions from '../actions/namespaces.actions';

@Injectable()
export class NamespacesEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly projectService: ProjectService,
  ) {}

  @Effect()
  getNamespacesByCluster$ = this.actions$.pipe(
    ofType(namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT),
    map(
      (action: namespaceActions.GetNamespacesByClusterAndProject) =>
        action.payload,
    ),
    groupBy(cluster => cluster),
    mergeMap(group => {
      return group.pipe(
        switchMap(({ cluster, project }) => {
          return this.projectService
            .getNamespacesByProjectAndCluster(project, cluster)
            .pipe(
              pluck('items'),
              map(
                (namespaces: Namespace[]) =>
                  new namespaceActions.GetNamespacesByClusterAndProjectSucc({
                    project,
                    cluster,
                    namespaces,
                  }),
              ),
              catchError(error =>
                of(
                  new namespaceActions.GetNamespacesByClusterAndProjectFail(
                    error,
                  ),
                ),
              ),
            );
        }),
      );
    }),
  );
}
