import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, pluck, switchMap } from 'rxjs/operators';

import { ProjectService } from 'app/services/api/project.service';
import { Project } from 'app/typings';

import * as projectActions from '../actions/projects.actions';

@Injectable()
export class ProjectEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly projectService: ProjectService,
  ) {}

  @Effect()
  loadProjects$ = this.actions$.pipe(
    ofType(projectActions.LOAD_PROJECTS),
    switchMap(() => {
      return this.projectService.getProjects().pipe(
        pluck('items'),
        map(
          (projects: Project[]) =>
            new projectActions.LoadProjectsSuccess(projects),
        ),
        catchError(error => of(new projectActions.LoadProjectsFail(error))),
      );
    }),
  );
}
