import { createSelector } from '@ngrx/store';

import { Namespace } from 'app/typings';

import * as fromFeature from '../reducers';
import * as fromNamespace from '../reducers/namespaces.reducers';

export interface NamespaceEntity {
  cluster: string;
  project: string;
  namespace: Namespace;
}

export const getProjectClusterMappings = createSelector(
  fromFeature.getNamespacesState,
  fromNamespace.getProjectClusterMappings,
);

export const getNamespaces = createSelector(
  getProjectClusterMappings,
  (mappings: fromNamespace.ProjectClusterMappings) => {
    return Object.keys(mappings).reduce((list, project: string) => {
      const mapping = mappings[project];
      const clusterNamespacesList = Object.keys(mapping).reduce(
        (_list, cluster: string) => {
          return [
            ..._list,
            ...Object.values(mapping[cluster].entities).map(namespace => {
              return { namespace, project, cluster };
            }),
          ];
        },
        [],
      );
      return [...list, ...clusterNamespacesList];
    }, []) as NamespaceEntity[];
  },
);

export const getNamespacesMappingLoading = createSelector(
  fromFeature.getNamespacesState,
  fromNamespace.getNamespaceLoading,
);
