import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromProject from '../reducers/projects.reducers';

export const getProjectsEntities = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectEntities,
);

export const getAllProjects = createSelector(getProjectsEntities, entities => {
  return Object.keys(entities).map(name => entities[name]);
});

export const getProjectsLoading = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoading,
);
export const getProjectsLoaded = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoaded,
);
