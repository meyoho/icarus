import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromCluster from '../reducers/clusters.reducers';

export const getClustersEntities = createSelector(
  fromFeature.getClustersState,
  fromCluster.getClusterEntities,
);

export const getAllClusters = createSelector(getClustersEntities, entities => {
  return Object.keys(entities).map(name => entities[name]);
});

export const getClustersLoading = createSelector(
  fromFeature.getClustersState,
  fromCluster.getClusterLoading,
);
export const getClustersLoaded = createSelector(
  fromFeature.getClustersState,
  fromCluster.getClusterLoaded,
);
