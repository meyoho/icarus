import { TranslateService } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Highcharts from 'highcharts';

@Component({
  selector: 'rc-root',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private readonly router: Router,
    private readonly location: Location,
    private readonly translate: TranslateService,
  ) {
    this.linkHandler = this.linkHandler.bind(this);
  }

  ngOnInit() {
    Highcharts.setOptions({
      time: {
        useUTC: false,
      },
      lang: {
        noData: this.translate.get('no_data'),
      },
    });
    document.addEventListener('click', this.linkHandler);
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.linkHandler);
  }

  // Workaround for events href links, https://stackoverflow.com/a/34787493
  private linkHandler(e: MouseEvent) {
    const el = this.getTargetLinkElement(e.target as Node);
    if (!el) {
      return;
    }

    const href = el.getAttribute('href');
    if (!href || href === '#' || href.startsWith('javascript:')) {
      e.preventDefault();
      return false;
    }
    const { pathname } = el;
    if (/^\/(admin|workspace)\//.test(pathname)) {
      e.preventDefault();
      this.router.navigateByUrl(this.location.normalize(pathname) + el.search);
      return false;
    }
  }

  private getTargetLinkElement(el: Node) {
    while (el) {
      if (el.nodeName === 'A') {
        return el as HTMLAnchorElement;
      }
      el = el.parentNode;
    }
    return null;
  }
}
