import { API_GATEWAY, ApiGatewayService } from '@alauda/common-snippet';
import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class ApiGatewayInterceptor implements HttpInterceptor {
  constructor(private readonly apiGateway: ApiGatewayService) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler) {
    if (!req.url.startsWith(API_GATEWAY)) {
      return next.handle(req);
    }

    return this.apiGateway.getApiAddress().pipe(
      switchMap(apiAddress => {
        const url = apiAddress + req.url.replace(API_GATEWAY, '');
        return next.handle(req.clone({ url }));
      }),
    );
  }
}
