import { CodeEditorIntl } from '@alauda/code-editor';
import {
  AuthorizationInterceptorService,
  NAV_CONFIG_LOCAL_STORAGE_KEY,
  ResourceErrorInterceptor,
  TOKEN_BASE_DOMAIN,
  TOKEN_GLOBAL_NAMESPACE,
  TOKEN_RESOURCE_DEFINITIONS,
} from '@alauda/common-snippet';
import { PaginatorIntl, TooltipCopyIntl } from '@alauda/ui';
import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MonacoProviderService } from 'ng-monaco-editor';

import { AppPaginatorIntl } from 'app/services/app-paginator-intl.service';
import { AppTooltipCopyIntl } from 'app/services/app-tooltip-copy-intl.service';
import { CustomCodeEditorIntlService } from 'app/services/code-editor-intl.service';
import { CustomMonacoProviderService } from 'app/services/custom-monaco-provider';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, RESOURCE_DEFINITIONS } from 'app/typings';

import { globalEnvironments } from '../app-global';

import { ApiGatewayInterceptor } from './api-gateway.interceptor';

export function getAppBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

@NgModule({
  providers: [
    {
      provide: ENVIRONMENTS,
      useValue: globalEnvironments,
    },
    {
      provide: TOKEN_BASE_DOMAIN,
      useFactory: (env: Environments) => env.LABEL_BASE_DOMAIN,
      deps: [ENVIRONMENTS],
    },
    {
      provide: TOKEN_GLOBAL_NAMESPACE,
      useFactory: (env: Environments) => env.GLOBAL_NAMESPACE,
      deps: [ENVIRONMENTS],
    },
    {
      provide: NAV_CONFIG_LOCAL_STORAGE_KEY,
      useValue: 'icarus',
    },
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: CodeEditorIntl,
      useClass: CustomCodeEditorIntlService,
    },
    {
      provide: PaginatorIntl,
      useClass: AppPaginatorIntl,
    },
    {
      provide: TooltipCopyIntl,
      useClass: AppTooltipCopyIntl,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResourceErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
    {
      provide: APP_BASE_HREF,
      useFactory: getAppBaseHref,
      deps: [PlatformLocation],
    },
    {
      provide: TOKEN_RESOURCE_DEFINITIONS,
      useValue: RESOURCE_DEFINITIONS,
    },
  ],
})
export class ServicesModule {}
