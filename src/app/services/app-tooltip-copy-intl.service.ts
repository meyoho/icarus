import { TranslateService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

@Injectable()
export class AppTooltipCopyIntl {
  constructor(private readonly translate: TranslateService) {}

  get copyTip() {
    return this.translate.get('tooltip_copy_click_to_copy');
  }

  get copySuccessTip() {
    return this.translate.get('tooltip_copy_succeeded');
  }

  get copyFailTip() {
    return this.translate.get('tooltip_copy_failed');
  }
}
