import { NavItemConfig } from '@alauda/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

/**
 * Provides a holder for UI templates to be used globally.
 */
export class TemplateHolder {
  private readonly templatePortalSubject = new BehaviorSubject<TemplatePortal>(
    undefined,
  );

  get templatePortal$() {
    return this.templatePortalSubject.pipe(debounceTime(0));
  }

  setTemplatePortal(templatePortal: TemplatePortal) {
    this.templatePortalSubject.next(templatePortal);
  }
}

export enum TemplateHolderType {
  PageHeaderContent = 'PageHeaderContent',
}

export interface ActiveNavItem {
  label: string;
  routerLink?: string | any[];
}

/**
 * Acts as a general ui state store
 */
// tslint:disable-next-line: max-classes-per-file
@Injectable({ providedIn: 'root' })
export class UiStateService {
  private readonly templateHolders = new Map<
    TemplateHolderType,
    TemplateHolder
  >();

  private _prevViewSwitchState: boolean = null;
  private _currViewSwitchState: boolean = null;

  activeItemInfo$ = new ReplaySubject<NavItemConfig[]>(1);
  logoMainSrc = 'logos/logo.svg';

  private registerTemplateHolder(id: TemplateHolderType) {
    if (this.templateHolders.has(id)) {
      throw new Error(`Template holder for ${id} has already registered!`);
    }
    this.templateHolders.set(id, new TemplateHolder());
  }

  // Will init template holder if not initialled yet
  getTemplateHolder(id: TemplateHolderType) {
    if (!this.templateHolders.has(id)) {
      this.registerTemplateHolder(id);
    }

    return this.templateHolders.get(id);
  }

  /**
   * NOTE: only set active item info in ConsoleComponent
   */
  setActiveItemInfo(activeNavItemInfo: NavItemConfig[]) {
    this.activeItemInfo$.next(activeNavItemInfo);
  }

  /**
   * NOTE: only use for view switch status recording
   */
  setViewSwitchState(state: boolean) {
    this._prevViewSwitchState = this._currViewSwitchState;
    this._currViewSwitchState = state;
  }

  shouldViewSwitchAnimate() {
    return ![null, this._currViewSwitchState].includes(
      this._prevViewSwitchState,
    );
  }
}
