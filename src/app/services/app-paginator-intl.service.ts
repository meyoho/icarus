import { TranslateService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

@Injectable()
export class AppPaginatorIntl {
  readonly changes = this.translate.locale$;

  get itemsPerPageLabel() {
    return this.translate.get('paginator_page_items');
  }

  get jumperLabelPrefix() {
    return this.translate.get('pagination_goto');
  }

  get jumperLabelSuffix() {
    return this.translate.get('pagination_page');
  }

  getTotalLabel = (length: number) =>
    this.translate.get('paginator_total_records', { length });

  constructor(private readonly translate: TranslateService) {}
}
