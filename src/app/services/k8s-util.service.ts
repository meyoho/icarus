import {
  ANNOTATIONS,
  K8sUtilService as _K8sUtilService,
  KubernetesResource,
  LABELS,
  NAME,
  ResourceWriteParams,
  publishRef,
} from '@alauda/common-snippet';
import { Injectable, Injector } from '@angular/core';
import { cloneDeep, get } from 'lodash-es';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {
  UpdateKeyValueDialogComponent,
  UpdateKeyValueDialogData,
} from 'app/shared/form-table/update-key-value-dialog/update-key-value-dialog.component';
import { RESOURCE_TYPES, ResourceType } from 'app/typings';
import { KUBE_PUBLIC_NAMESPACE, TRUE } from 'app/utils';
import { ClusterNetworkType } from 'app_admin/features/network/type';

import { FeatureGateApiService } from './api/feature-gate.service';

@Injectable({
  providedIn: 'root',
})
export class K8sUtilService extends _K8sUtilService<ResourceType> {
  constructor(
    injector: Injector,
    private readonly featureGateApi: FeatureGateApiService,
  ) {
    super(injector);
  }

  /**
   * 更新 Annotation 或 Label
   */
  updatePart<T extends KubernetesResource>(
    params: ResourceWriteParams<ResourceType, T> & {
      readonlyKeys?: Array<string | RegExp>;
      part: typeof ANNOTATIONS | typeof LABELS;
    },
  ) {
    const { part, resource, readonlyKeys } = params;
    const keyValues = get(resource, `metadata.${part}`, {});
    const updated$$ = new Subject<T>();
    this.dialog.open<
      UpdateKeyValueDialogComponent<T>,
      UpdateKeyValueDialogData<T>
    >(UpdateKeyValueDialogComponent, {
      data: {
        title: `update_${part}`,
        keyValues,
        readonlyKeys,
        updateSuccessMsg: `update_${part}_successed`,
        updateFailMsg: `update_${part}_failed`,
        onUpdate: results => {
          const checkedResults = cloneDeep(results);
          Object.keys(keyValues).forEach(key => {
            if (!(key in checkedResults)) {
              checkedResults[key] = null;
            }
          });
          // @ts-ignore
          return (this.k8sApi[
            'cluster' in params && params.cluster
              ? 'patchResource'
              : 'patchGlobalResource'
          ]({
            ...params,
            part: {
              metadata: {
                [part]: checkedResults,
              },
            },
          }) as Observable<T>).pipe(
            tap(_resource => updated$$.next(_resource)),
          );
        },
      },
    });
    return updated$$.asObservable();
  }

  /**
   *  集群网络类型，支持kube-ovn，calico,galaxy
   *  若kube-ovn，calico同时存在，优先判定为ovn
   */
  getNetworkTypeByCluster$(cluster: string): Observable<ClusterNetworkType> {
    // 获取kube-public下全部的config-map进行匹配，此步骤数据不会很大，并且只调一次API
    return combineLatest([
      this.k8sApi.getResourceList({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster,
        namespace: KUBE_PUBLIC_NAMESPACE,
      }),
      // galaxy network judged from feature gate,others will use feature gate gradually
      this.featureGateApi.getClusterGate(cluster, 'galaxy').pipe(
        map(feature => !!get(feature, 'status.enabled')),
        catchError(() => of(false)),
      ),
    ]).pipe(
      map(([{ items }, isGalaxy]) => {
        if (isGalaxy) {
          return ClusterNetworkType.GALAXY;
        }
        let networkType = ClusterNetworkType.DEFAULT;
        if (!Array.isArray(items) || items.length === 0) {
          return networkType;
        }
        items.forEach(item => {
          switch (get(item, 'metadata.name')) {
            case ClusterNetworkType.KUBE_OVN: {
              networkType = ClusterNetworkType.KUBE_OVN;
              break;
            }
            case ClusterNetworkType.MACVLAN: {
              networkType = ClusterNetworkType.MACVLAN;
              break;
            }
            case ClusterNetworkType.CALICO: {
              // only override default
              if (networkType === ClusterNetworkType.DEFAULT) {
                networkType = ClusterNetworkType.CALICO;
              }
              break;
            }
          }
        });
        return networkType;
      }),
      catchError(() => of(ClusterNetworkType.DEFAULT)),
      publishRef(),
    );
  }

  isFederatedResource(resource: KubernetesResource) {
    return resource && this.getLabel(resource, 'federated') === TRUE;
  }

  isFederatedMasterCluster(resource: KubernetesResource) {
    return (
      resource &&
      this.getLabel(resource, 'hostname', 'federation') ===
        resource.metadata.name
    );
  }

  isFederatedCluster(resource: KubernetesResource) {
    return resource && !!this.getLabel(resource, NAME, 'federation');
  }
}
