import { API_GATEWAY, KubernetesResourceList } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Namespace, Project } from 'app/typings';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  constructor(private readonly httpClient: HttpClient) {}

  getProjects(
    params: {
      limit?: string;
      continue?: string;
    } = {},
  ) {
    return this.httpClient.get<KubernetesResourceList<Project>>(
      `${API_GATEWAY}/auth/v1/projects`,
      {
        params,
      },
    );
  }

  getNamespacesByProjectAndCluster(
    project: string,
    cluster: string,
    params?: {
      continue?: string;
    },
  ) {
    return this.httpClient.get<KubernetesResourceList<Namespace>>(
      `${API_GATEWAY}/auth/v1/projects/${project}/clusters/${cluster}/namespaces`,
      {
        params,
      },
    );
  }
}
