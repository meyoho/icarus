import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

export const DIAGNOSE_URL = `${API_GATEWAY}/morgans/_diagnose`;
export interface Diagnose {
  details: Array<{
    name: string;
    status: string;
    message: string;
  }>;
}

@Injectable({ providedIn: 'root' })
export class AdvanceApiService {
  constructor(private readonly http: HttpClient) {}

  getDiagnoseInfo() {
    return this.http.get(DIAGNOSE_URL).pipe(
      map((res: Diagnose) => {
        const esStatus = res.details.find(
          detail => detail.name === 'elasticsearch',
        );
        return (
          !esStatus ||
          (esStatus.status === 'ok' &&
            (esStatus.message === 'green' || esStatus.message === 'yellow'))
        );
      }),
    );
  }
}
