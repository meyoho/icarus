import { API_GATEWAY } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

import {
  K8sEventQuery,
  K8sEventsResult,
} from 'app/features-shared/event/k8s-event-list/event.types';
import { HttpService } from 'app/services/http.service';

@Injectable({ providedIn: 'root' })
export class EventService {
  K8S_EVENT_URL: string;

  constructor(private readonly httpService: HttpService) {
    this.K8S_EVENT_URL = `${API_GATEWAY}/v4/events`;
  }

  getK8sEvents({
    start_time,
    end_time,
    cluster,
    name = '',
    kind = '',
    namespace = '',
    fuzzy,
    pageno = 1,
    size = 20,
  }: K8sEventQuery): Promise<K8sEventsResult> {
    return this.httpService.request(this.K8S_EVENT_URL, {
      method: 'GET',
      params: {
        start_time,
        end_time,
        cluster,
        name,
        fuzzy,
        kind,
        namespace,
        pageno,
        size,
      },
    });
  }
}
