import {
  API_GATEWAY,
  KubernetesResource,
  KubernetesResourceList,
  StringMap,
  TOKEN_GLOBAL_NAMESPACE,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { Chart, ChartRepo, ChartRepoCreate } from 'app/typings';
const BASE_URL = `${API_GATEWAY}/catalog/v1`;

interface ChartFiles {
  files: StringMap;
}

interface HelmRequestResources {
  resources: KubernetesResource[];
}

/**
 * http://confluence.alaudatech.com/pages/viewpage.action?pageId=50836763
 */
@Injectable({
  providedIn: 'root',
})
export class CatalogApiService {
  constructor(
    private readonly httpClient: HttpClient,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  createChartRepo(payload: ChartRepoCreate) {
    return this.httpClient.post(`${BASE_URL}/chartrepos`, payload);
  }

  updateChartRepo(payload: ChartRepoCreate) {
    const { name } = payload.spec.chartRepo.metadata;
    return this.httpClient.put<ChartRepo>(
      `${BASE_URL}/chartrepos/${this.globalNamespace}/${name}`,
      payload,
    );
  }

  triggerRepoSync(chartRepo: ChartRepo) {
    const { name } = chartRepo.metadata;
    return this.httpClient.put<ChartRepo>(
      `${BASE_URL}/chartrepos/${this.globalNamespace}/${name}/resync`,
      {},
    );
  }

  getChartList(params: {
    appCluster?: string;
    appNamespace?: string;
    labelSelector?: string;
    fieldSelector?: string;
  }) {
    return this.httpClient.get<KubernetesResourceList<Chart>>(
      `${BASE_URL}/charts/${this.globalNamespace}`,
      {
        params,
      },
    );
  }

  getChartFiles(params?: { chartName: string; chartVersion: string }) {
    return this.httpClient.get<ChartFiles>(
      `${BASE_URL}/charts/${this.globalNamespace}/${params.chartName}/versions/${params.chartVersion}/files`,
    );
  }

  getHelmRequestResources(params: {
    cluster: string;
    namespace: string;
    hrName: string;
  }) {
    return this.httpClient.get<HelmRequestResources>(
      `${BASE_URL}/clusters/${params.cluster}/helmrequests/${params.namespace}/${params.hrName}/resources`,
    );
  }
}
