import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { PodSecurityPolicy } from 'app/typings';

export const DEFAULT_PSP_NAME = 'cluster';

@Injectable({ providedIn: 'root' })
export class PodSecurityPolicyService {
  constructor(private readonly http: HttpClient) {}

  url(cluster: string) {
    return `${API_GATEWAY}/acp/v1/kubernetes/${cluster}/podsecuritypolicies/${DEFAULT_PSP_NAME}`;
  }

  getPolicy(cluster: string): Observable<PodSecurityPolicy> {
    return this.http.get(this.url(cluster));
  }

  postPolicy(cluster: string, res: PodSecurityPolicy) {
    return this.http.post(this.url(cluster), res);
  }

  putPolicy(cluster: string, res: PodSecurityPolicy) {
    return this.http.put(this.url(cluster), res);
  }

  deletePolicy(cluster: string): Observable<void> {
    return this.http.delete<void>(this.url(cluster));
  }
}
