import {
  API_GATEWAY,
  KubernetesResource,
  LabelSelector,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DeploymentStatus, StatefulSetStatus } from 'app/typings';

// http://confluence.alauda.cn/pages/viewpage.action?pageId=56991250
@Injectable({ providedIn: 'root' })
export class FederatedAppApiService {
  constructor(private readonly http: HttpClient) {}

  url({
    cluster,
    namespace,
    name = '',
  }: {
    cluster: string;
    namespace: string;
    name?: string;
  }) {
    return `${API_GATEWAY}/acp/v1/kubernetes/${cluster}/namespaces/${namespace}/federatedapplications/${name}`;
  }

  postFederatedApp(
    params: FederatedAppParams,
    body: FederatedAppTemplatesResource,
  ) {
    return this.http.post<FederatedApp>(this.url(params), body);
  }

  putFederatedApp(
    params: SpecificFederatedAppParams,
    body: FederatedAppTemplatesResource,
  ) {
    return this.http.put<FederatedApp>(this.url(params), body);
  }

  startFederatedApp(params: SpecificFederatedAppParams) {
    return this.http.post<void>(this.url(params) + '/start', null);
  }

  stopFederatedApp(params: SpecificFederatedAppParams) {
    return this.http.post<void>(this.url(params) + '/stop', null);
  }

  getFederatedAppResource(params: SpecificFederatedAppParams) {
    return this.http.get<FederatedAppComponentsResource>(
      this.url(params) + '/components',
    );
  }
}

export interface FederatedAppParams {
  cluster: string;
  namespace: string;
}

export interface SpecificFederatedAppParams extends FederatedAppParams {
  name: string;
}

export interface FederatedAppSpecTemplate extends KubernetesResource {
  spec: {
    assemblyPhase: string;
    componentKinds: Array<{ group: string; kind: string }>;
    componentTemplates: FederatedAppTemplate[];
    descriptor: unknown;
    selector: LabelSelector;
  };
  status: FederatedAppWorkloadStatus;
}

export interface FederatedAppSpec {
  placement: {
    clusterSelector: {
      matchLabels: Record<string, string>;
    };
  };
  overrides: FederatedAppOverride[];
  template: FederatedAppSpecTemplate;
}

export interface FederatedAppWorkloadStatus {
  name: string;
  kind: string;
  group: string;
  current: number;
  desired: number;
  status: DeploymentStatus | StatefulSetStatus;
  messages: unknown[];
}

export interface FederatedAppStatus {
  clusters?: Array<{ name: string; status?: string }>;
  conditions: Array<{
    lastTransitionTime: string;
    lastUpdateTime: string;
    status: string;
    type: string;
  }>;
}

export interface FederatedApp extends KubernetesResource {
  spec: FederatedAppSpec;
  status: FederatedAppStatus;
}

export enum ClusterOverrideOp {
  Add = 'add',
  Remove = 'remove',
  Replace = 'replace',
}

export interface ClusterOverride {
  op?: ClusterOverrideOp;
  path: string;
  value?: any;
}

export interface FederatedAppOverride {
  clusterName: string;
  clusterOverrides: ClusterOverride[];
}

export interface FederatedAppComponent {
  resource: KubernetesResource;
  overrides: FederatedAppOverride[];
}

export interface FederatedAppTemplate extends KubernetesResource {
  spec: {
    componentTemplates: FederatedAppComponent[];
    selector?: LabelSelector;
    descriptor?: unknown;
  };
}

export interface FederatedAppTemplatesSpec {
  placement: {
    clusterSelector: LabelSelector;
  };
  overrides: FederatedAppOverride[];
  template: FederatedAppTemplate;
}

export interface FederatedAppTemplatesResource extends KubernetesResource {
  spec?: FederatedAppTemplatesSpec;
}

export interface FederatedAppComponentsResource extends KubernetesResource {
  spec: {
    components: KubernetesResource[];
  };
}
