import { StringMap } from '@alauda/common-snippet';

export const ZH_ERRORS: StringMap = {
  Unknown: '未知错误',
  Unauthorized: '授权不足',
  Forbidden: '资源不可用',
  NotFound: '找不到资源',
  AlreadyExists: '资源已存在',
  Conflict: '请求冲突',
  Gone: '资源位置已变更',
  Invalid: '数据错误',
  ServerTimeout: '响应超时',
  Timeout: '请求超时',
  TooManyRequests: '请求太多，超过服务能力',
  BadRequest: '无效的请求',
  MethodNotAllowed: '请求的操作不被允许',
  NotAcceptable: '接收的数据类型不被支持',
  RequestEntityTooLarge: '请求实体太大',
  UnsupportedMediaType: '发送的数据类型不被支持',
  InternalError: '内部错误',
  Expired: '请求内容已过期',
  ServiceUnavailable: '当前请求的服务不可用，可稍后重试',
};

export const ZH_ERROR_DEFAULT = '网络错误';
