import { CodeEditorIntl } from '@alauda/code-editor';
import { TranslateService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
/**
 * aui-code-editor 中 label 翻译依赖 CodeEditorIntl 中的具体属性值
 * 由于引用方式为 intl.copyLabel，因此为了响应 translation , 将各属性转化为 getter
 */
@Injectable()
export class CustomCodeEditorIntl extends CodeEditorIntl {
  copyLabel = 'copy';

  copiedLabel = 'code_editor_copied';

  readonlyLabel = 'readonly';

  readwriteLabel = 'code_editor_writable';

  clearLabel = 'clear';

  recoverLabel = 'code_editor_recover';

  findLabel = 'code_editor_find';

  formatLabel = 'code_editor_format';

  foldLabel = 'code_editor_fold';

  exportLabel = 'code_editor_export';

  importLabel = 'code_editor_import';

  lightThemeLabel = 'light_theme';

  darkThemeLabel = 'dark_theme';

  showDiffLabel = 'code_editor_show_diff';

  fullscreenLabel = 'code_editor_fullscreen';

  exitFullscreenLabel = 'code_editor_exit_fullscreen';

  constructor(public translate: TranslateService) {
    super();

    Object.entries(this as Record<string, unknown>)
      .filter(
        // 标签名应当以Label结尾，同时类型检查，以防是方法
        ([key, value]) => key.endsWith('Label') && typeof value === 'string',
      )
      .forEach(([key, value]) => {
        Object.defineProperty(this, key, {
          get: () => this.translate.get(value),
        });
      });
  }
}
