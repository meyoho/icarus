import {
  K8sApiService,
  KubernetesResource,
  Status,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { assocPath, path } from 'ramda';
import { Observable, from, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { ResourceType } from 'app/typings';

@Injectable({ providedIn: 'root' })
export class ResourceUpdateService {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
  ) {}

  retryPutResource<T extends KubernetesResource>(params: {
    type: ResourceType;
    cluster: string;
    resource: T;
  }): Observable<T> {
    const put = (res: T) =>
      this.k8sApi.putResource({ ...params, resource: res });
    const get = () =>
      this.k8sApi.getResource<T>({
        type: params.type,
        cluster: params.cluster,
        namespace: params.resource.metadata.namespace,
        name: params.resource.metadata.name,
      });

    return this.retryUpdate(put, get, params.resource);
  }

  retryPutGlobalResource<T extends KubernetesResource>(params: {
    type: ResourceType;
    namespaced: boolean;
    resource: T;
  }): Observable<T> {
    const put = (res: T) =>
      this.k8sApi.putGlobalResource({ ...params, resource: res });
    const get = () =>
      this.k8sApi.getGlobalResource<T>({
        type: params.type,
        namespaced: params.namespaced,
        name: params.resource.metadata.name,
      });

    return this.retryUpdate(put, get, params.resource);
  }

  private retryUpdate<T extends KubernetesResource>(
    put: (resource: T) => Observable<T>,
    get: () => Observable<T>,
    resource: T,
  ): Observable<T> {
    const waitUserConfirmRetry = (err: Status) =>
      from(
        this.dialog.confirm({
          title: this.translate.get('resource_version_conflict'),
          confirmText: this.translate.get('retry'),
          cancelText: this.translate.get('cancel'),
        }),
      ).pipe(
        switchMap(() =>
          get().pipe(
            switchMap(newResource => {
              const lens = ['metadata', 'resourceVersion'];
              return put(
                assocPath(lens, path<string>(lens, newResource), resource),
              );
            }),
            catchError(() => {
              return this.retryUpdate(put, get, resource);
            }),
          ),
        ),
        catchError(() => {
          return throwError(err);
        }),
      );

    return put(resource).pipe(
      catchError((err: Status) => {
        if (err.code !== 409) {
          return throwError(err);
        } else {
          return waitUserConfirmRetry(err);
        }
      }),
    );
  }
}
