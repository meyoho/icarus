import { K8sApiService, publishRef } from '@alauda/common-snippet';
import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import { catchError, map, shareReplay, takeUntil } from 'rxjs/operators';

import { Cluster, Namespace, RESOURCE_TYPES } from 'app/typings';

import { K8sUtilService } from './k8s-util.service';

@Injectable({ providedIn: 'root' })
export class FederationUtilService implements OnDestroy {
  private readonly destroy$$ = new Subject<void>();

  clusterCache: Record<string, Observable<Cluster>> = {};
  namespaceCache: Record<string, Observable<Namespace>> = {};

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  isMasterFedContext(cluster: string, namespace: string) {
    return combineLatest([
      this.getCluster(cluster).pipe(
        map(
          cluster =>
            this.k8sUtil.isFederatedCluster(cluster) &&
            this.k8sUtil.isFederatedMasterCluster(cluster),
        ),
      ),
      this.getNamespace(cluster, namespace).pipe(
        map(this.k8sUtil.isFederatedResource.bind(this.k8sUtil)),
      ),
    ]).pipe(
      map(conditions => conditions.every(v => v)),
      catchError(() => of(false)),
      publishRef(),
    );
  }

  isFedNamespace(cluster: string, namespace: string) {
    return this.getNamespace(cluster, namespace).pipe(
      map(this.k8sUtil.isFederatedResource.bind(this.k8sUtil)),
      catchError(() => of(false)),
      publishRef(),
    );
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  private getCluster(cluster: string) {
    if (!this.clusterCache[cluster]) {
      this.clusterCache[cluster] = this.k8sApi
        .getGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          namespaced: true,
          name: cluster,
        })
        .pipe(takeUntil(this.destroy$$), shareReplay(1));
    }
    return this.clusterCache[cluster];
  }

  private getNamespace(cluster: string, namespace: string) {
    const key = `${cluster}.${namespace}`;
    if (!this.namespaceCache[key]) {
      this.namespaceCache[key] = this.k8sApi
        .getResource({
          type: RESOURCE_TYPES.NAMESPACE,
          cluster,
          name: namespace,
        })
        .pipe(takeUntil(this.destroy$$), shareReplay(1));
    }
    return this.namespaceCache[key];
  }
}
