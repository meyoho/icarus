import {
  AuthorizationStateService,
  K8sPermissionService,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { Inject, Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map, pluck, switchMap } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, RESOURCE_TYPES } from 'app/typings';

export interface MenuDefinition {
  name: string;
  icon: string;
  link?: string;
  href?: string;
  type?: string;
  allowed?: boolean;
  env?: string;
}

export const MENU_DEFINITIONS: MenuDefinition[] = [
  {
    name: 'project_management',
    icon: 'project_management_s',
    link: 'home/project',
    type: 'projectview',
  },
  {
    name: 'platform_management',
    icon: 'setting_home',
    link: 'manage',
    type: 'platformview',
  },
  {
    name: 'my_profile',
    icon: 'user_s',
    link: 'home/personal-info',
    allowed: true,
  },
];

@Injectable({ providedIn: 'root' })
export class GlobalPermissionService {
  accountMenus$: Observable<MenuDefinition[]> = combineLatest([
    this.translate.locale$,
    this.auth.getToken(),
  ]).pipe(
    map(([locale, token]) => {
      const OTHER_MENU_DEFINITIONS: MenuDefinition[] = [];

      if (this.env.API_MARKET_URL) {
        const url = new URL(this.env.API_MARKET_URL);

        url.searchParams.set('locale', locale);
        url.searchParams.set('id_token', token);

        OTHER_MENU_DEFINITIONS.push({
          name: 'api_market',
          icon: 'api_market',
          href: url.href,
          allowed: true,
        });
      }

      return MENU_DEFINITIONS.concat(OTHER_MENU_DEFINITIONS);
    }),
    switchMap(menuDefinitions => {
      return this.k8sPermission
        .isAllowed({
          type: RESOURCE_TYPES.VIEW,
          name: menuDefinitions.reduce<string[]>((acc, { allowed, type }) => {
            if (!allowed) {
              acc.push(type);
            }
            return acc;
          }, []),
        })
        .pipe(
          map(statuses => {
            return menuDefinitions.map((menu, index) => ({
              ...menu,
              allowed: menu.allowed || statuses[index],
            }));
          }),
        );
    }),
    publishRef(),
  );

  viewPermissions$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.VIEW,
      name: ['acp-userview', 'acp-manageview'],
    })
    .pipe(
      map(res => ({
        userView: res[0],
        adminView: res[1],
      })),
      publishRef(),
    );

  showSwitchView$ = this.viewPermissions$.pipe(
    map(permissions => permissions.userView && permissions.adminView),
  );

  userViewAllowed$ = this.viewPermissions$.pipe(pluck('userView'));

  adminViewAllowed$ = this.viewPermissions$.pipe(pluck('adminView'));

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly translate: TranslateService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly auth: AuthorizationStateService,
  ) {}
}
