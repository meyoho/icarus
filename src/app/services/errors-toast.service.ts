import { TranslateService } from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { get, sortBy } from 'lodash-es';

import { ErrorResponse } from 'app/services/http.service';

export interface ErrorsItem {
  code: string;
  source?: number;
  message?: string;
  fields?: any;
}

export const genericCodes = [
  'unknown_issue',
  'server_error',
  'invalid_args',
  'resource_not_exist',
  'resource_already_exist',
  'kubernetes_error',
  'partial_permission_denied',
  'project_resources_exist',
  'child_resources_exist',
];

@Injectable({ providedIn: 'root' })
export class ErrorsToastService {
  constructor(
    private readonly auiMessageService: MessageService,
    private readonly auiNotificationService: NotificationService,
    private readonly translateService: TranslateService,
  ) {}

  /**
   * 处理用用的错误代码，ErrorResponse中 errors字段表示API返回的错误（参考ErrorResponse定义），通常我们只关心第一个errors[0]
   * 每个error 包含如下结构：
   * {
   *  code: string;
   *  source?: number;
   *  message?: string;
   *  fields?: any;
   * }
   * 这里应该只处理 在多个业务场景下都存在的error 比如 partial_permission_denied 的权限问题
   * 处理过的error 将从源errors中移除，在业务组件中可以继续处理没有匹配到的error
   * 包含在ignoredCodes 中的error code将不会被处理
   */
  handleGenericAjaxError({
    errors,
    ignoredCodes,
    handleNonGenericCodes = true,
    fallbackMessage,
  }: {
    errors: ErrorsItem[];
    ignoredCodes?: string[];
    handleNonGenericCodes?: boolean; // 若为true， 将会处理所有的error code，业务组件里就不需要额外处理了。 默认开启。
    fallbackMessage?: string;
  }) {
    const error = this.parseErrors({
      errors,
      ignoredCodes,
      handleNonGenericCodes,
      fallbackMessage,
    });
    if (error.title || error.content) {
      this.auiNotificationService.error({
        title: error.title,
        content: error.content,
      });
      errors.shift();
    }
    return errors;
  }

  parseErrors({
    errors,
    ignoredCodes,
    handleNonGenericCodes = true,
    fallbackMessage,
  }: {
    errors: ErrorsItem[];
    ignoredCodes?: string[];
    handleNonGenericCodes?: boolean; // 若为true， 将会处理所有的error code，业务组件里就不需要额外处理了。 默认开启。
    fallbackMessage?: string;
  }): {
    title?: string;
    content?: string;
  } {
    let errorContent: string;
    let errorTitle: string;
    if (!errors || errors.length === 0) {
      if (fallbackMessage) {
        this.auiMessageService.error({
          content: fallbackMessage,
        });
      }
      return {};
    }
    // We just handle the first error
    const error = errors[0];
    if (ignoredCodes && ignoredCodes.includes(error.code)) {
      return {};
    }
    if (genericCodes.includes(error.code)) {
      errorTitle = this.translateService.get(error.code);
      switch (error.code) {
        case 'partial_permission_denied':
          errorContent = this.getPartialPermissionMessage(error);
          break;
        case 'project_resources_exist':
        case 'child_resources_exist':
          errorContent = this.getResourcesExistMessage(error);
          break;
        case 'resource_not_exist':
        case 'resource_already_exist':
        case 'kubernetes_error':
        case 'unknown_issue':
        case 'server_error':
        case 'invalid_args':
          errorContent = error.message;
          break;
        default:
          errorContent = error.message;
          break;
      }
    } else {
      if (handleNonGenericCodes) {
        const translatedCode = this.translateService.get(error.code);
        errorContent =
          error.code === translatedCode ? error.message : translatedCode;
      }
    }
    return {
      title: errorTitle,
      content: errorContent,
    };
  }

  error(rejection: any) {
    if (!(rejection instanceof ErrorResponse)) {
      if (rejection && process.env.NODE_ENV === 'development') {
        console.error(rejection);
      }
      return;
    }

    if (rejection.errors) {
      return this.handleGenericAjaxError({ errors: rejection.errors });
    }

    this.auiNotificationService.error({
      content: this.translateService.get(
        get(rejection, 'error.message') ||
          this.translateService.get('unknown_issue'),
      ),
    });
  }

  private getPartialPermissionMessage(error: ErrorsItem) {
    const messageFields = JSON.parse(error.message);
    return messageFields
      .map((field: any) => {
        return `${field.action} (${field.name})`;
      })
      .join('<br>');
  }

  private getResourcesExistMessage(error: ErrorsItem) {
    let messageFields = JSON.parse(error.message);
    messageFields = sortBy(messageFields, (item: any) => {
      return item.type;
    });
    return messageFields
      .map((field: any) => {
        return `${this.translateService.get(field.type.toLowerCase())}: ${
          field.name
        }`;
      })
      .join('<br>');
  }
}
