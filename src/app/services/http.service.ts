import { TranslateService } from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { TimeoutError } from 'rxjs';
import { timeout } from 'rxjs/operators';

import { TRUE } from 'app/utils';

/**
 * Mimic the error response object between rubick UI and Django (MathildeErrorResponse)
 */
export class ErrorResponse extends HttpErrorResponse {
  readonly errors?: [
    {
      code: string;
      source?: number;
      message?: string;
      fields?: any;
    },
  ];

  constructor(init: any, errors: any) {
    super(init);
    this.errors = errors;
  }
}

/**
 * Interface for request options.
 */
export interface RequestOptions<T> {
  method: string;

  /**
   * Parameters in the URL (search string)
   */
  params?: {
    [key: string]: any | any[];
  };

  /**
   * Whether to delete project_name param
   */
  ignoreProject?: boolean;

  /**
   * Body, aka 'data' for legacy service.
   */
  body?: any;

  /**
   * Expected response type. Will be used to do decoding.
   */
  responseType?: HttpRequest<T>['responseType'];

  /**
   * Whether this request should be made in a way that exposes progress events.
   *
   * Progress events are expensive (change detection runs on each event) and so
   * they should only be requested if the consumer intends to monitor them.
   */
  reportProgress?: boolean;

  /**
   * Whether or not to cache the result?
   * Should only be valid for GET/HEAD request.
   *
   * Note, this will enable lifetime cache for the request. There is not way to clean up or update.
   */
  cache?: boolean;

  /**
   * Optional URL. Only be viable when the first request param is not URL.
   */
  url?: string;

  /**
   * Custom timeout in ms
   */
  timeout?: number;
}

const HEADER_NO_JSON_PARSE = 'no-json';

/**
 * Utility function for fetching XHR requests
 */
// tslint:disable-next-line: max-classes-per-file
@Injectable({ providedIn: 'root' })
export class HttpService {
  private readonly _cache = new Map<string, Promise<any>>();
  TIMEOUT_IN_MS: number;

  constructor(
    private readonly http: HttpClient,
    private readonly translate: TranslateService,
    private readonly auiMessageService: MessageService,
    private readonly auiNotificationService: NotificationService,
    private readonly router: Router,
  ) {
    this.TIMEOUT_IN_MS = 30000; // default 30 seconds
  }

  /**
   * Wraps Angular 2 http service:
   *   - Add new headers
   *   - Map Observable to Promise
   *   - Handle exceptions
   *   - Handle timeout
   * @param url
   * @param options
   * @returns {Promise<any>}
   */
  request<T>(url: string, options?: RequestOptions<T>): Promise<T>;
  request<T>(options: RequestOptions<T>): Promise<T>;
  request(url: string, options?: RequestOptions<any>): Promise<any>;
  request(options: RequestOptions<any>): Promise<any>;
  // eslint-disable-next-line sonarjs/cognitive-complexity
  request<T>(
    urlOrOptions: string | RequestOptions<T>,
    requestOptions: RequestOptions<T> = { method: 'GET' },
  ): Promise<T> {
    const { url, method, options } = this.prepareRequest(
      urlOrOptions,
      requestOptions,
    );

    const cacheKey = this.generateCacheKey(url, options.params);
    if (requestOptions.cache && this._cache.has(cacheKey)) {
      return this._cache.get(cacheKey);
    }

    const requestPromise = this.http
      .request(method, url, options)
      .pipe(
        timeout(
          requestOptions.timeout ? requestOptions.timeout : this.TIMEOUT_IN_MS,
        ),
      )
      .toPromise()
      .catch(error => {
        // In a real world app, we might use a remote logging infrastructure
        // Note: using TS 2.1 new feature 'lookup types' here.
        if (error instanceof HttpErrorResponse) {
          // Log out the user when API returns 401
          if (this.isJakiroErrorResponse(error) && error.status === 401) {
            this.logout();
            const message = this.translate.get('logging_out');
            this.auiNotificationService.error({
              content: message,
              id: 'logging-notification',
            });
          } else if (error.status === 0 || error.status === 504) {
            if (method.toLowerCase() === 'post') {
              this.auiMessageService.warning({
                content: this.translate.get('network_ex'),
              });
            }
            throw error;
          }

          throw new ErrorResponse(error, error.error.errors);
        } else if (
          error instanceof TimeoutError &&
          method.toLowerCase() === 'post'
        ) {
          this.auiMessageService.warning({
            content: this.translate.get('network_ex'),
          });
        }
        throw error;
      });

    if (requestOptions.cache) {
      this._cache.set(cacheKey, requestPromise);
    }
    return requestPromise;
  }

  logout() {
    this.request('ap/logout')
      .then(({ url }) => {
        this.router.navigateByUrl(url || '/landing');
      })

      .catch(console.error);
  }

  private prepareRequest<T>(
    urlOrOptions: string | RequestOptions<T>,
    requestOptions?: RequestOptions<T>,
  ) {
    let url: string;
    if (typeof urlOrOptions === 'object') {
      if (!urlOrOptions.url) {
        throw new Error('No URL is given!');
      }
      url = urlOrOptions.url;
      requestOptions = urlOrOptions;
    } else {
      url = urlOrOptions;
    }

    const params = Object.entries(requestOptions.params || {}).reduce(
      (accum, [key, value]) => {
        if (value != null) {
          accum = accum.set(key, value);
        }
        return accum;
      },
      new HttpParams(),
    );

    const headers = new HttpHeaders();
    if (
      requestOptions.responseType &&
      (requestOptions.responseType === 'blob' ||
        requestOptions.responseType === 'arraybuffer')
    ) {
      headers.set(HEADER_NO_JSON_PARSE, TRUE);
    }

    return {
      url,
      method: requestOptions.method || 'GET',
      options: {
        body: requestOptions.body,
        params,
        responseType: requestOptions.responseType,
        headers,
      },
    };
  }

  private generateCacheKey(url: string, params: any) {
    return (
      url +
      '/' +
      Object.entries(params)
        .map(([param, value]) => param + '=' + value)
        .join('&')
    );
  }

  private isJakiroErrorResponse(response: HttpErrorResponse) {
    const error = get(response, 'error.errors[0]');
    if (error) {
      return error.source === '1007';
    } else {
      return false;
    }
  }
}
