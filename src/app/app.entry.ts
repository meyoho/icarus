import { recordInitUrl } from '@alauda/common-snippet';
import { ApplicationRef, enableProdMode } from '@angular/core';
import { enableDebugTools } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ajax } from 'rxjs/ajax';
import { retry, switchMap } from 'rxjs/operators';
import 'zone.js/dist/zone';

import { environment } from '../environments/environment';

import { globalEnvironments } from './app-global';
import { AppModule } from './app.module';
import './vendor.entry';

if (environment.production) {
  enableProdMode();
}

recordInitUrl();

ajax
  .getJSON('api/v1/envs')
  .pipe(
    retry(3),
    switchMap(async envs => {
      Object.assign(globalEnvironments, envs);
      const platformRef = await platformBrowserDynamic().bootstrapModule(
        AppModule,
      );
      if (globalEnvironments.DEBUG) {
        Error.stackTraceLimit = Infinity;
        enableDebugTools(
          platformRef.injector.get(ApplicationRef).components[0],
        );
      }
    }),
  )
  .subscribe();
