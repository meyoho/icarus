// tslint:disable: no-duplicate-string
import { createResourceDefinitions } from '@alauda/common-snippet';

const AIOPS_GROUP = 'aiops.alauda.io';
const CRD_GROUP = 'crd.alauda.io';
const APP_GROUP = 'app.alauda.io';
const AUTH_GROUP = 'auth.alauda.io';
const TKE_PLATFORM_GROUP = 'platform.tkestack.io';

// reference: http://confluence.alaudatech.com/pages/viewpage.action?pageId=39338225
// FIXME: object destructuring will result `any` type here, why???
const _ = createResourceDefinitions({
  ALAUDA_LOADBALANCER2: {
    apiGroup: CRD_GROUP,
    type: 'alaudaloadbalancer2',
  },
  ALERT_TEMPLATE: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'alerttemplates',
  },
  APPLICATION: {
    apiGroup: 'app.k8s.io',
    apiVersion: 'v1beta1',
    type: 'applications',
  },
  FEDERATED_APPLICATION: {
    apiGroup: 'types.kubefed.io',
    apiVersion: 'v1beta1',
    type: 'federatedapplications',
  },
  APPLICATION_HISTORY: {
    apiGroup: 'app.k8s.io',
    apiVersion: 'v1beta1',
    type: 'applicationhistories',
  },
  CLUSTER: {
    apiGroup: 'clusterregistry.k8s.io',
    apiVersion: 'v1alpha1',
    type: 'clusters',
  },
  CLUSTER_FED: {
    apiGroup: 'infrastructure.alauda.io',
    apiVersion: 'v1alpha1',
    type: 'clusterfeds',
  },
  DEPLOYMENT: {
    apiGroup: 'apps',
    type: 'deployments',
  },
  DAEMONSET: {
    apiGroup: 'apps',
    type: 'daemonsets',
  },
  STATEFULSET: {
    apiGroup: 'apps',
    type: 'statefulsets',
  },
  TAPP: {
    apiGroup: 'apps.tkestack.io',
    type: 'tapps',
    apiVersion: 'v1',
  },
  DOMAIN: {
    apiGroup: CRD_GROUP,
    apiVersion: 'v2',
    type: 'domains',
  },
  FEATURE: {
    apiGroup: 'infrastructure.alauda.io',
    apiVersion: 'v1alpha1',
    type: 'features',
  },
  ALAUDA_FEATURE_GATE: {
    apiGroup: 'alauda.io',
    apiVersion: 'v1',
    type: 'alaudafeaturegates',
  },
  CLUSTER_ALAUDA_FEATURE_GATE: {
    apiGroup: 'alauda.io',
    apiVersion: 'v1',
    type: 'clusteralaudafeaturegates',
  },
  FRONTEND: {
    apiGroup: CRD_GROUP,
    type: 'frontends',
  },
  HORIZONTAL_POD_AUTOSCALER: {
    apiGroup: 'autoscaling',
    type: 'horizontalpodautoscalers',
  },
  LOG: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'logs',
  },
  PROJECT: {
    apiGroup: AUTH_GROUP,
    type: 'projects',
  },
  PROMETHEUS_RULE: {
    apiGroup: 'monitoring.coreos.com',
    apiVersion: 'v1',
    type: 'prometheusrules',
  },
  RULE: {
    apiGroup: CRD_GROUP,
    type: 'rules',
  },
  SELF_SUBJECT_ACCESS_REVIEW: {
    apiGroup: 'authorization.k8s.io',
    type: 'selfsubjectaccessreviews',
  },
  VIEW: {
    apiGroup: AUTH_GROUP,
    type: 'views',
  },
  POD: {
    type: 'pods',
  },
  PODS_EXEC: {
    type: 'pods/exec',
  },
  PODS_ROOT_EXEC: {
    type: 'pods/root-exec',
  },
  SERVICE: {
    type: 'services',
  },
  LIMIT_RANGE: {
    type: 'limitranges',
  },
  SECRET: {
    type: 'secrets',
  },
  PRODUCT: {
    type: 'alaudaproducts',
    apiGroup: 'portal.alauda.io',
    apiVersion: 'v1alpha1',
  },
  CRON_JOB: {
    apiGroup: 'batch',
    apiVersion: 'v1beta1',
    type: 'cronjobs',
  },
  JOB: {
    apiGroup: 'batch',
    type: 'jobs',
  },
  CONFIG_MAP: {
    type: 'configmaps',
  },
  INGRESS: {
    apiGroup: 'extensions',
    apiVersion: 'v1beta1',
    type: 'ingresses',
  },
  PVC: {
    type: 'persistentvolumeclaims',
  },
  STORAGE_CLASS: {
    type: 'storageclasses',
    apiGroup: 'storage.k8s.io',
  },
  NAMESPACE: {
    type: 'namespaces',
  },
  NODE: {
    type: 'nodes',
  },
  NODE_METRICS: {
    apiGroup: 'metrics.k8s.io',
    apiVersion: 'v1beta1',
    type: 'nodes',
  },
  NOTIFICATION: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notifications',
  },
  NOTIFICATION_SERVER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationservers',
  },
  NOTIFICATION_SENDER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationsenders',
  },
  NOTIFICATION_RECEIVER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationreceivers',
  },
  NOTIFICATION_TEMPLATE: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationtemplates',
  },
  PV: {
    type: 'persistentvolumes',
  },
  CUSTOM_RESOURCE_DEFINITION: {
    type: 'customresourcedefinitions',
    apiGroup: 'apiextensions.k8s.io',
    apiVersion: 'v1beta1',
  },
  SUBNET_KUBE_OVN: {
    type: 'subnets',
    apiGroup: 'kubeovn.io',
  },
  SUBNET_KUBE_OVN_IP: {
    type: 'ips',
    apiGroup: 'kubeovn.io',
  },
  TRACE_JOB: {
    apiGroup: 'tracer.alauda.io',
    apiVersion: 'v1beta1',
    type: 'tracejobs',
  },
  TRACE_JOB_RECORD: {
    type: 'tracejobreports',
    apiGroup: 'tracer.alauda.io',
    apiVersion: 'v1beta1',
  },
  POOL: {
    type: 'pools',
    apiGroup: 'galaxy.k8s.io',
    apiVersion: 'v1alpha1',
  },
  CHART_REPO: {
    type: 'chartrepos',
    apiGroup: APP_GROUP,
    apiVersion: 'v1beta1',
  },
  CHART: {
    type: 'charts',
    apiGroup: APP_GROUP,
    apiVersion: 'v1alpha1',
  },
  HELM_REQUEST: {
    type: 'helmrequests',
    apiGroup: APP_GROUP,
    apiVersion: 'v1alpha1',
  },
  ROUTE: {
    type: 'routes',
    apiGroup: 'route.openshift.io',
  },
  USER_BINDING: {
    apiGroup: AUTH_GROUP,
    type: 'userbindings',
  },
  NETWORK_POLICY: {
    apiGroup: 'networking.k8s.io',
    type: 'networkpolicies',
  },
  TKE_CLUSTER: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clusters',
  },
  MACHINE: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'machines',
  },
  CLUSTER_CREDENTIAL: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clustercredentials',
  },
  CLUSTERADDONTYPE: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clusteraddontypes',
  },
  ADDON: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'addons',
  },
  SERVICE_ACCOUNT: {
    type: 'serviceaccounts',
  },
  REPLICA_SET: {
    apiGroup: 'apps',
    type: 'replicasets',
  },
  POD_SECURITY_POLICY: {
    apiGroup: 'policy',
    apiVersion: 'v1beta1',
    type: 'podsecuritypolicies',
  },
  NAMESPACE_OVERVIEW: {
    apiGroup: 'k8s.io',
    type: 'namespaceoverviews',
  },
  CSP: {
    apiGroup: 'product.alauda.io',
    type: 'csps',
  },
});

export const RESOURCE_DEFINITIONS = _.RESOURCE_DEFINITIONS;
export const RESOURCE_TYPES = _.RESOURCE_TYPES;
export const getYamlApiVersion = _.getYamlApiVersion;

export type ResourceType = keyof typeof RESOURCE_TYPES;

export type WorkloadType =
  | typeof RESOURCE_TYPES.DEPLOYMENT
  | typeof RESOURCE_TYPES.DAEMONSET
  | typeof RESOURCE_TYPES.STATEFULSET
  | typeof RESOURCE_TYPES.TAPP
  | typeof RESOURCE_TYPES.CRON_JOB;
