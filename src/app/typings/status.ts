import { StringMap } from '@alauda/common-snippet';

export enum ResourceStatus {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Running = 'Running',
  Pending = 'Pending',
  Unknown = 'Unknown',
}

export enum GenericStatusIcon {
  Check = 'check_circle_s',
  Running = 'basic:running_circle_s',
  Error = 'basic:close_circle_s',
  Stopped = 'basic:stop_circle_s',
  Pending = 'basic:sync_circle_s',
  Question = 'basic:question_circle_s',
  Exclamation = 'exclamation_circle_s',
  Warning = 'exclamation_triangle_s',
  Deleting = 'basic:trash_s',
  Invalid = 'basic:minus_circle_s',
}

// http://confluence.alaudatech.com/pages/viewpage.action?pageId=48727086
export enum GenericStatusColor {
  Running = '#0ABF5B',
  Failed = '#E54545',
  Stopped = '#999',
  Empty = '#ccc',
  Pending = '#006EFF',
  Succeeded = '#1BA4B3',
  Warning = '#ff9d00',
}

export const DefaultStatusColorMapper: StringMap = {
  success: GenericStatusColor.Running,
  running: GenericStatusIcon.Running,
  error: GenericStatusColor.Failed,
  stopped: GenericStatusColor.Stopped,
  pending: GenericStatusColor.Pending,
  info: GenericStatusColor.Empty,
};

export const DefaultStatusIconMapper: StringMap = {
  success: GenericStatusIcon.Check,
  running: GenericStatusIcon.Running,
  error: GenericStatusIcon.Error,
  stopped: GenericStatusIcon.Stopped,
  pending: GenericStatusIcon.Pending,
  info: GenericStatusIcon.Question,
};

export enum AlarmStatusEnum {
  normal = 'normal',
  pending = 'pending',
  firing = 'firing',
}

// http://confluence.alaudatech.com/pages/viewpage.action?pageId=50824334
export enum PodStatusEnum {
  running = 'running',
  pending = 'pending',
  completed = 'completed',
  error = 'error',
}

export const PodStatusColorMapper = {
  [PodStatusEnum.running]: GenericStatusColor.Running,
  [PodStatusEnum.completed]: GenericStatusColor.Succeeded,
  [PodStatusEnum.error]: GenericStatusColor.Failed,
};

export const PodStatusIconMapper = {
  [PodStatusEnum.completed]: GenericStatusIcon.Check,
  [PodStatusEnum.error]: GenericStatusIcon.Error,
};

export enum JobStatusEnum {
  EXECUTING = 'executing',
  EXECUTE_SUCCEEDED = 'execute_succeeded',
  EXECUTE_FAILED = 'execute_failed',
  UNKNOWN = 'unknown', // 比如 namespace pod 数量达到限额上限，任务记录就会只触发，但是没有状态
}

export const JobStatusColorMapper = {
  [JobStatusEnum.EXECUTING]: GenericStatusColor.Pending,
  [JobStatusEnum.EXECUTE_SUCCEEDED]: GenericStatusColor.Succeeded,
  [JobStatusEnum.EXECUTE_FAILED]: GenericStatusColor.Failed,
  [JobStatusEnum.UNKNOWN]: GenericStatusColor.Empty,
};

export const JobStatusIconMapper = {
  [JobStatusEnum.EXECUTING]: GenericStatusIcon.Pending,
  [JobStatusEnum.EXECUTE_SUCCEEDED]: GenericStatusIcon.Check,
  [JobStatusEnum.EXECUTE_FAILED]: GenericStatusIcon.Error,
  [JobStatusEnum.UNKNOWN]: GenericStatusIcon.Question,
};

export enum WorkloadStatusEnum {
  running = 'running',
  stopped = 'stopped',
  pending = 'pending',
  killed = 'killed',
}

export const WorkloadStatusColorMapper = {
  [WorkloadStatusEnum.running]: GenericStatusColor.Running,
  [WorkloadStatusEnum.pending]: GenericStatusColor.Pending,
  [WorkloadStatusEnum.stopped]: GenericStatusColor.Stopped,
  [WorkloadStatusEnum.killed]: GenericStatusColor.Stopped,
};

export const WorkloadStatusIconMapper = {
  [WorkloadStatusEnum.running]: GenericStatusIcon.Running,
  [WorkloadStatusEnum.pending]: GenericStatusIcon.Pending,
  [WorkloadStatusEnum.stopped]: GenericStatusIcon.Stopped,
  [WorkloadStatusEnum.killed]: GenericStatusIcon.Stopped,
};

export enum ApplicationStatusEnum {
  RUNNING = 'running',
  PARTIAL_RUNNING = 'partial_running',
  STOPPED = 'stopped',
  PENDING = 'pending',
  FAILED = 'failed',
  NO_WORKLOAD = 'no_workload',
  UNKNOWN = 'unknown',
}

// http://confluence.alaudatech.com/pages/viewpage.action?pageId=48727086
export const ApplicationStatusColorMapper = {
  [ApplicationStatusEnum.RUNNING]: GenericStatusColor.Running,
  [ApplicationStatusEnum.PARTIAL_RUNNING]: GenericStatusColor.Running,
  [ApplicationStatusEnum.STOPPED]: GenericStatusColor.Stopped,
  [ApplicationStatusEnum.PENDING]: GenericStatusColor.Pending,
  [ApplicationStatusEnum.FAILED]: GenericStatusColor.Failed,
  [ApplicationStatusEnum.NO_WORKLOAD]: GenericStatusColor.Empty,
  [ApplicationStatusEnum.UNKNOWN]: GenericStatusColor.Empty,
};

export const ApplicationStatusIconMapper = {
  [ApplicationStatusEnum.RUNNING]: GenericStatusIcon.Running,
  [ApplicationStatusEnum.PARTIAL_RUNNING]: GenericStatusIcon.Running,
  [ApplicationStatusEnum.STOPPED]: GenericStatusIcon.Stopped,
  [ApplicationStatusEnum.PENDING]: GenericStatusIcon.Pending,
  [ApplicationStatusEnum.FAILED]: GenericStatusIcon.Error,
  [ApplicationStatusEnum.NO_WORKLOAD]: 'basic:minus_circle_s',
  [ApplicationStatusEnum.UNKNOWN]: GenericStatusIcon.Question,
};

export enum PvcStatusEnum {
  matching = 'matching',
  bound = 'bound',
  invalid = 'invalid',
}

export const PvcStatusColorMapper = {
  [PvcStatusEnum.matching]: GenericStatusColor.Pending,
  [PvcStatusEnum.bound]: GenericStatusColor.Running,
  [PvcStatusEnum.invalid]: GenericStatusColor.Empty,
};

export const PvcStatusIconMapper = {
  [PvcStatusEnum.matching]: GenericStatusIcon.Pending,
  [PvcStatusEnum.bound]: 'basic:link_circle_s',
  [PvcStatusEnum.invalid]: GenericStatusIcon.Invalid,
};

export enum PvStatusEnum {
  AVAILABLE = 'available',
  BOUND = 'bound',
  WAITING_RECYCLE = 'waiting_recycle',
  RECYCLE_FAILED = 'recycle_failed',
}

export const PvStatusColorMapper = {
  [PvStatusEnum.AVAILABLE]: GenericStatusColor.Succeeded,
  [PvStatusEnum.BOUND]: GenericStatusColor.Running,
  [PvStatusEnum.WAITING_RECYCLE]: GenericStatusColor.Warning,
  [PvStatusEnum.RECYCLE_FAILED]: GenericStatusColor.Failed,
};

export const PvStatusIconMapper = {
  [PvStatusEnum.AVAILABLE]: GenericStatusIcon.Check,
  [PvStatusEnum.BOUND]: 'basic:link_circle_s',
  [PvStatusEnum.WAITING_RECYCLE]: 'basic:trash_s',
  [PvStatusEnum.RECYCLE_FAILED]: GenericStatusIcon.Error,
};

export enum HelmRequestStatusEnum {
  DEPLOY_SUCCEEDED = 'deploy_succeeded',
  DEPLOY_FAILED = 'deploy_failed',
  DEPLOYING = 'deploying',
}

export const HelmRequestStatusColorMapper = {
  [HelmRequestStatusEnum.DEPLOY_SUCCEEDED]: GenericStatusColor.Succeeded,
  [HelmRequestStatusEnum.DEPLOY_FAILED]: GenericStatusColor.Failed,
  [HelmRequestStatusEnum.DEPLOYING]: GenericStatusColor.Pending,
};

export const HelmRequestStatusIconMapper = {
  [HelmRequestStatusEnum.DEPLOY_SUCCEEDED]: GenericStatusIcon.Check,
  [HelmRequestStatusEnum.DEPLOY_FAILED]: GenericStatusIcon.Error,
  [HelmRequestStatusEnum.DEPLOYING]: GenericStatusIcon.Pending,
};

export enum TraceJobStatusEnum {
  SUCCESS = 'Success',
  FAILED = 'Failed',
  RUNNING = 'Running',
}

export const TraceJobStatusColorMapper = {
  [TraceJobStatusEnum.SUCCESS]: GenericStatusColor.Succeeded,
  [TraceJobStatusEnum.FAILED]: GenericStatusColor.Failed,
  [TraceJobStatusEnum.RUNNING]: GenericStatusColor.Pending,
};

export const TraceJobStatusIconMapper = {
  [TraceJobStatusEnum.SUCCESS]: GenericStatusIcon.Check,
  [TraceJobStatusEnum.FAILED]: GenericStatusIcon.Error,
  [TraceJobStatusEnum.RUNNING]: GenericStatusIcon.Pending,
};
