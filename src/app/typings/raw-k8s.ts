// tslint:disable: no-commented-code
import {
  KubernetesResource,
  ObjectMeta,
  StringMap,
  TypeMeta,
} from '@alauda/common-snippet';

import { RuleIndicator } from './k8s-form-model';
import { RESOURCE_TYPES, getYamlApiVersion } from './k8s-resource-definitions';
import { WorkloadStatusEnum } from './status';

export interface CrossVersionObjectReference {
  apiVersion: string;
  kind: string;
  name: string;
}

export interface LabelSelector {
  matchLabels?: StringMap;
  matchExpressions?: LabelSelectorRequirement[];
}

export interface LabelSelectorRequirement {
  key: string;
  operator?: string;
  values?: string[];
}

export interface HorizontalPodAutoscaler extends KubernetesResource {
  spec: HorizontalPodAutoscalerSpec;
  status?: any;
}
export interface CronHorizontalPodAutoscaler extends KubernetesResource {
  spec: CronHorizontalPodAutoscalerSpec;
}

export interface HorizontalPodAutoscalerSpec {
  maxReplicas?: number;
  minReplicas?: number;
  targetCPUUtilizationPercentage?: string;
  scaleTargetRef?: CrossVersionObjectReference;
  metrics?: MetricSpec[]; // appears in v2beta2
}

export interface CronHorizontalPodAutoscalerSpec {
  crons: CronHorizontalPodAutoscalerRule[];
  scaleTargetRef?: CrossVersionObjectReference;
}

export interface CronHorizontalPodAutoscalerRule {
  schedule: string;
  targetReplicas: number;
}

export interface MetricSpec {
  external?: ExternalMetricSource;
  object?: ObjectMetricSource;
  resource?: ResourceMetricSource;
  pods?: PodsMetricSource;
  type: string;
}

export interface ExternalMetricSource {
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface ObjectMetricSource {
  describedObject?: CrossVersionObjectReference;
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface PodsMetricSource {
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface ResourceMetricSource {
  name: string;
  target?: MetricTarget;
}

export interface MetricIdentifier {
  name: string;
  selector?: LabelSelector;
}

export interface MetricTarget {
  averageUtilization?: number;
  averageValue?: string;
  type: string;
  value?: string;
}

/**
 * Workload API
 */
export interface Deployment extends KubernetesResource {
  spec?: DeploymentSpec;
  status?: DeploymentStatus;
  generation?: number;
}

// DeploymentSpec is the specification of the desired behavior of the Deployment.
export interface DeploymentSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  strategy?: DeploymentStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export interface DeploymentStatus {
  availableReplicas?: number;
  collisionCount?: number;
  observedGeneration?: number;
  readyReplicas?: number;
  replicas?: number;
  unavailableReplicas?: number;
  updatedReplicas?: number;
  conditions?: DeploymentCondition[];
}

export interface DeploymentCondition {
  lastProbeTime?: string;
  lastTransitionTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface DaemonSet extends KubernetesResource {
  spec?: DaemonSetSpec;
  status?: DaemonSetStatus;
}

export interface DaemonSetSpec {
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  updateStrategy?: DaemonSetUpdateStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export type DaemonSetStatus = Partial<{
  currentNumberScheduled: number;
  numberMisscheduled: number;
  desiredNumberScheduled: number;
  numberReady: number;
  observedGeneration: number;
  updatedNumberScheduled: number;
  numberUnavailable: number;
  numberAvailable: number;
}>;

export interface StatefulSet extends KubernetesResource {
  spec?: StatefulSetSpec;
  status?: StatefulSetStatus;
}

export interface StatefulSetSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;

  // serviceName is the name of the service that governs this StatefulSet.
  // This service must exist before the StatefulSet, and is responsible for
  // the network identity of the set. Pods get DNS/hostnames that follow the
  // pattern: pod-specific-string.serviceName.default.svc.cluster.local
  // where "pod-specific-string" is managed by the StatefulSet controller.
  serviceName?: string;
  updateStrategy?: StatefulSetUpdateStrategy;
  revisionHistoryLimit?: number;
}

export type StatefulSetStatus = Partial<{
  collisionCount: number;
  observedGeneration: number;
  replicas: number;
  readyReplicas: number;
  currentReplicas: number;
  currentRevision: string;
  updateRevision: string;
}>;

export type DeploymentStrategyType = 'RollingUpdate' | 'Recreate';
export type DaemonSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';
export type StatefulSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';

export interface DeploymentStrategy {
  type?: DeploymentStrategyType;
  rollingUpdate?: RollingUpdateDeployment;
}

export interface DaemonSetUpdateStrategy {
  type?: DaemonSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateDaemonSet;
}

export interface StatefulSetUpdateStrategy {
  type?: StatefulSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateStatefulSetStrategy;
}

export interface RollingUpdateDeployment {
  maxUnavailable?: string | number;
  maxSurge?: string | number;
}

export interface RollingUpdateDaemonSet {
  maxUnavailable?: string | number;
}

// Partition indicates the ordinal at which the StatefulSet should be
// partitioned.
// Default value is 0.
export interface RollingUpdateStatefulSetStrategy {
  partition?: string | number;
}

export interface PodTemplateSpec {
  metadata?: ObjectMeta;
  spec?: PodSpec;
}

export interface PodSpec {
  containers?: Container[];
  initContainers?: Container[];
  volumes?: Volume[];
  nodeSelector?: StringMap;
  hostNetwork?: boolean;
  affinity?: PodSpecAffinity;
  restartPolicy?: string;
  imagePullSecrets?: LocalObjectReference[];
  serviceAccountName?: string;
  serviceAccount?: string;
  nodeName?: string;
}

export interface PodSpecAffinity {
  podAffinity?: Affinity;
  podAntiAffinity?: Affinity;
  nodeAffinity?: Affinity;
}

export interface Affinity {
  requiredDuringSchedulingIgnoredDuringExecution?: PodAffinityTerm[];
  preferredDuringSchedulingIgnoredDuringExecution?: WeightedPodAffinityTerm[];
}

export interface WeightedPodAffinityTerm {
  podAffinityTerm: PodAffinityTerm;
  weight: number;
}

export interface PodAffinityTerm {
  labelSelector: LabelSelector;
  topologyKey: string;
  namespaces?: string[];
}

export interface Pod extends KubernetesResource {
  spec?: PodSpec;
  status?: PodStatus;
}

export interface PodStatus {
  conditions?: PodCondition[];
  containerStatuses?: ContainerStatus[];
  hostIP?: string;
  initContainerStatuses?: ContainerStatus[];
  message?: string;
  nominatedNodeName?: string;
  phase?: string;
  podIP?: string;
  qosClass?: string;
  reason?: string;
  startTime?: string;
}

export interface PodCondition {
  lastProbeTime?: string;
  lastTransitionTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface ContainerStatus {
  containerID?: string;
  image?: string;
  imageID?: string;
  lastState?: ContainerState;
  name?: string;
  ready?: boolean;
  restartCount?: number;
  state?: ContainerState;
}

export interface ContainerState {
  running?: ContainerStateRunning;
  terminated?: ContainerStateTerminated;
  waiting?: ContainerStateWaiting;
}

export interface ContainerStateRunning {
  startedAt?: string;
}

export interface ContainerStateTerminated {
  containerID?: string;
  exitCode?: number;
  finishedAt?: string;
  message?: string;
  reason?: string;
  signal?: number;
  startedAt?: string;
}

export interface ContainerStateWaiting {
  message?: string;
  reason?: string;
}

export enum VolumeTypeEnum {
  persistentVolumeClaim = 'persistentVolumeClaim',
  configMap = 'configMap',
  secret = 'secret',
  hostPath = 'hostPath',
  emptyDir = 'emptyDir',
}

export interface Volume {
  name: string;
  [VolumeTypeEnum.emptyDir]?: EmptyDirVolumeSource;
  [VolumeTypeEnum.configMap]?: ConfigMapVolumeSource;
  [VolumeTypeEnum.secret]?: SecretVolumeSource;
  [VolumeTypeEnum.persistentVolumeClaim]?: PersistentVolumeClaimVolumeSource;
  [VolumeTypeEnum.hostPath]?: HostPathVolumeSource;
  // other iaas storage definitions not included
}

export interface KeyToPath {
  key: string;
  mode?: number;
  path: string;
}

export interface EmptyDirVolumeSource {
  medium?: string;
  sizeLimit?: string; // eg: 1500m refers to: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#quantity-resource-core
}

export interface ConfigMapVolumeSource {
  name: string;
  optional?: boolean;
  items?: KeyToPath[];
}

export interface SecretVolumeSource {
  secretName: string;
  optional?: boolean;
  items?: KeyToPath[];
}

export interface HostPathVolumeSource {
  type?: string;
  path: string;
}

export interface PersistentVolumeClaimVolumeSource {
  claimName: string;
  readOnly?: boolean;
}

/**
 * Config And Storage API
 */

export interface StorageClass extends KubernetesResource {
  mountOptions?: string[];
  parameters?: StorageClassParameter;
  provisioner?: string;
  reclaimPolicy?: string;
  volumeBindingMode?: string;
}
export interface StorageClassParameter {
  resturl?: string;
  volumetype?: string;
  // CephFs driver parameter
  adminId?: string;
  adminSecretName?: string;
  adminSecretNamespace?: string;
  claimRoot?: string;
  monitors?: string;

  // CSP driver parameters
  cluster?: string;
  pool?: string;
  clientIP?: string;
  controllerAddress?: string;
}

export interface PersistentVolumeClaim extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeClaimSpec;
  status?: {
    phase: string;
  };
}

export interface PersistentVolumeClaimSpec {
  storageClassName?: string;
  volumeName?: string;
  resources: {
    requests: { storage: string };
  };
  selector?: {
    matchLabels: StringMap;
  };
  accessModes: string[];
}

export interface PersistentVolume extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeSpec;
  status?: {
    phase: string;
  };
}

export interface PersistentVolumeSpecMeta {
  capacity: {
    storage: string;
  };
  claimRef?: {
    apiVersion: string;
    kind: 'PersistentVolumeClaim';
    name: string;
    namespace: string;
    uid: string;
  };
  persistentVolumeReclaimPolicy: string; // 'Retain' | 'Delete';
  hostPath?: {
    path: string;
  };
  nfs?: {
    server: string;
    path: string;
  };
  cephfs?: CephFSPersistentVolumeSource;
}

export interface CephFSPersistentVolumeSource {
  monitors: string[];
  path?: string;
  secretRef: SecretReference;
  user: string;
}

export interface SecretReference {
  name: string;
  namespace: string;
}

export interface PersistentVolumeSpec extends PersistentVolumeSpecMeta {
  accessModes: string[]; // ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
}

export interface VolumeMount {
  name: string;
  mountPath?: string;
  readOnly?: boolean;
  subPath?: string;
  mountPropagation?: string;
}

export interface ResourceRequirements {
  limits?: StringMap;
  requests?: StringMap;
}

export interface Container {
  name?: string;
  image?: string;
  command?: string[];
  args?: string[];
  env?: EnvVar[];
  envFrom?: EnvFromSource[];
  workingDir?: string;
  ports?: ContainerPort[];
  resources?: ResourceRequirements;
  volumeMounts?: any;
  livenessProbe?: Probe;
  readinessProbe?: Probe;
}

export interface ContainerPort {
  name?: string;
  hostPort?: number;
  containerPort?: number;
  protocol?: string;
  hostIP?: string;
}

export interface ObjectFieldSelector {
  apiVersion?: string;
  fieldPath: string;
}

export interface ResourceFieldSelector {
  containerName?: string;
  divisor?: string;
  resource: string;
}

export interface EnvVarSource {
  configMapKeyRef?: ConfigMapKeyRef;
  secretKeyRef?: SecretKeyRef;
  fieldRef?: ObjectFieldSelector;
  resourceFieldRef?: ResourceFieldSelector;
}

export interface EnvVar {
  name: string;
  value?: string;
  valueFrom?: EnvVarSource;
}

export interface EnvFromSource {
  prefix?: string;
  configMapRef?: ConfigMapRef;
  secretRef?: SecretRef;
}

export interface Probe {
  exec?: ExecAction;
  failureThreshold?: number;
  httpGet?: HTTPGetAction;
  initialDelaySeconds?: number;
  periodSeconds?: number;
  successThreshold?: number;
  tcpSocket?: TCPSocketAction;
  timeoutSeconds?: number;
}

export interface ExecAction {
  command?: string[];
}

export interface HTTPGetAction {
  host?: string;
  httpHeaders?: HTTPHeader[];
  path: string;
  port?: number | string;
  scheme?: string;
}

export interface HTTPHeader {
  name?: string;
  value?: string;
}

export interface TCPSocketAction {
  host?: string;
  port?: number | string;
}

// tslint:disable-next-line:no-empty-interface
export interface Namespace extends KubernetesResource {
  status?: NamespaceStatus;
}

export interface NamespaceStatus {
  phase?: string;
}

export interface LocalObjectReference {
  name: string;
}

export interface ConfigMapKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface SecretKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface ConfigMapRef extends LocalObjectReference {
  optional?: boolean;
}

export interface SecretRef extends LocalObjectReference {
  optional?: boolean;
}

export interface IngressSpec {
  backend?: IngressBackend;
  rules?: IngressRule[];
  tls?: IngressTLS[];
}

export interface IngressRule {
  host?: string;
  http: HTTPIngressRuleValue;
}
export interface HTTPIngressRuleValue {
  paths?: HTTPIngressPath[];
}

export interface HTTPIngressPath {
  path: string;
  backend: IngressBackend;
}

export interface IngressBackend {
  serviceName?: string;
  servicePort?: string | number;
}

export interface IngressTLS {
  hosts?: string[];
  secretName?: string;
}

export interface IngressStatus {
  loadBalancer?: IngressLoadBalancerStatus;
}

export interface IngressLoadBalancerStatus {
  ingress?: LoadBalancerIngress[];
}

export interface LoadBalancerIngress {
  ip: string;
  hostname: string;
}

export interface LimitRange extends KubernetesResource {
  spec?: LimitRangeSpec;
}

export type LimitType = 'Container' | 'Pod' | 'PersistentVolumeClaim';

export interface LimitRangeItem {
  type: LimitType;
  min?: StringMap;
  max?: StringMap;
  default?: StringMap;
  defaultRequest?: StringMap;
  maxLimitRequestRatio?: StringMap;
}

export interface LimitRangeSpec extends KubernetesResource {
  limits: LimitRangeItem[];
}

export type ResourceQuotaScope =
  | 'Terminating'
  | 'NotTerminating'
  | 'BestEffort'
  | 'NotBestEffort';

export interface ResourceQuota extends KubernetesResource {
  spec?: ResourceQuotaSpec;
}

export interface ResourceQuotaItem {
  pods?: string;
  'limits.cpu'?: string;
  'requests.cpu'?: string;
  'limits.memory'?: string;
  'requests.memory'?: string;
  'requests.storage'?: string;
  persistentvolumeclaims?: string;
}

export interface ResourceQuotaSpec {
  hard?: StringMap;
  scopes?: ResourceQuotaScope[];
}

export interface Service extends KubernetesResource {
  spec?: ServiceSpec;
  status?: any;
}

export const SERVICE_TYPES = ['ClusterIP', 'NodePort'] as const;
export const SERVICE_SESSION_AFFINITIES = ['None', 'ClientIP'] as const;

export type ServiceType = typeof SERVICE_TYPES[number];
export type ServiceSessionAffinity = typeof SERVICE_SESSION_AFFINITIES[number];

export interface ServiceSpec {
  clusterIP?: string;
  externalIPs?: string[];
  externalName?: string;
  ports?: ServicePort[];
  selector?: StringMap;
  type?: ServiceType;
  sessionAffinity?: ServiceSessionAffinity;
}

export const SERVICE_PORT_BASE_PROTOCOLS = [
  'TCP',
  'UDP', // not supported for now
] as const;
export type ServicePortBaseProtocol = typeof SERVICE_PORT_BASE_PROTOCOLS[number];

export const SERVICE_PORT_PROTOCOLS = [
  'TCP',
  'UDP',
  'HTTP',
  'HTTPS',
  'HTTP2',
  'gRPC',
] as const;
export type ServicePortProtocol = typeof SERVICE_PORT_PROTOCOLS[number];

export interface ServicePort {
  name?: string;
  nodePort?: number;
  port?: number;
  protocol?: ServicePortBaseProtocol;
  targetPort?: number | string;
}

export interface ConfigMap extends KubernetesResource {
  data?: StringMap;
  binaryData?: StringMap;
}

export interface Secret extends KubernetesResource {
  data?: StringMap;
  stringData?: StringMap;
  type?: SecretType;
}

export interface POOL extends KubernetesResource {
  size?: number;
}

export enum SecretType {
  Opaque = 'Opaque',
  TLS = 'kubernetes.io/tls',
  SSHAuth = 'kubernetes.io/ssh-auth',
  BasicAuth = 'kubernetes.io/basic-auth',
  DockerConfigJson = 'kubernetes.io/dockerconfigjson',
  NotificationSender = 'NotificationSender',
}

export interface AlternateBackend {
  kind: string;
  name: string;
  weight: number;
}

export interface RouteSpec {
  host?: string;
  path?: string;
  port?: {
    targetPort: string;
  };
  to?: AlternateBackend;
  wildcardPolicy?: string;
  alternateBackends?: AlternateBackend[];
}

export interface RouteIngress {
  status: string;
  type: string;
  host: string;
  routerName: string;
  wildcardPolicy: string;
}

export interface RouteStatus {
  ingress: RouteIngress[];
}

export interface Route extends KubernetesResource {
  spec?: RouteSpec;
  status?: RouteStatus;
}

export interface Application extends KubernetesResource {
  spec: ApplicationSpec;
  status: ApplicationStatus;
}

export interface ApplicationSpec {
  componentKinds: GroupKind[];
  assemblyPhase?: ApplicationPhaseEnum;
  selector?: LabelSelector;
  descriptor?: any;
}

export enum ApplicationStateEnum {
  Pending = 'Pending',
  PartialRunning = 'PartialRunning',
  Running = 'Running',
  Stopped = 'Stopped',
  Empty = 'Empty',
}

export enum ApplicationPhaseEnum {
  Succeeded = 'Succeeded',
  Failed = 'Failed',
  Pending = 'Pending',
}

export interface ApplicationStatus {
  state: ApplicationStateEnum;
  totalComponents: number;
  conditions?: ApplicationCondition[];
  workloadsStatus?: {
    pending?: number;
    ready?: number;
    stopped?: number;
    workloads?: WorkloadStatus[];
  };
}

export interface GenericWorkloadStatus {
  status?: WorkloadStatusEnum;
  desired?: number;
  current?: number;
}

export interface WorkloadStatus extends GenericWorkloadStatus {
  name?: string;
  group?: string;
  kind?: string;
  messages?: Array<{
    message: string;
    reason?: string;
  }>;
}

export type ApplicationHistoryDiff = Record<
  'create' | 'update' | 'delete',
  Array<{
    kind?: string;
    name?: string;
  }>
>;

export interface ApplicationHistory extends KubernetesResource {
  spec: ApplicationHistorySpec;
}

export interface ApplicationHistorySpec {
  revision?: number;
  yaml?: string;
  creationTimestamp?: string;
  user?: string;
  changeCause?: string;
  // compare with latest versions
  resourceDiffsWithLatest?: ApplicationHistoryDiff;
  resourceDiffs: ApplicationHistoryDiff;
  latestRevision?: number;
}

export interface ApplicationCondition {
  lastTransitionTime?: string;
  lastUpdateTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface GroupKind {
  kind: string;
  group: string;
}

export const ApplicationTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.APPLICATION),
  kind: 'Application',
};

export const DeploymentTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.DEPLOYMENT),
  kind: 'Deployment',
};

export const DaemonSetTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.DAEMONSET),
  kind: 'DaemonSet',
};

export const StatefulSetTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.STATEFULSET),
  kind: 'StatefulSet',
};

export const ConfigMapTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CONFIG_MAP),
  kind: 'ConfigMap',
};

export const SecretTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.SECRET),
  kind: 'Secret',
};

export const ServiceTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.SERVICE),
  kind: 'Service',
};

export const HorizontalPodAutoscalerMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.HORIZONTAL_POD_AUTOSCALER),
  kind: 'HorizontalPodAutoscaler',
};

export const ApplicationHistoryMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.APPLICATION_HISTORY),
  kind: 'ApplicationRollback',
};

export const NotificationMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION),
  kind: 'Notification',
};

export const NotificationServerMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_SERVER),
  kind: 'NotificationServer',
};

export const NotificationSenderMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_SENDER),
  kind: 'NotificationSender',
};

export const NotificationReceiverMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_RECEIVER),
  kind: 'NotificationReceiver',
};

export const NotificationTemplateMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_TEMPLATE),
  kind: 'NotificationTemplate',
};

export const AlertTemplateMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.ALERT_TEMPLATE),
  kind: 'AlertTemplate',
};

export const PrometheusRuleMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.PROMETHEUS_RULE),
  kind: 'PrometheusRule',
};

export const ClusterFedMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTER_FED),
  kind: 'Clusterfed',
};

export const UserBindingMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.USER_BINDING),
  kind: 'UserBinding',
};

export const TKEClusterMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.TKE_CLUSTER),
  kind: 'Cluster',
};

export const MachineMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.MACHINE),
  kind: 'Machine',
};

export const ClusterCredentialMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTER_CREDENTIAL),
  kind: 'ClusterCredential',
};

export const ClusterAddonTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTERADDONTYPE),
  kind: 'ClusterAddonType',
};

export interface Ingress extends KubernetesResource {
  spec?: IngressSpec;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobspec-v1-batch
export interface JobSpec {
  activeDeadlineSeconds?: number | '';
  backoffLimit?: number | '';
  completions?: number | '';
  manualSelector?: boolean;
  parallelism?: number | '';
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  startingDeadlineSeconds?: number | '';
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobtemplatespec-v1beta1-batch
export interface JobTemplateSpec {
  metadata?: ObjectMeta;
  spec?: JobSpec;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#cronjobspec-v1beta1-batch
export interface CronJobSpec {
  concurrencyPolicy?: string;
  failedJobsHistoryLimit?: number | '';
  schedule?: string;
  startingDeadlineSeconds?: number | '';
  successfulJobsHistoryLimit?: number | '';
  suspend?: boolean;
  jobTemplate?: JobTemplateSpec;
}

export interface ObjectReference {
  apiVersion: string;
  fieldPath: string;
  kind: string;
  name: string;
  namespace: string;
  resourceVersion: string;
  uid: string;
}

export interface CronJobStatus {
  active?: ObjectReference[];
  lastScheduleTime: string;
}

export interface CronJob extends KubernetesResource {
  spec?: CronJobSpec;
  status?: CronJobStatus;
}

export interface JobCondition {
  lastProbeTime: string;
  lastTransitionTime: string;
  message: string;
  reason: string;
  status: string;
  type: string;
}

export interface JobStatus {
  active: number | '';
  completionTime: string;
  conditions: JobCondition[];
  startTime: string;
  succeeded: number;
  failed: number;
}

export interface Job extends KubernetesResource {
  spec?: JobSpec;
  status?: JobStatus;
}

// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IPBlock {
  cidr: string;
  except: string[];
}

export interface NetworkPolicyPort {
  port: number;
  protocol: string;
}

export interface NetworkPolicyPeer {
  ipBlock?: IPBlock;
  namespaceSelector?: LabelSelector;
  podSelector?: LabelSelector;
}

export interface NetworkPolicyEgressRule {
  ports?: NetworkPolicyPort[];
  to?: NetworkPolicyPeer[];
}

export interface NetworkPolicyIngressRule {
  from?: NetworkPolicyPeer[];
  ports?: NetworkPolicyPort[];
}

export interface NetworkPolicySpec {
  egress?: NetworkPolicyEgressRule[];
  ingress?: NetworkPolicyIngressRule[];
  podSelector?: LabelSelector;
  policyTypes?: string[];
}

export interface NetworkPolicy extends KubernetesResource {
  spec?: NetworkPolicySpec;
}

export interface ClusterSpec {
  authInfo?: {
    controller: {
      kind: string;
      namespace: string;
      name: string;
    };
  };
  kubernetesApiEndpoints?: {
    caBundle?: string;
    serverEndpoints: Array<{
      clientCIDR: string;
      serverAddress: string;
    }>;
  };
}

export type ClusterStatusReason =
  | 'Timeout'
  | 'Forbidden'
  | 'Unauthorized'
  | 'SomeComponentsUnhealthy'
  | 'SomeNodesNotReady';

export interface ClusterStatus {
  version: string;
  conditions: [
    {
      lastTransitionTime: string;
      type: 'NotReachable' | 'NotAccessible' | 'ComponentNotHealthy';
      status: 'True' | 'False';
      reason: ClusterStatusReason;
      message: string;
    },
  ];
}

export interface Cluster extends KubernetesResource {
  metadata?: ObjectMeta & {
    finalizers?: string[];
  };
  spec?: ClusterSpec;
  status?: ClusterStatus;
  details?: {
    group: string;
    kind: string;
    name: string;
    uid: string;
  };
}

export type NodeStatusReason =
  | 'KubeletHasSufficientDisk'
  | 'KubeletHasSufficientMemory'
  | 'KubeletHasNoDiskPressure'
  | 'KubeletReady';

export type NodeStatusType =
  | 'OutOfDisk'
  | 'MemoryPressure'
  | 'DiskPressure'
  | 'Ready';

export interface NodeStatus {
  volumesAttached: boolean;
  daemonEndpoints: {
    kubeletEndpoint: {
      port: number;
    };
  };
  capacity: {
    'alpha.kubernetes.io/nvidia-gpu': string;
    'tencent.com/vcuda-core': string;
    'tencent.com/vcuda-memory': string;
    pods: string;
    cpu: string;
    memory: string;
  };
  addresses: Array<{
    type: string;
    address: string;
  }>;
  allocatable: {
    'alpha.kubernetes.io/nvidia-gpu': string;
    'tencent.com/vcuda-core': string;
    'tencent.com/vcuda-memory': string;
    pods: string;
    cpu: string;
    memory: string;
  };
  images: Array<{
    sizeBytes: number;
    names: string[];
  }>;
  nodeInfo: {
    kubeProxyVersion: string;
    operatingSystem: string;
    kernelVersion: string;
    systemUUID: string;
    containerRuntimeVersion: string;
    osImage: string;
    architecture: string;
    bootID: string;
    machineID: string;
    kubeletVersion: string;
  };
  volumesInUse: boolean;
  phase: string;
  conditions: Array<{
    lastHeartbeatTime: string;
    status: string;
    lastTransitionTime: string;
    reason: NodeStatusReason;
    message: string;
    type: NodeStatusType;
  }>;
}

export interface NodeTaint {
  key: string;
  timeAdded?: string;
  effect: string;
  value?: string;
  unschedulable?: boolean;
}

export interface Node extends KubernetesResource {
  spec?: {
    providerID?: string;
    taints?: NodeTaint[];
    configSource?: string;
    unschedulable?: boolean;
    podCIDR?: string;
    externalID?: string;
    ip?: string;
  };
  status?: NodeStatus;
}

export const FEATURE_RESOURCE_RATIO = 'resourceratio';

export interface ResourceRatio {
  cpu: number;
  memory: number;
}

export interface ClusterFeatureSpec {
  type: string;
  accessInfo?: {
    grafanaAdminPassword: string;
    grafanaAdminUser: string;
    grafanaUrl: string;
    name: string;
    namespace: string;
    prometheusTimeout: number;
    prometheusUrl: string;
    alertmanagerUrl: string;
  };
  instanceType?: string;
  deployInfo?: ResourceRatio;
  version?: string;
}

export interface ClusterFeature extends KubernetesResource {
  spec?: ClusterFeatureSpec;
}

export enum IPIP_MODE_TYPE {
  Never = 'Never',
  Always = 'Always',
  CrossSubnet = 'CrossSubnet',
}

export interface SubnetStatus {
  usingIPs: number;
  availableIPs: number;
}

export interface SubnetSpec {
  default: boolean; // whether default
  protocol: string; // 4 or 6,default 4
  namespaces?: string[];
  cidrBlock: string; // segment
  natOutgoing?: boolean;
  // for calico
  ipipMode?: string;
  blockSize?: number;

  // for kub-ovn
  gateway: string; // default,cidr_block's first available
  excludeIps: string[];
  gatewayType: string;
  gatewayNode?: string; // required when gateway_type is Centralized
  private?: boolean; // private network,default false
  allowSubnets?: string[]; // enable when private is true
}

export interface Subnet extends KubernetesResource {
  spec?: SubnetSpec;
  status?: SubnetStatus;
}

export interface SubnetIpSpec {
  podName: string;
  namespace: string;
  subnet: string;
  nodeName: string;
  ipAddress?: string;
  macAddress: string;
}

export interface SubnetIp extends KubernetesResource {
  spec?: SubnetIpSpec;
}

export interface LoadBalancer extends KubernetesResource {
  spec?: LoadBalancerSpec;
  status?: LoadBalancerStatus;
}

export interface LoadBalancerStatus {
  probeTime: string;
  reason: string;
  state: 'ready' | 'warning';
}

export interface LoadBalancerSpec {
  address: string;
  bind_address: string;
  domains: string[];
  iaas_id: string;
  type: string;
}

// reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=67542465
export interface TraceJob extends KubernetesResource {
  spec: TraceJobSpec;
  status?: TraceJobStatus;
}

export interface TraceJobSpec {
  schedule?: string;
  builtInProbes?: string[];
  probes?: TraceJobProbe[];
  successfulJobsHistoryLimit?: number;
  failedJobsHistoryLimit?: number;
}

export interface TraceJobProbe {
  protocol?: string;
  source?: TraceJobAddressInfo;
  destination?: TraceJobAddressInfo;
  result?: boolean;
  reason?: string;
  args?: StringMap;
}

export interface TraceJobAddressInfo {
  ip?: string;
  port?: number;
  url?: string;
  domain?: string;
  podName?: string;
  podNamespace?: string;
  nodeName?: string;
  networkMode?: string;
}

export interface TraceJobStatus {
  lastScheduleTime: string;
}

export interface TraceJobReport extends KubernetesResource {
  spec: TraceJobReportSpec;
  status: TraceJobReportStatus;
}

export interface TraceJobReportSpec {
  allAgents: AgentInfo[];
}
export interface AgentInfo {
  podName: string;
  podNamespace: string;
  podIP: string;
  nodeName: string;
  networkMode: string;
}
export interface TraceJobReportStatus {
  observedGeneration: number;
  phase: string;
  reason?: string;
  detail: TraceJobProbe[];
  startTime: string;
  reportedAgents: AgentInfo[];
  finishTime?: string;
}

export interface RuleService {
  name: string;
  namespace: string;
  port: number | '';
  weight: number;
}

export interface ServiceGroup {
  services: RuleService[];
  session_affinity_attribute: string;
  session_affinity_policy: string;
}

export interface FrontendSpec {
  port?: number | '';
  protocol?: string;
  source?: {
    name: string;
    namespace: string;
    type: string;
  };
  serviceGroup?: ServiceGroup;
  certificate_name?: string;
}

export interface FrontendStatus {
  instances: Record<
    string,
    {
      conflict: boolean;
      probe_time: string;
    }
  >;
}

// http://confluence.alauda.cn/pages/viewpage.action?pageId=27177567
// 后端对于 Frontend 的资源定义
export interface Frontend extends KubernetesResource {
  spec?: FrontendSpec;
  status?: FrontendStatus;
}

export interface RuleSpec {
  domain?: string;
  url?: string;
  dsl?: string;
  dslx?: RuleIndicator[];
  description?: string;
  certificate_name?: string;
  source?: {
    name: string;
    namespace: string;
    type: string;
  };
  serviceGroup?: ServiceGroup;
  rewrite_target?: string;
  type?: string;
  backendProtocol?: 'HTTP' | 'HTTPS';
  redirectCode?: 301 | 302 | 307;
  redirectURL?: string;
  enableCORS?: boolean;
}

export interface Rule extends KubernetesResource {
  spec?: RuleSpec;
}

export interface Project extends KubernetesResource {
  spec: ProjectSpec;
}

export interface ProjectSpec {
  clusters: ProjectCluster[];
  status?: {
    phase: string;
  };
}

export interface ProjectCluster {
  name: string;
  quota: string; // "{\"cpu\":-1,\"memory\":-1,\"pods\":-1,\"pvc_num\":-1,\"storage\":-1}"
}

export interface DomainSpec {
  name: string;
  kind: 'full' | 'extensive';
}

export interface Domain extends KubernetesResource {
  spec?: DomainSpec;
}

export interface Status {
  kind: 'Status';
  apiVersion: string;
  metadata: {
    continue?: string;
  };
  status: string;
  message: string;
  reason: string;
  code: number;
}

export interface ProductSpec {
  homepage: string;
  hide: boolean;
  index: number;
}

export interface Product extends KubernetesResource {
  kind: 'AlaudaProduct';
  spec: ProductSpec;
}

export interface TAppSpec {
  replicas?: number;
  forceDeletePod?: boolean;
  updateStrategy?: { maxUnavailable: number | string };
  template?: PodTemplateSpec;
  templatePool?: Record<string, PodTemplateSpec>;
  templates?: Record<string, string>;
  selector?: LabelSelector;
  statuses?: Record<string, string>;
}

export interface TAppStatus {
  appStatus: string;
  observedGeneration?: number;
  replicas: number;
  readyReplicas?: number;
  scaleLabelSelector?: string;
  statuses: { [key: number]: string };
}

export interface TApp extends KubernetesResource {
  spec?: TAppSpec;
  status?: TAppStatus;
}

export const CRD_FEATURE = 'features.infrastructure.alauda.io';

export interface CustomResourceDefinition extends KubernetesResource {
  kind: 'CustomResourceDefinition';
}

export interface NotificationSubscription {
  method: string;
  receivers: Array<{
    name: string;
    namespace: string;
  }>;
  sender: string;
  template: string;
}

export interface NotificationSpec {
  subscriptions: NotificationSubscription[];
}

export interface Notification extends KubernetesResource {
  kind: string;
  spec?: NotificationSpec;
}

export interface AlarmAction {
  name: string;
  namespace: string;
}

export interface AlertTemplateItem {
  name: string;
  compare: string;
  threshold: number;
  unit?: string;
  wait: number;
  notifications: AlarmAction[];
  annotations?: StringMap;
  labels?: StringMap;
  metric: {
    queries: Array<{
      aggregator: string;
      range: number;
      labels: Array<{
        name: string;
        value: string;
      }>;
    }>;
  };
  expr?: string;
  metric_name?: string;
}

export interface AlertTemplateSpec {
  templates: AlertTemplateItem[];
}

export interface AlertTemplate extends KubernetesResource {
  kind: string;
  spec?: AlertTemplateSpec;
}

// http://confluence.alaudatech.com/x/Z4YHAw
// 后端对于 PrometheusRule 的资源定义
export interface PrometheusRuleItemAnnotation extends StringMap {
  // 告警规则触发时的当前值
  alert_current_value?: string;
  // 告警关联的通知的列表
  alert_notifications?: string;
}

export interface PrometheusRuleItemLabel extends StringMap {
  // 等级
  severity: string;
  // 所属应用的名称
  application?: string;
  // 告警规则名称
  alert_name: string;
  // 所属对象的类型 Deployment/StatefulSet/DaemonSet/Node/Cluster
  alert_involved_object_kind: string;
  // 所属对象的名称
  alert_involved_object_name: string;
  // 所属对象的命名空间 可能为空字符串
  alert_involved_object_namespace: string;
  // 所属集群名称
  alert_cluster: string;
  // 所属对象的项目名称 可能为空字符串
  alert_project: string;
  // 创建人
  alert_creator?: string;
  // 指标名称 预设的告警指标/custom
  alert_indicator: string;
  // 聚合时间
  alert_indicator_aggregate_range?: string;
  // 聚合方式 max/min/avg
  alert_indicator_aggregate_function?: string;
  // 比较方式
  alert_indicator_comparison: string;
  // 阈值
  alert_indicator_threshold: string;
  // 日志告警的查询语句
  alert_indicator_query?: string;
  // 自定义告警必传 可能是空字符串
  alert_indicator_unit?: string;
  alarm_status?: string;
}

export interface PrometheusRuleItem {
  // alert是告警规则名称：<规则名称>-<一个32位MD5值>
  // 规则名称：<指标名称>-<5位随机字符串(小写字母+数字的组合)>
  // MD5值：MD5<resourceName__groupName__ruleName>，是为规则生成一个唯一的名字
  alert: string;
  // Prometheus表达式
  expr: string;
  // 持续时间
  for: string;
  annotations: PrometheusRuleItemAnnotation;
  labels: PrometheusRuleItemLabel;
}

export interface PrometheusRuleSpec {
  groups: Array<{
    // 告警规则所属的group的名称，固定为general
    name: string;
    rules: PrometheusRuleItem[];
  }>;
}

export interface PrometheusRule extends KubernetesResource {
  kind: string;
  spec?: PrometheusRuleSpec;
}

export interface Log extends KubernetesResource {
  kind: string;
  spec?: {
    AUDIT_TTL: number;
    EVENT_TTL: number;
    LOG_TTL: number;
  };
}

export interface ValueFromSecret {
  namespace: string;
  name: string;
  key: string;
}

export interface NotificationEmailServer {
  host: string;
  port: number;
  username: string;
  password: string;
  valueFromSecret: ValueFromSecret;
  ssl: boolean;
  insecureSkipVerify: boolean;
}

export interface NotificationSmsServer {
  host: string;
  port: number;
  provider: string;
  softVersion: string;
  accountSid: {
    valueFromSecret: ValueFromSecret;
  };
  accountToken: {
    valueFromSecret: ValueFromSecret;
  };
  appId: string;
}

export interface NotificationServer extends KubernetesResource {
  spec?: {
    email?: NotificationEmailServer;
    sms?: NotificationSmsServer;
  };
}

export interface NotificationReceiver extends KubernetesResource {
  spec: {
    destination: string;
  };
}

export interface NotificationTemplate extends KubernetesResource {
  spec: {
    subject?: string;
    content: string;
  };
}
// ChartRepo & Chart & HelmRequest
// http://confluence.alaudatech.com/pages/viewpage.action?pageId=50836763
export interface ChartRepo extends KubernetesResource {
  spec: ChartRepoSpec;
  status?: ChartRepoStatus;
}

export interface WorkloadStats {
  ready: number;
  pending: number;
  stopped: number;
}

export interface PodError {
  name: string;
  status: string;
  reason: string;
}

export interface NamespaceOverviewAppStats {
  total: number;
  running: number;
  partialRunning: number;
  pending: number;
  failed: number;
  empty: number;
  stopped: number;
}

export interface NamespaceOverview extends KubernetesResource {
  kind: 'NamespaceOverview';
  spec: {
    application: NamespaceOverviewAppStats;
    resourceQuota: {
      hard?: ResourceQuotaItem;
      used?: ResourceQuotaItem;
    };
    workload: {
      deployment: WorkloadStats;
      daemonset: WorkloadStats;
      statefulset: WorkloadStats;
    };
    pod: {
      stats: {
        running: number;
        pending: number;
        error: number;
        succeeded: number;
        evicted: number;
      };
      errors: PodError[];
    };
  };
}

export enum ChartRepoType {
  Chart = 'Chart',
  Git = 'Git',
  Svn = 'SVN',
}

export interface ChartRepoSpec {
  secret?: {
    name: string;
  };
  url: string;
  type?: ChartRepoType;
  source?: {
    url: string;
    path: string;
  };
}

export interface ChartRepoStatus {
  phase: ChartRepoPhaseEnum;
}

export enum ChartRepoPhaseEnum {
  Synced = 'Synced',
  Failed = 'Failed',
  Pending = 'Pending',
}

export interface ChartRepoCreate extends KubernetesResource {
  spec: ChartRepoCreateSpec;
}

export interface ChartRepoCreateSpec {
  secret?: Secret;
  chartRepo: ChartRepo;
}

export interface Chart extends KubernetesResource {
  spec: {
    appName?: string; // refers to: http://confluence.alauda.cn/pages/viewpage.action?pageId=50836763#id-[WIP]API%E8%AE%BE%E8%AE%A1-Chart%E5%88%97%E8%A1%A8
    versions?: ChartVersion[];
  };
}

export interface ChartVersion {
  name: string;
  apiVersion: string;
  appVersion: string;
  deprecated: boolean;
  created: string;
  description?: string;
  digest?: string;
  urls: string[];
  sources: string[];
  version: string;
  icon: string;
  home: string;
}

export interface HelmRequest extends KubernetesResource {
  spec?: HelmRequestSpec;
  status?: {
    phase: HelmRequestPhaseEnum;
    notes?: string;
    lastSpecHash?: string;
  };
}

export enum HelmRequestPhaseEnum {
  Synced = 'Synced',
  Failed = 'Failed',
  Pending = 'Pending',
}

export interface HelmRequestSpec {
  chart: string;
  values: Record<string, unknown>;
  valuesFrom?: Array<{
    configMapKeyRef: ConfigMapKeyRef;
  }>;
  clusterName?: string;
  namespace?: string;
  releaseName?: string;
  version?: string;
}

export interface NodeMetrics extends KubernetesResource {
  usage?: {
    cpu: string;
    memory: string;
  };
  window?: string;
}

export interface ClusterFedItem {
  name: string;
  type: string;
}

export interface ClusterFedSpec {
  clusters: ClusterFedItem[];
  unjoinpolicy?: string;
}

export interface ClusterFed extends KubernetesResource {
  kind: string;
  spec?: ClusterFedSpec;
  status?: {
    phase?: string;
  };
}

export interface TKEClusterSpecMachine {
  ip: string;
  port: number | string;
  username: string;
  password?: string;
  privateKey?: string;
  passPhrase?: string;
  labels: StringMap;
}

export interface TKEClusterSpec {
  displayName: string;
  clusterCIDR?: string;
  networkDevice?: string;
  properties?: {
    maxClusterServiceNum?: number;
    maxNodePodNum?: number;
    oversoldRatio?: {
      cpu: string;
      memory: string;
    };
  };
  features?: {
    enableMasterSchedule?: boolean;
    gpuType?: 'Physical' | 'Virtual';
    files?: Array<{
      src?: string;
      dst?: string;
    }>;
    ha?: {
      thirdParty?: {
        vip?: string;
        vport?: number;
      };
    };
  };
  type: 'Baremetal' | 'Imported';
  version?: string;
  machines?: TKEClusterSpecMachine[];
  dnsDomain?: string;
  apiServerExtraArgs?: StringMap;
}

//  http://confluence.alauda.cn/pages/viewpage.action?pageId=61907233
export interface TKEClusterStatusCondition {
  type: string;
  status: string;
  lastProbeTime: string;
  lastTransitionTime: string;
  reason: string;
}

export interface TKECluster extends KubernetesResource {
  spec?: TKEClusterSpec;
  status?: {
    version?: string;
    phase?: string;
    conditions?: TKEClusterStatusCondition[];
    addresses?: Array<{
      host: string;
      type: 'Advertise';
      port: number;
    }>;
    serviceCIDR?: string;
    nodeCIDRMaskSize?: number;
    dnsIP?: string;
  };
}

export interface TKEClusterMetadata extends ObjectMeta {
  generateName?: string;
}

export interface ClusterCredential extends KubernetesResource {
  metadata?: TKEClusterMetadata;
  clusterName: string;
  caCert: string;
  token: string;
}

export interface TKEMachineSpec {
  clusterName: string;
  ip: string;
  port: number;
  password?: string;
  privateKey?: string;
  passPhrase?: string;
  username: string;
  labels?: {
    ['nvidia-device-enable']?: string;
  };
  type: string;
}

export interface TKEMachine extends KubernetesResource {
  spec?: TKEMachineSpec;
  status?: NodeStatus;
}

export interface ClusterAddOnType extends KubernetesResource {
  type: string;
  level: string;
  latestVersion: string;
  description?: string;
}

export interface ClusterAddOnSpec {
  type: string;
  level: string;
  version: string;
}

export interface ClusterAddOn extends KubernetesResource {
  spec?: ClusterAddOnSpec;
  status?: {
    phase?: string;
  };
}

export interface PodSecurityPolicy extends KubernetesResource {
  spec?: PodSecurityPolicySpec;
}

export interface PodSecurityPolicySpec {
  privileged: boolean;
  allowPrivilegeEscalation: boolean;
  defaultAllowPrivilegeEscalation?: boolean;
  hostPID: boolean;
  hostIPC: boolean;
  hostNetwork: boolean;
  readOnlyRootFilesystem: boolean;
  allowedHostPaths: Array<{ pathPrefix: string; readOnly: boolean }>;
  hostPorts: Array<{ min: number; max: number }>;
  runAsUser: RunAsUser;
}

export interface RunAsUser {
  rule: RunAsUserRule;
  ranges?: Array<{ min: number; max: number }>;
}

export enum RunAsUserRule {
  MustRunAs = 'MustRunAs',
  MustRunAsNonRoot = 'MustRunAsNonRoot',
  RunAsAny = 'RunAsAny',
}

export interface Csp extends KubernetesResource {
  spec: CspSpec;
  status?: CspStatus;
}

export interface CspSpec {
  productName?: 'csp';
  productType?: string;
  version?: string;
  installerNamespace?: string;
  highAvailable: boolean;
  machines: [
    {
      type: 'master';
      ip: string;
      port: number;
      password: string;
    },
    {
      type: 'slave';
      ip: string;
      port: number;
      password: string;
    }?,
  ];
  vip?: string;
  clusterInfo?: {
    adminUser?: string;
    adminKeyRing?: string;
    controllerAddress?: string;
    monitors?: string;
  };
}

type CspStatusPhase =
  | 'Pending'
  | 'Provisioning'
  | 'Provisioned'
  | 'Deleting'
  | 'Failed'
  | 'Running';

export interface CspStatus {
  conditions: Array<{
    status: 'True';
    reason: string;
    message: string;
    type: 'Ready';
    lastProbeTime: string;
  }>;
  phase?: CspStatusPhase;
  version: string;
  productEntrypoint: string;
  installerNamespaceName: string;
  lastProbeTime: string;
  deployTime: string;
}
