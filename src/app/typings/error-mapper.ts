/**
 * Defines how a error object (validator key, error object, the control object) should be mapped to string.
 */
export declare interface ErrorMapperInterface {
  /**
   * Maps an error to a string.
   * @param {string} key the error key. i.e., required, etc
   * @param error the error object
   * @param control control Object (AbstractControl for Angular, NgModel for AngularJS)
   * @returns {string}
   */
  map(key: string, error?: any, control?: any): string;
}

/**
 * Defines how an error key should be mapped to a string.
 */
export declare interface ErrorMapperObject {
  [key: string]: string;
}

export declare type ErrorMapper = ErrorMapperInterface | ErrorMapperObject;
