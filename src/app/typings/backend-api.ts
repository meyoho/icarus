import { K8sResourceAction, KubernetesResource } from '@alauda/common-snippet';

import { DaemonSet, Deployment, StatefulSet, TApp } from './raw-k8s';

export type Workload = Deployment | StatefulSet | DaemonSet | TApp;

export enum WorkloadKind {
  Deployment = 'Deployment',
  StatefulSet = 'StatefulSet',
  DaemonSet = 'DaemonSet',
  TApp = 'TApp',
}

export interface K8sResourceWithActions<
  T extends KubernetesResource = KubernetesResource
> {
  kubernetes: T;
  resource_actions?: string[];
}

/**
 * application address
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.1.6GET/acp/v1/kubernetes/%3Ccluster_name%3E/namespaces/%3Cnamespace%3E/applications/%3Cname%3E/address
 */
export interface ApplicationAddress {
  workloads?: {
    [key: string]: {
      [key: string]: WorkloadAddress[];
    };
  };
}

export interface WorkloadAddress {
  name?: string;
  protocol?: string;
  host?: string;
  frontend?: number;
  port?: number;
  url?: string;
}

/**
 * Topology Resources
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.1.8GET/acp/v1/kubernetes/%3Ccluster%3E/topology/%3Cnamespace%3E/%3Ckind%3E/%3Cname%3E
 */
export interface TopologyResponse {
  nodes: {
    [key: string]: KubernetesResource;
  };
  edges: TopologyEdge[];
}

export interface TopologyEdge {
  type: string;
  from: string;
  to: string;
}

/**
 * API resource types
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.2.0GET/acp/v1/resources/{cluster}/resourcetypes
 */
export interface APIResource {
  kind: string;
  name: string;
  singularName?: string;
  shortNames?: string[];
  namespaced: boolean;
  verbs: K8sResourceAction[];
  // copied from APIResourceList
  groupVersion?: string;
}

export interface APIResourceList {
  kind: 'APIResourceList';
  apiVersion?: string;
  groupVersion: string;
  resources: APIResource[];
}

export interface Workloads {
  Deployment?: Deployment[];
  DaemonSet?: DaemonSet[];
  StatefulSet?: StatefulSet[];
  TApp?: TApp[];
}

/**
 * Container Log
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.2.1GET/acp/v1/kubernetes/{cluster}/namespace/{namespace}/pods/{podName}/{containerName}/log
 */
export interface ContainerLogParams {
  previous?: string;
  logFilePosition?: string;
  referenceTimestamp?: string;
  referenceLineNum?: string;
  offsetFrom?: string;
  offsetTo?: string;
}

export interface LogInfo {
  podName: string;
  containerName: string;
  initContainerName: string;
  fromDate: string;
  toDate: string;
  truncated: boolean;
}

export interface LogLine {
  timestamp: string;
  content: string;
}

export interface LogSelection {
  logFilePosition: string;
  referencePoint: LogLineReference;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogLineReference {
  timestamp: string;
  lineNum: number;
}

export interface ContainerLog {
  info: LogInfo;
  logs: LogLine[];
  selection: LogSelection;
}
