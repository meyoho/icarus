import { ResourceListParams } from '@alauda/common-snippet';

export interface WorkspaceBaseParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}

export interface WorkspaceDetailParams extends WorkspaceBaseParams {
  name?: string;
}

export interface WorkspaceListParams
  extends WorkspaceBaseParams,
    ResourceListParams {}

export interface AdminBaseParams {
  cluster?: string;
  namespace?: string;
}

export interface AdminDetailParams extends AdminBaseParams {
  name?: string;
}

export interface AdminListParams extends AdminBaseParams, ResourceListParams {}

export interface Environments {
  DEBUG?: boolean;
  LABEL_BASE_DOMAIN: string;
  GLOBAL_NAMESPACE: string;
  API_MARKET_URL?: string;
  LOGO_URL?: string;
}

/**
 * Console related status.
 *
 * All feature related status should be mapped into one of the categories
 */
export enum UnifiedStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  INACTIVE = 'INACTIVE',
}

export interface HostModel {
  prefix?: string;
  name: string;
  value: string;
}
