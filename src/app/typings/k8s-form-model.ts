import {
  KubernetesResource,
  ObjectMeta,
  StringMap,
} from '@alauda/common-snippet';

import { IpRecycleStrategyEnum } from 'app/features-shared/app/pod-template-form/pod-network-form/util';
import { PodControllerKindEnum } from 'app/features-shared/app/utils';
import { MatchType, RuleType } from 'app/features-shared/load_balancer/util';
import {
  Container,
  DaemonSetSpec,
  DaemonSetUpdateStrategy,
  DeploymentSpec,
  DeploymentStrategy,
  IngressBackend,
  IngressRule,
  JobSpec,
  LabelSelector,
  LocalObjectReference,
  PodAffinityTerm,
  PodSpecAffinity,
  Probe,
  Service,
  StatefulSetSpec,
  StatefulSetUpdateStrategy,
  WeightedPodAffinityTerm,
} from 'app/typings';
import { ClusterNetworkType } from 'app_admin/features/network/type';

export const LOG_PATH_KEY = '__FILE_LOG_PATH__';
export const EXCLUDE_LOG_PATH_KEY = '__EXCLUDE_LOG_PATH__';

export interface WorkloadOption {
  kind: PodControllerKindEnum;
  name: string;
}

export interface ApplicationFormModel extends KubernetesResource {
  spec: {
    componentTemplates: KubernetesResource[];
  };
}

export interface PodControllerFormModel extends KubernetesResource {
  suffixName?: string; // UI 输入的后半部分名字
  spec?: DeploymentSpec | StatefulSetSpec | DaemonSetSpec;
}

export interface PodTemplateSpecFormModel {
  network?: PodNetworkFormModel;
  metadata: ObjectMeta;
  spec: PodSpecFormModel;
}
export interface PodSpecFormModel {
  initContainers?: ContainerFormModel[];
  containers?: ContainerFormModel[];
  hostNetwork?: boolean;
  nodeSelector?: StringMap;
  affinity?: PodSpecAffinity;
  imagePullSecrets?: LocalObjectReference[];
}

export interface PodNetworkFormModel {
  // 子网的annotation需要设置 PodTemplateSpec.metadata.annotation
  //  ovn.kubernetes.io/ip_pool: "10.16.0.15,10.16.0.16,10.16.0.17" # 可选，当需要指定 ip 时，填入所期望的 ip，不填则自动分配
  //  tke.cloud.tencent.com/eni-ip-pool: <pool_name>  # galaxy下ip池选择
  fixIp?: string[];
  networkType?: ClusterNetworkType;
  floatingIp?: boolean;
  ipRecycleStrategy?: IpRecycleStrategyEnum;

  // macvlan
  subnet?: string;
}

export interface ContainerFormModel extends Container {
  probes?: {
    livenessProbe?: Probe;
    readinessProbe?: Probe;
  };
  logPaths?: string[];
  excludeLogPaths?: string[];
}

export interface PodAffinityFormModel {
  type: string; // required, preferred
  weight: number;
  topologyKey: string;
  labelSelector: LabelSelector;
}

export type AffinityTerm = WeightedPodAffinityTerm | PodAffinityTerm;

export const PodAffinityType = {
  required: 'requiredDuringSchedulingIgnoredDuringExecution',
  preferred: 'preferredDuringSchedulingIgnoredDuringExecution',
} as const;

export interface ResourceRequirementsFormModel {
  limits?: {
    cpu?: {
      value: string;
      unit: string;
    };
    memory?: {
      value: string;
      unit: string;
    };
  };
  requests?: {
    cpu?: {
      value: string;
      unit: string;
    };
    memory?: {
      value: string;
      unit: string;
    };
  };
  // 用户自定义资源限制: http://confluence.alaudatech.com/pages/viewpage.action?pageId=50829241
  customResource?: {
    [key: string]: string;
  };
}

export type StrategyFormModel =
  | DeploymentStrategy
  | DaemonSetUpdateStrategy
  | StatefulSetUpdateStrategy;

export interface ServiceFormModel extends Service {
  workloadName?: string; // 计算组件名称，在创建应用的过程中创建 Service，才会有这个 formControl
  virtualIp?: boolean; // 虚拟 ip，为 false 时需要设置 spec.clusterIp: None
  internetAccess?: boolean; // 外网访问，为 false 时需要设置 spec.type: NodePort
  sessionPersistence?: boolean; // 会话保持，对应 spec.sessionAffinity: ClientIP/None
}

export enum ServiceTypeEnum {
  ClusterIP = 'ClusterIP',
  Headless = 'Headless',
  NodePort = 'NodePort',
}

export enum PodNetworkTypeEnum {
  host = 'Host',
  cluster = 'Cluster',
}

export interface LoadBalancerFormModel {
  loadbalancerName: string;
  address: string;
  replicas: number;
  nodeSelector: StringMap;
}

export type RuleIndicatorValues = Array<[MatchType, ...string[]]>;

export interface RuleIndicator {
  type: RuleType;
  values: RuleIndicatorValues;
  key: string;
}

export interface IngressRuleFormModel extends IngressRule {
  secretName?: string;
  domain?: string;
  subDomain?: string;
}

export interface IngressSpecFormModel {
  backend?: IngressBackend;
  rules?: IngressRuleFormModel[];
}

export interface IngressFormModel extends KubernetesResource {
  spec?: IngressSpecFormModel;
}

export interface JobSpecFormModel extends JobSpec {
  jobType?: string;
  startingDeadlineSeconds?: number | '';
}

export interface NotificationSubscriptionFormModel {
  method: string;
  receivers: string[];
  sender: string;
  template: string;
}

export interface NotificationReceiverFormModel {
  method: string;
  displayName: string;
  email: string;
  sms: string;
  dingtalk: string;
  wechat: string;
  webhook: string;
  apiVersion: string;
  kind: string;
  name: string;
  namespace: string;
  resourceVersion: string;
}
