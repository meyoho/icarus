import { NgModule } from '@angular/core';

import { CrontabNextPipe } from './pipes/crontab-next.pipe';
import { FormatUtcStrPipe } from './pipes/format-utc-str.pipe';
import { IsDeletingPipe } from './pipes/is-deleting.pipe';
import { IsFederatedResourcePipe } from './pipes/is-federated.pipe';
import { IsGrayReleasePipe } from './pipes/is-gray-release.pipe';
import { TrustHtmlPipe } from './pipes/trust-html.pipe';
import { TrustStylePipe } from './pipes/trust-style.pipe';
import { TrustUrlPipe } from './pipes/trust-url.pipe';

const PIPES = [
  TrustHtmlPipe,
  FormatUtcStrPipe,
  TrustUrlPipe,
  TrustStylePipe,
  CrontabNextPipe,
  IsFederatedResourcePipe,
  IsDeletingPipe,
  IsGrayReleasePipe,
];

@NgModule({
  declarations: PIPES,
  exports: PIPES,
})
export class PipesModule {}
