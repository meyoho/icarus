import { StringMap } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Optional,
  SimpleChanges,
} from '@angular/core';
import {
  ControlContainer,
  FormArray,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

import { rowBackgroundColorFn } from 'app/utils';

@Component({
  selector: 'rc-string-array-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StringArrayFormTableComponent
  extends BaseResourceFormComponent<string[]>
  implements OnInit, OnChanges {
  @Input()
  resourceName: string;

  @Input()
  validators: ValidatorFn[] = [];

  @Input()
  errorMapper: StringMap = {};

  form: FormArray;

  constructor(
    public injector: Injector,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  ngOnChanges({ validators }: SimpleChanges): void {
    if (validators && validators.currentValue && this.form) {
      this.form.controls.forEach((formControl: FormControl) => {
        formControl.setValidators(validators.currentValue);
      });
    }
  }

  rowBackgroundColorFn = rowBackgroundColorFn;

  getResourceMergeStrategy() {
    return false;
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): string[] {
    return [];
  }

  add(index = this.form.length) {
    this.form.insert(index, this.getOnFormArrayResizeFn()());
    this.cdr.markForCheck();
  }

  remove(index: number) {
    this.form.removeAt(index);
    this.cdr.markForCheck();
  }

  getOnFormArrayResizeFn() {
    return () => this.fb.control('', this.validators);
  }
}
