// tslint:disable: max-classes-per-file no-commented-code
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ContentChildren,
  Directive,
  EventEmitter,
  Injector,
  Input,
  Output,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { FormGroupDirective, NgForm } from '@angular/forms';

export interface ArrayFormTableRowContext {
  allowEdit: boolean;
  allowDelete: boolean;
}

@Directive({
  selector: '[rcArrayFormTableHeader]',
})
export class ArrayFormTableHeaderDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Directive({
  selector: '[rcArrayFormTableRow]',
})
export class ArrayFormTableRowDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Directive({
  selector: '[rcArrayFormTableZeroState]',
})
export class ArrayFormTableZeroStateDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Directive({
  selector: '[rcArrayFormTableRowControl]',
})
export class ArrayFormTableRowControlDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Directive({
  selector: '[rcArrayFormTableFooter]',
})
export class ArrayFormTableFooterDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

const ROW_DEFAULT_COLOR = 'transparent';

@Component({
  selector: 'rc-array-form-table',
  templateUrl: './template.html',
  // Since rows maybe updated without changing reference, we use Default here:
  changeDetection: ChangeDetectionStrategy.Default,
})
export class ArrayFormTableComponent {
  @ContentChild(ArrayFormTableHeaderDirective, {
    read: TemplateRef,
    static: true,
  })
  headerTemplate: TemplateRef<any>;

  // 用户可以提供多行模板, 但只有第一行有控制按键.
  @ContentChildren(ArrayFormTableRowDirective, { read: TemplateRef })
  rowTemplates: QueryList<TemplateRef<any>>;

  // 表单默认提供删除按键; 假如用户提供模板, 将使用用户的模板
  @ContentChild(ArrayFormTableRowControlDirective, {
    read: TemplateRef,
    static: true,
  })
  rowControlTemplate: TemplateRef<any>;

  // 表单默认提供添加按键; 假如用户提供模板, 将使用用户的模板
  @ContentChild(ArrayFormTableFooterDirective, {
    read: TemplateRef,
    static: true,
  })
  footerTemplate: TemplateRef<any>;

  // 表单默认为空数据提供无xxx的模板; 假如用户提供模板, 将使用用户的模板
  @ContentChild(ArrayFormTableZeroStateDirective, {
    read: TemplateRef,
    static: true,
  })
  zeroStateTemplate: TemplateRef<any>;

  // Data context for rows.
  @Input()
  rows: any[];

  @Input()
  noRowSeparator = false;

  // 将使用这个值来渲染无数据提示
  @Input()
  resourceName = 'data';

  @Output()
  add = new EventEmitter();

  @Output()
  remove = new EventEmitter<number>(); // Returns the index to be removed

  @Input()
  readonly = false;

  @Input()
  addDisabled = false;

  @Input()
  rowBackgroundColorFn = <T>(_row: T, _index?: number) => ROW_DEFAULT_COLOR;

  @Input()
  minRow = 0;

  constructor(public injector: Injector) {}

  getRowBackgroundColor(control: any) {
    const formDirective =
      this.injector.get(NgForm, null) ||
      this.injector.get(FormGroupDirective, null);
    // check if control in form, which is most common scene, dummy check AbstractControl is enough
    if (formDirective && 'dirty' in control) {
      return formDirective.submitted || control.dirty
        ? this.rowBackgroundColorFn(control)
        : ROW_DEFAULT_COLOR;
    }
    return this.rowBackgroundColorFn(control);
  }
}
