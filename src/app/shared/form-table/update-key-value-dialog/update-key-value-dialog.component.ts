import { StringMap, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { cloneDeep, isEqual } from 'lodash-es';
import { BehaviorSubject, EMPTY, Observable, Subject, from } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';

export interface UpdateKeyValueDialogData<
  R = unknown,
  Context = unknown,
  T = StringMap
> {
  title?: string;
  keyValues?: T;
  readonlyKeys?: Array<string | RegExp>;
  onUpdate?: (keyValues: T) => Promise<R> | Observable<R>;
  context?: Context;
  updateSuccessMsg?: string;
  updateFailMsg?: string;
}

@Component({
  selector: 'rc-update-key-value-dialog',
  templateUrl: './update-key-value-dialog.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateKeyValueDialogComponent<
  R = unknown,
  Context = unknown,
  T = StringMap
> implements OnInit, OnDestroy {
  readonly onUpdate$$ = new Subject<void>();

  readonly submitting$$ = new BehaviorSubject(false);

  originalKeyValues: T;

  keyValues: T;

  private readonly destroy$$ = new Subject<void>();

  constructor(
    public dialogRef: DialogRef<
      UpdateKeyValueDialogComponent<R, Context, T>,
      UpdateKeyValueDialogData<R, Context, T>
    >,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Optional()
    @Inject(DIALOG_DATA)
    public data: UpdateKeyValueDialogData<R, Context, T> = {},
  ) {
    this.originalKeyValues = this.data.keyValues || ({} as T);
    this.keyValues = cloneDeep(this.originalKeyValues);
  }

  ngOnInit() {
    const { context, onUpdate } = this.data;
    if (!onUpdate) {
      return;
    }
    this.onUpdate$$
      .pipe(
        takeUntil(this.destroy$$),
        tap(() => this.submitting$$.next(true)),
        switchMap(() => {
          // nothing changed
          if (isEqual(this.originalKeyValues, this.keyValues)) {
            this.dialogRef.close();
            return EMPTY;
          }
          return from(onUpdate.call(context, this.keyValues)).pipe(
            tap(() => {
              this.message.success(
                this.translate.get(
                  this.data.updateSuccessMsg || 'update_successed',
                ),
              );
              this.submitting$$.next(false);
              this.dialogRef.close();
            }),
            catchError(() => {
              this.message.error(
                this.translate.get(this.data.updateFailMsg || 'update_failed'),
              );
              this.submitting$$.next(false);
              return EMPTY;
            }),
          );
        }),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$$.next();
  }
}
