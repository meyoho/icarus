// tslint:disable: no-commented-code
import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { FormArray, ValidatorFn } from '@angular/forms';
import { sortBy } from 'lodash-es';

import { BaseStringMapFormComponent } from 'app/abstract';
import { ErrorMapper } from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

import { ArrayFormTableFooterDirective } from '../array-form-table/component';

@Component({
  selector: 'rc-key-value-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormTableComponent extends BaseStringMapFormComponent
  implements OnInit, OnChanges {
  @Input()
  resourceName: string;

  @Input()
  validator: {
    key: ValidatorFn[];
    value: ValidatorFn[];
  } = {
    key: [],
    value: [],
  };

  @Input()
  errorMapper: {
    key: ErrorMapper;
    value: ErrorMapper;
  } = {
    key: {},
    value: {},
  };

  @ValueHook<KeyValueFormTableComponent, 'readonlyKeys'>(
    readonlyKeys => !!readonlyKeys,
  )
  @Input()
  readonlyKeys: Array<string | RegExp> = [];

  // 表单默认提供添加按键; 假如用户提供模板, 将使用用户的模板
  @ContentChild(ArrayFormTableFooterDirective, {
    read: TemplateRef,
    static: true,
  })
  footerTemplate: TemplateRef<any>;

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnChanges({ validator }: SimpleChanges): void {
    if (validator && validator.currentValue && this.form) {
      this.form.controls.forEach((formArray: FormArray) => {
        const [key, value] = formArray.controls;
        key.setValidators(this.getKeyValidators());
        value.setValidators(this.getValueValidators());
      });
    }
  }

  rowBackgroundColorFn = rowBackgroundColorFn;

  isReadonly(key: string) {
    return this.readonlyKeys.some(readonlyKey => {
      if (!readonlyKey) {
        return false;
      }
      if (typeof readonlyKey === 'string') {
        return readonlyKey === key;
      }
      return readonlyKey.test(key);
    });
  }

  adaptResourceModel(resource: { [key: string]: string }) {
    let newFormModel = Object.entries(resource || {});
    // 排序，先按字母排序，再把 readonly 的放前面
    newFormModel = sortBy(newFormModel, (arr: [string, string]) => {
      return arr[0];
    });
    newFormModel.sort((arr1: [string, string], arr2: [string, string]) => {
      const flag1 = this.isReadonly(arr1[0]);
      const flag2 = this.isReadonly(arr2[0]);
      if ((flag1 && flag2) || (!flag1 && !flag2)) {
        return 0;
      } else {
        return flag1 ? -1 : 1;
      }
    });
    if (newFormModel.length === 0) {
      newFormModel = this.getDefaultFormModel();
    }

    return newFormModel;
  }

  getKeyValidators() {
    return (this.validator && this.validator.key) || [];
  }

  getValueValidators() {
    return (this.validator && this.validator.value) || [];
  }

  isLabelReadonly(index: number) {
    const formArray = this.form.controls[index] as FormArray;
    return formArray.valid && this.isReadonly(formArray.controls[0].value);
  }
}
