import { StringMap, TranslateService } from '@alauda/common-snippet';
import { Injector } from '@angular/core';

import { DefaultErrorMapperService } from 'app/services/default-error-mapper.service';
import { ErrorMapperInterface } from 'app/typings';

export abstract class BaseErrorsMapper {
  translate: TranslateService;
  mapperService: ErrorMapperInterface;

  protected getErrorMessage(
    errors: Record<string, boolean>,
    errorsMapper?: StringMap,
    namePrefix?: string,
  ) {
    const firstError = Object.keys(errors || {})[0];
    if (!firstError) {
      return;
    }
    if (errorsMapper?.[firstError]) {
      return errorsMapper[firstError];
    }
    // only required error should be modified
    if (namePrefix && firstError === 'required') {
      return this.translate.get('required_item', { name: namePrefix });
    }
    const definedError = this.mapperService.map(firstError, errors[firstError]);

    return definedError
      ? (namePrefix || '') + definedError
      : this.translate.get('unknown_error');
  }

  constructor(injector: Injector) {
    this.translate = injector.get(TranslateService);
    this.mapperService = injector.get(DefaultErrorMapperService);
  }
}
