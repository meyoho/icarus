import { ObservableInput } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';
import { Observable } from 'rxjs';

import { createActions, yamlWriteOptions } from 'app/utils';

/**
 * http://confluence.alauda.cn/display/FRON/Issue-Angular+related
 * 由于formControl存在内存泄漏问题，内部 form 展示-隐藏切换应当采用 hidden 进行控制
 */
export abstract class BaseFormContainer<T = any> implements OnInit {
  @Input()
  isUpdate = false;

  @Input()
  initResource: T;

  @ObservableInput(true)
  initResource$: Observable<T>;

  @ViewChild('resourceForm', { static: false })
  private readonly ngForm: NgForm;

  codeEditorOptions = yamlWriteOptions;
  actionsConfig = createActions;
  formView: 'UI' | 'YAML' = 'UI';
  submitting = false;

  resource: T;
  yaml: string;
  // 支持带 ‘恢复’ 的 codeEditor
  originalYaml: string;
  fb: FormBuilder;
  form: FormGroup;

  protected notification: NotificationService;
  protected location: Location;

  abstract submit(): void;
  protected abstract fillResource(resource: T): T;

  constructor(injector: Injector) {
    this.notification = injector.get(NotificationService);
    this.location = injector.get(Location);
    this.fb = injector.get(FormBuilder);
    this.form = this.fb.group({ resource: this.fb.control('') });
  }

  ngOnInit() {
    this.resource = this.initResource;
    this.form.patchValue({
      resource: this.initResource,
    });
  }

  switchFormView(view: 'YAML' | 'UI') {
    let resource: T;
    if (view === 'UI') {
      try {
        this.form.enable();
        resource = this.yamlToResource(this.yaml);
        this.form.patchValue({
          resource,
        });
      } catch (e) {}
    } else {
      this.form.disable();
      resource = this.form.get('resource').value;
      this.originalYaml = this.yaml = this.resourceToYaml(resource);
    }
    this.resource = resource;
    this.formView = view;
  }

  cancel() {
    this.location.back();
  }

  protected checkForm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return false;
    }

    if (this.formView === 'YAML') {
      try {
        this.resource = this.yamlToResource(this.yaml);
        return true;
      } catch (e) {
        return false;
      }
    }
    const resource = this.form.get('resource').value;
    this.resource = this.fillResource(resource);

    return true;
  }

  protected yamlToResource(yaml: string) {
    try {
      return jsyaml.safeLoad(yaml);
    } catch (e) {
      this.notification.error(e.message);
      throw e;
    }
  }

  protected resourceToYaml(resource: T) {
    try {
      return jsyaml.safeDump(cloneDeep(this.fillResource(resource)));
    } catch (e) {
      return '';
    }
  }

  protected updateResource(resource: T) {
    this.form.patchValue({ resource });
    this.resource = resource;
  }
}
