import { DialogContentComponent } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  Inject,
  Injector,
  Input,
  OnDestroy,
  Optional,
  Renderer2,
  forwardRef,
} from '@angular/core';
import {
  FormGroup,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { Subscription, merge } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

import { DefaultErrorMapperService } from 'app/services/default-error-mapper.service';
import { ErrorMapper, ErrorMapperInterface } from 'app/typings';

import { BaseTooltip } from './base-tooltip';
import { TooltipMode } from './tooltip-content/tooltip-content.component';

function isErrorMapperInterface(
  errorMapper: ErrorMapper,
): errorMapper is ErrorMapperInterface {
  return typeof (errorMapper as ErrorMapperInterface).map === 'function';
}

@Directive({ selector: '[rcErrorsTooltip]' })
export class ErrorsTooltipDirective extends BaseTooltip
  implements AfterViewInit, OnDestroy {
  @Input()
  rcErrorsTooltip: NgControl | FormGroup;

  @Input()
  rcErrorsTooltipPosition = 'top right';

  @Input()
  rcErrorsTooltipMapper: ErrorMapper;

  @Input()
  rcErrorsTooltipTarget: HTMLElement;

  @Input()
  rcErrorsTooltipInline = false;

  private statusChangeSub: Subscription;

  constructor(
    injector: Injector,
    private readonly cdr: ChangeDetectorRef,
    private readonly elm: ElementRef,
    private readonly defaultMapper: DefaultErrorMapperService,
    private readonly renderer2: Renderer2,
    @Optional()
    @Inject(forwardRef(() => DialogContentComponent))
    private readonly dialogContent: DialogContentComponent,
    @Optional()
    private readonly control: NgControl,
    @Optional()
    private readonly form: NgForm,
    @Optional()
    private readonly formGroup: FormGroupDirective,
  ) {
    super(injector);
  }

  ngAfterViewInit() {
    if (!this.rcErrorsTooltip) {
      this.rcErrorsTooltip = this.control;
    }
    if (!this.rcErrorsTooltip) {
      throw new Error(
        'Errors tooltip is not properly configured. No valid control is found.',
      );
    }

    const statusChange = [this.rcErrorsTooltip.statusChanges];

    if (this.form) {
      statusChange.push(this.form.ngSubmit);
    }
    if (this.formGroup) {
      statusChange.push(this.formGroup.ngSubmit);
    }

    this.statusChangeSub = merge(...statusChange)
      .pipe(
        debounceTime(100),
        map(() => this.getErrorTooltip()),
      )
      .subscribe(content => {
        this.updateTooltipContent(content);
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.detachTooltip();
    this.statusChangeSub.unsubscribe();
  }

  private getErrorTooltip(): string {
    const errors = Object.entries(this.rcErrorsTooltip.errors || {});

    return errors
      .map(([key, error]: [string, any]) => {
        if (!this.rcErrorsTooltipMapper) {
          return this.defaultMapper.map(key, error);
        } else if (isErrorMapperInterface(this.rcErrorsTooltipMapper)) {
          return this.rcErrorsTooltipMapper.map(
            key,
            error,
            this.rcErrorsTooltip,
          );
          // eslint-disable-next-line no-prototype-builtins
        } else if (this.rcErrorsTooltipMapper.hasOwnProperty(key)) {
          return this.rcErrorsTooltipMapper[key];
        } else {
          return this.defaultMapper.map(key, error);
        }
      })
      .filter(tooltip => !!tooltip)
      .map(tooltip => `<span>${tooltip}</span>`)
      .join('<br>');
  }

  private updateTooltipContent(content: string) {
    const rawElement = this.elm.nativeElement;
    const shouldShowTooltip =
      content &&
      (this.rcErrorsTooltip.dirty || this.formSubmitted) &&
      !this.rcErrorsTooltip.disabled;
    if (shouldShowTooltip) {
      this.attachTooltip(
        this.rcErrorsTooltipInline
          ? rawElement.parentElement
          : ErrorsTooltipDirective.DEFAULT_HOST,
      );
      this.setupConfigs({
        inDialog: !!this.dialogContent,
        placement: this.rcErrorsTooltipPosition,
        reference: rawElement,
        mode: TooltipMode.Error,
      });
      this.attachedInstance.title = content;
      this.updatePosition();
      this.renderer2.addClass(rawElement, 'hasError');
    } else {
      this.renderer2.removeClass(rawElement, 'hasError');
      this.detachTooltip();
    }
  }

  private get formSubmitted() {
    return (
      (this.form && this.form.submitted) ||
      (this.formGroup && this.formGroup.submitted)
    );
  }
}
