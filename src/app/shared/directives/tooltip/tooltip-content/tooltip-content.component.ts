import { ValueHook } from '@alauda/common-snippet';
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  Renderer2,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import Popper from 'popper.js';

export enum TooltipMode {
  Default = 'default',
  Success = 'success',
  Warning = 'warning',
  Error = 'error',
  Info = 'info',
  Empty = 'empty',
}

export interface TooltipConfigs {
  inDialog?: boolean;
  mode?: TooltipMode;
  noArrow?: boolean;
  empty?: boolean;
  reference?: HTMLElement;
  placement?: string;
  offset?: number | string;
  modifiers?: Popper.Modifiers;
}

// TODO: fade in out animation
@Component({
  selector: 'rc-tooltip-content',
  templateUrl: 'tooltip-content.component.html',
  styleUrls: ['tooltip-content.component.scss'],
})
export class TooltipContentComponent
  implements AfterViewInit, OnDestroy, TooltipConfigs {
  @ViewChild('tooltip', { static: true })
  tooltip: ElementRef;

  @ViewChild('arrow', { static: true })
  arrow: ElementRef;

  title: number | string;
  template: TemplateRef<any>;

  @ValueHook(function (this: TooltipContentComponent, inDialog: boolean) {
    if (typeof inDialog !== 'boolean') {
      return false;
    }
    this.renderer2[inDialog ? 'addClass' : 'removeClass'](
      this.elementRef.nativeElement,
      'in-dialog',
    );
  })
  inDialog: boolean;

  mode: TooltipMode = TooltipMode.Default;
  noArrow = false;
  empty = false;
  reference: HTMLElement;
  placement: Popper.Placement = 'right';
  offset: number | string = '0,8';
  context: any;
  modifiers: Popper.Modifiers;

  popper: Popper;

  static convertPlacementString(position: string): Popper.Placement {
    return position
      .split(' ')
      .map((item, index) => {
        if (index === 0) {
          return item;
        } else {
          switch (item) {
            case 'left':
            case 'top':
              return 'start';
            case 'right':
            case 'bottom':
              return 'end';
            default:
              return 'center';
          }
        }
      })
      .join('-') as Popper.Placement;
  }

  constructor(
    private readonly elementRef: ElementRef,
    private readonly renderer2: Renderer2,
  ) {}

  ngAfterViewInit() {
    this.popper = new Popper(
      this.reference,
      this.tooltip.nativeElement.parentElement,
      {
        placement: TooltipContentComponent.convertPlacementString(
          this.placement,
        ),
        modifiers: {
          arrow: {
            element: this.arrow.nativeElement,
          },
          offset: {
            offset: this.offset,
          },
          ...this.modifiers,
        },
      },
    );
  }

  ngOnDestroy() {
    this.popper.destroy();
  }

  updatePosition() {
    if (this.popper) {
      setTimeout(() => {
        this.popper.update();
      });
    }
  }
}
