import {
  ComponentPortal,
  DomPortalHost,
  PortalHost,
} from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  Injector,
} from '@angular/core';

import {
  TooltipConfigs,
  TooltipContentComponent,
} from './tooltip-content/tooltip-content.component';

export class BaseTooltip {
  static readonly DEFAULT_HOST = document.querySelector('body');

  // tslint:disable-next-line: deprecation
  protected portalHost: PortalHost;
  protected portal: ComponentPortal<TooltipContentComponent>;
  protected attachedInstance: TooltipContentComponent;

  private readonly appRef = this.injector.get(ApplicationRef);

  // tslint:disable-next-line: deprecation
  private readonly componentFactoryResolver = this.injector.get(
    ComponentFactoryResolver,
  );

  constructor(private readonly injector: Injector) {}

  private createTooltip(hostElm: HTMLElement) {
    if (this.portal && this.portalHost) {
      return;
    }
    // tslint:disable-next-line: deprecation
    this.portalHost = new DomPortalHost(
      hostElm,
      this.componentFactoryResolver,
      this.appRef,
      this.injector,
    );
    this.portal = new ComponentPortal(
      TooltipContentComponent,
      /**
       * FIXME: delete this cause ng ivy's issue https://github.com/angular/angular/issues/36449
       *  in future NG version should readd this. because with no viewContainerRef, appRef's attach will introduce additional one time dirty check
       */

      // this.viewContainerRef,
    );
  }

  protected attachTooltip(hostElm: HTMLElement = BaseTooltip.DEFAULT_HOST) {
    this.createTooltip(hostElm);
    if (!this.portalHost.hasAttached()) {
      this.attachedInstance = this.portalHost.attach(this.portal).instance;
    }
  }

  protected detachTooltip() {
    if (this.portalHost) {
      this.portalHost.detach();
    }
    this.attachedInstance = null;
  }

  protected setupConfigs(configs: TooltipConfigs) {
    if (!this.attachedInstance) {
      return;
    }
    Object.entries(configs).forEach(([key, value]) => {
      this.attachedInstance[key as keyof TooltipContentComponent] = value;
    });
  }

  updatePosition() {
    if (this.attachedInstance) {
      this.attachedInstance.updatePosition();
    }
  }
}
