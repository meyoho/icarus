// tslint:disable: directive-selector max-classes-per-file no-host-metadata-property
import { ValueHook } from '@alauda/common-snippet';
import { Directive, Input } from '@angular/core';
import { isBoolean } from 'lodash-es';

@Directive({
  selector: '[prevent]',
  host: {
    click: 'stop && $event.preventDefault()',
  },
})
export class PreventDirective {
  @ValueHook(isBoolean)
  @Input()
  prevent = true;
}

@Directive({
  selector: '[stop]',
  host: {
    click: 'stop && $event.stopPropagation()',
  },
})
export class StopDirective {
  @ValueHook(isBoolean)
  @Input()
  stop = true;
}
