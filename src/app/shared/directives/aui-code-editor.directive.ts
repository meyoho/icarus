/* tslint:disable:directive-selector */

import { TranslateService } from '@alauda/common-snippet';
import { ChangeDetectorRef, Directive, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Directive({
  selector: 'aui-code-editor',
})
export class AuiCodeEditorHelperDirective implements OnDestroy {
  private readonly translateSubscription: Subscription;
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
  ) {
    this.translateSubscription = this.translate.locale$.subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy() {
    this.translateSubscription.unsubscribe();
  }
}
