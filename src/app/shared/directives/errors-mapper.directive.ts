import { ObservableInput, StringMap } from '@alauda/common-snippet';
import { ComponentPortal, DomPortalOutlet, Portal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ApplicationRef,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Directive,
  Injector,
  Input,
  OnDestroy,
  Optional,
  Self,
} from '@angular/core';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { Observable, Subject, merge, of } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';

import { BaseErrorsMapper } from '../abstract/base-errors-mapper';

@Directive({
  selector: '[rcErrorsMapper]',
})
export class ErrorsMapperDirective extends BaseErrorsMapper
  implements AfterViewInit, OnDestroy {
  @Input()
  rcErrorsMapper: StringMap;

  /**
   * disable errors mapper
   */
  @ObservableInput()
  @Input('rcErrorsMapperDisabled')
  rcErrorsMapperDisabled$: Observable<boolean>;

  /**
   * speicfy outlet,consider this other than speicfy control entity to make sure di can always get ngForm
   */
  @Input()
  rcErrorsMapperOutlet: Element;

  @Input()
  rcErrorsMapperControlName: string;

  private portal: Portal<any>;
  private portalHost: DomPortalOutlet;
  private portalAttach: TextComponent;
  private readonly destroy$ = new Subject<void>();

  constructor(
    public injector: Injector,
    private readonly cdr: ChangeDetectorRef,
    private readonly cfr: ComponentFactoryResolver,
    private readonly appRef: ApplicationRef,
    @Self() private readonly control: NgControl,
    @Optional() private readonly ngForm: NgForm,
    @Optional()
    private readonly formGroup: FormGroupDirective,
  ) {
    super(injector);
  }

  ngAfterViewInit() {
    if (!this.control) {
      throw new Error(
        'Errors mapper is not properly configured. No valid control is found.',
      );
    }
    this.initPortal();
    const statusChange = [this.control.statusChanges];

    if (this.ngForm) {
      statusChange.push(this.ngForm.ngSubmit);
    }
    if (this.formGroup) {
      statusChange.push(this.formGroup.ngSubmit);
    }

    this.rcErrorsMapperDisabled$
      .pipe(
        takeUntil(this.destroy$),
        switchMap(isDisable => {
          return isDisable
            ? of(null)
            : merge(...statusChange).pipe(
                debounceTime(50),
                map(() =>
                  this.getErrorMessage(
                    this.control.errors,
                    this.rcErrorsMapper,
                    this.rcErrorsMapperControlName,
                  ),
                ),
              );
        }),
      )
      .subscribe(content => {
        this.updateContent(content);
        this.cdr.markForCheck();
      });
  }

  updateContent(content: string) {
    if (!content) {
      if (this.portalHost.hasAttached()) {
        this.portalHost.detach();
      }
      return;
    }
    if (!this.portalHost.hasAttached()) {
      this.portalAttach = this.portalHost.attach(this.portal).instance;
    }
    this.portalAttach.data = content;
  }

  initPortal() {
    this.portalHost = new DomPortalOutlet(
      this.rcErrorsMapperOutlet,
      this.cfr,
      this.appRef,
      this.injector,
    );
    this.portal = new ComponentPortal(TextComponent);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.portalHost.dispose();
  }
}

@Component({
  template: `
    {{ data }}
  `,
})
class TextComponent {
  data: string;
}
