import { ValueHook } from '@alauda/common-snippet';
import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[opacity]',
})
export class OpacityDirective {
  /**
   * all non-number opacity input such as empty string is prevented,
   * so that we can simply write `<div opacity></div>` instead of `<div [opacity]="0"></div>`
   */
  @ValueHook(Number.isFinite)
  @HostBinding('style.opacity')
  @Input()
  opacity = 0;
}
