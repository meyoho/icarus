import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator } from '@angular/forms';

import { cronValidator } from 'app/utils';

@Directive({
  selector: '[rcCron][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CronValidatorDirective,
      multi: true,
    },
  ],
})
export class CronValidatorDirective implements Validator {
  @Input()
  rcCron: {
    minInterval: number;
  };

  validate = cronValidator(this.rcCron?.minInterval);
}
