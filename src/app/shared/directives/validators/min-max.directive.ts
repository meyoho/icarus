import {
  Directive,
  HostBinding,
  Input,
  OnChanges,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
  Validators,
} from '@angular/forms';

/* tslint:disable:use-host-property-decorator */

/**
 * Angular form directives for min/max.
 */
@Directive({
  selector:
    '[rcInput][max],[max][formControlName],[max][formControl],[max][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MaxValidatorDirective),
      multi: true,
    },
  ],
})
export class MaxValidatorDirective implements Validator, OnChanges {
  private _validator: ValidatorFn;
  private _onChange: () => void;

  @Input()
  max: string;

  @HostBinding('attr.max')
  get maxValue() {
    return this.max ? this.max : null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('max' in changes) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.max != null ? this._validator(c) : null;
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = Validators.max(parseFloat(this.max));
  }
}

// tslint:disable-next-line: max-classes-per-file
@Directive({
  selector:
    '[rcInput][min],[min][formControlName],[min][formControl],[min][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MinValidatorDirective),
      multi: true,
    },
  ],
})
export class MinValidatorDirective implements Validator, OnChanges {
  private _validator: ValidatorFn;
  private _onChange: () => void;

  @HostBinding('attr.min')
  get maxValue() {
    return this.min ? this.min : null;
  }

  @Input()
  min: string;

  ngOnChanges(changes: SimpleChanges): void {
    if ('min' in changes) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.min != null ? this._validator(c) : null;
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = Validators.min(parseFloat(this.min));
  }
}
