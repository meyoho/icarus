import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { get } from 'lodash-es';

import {
  CIDR_IP_PATTERN,
  CIDR_PATTERN,
  RESERVED_IP_PATTERN,
  RESERVED_IP_SEGMENT_PATTERN,
} from 'app/utils';

/**
 *  subnet相关校验，用于多tag输入，不支持错误提示，错误时禁止添加，校验器统一返回null
 */
@Directive({
  selector: '[rcPreserveIPPattern]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: PreserveIPPatternDirective,
      multi: true,
    },
  ],
})
export class PreserveIPPatternDirective implements Validator {
  validate(control: AbstractControl): null {
    return preserveIpPatternValidateFn(control);
  }
}

// tslint:disable-next-line: max-classes-per-file
@Directive({
  selector: '[rcCidrPattern]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CidrPatternDirective,
      multi: true,
    },
  ],
})
export class CidrPatternDirective implements Validator {
  validate(control: AbstractControl): null {
    return cidrPatternValidateFn(control);
  }
}

function preserveIpPatternValidateFn(control: AbstractControl): null {
  const value: string[] = control.value || [];
  const length = value.length;
  const lastInput = get(value, length - 1, null);
  if (!lastInput) {
    return null;
  }
  // pattern error
  if (!RESERVED_IP_PATTERN.pattern.test(lastInput)) {
    control.setValue(value.slice(0, length - 1));
  }
  // order error
  if (RESERVED_IP_SEGMENT_PATTERN.pattern.test(lastInput)) {
    const ips = lastInput.split('..');
    if (_compareIP(ips[0], ips[1])) {
      control.setValue(value.slice(0, length - 1));
    }
  }
  return null;
}
function _compareIP(ipBegin: string, ipEnd: string): boolean {
  const temp1 = ipBegin.split('.').map(e => +e);
  const temp2 = ipEnd.split('.').map(e => +e);
  for (let i = 0; i < 4; i++) {
    if (temp1[i] > temp2[i]) {
      return true;
    } else if (temp1[i] < temp2[i]) {
      return false;
    }
  }
  // equal
  return true;
}

function cidrPatternValidateFn(control: AbstractControl): null {
  const value: string[] = control.value || [];
  const length = value.length;
  const lastInput = get(value, length - 1, '');
  // must be valid input
  if (CIDR_IP_PATTERN.pattern.test(lastInput)) {
    const patchInput = lastInput + '/32';
    if (value.includes(patchInput)) {
      control.setValue(value.slice(0, length - 1));
    } else {
      control.setValue([...value.slice(0, length - 1), patchInput]);
    }
    return null;
  }

  // pattern error
  if (!CIDR_PATTERN.pattern.test(lastInput) && length > 0) {
    control.setValue(value.slice(0, length - 1));
    return null;
  }

  return null;
}
