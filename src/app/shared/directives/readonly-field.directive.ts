import { FieldNotAvailablePipe, ObservableInput } from '@alauda/common-snippet';
import { FormItemControlDirective } from '@alauda/ui';
import {
  CdkPortalOutlet,
  ComponentPortal,
  TemplatePortal,
} from '@angular/cdk/portal';
import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  NgControl,
  ValidatorFn,
} from '@angular/forms';
import { Observable, Subject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  startWith,
  takeUntil,
} from 'rxjs/operators';

@Directive({ selector: '[rcReadonlyField]' })
export class ReadonlyFieldDirective implements OnInit, OnDestroy {
  @Input()
  rcReadonlyField: boolean;

  @ObservableInput(true)
  rcReadonlyField$: Observable<boolean>;

  private isControlRequired = false;

  private spanComponentRef: ComponentRef<SpanComponent>;

  private readonly templateContext = {};

  private readonly destroy$ = new Subject<void>();

  // 自定义模板
  @Input('rcReadonlyFieldTemplate')
  template: TemplateRef<any>;

  @HostBinding('hidden') get isHidden() {
    return this.rcReadonlyField;
  }

  constructor(
    private readonly control: NgControl,
    private readonly cfr: ComponentFactoryResolver,
    private readonly viewContainerRef: ViewContainerRef,
    // 仅考虑当前control，子control无法知晓父FormItemControl元素 ，包裹了多少个control，以及各自处于什么状态，该情况自行处理
    @Self()
    @Optional()
    private readonly controlDirective: FormItemControlDirective,
  ) {}

  private setValue(value: unknown) {
    if (this.template) {
      Object.assign(this.templateContext, { $implicit: value });
    } else {
      this.spanComponentRef.instance.setData(value);
    }
  }

  ngOnInit() {
    const controlEntity = this.control.control;
    // formControl will compose multi validators to 1, no matter defined in template nor FormBuilder
    const { validator, asyncValidator } = controlEntity;
    this.isControlRequired = this.controlDirective?.required || false;
    const portalOutlet = new CdkPortalOutlet(this.cfr, this.viewContainerRef);
    const portal = this.template
      ? new TemplatePortal(
          this.template,
          this.viewContainerRef,
          this.templateContext,
        )
      : new ComponentPortal(SpanComponent, this.viewContainerRef);

    const containerAttached$ = this.rcReadonlyField$.pipe(
      distinctUntilChanged(),
      map(isUpdate => {
        if (!isUpdate) {
          // 重置
          if (this.controlDirective) {
            this.controlDirective.required = this.isControlRequired;
          }
          if (portalOutlet.hasAttached()) {
            portalOutlet.detach();
          }
          this.applyValidators(controlEntity, validator, asyncValidator);
          return false;
        }

        if (this.controlDirective) {
          this.controlDirective.required = false;
        }
        const attachPoint = portalOutlet.attach(portal);
        if (!this.template) {
          this.spanComponentRef = attachPoint as ComponentRef<SpanComponent>;
        }
        this.clearValidators(controlEntity);
        return true;
      }),
    );

    const sourceValue$ = this.control.valueChanges.pipe(
      startWith(this.control.value),
    );

    combineLatest([sourceValue$, containerAttached$])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, attached]) => !!attached),
      )
      .subscribe(([value]) => {
        this.setValue(value);
      });
  }

  private applyValidators(
    control: AbstractControl,
    rawValidator?: ValidatorFn,
    rawAsyncValidator?: AsyncValidatorFn,
  ) {
    if (
      control.validator === rawValidator &&
      control.asyncValidator === rawAsyncValidator
    ) {
      return;
    }
    control.setValidators(rawValidator);
    control.setAsyncValidators(rawAsyncValidator);
    control.updateValueAndValidity();
  }

  private clearValidators(control: AbstractControl) {
    if (!control.validator && !control.asyncValidator) {
      return;
    }
    control.clearValidators();
    control.clearAsyncValidators();
    control.updateValueAndValidity();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}

// 功能简单，仅作为私有类，无需引入到外部 Module 中
@Component({
  template: '<span>{{data}}</span>',
})
class SpanComponent {
  data: string | number;

  setData(value: unknown) {
    this.data = FieldNotAvailablePipe.prototype.transform.call(null, value);
  }
}
