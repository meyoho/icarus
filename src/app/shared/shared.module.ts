import { CodeEditorModule } from '@alauda/code-editor';
import {
  AccountMenuModule,
  AsyncDataModule,
  CommonLayoutModule,
  DisabledContainerModule,
  HelpMenuModule,
  K8SResourceListModule,
  NotificationUtilModule,
  PageGuardModule,
  PlatformCenterNavModule,
  TranslateModule,
  UtilsModule,
} from '@alauda/common-snippet';
import {
  AccordionModule,
  AutocompleteModule,
  BackTopModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TableModule as AuiTableModule,
  TableOfContentsModule,
  TabsModule,
  TagModule,
  TooltipModule as AuiTooltipModule,
} from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HighchartsChartModule } from 'highcharts-angular';
import { AitSharedModule } from 'submodules/ait-shared/shared/shared.module';

import { PageHeaderContentDirective } from 'app/shared/directives/page-header-content.directive';
import {
  CidrPatternDirective,
  PreserveIPPatternDirective,
} from 'app/shared/directives/validators/subnet.directive';

import { DefaultBreadcrumbComponent } from './components/breadcrumb/default-breadcrumb.component';
import { CodeDisplayDialogComponent } from './components/code-display-dialog/component';
import { DeletingTagComponent } from './components/deleting-tag/component';
import { DonutChartComponent } from './components/donut-chart/component';
import { ErrorMapperComponent } from './components/error-mapper/error-mapper.component';
import { FeedbackNotificationComponent } from './components/feedback-notification/component';
import {
  FieldSetColumnComponent,
  FieldSetColumnGroupComponent,
} from './components/field-set/column/component';
import { FieldSetGroupComponent } from './components/field-set/group/component';
import { FieldSetItemComponent } from './components/field-set/item/compoent';
import { FoldableBlockComponent } from './components/foldable-block/component';
import { ForceDeleteComponent } from './components/force-delete/component';
import {
  FormInlineGroupComponent,
  FormInlineItemComponent,
} from './components/form-inline/component';
import { GrayReleaseTagComponent } from './components/gray-release-tag/component';
import { KeyValueDisplayComponent } from './components/key-value-display/component';
import { LoadingMaskComponent } from './components/loading-mask/loading-mask.component';
import { MetricChartComponent } from './components/metric-chart/component';
import { NotDeployedComponent } from './components/not-deployed/component';
import { RelativeTimeComponent } from './components/relative-time/component';
import { ResourceYamlDisplayComponent } from './components/resource-yaml-display/component';
import { StatusBadgeComponent } from './components/status-badge/status-badge.component';
import { StatusIconComponent } from './components/status-icon/status-icon.component';
import { SubResourceLinkComponent } from './components/subresource-link/component';
import { TagsLabelComponent } from './components/tags-label/tags-label.component';
import { TimePickrComponent } from './components/timepickr/component';
import { ViewSwitchComponent } from './components/view-switch/component';
import { ZeroStateComponent } from './components/zero-state/zero-state.component';
import { AsyncDataDirective } from './directives/async-data.directive';
import { AuiCodeEditorHelperDirective } from './directives/aui-code-editor.directive';
import { ErrorsMapperDirective } from './directives/errors-mapper.directive';
import { PreventDirective, StopDirective } from './directives/event.directive';
import { OpacityDirective } from './directives/opacity.directive';
import { ReadonlyFieldDirective } from './directives/readonly-field.directive';
import { TooltipModule } from './directives/tooltip/tooltip.module';
import { TrimDirective } from './directives/trim.directive';
import { CronValidatorDirective } from './directives/validators/cron.directive';
import {
  MaxValidatorDirective,
  MinValidatorDirective,
} from './directives/validators/min-max.directive';
import { VisibilityDirective } from './directives/visibility.directive';
import {
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
} from './form-table/array-form-table/component';
import { KeyValueFormTableComponent } from './form-table/key-value-form-table/component';
import { StringArrayFormTableComponent } from './form-table/string-array-form-table/component';
import { UpdateKeyValueDialogComponent } from './form-table/update-key-value-dialog/update-key-value-dialog.component';
import { PipesModule } from './pipes.module';

const NG_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  RouterModule,
];

const THIRD_PARTY_MODULES = [
  FlexLayoutModule,
  HighchartsChartModule,
  PortalModule,
];

const AUI_MODULES = [
  AccordionModule,
  AuiTableModule,
  AuiTooltipModule,
  AutocompleteModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TableOfContentsModule,
  TabsModule,
  TagModule,
  BackTopModule,
];

const COMMON_MODULES = [
  CommonLayoutModule,
  TranslateModule,
  K8SResourceListModule,
  AsyncDataModule,
  DisabledContainerModule,
  NotificationUtilModule,
  UtilsModule,
  PageGuardModule,
  AccountMenuModule,
  PlatformCenterNavModule,
  HelpMenuModule,
];

const SHARED_MODULES = [TooltipModule, AitSharedModule];

const EXPORTABLE_MODULES = [
  PipesModule,
  ...NG_MODULES,
  ...THIRD_PARTY_MODULES,
  ...AUI_MODULES,
  ...COMMON_MODULES,
  ...SHARED_MODULES,
];

const SHARED_DIRECTIVES = [
  AuiCodeEditorHelperDirective,
  CronValidatorDirective,
  MaxValidatorDirective,
  MinValidatorDirective,
  PageHeaderContentDirective,
  TrimDirective,
  VisibilityDirective,
  AsyncDataDirective,
  OpacityDirective,
  PreventDirective,
  StopDirective,
  CidrPatternDirective,
  PreserveIPPatternDirective,
  ReadonlyFieldDirective,
  ErrorsMapperDirective,
];

const SHARED_COMPONENTS = [
  DefaultBreadcrumbComponent,
  FoldableBlockComponent,
  ForceDeleteComponent,
  LoadingMaskComponent,
  StatusBadgeComponent,
  StatusIconComponent,
  TagsLabelComponent,
  TimePickrComponent,
  CodeDisplayDialogComponent,
  ViewSwitchComponent,
  ZeroStateComponent,
  RelativeTimeComponent,
  ErrorMapperComponent,
  FieldSetGroupComponent,
  FieldSetItemComponent,
  FieldSetColumnComponent,
  FieldSetColumnGroupComponent,
  FormInlineGroupComponent,
  FormInlineItemComponent,
  MetricChartComponent,
  KeyValueDisplayComponent,
  ResourceYamlDisplayComponent,
  FeedbackNotificationComponent,
  NotDeployedComponent,
  SubResourceLinkComponent,
  DeletingTagComponent,
  GrayReleaseTagComponent,
  DonutChartComponent,

  // FormTableComponents
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
  KeyValueFormTableComponent,
  StringArrayFormTableComponent,
  UpdateKeyValueDialogComponent,
];

/**
 *  Entry shared module containers source with app_admin and app_user
 */
@NgModule({
  imports: [...EXPORTABLE_MODULES],
  exports: [...EXPORTABLE_MODULES, ...SHARED_DIRECTIVES, ...SHARED_COMPONENTS],
  declarations: [...SHARED_DIRECTIVES, ...SHARED_COMPONENTS],
})
export class SharedModule {}
