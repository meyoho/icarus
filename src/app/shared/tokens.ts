import { InjectionToken } from '@angular/core';

import { Environments } from '../typings';

export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');
