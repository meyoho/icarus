import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { UiStateService } from 'app/services/ui-state.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';

@Component({
  selector: 'rc-page-header',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageHeaderComponent {
  constructor(
    @Inject(ENVIRONMENTS) public env: Environments,
    public uiState: UiStateService,
  ) {}
}
