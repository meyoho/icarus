import {
  NavConfigLoaderService,
  TranslateService,
  findNavPath,
  publishRef,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { HttpClient } from '@angular/common/http';
import { Injector, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {
  ActivatedRoute,
  NavigationEnd,
  ParamMap,
  Router,
} from '@angular/router';
import { Observable, Subject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { GlobalPermissionService } from 'app/services/global-permission.service';
import {
  TemplateHolderType,
  UiStateService,
} from 'app/services/ui-state.service';

export type NavConfigGroup = Array<{
  path: NavItemConfig[];
  left: NavItemConfig;
}>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const pickChildren = (
  item: NavItemConfig,
  path: NavItemConfig[] = [],
): NavConfigGroup =>
  item.children
    ? item.children.reduce(
        (prev, curr) => prev.concat(pickChildren(curr, path.concat(item))),
        [],
      )
    : [{ path: path.concat(item), left: item }];

export abstract class BaseLayoutComponent implements OnInit, OnDestroy {
  http: HttpClient;
  router: Router;
  title: Title;
  activatedRoute: ActivatedRoute;
  translate: TranslateService;
  uiState: UiStateService;
  navLoader: NavConfigLoaderService;
  globalPermission: GlobalPermissionService;

  // Default nav config
  navConfig$: Observable<NavItemConfig[]>;
  onDestroy$ = new Subject<void>();
  pageHeaderContentTemplate$: Observable<TemplatePortal>;
  activatedKey$: Observable<string>;

  abstract navConfigAddress: string;

  constructor(injector: Injector) {
    this.http = injector.get(HttpClient);
    this.router = injector.get(Router);
    this.title = injector.get(Title);
    this.translate = injector.get(TranslateService);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.uiState = injector.get(UiStateService);
    this.navLoader = injector.get(NavConfigLoaderService);
    this.globalPermission = injector.get(GlobalPermissionService);
  }

  ngOnInit() {
    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;

    this.uiState.activeItemInfo$
      .pipe(
        filter(activeItemInfo => activeItemInfo && activeItemInfo.length > 0),
        map(activeItemInfo => activeItemInfo[0].label),
        switchMap(primaryItemName =>
          this.translate.locale$.pipe(mapTo(primaryItemName)),
        ),
        takeUntil(this.onDestroy$),
      )
      .subscribe(title => this.title.setTitle(title));

    this.navConfig$ = this.navLoader
      .loadNavConfig(this.navConfigAddress)
      .pipe(
        this.navLoader.parseYaml(),
        this.navLoader.mapToAuiNav(),
        publishRef(),
      );

    this.activatedKey$ = combineLatest([
      this.navConfig$,
      this.router.events.pipe(
        filter(e => e instanceof NavigationEnd),
        map(({ url }: NavigationEnd) => url),
        startWith(this.router.url),
        distinctUntilChanged(),
        switchMap(() => this.activatedRoute.paramMap),
      ),
    ]).pipe(
      map(([trees, paramMap]) => {
        return findNavPath<NavItemConfig>(item =>
          this.router.isActive(this.buildUrl(item.href, paramMap), false),
        )(trees);
      }),
      filter(nodes => nodes.length > 0),
      tap<NavItemConfig[]>(nodes => {
        this.uiState.setActiveItemInfo(nodes);
      }),
      map(nodes => nodes[nodes.length - 1].key),
      distinctUntilChanged(),
      publishRef(),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  abstract buildUrl(href: string, paramMap?: ParamMap): string;

  handleActivatedItemChange({ href }: { href: string }) {
    if (href.startsWith('http://') || href.startsWith('https://')) {
      window.open(href, '_blank');
    } else {
      this.router.navigateByUrl(
        this.buildUrl(href, this.activatedRoute.snapshot.paramMap),
      );
    }
  }
}
