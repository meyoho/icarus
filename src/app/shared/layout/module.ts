import { NgModule } from '@angular/core';

import { PageHeaderComponent } from 'app/shared/layout/page-header/component';
import { SharedModule } from 'app/shared/shared.module';

const COMPONENTS = [PageHeaderComponent];

@NgModule({
  imports: [SharedModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class SharedLayoutModule {}
