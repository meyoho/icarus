import { KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

import { K8sUtilService } from 'app/services/k8s-util.service';

@Pipe({ name: 'rcIsFederatedResource' })
export class IsFederatedResourcePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource): boolean {
    return this.k8sUtil.isFederatedResource(resource);
  }
}
