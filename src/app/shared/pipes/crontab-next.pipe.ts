import { Locale, TranslateService } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';
import parser from 'cron-parser';
import dayjs from 'dayjs';
import esUS from 'dayjs/locale/es-us';
import zhCN from 'dayjs/locale/zh-cn';

@Pipe({
  name: 'rcCrontabNext',
})
export class CrontabNextPipe implements PipeTransform {
  constructor(private readonly translate: TranslateService) {}

  isFiledCountCorrect(exp: string) {
    const spaceCount = exp.split('').filter(val => val === ' ').length;
    const fieldCount = exp.split(' ').filter(val => val).length;
    return spaceCount === 4 && fieldCount === 5;
  }

  transform(
    rule: string,
    step = 1,
    options = {
      utc: false,
      format: 'YYYY-MM-DD HH:mm:ss',
      tz: 'Asia/Shanghai',
    },
  ) {
    let dateString = 'N/A';
    if (!rule || !this.isFiledCountCorrect(rule)) {
      return dateString;
    }
    try {
      const interval = parser.parseExpression(rule, options);
      let timestamp: number;
      for (let i = 0; i < step; i++) {
        timestamp = interval.next().getTime();
      }
      dateString = dayjs(timestamp)
        .locale(this.translate.locale === Locale.ZH ? zhCN : esUS)
        .format(options.format);
    } catch (err) {
      if (
        process.env.NODE_ENV === 'development' &&
        err &&
        !/invalid/i.test(err.message)
      ) {
        console.error(err);
      }
      dateString = 'N/A';
    }
    return dateString;
  }
}
