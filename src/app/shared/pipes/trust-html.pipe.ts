import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

/**
 * Pipe a HTML string to safe state.
 *
 * Angular has some native security concerns on how to interpolate html content,
 * we shall bypass it with some magic.
 *
 * Credit:
 * - http://stackoverflow.com/questions/39628007/angular2-innerhtml-binding-remove-style-attribute
 */
@Pipe({ name: 'rcTrustHtml' })
export class TrustHtmlPipe implements PipeTransform {
  constructor(private readonly sanitizer: DomSanitizer) {}

  transform(html: string): SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
}
