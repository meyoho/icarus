import { K8sUtilService, KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'rcIsGrayRelease' })
export class IsGrayReleasePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}
  transform(resource: KubernetesResource) {
    return !!this.k8sUtil.getLabel(resource, 'canaryrefer', 'servicemesh');
  }
}
