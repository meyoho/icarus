import { KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'rcIsDeleting' })
export class IsDeletingPipe implements PipeTransform {
  transform(resource: KubernetesResource) {
    return !!resource?.metadata?.deletionTimestamp;
  }
}
