import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
@Component({
  selector: 'rc-not-deployed',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotDeployedComponent {
  @Input()
  text = 'not_deployed';
}
