import { ObservableInput, StringMap, publishRef } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'rc-key-value-display',
  templateUrl: 'template.html',
})
export class KeyValueDisplayComponent {
  columns = ['key', 'value'] as const;

  @ObservableInput()
  @Input('source')
  private readonly source$: Observable<StringMap>;

  keyValues$ = this.source$.pipe(
    map(
      source =>
        source &&
        Object.entries(source).map(([key, value]) => ({
          key,
          value,
        })),
    ),
    publishRef(),
  );
}
