import { ValueHook } from '@alauda/common-snippet';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalPermissionService } from 'app/services/global-permission.service';
import { UiStateService } from 'app/services/ui-state.service';

@Component({
  selector: 'rc-view-switch',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('viewState', [
      state(
        'admin',
        style({
          left: '16px',
        }),
      ),
      state(
        'user',
        style({
          left: '0px',
        }),
      ),
      transition(':enter', animate(200)),
    ]),
  ],
})
export class ViewSwitchComponent {
  @ValueHook<ViewSwitchComponent, 'isAdminView'>(function (val) {
    this.uiState.setViewSwitchState(val);
  })
  @Input()
  isAdminView: boolean;

  constructor(
    private readonly router: Router,
    public globalPermission: GlobalPermissionService,
    private readonly uiState: UiStateService,
  ) {}

  get currentView() {
    return this.isAdminView ? 'admin' : 'user';
  }

  switchView() {
    if (this.isAdminView) {
      this.router.navigate(['/home']);
    } else {
      this.router.navigate(['/admin']);
    }
  }
}
