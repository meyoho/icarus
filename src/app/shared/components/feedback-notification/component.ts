import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackNotificationComponent {
  @Input()
  primary: string;

  @Input()
  secondary: string;

  @Input()
  content?: string;

  @Input()
  pre: boolean;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onPrimary = new EventEmitter<void>();

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onSecondary = new EventEmitter<void>();
}

export type FeedbackNotificationProps = Partial<
  Pick<
    FeedbackNotificationComponent,
    // tslint:disable-next-line: max-union-size
    'primary' | 'secondary' | 'content' | 'pre'
  >
>;
