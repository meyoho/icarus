import { K8sUtilService, KubernetesResource } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { WorkspaceBaseParams } from 'app/typings';

@Component({
  selector: 'rc-gray-release-tag',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrayReleaseTagComponent {
  @Input() baseParams: WorkspaceBaseParams;
  @Input()
  resource: KubernetesResource;

  @Input() isLink: boolean;

  constructor(private readonly k8sUtil: K8sUtilService) {}

  getCanaryReferer(resource: KubernetesResource) {
    return this.k8sUtil
      .getLabel(resource, 'canaryrefer', 'servicemesh')
      .split('.')[0];
  }
}
