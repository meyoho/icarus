import { TranslateService } from '@alauda/common-snippet';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { getUnifiedStatus } from 'app/shared/components/status-badge/status-mapper';

export type StatusBadgeMode = 'default' | 'indicator' | 'text' | 'rect';

@Component({
  selector: 'rc-status-badge',
  templateUrl: 'status-badge.component.html',
  styleUrls: ['status-badge.component.scss'],
})
export class StatusBadgeComponent implements OnChanges {
  statusText: string;

  @Input()
  status: string;

  @Input()
  showPrimitiveStatus: boolean;

  // if true, status text is not translated
  @Input()
  mode: StatusBadgeMode = 'default';

  constructor(private readonly translateService: TranslateService) {}

  ngOnChanges({ status }: SimpleChanges) {
    if (status.currentValue) {
      this.statusText = this.showPrimitiveStatus
        ? this.getCapitalizedStatus()
        : this.getTranslatedStatus();
    }
  }

  private getTranslatedStatus() {
    return this.translateService.get(this.status.toLowerCase());
  }

  private getCapitalizedStatus() {
    return this.status.charAt(0).toUpperCase() + this.status.slice(1);
  }

  get statusAttr() {
    return getUnifiedStatus(this.status);
  }
}
