import { UnifiedStatus } from 'app/typings';

const statusMaps = [
  {
    status: UnifiedStatus.SUCCESS,
    values: [
      'running',
      'used',
      'completed',
      'bound',
      'normal',
      'ok',
      'switch_on',
      'released',
      'ready',
      'schedulable',
    ],
  },
  {
    status: UnifiedStatus.ERROR,
    values: [
      'error',
      'matching_failed',
      'recycle_failed',
      'critical',
      'overflow',
      'alarm',
      'alert',
      'firing',
      'failed',
      'unschedulable',
      'notready',
    ],
  },
  {
    status: UnifiedStatus.PENDING,
    values: ['warning', 'pending', 'waiting_recycle'],
  },
  {
    status: UnifiedStatus.IN_PROGRESS,
    values: [
      'starting',
      'deploying',
      'free',
      'matching',
      'destroying',
      'stopping',
      'shutting_down',
      'insufficient_data',
      'insufficient_res',
      'updating',
      'deleting',
      'pending',
    ],
  },
  {
    status: UnifiedStatus.INACTIVE,
    values: ['stopped', 'unknown', 'invalid', 'switch_off'],
  },
];

export function getUnifiedStatus(status = '') {
  const res = statusMaps.find(item =>
    item.values.includes(status.toLowerCase()),
  );
  return res ? res.status : UnifiedStatus.INACTIVE;
}
