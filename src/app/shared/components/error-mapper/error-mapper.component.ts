import { ObservableInput, StringMap, publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';

import { BaseErrorsMapper } from 'app/shared/abstract/base-errors-mapper';

@Component({
  selector: 'rc-errors-mapper',
  template: `
    <ng-container *ngIf="errors$ | async">
      {{ errorMessage$ | async }}
    </ng-container>
  `,
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorMapperComponent extends BaseErrorsMapper {
  constructor(public injector: Injector) {
    super(injector);
  }

  @ObservableInput()
  @Input('errors')
  readonly errors$: Observable<Record<string, boolean>>;

  @ObservableInput()
  @Input('errorsMapper')
  readonly errorsMapper$: Observable<StringMap>;

  // used to add name prefix for most validation text
  @ObservableInput()
  @Input('controlName')
  controlName$: Observable<string>;

  readonly errorMessage$ = combineLatest([
    this.errors$,
    this.errorsMapper$,
    this.controlName$.pipe(startWith('')),
  ]).pipe(
    map(([errors, errorsMapper, name]) => {
      return this.getErrorMessage(errors, errorsMapper, name);
    }),
    distinctUntilChanged(),
    publishRef(),
  );
}
