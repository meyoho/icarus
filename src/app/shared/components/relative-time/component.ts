import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'rc-relative-time',
  template: `
    <span
      [auiTooltip]="time | rcFormatUtcStr"
      [auiTooltipDisabled]="!time"
      auiTooltipType="info"
    >
      {{ time | aclRelativeTime }}
    </span>
  `,
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RelativeTimeComponent implements OnInit, OnDestroy {
  private readonly destroy$ = new Subject<void>();

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
  ) {}

  @Input()
  time: string;

  ngOnInit(): void {
    this.translate.locale$
      .pipe(takeUntil(this.destroy$), distinctUntilChanged())
      .subscribe(() => {
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
