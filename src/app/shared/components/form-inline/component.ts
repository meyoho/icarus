import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'rc-inline-form-item',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormInlineItemComponent {}

// tslint:disable-next-line: max-classes-per-file
@Component({
  selector: 'rc-inline-form-group',
  template: '<ng-content></ng-content>',
  styles: [
    `
      :host {
        display: flex;
        flex-flow: row wrap;
        align-items: center;
      }
    `,
  ],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormInlineGroupComponent {}
