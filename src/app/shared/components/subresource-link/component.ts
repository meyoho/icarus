import {
  KubernetesResource,
  viewActions,
  yamlReadOptions,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Component, Input, TemplateRef } from '@angular/core';
import { safeDump } from 'js-yaml';

import { WorkspaceBaseParams } from 'app/typings';

import {
  getResourceRouterLink,
  isResourceLinkSupported,
} from '../../../features-shared/app/utils';

@Component({
  selector: 'rc-subresource-link',
  templateUrl: 'template.html',
})
export class SubResourceLinkComponent {
  @Input() baseParams: WorkspaceBaseParams;
  @Input() resource: KubernetesResource;

  isResourceLinkSupported = isResourceLinkSupported;
  getResourceRouterLink = getResourceRouterLink;
  editorActions = viewActions;
  editorOptions = yamlReadOptions;
  yamlInputValue: string;

  constructor(private readonly dialogService: DialogService) {}

  view(data: KubernetesResource, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(data, { lineWidth: 9999, sortKeys: true });
    this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: data.metadata.name,
      },
    });
  }
}
