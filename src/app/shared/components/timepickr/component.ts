import { Callback, noop } from '@alauda/common-snippet';
import { TooltipType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface TimePickrModel {
  hour: number;
  minute: number;
}
const HOUR_OPTIONS = Array.from({ length: 24 }).map((_, index) => index);

const MINUTES_OPTIONS = Array.from({ length: 60 }).map((_, index) => index);

@Component({
  selector: 'rc-timepickr',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TimePickrComponent,
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimePickrComponent implements ControlValueAccessor {
  timeModelData: TimePickrModel;
  TooltipType = TooltipType;
  onModelChange: Callback<[TimePickrModel], void>;
  disabled = false;
  HOUR_OPTIONS = HOUR_OPTIONS;
  MINUTES_OPTIONS = MINUTES_OPTIONS;

  constructor(private readonly cdr: ChangeDetectorRef) {}

  timeDigitPadding(value: number): string {
    return (value + '').padStart(2, '0');
  }

  chooseHour = (hour: number) => {
    this.timeModelData = { ...this.timeModelData, hour };
    this.syncModelData(this.timeModelData);
  };

  chooseMinute = (minute: number) => {
    this.timeModelData = { ...this.timeModelData, minute };
    this.syncModelData(this.timeModelData);
  };

  registerOnChange(fn: (param: TimePickrModel) => void): void {
    this.onModelChange = fn;
  }

  registerOnTouched = noop;

  setDisabledState(isDisabled: boolean): void {
    if (this.disabled !== isDisabled) {
      this.disabled = isDisabled;
      this.cdr.markForCheck();
    }
  }

  writeValue(timeInput: TimePickrModel): void {
    if (timeInput) {
      this.timeModelData = timeInput;
    }
  }

  private syncModelData(data: TimePickrModel) {
    this.onModelChange(data);
  }
}
