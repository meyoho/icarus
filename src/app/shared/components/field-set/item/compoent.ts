import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'rc-field-set-item',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetItemComponent {
  @Input() allowOverflow = false;
  @Input() allowWrap = false;
}
