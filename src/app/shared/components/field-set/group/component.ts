import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'rc-field-set-group',
  template: '<ng-content></ng-content>',
  styleUrls: ['./styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetGroupComponent {}
