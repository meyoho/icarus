import { ChangeDetectionStrategy, Component } from '@angular/core';

// 支持统一的列布局（如单列布局，双栏布局）
@Component({
  selector: 'rc-field-set-column-group',
  template: '<ng-content></ng-content>',
  styleUrls: ['./column-group.style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetColumnGroupComponent {}

@Component({
  selector: 'rc-field-set-column',
  template: '<ng-content></ng-content>',
  styleUrls: ['./column.style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetColumnComponent {}
