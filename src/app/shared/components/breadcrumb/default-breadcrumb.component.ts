import { TranslateService } from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { UiStateService } from 'app/services/ui-state.service';

const IGNORED_PARTIALS = new Set(['list', 'detail']);

@Component({
  selector: 'rc-default-breadcrumb',
  templateUrl: 'default-breadcrumb.component.html',
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<Array<{ href?: string; label: string }>>;

  constructor(
    private readonly uiState: UiStateService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit(): void {
    this.partials$ = combineLatest([
      this.uiState.activeItemInfo$,
      this.translate.locale$,
    ]).pipe(
      map(([navItemPartials]) => [...navItemPartials, ...this.getLeafUrls()]),
    );
  }

  findLeafRouteSnapshot(
    activatedRouteSnapshot: ActivatedRouteSnapshot,
  ): ActivatedRouteSnapshot {
    return activatedRouteSnapshot.firstChild
      ? this.findLeafRouteSnapshot(activatedRouteSnapshot.firstChild)
      : activatedRouteSnapshot;
  }

  getLeafUrls() {
    const snapshot = this.findLeafRouteSnapshot(this.activatedRoute.snapshot);
    const paramsMap = snapshot.paramMap;
    const urlParamKeys = paramsMap.keys.map(key => `:${key}`);
    return snapshot.url.length > 0
      ? snapshot.routeConfig.path
          .split('/')
          .filter(urlPart => !IGNORED_PARTIALS.has(urlPart))
          .map(urlPart => {
            const isParam = urlParamKeys.includes(urlPart);
            return {
              label: isParam
                ? paramsMap.get(urlPart.slice(1))
                : this.translate.get(urlPart),
            };
          })
      : paramsMap.keys.map(key => ({
          label: paramsMap.get(key),
        }));
  }
}
