import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import {
  IEditorConstructionOptions,
  readonlyOptions,
  viewActions,
} from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeDisplayDialogComponent {
  codeEditorOptions: IEditorConstructionOptions;

  editorActions = viewActions;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      language: string;
      title: string;
      code: string;
    },
  ) {
    this.codeEditorOptions = {
      ...readonlyOptions,
      language: data.language || 'yaml',
    };
  }
}
