import { StringMap } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import {
  DefaultStatusColorMapper,
  DefaultStatusIconMapper,
  GenericStatusColor,
  GenericStatusIcon,
} from 'app/typings';

@Component({
  selector: 'rc-status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusIconComponent {
  @Input()
  status: string;

  @Input() withText = true;

  @Input() colorMapper: StringMap;
  @Input() iconMapper: StringMap;

  isPending(status: string, colorMapper: StringMap) {
    const color = colorMapper
      ? { ...DefaultStatusColorMapper, ...colorMapper }[status]
      : DefaultStatusColorMapper[status] || GenericStatusColor.Empty;
    return color === GenericStatusColor.Pending;
  }

  getStatusColor(status: string, colorMapper: StringMap) {
    return colorMapper
      ? { ...DefaultStatusColorMapper, ...colorMapper }[status]
      : DefaultStatusColorMapper[status] || GenericStatusColor.Empty;
  }

  getStatusIcon(status: string, iconMapper: StringMap) {
    return iconMapper
      ? { ...DefaultStatusIconMapper, ...iconMapper }[status]
      : DefaultStatusIconMapper[status] || GenericStatusIcon.Question;
  }
}
