import { ObservableInput } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { arc, pie } from 'd3';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export const CHART_COLORS = {
  running: 'rgba(10, 191, 91, 0.85)',
  partial_running: 'rgba(31, 192, 204, 0.8)',
  pending: 'rgba(0, 110, 255, 0.85)',
  failed: 'rgba(229, 69, 69, 0.85)',
  stopped: 'rgba(153, 153, 153, 0.85)',
  empty: 'rgba(204, 204, 204, 0.85)',
  nil: 'rgba(242, 242, 242, 0.85)',
};

export interface ChartDataItem {
  name: string;
  value: number;
  color?: string;
  type?: string;
}

@Component({
  selector: 'alk-donut-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DonutChartComponent {
  @ObservableInput()
  @Input('data')
  data$: Observable<ChartDataItem[]>;

  @Input()
  size = 120;

  pie$ = this.data$.pipe(
    map(data => {
      const total = data?.reduce((sum, { value }) => sum + value, 0) || 0;
      return pie<ChartDataItem>().value(({ value }) => value)(
        total
          ? data
          : [
              {
                name: 'nil',
                value: 1,
              },
            ],
      );
    }),
  );

  CHART_COLORS = CHART_COLORS;

  get arc() {
    return arc()
      .innerRadius(this.size / 2 - 20)
      .outerRadius(this.size / 2)
      .padAngle(Math.PI / 180);
  }

  color(item: ChartDataItem) {
    return (
      item.color ||
      CHART_COLORS[(item.type || item.name) as keyof typeof CHART_COLORS]
    );
  }
}
