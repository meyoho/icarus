import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ForceDeleteComponent {
  @Output()
  close = new EventEmitter<boolean>();

  inputValue = '';

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      name: string;
      title: string;
      content: string;
      confirmText?: string;
    },
    private readonly translateService: TranslateService,
  ) {}

  delete() {
    this.close.emit(true);
  }

  cancel() {
    this.close.emit(false);
  }

  get disabled() {
    return this.inputValue !== this.dialogData.name;
  }

  get inputTitle() {
    return this.translateService.get('input_name_force_delete', {
      name: this.dialogData.name,
    });
  }
}
