import { KubernetesResource } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'rc-deleting-tag',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeletingTagComponent {
  @Input()
  resource: KubernetesResource;
}
