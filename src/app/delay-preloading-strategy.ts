import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';

@Injectable()
export class DelayPreloadingStrategy implements PreloadingStrategy {
  private loadCounter = 2; // starting from 2 to unblock other xhr.

  preload(_route: Route, fn: () => Observable<any>) {
    if (process.env.NODE_ENV === 'development') {
      return EMPTY;
    }
    return of(null).pipe(
      delay(this.loadCounter++ * 2000),
      switchMap(fn),
      catchError(() => EMPTY),
    );
  }
}
