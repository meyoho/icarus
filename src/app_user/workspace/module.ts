import { CommonLayoutModule } from '@alauda/common-snippet';
import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { HomeComponent } from './home/component';
import { WorkspaceRoutingModule } from './routing.module';
import { WorkspaceComponent } from './workspace.component';

@NgModule({
  imports: [
    SharedModule,
    SharedLayoutModule,
    PageModule,
    RouterModule,
    PortalModule,
    PlatformNavModule,
    CommonLayoutModule,
    WorkspaceRoutingModule,
  ],
  exports: [WorkspaceComponent],
  declarations: [WorkspaceComponent, HomeComponent],
  providers: [WorkspaceComponent],
})
export class WorkspaceModule {}
