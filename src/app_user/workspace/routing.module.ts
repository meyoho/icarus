import { FeatureGuard } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { HomeComponent } from './home/component';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomeComponent,
      },
      {
        path: 'app',
        loadChildren: () =>
          import('../features/app/module').then(M => M.ApplicationModule),
      },
      {
        path: 'federated_app',
        loadChildren: () =>
          import('../features/federated-app/module').then(
            M => M.FederatedAppModule,
          ),
      },
      {
        path: 'configmap',
        loadChildren: () =>
          import('../features/configmap/module').then(M => M.ConfigmapModule),
      },
      {
        path: 'secret',
        loadChildren: () =>
          import('../features/configsecret/module').then(
            M => M.ConfigSecretModule,
          ),
      },
      {
        path: 'pvc',
        loadChildren: () =>
          import('../features/pvc/pvc.module').then(M => M.PvcModule),
      },
      {
        path: 'service',
        loadChildren: () =>
          import('../features/service/module').then(M => M.ServiceModule),
      },
      {
        path: 'ingress',
        loadChildren: () =>
          import('../features/ingress/module').then(M => M.IngressModule),
      },
      {
        path: 'load_balancer',
        canActivate: [FeatureGuard],
        data: { featureName: 'alb2' },
        loadChildren: () =>
          import('app/features-shared/load_balancer/module').then(
            M => M.LoadBalancerModule,
          ),
      },
      {
        path: 'log',
        loadChildren: () =>
          import('../features/log/module').then(M => M.LogModule),
      },
      {
        path: 'cron_job',
        loadChildren: () =>
          import('../features/cron-job/module').then(M => M.CronJobModule),
      },
      {
        path: 'job_record',
        loadChildren: () =>
          import('../features/job-record/module').then(M => M.JobRecordModule),
      },
      {
        path: 'tapp',
        canActivate: [FeatureGuard],
        data: { featureName: 'tapp' },
        loadChildren: () =>
          import('../features/tapp/tapp.module').then(M => M.TAppModule),
      },
      {
        path: 'deployment',
        loadChildren: () =>
          import('../features/deployment/deployment.module').then(
            M => M.DeploymentModule,
          ),
      },
      {
        path: 'daemonset',
        loadChildren: () =>
          import('../features/daemonset/daemonset.module').then(
            M => M.DaemonSetModule,
          ),
      },
      {
        path: 'statefulset',
        loadChildren: () =>
          import('../features/statefulset/statefulset.module').then(
            M => M.StatefulSetModule,
          ),
      },
      {
        path: 'chart',
        canActivate: [FeatureGuard],
        data: { featureName: 'app-market' },
        loadChildren: () =>
          import('../features/app-catalog/chart.module').then(
            M => M.ChartModule,
          ),
      },
      {
        path: 'hr',
        canActivate: [FeatureGuard],
        data: { featureName: 'app-market' },
        loadChildren: () =>
          import('../features/app-catalog/hr.module').then(
            M => M.HelmRequestModule,
          ),
      },
      {
        path: 'route',
        canActivate: [FeatureGuard],
        data: { featureName: 'openshift-route' },
        loadChildren: () =>
          import('../features/route/module').then(M => M.RouteModule),
      },
      {
        path: 'network_policy',
        canActivate: [FeatureGuard],
        data: { featureName: 'network-policy' },
        loadChildren: () =>
          import('../features/network-policy/module').then(
            M => M.NetworkPolicyModule,
          ),
      },
      {
        path: 'pod',
        loadChildren: () =>
          import('../features/pod/pod.module').then(M => M.PodModule),
      },
      {
        path: 'overview',
        loadChildren: () =>
          import('../features/overview/module').then(M => M.OverviewModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
