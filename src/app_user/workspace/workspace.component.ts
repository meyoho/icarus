import {
  FeatureGateService,
  NamespaceIdentity,
  Reason,
  createRecursiveFilter,
  publishRef,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import { Component, Inject, Injector, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/shared/layout/base-layout-component';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as projectActions from 'app/store/actions/projects.actions';
import { ProjectsFacadeService } from 'app/store/facades';
import { Environments, Project, WorkspaceBaseParams } from 'app/typings';

@Component({
  styleUrls: [
    '../../app/shared/layout/layout.common.scss',
    'workspace.component.scss',
  ],
  templateUrl: 'workspace.component.html',
  providers: [WorkspaceComponent],
})
export class WorkspaceComponent extends BaseLayoutComponent
  implements OnDestroy {
  currentProject$: Observable<Project>;
  currentProjectName$: Observable<string>;
  currentNamespace$: Observable<NamespaceIdentity>;

  navConfigAddress = 'custom/navconfig-user.yaml';
  reason = Reason;
  constructor(
    injector: Injector,
    private readonly projectsFacade: ProjectsFacadeService,
    private readonly fgService: FeatureGateService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {
    super(injector);
    this.currentNamespace$ = this.activatedRoute.params.pipe(
      map(params => ({
        name: params.namespace,
        cluster: params.cluster,
      })),
    );
    this.currentProjectName$ = this.activatedRoute.params.pipe(
      map(params => {
        return params.project;
      }),
    );

    this.currentProject$ = this.currentProjectName$.pipe(
      switchMap((name: string) => {
        return this.projectsFacade.selectProjectByName$(name);
      }),
      publishRef(),
    );
    this.projectsFacade.dispatch(new projectActions.LoadProjects());
  }

  /**
   * {project: '', 'cluster': '', namespace: ''}
   */
  get baseParams(): Observable<WorkspaceBaseParams> {
    return this.activatedRoute.params as Observable<WorkspaceBaseParams>;
  }

  get baseParamsSnapshot(): Required<WorkspaceBaseParams> {
    const params = this.activatedRoute.snapshot.params;
    return {
      project: params.project,
      cluster: params.cluster,
      namespace: params.namespace,
    };
  }

  get baseActivatedRoute(): ActivatedRoute {
    return this.activatedRoute;
  }

  filterFg = (items: NavItemConfig[]) => {
    return this.baseParams.pipe(
      switchMap(({ cluster }) =>
        this.fgService.filterEnabled(
          items,
          item => item.gate,
          cluster,
          createRecursiveFilter(
            item => item.children,
            (item, children) => ({ ...item, children }),
          ),
        ),
      ),
    );
  };

  projectSelected(project: string) {
    this.router.navigate(['home'], {
      queryParams: {
        project,
      },
    });
  }

  namespaceSelected(item: NamespaceIdentity) {
    this.router.navigate([
      '/workspace',
      {
        project: this.baseParamsSnapshot.project,
        cluster: item.cluster,
        namespace: item.name,
      },
    ]);
  }

  buildUrl(href: string) {
    const paramString = Object.keys(this.baseParamsSnapshot)
      .map(
        key =>
          `${key}=${this.baseParamsSnapshot[key as keyof WorkspaceBaseParams]}`,
      )
      .join(';');
    return `/workspace;${paramString}/${href}`;
  }
}
