import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { IngressCreateComponent } from './ingress-create/ingress-create.component';
import { IngressDetailRulesComponent } from './ingress-detail/ingress-detail-rules/ingress-detail-rules.component';
import { IngressDetailTlsComponent } from './ingress-detail/ingress-detail-tls/ingress-detail-tls.component';
import { IngressDetailComponent } from './ingress-detail/ingress-detail.component';
import { IngressFormContainerComponent } from './ingress-form-container/ingress-form-container.component';
import { IngressListComponent } from './ingress-list/ingress-list.component';
import { IngressUpdateComponent } from './ingress-update/ingress-update.component';
import { IngressRoutingModule } from './routing.module';
import { IngressSharedModule } from './shared.module';
import { IngressUtilService } from './util.service';

@NgModule({
  imports: [SharedModule, IngressSharedModule, IngressRoutingModule],
  declarations: [
    IngressListComponent,
    IngressCreateComponent,
    IngressDetailComponent,
    IngressDetailRulesComponent,
    IngressDetailTlsComponent,
    IngressUpdateComponent,
    IngressFormContainerComponent,
  ],
  providers: [IngressUtilService],
})
export class IngressModule {}
