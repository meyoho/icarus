import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  NAME,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { equals } from 'ramda';
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { getBelongingApp } from 'app/features-shared/workload/util';
import { Ingress, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { IngressUtilService } from '../util.service';

@Component({
  templateUrl: './ingress-detail.component.html',
})
export class IngressDetailComponent {
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  getBelongingApp = getBelongingApp;

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    publishRef(),
  );

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.INGRESS,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
  );

  dataManager = new AsyncDataLoader<{
    resource: Ingress;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getResource({
          type: RESOURCE_TYPES.INGRESS,
          cluster: params.cluster,
          namespace: params.namespace,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly ingressUtil: IngressUtilService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  update(resource: Ingress) {
    this.router.navigate(['ingress', 'update', resource.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(resource: Ingress) {
    this.ingressUtil
      .delete(resource, this.baseParams.cluster)
      .then(this.jumpToListPage, noop);
  }

  jumpToListPage = () => {
    this.router.navigate(['ingress'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };
}
