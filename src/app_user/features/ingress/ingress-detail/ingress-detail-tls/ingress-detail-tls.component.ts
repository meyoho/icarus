import { Component, Input, OnInit } from '@angular/core';

import { IngressTLS } from 'app/typings';

@Component({
  selector: 'rc-ingress-detail-tls',
  templateUrl: './ingress-detail-tls.component.html',
  styleUrls: ['styles.scss'],
})
export class IngressDetailTlsComponent implements OnInit {
  @Input()
  data: IngressTLS[];

  columns = ['secretName', 'hosts'];

  ngOnInit() {
    if (!this.data) {
      this.data = [];
      return;
    }
    if (this.data && this.data.length === 1) {
      const arr = Object.keys(this.data[0]);
      if (arr.length === 0) {
        this.data = [];
      }
    }
  }

  getHostsDisplay(hosts: string[]) {
    return hosts ? hosts.join('; ') : '';
  }
}
