import { Component, Input, OnInit } from '@angular/core';
import { flatMap } from 'lodash-es';

import { IngressRule } from 'app/typings';
interface IngressRules {
  host: string;
  service: string;
  port: string | number;
}
@Component({
  selector: 'rc-ingress-detail-rules',
  templateUrl: 'ingress-detail-rules.component.html',
  styleUrls: ['styles.scss'],
})
export class IngressDetailRulesComponent implements OnInit {
  columns = ['host', 'service', 'port'];
  @Input()
  data: IngressRule[];

  transformData: IngressRules[];

  ngOnInit() {
    this.transformDataFn();
  }

  transformDataFn() {
    const data = flatMap(this.data, item => {
      if (item.http && item.http.paths.length > 0) {
        return item.http.paths.map(path => ({
          host: `${item.host || ''}${path.path || ''}`,
          service: path.backend.serviceName,
          port: path.backend.servicePort,
        }));
      } else {
        return {
          host: item.host,
          service: '',
          port: '',
        };
      }
    });
    this.transformData = data;
  }
}
