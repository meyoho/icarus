import {
  K8sApiService,
  ObservableInput,
  StringMap,
  matchExpressionsToString,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
} from '@angular/core';
import { FormArray, Validators } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, of } from 'rxjs';
import { catchError, pluck, switchMap, tap } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Domain,
  Environments,
  Ingress,
  IngressFormModel,
  IngressRuleFormModel,
  IngressTLS,
  RESOURCE_TYPES,
  Secret,
  SecretType,
  Service,
  WorkspaceBaseParams,
} from 'app/typings';
import { ASSIGN_ALL, K8S_RESOURCE_NAME_BASE } from 'app/utils';

import { getDefaultIngress } from '../util';

@Component({
  selector: 'rc-ingress-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressFormComponent extends BaseResourceFormGroupComponent<
  Ingress,
  IngressFormModel
> {
  @ObservableInput(true)
  baseParams$: Observable<WorkspaceBaseParams>;

  @Input() baseParams: WorkspaceBaseParams;

  @Input()
  isUpdate: boolean;

  @ObservableInput(true)
  services$: Observable<Service[]>;

  @Input() services: Service[];

  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  domains: Domain[] = [];
  domains$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sApi
        .getGlobalResourceList<Domain>({
          type: RESOURCE_TYPES.DOMAIN,
          queryParams: {
            labelSelector: matchExpressionsToString([
              {
                key: `project.${this.env.LABEL_BASE_DOMAIN}/name`,
                operator: 'in',
                values: [baseParams.project, ASSIGN_ALL],
              },
              {
                key: `cluster.${this.env.LABEL_BASE_DOMAIN}/name`,
                operator: 'in',
                values: [baseParams.cluster, ASSIGN_ALL],
              },
            ]),
          },
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
          tap(domains => (this.domains = domains)),
          publishRef(),
        ),
    ),
  );

  secrets$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sApi
        .getResourceList<Secret>({
          type: RESOURCE_TYPES.SECRET,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
          queryParams: {
            fieldSelector: matchLabelsToString({
              type: SecretType.TLS,
            }),
          },
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
          publishRef(),
        ),
    ),
  );

  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      annotations: this.fb.group({
        'kubernetes.io/ingress.class': this.fb.control(''),
      }),
    });
    const specForm = this.fb.group({
      tls: this.fb.control([]),
      rules: this.fb.array([]),
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getDefaultFormModel() {
    return getDefaultIngress(this.baseParams.namespace);
  }

  getRulesFormArray() {
    return this.form.get('spec.rules') as FormArray;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  adaptFormModel(form: IngressFormModel): Ingress {
    if (!form || !form.spec) {
      return {};
    }
    const spec = form.spec;
    const tls: IngressTLS[] = [];
    if (spec.backend && !spec.backend.serviceName) {
      delete spec.backend;
    }
    const rules = cloneDeep(spec.rules) || [];
    rules
      .filter(rule => rule)
      .forEach(rule => {
        if (rule.secretName) {
          const _tls = tls.find(_item => _item.secretName === rule.secretName);
          if (_tls && rule.host) {
            _tls.hosts.push(rule.host);
          } else {
            tls.push({
              secretName: rule.secretName,
              hosts: rule.host ? [rule.host] : [],
            });
          }
        }
        delete rule.secretName;
      });
    return {
      ...form,
      spec: {
        ...spec,
        tls,
      },
    };
  }

  adaptResourceModel(resource: Ingress): IngressFormModel {
    if (resource) {
      const spec = cloneDeep(resource.spec) || {};
      if (spec.tls && spec.tls.length > 0) {
        const tlsMap: StringMap = {};
        spec.tls.forEach(_tls => {
          if (Array.isArray(_tls.hosts)) {
            _tls.hosts.forEach(host => {
              tlsMap[host] = _tls.secretName;
            });
          }
        });
        spec.rules.forEach((rule: IngressRuleFormModel) => {
          const result = this.getHostValue(rule.host, tlsMap);
          if (result) {
            rule.secretName = result;
          }
        });
      }
      return { ...resource, spec };
    }
    return resource;
  }

  removeRule(index: number) {
    this.getRulesFormArray().removeAt(index);
  }

  addRule() {
    this.getRulesFormArray().push(this.fb.control(null));
  }

  private getHostValue(name: string, map: StringMap) {
    if (!name) {
      return '';
    }
    for (const [key, value] of Object.entries(map)) {
      if (
        (key.startsWith('*.') && name.endsWith(key.slice(2))) ||
        key === name
      ) {
        return value;
      }
    }
    return '';
  }
}
