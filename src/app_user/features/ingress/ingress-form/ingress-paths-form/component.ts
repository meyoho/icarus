import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Optional,
} from '@angular/core';
import { ControlContainer, FormGroup, Validators } from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { HTTPIngressPath, Service, ServicePort } from 'app/typings';
import { INGRESS_PATH_PATTERN, rowBackgroundColorFn } from 'app/utils';

@Component({
  selector: 'rc-ingress-paths-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressPathsFormComponent extends BaseResourceFormArrayComponent<
  HTTPIngressPath
> {
  @Input()
  services: Service[];

  pathReg = INGRESS_PATH_PATTERN;
  rowBackgroundColorFn = rowBackgroundColorFn;

  constructor(
    public injector: Injector,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  getDefaultFormModel(): HTTPIngressPath[] {
    return [
      {
        path: '',
        backend: {
          serviceName: '',
          servicePort: '',
        },
      },
    ];
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(resource: HTTPIngressPath[] = []) {
    return resource.map(item => {
      if (!item.path) {
        item.path = '';
      }
      return item;
    });
  }

  private createNewControl() {
    return this.fb.group({
      path: this.fb.control('', [Validators.pattern(this.pathReg.pattern)]),
      backend: this.fb.group({
        serviceName: this.fb.control('', [Validators.required]),
        servicePort: this.fb.control('', [Validators.required]),
      }),
    });
  }

  serviceChanged(serviceName: string, control: FormGroup) {
    control
      .get('backend.servicePort')
      .setValue(get(this.getPorts(serviceName), '[0].port'));
  }

  getPorts(serviceName: string): ServicePort[] {
    if (!this.services) {
      return [];
    }
    return get(
      this.services.find(service => service.metadata.name === serviceName),
      'spec.ports',
      [],
    );
  }
}
