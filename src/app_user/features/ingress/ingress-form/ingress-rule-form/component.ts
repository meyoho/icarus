import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Domain, IngressRuleFormModel, Secret, Service } from 'app/typings';

@Component({
  selector: 'rc-ingress-rule-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
})
export class IngressRuleFormComponent
  extends BaseResourceFormGroupComponent<IngressRuleFormModel>
  implements OnInit {
  @Input()
  domains: Domain[];

  @Input()
  services: Service[];

  @Input()
  secrets: Secret[];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.get('domain').valueChanges.subscribe(() => {
      this.setSubDomainControlState();
    });
    this.setSubDomainControlState();
  }

  createForm() {
    return this.fb.group({
      subDomain: this.fb.control(''),
      domain: this.fb.control({
        value: '',
      }),
      secretName: this.fb.control(''),
      http: this.fb.group({
        paths: this.fb.control([]),
      }),
    });
  }

  adaptResourceModel(rule: IngressRuleFormModel) {
    if (!rule) {
      return this.getDefaultFormModel();
    }
    return {
      subDomain: '',
      domain: rule.host || '',
      host: rule.host || '',
      http: rule.http,
      secretName: rule.secretName || '',
    };
  }

  adaptFormModel(rule: IngressRuleFormModel) {
    const extensive = rule.domain && rule.domain.startsWith('*.');
    const resource: IngressRuleFormModel = {
      host: !extensive
        ? rule.domain
        : rule.subDomain
        ? `${rule.subDomain}.${rule.domain.slice(2)}`
        : rule.domain,
      http: rule.http,
    };
    if (rule.secretName) {
      resource.secretName = rule.secretName;
    }
    if (resource.http.paths.length === 0) {
      delete resource.http;
    }
    return resource;
  }

  getDefaultFormModel(): IngressRuleFormModel {
    return {
      domain: '',
      subDomain: '',
      secretName: '',
      http: {
        paths: [],
      },
    };
  }

  setSubDomainControlState() {
    if (this.form.get('domain').value.startsWith('*.')) {
      this.form.get('subDomain').enable({ emitEvent: false });
    } else {
      this.form.get('subDomain').disable({
        emitEvent: false,
      });
      this.form.get('subDomain').setValue('', {
        emitEvent: false,
      });
    }
  }

  getDomainDisplay(value: string) {
    return value && value.startsWith('*.') ? `.${value.slice(2)}` : value;
  }

  isSubdomainDisabled() {
    return this.form.get('subDomain').disabled;
  }
}
