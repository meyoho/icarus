import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { Ingress, RESOURCE_TYPES } from 'app/typings';

@Injectable()
export class IngressUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
  ) {}

  delete(resource: Ingress, cluster: string) {
    return this.dialog
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_ingress_confirm', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.INGRESS,
              cluster,
              resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.message.success(this.translate.get('delete_success'));
      });
  }
}
