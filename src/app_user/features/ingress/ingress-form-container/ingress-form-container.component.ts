import {
  K8sApiService,
  ObservableInput,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { safeDump, safeLoad } from 'js-yaml';
import { intersection } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, finalize, pluck, switchMap } from 'rxjs/operators';

import {
  Ingress,
  RESOURCE_TYPES,
  Service,
  WorkspaceBaseParams,
} from 'app/typings';
import { K8S_RESOURCE_NAME_BASE, yamlWriteOptions } from 'app/utils';

import { getDefaultIngress, getIngressPaths } from '../util';

interface IngressCheckItem {
  name: string;
  paths: string[];
}
@Component({
  selector: 'rc-ingress-form-container',
  styleUrls: ['./ingress-form-container.style.scss'],
  templateUrl: './ingress-form-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressFormContainerComponent implements OnInit {
  @ViewChild('pathsNotification', { static: false })
  pathsNotification: TemplateRef<any>;

  @Input()
  data: Ingress;

  @ObservableInput(true)
  baseParams$: Observable<WorkspaceBaseParams>;

  @Input() baseParams: WorkspaceBaseParams;
  @Output() afterSubmit = new EventEmitter<Ingress>();
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  services$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sApi
        .getResourceList<Service>({
          type: RESOURCE_TYPES.SERVICE,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
          publishRef(),
        ),
    ),
  );

  submitting = false;
  ingressYaml: string;
  originalYaml: string;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  mode = 'UI';
  ingress: Ingress;

  codeEditorOptions = yamlWriteOptions;

  duplicatedIngressItem: IngressCheckItem[];
  private dialogRef: DialogRef;

  constructor(
    private readonly location: Location,
    private readonly notification: NotificationService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    private readonly dialog: DialogService,
  ) {}

  ngOnInit() {
    this.ingress = this.data || getDefaultIngress(this.baseParams.namespace);
  }

  modeChange(event: string) {
    switch (event) {
      case 'YAML':
        this.originalYaml = this.ingressYaml = safeDump(this.ingress);
        break;
      case 'UI':
        this.ingress = this.yamlToForm(this.ingressYaml);
        break;
    }
  }

  yamlToForm(yaml: string) {
    let formModel: Ingress;
    try {
      formModel = safeLoad(yaml);
    } catch (e) {
      this.notification.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return formModel;
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const payload = this.getPayload();
    if (!payload) {
      this.submitting = false;
      return;
    }

    this.k8sApi
      .getResourceList({
        type: RESOURCE_TYPES.INGRESS,
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      )
      .subscribe(items => {
        const _items = this.data
          ? items.filter(i => i.metadata.name !== this.data.metadata.name)
          : items;
        if (this.checkIngressPath(payload, _items)) {
          this.submit(payload);
        } else {
          this.submitting = false;
          this.cdr.markForCheck();
        }
      });
  }

  cancel() {
    this.location.back();
  }

  closeNotificationDialog() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }

  private getPayload() {
    let payload;
    if (this.mode === 'YAML') {
      payload = this.yamlToForm(this.ingressYaml);
      if (payload.metadata) {
        payload.metadata.namespace = this.baseParams.namespace;
      }
    } else {
      payload = this.ingress;
    }
    return payload;
  }

  private checkIngressPath(ingress: Ingress, items: Ingress[]) {
    const paths = getIngressPaths(ingress);
    const ingressCheckItems: IngressCheckItem[] = items.map(item => ({
      name: item.metadata.name,
      paths: getIngressPaths(item),
    }));
    this.duplicatedIngressItem = ingressCheckItems
      .map(item => ({
        name: item.name,
        paths: intersection(paths, item.paths),
      }))
      .filter(i => i.paths.length);
    if (this.duplicatedIngressItem.length > 0) {
      this.dialogRef = this.dialog.open(this.pathsNotification, {
        size: DialogSize.Medium,
      });
      return false;
    } else {
      return true;
    }
  }

  private submit(payload: Ingress) {
    const params = {
      type: RESOURCE_TYPES.INGRESS,
      resource: payload,
      cluster: this.baseParams.cluster,
    };
    this.k8sApi[this.data ? 'putResource' : 'postResource'](params)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get(this.data ? 'update_success' : 'create_success'),
        );
        this.afterSubmit.emit(payload);
      });
  }
}
