import { ifExist } from '@alauda/common-snippet';
import { flatMap, get } from 'lodash-es';

import { Ingress } from 'app/typings';

export function getDefaultIngress(namespace: string, name = ''): Ingress {
  return {
    apiVersion: 'extensions/v1beta1',
    kind: 'Ingress',
    metadata: {
      name,
      namespace,
      annotations: {
        'kubernetes.io/ingress.class': '',
      },
    },
    spec: {
      tls: [],
      rules: [
        {
          http: {
            paths: [
              {
                path: '',
                backend: {
                  serviceName: '',
                  servicePort: '',
                },
              },
            ],
          },
        },
      ],
    },
  };
}

export function getIngressPaths(ingress: Ingress) {
  const tls = get(ingress, ['spec', 'tls']) || [];
  const tlsHosts = flatMap(tls, ({ hosts }) => hosts || []);
  const rules = get(ingress, ['spec', 'rules']) || [];
  return flatMap(rules, rule => {
    const paths = get(rule, ['http', 'paths']) || [];
    const host = rule.host || '';
    return paths.map(
      ({ path }) =>
        ifExist(
          host,
          'http' +
            ifExist<boolean | string>(tlsHosts.includes(host), 's') +
            '://' +
            host,
        ) + path,
    );
  });
}
