import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Ingress } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-ingress-form-container
      [baseParams]="baseParams$ | async"
      (afterSubmit)="afterSubmit($event)"
    ></rc-ingress-form-container>
  `,
})
export class IngressCreateComponent {
  baseParams$ = this.workspaceComponent.baseParams;
  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly router: Router,
  ) {}

  afterSubmit(ingress: Ingress) {
    this.router.navigate(['ingress/detail', ingress.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
