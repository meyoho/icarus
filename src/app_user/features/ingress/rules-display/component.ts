import { ObservableInput, ifExist, publishRef } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { flatMap, get } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { Ingress, IngressRule, WorkspaceBaseParams } from 'app/typings';

@Component({
  selector: 'rc-ingress-rules-display',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class IngressRulesDisplayComponent {
  @Input() baseParams: WorkspaceBaseParams;

  @Input()
  ingress: Ingress;

  @Input()
  inTable: boolean;

  @Input()
  inForm: boolean;

  @Input()
  limit: number;

  @ObservableInput(true)
  ingress$: Observable<Ingress>;

  @ObservableInput(true)
  inForm$: Observable<boolean>;

  @ObservableInput(true)
  inTable$: Observable<boolean>;

  @ObservableInput(true, 3)
  limit$: Observable<number>;

  columns$ = this.inForm$.pipe(
    map(inForm =>
      inForm
        ? ['address', 'service', 'port', 'secret']
        : ['address', 'service', 'port'],
    ),
    publishRef(),
  );

  total: number;

  private readonly rules$ = this.ingress$.pipe(
    map(ingress => this.getRules(ingress)),
    publishRef(),
  );

  total$ = this.rules$.pipe(map(rules => rules.length));

  shownRules$ = combineLatest([this.rules$, this.inTable$, this.limit$]).pipe(
    map(([rules, inTable, limit]) => {
      return inTable && Number.isFinite(limit) && limit < rules.length
        ? rules.slice(0, limit)
        : rules;
    }),
    publishRef(),
  );

  normalizeLink({
    host,
    address,
    secret,
  }: {
    host: string;
    address: string;
    secret: string;
  }) {
    if (host) {
      return `http${ifExist<string>(secret, 's')}://${address}`;
    }

    // without host
    return 'javascript:;';
  }

  private getRules(ingress: Ingress) {
    if (!ingress.spec.rules) {
      return [];
    }
    return flatMap(ingress.spec.rules, item => {
      const secretName = this.getSecretNameByRule(item);
      if (item.http && item.http.paths.length > 0) {
        return item.http.paths.map(path => ({
          host: item.host,
          address: `${item.host || ''}${path.path || ''}`,
          service: path.backend.serviceName,
          port: path.backend.servicePort,
          secret: secretName,
        }));
      } else {
        return {
          host: item.host,
          address: item.host,
          service: '',
          port: '',
          secret: secretName,
        };
      }
    });
  }

  private getSecretNameByRule(rule: IngressRule): string {
    const tls = get(this.ingress, ['spec', 'tls'], []).find(
      item => item.hosts && item.hosts.includes(rule.host),
    );
    return tls ? tls.secretName : '';
  }
}
