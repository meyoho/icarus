import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { Ingress, RESOURCE_TYPES, WorkspaceListParams } from 'app/typings';
import { ACTION } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { IngressUtilService } from '../util.service';
@Component({
  templateUrl: './ingress-list.component.html',
  styleUrls: ['./ingress-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressListComponent {
  columns = [NAME, 'rule', 'created_time', ACTION];
  list: K8SResourceList<Ingress>;
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  permissions$ = this.k8sPermission
    .getAccess({
      type: RESOURCE_TYPES.INGRESS,
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
      action: COMMON_WRITABLE_ACTIONS,
    })
    .pipe(isAllowed());

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApiService: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly ingressUtil: IngressUtilService,
  ) {
    const fetchParams$ = combineLatest([
      this.activatedRoute.queryParams,
      this.workspaceComponent.baseParams,
    ]).pipe(
      map(([queryParams, params]) => Object.assign({}, queryParams, params)),
    );

    this.list = new K8SResourceList<Ingress>({
      fetcher: this.fetchResources.bind(this),
      fetchParams$,
      limit: 20,
    });
  }

  fetchResources({ cluster, namespace, ...queryParams }: WorkspaceListParams) {
    return this.k8sApiService.getResourceList({
      type: RESOURCE_TYPES.INGRESS,
      cluster,
      namespace,
      queryParams,
    });
  }

  update(item: Ingress) {
    this.router.navigate(['ingress', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  view(item: Ingress) {
    this.router.navigate(['ingress', 'detail', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(item: Ingress) {
    this.ingressUtil
      .delete(item, this.workspaceComponent.baseParamsSnapshot.cluster)
      .then(() => {
        this.list.delete(item);
      });
  }

  createIngress() {
    this.router.navigate(['ingress', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }
}
