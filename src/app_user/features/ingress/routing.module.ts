import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IngressCreateComponent } from './ingress-create/ingress-create.component';
import { IngressDetailComponent } from './ingress-detail/ingress-detail.component';
import { IngressListComponent } from './ingress-list/ingress-list.component';
import { IngressUpdateComponent } from './ingress-update/ingress-update.component';

const routes: Routes = [
  {
    path: '',
    component: IngressListComponent,
  },
  {
    path: 'create',
    component: IngressCreateComponent,
  },
  {
    path: 'detail/:name',
    component: IngressDetailComponent,
  },
  {
    path: 'update/:name',
    component: IngressUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngressRoutingModule {}
