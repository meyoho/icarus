import { K8sApiService, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Ingress, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-loading-mask
      *ngIf="!(data$ | async)"
      [loading]="true"
    ></rc-loading-mask>
    <rc-ingress-form-container
      *ngIf="data$ | async"
      [data]="data$ | async"
      [baseParams]="baseParams$ | async"
      (afterSubmit)="afterSubmit($event)"
    ></rc-ingress-form-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressUpdateComponent {
  baseParams$ = this.workspaceComponent.baseParams;
  data$ = combineLatest([this.activatedRoute.params, this.baseParams$]).pipe(
    map(([params, baseParams]) => ({
      name: params.name,
      ...baseParams,
    })),
    switchMap(({ cluster, namespace, name }) =>
      this.k8sApi.getResource<Ingress>({
        type: RESOURCE_TYPES.INGRESS,
        cluster,
        namespace,
        name,
      }),
    ),
    publishRef(),
  );

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
  ) {}

  afterSubmit(ingress: Ingress) {
    this.router.navigate(['ingress/detail', ingress.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
