import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { IngressFormComponent } from './ingress-form/component';
import { IngressPathsFormComponent } from './ingress-form/ingress-paths-form/component';
import { IngressRuleFormComponent } from './ingress-form/ingress-rule-form/component';
import { IngressRulesDisplayComponent } from './rules-display/component';
const COMPONENTS = [
  IngressFormComponent,
  IngressRuleFormComponent,
  IngressPathsFormComponent,
  IngressRulesDisplayComponent,
];
@NgModule({
  imports: [SharedModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class IngressSharedModule {}
