import { NgModule } from '@angular/core';
import { LogSharedModule } from 'submodules/ait-shared/log/shared.module';

import { SharedModule } from 'app/shared/shared.module';
import { LogRoutingModule } from 'app_user/features/log/routing.module';

import { UserLogComponent } from './component';
import { LogDashboardComponent } from './dashboard/component';

@NgModule({
  imports: [SharedModule, LogRoutingModule, LogSharedModule],
  declarations: [UserLogComponent, LogDashboardComponent],
})
export class LogModule {}
