import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserLogComponent } from './component';

const routes: Routes = [
  {
    path: '',
    component: UserLogComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogRoutingModule {}
