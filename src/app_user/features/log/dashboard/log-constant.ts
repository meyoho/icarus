export const TIME_STAMP_OPTIONS = [
  {
    type: 'custom_time_range',
    offset: 0,
  },
  {
    type: 'last_10_minutes',
    offset: 10 * 60 * 1000,
  },
  {
    type: 'last_30_minutes',
    offset: 30 * 60 * 1000,
  },
  {
    type: 'last_hour',
    offset: 60 * 60 * 1000,
  },
  {
    type: 'last_3_hours',
    offset: 3 * 3600 * 1000,
  },
  {
    type: 'last_6_hours',
    offset: 6 * 3600 * 1000,
  },
  {
    type: 'last_12_hours',
    offset: 12 * 3600 * 1000,
  },
  {
    type: 'last_day',
    offset: 24 * 3600 * 1000,
  },
  {
    type: 'last_2_days',
    offset: 2 * 24 * 3600 * 1000,
  },
  {
    type: 'last_3_days',
    offset: 3 * 24 * 3600 * 1000,
  },
  {
    type: 'last_5_days',
    offset: 5 * 24 * 3600 * 1000,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 3600 * 1000,
  },
];
