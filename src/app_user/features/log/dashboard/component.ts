import {
  API_GATEWAY,
  K8sApiService,
  ObservableInput,
  StringMap,
  TranslateService,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import dayjs from 'dayjs';
import Highcharts from 'highcharts';
import {
  findIndex,
  get,
  isEmpty,
  map as lodashMap,
  mergeWith,
  slice,
} from 'lodash-es';
import { BehaviorSubject, EMPTY, Observable, combineLatest } from 'rxjs';
import {
  catchError,
  map,
  pluck,
  skipUntil,
  switchMap,
  tap,
} from 'rxjs/operators';

import { DATE_FORMAT } from 'app/features-shared/event/k8s-event-list/k8s-event.constant';
import { AdvanceApiService } from 'app/services/api/advance.service';
import {
  Log,
  RESOURCE_TYPES,
  ResourceType,
  WorkspaceBaseParams,
} from 'app/typings';

import { TIME_STAMP_OPTIONS } from './log-constant';

interface SeriesChartData {
  name: string | number | Date;
  value: number;
}

interface LogItem {
  application_name: string;
  cluster_name: string;
  container_id: string;
  container_id8: string;
  docker_container_name: string;
  file_name: string;
  kubernetes_container_name: string;
  kubernetes_namespace: string;
  log_data: string;
  log_id: string;
  log_index: string;
  log_level: number;
  log_type: string;
  node: string;
  nodes: string;
  pod_id: string;
  pod_name: string;
  project_name: string;
  region_id: string;
  region_name: string;
  root_account: string;
  time: number;
}

interface Logs {
  items: Array<{
    spec: {
      data: LogItem;
    };
  }>;
  total_items: number;
  total_page: number;
}

interface Types {
  clusters?: string[];
  services?: string[];
  nodes?: string[];
  paths?: string[];
  applications?: string[];
  components?: string[];
  sources?: string[];
}

interface TypeResult {
  items: Types[];
}

interface Aggregations {
  items: Array<{
    count: number;
    time: number;
  }>;
}

const LOG_URL = `${API_GATEWAY}/v4/logs/`;

@Component({
  selector: 'rc-log-dashboard',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class LogDashboardComponent implements OnInit {
  constructor(
    private readonly advanceApiService: AdvanceApiService,
    private readonly httpClient: HttpClient,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly auiNotificationService: NotificationService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}

  baseParams: WorkspaceBaseParams;
  // 在用户视角下设置传入当前project、namespace和cluster，管理视角则不需要
  @ObservableInput()
  @Input('baseParams')
  baseParams$: Observable<WorkspaceBaseParams>;

  isDeployed$ = this.advanceApiService.getDiagnoseInfo().pipe(publishRef());

  loadingSuggestions = true;
  hasQueryString = false;
  loadingLogs = false;
  loadingSeries = false;
  showChart = false;
  logTypes: Types;
  tags: Array<{ type: string; name: string }> = [];
  tagRegex = /\w+: .+/;
  _bucketsInterval = 0; // interval of two buckets
  Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    chart: {
      type: 'column',
    },
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    title: {
      text: null as string,
    },
    tooltip: {
      xDateFormat: '%Y-%m-%d %H:%M:%S',
    },
    plotOptions: {
      column: {
        borderRadius: 1,
        color: '#8bd6fa',
        events: {
          click: this.onChartSelect.bind(this),
        },
      },
    },
    xAxis: {
      dateTimeLabelFormats: {
        day: '%m-%d',
      },
      type: 'datetime',
      crosshair: true,
    },
    yAxis: {
      title: {
        text: null as string,
      },
    },
  };

  updated$ = new BehaviorSubject(null);
  suggestions$ = this.baseParams$.pipe(
    skipUntil(this.isDeployed$),
    tap(baseParams => {
      this.baseParams = baseParams;
      this.loadingSuggestions = true;
    }),
    switchMap(baseParams =>
      this.httpClient.get<TypeResult>(LOG_URL + 'types', {
        params: baseParams
          ? {
              projects: baseParams.project,
              namespaces: baseParams.namespace,
            }
          : null,
      }),
    ),
    pluck('items'),
    map(types =>
      types.reduce((acc, cur) => {
        const suggestion = Object.entries(cur);
        if (suggestion.length > 0) {
          const [type, arr] = suggestion[0];
          return arr
            ? acc.concat(
                arr.map((name: string) => ({
                  type: type.endsWith('s') ? type.slice(0, -1) : type,
                  name,
                })),
              )
            : acc;
        } else {
          return acc;
        }
      }, []),
    ),
    tap(() => {
      this.loadingSuggestions = false;
    }),
  );

  logParams$ = combineLatest([this.baseParams$, this.updated$]).pipe(
    map(([baseParams]) => {
      this.logCollapseMap = {};
      if (this.timestampOption !== 'custom_time_range') {
        this.resetTimeRange();
      }
      const startTime = this.dateValue(this.startTime);
      const endTime = this.dateValue(this.endTime);
      if (!startTime || !endTime) {
        this.auiNotificationService.warning(
          this.translateService.get('log_query_timerange_required'),
        );
        return {};
      }
      if (
        this.timestampOption === 'custom_time_range' &&
        startTime >= endTime
      ) {
        this.auiNotificationService.warning(
          this.translateService.get('log_query_timerange_warning'),
        );
        return {};
      }
      return this.paramsCompletion(startTime, endTime, baseParams);
    }),
    publishRef(),
  );

  logs$ = this.logParams$.pipe(
    tap(() => {
      this.loadingLogs = true;
    }),
    switchMap(params =>
      this.httpClient.get<Logs>(LOG_URL + 'search', {
        params,
      }),
    ),
    map(res => {
      if (Array.isArray(res) || !res.items) {
        this.pagination.count = 0;
        return [];
      }
      this.pagination.count = res.total_items > 10000 ? 10000 : res.total_items;
      this.pagination.pages = Math.ceil(
        this.pagination.count / this.pagination.size,
      );
      this.logCache = {};
      return res.items.map(item => item.spec.data);
    }),
    tap(() => {
      this.loadingLogs = false;
    }),
    skipError(),
    publishRef(),
  );

  aggregations$ = this.logParams$.pipe(
    tap(() => {
      this.loadingSeries = true;
      this.showChart = false;
    }),
    switchMap(params =>
      this.httpClient.get<Aggregations>(LOG_URL + 'aggregations', {
        params,
      }),
    ),
    map(res => {
      if (!get(res, 'items.length')) {
        // the buckets length can't equal 0 except time offset is 0
        return;
      }
      this.showChart = true;
      const buckets = res.items;
      buckets.forEach(bucket => {
        const _date = new Date(bucket.time * 1000);
        bucket.time = _date.getTime();
      });
      if (buckets.length > 2) {
        this._bucketsInterval = buckets[1].time - buckets[0].time;
      }
      const totalCount = buckets.reduce(
        (result, bucket) => result + bucket.count,
        0,
      );
      this.renderLogEventsChart(buckets);
      return totalCount + ' Logs';
    }),
    tap(() => {
      this.loadingSeries = false;
    }),
    catchError(() => {
      this.chartData = [];
      this.pagination.count = 0;
      return EMPTY;
    }),
  );

  currentTime = dayjs();
  startTime = this.currentTime
    .clone()
    .startOf('day')
    .subtract(6, 'day')
    .format(DATE_FORMAT);

  endTime = this.currentTime.format(DATE_FORMAT);

  dateTimeOptions = {
    maxDate: this.currentTime.clone().endOf('day').valueOf(),
    minDate: this.currentTime
      .clone()
      .startOf('day')
      .subtract(6, 'day')
      .valueOf(),
    dateFormat: 'Y-m-d H:i:S',
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };

  logCache: Record<
    string,
    {
      before: LogItem[];
      later: LogItem[];
    }
  > = {};

  logCollapseMap: Record<string, boolean> = {};
  logLoadingMap: Record<string, boolean> = {};
  timestampOptions = TIME_STAMP_OPTIONS.map(option => ({
    ...option,
    name: this.translateService.get(option.type),
  }));

  timestampOption = 'last_30_minutes';
  pagination = {
    count: 0,
    size: 50,
    current: 1,
    pages: 0,
  };

  chartData: SeriesChartData[];
  trackFn = (val: any = {}) => {
    if (val.type) {
      return `${val.type}: ${val.name}`;
    } else if (this.tagRegex.test(val)) {
      return val;
    } else {
      return `search: ${val}`;
    }
  };

  filterFn = (filter: string, option: any) =>
    `${option.value.type}: ${option.value.name}`.includes(filter);

  ngOnInit() {
    this.k8sApi
      .getGlobalResource<Log>({
        type: RESOURCE_TYPES.LOG,
        namespaced: true,
        name: 'alauda-es-config',
      })
      .subscribe(logConfigs => {
        if (logConfigs.spec.LOG_TTL) {
          this.dateTimeOptions.minDate = this.currentTime
            .clone()
            .startOf('day')
            .subtract(logConfigs.spec.LOG_TTL, 'day')
            .valueOf();
        }
      });
  }

  currentPageChange(_currentPage: number) {
    this.updated$.next(null);
  }

  pageSizeChange(size: number) {
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.updated$.next(null);
  }

  fetchLogs() {
    this.pagination.current = 1;
    this.updated$.next(null);
  }

  onTimestampChanged(type: string) {
    this.timestampOption = type;
    this.fetchLogs();
  }

  addKeywordByClick(type: string, value: string) {
    if (!this.tags.find(element => element.name === value)) {
      this.tags = this.tags.concat([
        {
          type,
          name: value,
        },
      ]);
    }
  }

  tagClassFn(_label: string, tag: any) {
    // init aui-multi-select tagClass
    if (tag.type) {
      return `tag-${tag.type}`;
    } else if (typeof tag === 'string' && /\w+: .+/.test(tag)) {
      return `tag-${/(\w+): .+/.exec(tag)[1]}`;
    }
  }

  private paramsCompletion(
    startTime: number,
    endTime: number,
    baseParams: WorkspaceBaseParams,
  ) {
    let payload: StringMap = {
      pageno: String(this.pagination.current),
      size: String(this.pagination.size),
      start_time: String(startTime),
      end_time: String(endTime),
      ...this.generateQueryFromTagObjects(this.tags),
    };
    // 在用户视角下设置固定的projects、namespaces和clusters，管理视角则不需要
    if (baseParams) {
      payload = {
        ...payload,
        projects: baseParams.project,
        namespaces: baseParams.namespace,
        clusters: baseParams.cluster,
      };
    }
    // 补充 paths
    if (!payload.paths) {
      payload.paths = this.tags
        .filter(tag => tag.type === 'path')
        .map(tag => tag.name)
        .join(',');
    }
    // 补充 query_string
    this.hasQueryString = false;
    if (payload.search) {
      this.hasQueryString = true;
      payload.query_string = payload.search;
      delete payload.search;
    }
    return payload;
  }

  generateQueryFromTagObjects(
    tags: Array<string | { type: string; name: string }>,
  ) {
    const tagMap = lodashMap(tags, tag => {
      // from[{type:service,name:'a',uuid:'x'}, {type:node,name:'b'}, 'search string'] to [{services:a},{nodes:b},{search:c}]
      const param = {} as Record<string, string>;
      if (typeof tag === 'string') {
        const regex = /(\w+): (.+)/;
        if (regex.test(tag)) {
          const regArr = regex.exec(tag);
          param[regArr[1] + 's'] = regArr[2];
        } else {
          param.search = tag;
        }
      } else {
        param[tag.type + 's'] = tag.name;
      }
      return param;
    });
    return tagMap.reduce(
      (result, param) =>
        mergeWith(result, param, (pre, cur) => {
          // from [{services:a},{services:b},{clusters:c},{clusters:d},{search:e}] to {services:'a,b',clusters:'c,d',search:'e'}
          if (pre) {
            return [pre, cur].join(',');
          } else {
            return cur;
          }
        }),
      {},
    );
  }

  private resetTimeRange() {
    const timeRange = TIME_STAMP_OPTIONS.find(
      option => option.type === this.timestampOption,
    );
    this.endTime = dayjs().format(DATE_FORMAT);
    this.startTime = dayjs()
      .startOf('second')
      .subtract(timeRange.offset, 'ms')
      .format(DATE_FORMAT);
  }

  onStartTimeSelect(time: string) {
    this.startTime = time;
  }

  onEndTimeSelect(time: string) {
    this.endTime = time;
  }

  private dateValue(date: string) {
    return dayjs(date).valueOf() / 1000;
  }

  private renderLogEventsChart(
    buckets: Array<{
      time: number;
      count: number;
    }> = [],
  ) {
    this.chartData = buckets.map(bucket => ({
      name: bucket.time,
      value: bucket.count,
    }));
    this.chartOptions.series = [
      {
        type: 'column',
        name: this.translateService.get('log'),
        data: buckets.map(bucket => ({
          x: bucket.time,
          y: bucket.count,
        })),
      },
    ];
  }

  onChartSelect(event: {
    point: {
      x: number;
    };
  }) {
    if (this._bucketsInterval < 100) {
      return;
    }
    this.timestampOption = 'custom_time_range';
    this.startTime = dayjs(event.point.x).format(DATE_FORMAT);
    this.endTime = dayjs(event.point.x + this._bucketsInterval).format(
      DATE_FORMAT,
    );
    this.fetchLogs();
  }

  getTip(val: Date) {
    return dayjs(val).format('YYYY-MM-DD HH:mm:ss');
  }

  logInsight(
    log: LogItem,
    direction: string,
    time: number,
    timeViewMore?: number,
  ) {
    const { log_index, log_type, log_id } = log;
    if (!direction) {
      this.logCollapseMap[log.time] = !this.logCollapseMap[log.time];
      if (this.logCache[log.time]) {
        return;
      }
    }

    this.loadingLogs = true;
    this.logLoadingMap[timeViewMore || time || log.time] = true;
    this.httpClient
      .get<Logs>(LOG_URL + 'context', {
        params: {
          size: '5',
          log_index,
          log_type,
          log_id,
          direction: direction || '',
        },
      })
      .pipe(
        catchError(() => {
          this.logCollapseMap[log.time] = false;
          this.loadingLogs = false;
          this.logLoadingMap[timeViewMore || time || log.time] = false;
          return EMPTY;
        }),
      )
      .subscribe(data => {
        if (
          !isEmpty(data.items) &&
          (direction || (!direction && data.items.length > 1))
        ) {
          this.showContextByMode(
            log,
            data.items.map(item => item.spec.data),
            direction,
            time,
          );
        } else {
          this.messageService.warning(
            this.translateService.get('no_more_logs'),
          );
        }
        this.loadingLogs = false;
        this.logLoadingMap[timeViewMore || time || log.time] = false;
      });
  }

  private showContextByMode(
    log: LogItem,
    logs: LogItem[],
    mode = '',
    time: number,
  ) {
    const logTime = time || log.time;
    const index = findIndex(logs, item => {
      return item.time === logTime;
    });
    switch (mode) {
      case '':
        this.logCache[logTime] = {
          later: slice(logs, 0, index),
          before: slice(logs, index + 1),
        };
        break;
      case 'before':
        this.logCache[logTime].before = this.logCache[logTime].before.concat(
          logs,
        );
        break;
      case 'later':
        this.logCache[logTime].later = logs.concat(
          this.logCache[logTime].later,
        );
    }
  }
}
