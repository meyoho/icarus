import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { pluck } from 'rxjs/operators';

import { FeatureGateApiService } from 'app/services/api/feature-gate.service';
import { WorkspaceBaseParams } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <ng-container *ngIf="(logPolicyEnabled$ | async) === true">
      <acl-log-dashboard [baseParams]="baseParams"></acl-log-dashboard>
    </ng-container>
    <ng-container *ngIf="(logPolicyEnabled$ | async) === false">
      <rc-log-dashboard [baseParams]="baseParams"></rc-log-dashboard>
    </ng-container>
  `,
})
export class UserLogComponent implements OnInit {
  baseParams: WorkspaceBaseParams;
  logPolicyEnabled$ = this.featureApi
    .getGlobalGate('logpolicy')
    .pipe(pluck('status', 'enabled'));

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly featureApi: FeatureGateApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.cdr.markForCheck();
  }
}
