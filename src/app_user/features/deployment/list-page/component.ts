import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <rc-workload-list></rc-workload-list>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {}
