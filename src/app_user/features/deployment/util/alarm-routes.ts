import { AlarmDetailComponent } from 'app/features-shared/alarm/detail/component';
import { AlarmFormComponent } from 'app/features-shared/alarm/form/component';

export const alarmRoutes = {
  path: 'alert',
  children: [
    {
      path: 'create',
      component: AlarmFormComponent,
    },
    {
      path: 'update/:alarmname',
      component: AlarmFormComponent,
    },
    {
      path: 'detail/:name',
      component: AlarmDetailComponent,
    },
  ],
};
