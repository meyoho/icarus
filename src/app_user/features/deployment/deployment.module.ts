import { NgModule } from '@angular/core';

import { WORKLOAD_TYPE } from 'app/features-shared/workload/inject-tokens';
import { WorkloadSharedModule } from 'app/features-shared/workload/shared.module';
import { UtilService } from 'app/features-shared/workload/util.service';
import { SharedModule } from 'app/shared/shared.module';
import { RESOURCE_TYPES } from 'app/typings';

import { CreateFromYamlComponent } from './create-from-yaml/component';
import { CreateComponent } from './create/component';
import { DeploymentRoutingModule } from './deployment.routing.module';
import { DetailPageComponent } from './detail-page/component';
import { ListPageComponent } from './list-page/component';
import { UpdateComponent } from './update/component';

@NgModule({
  imports: [DeploymentRoutingModule, SharedModule, WorkloadSharedModule],
  providers: [
    {
      provide: WORKLOAD_TYPE,
      useValue: RESOURCE_TYPES.DEPLOYMENT,
    },
    UtilService,
  ],
  declarations: [
    ListPageComponent,
    DetailPageComponent,
    CreateComponent,
    CreateFromYamlComponent,
    UpdateComponent,
  ],
})
export class DeploymentModule {}
