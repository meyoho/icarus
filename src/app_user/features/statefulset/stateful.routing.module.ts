import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { alarmRoutes } from 'app_user/features/deployment/util/alarm-routes';

import { CreateFromYamlComponent } from './create-from-yaml/component';
import { CreateComponent } from './create/component';
import { DetailPageComponent } from './detail-page/component';
import { ListPageComponent } from './list-page/component';
import { UpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: ListPageComponent,
  },
  {
    path: 'detail/:name',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: DetailPageComponent,
      },
      alarmRoutes,
    ],
  },
  {
    path: 'create',
    component: CreateComponent,
  },
  {
    path: 'create_from_yaml',
    component: CreateFromYamlComponent,
  },
  {
    path: 'update/:name',
    component: UpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatefulSetRoutingModule {}
