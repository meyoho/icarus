import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <rc-workload-create></rc-workload-create>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent {}
