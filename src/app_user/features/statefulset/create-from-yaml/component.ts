import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <rc-create-workload-from-yaml></rc-create-workload-from-yaml>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateFromYamlComponent {}
