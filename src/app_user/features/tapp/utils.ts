import { TApp, TAppStatus } from 'app/typings';

export function parseToWorkloadStatus(data: TApp) {
  const specReplicas = data.spec.replicas || 0;
  const status: TAppStatus = data.status || {
    appStatus: 'Pending',
    replicas: 0,
    statuses: {},
  };
  if (specReplicas === 0 && status.replicas === 0) {
    return {
      status: 'stopped',
      desired: 0,
      current: 0,
    };
  }
  const current = Object.values(status.statuses || {}).filter(
    v => v === 'Running',
  ).length;
  return {
    status: status.appStatus.toLowerCase(),
    desired: specReplicas || 0,
    current,
  };
}

export const demoTemplate = `---
apiVersion: apps.tkestack.io/v1
kind: TApp
metadata:
  name: example-tapp
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: example-tapp
    spec:
      containers:
      - name: nginx
        image: nginx:1.9.1
  templatePool:
    "example":
      metadata:
        labels:
          app: example-tapp
      spec:
        containers:
        - name: nginx
          image: nginx:latest
  templates:
      "1": "example"
  status:
      "1": "Killed"
`;
