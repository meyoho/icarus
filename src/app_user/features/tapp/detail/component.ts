import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  LABELS,
  METADATA,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { equals, path } from 'ramda';
import { combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  first,
  map,
  startWith,
} from 'rxjs/operators';

import { EnvUpdateParams } from 'app/features-shared/app/detail/env/component';
import { UpdateEnvDialogComponent } from 'app/features-shared/app/dialog/update-env/component';
import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  ClusterFeature,
  HorizontalPodAutoscaler,
  Pod,
  RESOURCE_TYPES,
  TApp,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { BlueGreenComponent } from './blue-green/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../../features/app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailComponent {
  @Output()
  redirect = new EventEmitter();

  baseParams = this.workspace.baseParamsSnapshot;
  tabIndex$ = this.activatedRoute.queryParamMap.pipe(
    map(params => parseInt(params.get('tabIndex'), 10) || 0),
    distinctUntilChanged(),
  );

  logInitialState$ = this.activatedRoute.queryParamMap.pipe(
    map(params => ({
      pod: params.get('pod'),
      container: params.get('container'),
    })),
  );

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspace.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{ name: string; cluster: string; namespace: string }>(
      equals,
    ),
    publishRef(),
  );

  showMetricTab$ = this.k8sApi
    .getResourceList<ClusterFeature>({
      type: RESOURCE_TYPES.FEATURE,
      cluster: this.baseParams.cluster,
      queryParams: {
        labelSelector: 'instanceType=prometheus',
      },
    })
    .pipe(
      map(features => {
        return !!features.items.length;
      }),
      startWith(true),
      catchError(() => of(false)),
      publishRef(),
    );

  dataManager = new AsyncDataLoader<{
    resource: TApp;
    autoScaler: HorizontalPodAutoscaler;
  }>({
    params$: this.params$,
    fetcher: params => {
      const hpa$ = this.acpApi
        .getHpaResource({
          workloadType: 'tapp',
          cluster: params.cluster,
          namespace: params.namespace,
          workloadName: params.name,
        })
        .pipe(
          catchError(() => {
            return of(null);
          }),
        );

      const permissions$ = this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.TAPP,
        cluster: params.cluster,
        namespace: params.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      });

      return combineLatest([
        this.k8sApi.watchResource<TApp>({
          type: RESOURCE_TYPES.TAPP,
          cluster: params.cluster,
          namespace: params.namespace,
          name: params.name,
        }),
        hpa$,
        permissions$,
      ]).pipe(
        map(([resource, autoScaler, permissions]) =>
          resource
            ? {
                resource,
                autoScaler,
                permissions,
              }
            : null,
        ),
      );
    },
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly workspace: WorkspaceComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly acpApi: AcpApiService,
  ) {}

  tabIndexChanged(index: number) {
    const currentQueryParams = this.activatedRoute.snapshot.queryParams;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...currentQueryParams,
        tabIndex: index,
      },
    });
  }

  stateChanged(data: { resource: TApp; autoScaler?: HorizontalPodAutoscaler }) {
    if (data) {
      this.dataManager.reload({ ...this.dataManager.snapshot.data, ...data });
    } else {
      this.dataManager.reload();
    }
  }

  viewLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 4,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  killPod({ pod, callback }: { pod: Pod; callback: () => void }) {
    const insKey: string = path([METADATA, LABELS, 'tapp_instance_key'], pod);
    const part = { spec: { statuses: { [insKey]: 'Killed' } } };
    this.dialog
      .confirm({
        title: this.translate.get('pod_forbid_confirm', {
          name: pod.metadata.name,
        }),
        content: this.translate.get('pod_forbid_content'),
        confirmText: this.translate.get('forbid'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) =>
          this.k8sApi
            .patchResource<TApp>({
              type: RESOURCE_TYPES.TAPP,
              cluster: this.baseParams.cluster,
              resource: this.dataManager.snapshot.data.resource,
              part,
            })
            .subscribe(resolve, reject),
      })
      .then(() => {
        this.dataManager.reload();
        // eslint-disable-next-line promise/no-callback-in-promise
        callback();
      })
      .catch(noop);
  }

  updateBlueGreen({ pod, callback }: { pod: Pod; callback: () => void }) {
    this.dialog
      .open(BlueGreenComponent, {
        data: {
          pod,
          tapp: this.dataManager.snapshot.data.resource,
          params: this.baseParams,
        },
        size: DialogSize.Large,
        fitViewport: true,
      })
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.dataManager.reload();
          callback();
        }
      });
  }

  updateEnv(params: EnvUpdateParams) {
    const dialogRef = this.dialog.open(UpdateEnvDialogComponent, {
      size: params.type === 'env' ? DialogSize.Big : DialogSize.Medium,
      data: {
        podController: this.dataManager.snapshot.data.resource,
        baseParams: this.baseParams,
        ...params,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.message.success(this.translate.get('update_success'));
          this.dataManager.reload();
        }
        dialogRef.close();
      });
  }

  jumpToListPage() {
    this.redirect.next();
  }
}
