import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import {
  ImageParams,
  RcImageSelection,
} from 'app/features-shared/image/image.type';
import { Container, Pod, RESOURCE_TYPES, TApp } from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlueGreenComponent {
  tapp: TApp;
  pod: Pod;
  params: ImageParams;
  containers: Container[];
  loading = false;

  constructor(
    @Inject(DIALOG_DATA)
    { pod, tapp, params }: { tapp: TApp; pod: Pod; params: ImageParams },
    private readonly dialogRef: DialogRef,
    private readonly k8sApi: K8sApiService,
    private readonly appShared: AppSharedService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.tapp = tapp;
    this.pod = pod;
    this.params = params;

    this.containers = pod.spec.containers.map(({ name, image }) => ({
      name,
      image,
    }));
  }

  selectImage(container: Container) {
    this.appShared
      .selectImage(this.params)
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.containers = this.containers.map(item => {
            if (item.name === container.name) {
              return {
                name: item.name,
                image: `${res.full_image_name}:${res.tag}`,
              };
            } else {
              return item;
            }
          });
          this.cdr.markForCheck();
        }
      });
  }

  update() {
    this.loading = true;
    const podKey = this.pod.metadata.labels.tapp_instance_key;
    const { target, template } = this.getSource();
    const newTemplate = {
      ...template,
      spec: {
        ...template.spec,
        containers: template.spec.containers.map(item => ({
          ...item,
          image: this.getContainerImage(item.name),
        })),
      },
    };
    const part = {
      spec: {
        templates: {
          [podKey]: target,
        },
        templatePool: {
          [target]: newTemplate,
        },
      },
    };
    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.TAPP,
        cluster: this.params.cluster,
        resource: this.tapp,
        part,
      })
      .subscribe(
        res => {
          this.dialogRef.close(res);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }

  private getSource() {
    const templates = this.tapp.spec.templates || {};
    const podKey = this.pod.metadata.labels.tapp_instance_key;
    const currentTemplate = templates[podKey];
    const couldReuse =
      !!currentTemplate &&
      !Object.entries(templates).some(([key, value]) => {
        return key !== podKey && value === currentTemplate;
      });

    const target = couldReuse
      ? currentTemplate
      : this.genNewTemplateKey(podKey);

    const template = currentTemplate
      ? this.tapp.spec.templatePool[currentTemplate]
      : this.tapp.spec.template;

    return { target, template };
  }

  private genNewTemplateKey(source: string, index = 0): string {
    const keys = Object.keys(this.tapp.spec.templatePool || {});
    const result = index ? `${source}-${index}` : source;

    if (keys.includes(result)) {
      return this.genNewTemplateKey(source, index + 1);
    } else {
      return result;
    }
  }

  private getContainerImage(name: string) {
    return this.containers.find(item => item.name === name).image;
  }
}
