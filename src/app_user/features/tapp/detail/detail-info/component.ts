import {
  ANNOTATIONS,
  K8sApiService,
  K8sResourceAction,
  METADATA,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { compose } from '@ngrx/store';
import { toNumber } from 'lodash-es';
import { assocPath, max, pathOr, toString } from 'ramda';
import { first, tap } from 'rxjs/operators';

import { HpaConfigResource } from 'app/features-shared/app/detail/hpa/type';
import { UpdateWorkloadLabelsDialogComponent } from 'app/features-shared/app/dialog/update-workload-labels/component';
import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import {
  FlattenAffinity,
  flatAffinity,
  getBelongingApp,
  getVolumes,
} from 'app/features-shared/workload/util';
import { TerminalService } from 'app/services/api/terminal.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Environments,
  HorizontalPodAutoscaler,
  RESOURCE_TYPES,
  TApp,
} from 'app/typings';
import {
  CALICO_ANNOTATION_PREFIX,
  KUBE_OVN_ANNOTATION_PREFIX,
} from 'app_admin/features/network/subnet/util';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { parseToWorkloadStatus } from '../../utils';

@Component({
  selector: 'rc-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailInfoComponent {
  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly dialog: DialogService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly terminal: TerminalService,
    private readonly appUtilService: AppUtilService,
    private readonly translate: TranslateService,
    private readonly notification: NotificationService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    this.getIpPool = this.getIpPool.bind(this);
  }

  @Input()
  resource: TApp;

  @Input()
  autoScaler: HorizontalPodAutoscaler;

  @Input()
  permissions: Partial<Record<K8sResourceAction, boolean>> = {};

  @Output()
  stateChange = new EventEmitter<{
    resource: TApp;
    autoScaler?: HorizontalPodAutoscaler;
  } | void>();

  baseParams = this.workspace.baseParamsSnapshot;
  getBelongingApp = getBelongingApp;
  parseToWorkloadStatus = parseToWorkloadStatus;
  path = pathOr('-');
  getVolumes = getVolumes;

  private readonly lastReplicasPath = [
    METADATA,
    ANNOTATIONS,
    `tapp.${this.env.LABEL_BASE_DOMAIN}/last-replica`,
  ];

  update() {
    this.router.navigate(['tapp', 'update', this.resource.metadata.name], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  start() {
    const lastReplicas = compose(
      max(1),
      toNumber,
      pathOr(1, this.lastReplicasPath),
    )(this.resource);
    const part = { spec: { replicas: lastReplicas } };
    this.dialog
      .confirm({
        title: this.translate.get('start'),
        content: this.translate.get(`tapp_start_confirm`, {
          name: this.resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .patchResource({
              type: RESOURCE_TYPES.TAPP,
              cluster: this.baseParams.cluster,
              resource: this.resource,
              part,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(resource => this.stateChange.emit({ resource }), noop);
  }

  stop() {
    const part: TApp = assocPath(
      this.lastReplicasPath,
      compose(toString, pathOr(1, ['spec', 'replicas']))(this.resource),
      { spec: { replicas: 0 } },
    );
    this.dialog
      .confirm({
        title: this.translate.get('stop'),
        content: this.translate.get(`tapp_stop_confirm`, {
          name: this.resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        // eslint-disable-next-line sonarjs/no-identical-functions
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .patchResource({
              type: RESOURCE_TYPES.TAPP,
              cluster: this.baseParams.cluster,
              resource: this.resource,
              part,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(resource => this.stateChange.emit({ resource }), noop);
  }

  delete() {
    return this.dialog
      .confirm({
        title: this.translate.get(`tapp_delete_confirm`, {
          name: this.resource.metadata.name,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.TAPP,
              cluster: this.baseParams.cluster,
              resource: this.resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.router.navigate(['tapp'], {
          relativeTo: this.workspace.baseActivatedRoute,
        });
      }, noop);
  }

  updateReplicas(num: number) {
    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.TAPP,
        cluster: this.baseParams.cluster,
        resource: this.resource,
        part: { spec: { replicas: num } },
      })
      .subscribe(resource => {
        this.stateChange.emit({ resource });
      });
  }

  updateLabels() {
    const dialogRef = this.dialog.open(UpdateWorkloadLabelsDialogComponent, {
      size: DialogSize.Big,
      data: {
        podController: this.resource,
        baseParams: {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
        },
      },
    });
    return dialogRef.componentInstance.close
      .pipe(
        first(),
        tap(() => {
          dialogRef.close();
        }),
      )
      .subscribe(res => {
        if (res) {
          this.notification.success(this.translate.get('update_success'));
          this.stateChange.emit();
        }
      });
  }

  getNodeSelector(resource: TApp) {
    return pathOr({}, ['spec', 'template', 'spec', 'nodeSelector'], resource);
  }

  getIpPool(resource: TApp) {
    const { template } = resource.spec;
    const ipList: string[] = (
      this.k8sUtil.getAnnotation(template, {
        type: 'ip_pool',
        baseDomain: `${KUBE_OVN_ANNOTATION_PREFIX}`,
      }) || ''
    )
      .split(',')
      .filter(i => i);
    if (ipList.length > 0) {
      return ipList;
    }
    return JSON.parse(
      this.k8sUtil.getAnnotation(template, {
        type: 'ipAddrs',
        baseDomain: `${CALICO_ANNOTATION_PREFIX}`,
      }) || '[]',
    );
  }

  getImagePullSecrets(resource: TApp) {
    return pathOr(
      [],
      ['spec', 'template', 'spec', 'imagePullSecrets'],
      resource,
    ).map(({ name }) => name);
  }

  getAffinityInfo(resource: TApp): FlattenAffinity[] {
    const items = compose(
      flatAffinity,
      pathOr({}, ['spec', 'template', 'spec', 'affinity']),
    )(resource);
    return items.length > 0 ? items : null;
  }

  updateImageTag(containerName: string) {
    this.appUtilService
      .updateImageTag({
        podController: this.resource,
        containerName,
        baseParams: this.baseParams,
      })
      .subscribe(res => {
        if (res) {
          this.stateChange.emit();
        }
      });
  }

  updateResourceSize(containerName: string) {
    this.appUtilService
      .updateResourceSize({
        podController: this.resource,
        containerName,
        baseParams: this.baseParams,
      })
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(res => {
        if (res) {
          this.stateChange.emit();
        }
      });
  }

  openTerminal(params: {
    pods: string;
    selectedPod: string;
    container: string;
  }) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (data) {
        this.terminal.openTerminal({
          ctl: this.resource.metadata.name,
          pods: params.pods,
          selectedPod: params.selectedPod,
          container: params.container,
          namespace: this.baseParams.namespace,
          cluster: this.baseParams.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  openLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 4,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  updateHpa(resource: HpaConfigResource) {
    if (resource) {
      this.stateChange.emit();
    }
  }
}
