import {
  K8sApiService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { LocationStrategy } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { safeDump, safeLoadAll } from 'js-yaml';

import { RESOURCE_TYPES, ResourceType } from 'app/typings';
import { createActions, yamlWriteOptions } from 'app/utils';
import { getResourcesFromYaml } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { demoTemplate } from '../utils';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateFromYamlComponent {
  yaml: string;
  yamlDemo: string;
  codeEditorOptions = yamlWriteOptions;
  actionsConfig = createActions;
  loading = false;

  @ViewChild('form', { static: false })
  private readonly ngForm: NgForm;

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly dialog: DialogService,
    private readonly location: LocationStrategy,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.initYamlDemo();
  }

  private initYamlDemo() {
    this.yamlDemo = safeLoadAll(demoTemplate)
      .filter(r => !!r)
      .map((r: KubernetesResource) => {
        return {
          ...r,
          metadata: {
            ...r.metadata,
            namespace: this.workspace.baseParamsSnapshot.namespace,
          },
        };
      })
      .map(r => safeDump(r, { sortKeys: true }))
      .join('---\r\n');
  }

  private yamlToFormModel() {
    try {
      return getResourcesFromYaml(
        this.yaml,
        this.workspace.baseParamsSnapshot.namespace,
      )[0];
    } catch (err) {
      this.notification.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return null;
    }
  }

  useDemoTemplate() {
    this.yaml = this.yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialog.open(template, {
      size: DialogSize.Big,
    });
  }

  create() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    const model = this.yamlToFormModel();
    if (model) {
      this.loading = true;
      this.k8sApi
        .postResource({
          type: RESOURCE_TYPES.TAPP,
          cluster: this.workspace.baseParamsSnapshot.cluster,
          resource: model,
        })
        .subscribe(
          () => {
            this.router.navigate(['tapp', 'detail', model.metadata.name], {
              relativeTo: this.workspace.baseActivatedRoute,
              replaceUrl: true,
            });
          },
          () => {
            this.loading = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  cancel() {
    this.location.back();
  }
}
