import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, TApp } from 'app/typings';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent {
  resource$ = this.activatedRoute.paramMap.pipe(
    map(param => param.get(NAME)),
    switchMap(name =>
      this.k8sApi.getResource({
        type: RESOURCE_TYPES.TAPP,
        cluster: this.workspace.baseParamsSnapshot.cluster,
        namespace: this.workspace.baseParamsSnapshot.namespace,
        name,
      }),
    ),
    map(removeDirtyDataBeforeUpdate),
    map((data: TApp) => {
      data.spec.forceDeletePod = data.spec.forceDeletePod || false;
      return data;
    }),
    publishRef(),
  );

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
  ) {}
}
