import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { RESOURCE_TYPES, ResourceType, TApp } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { parseToWorkloadStatus } from '../utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent implements OnDestroy {
  private readonly baseParams = this.workspace.baseParamsSnapshot;
  parseToWorkloadStatus = parseToWorkloadStatus;
  list: K8SResourceList<TApp>;

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.TAPP,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
    private readonly workspace: WorkspaceComponent,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermission: K8sPermissionService,
    private readonly appSharedService: AppSharedService,
  ) {
    this.list = new K8SResourceList<TApp>({
      fetcher: queryParams =>
        this.k8sApi.getResourceList({
          type: RESOURCE_TYPES.TAPP,
          cluster: workspace.baseParamsSnapshot.cluster,
          namespace: workspace.baseParamsSnapshot.namespace,
          queryParams,
        }),
      activatedRoute,
      watcher: seed =>
        this.k8sApi.watchResourceChange(seed, {
          type: RESOURCE_TYPES.TAPP,
          cluster: workspace.baseParamsSnapshot.cluster,
          namespace: workspace.baseParamsSnapshot.namespace,
        }),
    });
  }

  ngOnDestroy() {
    this.list.destroy();
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  create() {
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['create'], {
            relativeTo: this.activatedRoute,
            queryParams: res,
          });
        }
      });
  }

  createFromYaml() {
    this.router.navigate(['create_from_yaml'], {
      relativeTo: this.activatedRoute,
    });
  }

  update(rowData: TApp) {
    this.router.navigate(['update', rowData.metadata.name], {
      relativeTo: this.activatedRoute,
    });
  }

  delete(rowData: TApp) {
    this.dialog
      .confirm({
        title: this.translate.get('tapp_delete_confirm', {
          name: rowData.metadata.name,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.TAPP,
              cluster: this.baseParams.cluster,
              resource: rowData,
            })
            .subscribe(resolve, () => {
              reject();
            });
        },
      })
      .then(() => {
        this.list.scanItems(items =>
          items.filter(item => item.metadata.name !== rowData.metadata.name),
        );
      })
      .catch(noop);
  }
}
