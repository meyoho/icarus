import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateFromYamlComponent } from './create-from-yaml/component';
import { CreateComponent } from './create/component';
import { DetailComponent } from './detail/component';
import { ListComponent } from './list/component';
import { UpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
  },
  {
    path: 'create',
    component: CreateComponent,
  },
  {
    path: 'create_from_yaml',
    component: CreateFromYamlComponent,
  },
  {
    path: 'update/:name',
    component: UpdateComponent,
  },
  {
    path: 'detail/:name',
    component: DetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TAppRoutingModule {}
