import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { TApp } from 'app/typings';
import {
  checkImageSelectionParam,
  getDefaultPodController,
} from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent implements OnInit {
  baseParams = this.workspace.baseParamsSnapshot;

  resource: TApp;

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly appSharedService: AppSharedService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (!checkImageSelectionParam(queryParams as RcImageSelection)) {
      return this.router.navigate(['tapp'], {
        relativeTo: this.workspace.baseActivatedRoute,
      });
    }

    this.appSharedService
      .getDefaultLimitResources({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      })
      .pipe(
        catchError(() => of({})),
        map(defaultLimit => {
          return getDefaultPodController({
            imageSelectionParams: queryParams,
            namespace: this.baseParams.namespace,
            defaultLimit,
            kind: 'TApp',
          });
        }),
      )
      .subscribe(res => {
        res.spec.forceDeletePod = false;
        res.spec.updateStrategy = { maxUnavailable: 1 };
        this.resource = res;
        this.cdr.markForCheck();
      });
  }
}
