import {
  DISPLAY_NAME,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { compose } from '@ngrx/store';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { pathOr } from 'ramda';

import {
  copyMatchLabels,
  projectSetter,
  serviceSetter,
} from 'app/features-shared/workload/util';
import { TApp } from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

@Component({
  selector: 'rc-tapp-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent extends BaseResourceFormGroupComponent<TApp, TApp>
  implements OnInit {
  @Input()
  isUpdate = false;

  @Input()
  name = '';

  @Input()
  project: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  displayNameKey = `${this.baseDomain}/${DISPLAY_NAME}`;

  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      prefixPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  matchLabels: Record<string, string>;

  private setupInitialMatchLabels: (r: TApp) => TApp;

  constructor(
    injector: Injector,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.setupInitialMatchLabels = compose(
      copyMatchLabels(this.baseDomain),
      projectSetter(this.baseDomain, this.project),
      serviceSetter(this.baseDomain),
    );
  }

  createForm() {
    const specGroup = {
      replicas: this.fb.control(1, [Validators.required, Validators.min(0)]),
      forceDeletePod: this.fb.control(false),
      updateStrategy: this.fb.group({
        maxUnavailable: this.fb.control(''),
      }),
      template: this.fb.control({}),
    };

    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control(this.name, [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ]),
        labels: this.fb.control({}),
        annotations: this.fb.group({
          [this.displayNameKey]: this.fb.control(''),
        }),
      }),
      spec: this.fb.group(specGroup),
    });
  }

  getDefaultFormModel(): TApp {
    return {};
  }

  adaptResourceModel(resource: TApp) {
    if (!this.matchLabels && resource) {
      if (this.isUpdate) {
        this.matchLabels = pathOr(
          {},
          ['spec', 'selector', 'matchLabels'],
          resource,
        );
      } else {
        this.matchLabels = {};
      }
    }
    return resource;
  }

  adaptFormModel(formModel: TApp) {
    return this.isUpdate ? formModel : this.setupInitialMatchLabels(formModel);
  }
}
