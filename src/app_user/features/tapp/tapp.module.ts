import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/features-shared/alarm/shared.module';
import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { PodSharedModule } from 'app/features-shared/pod/shared.module';
import { WORKLOAD_TYPE } from 'app/features-shared/workload/inject-tokens';
import { SharedModule } from 'app/shared/shared.module';
import { RESOURCE_TYPES } from 'app/typings';

import { CreateFromYamlComponent } from './create-from-yaml/component';
import { CreateComponent } from './create/component';
import { FormContainerComponent } from './create/form-container/component';
import { FormComponent } from './create/form/component';
import { BlueGreenComponent } from './detail/blue-green/component';
import { DetailComponent } from './detail/component';
import { DetailInfoComponent } from './detail/detail-info/component';
import { ListComponent } from './list/component';
import { TAppRoutingModule } from './tapp.routing.module';
import { UpdateComponent } from './update/component';

@NgModule({
  imports: [
    SharedModule,
    TAppRoutingModule,
    AppSharedModule,
    AlarmSharedModule,
    ImageSharedModule,
    EventSharedModule,
    PodSharedModule,
  ],
  providers: [
    {
      provide: WORKLOAD_TYPE,
      useValue: RESOURCE_TYPES.TAPP,
    },
  ],
  declarations: [
    ListComponent,
    CreateComponent,
    CreateFromYamlComponent,
    UpdateComponent,
    FormContainerComponent,
    FormComponent,
    DetailComponent,
    DetailInfoComponent,
    BlueGreenComponent,
  ],
})
export class TAppModule {}
