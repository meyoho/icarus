import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { NetworkPolicyCreateComponent } from './create/component';
import { NetworkPolicyEditComponent } from './edit/component';
import { NetworkPolicyListComponent } from './list/component';
import { NetworkPolicyRoutingModule } from './routing.module';
import { NetworkPolicyTemplateSelectComponent } from './template-select/component';
import { NetworkPolicyUpdateComponent } from './update/component';

const components = [
  NetworkPolicyListComponent,
  NetworkPolicyCreateComponent,
  NetworkPolicyUpdateComponent,
  NetworkPolicyEditComponent,
  NetworkPolicyTemplateSelectComponent,
];
@NgModule({
  imports: [SharedModule, NetworkPolicyRoutingModule],
  declarations: components,
})
export class NetworkPolicyModule {}
