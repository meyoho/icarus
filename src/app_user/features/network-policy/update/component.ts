import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <rc-network-policy-edit-component
      [isUpdate]="true"
    ></rc-network-policy-edit-component>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NetworkPolicyUpdateComponent {}
