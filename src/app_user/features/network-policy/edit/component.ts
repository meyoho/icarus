import {
  K8sApiService,
  METADATA,
  NAME,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump, safeLoad } from 'js-yaml';
import { set } from 'lodash-es';
import { combineLatest, of } from 'rxjs';
import { finalize, map, switchMap } from 'rxjs/operators';

import {
  NetworkPolicy,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { createActions, yamlWriteOptions } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { NetworkPolicyType, NetworkTemplateMapper } from '../util';

@Component({
  selector: 'rc-network-policy-edit-component',
  templateUrl: 'template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NetworkPolicyEditComponent implements OnInit {
  createPayload: NetworkPolicy;
  submitting: boolean;
  baseParams: WorkspaceBaseParams;

  originalYaml = '';
  yamlInputValue = '';
  editorActions = createActions;
  editorOptions = yamlWriteOptions;

  @Input()
  isUpdate = false;

  @ViewChild('form', { static: false })
  form: NgForm;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    combineLatest([
      this.activatedRoute.paramMap,
      this.activatedRoute.queryParamMap,
      this.workspaceComponent.baseParams,
    ])
      .pipe(
        switchMap(([params, queryParams, baseParams]) => {
          this.baseParams = baseParams;
          const name = params.get(NAME);
          const templateType = queryParams.get(
            'template_type',
          ) as NetworkPolicyType;
          return this.isUpdate
            ? this.k8sApi
                .getResource({
                  type: RESOURCE_TYPES.NETWORK_POLICY,
                  cluster: baseParams.cluster,
                  namespace: baseParams.namespace,
                  name,
                })
                .pipe(map(resource => safeDump(resource)))
            : of(templateType ? NetworkTemplateMapper[templateType] || '' : '');
        }),
      )
      .subscribe(originCode => {
        this.originalYaml = originCode;
        this.yamlInputValue = originCode;
        this.cdr.markForCheck();
      });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const { namespace, cluster } = this.baseParams;
    try {
      const loadedYaml = safeLoad(this.yamlInputValue) as NetworkPolicy;
      this.createPayload = loadedYaml;
      set(this.createPayload, [METADATA, NAMESPACE], namespace);
    } catch (error) {
      this.notification.error(this.translate.get('yaml_format_hint'));
      return;
    }
    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      type: RESOURCE_TYPES.NETWORK_POLICY,
      cluster,
      resource: this.createPayload,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get(
            this.isUpdate ? 'update_success' : 'create_success',
          ),
        );
        this.jumpToList();
      });
  }

  cancel() {
    this.jumpToList();
  }

  jumpToList() {
    this.router.navigate(['network_policy'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
