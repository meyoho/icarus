import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
  TranslateService,
  catchPromise,
  isAllowed,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { Subject, combineLatest } from 'rxjs';
import { concatMapTo, map, shareReplay, switchMap, take } from 'rxjs/operators';

import { NetworkPolicy, RESOURCE_TYPES } from 'app/typings';
import { ACTION, viewActions, yamlReadOptions } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { NetworkPolicyTemplateSelectComponent } from '../template-select/component';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NetworkPolicyListComponent implements OnDestroy {
  editorActions = viewActions;
  editorOptions = yamlReadOptions;
  yamlInputValue = '';
  columns = [NAME, 'created_time', ACTION];

  private readonly onDestroy$ = new Subject<any>();

  params$ = this.workspaceComponent.baseActivatedRoute.params;

  permissions$ = this.params$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.NETWORK_POLICY,
        cluster,
        namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  networkPolicyParams$ = combineLatest([
    this.params$,
    this.activatedRoute.queryParamMap,
  ]).pipe(
    map(([params, myParamMap]) => ({
      ...params,
      keyword: myParamMap.get('keyword'),
    })),
    shareReplay(1),
  );

  list = new K8SResourceList({
    fetcher: this.fetchNetworkPolicies.bind(this),
    fetchParams$: this.networkPolicyParams$,
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster, namespace }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.NETWORK_POLICY,
            cluster,
            namespace,
          }),
        ),
      ),
  });

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly message: MessageService,
  ) {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  fetchNetworkPolicies({
    cluster,
    search: _search,
    namespace,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi.getResourceList<NetworkPolicy>({
      type: RESOURCE_TYPES.NETWORK_POLICY,
      cluster,
      namespace,
      queryParams,
    });
  }

  update(item: NetworkPolicy) {
    this.router.navigate(['network_policy', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(item: NetworkPolicy) {
    const {
      cluster,
    } = this.workspaceComponent.baseActivatedRoute.snapshot.params;

    catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('delete_network_policy_confirm', {
          policy_name: item.metadata.name,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMapTo(
          this.k8sApi.deleteResource({
            cluster,
            resource: item,
            type: RESOURCE_TYPES.NETWORK_POLICY,
          }),
        ),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get('delete_success', {
            name: item.metadata.name,
          }),
        );
      });
  }

  create() {
    const dialogRef = this.dialogService.open(
      NetworkPolicyTemplateSelectComponent,
      {
        size: DialogSize.Large,
        fitViewport: true,
      },
    );
    dialogRef.componentInstance.close.pipe(take(1)).subscribe(type => {
      if (type) {
        this.router.navigate(['network_policy', 'create'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
          queryParams: { template_type: type },
        });
      }
      dialogRef.close();
    });
  }

  createFromYaml() {
    this.router.navigate(['network_policy', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  view(data: NetworkPolicy, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(data, { lineWidth: 9999, sortKeys: true });
    this.dialogService.open(templateRef, {
      data: {
        name: data.metadata.name,
      },
      size: DialogSize.Large,
    });
  }

  searchByName(keyword: string) {
    return this.router.navigate(['.'], {
      queryParams: { keyword },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }
}
