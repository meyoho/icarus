export enum NetworkPolicyType {
  DEFAULT_DENY_ALL_WITH_NAMESPACE = 'default_deny_all_with_namespace',
  DENY_OTHER_NAMESPACES = 'deny_other_namespaces',
  ALLOW_OTHER_NAMESPACES = 'allow_other_namespaces',
  ALLOW_IP_WITH_NAMESPACE = 'allow_ip_with_namespace',
  DEFAULT_DENY_ALL_WITH_POD = 'default_deny_all_with_pod',
  ALLOW_SOME_PODS_IN_NAMESPACE = 'allow_some_pods_in_namespace',
  ALLOW_PODS_IN_NAMESPACE = 'allow_pods_in_namespace',
  ALLOW_SOME_PODS_IN_OTHER_NAMESPACES = 'allow_some_pods_in_other_namespaces',
  ALLOW_PODS_IN_OTHER_NAMESPACES = 'allow_pods_in_other_namespaces',
  ALLOW_IP_WITH_PODS = 'allow_ip_with_pods',
  DEFALUT_DENY_ALL_IN_EGRESS = 'default_deny_all_in_egress',
  ALLOW_IP_IN_EGRESS = 'allow_ip_in_egress',
}

const DEFAULT_DENY_ALL_WITH_NAMESPACE_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-deny-all
spec:
  podSelector: {}
  policyTypes:
    - Ingress`;

const DENY_OTHER_NAMESPACES_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: deny-other-namespaces
spec:
  podSelector: {}
  ingress:
    - from:
        - podSelector: {}`;

const ALLOW_OTHER_NAMESPACES_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-other-namespaces
spec:
  podSelector: {}
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              env: production`;

const ALLOW_IP_WITH_NAMESPACE_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-ip
spec:
  podSelector: {}
  ingress:
    - from:
        - ipBlock:
            cidr: 10.16.0.0/16
            except:
              - 10.16.8.0/24
              - 10.16.0.2/32`;

const DEFAULT_DENY_ALL_WITH_POD_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-deny-all
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
    - Ingress`;

const ALLOW_SOME_PODS_IN_NAMESPACE_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-some-same-namespace
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - podSelector:
            matchLabels:
              app: mail`;

const ALLOW_PODS_IN_NAMESPACE_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-same-namespace
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - podSelector: {}`;

const ALLOW_SOME_PODS_IN_OTHER_NAMESPACES_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-some-other-namespace
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              env: production
          podSelector:
            matchLabels:
              app: mail`;

const ALLOW_PODS_IN_OTHER_NAMESPACES_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-other-namespace
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              env: production`;

const ALLOW_IP_WITH_PODS_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-ip
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - ipBlock:
            cidr: 10.16.0.0/16
            except:
              - 10.16.8.0/24
              - 10.16.0.2/32`;

const DEFALUT_DENY_ALL_IN_EGRESS_TEMPLATE = `kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: default-deny-all
spec:
  podSelector: {}
  egress:
    - to:
      - podSelector: {}
  policyTypes:
  - Egress`;

const ALLOW_IP_IN_EGRESS_TEMPLATE = `apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-ip
spec:
  podSelector:
    matchLabels:
      role: db
  egress:
    - to:
        - ipBlock:
            cidr: 10.16.0.0/16
            except:
              - 10.16.8.0/24
              - 10.16.0.2/32
  policyTypes:
  - Egress`;

export const NetworkTemplateMapper: Record<NetworkPolicyType, string> = {
  [NetworkPolicyType.DEFAULT_DENY_ALL_WITH_NAMESPACE]: DEFAULT_DENY_ALL_WITH_NAMESPACE_TEMPLATE,
  [NetworkPolicyType.DENY_OTHER_NAMESPACES]: DENY_OTHER_NAMESPACES_TEMPLATE,
  [NetworkPolicyType.ALLOW_OTHER_NAMESPACES]: ALLOW_OTHER_NAMESPACES_TEMPLATE,
  [NetworkPolicyType.ALLOW_IP_WITH_NAMESPACE]: ALLOW_IP_WITH_NAMESPACE_TEMPLATE,
  [NetworkPolicyType.DEFAULT_DENY_ALL_WITH_POD]: DEFAULT_DENY_ALL_WITH_POD_TEMPLATE,
  [NetworkPolicyType.ALLOW_SOME_PODS_IN_NAMESPACE]: ALLOW_SOME_PODS_IN_NAMESPACE_TEMPLATE,
  [NetworkPolicyType.ALLOW_PODS_IN_NAMESPACE]: ALLOW_PODS_IN_NAMESPACE_TEMPLATE,
  [NetworkPolicyType.ALLOW_SOME_PODS_IN_OTHER_NAMESPACES]: ALLOW_SOME_PODS_IN_OTHER_NAMESPACES_TEMPLATE,
  [NetworkPolicyType.ALLOW_PODS_IN_OTHER_NAMESPACES]: ALLOW_PODS_IN_OTHER_NAMESPACES_TEMPLATE,
  [NetworkPolicyType.ALLOW_IP_WITH_PODS]: ALLOW_IP_WITH_PODS_TEMPLATE,
  [NetworkPolicyType.DEFALUT_DENY_ALL_IN_EGRESS]: DEFALUT_DENY_ALL_IN_EGRESS_TEMPLATE,
  [NetworkPolicyType.ALLOW_IP_IN_EGRESS]: ALLOW_IP_IN_EGRESS_TEMPLATE,
};

export const NETWORK_POLICY_TYPES = Object.keys(
  NetworkTemplateMapper,
) as NetworkPolicyType[];
