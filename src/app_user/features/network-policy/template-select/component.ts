import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
} from '@angular/core';

import {
  NETWORK_POLICY_TYPES,
  NetworkPolicyType,
  NetworkTemplateMapper,
} from '../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class NetworkPolicyTemplateSelectComponent {
  value: NetworkPolicyType;

  NETWORK_POLICY_TYPES = NETWORK_POLICY_TYPES;
  NetworkTemplateMapper = NetworkTemplateMapper;
  close = new EventEmitter<NetworkPolicyType>();

  getImage(type: NetworkPolicyType) {
    return `icons/network-policy/${type}.svg`;
  }

  confirm() {
    this.close.next(this.value);
  }

  cancel() {
    this.close.next();
  }

  formatIndex(index: number) {
    return ('' + index).padStart(2, '0');
  }
}
