import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NetworkPolicyCreateComponent } from './create/component';
import { NetworkPolicyListComponent } from './list/component';
import { NetworkPolicyUpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: NetworkPolicyListComponent,
    pathMatch: 'full',
  },
  {
    path: 'create',
    component: NetworkPolicyCreateComponent,
  },
  {
    path: 'update/:name',
    component: NetworkPolicyUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NetworkPolicyRoutingModule {}
