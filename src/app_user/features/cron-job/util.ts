// add non-injectable token, just constants, so not involved in form env
import { ANNOTATIONS, METADATA } from '@alauda/common-snippet';
import { set } from 'lodash-es';

import { PodTemplateSpec } from 'app/typings';
import { FALSE } from 'app/utils';

export function addUnInjectableToken(
  template: PodTemplateSpec = {},
): PodTemplateSpec {
  set(template, [METADATA, ANNOTATIONS, 'sidecar.istio.io/inject'], FALSE);
  return template;
}
