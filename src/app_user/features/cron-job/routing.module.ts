import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateCronJobComponent } from 'app_user/features/cron-job/create/component';
import { CronJobDetailComponent } from 'app_user/features/cron-job/detail/component';
import { CronJobListComponent } from 'app_user/features/cron-job/list/component';
import { CronJobUpdateComponent } from 'app_user/features/cron-job/update/component';

import { CreateCronJobFromYamlComponent } from './create-from-yaml/component';

const routes: Routes = [
  {
    path: '',
    component: CronJobListComponent,
  },
  {
    path: 'create',
    component: CreateCronJobComponent,
  },
  {
    path: 'create_from_yaml',
    component: CreateCronJobFromYamlComponent,
  },
  {
    path: 'detail/:name',
    component: CronJobDetailComponent,
  },
  {
    path: 'update/:name',
    component: CronJobUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CronJobRoutingModule {}
