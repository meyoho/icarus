import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { JobRecordSharedModule } from '../job-record/shared.module';

import { CreateCronJobFromYamlComponent } from './create-from-yaml/component';
import { CreateCronJobComponent } from './create/component';
import { CronJobDetailComponent } from './detail/component';
import { CronJobDetailInfoComponent } from './detail/info/component';
import { CronJobFormContainerComponent } from './form-container/component';
import { CronJobFormComponent } from './form/component';
import { CronJobConfigTimeFormComponent } from './form/cron-job-config-time/component';
import { CronJobSpecFormComponent } from './form/cron-job-spec/component';
import { JobSpecFormComponent } from './form/job-spec/component';
import { CronJobListComponent } from './list/component';
import { CronJobRoutingModule } from './routing.module';
import { JobService } from './service';
import { CornJobSharedModule } from './shared.module';
import { CronJobUpdateComponent } from './update/component';

const components = [
  CronJobListComponent,
  CronJobFormComponent,
  CreateCronJobComponent,
  CronJobFormContainerComponent,
  CronJobSpecFormComponent,
  JobSpecFormComponent,
  CronJobConfigTimeFormComponent,
  CronJobDetailComponent,
  CronJobDetailInfoComponent,
  CronJobUpdateComponent,
  CreateCronJobFromYamlComponent,
];
@NgModule({
  imports: [
    SharedModule,
    AppSharedModule,
    CronJobRoutingModule,
    EventSharedModule,
    ImageSharedModule,
    JobRecordSharedModule,
    CornJobSharedModule,
  ],
  providers: [JobService],
  declarations: components,
})
export class CronJobModule {}
