import {
  API_GATEWAY,
  K8sApiResourceService,
  K8sResourceDefinition,
} from '@alauda/common-snippet';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { RESOURCE_DEFINITIONS, WorkspaceDetailParams } from 'app/typings';

const DELETE_HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  body: { propagationPolicy: 'Background' },
};

@Injectable()
export class JobService {
  CLUSTERS_ENDPOINT_MISC = `${API_GATEWAY}/v2/misc/clusters/`;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly k8sApiResource: K8sApiResourceService,
  ) {}

  deleteCronJob(params: WorkspaceDetailParams) {
    return this._deleteWithBody(params, RESOURCE_DEFINITIONS.CRON_JOB);
  }

  deleteJobRecord(params: WorkspaceDetailParams) {
    return this._deleteWithBody(params, RESOURCE_DEFINITIONS.JOB);
  }

  _deleteWithBody(
    params: WorkspaceDetailParams,
    definition: K8sResourceDefinition,
  ) {
    // this delete action need pass request body,so can't use normalize delete api
    // this scene is extremely rare,so handle this by extracted as an independent api
    return this.httpClient.delete(
      this.k8sApiResource.getApiPath(
        Object.assign(
          {
            cluster: params.cluster,
            namespace: params.namespace,
            name: params.name,
          },
          definition,
        ),
      ),
      DELETE_HTTP_OPTIONS,
    );
  }
}
