import {
  K8sApiService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { LocationStrategy } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { safeDump, safeLoadAll } from 'js-yaml';
import { finalize } from 'rxjs/operators';

import { CronJob, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { createActions, yamlWriteOptions } from 'app/utils';
import { getResourcesFromYaml } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateCronJobFromYamlComponent {
  yaml: string;
  yamlDemo: string;
  codeEditorOptions = yamlWriteOptions;
  actionsConfig = createActions;
  loading = false;

  @ViewChild('form', { static: false })
  private readonly ngForm: NgForm;

  demo = `
  apiVersion: batch/v1beta1
  kind: CronJob
  metadata:
    name: hello
  spec:
    schedule: "*/1 * * * *"
    jobTemplate:
      spec:
        template:
          spec:
            containers:
            - name: hello
              image: busybox
              args:
              - /bin/sh
              - -c
              - date; echo Hello from the Kubernetes cluster
            restartPolicy: OnFailure
`;

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly dialog: DialogService,
    private readonly location: LocationStrategy,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.initYamlDemo();
  }

  private initYamlDemo() {
    this.yamlDemo = safeLoadAll(this.demo)
      .filter(r => !!r)
      .map((r: KubernetesResource) => {
        return {
          ...r,
          metadata: {
            ...r.metadata,
            namespace: this.workspace.baseParamsSnapshot.namespace,
          },
        };
      })
      .map(r => safeDump(r, { sortKeys: true }))
      .join('---\r\n');
  }

  private yamlToFormModel() {
    try {
      return getResourcesFromYaml(
        this.yaml,
        this.workspace.baseParamsSnapshot.namespace,
      )[0];
    } catch (err) {
      this.notification.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return null;
    }
  }

  useDemoTemplate() {
    this.yaml = this.yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialog.open(template, {
      size: DialogSize.Big,
    });
  }

  create() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    const payload = this.yamlToFormModel() as CronJob;
    if (payload) {
      this.loading = true;
      this.k8sApi
        .postResource({
          cluster: this.workspace.baseParamsSnapshot.cluster,
          type: RESOURCE_TYPES.CRON_JOB,
          resource: payload,
        })
        .pipe(
          finalize(() => {
            this.loading = false;
            this.cdr.markForCheck();
          }),
        )
        .subscribe(() => {
          this.router.navigate(['cron_job', 'detail', payload.metadata.name], {
            relativeTo: this.workspace.baseActivatedRoute,
          });
        });
    }
  }

  cancel() {
    this.location.back();
  }
}
