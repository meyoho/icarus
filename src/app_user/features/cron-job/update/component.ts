import { K8sApiService, NAME } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, combineLatest } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { ErrorsToastService } from 'app/services/errors-toast.service';
import { CronJob, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-cron-job-form-container
      *ngIf="data$ | async as data"
      [data]="data"
      [baseParams]="workspaceComponent.baseParams | async"
    ></rc-cron-job-form-container>
  `,
})
export class CronJobUpdateComponent {
  data$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspaceComponent.baseParams,
  ]).pipe(
    switchMap(([params, { cluster, namespace }]) =>
      this.k8sApi.getResource<CronJob>({
        type: RESOURCE_TYPES.CRON_JOB,
        cluster,
        name: params.get(NAME),
        namespace,
      }),
    ),
    catchError(e => {
      this.errorsToastService.error(e);
      return EMPTY;
    }),
  );

  constructor(
    public readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly errorsToastService: ErrorsToastService,
    private readonly k8sApi: K8sApiService,
  ) {}
}
