import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  KubernetesResource,
  NAME,
  ResourceListParams,
  TranslateService,
  isAllowed,
  noop,
} from '@alauda/common-snippet';
import { DialogService, MessageService, NotificationService } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { CronJob, RESOURCE_TYPES, WorkspaceBaseParams } from 'app/typings';
import { ACTION } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { JobService } from '../service';

@Component({
  templateUrl: 'template.html',
})
export class CronJobListComponent implements OnInit, OnDestroy {
  private readonly onDestroy$ = new Subject<any>();
  private clusterName: string;
  private namespaceName: string;

  baseParams: WorkspaceBaseParams;
  project: string;
  columns = [
    NAME,
    'execute_type',
    'next_trig_time',
    'last_schedule_time',
    'created_time',
    ACTION,
  ];

  params$ = this.workspaceComponent.baseActivatedRoute.params.pipe(
    map(params => ({
      cluster: params.cluster,
      project: params.project,
      namespace: params.namespace,
    })),
  );

  permissions$ = this.params$.pipe(
    switchMap(param => {
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.CRON_JOB,
        cluster: param.cluster,
        namespace: param.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      });
    }),
    isAllowed(),
  );

  jobParams$ = combineLatest([
    this.params$,
    this.activatedRoute.queryParamMap,
  ]).pipe(
    map(([params, myParamMap]) => ({
      ...params,
      keyword: myParamMap.get('keyword'),
    })),
  );

  list = new K8SResourceList({
    fetcher: this.fetchCronJobs.bind(this),
    fetchParams$: this.jobParams$,
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster, namespace }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.CRON_JOB,
            cluster,
            namespace,
          }),
        ),
      ),
  });

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly appSharedService: AppSharedService,
    private readonly jobService: JobService,
    private readonly acpApi: AcpApiService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtilService: K8sUtilService,
    private readonly message: MessageService,
    private readonly notification: NotificationService,
  ) {
    this.getDisplayName = this.getDisplayName.bind(this);
  }

  ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = this.baseParams.cluster;
    this.namespaceName = this.baseParams.namespace;
    this.project = this.baseParams.project;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  fetchCronJobs({
    cluster,
    search: _search,
    namespace,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi.getResourceList<CronJob>({
      type: RESOURCE_TYPES.CRON_JOB,
      cluster,
      namespace,
      queryParams,
    });
  }

  update(item: CronJob) {
    this.router.navigate(['cron_job', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async executeNow(item: CronJob) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('cron_job_execute_now_confirm', {
          name: item.metadata.name,
        }),
        confirmText: this.translate.get('execute_now'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.acpApi
      .manualExecJob({
        cluster: this.clusterName,
        namespace: this.namespaceName,
        name: item.metadata.name,
      })
      .subscribe(
        job => {
          this.router.navigate(['job_record', 'detail', job.metadata.name], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
          });
        },
        e => {
          this.notification.error(e);
        },
      );
  }

  async delete(item: CronJob) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_cron_job_confirm', {
          name: ` "${item.metadata.name}" `,
        }),
        content: this.translate.get('delete_cron_job_tip'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.jobService
      .deleteCronJob({
        cluster: this.clusterName,
        name: item.metadata.name,
        namespace: this.namespaceName,
      })
      .subscribe(() => {
        this.message.success(
          this.translate.get('delete_cron_job_success', {
            name: item.metadata.name,
          }),
        );
      }, noop);
  }

  create() {
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['cron_job', 'create'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
            queryParams: res,
          });
        }
      });
  }

  getDisplayName(rowData: KubernetesResource) {
    return this.k8sUtilService.getDisplayName(rowData);
  }

  searchByName(keyword: string) {
    return this.router.navigate(['.'], {
      queryParams: { keyword },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  onUpdate() {
    this.list.reload();
  }

  createFromYaml() {
    this.router.navigate(['create_from_yaml'], {
      relativeTo: this.activatedRoute,
    });
  }
}
