import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { CronJobSpec } from 'app/typings';
import { NATURAL_NUMBER_PATTERN, cronValidator } from 'app/utils';

@Component({
  selector: 'rc-cron-job-spec-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobSpecFormComponent
  extends BaseResourceFormGroupComponent<CronJobSpec, CronJobSpec>
  implements OnInit {
  @Input()
  project: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  pattern = NATURAL_NUMBER_PATTERN;

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.get('suspend').valueChanges.subscribe(_value => {
      this.suspendChange(_value);
    });
  }

  suspendChange(value: boolean) {
    this.form
      .get('schedule')
      [value ? 'disable' : 'enable']({ emitEvent: true });
  }

  getDefaultFormModel() {
    return {};
  }

  createForm() {
    const jobTemplate = this.fb.group({
      spec: this.fb.control({}),
    });
    return this.fb.group({
      suspend: this.fb.control(false),
      schedule: this.fb.control('', [Validators.required, cronValidator()]),
      concurrencyPolicy: this.fb.control('Allow'),
      failedJobsHistoryLimit: this.fb.control(20, [
        Validators.required,
        Validators.pattern(this.pattern.pattern),
      ]),
      successfulJobsHistoryLimit: this.fb.control(20, [
        Validators.required,
        Validators.pattern(this.pattern.pattern),
      ]),
      startingDeadlineSeconds: this.fb.control(''),
      jobTemplate,
    });
  }

  adaptFormModel(formModel: CronJobSpec) {
    if (formModel && formModel.jobTemplate) {
      const resource = cloneDeep(formModel);
      resource.startingDeadlineSeconds =
        resource.jobTemplate.spec.startingDeadlineSeconds;
      delete resource.jobTemplate.spec.startingDeadlineSeconds;
      if (resource.suspend) {
        delete resource.concurrencyPolicy;
        // default schedule when set schedule to true
        resource.schedule = '1 1 30 2 *';
      }
      if (
        resource.failedJobsHistoryLimit === null ||
        resource.failedJobsHistoryLimit === ''
      ) {
        delete resource.failedJobsHistoryLimit;
      }
      if (
        resource.successfulJobsHistoryLimit === null ||
        resource.successfulJobsHistoryLimit === ''
      ) {
        delete resource.successfulJobsHistoryLimit;
      }
      return resource;
    }
    return formModel;
  }

  adaptResourceModel(resource: CronJobSpec) {
    if (resource && resource.jobTemplate) {
      const formModel = cloneDeep(resource);
      this.suspendChange(formModel.suspend);
      const jobTemplate = formModel.jobTemplate;
      jobTemplate.spec.startingDeadlineSeconds =
        formModel.startingDeadlineSeconds;
      delete formModel.startingDeadlineSeconds;
      return formModel;
    }
    return resource;
  }
}
