import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { CronJob, Environments } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

@Component({
  selector: 'rc-cron-job-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobFormComponent extends BaseResourceFormGroupComponent<
  CronJob,
  CronJob
> {
  @Input()
  isUpdate: boolean;

  @Input()
  project: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  namePattern = K8S_RESOURCE_NAME_BASE;
  annotationName: string;
  constructor(
    injector: Injector,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
    this.annotationName = `${this.env.LABEL_BASE_DOMAIN}/display-name`;
  }

  getDefaultFormModel() {
    return {};
  }

  createForm() {
    const annotationsForm = this.fb.group({
      [this.annotationName]: this.fb.control(''),
    });
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.namePattern.pattern),
      ]),
      namespace: this.fb.control(''),
      annotations: annotationsForm,
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: this.fb.control({}),
    });
  }
}
