import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { JobConfigComponent } from './job-config/component';

const EXPORT_COMPONENTS = [JobConfigComponent];

@NgModule({
  imports: [SharedModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
})
export class CornJobSharedModule {}
