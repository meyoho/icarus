import { Component } from '@angular/core';

import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-cron-job-form-container
      [baseParams]="workspaceComponent.baseParams | async"
    ></rc-cron-job-form-container>
  `,
})
export class CreateCronJobComponent {
  constructor(public workspaceComponent: WorkspaceComponent) {}
}
