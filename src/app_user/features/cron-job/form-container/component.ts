import { K8sApiService, TranslateService, noop } from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import jsyaml from 'js-yaml';
import { merge, set } from 'lodash-es';
import { finalize } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  CronJob,
  Environments,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  createActions,
  updateActions,
  yamlWriteOptions,
} from 'app/utils';
import {
  checkImageSelectionParam,
  getDefaultPodController,
} from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  selector: 'rc-cron-job-form-container',
  styleUrls: ['./styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobFormContainerComponent implements OnInit {
  @Input()
  data: CronJob;

  @Input()
  baseParams: WorkspaceBaseParams;

  @ViewChild('resourceForm', { static: false })
  resourceForm: NgForm;

  resource: CronJob;
  yaml: string;

  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  codeEditorOptions = yamlWriteOptions;
  actionsConfig = createActions;
  submitting = false;
  initialized = false;
  formModel = 'UI';
  createResourceNotifyContent = 'create_cron_job_success';
  updateResourceNotifyContent = 'update_cron_job_success';

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly translateService: TranslateService,
    private readonly router: Router,
    private readonly location: Location,
    private readonly auiNotificationService: NotificationService,
    private readonly message: MessageService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService,
    private readonly appSharedService: AppSharedService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {}

  ngOnInit() {
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (this.data) {
      this.resource = this.updateInitResource(this.data);
      this.actionsConfig = updateActions;
      this.initialized = true;
    } else {
      if (!checkImageSelectionParam(queryParams)) {
        this.cancel();
      }
      const { cluster, namespace } = this.baseParams;
      this.appSharedService
        .getDefaultLimitResources({
          cluster,
          namespace,
        })
        .subscribe(defaultLimit => {
          this.resource = this.genDefaultResource();
          const podController = getDefaultPodController({
            imageSelectionParams: queryParams,
            namespace,
            defaultLimit,
            kind: 'Deployment',
          });
          merge(
            this.resource.spec.jobTemplate.spec.template,
            podController.spec.template,
          );
          this.initialized = true;
          this.cdr.markForCheck();
        });
    }
  }

  genDefaultResource(): CronJob {
    return {
      apiVersion: 'batch/v1beta1',
      kind: 'CronJob',
      metadata: {
        name: '',
        namespace: this.baseParams.namespace,
      },
      spec: {
        concurrencyPolicy: 'Allow',
        failedJobsHistoryLimit: 20,
        successfulJobsHistoryLimit: 20,
        schedule: '',
        suspend: false,
        jobTemplate: {
          spec: {
            // default 2 hours
            activeDeadlineSeconds: 7200,
            backoffLimit: 6,
            template: {
              spec: {
                restartPolicy: 'Never',
                containers: [],
              },
            },
          },
        },
      },
    };
  }

  updateInitResource(resource: CronJob) {
    delete resource.metadata.resourceVersion;
    return resource;
  }

  goToResourceDetailPage(name: string) {
    this.router.navigate(['cron_job', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  confirm() {
    this.resourceForm.onSubmit(null);
    if (this.resourceForm.invalid) {
      return;
    }

    this.submitting = true;
    let payload: CronJob;
    try {
      payload = this.genPayload();
    } catch (err) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: err.message,
      });
      this.submitting = false;
      return;
    }

    this.confirmResource(payload);
  }

  private confirmResource(payload: CronJob) {
    const notifyContent = this.data
      ? this.updateResourceNotifyContent
      : this.createResourceNotifyContent;

    this.k8sApi[this.data ? 'putResource' : 'postResource']({
      cluster: this.baseParams.cluster,
      type: RESOURCE_TYPES.CRON_JOB,
      resource: payload,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translateService.get(notifyContent, {
            name: payload.metadata.name || '',
          }),
        );
        this.goToResourceDetailPage(payload.metadata.name);
      }, noop);
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.yaml = this.formModelToYaml(this.formatResource());
        break;
      case 'UI':
        this.resource = this.yamlToFormModel(this.yaml);
        break;
    }
  }

  private formModelToYaml(formModel: CronJob) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      return jsyaml.safeDump(json);
    } catch (err) {
      this.auiNotificationService.error(err.message);
    }
  }

  private yamlToFormModel(yaml: string) {
    let formModel: CronJob;
    try {
      formModel = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultResource(), formModel);
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genResourceFromYaml();
    }
    return this.formatResource();
  }

  formatResource() {
    this.setJobLabels(this.resource);
    return this.resource;
  }

  genResourceFromYaml() {
    const resource: CronJob = jsyaml.safeLoad(this.yaml);
    if (resource.metadata) {
      resource.metadata.namespace = this.baseParams.namespace;
      this.setJobLabels(resource);
    }
    return resource;
  }

  setJobLabels(resource: CronJob) {
    set(
      resource.spec.jobTemplate,
      `metadata.labels['cronjob.${this.env.LABEL_BASE_DOMAIN}/name']`,
      resource.metadata.name,
    );
    set(
      this.resource.spec.jobTemplate.spec.template,
      `metadata.labels['project.${this.env.LABEL_BASE_DOMAIN}/name']`,
      this.baseParams.project,
    );
  }

  cancel() {
    this.location.back();
  }
}
