// tslint:disable: no-output-on-prefix
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { get } from 'lodash-es';
import { Subject } from 'rxjs';

import { CronJob, JobSpec, WorkspaceBaseParams } from 'app/typings';
import { AppUtilService } from 'app_user/features/app/app-util.service';

@Component({
  selector: 'rc-cron-job-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class CronJobDetailInfoComponent implements OnInit, OnDestroy {
  private readonly onDestroy$ = new Subject<any>();

  @Input()
  data: CronJob;

  @Input()
  canUpdate: boolean;

  @Input()
  canDelete: boolean;

  @Input()
  baseParams: WorkspaceBaseParams;

  @Output()
  onUpdateSuccess = new EventEmitter<void>();

  @Output()
  onUpdate = new EventEmitter<void>();

  @Output()
  onDelete = new EventEmitter<void>();

  @Output()
  onExecuteNow = new EventEmitter<void>();

  jobSpec: JobSpec;

  constructor(private readonly appUtilService: AppUtilService) {}

  ngOnInit() {
    this.jobSpec = this.data.spec.jobTemplate.spec;
  }

  update() {
    this.onUpdate.next();
  }

  executeNow() {
    this.onExecuteNow.next();
  }

  updateImageTag(containerName: string) {
    this.appUtilService
      .updateImageTag({
        cronJob: this.data,
        containerName,
        baseParams: this.baseParams,
      })
      .subscribe(res => {
        if (res) {
          this.onUpdateSuccess.next();
        }
      });
  }

  updateResourceSize(containerName: string) {
    this.appUtilService
      .updateResourceSize({
        cronJob: this.data,
        containerName,
        baseParams: this.baseParams,
      })
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(res => {
        if (res) {
          this.onUpdateSuccess.next();
        }
      });
  }

  getVolumes(cronJob: CronJob) {
    return get(cronJob, [
      'spec',
      'jobTemplate',
      'spec',
      'template',
      'spec',
      'volumes',
    ]);
  }

  delete() {
    this.onDelete.next();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
