import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  ObjectMeta,
  StringMap,
  TranslateService,
  catchPromise,
  isAllowed,
} from '@alauda/common-snippet';
import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { Component, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { equals } from 'ramda';
import { Subject, combineLatest } from 'rxjs';
import {
  concatMapTo,
  distinctUntilChanged,
  first,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { EnvUpdateParams } from 'app/features-shared/app/detail/env/component';
import { UpdateEnvDialogComponent } from 'app/features-shared/app/dialog/update-env/component';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  CronJob,
  Environments,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { JobService } from '../service';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../app/detail-styles.common.scss'],
})
export class CronJobDetailComponent implements OnDestroy {
  dataInfo: ObjectMeta;
  yaml: string;
  loading = false;
  result: CronJob;
  baseParams: WorkspaceBaseParams;
  labelSelector: StringMap = {};

  destroy$ = new Subject<void>();

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(params => {
      this.baseParams = params;
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.CRON_JOB,
        cluster: params.cluster,
        namespace: params.namespace,
        action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
      });
    }),
    isAllowed(),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => {
      this.labelSelector[`cronjob.${this.env.LABEL_BASE_DOMAIN}/name`] = name;
      return { name, cluster, namespace };
    }),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  dataManager = new AsyncDataLoader<{
    resource: CronJob;
    permissions: { update: boolean; delete: boolean };
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly job: JobService,
    private readonly acpApi: AcpApiService,
    private readonly message: MessageService,
    private readonly notification: NotificationService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {}

  private dataFetcher({ cluster, name, namespace }: Params) {
    return combineLatest([
      this.k8sApi
        .watchResource<CronJob>({
          type: RESOURCE_TYPES.CRON_JOB,
          cluster,
          name,
          namespace,
        })
        .pipe(
          tap(result => {
            this.result = result;
            this.dataInfo = result.metadata || {};
          }),
        ),
      this.permissions$,
    ]).pipe(
      map(([resource, permissions]) => ({
        resource,
        permissions,
      })),
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  update(resource: CronJob) {
    this.router.navigate(['cron_job', 'update', resource.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  executeNow(resource: CronJob) {
    const { cluster, namespace } = this.baseParams;
    catchPromise(
      this.dialog.confirm({
        title: this.translate.get('cron_job_execute_now_confirm', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('execute_now'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMapTo(
          this.acpApi.manualExecJob({
            cluster,
            namespace,
            name: resource.metadata.name,
          }),
        ),
      )
      .subscribe(
        job => {
          this.router.navigate(['job_record', 'detail', job.metadata.name], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
          });
        },
        e => {
          this.notification.error(e);
        },
      );
  }

  updateEnv(params: EnvUpdateParams, cronJob: CronJob) {
    const dialogRef = this.dialog.open(UpdateEnvDialogComponent, {
      size: params.type === 'env' ? DialogSize.Big : DialogSize.Medium,
      data: {
        ...params,
        cronJob,
        baseParams: this.baseParams,
      },
    });
    return dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      if (res) {
        this.message.success(this.translate.get('update_success'));
      }
      dialogRef.close();
    });
  }

  delete(resource: CronJob) {
    const { cluster, namespace } = this.baseParams;
    catchPromise(
      this.dialog.confirm({
        title: this.translate.get('delete_cron_job_confirm', {
          name: resource.metadata.name,
        }),
        content: this.translate.get('delete_cron_job_tip'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMapTo(
          this.job.deleteCronJob({
            cluster,
            namespace,
            name: resource.metadata.name,
          }),
        ),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get('delete_cron_job_success', {
            name: resource.metadata.name,
          }),
        );
        this.jumpToListPage();
      });
  }

  jumpToListPage() {
    this.router.navigate(['cron_job'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
