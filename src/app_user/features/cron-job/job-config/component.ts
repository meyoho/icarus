import { Component, Input } from '@angular/core';

import { CronJob, JobSpec } from 'app/typings';

@Component({
  selector: 'rc-job-config',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobConfigComponent {
  @Input()
  jobSpec: JobSpec;

  @Input()
  showJobType = true;

  resource: CronJob;

  get jobType() {
    return this.jobSpec.completions
      ? 'fixed_number_job'
      : this.jobSpec.parallelism
      ? 'parallel_job'
      : 'single_job';
  }
}
