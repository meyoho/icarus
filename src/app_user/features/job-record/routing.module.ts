import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JobDetailComponent } from 'app_user/features/job-record/detail/component';
import { JobRecordListComponent } from 'app_user/features/job-record/list/component';

const routes: Routes = [
  {
    path: '',
    component: JobRecordListComponent,
  },
  {
    path: 'detail/:name',
    component: JobDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobRecordRoutingModule {}
