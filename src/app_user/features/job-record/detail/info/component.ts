// tslint:disable: no-output-on-prefix
import {
  K8sPermissionService,
  K8sResourceAction,
  isAllowed,
} from '@alauda/common-snippet';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';

import {
  Job,
  JobStatusColorMapper,
  JobStatusIconMapper,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { getJobStatus } from 'app/utils';
@Component({
  selector: 'rc-job-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobDetailInfoComponent implements OnInit {
  @Input()
  data: Job;

  @Input()
  baseParams: WorkspaceBaseParams;

  @Output()
  onDelete = new EventEmitter<void>();

  @Output()
  onOpenCronJobDetail = new EventEmitter<string>();

  @Output()
  onViewLog = new EventEmitter<{
    pod: string;
    container: string;
  }>();

  permissions$: Observable<Record<typeof K8sResourceAction.DELETE, boolean>>;

  getJobStatus = getJobStatus;
  JobStatusColorMapper = JobStatusColorMapper;
  JobStatusIconMapper = JobStatusIconMapper;

  constructor(private readonly k8sPermission: K8sPermissionService) {}

  ngOnInit() {
    this.permissions$ = this.k8sPermission
      .getAccess({
        type: RESOURCE_TYPES.JOB,
        action: K8sResourceAction.DELETE,
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      })
      .pipe(isAllowed());
  }

  getOwnerCronJob() {
    const ownerReferences = this.data.metadata.ownerReferences;
    if (ownerReferences) {
      const ownerCronJob = ownerReferences.find(
        reference => reference.kind === 'CronJob',
      );
      return ownerCronJob
        ? { link: true, name: ownerCronJob.name }
        : {
            link: false,
            name: `${ownerReferences[0].kind}:${ownerReferences[0].name}`,
          };
    }
    return { link: false, name: '-' };
  }

  onOpenLogHandler(data: { pod: string; container: string }) {
    this.onViewLog.emit(data);
  }
}
