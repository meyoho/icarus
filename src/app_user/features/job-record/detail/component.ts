import {
  AsyncDataLoader,
  K8sApiService,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { equals } from 'ramda';
import { Subject, combineLatest } from 'rxjs';
import { distinctUntilChanged, finalize, map, takeUntil } from 'rxjs/operators';

import { Job, RESOURCE_TYPES, WorkspaceBaseParams } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { JobService } from '../../cron-job/service';
import { JobQueryParams } from '../job-record-list/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobDetailComponent implements OnInit, OnDestroy {
  queryParams: JobQueryParams = {
    activeTabIndex: 0,
    pod: '',
    container: '',
  };

  logInitialState = {
    pod: '',
    container: '',
  };

  destroy$ = new Subject<void>();
  parentPath = 'job_record';
  baseParams: WorkspaceBaseParams;

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    publishRef(),
    takeUntil(this.destroy$),
  );

  dataManager = new AsyncDataLoader<Job>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly jobService: JobService,
  ) {}

  private dataFetcher(params: Params) {
    return this.k8sApi.watchResource<Job>({
      type: RESOURCE_TYPES.JOB,
      cluster: params.cluster,
      namespace: params.namespace,
      name: params.name,
    });
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.queryParams = {
        ...this.queryParams,
        activeTabIndex: parseInt(queryParams.activeTabIndex, 10),
      };
      this.logInitialState.pod = queryParams.pod;
      this.logInitialState.container = queryParams.container;
    });
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  jumpToListPage() {
    this.router.navigate([this.parentPath], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete() {
    const name = this.dataManager.snapshot.data.metadata.name;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_job_record_confirm', {
          name,
        }),
        content: this.translate.get('delete_job_record_tip'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.jobService
      .deleteJobRecord({
        cluster: this.baseParams.cluster,
        name,
        namespace: this.baseParams.namespace,
      })
      .pipe(
        finalize(() => {
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get('delete_job_record_success', {
            name,
          }),
        );
        this.jumpToListPage();
      }, noop);
  }

  onTabIndexChange(index: number) {
    const params: JobQueryParams = {
      activeTabIndex: index,
    };
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: 'merge',
    });
  }

  openCronJobDetail(name: string) {
    this.router.navigate(['cron_job', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  getMatchLabels(job: Job) {
    return job.spec.selector.matchLabels;
  }

  viewLog({ pod, container }: { pod: string; container: string }) {
    return this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        activeTabIndex: 4,
        pod,
        container,
      },
    });
  }
}
