import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  ResourceListParams,
  StringMap,
  TranslateService,
  isAllowed,
  matchLabelsToString,
  noop,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import {
  Job,
  JobStatusColorMapper,
  JobStatusIconMapper,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { ACTION, STATUS, getJobStatus } from 'app/utils';
import { JobService } from 'app_user/features/cron-job/service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

export interface JobQueryParams {
  activeTabIndex?: number;
  pod?: string;
  container?: string;
}

@Component({
  selector: 'rc-job-record-list',
  templateUrl: 'template.html',
})
export class JobRecordListComponent implements OnInit, OnDestroy {
  @Input()
  hasReferencesJobRecord = true;

  @Input()
  labelSelector: StringMap;

  private readonly onDestroy$ = new Subject<void>();
  clusterName: string;
  namespaceName: string;
  columns = [NAME, STATUS, 'references_cron_job', 'created_time', ACTION];
  baseParams: WorkspaceBaseParams;
  params$ = this.workspaceComponent.baseActivatedRoute.params.pipe(
    map(params => ({
      cluster: params.cluster,
      project: params.project,
      namespace: params.namespace,
      search: params.search,
    })),
  );

  list: K8SResourceList<Job>;

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  permissions$ = this.params$.pipe(
    switchMap(params => {
      return this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.JOB,
        action: [K8sResourceAction.DELETE],
        cluster: params.cluster,
        namespace: params.namespace,
      });
    }),
    isAllowed(),
  );

  // combine param from workspace(father root) and self
  jobParams$ = combineLatest([
    this.params$,
    this.activatedRoute.queryParamMap,
  ]).pipe(
    map(([params, myParamMap]) => ({
      ...params,
      keyword: myParamMap.get('keyword'),
    })),
  );

  getJobStatus = getJobStatus;
  JobStatusColorMapper = JobStatusColorMapper;
  JobStatusIconMapper = JobStatusIconMapper;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
    private readonly jobService: JobService,
  ) {}

  ngOnInit() {
    if (!this.hasReferencesJobRecord) {
      this.columns = [NAME, STATUS, 'created_time', ACTION];
    }
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = this.baseParams.cluster;
    this.namespaceName = this.baseParams.namespace;

    this.list = new K8SResourceList({
      fetcher: this.fetchJobRecords.bind(this),
      fetchParams$: this.jobParams$,
      watcher: seed =>
        this.k8sApi.watchResourceChange(seed, {
          type: RESOURCE_TYPES.JOB,
          cluster: this.workspaceComponent.baseParamsSnapshot.cluster,
          namespace: this.workspaceComponent.baseParamsSnapshot.namespace,
          queryParams: {
            labelSelector: matchLabelsToString(this.labelSelector),
          },
        }),
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  viewLogs(item: Job) {
    this.router.navigate(['job_record', 'detail', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
      queryParams: { activeTabIndex: 4, pod: '' },
    });
  }

  async delete(item: Job) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_job_record_confirm', {
          name: item.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.jobService
      .deleteJobRecord({
        cluster: this.clusterName,
        name: item.metadata.name,
        namespace: this.namespaceName,
      })
      .subscribe(() => {
        this.message.success(
          this.translate.get('delete_job_record_success', {
            name: item.metadata.name,
          }),
        );
        this.list.delete(item);
      }, noop);
  }

  getOwnerCronJob(item: Job) {
    const ownerReferences = item.metadata.ownerReferences;
    if (ownerReferences) {
      const ownerCronJob = ownerReferences.find(
        reference => reference.kind === 'CronJob',
      );
      return ownerCronJob
        ? { link: true, name: ownerCronJob.name }
        : {
            link: false,
            name: `${ownerReferences[0].kind}:${ownerReferences[0].name}`,
          };
    }
    return { link: false, name: '-' };
  }

  searchByName(keyword: string) {
    return this.router.navigate(['.'], {
      queryParams: { keyword },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  onUpdate() {
    this.list.reload();
  }

  private fetchJobRecords({
    cluster,
    search: _search,
    namespace,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi.getResourceList<Job>({
      type: RESOURCE_TYPES.JOB,
      cluster,
      namespace,
      queryParams: {
        ...queryParams,
        labelSelector: matchLabelsToString(this.labelSelector),
      },
    });
  }
}
