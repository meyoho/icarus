import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { PodSharedModule } from 'app/features-shared/pod/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { JobService } from '../cron-job/service';
import { CornJobSharedModule } from '../cron-job/shared.module';

import { JobDetailComponent } from './detail/component';
import { JobDetailInfoComponent } from './detail/info/component';
import { JobRecordListComponent } from './list/component';
import { JobRecordRoutingModule } from './routing.module';
import { JobRecordSharedModule } from './shared.module';

@NgModule({
  imports: [
    SharedModule,
    JobRecordRoutingModule,
    EventSharedModule,
    AppSharedModule,
    ImageSharedModule,
    PodSharedModule,
    JobRecordSharedModule,
    CornJobSharedModule,
  ],
  declarations: [
    JobRecordListComponent,
    JobDetailComponent,
    JobDetailInfoComponent,
  ],
  providers: [JobService],
})
export class JobRecordModule {}
