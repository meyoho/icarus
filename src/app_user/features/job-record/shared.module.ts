import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { JobRecordListComponent } from './job-record-list/component';

const EXPORT_COMPONENTS = [JobRecordListComponent];

@NgModule({
  imports: [SharedModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
})
export class JobRecordSharedModule {}
