import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceCreateComponent } from './create/service-create.component';
import { ServiceDetailComponent } from './detail/component';
import { ServiceListComponent } from './list/service-list.component';
import { ServiceUpdateComponent } from './update/service-update.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceListComponent,
  },
  {
    path: 'create',
    component: ServiceCreateComponent,
  },
  {
    path: 'update/:name',
    component: ServiceUpdateComponent,
  },
  {
    path: 'detail/:name',
    component: ServiceDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceRoutingModule {}
