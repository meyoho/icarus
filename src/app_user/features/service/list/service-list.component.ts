import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  RESOURCE_TYPES,
  ResourceType,
  Service,
  WorkspaceListParams,
} from 'app/typings';
import { ACTION } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListComponent {
  baseParams$ = this.workspace.baseParams;

  columns = [NAME, 'clusterIp', 'port', 'created_time', ACTION];

  list = new K8SResourceList<Service>({
    fetcher: this.fetchResources.bind(this),
    fetchParams$: combineLatest([
      this.route.queryParams,
      this.baseParams$,
    ]).pipe(map(([queryParams, params]) => ({ ...queryParams, ...params }))),
  });

  permissions$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SERVICE,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  appKey = this.k8sUtil.normalizeType(NAME, 'app');

  serviceKey = this.k8sUtil.normalizeType(NAME, 'service');

  searchKey$ = this.route.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly workspace: WorkspaceComponent,
  ) {}

  fetchResources(params?: WorkspaceListParams) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { cluster, namespace, project, ...queryParams } = params;
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.SERVICE,
      cluster,
      namespace,
      queryParams,
    });
  }

  trackByFn(_index: number, item: Service) {
    return item.metadata.uid;
  }

  createService() {
    this.router.navigate(['create'], {
      relativeTo: this.route,
    });
  }

  getTargetAppName(item: Service) {
    if (item.spec.selector && item.spec.selector[this.appKey]) {
      return item.spec.selector[this.appKey].split('.')[0];
    }

    // 兼容之前只选择应用的情况
    if (item.spec.selector && item.spec.selector[this.serviceKey]) {
      const value = item.spec.selector[this.serviceKey];
      return value.split('-').length > 1 ? '' : value;
    }
    return '';
  }

  getComponentName(item: Service) {
    if (item.spec.selector && item.spec.selector[this.serviceKey]) {
      const value = item.spec.selector[this.serviceKey];
      // 兼容之前只选择应用的情况
      return value.split('-').length > 1 ? value : '';
    }
    return '';
  }

  delete(service: Service) {
    this.k8sUtil
      .deleteResource(
        {
          type: RESOURCE_TYPES.SERVICE,
          cluster: this.workspace.baseParamsSnapshot.cluster,
          resource: service,
        },
        'k8s_service',
      )
      .subscribe(() => this.list.delete(service));
  }

  searchByName(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.route,
      queryParamsHandling: 'merge',
    });
  }
}
