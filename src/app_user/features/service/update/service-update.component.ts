import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType, Service } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-loading-mask
      *ngIf="!(service$ | async)"
      [loading]="true"
    ></rc-loading-mask>
    <rc-service-form-wrapper
      *ngIf="service$ | async as resource"
      (afterSubmit)="afterSubmit($event)"
      [resource]="resource"
      [baseParams]="params$ | async"
    ></rc-service-form-wrapper>
  `,
})
export class ServiceUpdateComponent {
  params$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([params, baseParams]) => ({
      name: params.get(NAME),
      ...baseParams,
    })),
    publishRef(),
  );

  service$ = this.params$.pipe(
    switchMap(({ cluster, namespace, name }) =>
      this.k8sApi.getResource<Service>({
        type: RESOURCE_TYPES.SERVICE,
        name,
        cluster,
        namespace,
      }),
    ),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {}

  afterSubmit(svc: Service) {
    this.router.navigate(['service', 'detail', svc.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
