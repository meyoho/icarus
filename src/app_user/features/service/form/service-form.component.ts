import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  Service,
  ServiceFormModel,
  ServiceTypeEnum,
  ServiceTypeMeta,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';
import { K8S_SERVICE_NAME, TYPE } from 'app/utils';

@Component({
  selector: 'rc-service-form',
  templateUrl: './service-form.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceFormComponent
  extends BaseResourceFormGroupComponent<Service, ServiceFormModel>
  implements OnInit, OnDestroy {
  @Input() isUpdate: boolean;
  resourceNameReg = K8S_SERVICE_NAME;

  @Input() baseParams: WorkspaceBaseParams;

  private readonly onDestroy$ = new Subject<void>();

  constructor(readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('virtualIp')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(value => {
        const internetAccess = this.form.get('internetAccess').value;
        if (value === false && internetAccess === true) {
          this.form.get('internetAccess').setValue(false, {
            emitEvent: true,
          });
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel(): ServiceFormModel {
    return {
      virtualIp: true,
      internetAccess: false,
      sessionPersistence: false,
      spec: {
        type: ServiceTypeEnum.ClusterIP,
        ports: [],
      },
    };
  }

  adaptFormModel(formModel: ServiceFormModel): Service {
    const {
      virtualIp,
      internetAccess,
      sessionPersistence,
      ...resource
    } = formModel;

    if (virtualIp && get(resource, ['spec', 'clusterIP']) === 'None') {
      delete resource.spec.clusterIP;
    } else if (!virtualIp) {
      resource.spec.clusterIP = 'None';
    }

    if (internetAccess) {
      resource.spec.type = 'NodePort';
    } else {
      resource.spec.type = 'ClusterIP';
    }

    if (sessionPersistence) {
      resource.spec.sessionAffinity = 'ClientIP';
    } else {
      resource.spec.sessionAffinity = 'None';
    }

    return Object.assign({}, ServiceTypeMeta, resource);
  }

  adaptResourceModel(resource: Service): ServiceFormModel {
    if (!resource) {
      return {};
    }

    return {
      ...resource,
      virtualIp: get(resource, ['spec', 'clusterIP']) !== 'None',
      internetAccess: get(resource, ['spec', TYPE]) === 'NodePort',
      sessionPersistence:
        get(resource, ['spec', 'sessionAffinity']) === 'ClientIP',
    };
  }

  createForm() {
    return this.fb.group({
      virtualIp: this.fb.control([true]),
      internetAccess: this.fb.control([false]),
      sessionPersistence: this.fb.control([false]),
      metadata: this.fb.group({
        name: this.fb.control(
          {
            value: '',
          },
          [
            Validators.required,
            Validators.maxLength(32),
            Validators.pattern(K8S_SERVICE_NAME.pattern),
          ],
        ),
        labels: this.fb.control({}),
        annotations: this.fb.control({}),
      }),
      spec: this.fb.group({
        ports: this.fb.control(null),
        selector: [],
      }),
    });
  }

  onImportWorkload(workload: Workload) {
    if (!this.form.get('metadata.name').value) {
      this.form.get('metadata.name').setValue(workload.metadata.name, {
        eventEvent: false,
      });
    }
  }
}
