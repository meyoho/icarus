import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import jsyaml from 'js-yaml';
import { cloneDeep, merge } from 'lodash-es';
import { finalize } from 'rxjs/operators';

import {
  RESOURCE_TYPES,
  ResourceType,
  Service,
  WorkspaceBaseParams,
} from 'app/typings';
import { yamlWriteOptions } from 'app/utils';

import { getDefaultService } from '../util';

@Component({
  selector: 'rc-service-form-wrapper',
  templateUrl: './service-form-wrapper.component.html',
  styleUrls: ['./service-form-wrapper.style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceFormWrapperComponent implements OnInit {
  @Input()
  resource: Service;

  yaml: string;

  @Input()
  baseParams: WorkspaceBaseParams;

  @Output() afterSubmit = new EventEmitter<Service>();
  codeEditorOptions = yamlWriteOptions;

  submitting = false;
  mode = 'UI';

  isUpdate: boolean;

  // resource: Service;

  @ViewChild('resourceForm', { static: false })
  form: NgForm;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly location: Location,
    private readonly notification: NotificationService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.isUpdate = !!this.resource;
    if (!this.resource) {
      this.resource = getDefaultService(this.baseParams.namespace);
    }
  }

  cancel() {
    this.location.back();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    let payload: Service;
    try {
      payload = this.getPayload();
      if (this.checkDuplicatePort(payload)) {
        this.submitting = false;
        return;
      }
    } catch (err) {
      this.notification.error({
        title: this.translate.get('yaml_error'),
        content: err.message,
      });
      return;
    }
    this.submitting = true;
    this.cdr.markForCheck();
    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      type: RESOURCE_TYPES.SERVICE,
      resource: payload,
      cluster: this.baseParams.cluster,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get(
            this.isUpdate ? 'update_success' : 'create_success',
          ),
        );
        this.afterSubmit.emit(payload);
      });
  }

  modeChange(event: string) {
    switch (event) {
      case 'YAML':
        this.yaml = this.jsonToYaml(this.resource);
        break;
      case 'UI':
        this.resource = this.yamlToJson(this.yaml);
        break;
    }
    this.cdr.markForCheck();
  }

  private checkDuplicatePort(payload: Service) {
    const servicePortList = payload.spec.ports;
    const portList: number[] = [];
    let duplicatePort = '';
    servicePortList.forEach(item => {
      if (portList.includes(item.port)) {
        duplicatePort = `${duplicatePort}${item.port}`;
      } else {
        portList.push(item.port);
      }
    });
    if (duplicatePort) {
      this.notification.error({
        content: this.translate.get('duplicate_service_port', {
          portList: duplicatePort,
        }),
      });
      return true;
    }
    return false;
  }

  private jsonToYaml(resource: Service) {
    try {
      return jsyaml.safeDump(cloneDeep(resource));
    } catch (err) {
      this.notification.error(err.message);
    }
  }

  private yamlToJson(yaml: string) {
    let resource: Service;
    try {
      resource = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.notification.error(e.message);
    }
    if (!resource || resource instanceof String) {
      resource = {};
    }
    return merge(getDefaultService(this.baseParams.namespace), resource);
  }

  private getPayload() {
    if (this.mode === 'YAML') {
      const payload = this.yamlToJson(this.yaml);
      payload.metadata.namespace = this.baseParams.namespace;
      return payload;
    }
    return this.resource;
  }
}
