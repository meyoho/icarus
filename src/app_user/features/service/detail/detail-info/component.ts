import { CommonWritablePermissions } from '@alauda/common-snippet';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, Service, WorkspaceBaseParams } from 'app/typings';
import { RESOURCE_ROUTE_MAPPER } from 'app/utils';

@Component({
  selector: 'rc-service-detail-info',
  templateUrl: 'template.html',
})
export class ServiceDetailInfoComponent {
  @Input()
  baseParams: WorkspaceBaseParams;

  @Input()
  service: Service;

  @Input()
  permissions: CommonWritablePermissions;

  @Output()
  update = new EventEmitter<Service>();

  RESOURCE_ROUTE_MAPPER = RESOURCE_ROUTE_MAPPER;

  get resourceParams() {
    return {
      type: RESOURCE_TYPES.SERVICE,
      cluster: this.baseParams.cluster,
      resource: this.service,
    };
  }

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  onUpdate(part: 'annotations' | 'labels') {
    this.k8sUtil
      .updatePart({ ...this.resourceParams, part })
      .subscribe(service => this.update.emit(service));
  }

  onDelete() {
    this.k8sUtil
      .deleteResource(this.resourceParams, 'k8s_service')
      .subscribe(() => {
        this.router.navigate(['../..'], {
          relativeTo: this.route,
        });
      });
  }
}
