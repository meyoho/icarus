import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  NAME,
  publishRef,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { equals } from 'ramda';
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType, Service } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
})
export class ServiceDetailComponent {
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    publishRef(),
  );

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.SERVICE,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
  );

  dataManager = new AsyncDataLoader<{
    resource: Service;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getResource({
          type: RESOURCE_TYPES.SERVICE,
          cluster: params.cluster,
          namespace: params.namespace,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  getMatchLabels(service: Service) {
    return service ? service.spec.selector : {};
  }

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly router: Router,
  ) {}

  jumpToListPage = () => {
    this.router.navigate(['service'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };
}
