import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Service } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-service-form-wrapper
      [baseParams]="baseParams$ | async"
      (afterSubmit)="afterSubmit($event)"
    ></rc-service-form-wrapper>
  `,
})
export class ServiceCreateComponent {
  baseParams$ = this.workspaceComponent.baseParams;
  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly router: Router,
  ) {}

  afterSubmit(svc: Service) {
    this.router.navigate(['service', 'detail', svc.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
