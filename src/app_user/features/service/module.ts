import { NgModule } from '@angular/core';

import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { PodSharedModule } from 'app/features-shared/pod/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { ServiceCreateComponent } from './create/service-create.component';
import { ServiceDetailComponent } from './detail/component';
import { ServiceDetailInfoComponent } from './detail/detail-info/component';
import { ImportWorkloadsComponent } from './fieldset/import-workloads-form.component';
import { ServiceSelectorFieldsetComponent } from './fieldset/service-selector-fieldset.component';
import { ServiceFormWrapperComponent } from './form/service-form-wrapper.component';
import { ServiceFormComponent } from './form/service-form.component';
import { ServiceListComponent } from './list/service-list.component';
import { ServicePortsDisplayComponent } from './ports-display/component';
import { ServiceRoutingModule } from './routing.module';
import { ServiceSharedModule } from './shared.module';
import { ServiceUpdateComponent } from './update/service-update.component';

@NgModule({
  imports: [
    SharedModule,
    PodSharedModule,
    ExecSharedModule,
    ServiceSharedModule,
    ServiceRoutingModule,
  ],
  declarations: [
    ServiceListComponent,
    ServiceFormWrapperComponent,
    ServiceFormComponent,
    ServiceCreateComponent,
    ServiceUpdateComponent,
    ServiceSelectorFieldsetComponent,
    ImportWorkloadsComponent,
    ServiceDetailComponent,
    ServicePortsDisplayComponent,
    ServiceDetailInfoComponent,
  ],
})
export class ServiceModule {}
