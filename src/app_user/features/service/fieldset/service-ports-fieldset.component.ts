import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Optional,
} from '@angular/core';
import { ControlContainer, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import {
  SERVICE_PORT_BASE_PROTOCOLS,
  SERVICE_PORT_PROTOCOLS,
  ServicePort,
  ServicePortProtocol,
} from 'app/typings';
import { rowBackgroundColorFn } from 'app/utils';

import { getNormalizedTargetPort, getServiceProtocolDisplay } from '../util';

export interface ServicePortModel extends ServicePort {
  _protocol: ServicePortProtocol;
}

@Component({
  selector: 'rc-service-ports-fieldset',
  templateUrl: './service-ports-fieldset.component.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicePortsFieldsetComponent extends BaseResourceFormArrayComponent<
  ServicePort,
  ServicePortModel
> {
  constructor(
    public injector: Injector,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  servicePortProtocols = SERVICE_PORT_PROTOCOLS;
  rowBackgroundColorFn = rowBackgroundColorFn;

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    const validators = [Validators.min(1), Validators.max(65535)];
    const protocol = SERVICE_PORT_BASE_PROTOCOLS[0];
    return this.fb.group({
      name: [protocol.toLowerCase()],
      protocol: [protocol],
      _protocol: [SERVICE_PORT_PROTOCOLS[0]],
      port: ['', [Validators.required, ...validators]],
      targetPort: ['', validators],
    });
  }

  adaptResourceModel(ports: ServicePort[]): ServicePortModel[] {
    return (ports || []).map(item => {
      const _protocol = getServiceProtocolDisplay(item);
      return {
        ...item,
        // 目前内部路由中的 protocol 字段固定是 TCP，这里修改的值是用来自动生成内部路由名称前缀
        _protocol,
      };
    });
  }

  adaptFormModel(ports: ServicePortModel[]): ServicePort[] {
    return ports.map(item => {
      const name = (item.name = [
        item._protocol.toLowerCase(),
        item.port,
        item.targetPort,
      ]
        .filter(_ => _)
        .join('-'));
      item = {
        ...item,
        name,
      };
      item.protocol = item._protocol === 'UDP' ? 'UDP' : 'TCP';
      delete item._protocol;
      if (!item.targetPort) {
        delete item.targetPort;
      } else {
        item.targetPort = getNormalizedTargetPort(item.targetPort);
      }
      return item;
    });
  }
}
