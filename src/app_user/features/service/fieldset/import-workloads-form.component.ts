import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { Workload, Workloads } from 'app/typings';

const WORKLOAD_TYPES = [
  'Deployment',
  'DaemonSet',
  'StatefulSet',
  'TApp',
] as const;

@Component({
  selector: 'rc-import-workloads-form',
  templateUrl: 'import-workloads-form.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportWorkloadsComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  workloads: Workloads;

  @Input()
  workloadsLoading: boolean;

  @ObservableInput(true)
  workloads$: Observable<Workloads>;

  workloadTypes = WORKLOAD_TYPES;

  workloadNames$: Observable<string[]>;

  constructor(public injector: Injector, public fb: FormBuilder) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.workloadNames$ = combineLatest([
      this.workloads$,
      this.form.get('kind').valueChanges,
    ]).pipe(
      map(([workloads, workloadType]) =>
        (
          ((workloads || {})[workloadType as keyof Workloads] as Workload[]) ||
          []
        ).map(({ metadata: { name } }) => name),
      ),
    );
  }

  createForm() {
    return this.fb.group({
      name: null,
      kind: null,
    });
  }

  getDefaultFormModel() {
    return {
      kind: WORKLOAD_TYPES[0],
    };
  }
}
