import { LABELS, METADATA, ObservableInput } from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnDestroy,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { finalize, shareReplay, switchMap, takeUntil } from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';
import { Workload, Workloads, WorkspaceDetailParams } from 'app/typings';

@Component({
  selector: 'rc-service-selector-fieldset',
  templateUrl: './service-selector-fieldset.component.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceSelectorFieldsetComponent extends BaseResourceFormComponent
  implements OnDestroy {
  @Input()
  isUpdate: boolean;

  @ObservableInput()
  @Input('baseParams')
  baseParams$: Observable<WorkspaceDetailParams>;

  @Output() importWorkload = new EventEmitter<Workload>();

  destroy$$ = new Subject<void>();

  workloadFormModel: {
    name: string;
    kind: keyof Workloads;
  };

  workloadsLoading$$ = new ReplaySubject(1);

  workloads$ = this.baseParams$.pipe(
    switchMap(({ cluster, namespace }) => {
      this.workloadsLoading$$.next(true);
      return this.acpApi
        .getWorkloads({
          cluster,
          namespace,
        })
        .pipe(finalize(() => this.workloadsLoading$$.next(false)));
    }),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  dialogRef: DialogRef;

  constructor(
    public injector: Injector,
    public fb: FormBuilder,
    private readonly dialog: DialogService,
    private readonly acpApi: AcpApiService,
  ) {
    super(injector);
  }

  ngOnDestroy() {
    this.destroy$$.next();
  }

  importFromWorkloads(templateRef: TemplateRef<any>) {
    this.dialogRef = this.dialog.open(templateRef);
  }

  onImportWorkload(workloads: Workloads) {
    const workload = (workloads[
      this.workloadFormModel.kind
    ] as Workload[]).find(w => w.metadata.name === this.workloadFormModel.name);
    const selector = get(workload, ['spec', 'template', METADATA, LABELS]);
    this.dialogRef.close();
    this.form.setValue({
      ...this.form.value,
      ...selector,
    });
    this.importWorkload.emit(workload);
    this.workloadFormModel = null;
  }

  createForm() {
    return this.fb.control(null);
  }

  getDefaultFormModel() {
    return {};
  }

  getResourceMergeStrategy() {
    return false;
  }
}
