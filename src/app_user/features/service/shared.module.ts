import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ServicePortsFieldsetComponent } from './fieldset/service-ports-fieldset.component';
const COMPONENTS = [ServicePortsFieldsetComponent];
@NgModule({
  imports: [SharedModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class ServiceSharedModule {}
