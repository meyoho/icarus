import {
  SERVICE_PORT_BASE_PROTOCOLS,
  SERVICE_PORT_PROTOCOLS,
  SERVICE_SESSION_AFFINITIES,
  SERVICE_TYPES,
  Service,
  ServicePort,
  ServicePortProtocol,
  ServiceTypeMeta,
} from 'app/typings';

export function getDefaultService(namespace: string, name = ''): Service {
  const protocol = SERVICE_PORT_BASE_PROTOCOLS[0];
  return Object.assign({}, ServiceTypeMeta, {
    metadata: {
      name,
      namespace,
    },
    spec: {
      type: SERVICE_TYPES[0],
      sessionAffinity: SERVICE_SESSION_AFFINITIES[0],
      ports: [
        {
          protocol,
          name: protocol.toLowerCase(),
          port: null,
          targetPort: '',
        },
      ],
      selector: {},
    },
  });
}

export function getServiceProtocolDisplay(port: ServicePort) {
  if (port.protocol === 'UDP') {
    return 'UDP' as ServicePortProtocol;
  } else {
    const protocol = port.name
      ? (port.name.split('-')[0].toUpperCase() as ServicePortProtocol)
      : port.protocol;
    const portProtocol = SERVICE_PORT_PROTOCOLS.find(
      p => p.toUpperCase() === protocol.toUpperCase(),
    );
    return portProtocol || SERVICE_PORT_PROTOCOLS[0];
  }
}

export function getNormalizedTargetPort(targetPort: number | string) {
  return isNaN(+targetPort) ? targetPort : +targetPort;
}
