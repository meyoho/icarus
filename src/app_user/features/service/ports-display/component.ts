import { NAME, ObservableInput, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { Service } from 'app/typings';

import { getServiceProtocolDisplay } from '../util';

const PORT_TYPES = ['protocol', 'port', 'targetPort', 'nodePort'];

const COLUMN_NAME_MAPPER = {
  port: 'service_port',
  protocol: 'protocol',
  targetPort: 'container_port',
  nodePort: 'node_port',
  name: 'port_name',
} as const;

@Component({
  selector: 'rc-service-ports',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicePortsDisplayComponent {
  @Input()
  service: Service;

  @Input()
  inTable: boolean;

  @Input()
  limit: number;

  @ObservableInput(true)
  service$: Observable<Service>;

  @ObservableInput(true)
  inTable$: Observable<boolean>;

  @ObservableInput(true, 3)
  limit$: Observable<number>;

  COLUMN_NAME_MAPPER = COLUMN_NAME_MAPPER;
  getServiceProtocolDisplay = getServiceProtocolDisplay;

  columns$ = this.inTable$.pipe(
    map(inTable => {
      return inTable ? PORT_TYPES : [NAME, ...PORT_TYPES];
    }),
  );

  shownPorts$ = combineLatest([this.service$, this.inTable$, this.limit$]).pipe(
    map(([{ spec: { ports } }, inTable, limit]) =>
      inTable && Number.isFinite(limit) && limit < ports.length
        ? ports.slice(0, limit)
        : ports,
    ),
    publishRef(),
  );
}
