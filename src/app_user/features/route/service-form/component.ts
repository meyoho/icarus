import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { intersection } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { AlternateBackend, Service } from 'app/typings';

@Component({
  selector: 'alk-route-service-form',
  templateUrl: 'template.html',
  styles: [
    `
      :host {
        display: flex;
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class ServiceFormComponent
  extends BaseResourceFormArrayComponent<AlternateBackend>
  implements OnInit, OnDestroy {
  @ObservableInput(true)
  mainService$: Observable<string>;

  @Input()
  mainService: string;

  @ObservableInput(true)
  services$: Observable<Service[]>;

  @Input()
  services: Service[];

  destroy$$ = new Subject<void>();

  filteredServices: Service[] = [];

  @Output()
  portsChange = new EventEmitter<string[]>();

  constructor(public injector: Injector) {
    super(injector);
    this.createNewControl = this.createNewControl.bind(this);
  }

  ngOnInit() {
    super.ngOnInit();
    combineLatest([this.mainService$, this.services$])
      .pipe(
        map(([mainService, services]) =>
          (services || []).filter(
            service => service.metadata.name !== mainService,
          ),
        ),
        tap(filteredServices => (this.filteredServices = filteredServices)),
        takeUntil(this.destroy$$),
      )
      .subscribe();
  }

  getOnFormArrayResizeFn() {
    return this.createNewControl;
  }

  filterServices(index: number) {
    if (!this.services || !this.mainService) {
      return [];
    }
    const ports =
      this.services.find(service => service.metadata.name === this.mainService)
        .spec.ports || [];
    const controls = this.form.controls.slice(0, index);
    return this.filteredServices.filter(
      service =>
        !controls.find(ctrl => ctrl.value.name === service.metadata.name) &&
        intersection(
          ports.map(({ name }) => name),
          service.spec.ports.map(({ name }) => name),
        ).length,
    );
  }

  getWeightsPercent(weight: number) {
    const total = this.form.controls.reduce(
      (acc, control) => acc + (control.get('weight').value || 0),
      // from main service
      100,
    );
    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  onServiceChange() {
    const serviceNames = this.form.controls.reduce<string[]>((acc, ctrl) => {
      const { name } = ctrl.value;
      if (name) {
        acc.push(name);
      }
      return acc;
    }, []);
    const ports = this.services.reduce<Set<string>>((acc, service) => {
      if (serviceNames.includes(service.metadata.name)) {
        service.spec.ports.forEach(port => acc.add(port.name));
      }
      return acc;
    }, new Set());
    this.portsChange.emit([...ports]);
  }

  private createNewControl() {
    return this.fb.group({
      name: this.fb.control(''),
      weight: this.fb.control(100),
    });
  }
}
