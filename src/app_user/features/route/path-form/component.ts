import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector,
  Input,
} from '@angular/core';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alk-route-path-form',
  template: `
    <span>/</span>
    <input
      aui-input
      [formControl]="form"
      [placeholder]="'path' | translate"
      style="margin-left:5px;"
    />
  `,
  styles: [
    `
      :host {
        display: inline-flex;
        align-items: center;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class PathFormComponent extends BaseResourceFormComponent<string> {
  @HostBinding('style.flex')
  @Input()
  flex: number;

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.control('');
  }

  getDefaultFormModel() {
    return '';
  }

  getResourceMergeStrategy() {
    return false;
  }

  adaptFormModel(value: string) {
    return '/' + (value || '');
  }

  adaptResourceModel(value: string) {
    return value && value.startsWith('/') ? value.slice(1) : value;
  }
}
