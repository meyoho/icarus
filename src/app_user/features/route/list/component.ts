import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
  isAllowed,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  RESOURCE_TYPES,
  ResourceType,
  Route,
  WorkspaceListParams,
} from 'app/typings';
import { ACTION } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class RouteListComponent {
  columns = [NAME, 'domain', 'k8s_service', 'weight', 'port', ACTION];

  params$: Observable<WorkspaceListParams> = combineLatest([
    this.route.queryParamMap,
    this.workspace.baseParams,
  ]).pipe(
    map(([params, { cluster, namespace }]) => ({
      cluster,
      namespace,
      keyword: params.get('keyword'),
    })),
  );

  permissions$ = this.workspace.baseParams.pipe(
    switchMap(({ cluster, namespace }) =>
      this.permission.getAccess({
        type: RESOURCE_TYPES.ROUTE,
        cluster,
        namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly permission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly workspace: WorkspaceComponent,
  ) {}

  dataFetcher({ cluster, namespace, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.ROUTE,
      cluster,
      namespace,
      queryParams,
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  deleteRoute(route: Route) {
    this.k8sUtil
      .deleteResource(
        {
          type: RESOURCE_TYPES.ROUTE,
          resource: route,
          cluster: this.workspace.baseParamsSnapshot.cluster,
        },
        'openshift_route',
      )
      .subscribe(() => this.list.delete(route));
  }

  getWeightPercent(weight: number, route: Route) {
    return (
      +(
        (weight /
          (route.spec.to.weight +
            route.spec.alternateBackends.reduce(
              (sum, backend) => sum + backend.weight,
              0,
            ))) *
        100
      ).toFixed(2) + '%'
    );
  }
}
