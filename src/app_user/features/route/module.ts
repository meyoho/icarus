import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { RouteDetailComponent } from './detail/component';
import { RouteEditComponent } from './edit/component';
import { HostFormComponent } from './host-form/component';
import { RouteListComponent } from './list/component';
import { PathFormComponent } from './path-form/component';
import { RouteRoutingModule } from './routing.module';
import { ServiceFormComponent } from './service-form/component';

@NgModule({
  imports: [SharedModule, RouteRoutingModule],
  declarations: [
    RouteListComponent,
    RouteDetailComponent,
    RouteEditComponent,
    HostFormComponent,
    PathFormComponent,
    ServiceFormComponent,
  ],
})
export class RouteModule {}
