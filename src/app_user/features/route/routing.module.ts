import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RouteDetailComponent } from './detail/component';
import { RouteEditComponent } from './edit/component';
import { RouteListComponent } from './list/component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: RouteListComponent,
      },
      {
        path: 'detail/:name',
        component: RouteDetailComponent,
      },
      {
        path: 'create',
        component: RouteEditComponent,
      },
      {
        path: 'update/:name',
        component: RouteEditComponent,
      },
    ]),
  ],
})
export class RouteRoutingModule {}
