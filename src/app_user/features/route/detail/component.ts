import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import {
  AlternateBackend,
  RESOURCE_TYPES,
  ResourceType,
  Route,
  WorkspaceDetailParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class RouteDetailComponent {
  params$ = combineLatest([
    this.route.paramMap,
    this.workspace.baseParams,
  ]).pipe(
    map(([params, baseParams]) => ({
      ...baseParams,
      name: params.get(NAME),
    })),
  );

  asyncDataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetchRoute.bind(this),
  });

  alternateBackends: AlternateBackend[] = [];

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly workspace: WorkspaceComponent,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  fetchRoute({ cluster, namespace, name }: WorkspaceDetailParams) {
    return combineLatest([
      this.k8sApi
        .getResource<Route>({
          type: RESOURCE_TYPES.ROUTE,
          cluster,
          namespace,
          name,
        })
        .pipe(
          tap(route => {
            this.alternateBackends = [get(route, ['spec', 'to'])].concat(
              get(route, ['spec', 'alternateBackends'], []),
            );
          }),
        ),
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.ROUTE,
        cluster,
        namespace,
        name,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
      }),
    ]).pipe(map(([resource, permissions]) => ({ resource, permissions })));
  }

  deleteRoute(route: Route) {
    this.k8sUtil
      .deleteResource(
        {
          type: RESOURCE_TYPES.ROUTE,
          resource: route,
          cluster: this.workspace.baseParamsSnapshot.cluster,
        },
        'openshift_route',
      )
      .subscribe(() => {
        this.router.navigate(['../..'], {
          relativeTo: this.route,
        });
      });
  }

  getWeightsPercent(weight: number) {
    const total = this.alternateBackends.reduce(
      (acc, curr) => acc + (curr.weight || 0),
      0,
    );
    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }
}
