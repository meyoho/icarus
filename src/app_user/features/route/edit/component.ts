import {
  K8sApiService,
  NAME,
  TOKEN_BASE_DOMAIN,
  TranslateService,
  matchExpressionsToString,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump, safeLoad } from 'js-yaml';
import { get, merge } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { EMPTY, Subject, combineLatest } from 'rxjs';
import {
  map,
  pluck,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  Domain,
  RESOURCE_TYPES,
  ResourceType,
  Route,
  Service,
  ServicePort,
  getYamlApiVersion,
} from 'app/typings';
import {
  ASSIGN_ALL,
  K8S_RESOURCE_NAME_BASE,
  STANDALONE_NG_MODEL,
  createActions,
  getResourceYaml,
  yamlWriteOptions,
} from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class RouteEditComponent extends BaseResourceFormGroupComponent<Route>
  implements OnInit {
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  destroy$ = new Subject<void>();

  name$ = this.route.paramMap.pipe(
    map(params => params.get(NAME)),
    tap(name => (this.isUpdate = !!name)),
  );

  isUpdate: boolean;

  // 是否启用备用流量功能
  abEnabled = true;

  routeModel = this.getDefaultFormModel();

  route$ = combineLatest([this.workspace.baseParams, this.name$]).pipe(
    switchMap(([{ cluster, namespace }, name]) =>
      name
        ? this.k8sApi.getResource<Route>({
            type: RESOURCE_TYPES.ROUTE,
            cluster,
            namespace,
            name,
          })
        : EMPTY,
    ),
    publishRef(),
    tap(route => this.form.patchValue((this.routeModel = route))),
  );

  originalYaml$ = this.route$.pipe(map(route => safeDump(route)));

  domains$ = this.workspace.baseParams.pipe(
    switchMap(({ project, cluster }) =>
      this.k8sApi.getGlobalResourceList<Domain>({
        type: RESOURCE_TYPES.DOMAIN,
        queryParams: {
          labelSelector: matchExpressionsToString([
            {
              key: `project.${this.baseDomain}/name`,
              operator: 'in',
              values: [project, ASSIGN_ALL],
            },
            {
              key: `cluster.${this.baseDomain}/name`,
              operator: 'in',
              values: [cluster, ASSIGN_ALL],
            },
          ]),
        },
      }),
    ),
    pluck('items'),
    publishRef(),
    takeUntil(this.destroy$),
  );

  services$ = this.workspace.baseParams.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sApi.getResourceList<Service>({
        type: RESOURCE_TYPES.SERVICE,
        cluster,
        namespace,
      }),
    ),
    pluck('items'),
    takeUntil(this.destroy$),
    shareReplay(1),
    tap(services => (this.services = services)),
  );

  formOrYaml = true;

  yaml = '';

  services: Service[] = [];
  ports: string[];

  STANDALONE_NG_MODEL = STANDALONE_NG_MODEL;
  actionsConfig = createActions;
  codeEditorOptions = yamlWriteOptions;

  constructor(
    public injector: Injector,
    public location: Location,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly workspace: WorkspaceComponent,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
    this.getSelectedService = this.getSelectedService.bind(this);
    this.onServiceNameChange = this.onServiceNameChange.bind(this);
  }

  createForm() {
    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control(''),
      }),
      spec: this.fb.group({
        host: this.fb.control(''),
        path: this.fb.control(''),
        port: this.fb.group({
          targetPort: this.fb.control({
            value: '',
            disabled: true,
          }),
        }),
        to: this.fb.group({
          name: this.fb.control(''),
        }),
        alternateBackends: this.fb.control([]),
      }),
    });
  }

  getDefaultFormModel(): Route {
    return {
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.ROUTE),
      metadata: {
        name: '',
        namespace: this.workspace.baseParamsSnapshot.namespace,
      },
      spec: {
        host: '',
        port: {
          targetPort: '',
        },
        path: '',
        to: {
          kind: 'Service',
          name: '',
          weight: 100,
        },
        alternateBackends: [],
      },
    };
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get(['spec', 'to', NAME])
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(this.onServiceNameChange);
  }

  onServiceNameChange(serviceName?: string) {
    const specGroup = this.form.get('spec') as FormGroup;
    const portCtrl = specGroup.get('port') as FormControl;
    const targetPortCtrl = portCtrl.get('targetPort');
    targetPortCtrl.reset();
    targetPortCtrl[serviceName ? 'enable' : 'disable']();
  }

  onFormOrYamlChange() {
    if (this.formOrYaml) {
      this.form.patchValue(this.getRoute(safeLoad(this.yaml)), {
        emitEvent: false,
        onlySelf: true,
      });
    } else {
      this.yaml = getResourceYaml(this.getRoute(this.form.getRawValue()));
      this.cdr.detectChanges();
    }
  }

  getRoute(route: Route) {
    const merged = merge({}, this.routeModel, route);
    merged.spec.alternateBackends =
      get(route, ['spec', 'alternateBackends'], []).length > 0
        ? route.spec.alternateBackends.concat()
        : [];
    return merged;
  }

  getSelectedService(serviceName: string) {
    return this.services.find(service => service.metadata.name === serviceName);
  }

  onSubmit() {
    if (!this.formOrYaml && !this.yaml.trim()) {
      return;
    }

    const route = this.getRoute(
      this.formOrYaml ? this.form.getRawValue() : safeLoad(this.yaml),
    );

    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']<Route>({
      type: RESOURCE_TYPES.ROUTE,
      cluster: this.workspace.baseParamsSnapshot.cluster,
      resource: route,
    }).subscribe(() => {
      this.message.success(
        this.translate.get(
          `openshift_route_${this.isUpdate ? 'updated' : 'created'}`,
        ),
      );
      this.router.navigate(
        [this.isUpdate ? '../..' : '..', 'detail', route.metadata.name],
        {
          relativeTo: this.route,
        },
      );
    });
  }

  mapPorts(ports: ServicePort[]) {
    return ports && ports.map(port => port.name);
  }
}
