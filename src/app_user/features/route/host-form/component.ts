import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Domain, HostModel } from 'app/typings';
import { parseHost, stringifyHostModel } from 'app/utils';

@Component({
  selector: 'alk-route-host-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class HostFormComponent
  extends BaseResourceFormComponent<string, HostModel>
  implements OnInit, OnDestroy {
  @ValueHook(function (this: HostFormComponent, domains: Domain[]) {
    this.form.patchValue(this.adaptResourceModel(this.host, domains));
  })
  @Input()
  domains: Domain[];

  host: string;

  destroy$$ = new Subject<void>();

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      prefix: this.fb.control({
        value: '',
        disabled: true,
      }),
      value: this.fb.control(''),
    });
  }

  ngOnInit() {
    super.ngOnInit();
    const prefixCtrl = this.form.get('prefix');
    this.form
      .get('value')
      .valueChanges.pipe(takeUntil(this.destroy$$))
      .subscribe((value: string) => {
        const isFull = !value.startsWith('*');
        if (isFull) {
          prefixCtrl.setValue('');
        }
        prefixCtrl[isFull ? 'disable' : 'enable']();
      });
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  adaptResourceModel(host: string, domains = this.domains) {
    this.host = host;
    return parseHost(host, domains);
  }

  adaptFormModel = stringifyHostModel;

  getDefaultFormModel() {
    return {
      name: '',
      value: '',
    };
  }

  getResourceMergeStrategy() {
    return true;
  }
}
