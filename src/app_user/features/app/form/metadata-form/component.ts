import { ObjectMeta } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

interface MetadataFormModel extends ObjectMeta {
  displayName?: string;
}

@Component({
  selector: 'rc-application-metadata-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMetadataFormComponent
  extends BaseResourceFormGroupComponent<ObjectMeta, MetadataFormModel>
  implements OnInit {
  @Input()
  isUpdate: boolean;

  @Input()
  namespace: string;

  env: Environments;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  constructor(injector: Injector, private readonly k8sUtil: K8sUtilService) {
    super(injector);
    this.env = this.injector.get(ENVIRONMENTS);
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: MetadataFormModel): ObjectMeta {
    const { displayName, annotations, ...rest } = formModel;
    return {
      ...rest,
      namespace: this.namespace,
      annotations: {
        ...(annotations || {}),
        [`${this.env.LABEL_BASE_DOMAIN}/display-name`]: displayName,
      },
    };
  }

  adaptResourceModel(metadata: ObjectMeta): MetadataFormModel {
    return {
      ...metadata,
      displayName: this.k8sUtil.getDisplayName({ metadata }),
    };
  }

  createForm() {
    return this.fb.group({
      name: [
        '',
        this.isUpdate
          ? []
          : [
              Validators.required,
              Validators.maxLength(20),
              Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
            ],
      ],
      displayName: [
        {
          value: '',
          disabled: false,
        },
      ],
    });
  }
}
