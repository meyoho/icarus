// tslint:disable: no-output-on-prefix
import {
  KubernetesResource,
  NAME,
  ObjectMeta,
  TranslateService,
} from '@alauda/common-snippet';
import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { safeDump } from 'js-yaml';
import { Subject } from 'rxjs';
import { filter, first, take, takeUntil } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { AddServiceFormDialogComponent } from 'app/features-shared/app/dialog/add-service/dialog';
import {
  PodControllerKindEnum,
  PodControllerKinds,
} from 'app/features-shared/app/utils';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  ApplicationFormModel,
  Ingress,
  Service,
  WorkspaceBaseParams,
} from 'app/typings';
import { ACTION, TYPE, updateActions, yamlWriteOptions } from 'app/utils';

import { AppDataService } from '../../app-data.service';
import { IngressFormDialogComponent } from '../../dialog/add-ingress/dialog';
import { AddResourceComponent } from '../../dialog/add-resource/component';
import { getResourcesFromYaml } from '../../util';

const CHANGE_CAUSE_TYPE = 'change-cause';

export interface AppSubmitOption {
  forceUpdate?: boolean;
}
@Component({
  selector: 'rc-app-create-preview-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreatePreviewFormComponent
  implements OnInit, OnDestroy {
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  @Input() isUpdate: boolean;
  @Input() baseParams: WorkspaceBaseParams;

  @Output()
  onAddComponent = new EventEmitter<RcImageSelection>();

  @Output()
  onUpdateComponent = new EventEmitter<{
    name: string;
    kind: string;
  }>();

  @Output()
  onSubmit = new EventEmitter<AppSubmitOption>();

  @Output()
  onCancel = new EventEmitter<void>();

  onDestroy$ = new Subject<void>();

  initialized: boolean;
  metadata: ObjectMeta;
  resources: KubernetesResource[];
  yaml = '';
  latestYaml = '';
  versionComment = '';
  forceUpdate: boolean;

  mode = 'list';
  columns = [NAME, TYPE, 'image', ACTION];
  editorActions = updateActions;
  editorOptions = yamlWriteOptions;
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly messageService: MessageService,
    private readonly notificationService: NotificationService,
    private readonly dialogService: DialogService,
    private readonly appDataService: AppDataService,
    private readonly appSharedService: AppSharedService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.appDataService.resourceNotFound$.pipe(take(1)).subscribe(res => {
      if (res) {
        this.messageService.error(this.translate.get('resource_not_exist'));
        this.onCancel.emit();
      }
      this.cdr.markForCheck();
    });

    this.appDataService.applicationModel$
      .pipe(
        filter(item => !!item),
        takeUntil(this.onDestroy$),
      )
      .subscribe((formModel: ApplicationFormModel) => {
        this.resources = formModel.spec.componentTemplates;
        if (!this.metadata) {
          this.metadata = formModel.metadata;
        }
        this.initialized = true;
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  submit() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }

    if (
      this.mode === 'list' ||
      (this.mode === 'yaml' && this.yamlToFormModel())
    ) {
      // in case metadata is null
      const annotations = this.metadata.annotations || {};
      annotations[
        `${this.k8sUtil.normalizeType(CHANGE_CAUSE_TYPE, 'app')}`
      ] = this.versionComment;
      this.metadata.annotations = annotations;
      this.appDataService.updateMetadata(this.metadata);
      return this.onSubmit.emit(
        this.isUpdate
          ? {
              forceUpdate: this.forceUpdate,
            }
          : null,
      );
    }
  }

  cancel() {
    this.onCancel.emit(null);
  }

  modeChange(mode: 'list' | 'yaml') {
    if (this.mode === mode) {
      return;
    }
    if (mode === 'yaml') {
      this.yaml = this.formModelToYaml();
      this.latestYaml = this.yaml;
    } else if (mode === 'list') {
      this.yamlToFormModel();
    }
  }

  trackByFn(_index: number) {
    return _index;
  }

  getResourceKindGroup(kind: string) {
    switch (kind) {
      case PodControllerKindEnum.Deployment:
      case PodControllerKindEnum.DaemonSet:
      case PodControllerKindEnum.StatefulSet:
        return 'Workload';
      default:
        return kind;
    }
  }

  addComponent() {
    this.appDataService.updateMetadata(this.metadata);
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((params: RcImageSelection) => {
        if (params) {
          this.onAddComponent.emit(params);
        }
      });
  }

  addService() {
    const dialogRef = this.dialogService.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.baseParams.namespace,
        workloads: this.getWorkloads(),
        existedNames: this.getExistedNamesByKind('Service'),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (data) {
          this.appDataService.addResource(data);
        }
      });
  }

  addIngress() {
    const dialogRef = this.dialogService.open(IngressFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        baseParams: this.baseParams,
        services: this.getServices(),
        existedNames: this.getExistedNamesByKind('Ingress'),
      },
    });
    return (
      dialogRef.componentInstance.close
        .pipe(first())
        // eslint-disable-next-line sonarjs/no-identical-functions
        .subscribe((data: Ingress) => {
          dialogRef.close();
          if (data) {
            this.appDataService.addResource(data);
          }
        })
    );
  }

  addResourceFromYaml() {
    const dialogRef = this.dialogService.open(AddResourceComponent, {
      size: DialogSize.Large,
      data: {
        appDataService: this.appDataService,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((resource: KubernetesResource) => {
        dialogRef.close();
        if (resource) {
          return this.appDataService.addResource(resource);
        }
      });
  }

  updateResource(resource: KubernetesResource) {
    switch (resource.kind) {
      case PodControllerKindEnum.Deployment:
      case PodControllerKindEnum.DaemonSet:
      case PodControllerKindEnum.StatefulSet:
        this.updateComponent({
          name: resource.metadata.name,
          kind: resource.kind.toLowerCase(),
        });
        break;
      case 'Service': {
        this.updateService(resource as Service);
        break;
      }
      case 'Ingress': {
        this.updateIngress(resource as Ingress);
        break;
      }
      default: {
        this.updateResourceByYaml(resource);
        break;
      }
    }
  }

  removeResource(resource: KubernetesResource) {
    this.appDataService.removeResource(resource);
  }

  private updateComponent(params: { name: string; kind: string }) {
    this.onUpdateComponent.emit(params);
  }

  private updateService(service: Service) {
    const dialogRef = this.dialogService.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...service },
        namespace: this.baseParams.namespace,
        workloads: this.getWorkloads(),
        existedNames: this.getExistedNamesByKind('Service').filter(
          n => n !== service.metadata.name,
        ),
        isUpdateApp: this.isUpdate,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((resource: Service) => {
        dialogRef.close();
        this._updateResource(resource, service);
      });
  }

  private updateIngress(ingress: Ingress) {
    const dialogRef = this.dialogService.open(IngressFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...ingress },
        baseParams: this.baseParams,
        services: this.getServices(),
        isUpdateApp: this.isUpdate,
        existedNames: this.getExistedNamesByKind('Ingress').filter(
          n => n !== ingress.metadata.name,
        ),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((resource: Ingress) => {
        dialogRef.close();
        this._updateResource(resource, ingress);
      });
  }

  private updateResourceByYaml(resource: KubernetesResource) {
    const dialogRef = this.dialogService.open(AddResourceComponent, {
      size: DialogSize.Fullscreen,
      data: {
        appDataService: this.appDataService,
        resource,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((_resource: KubernetesResource) => {
        dialogRef.close();
        this._updateResource(_resource, resource);
      });
  }

  private yamlToFormModel() {
    try {
      const application = this.appDataService.applicationModel;
      const resources = getResourcesFromYaml(
        this.yaml,
        this.baseParams.namespace,
      );
      this.appDataService.setApplicationFormModel({
        ...application,
        spec: {
          ...application.spec,
          componentTemplates: resources,
        },
      });
    } catch (err) {
      this.notificationService.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return false;
    }
    return true;
  }

  private formModelToYaml() {
    let yaml = '';
    const payload = this.appDataService.generateApplicationPayload(
      this.appDataService.applicationModel,
      this.baseParams,
    );
    try {
      const jsonArray = JSON.parse(
        JSON.stringify(payload.spec.componentTemplates),
      );
      yaml = jsonArray
        .sort((a: KubernetesResource, b: KubernetesResource) =>
          a.kind <= b.kind ? -1 : 1,
        )
        .map((res: KubernetesResource) =>
          safeDump(res, {
            sortKeys: true,
          }),
        )
        .join('---\r\n');
    } catch (err) {
      console.error(err);
    }
    return yaml;
  }

  private getWorkloads() {
    return this.resources.filter(c => PodControllerKinds.includes(c.kind));
  }

  private getServices() {
    return this.resources.filter(c => c.kind === 'Service');
  }

  private _updateResource(
    resource: KubernetesResource,
    original: KubernetesResource,
  ) {
    if (resource) {
      return this.appDataService.updateResource(resource, {
        name: original.metadata.name,
        kind: original.kind,
      });
    }
  }

  private getExistedNamesByKind(kind: string) {
    return this.appDataService.applicationModel.spec.componentTemplates
      .filter(r => r.kind === kind)
      .map(r => r.metadata.name);
  }
}
