import { Callback, noop } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectorRef, Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AddServiceFormDialogComponent } from 'app/features-shared/app/dialog/add-service/dialog';
import { Service } from 'app/typings';

@Component({
  selector: 'rc-services-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
        width: 100%;
      }
    `,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ServicesFormComponent),
      multi: true,
    },
  ],
})
export class ServicesFormComponent implements ControlValueAccessor {
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
  ) {}

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input() defaultName: string;
  @Input() existedNames: string[];

  services: Service[] = [];

  onCvaChange: Callback;
  onCvaTouched: Callback;

  onChange() {
    this.onCvaChange(this.services);
  }

  onValidatorChange = noop;

  registerOnChange(fn: (value: Service[]) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_resource: Service[]) {
    this.services = _resource || [];
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.namespace,
        defaultName: this.defaultName,
        existedNames: this.getExistedNames(),
        disableWorkloadSelect: true,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (data) {
          this.services.push(data);
        }
        this.onChange();
      });
  }

  remove(index: number) {
    this.services = this.services.filter((_v, i: number) => {
      return index !== i;
    });
    this.onChange();
  }

  update(index: number) {
    const service = this.services[index];
    const dialogRef = this.dialogService.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...service },
        namespace: this.namespace,
        existedNames: this.getExistedNames().filter(
          n => n !== service.metadata.name,
        ),
        disableWorkloadSelect: true,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (data) {
          this.services[index] = { ...data };
        }
        this.onChange();
      });
  }

  private getExistedNames() {
    return (this.existedNames || []).concat(
      this.services.map(r => r.metadata.name),
    );
  }
}
