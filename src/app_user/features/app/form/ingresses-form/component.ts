import { Callback, noop } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectorRef, Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { first } from 'rxjs/operators';

import { Ingress, Service, WorkspaceBaseParams } from 'app/typings';

import { IngressFormDialogComponent } from '../../dialog/add-ingress/dialog';

@Component({
  selector: 'rc-ingresses-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
        width: 100%;
      }
    `,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IngressesFormComponent),
      multi: true,
    },
  ],
})
export class IngressesFormComponent implements ControlValueAccessor {
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
  ) {}

  @Input()
  baseParams: WorkspaceBaseParams;

  @Input() services: Service[];
  ingresses: Ingress[] = [];
  @Input() defaultName: string;
  @Input() existedNames: string[];

  onCvaChange: Callback;
  onCvaTouched: Callback;

  onChange() {
    this.onCvaChange(this.ingresses);
  }

  onValidatorChange = noop;

  registerOnChange(fn: (value: Ingress[]) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_resource: Ingress[]) {
    this.ingresses = _resource || [];
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(IngressFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        baseParams: this.baseParams,
        defaultName: this.defaultName,
        services: this.services,
        existedNames: this.getExistedNames(),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Ingress) => {
        dialogRef.close();
        if (data) {
          this.ingresses.push(data);
        }
        this.onChange();
      });
  }

  update(index: number) {
    const ingress = this.ingresses[index];
    const dialogRef = this.dialogService.open(IngressFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...ingress },
        baseParams: this.baseParams,
        services: this.services,
        existedNames: this.getExistedNames().filter(
          n => n !== ingress.metadata.name,
        ),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Ingress) => {
        dialogRef.close();
        if (data) {
          this.ingresses[index] = { ...data };
        }
        this.onChange();
      });
  }

  remove(index: number) {
    this.ingresses = this.ingresses.filter((_v, i: number) => {
      return index !== i;
    });
    this.onChange();
  }

  private getExistedNames() {
    return (this.existedNames || []).concat(
      this.ingresses.map(r => r.metadata.name),
    );
  }
}
