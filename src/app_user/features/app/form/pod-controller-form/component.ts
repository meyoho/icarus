import {
  LABELS,
  METADATA,
  ObservableInput,
  StringMap,
  TOKEN_BASE_DOMAIN,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { lensPath, set } from 'ramda';
import { Observable, Subject, combineLatest } from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { PodControllerKindEnum } from 'app/features-shared/app/utils';
import {
  DaemonSetTypeMeta,
  DeploymentTypeMeta,
  Environments,
  PodControllerFormModel,
  StatefulSet,
  StatefulSetTypeMeta,
  Workload,
  WorkloadOption,
} from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

@Component({
  selector: 'rc-pod-controller-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerFormComponent
  extends BaseResourceFormGroupComponent<Workload, PodControllerFormModel>
  implements OnInit, OnDestroy, OnChanges {
  onDestroy$: Subject<void> = new Subject<void>();
  @Input()
  project: string;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  @Input() appName: string;
  @Input() hasPrefix: boolean;
  // 更新app的时候，podController的名称和类型限制从UI上不能修改
  @Input() isUpdateApp: boolean;

  @ObservableInput(true)
  appName$: Observable<string>;

  env: Environments;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  matchLabels: StringMap;

  workloadOptions$: Observable<WorkloadOption[]>;

  kindOptions = [
    DeploymentTypeMeta.kind,
    DaemonSetTypeMeta.kind,
    StatefulSetTypeMeta.kind,
  ];

  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      prefixPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  constructor(
    injector: Injector,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    const nameControl = this.form.get('metadata.name');
    this.form
      .get('kind')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((value: string) => {
        if (value === DaemonSetTypeMeta.kind) {
          this.form.get('spec.replicas').disable();
        } else {
          this.form.get('spec.replicas').enable();
        }
      });

    if (this.hasPrefix) {
      combineLatest([
        this.form.get('suffixName').valueChanges.pipe(
          startWith(this.form.get('suffixName').value),
          filter(v => !!v),
        ),
        this.appName$.pipe(startWith(this.appName)),
      ])
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(([suffix, appName]) => {
          nameControl.setValue(`${appName}-${suffix}`);
        });
    }

    this.workloadOptions$ = combineLatest([
      nameControl.valueChanges.pipe(startWith(nameControl.value)),
      this.form
        .get('kind')
        .valueChanges.pipe(startWith(this.form.get('kind').value)),
    ]).pipe(
      debounceTime(300),
      map(([name, kind]) => [
        {
          kind,
          name,
        },
      ]),
      publishRef(),
    );
  }

  ngOnChanges({ hasPrefix }: SimpleChanges) {
    if (hasPrefix && hasPrefix.currentValue && this.form) {
      this.form.get('suffixName').enable({
        emitEvent: false,
      });
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control(
        {
          value: '',
        },
        [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const specForm = this.fb.group({
      selector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
      strategy: this.fb.control({}),
      replicas: this.fb.control(1, [Validators.min(0), Validators.required]),
      template: this.fb.control(null),
    });

    return this.fb.group({
      suffixName: this.fb.control(
        {
          value: '',
          disabled: !this.hasPrefix,
        },
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ),
      kind: this.fb.control('', [Validators.required]),
      metadata: metadataForm,
      spec: specForm,
      network: this.fb.control({}),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptResourceModel(resource: Workload): PodControllerFormModel {
    if (!resource) {
      return this.getDefaultFormModel();
    }
    this.recordMatchLabels(resource);
    if (this.hasPrefix) {
      const suffixName = resource.metadata ? resource.metadata.name : '';
      resource.metadata.name = `${this.appName}-${resource.metadata.name}`;
      return {
        ...resource,
        suffixName,
      };
    } else {
      return resource;
    }
  }

  adaptFormModel(formModel: PodControllerFormModel): Workload {
    let resource = { ...formModel };
    resource.apiVersion =
      resource.kind === StatefulSetTypeMeta.kind
        ? StatefulSetTypeMeta.apiVersion
        : DeploymentTypeMeta.apiVersion;
    delete resource.suffixName;
    // StatefulSet 需要指定 spec.serviceName
    if (resource.kind === PodControllerKindEnum.StatefulSet) {
      (resource as StatefulSet).spec.serviceName = resource.metadata.name;
    } else {
      delete (resource as any).spec.serviceName;
    }
    // pod labels 添加 service.alauda.io/name=`${kind.toLowerCase()}-${name}`
    const labelPath = lensPath([
      'spec',
      'template',
      METADATA,
      LABELS,
      `service.${this.baseDomain}/name`,
    ]);
    resource = set(
      labelPath,
      `${resource.kind.toLowerCase()}-${resource.metadata.name}`,
      resource,
    );
    // http://jira.alaudatech.com/browse/ACP-25 微服务需要的两个指定的 pod labels
    resource.spec.template.metadata.labels.version = 'v1';
    resource.spec.template.metadata.labels.app = `${resource.kind.toLowerCase()}-${
      resource.metadata.name
    }`;

    // annotations
    if (!resource.spec.template.metadata.annotations) {
      resource.spec.template.metadata.annotations = {};
    }

    return resource;
  }

  get shouldDisableReplicas() {
    return this.form.get('kind').value === PodControllerKindEnum.DaemonSet;
  }

  get shouldHideStrategy() {
    return this.form.get('kind').value !== PodControllerKindEnum.Deployment;
  }

  private recordMatchLabels(resource: Workload) {
    if (
      !this.matchLabels &&
      get(resource, ['spec', 'selector', 'matchLabels'])
    ) {
      this.matchLabels = get(resource, ['spec', 'selector', 'matchLabels']);
    }
  }
}
