import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  PodControllerKindEnum,
  PodControllerUpdateStrategyEnum,
} from 'app/features-shared/app/utils';
import {
  RollingUpdateDaemonSet,
  RollingUpdateDeployment,
  RollingUpdateStatefulSetStrategy,
  StrategyFormModel,
} from 'app/typings';
import { TYPE } from 'app/utils';

@Component({
  selector: 'rc-update-strategy-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateStrategyFormComponent
  extends BaseResourceFormGroupComponent<StrategyFormModel>
  implements OnInit, OnChanges {
  @Input()
  podControllerKind: string;

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnChanges({ podControllerKind }: SimpleChanges) {
    if (
      podControllerKind &&
      podControllerKind.currentValue &&
      podControllerKind.previousValue
    ) {
      this.updateStrategyType();
    }
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: StrategyFormModel) {
    if (
      this.podControllerKind &&
      this.podControllerKind === PodControllerKindEnum.Deployment
    ) {
      if (formModel && formModel.rollingUpdate) {
        const rollingUpdate = formModel.rollingUpdate as RollingUpdateDeployment &
          RollingUpdateDaemonSet &
          RollingUpdateStatefulSetStrategy;
        Object.keys(rollingUpdate).forEach(
          (key: keyof typeof rollingUpdate) => {
            rollingUpdate[key] = this.adaptNumbers(rollingUpdate[key]);
          },
        );
      }
      return Object.assign({}, formModel, {
        type: 'RollingUpdate',
      }) as any;
    } else {
      // TODO: 针对三种PodController需要不同的 strategyForm
      return {
        type: 'OnDelete',
      };
    }
  }

  createForm() {
    return this.fb.group({
      type: [PodControllerUpdateStrategyEnum.RollingUpdate],
      rollingUpdate: this.fb.group({
        maxSurge: ['25%'],
        maxUnavailable: ['25%'],
      }),
    });
  }

  private adaptNumbers(value: string | number) {
    if (Number.isInteger(+value)) {
      return +value;
    } else {
      return value;
    }
  }

  private updateStrategyType() {
    if (!this.form) {
      return;
    }
    if (this.podControllerKind === PodControllerKindEnum.Deployment) {
      this.form
        .get(TYPE)
        .setValue(PodControllerUpdateStrategyEnum.RollingUpdate);
    } else {
      this.form.get(TYPE).setValue(PodControllerUpdateStrategyEnum.OnDelete);
    }
  }
}
