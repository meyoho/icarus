import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-app-address',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class AppAddressComponent {
  @Input()
  urlList: string[] = [];
}
