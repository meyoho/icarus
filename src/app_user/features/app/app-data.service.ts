import {
  KubernetesResource,
  LABELS,
  METADATA,
  ObjectMeta,
  publishRef,
} from '@alauda/common-snippet';
import { Inject, Injectable } from '@angular/core';
import { cloneDeep, get, remove, set } from 'lodash-es';
import { all, omit } from 'ramda';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { PodControllerKinds } from 'app/features-shared/app/utils';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  ApplicationFormModel,
  Environments,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';

import { generateApplicationModel } from './util';

@Injectable()
export class AppDataService {
  applicationModelSubject$: BehaviorSubject<
    ApplicationFormModel
  > = new BehaviorSubject<ApplicationFormModel>(null);

  applicationModel$: Observable<ApplicationFormModel>;
  resourceNotFound$ = new ReplaySubject<boolean>();
  updateAppName: string; // name parameter in url when updating app
  private readonly labelBaseDomain: string;
  constructor(
    private readonly acpApi: AcpApiService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    this.applicationModel$ = this.applicationModelSubject$.asObservable().pipe(
      filter(r => !!r),
      publishRef(),
    );
    this.labelBaseDomain = this.env.LABEL_BASE_DOMAIN || 'alauda.io';
  }

  initFetchRequest(cluster: string, namespace: string, name: string) {
    this.updateAppName = name;
    this.acpApi
      .getApplicationYaml({
        cluster,
        namespace,
        name,
      })
      .subscribe(
        resources => {
          this.applicationModelSubject$.next(
            generateApplicationModel(resources),
          );
        },
        () => {
          this.resourceNotFound$.next(true);
        },
      );
  }

  get applicationModel() {
    return this.applicationModelSubject$.value;
  }

  // ***** ApplicationFormModel ***** //

  setApplicationFormModel(app: ApplicationFormModel) {
    this.applicationModelSubject$.next(app);
  }

  updateMetadata(metadata: ObjectMeta) {
    const applicationModel = { ...this.applicationModel };
    applicationModel.metadata = { ...metadata };
    this.applicationModelSubject$.next(applicationModel);
  }

  updateResource(
    resource: KubernetesResource,
    original: {
      name: string;
      kind: string;
    },
  ) {
    const applicationModel = cloneDeep(this.applicationModel);
    const originalResource = applicationModel.spec.componentTemplates.find(
      item =>
        item.metadata.name === original.name && item.kind === original.kind,
    );
    Object.assign(originalResource, resource);
    this.applicationModelSubject$.next(applicationModel);
  }

  addResource(resource: KubernetesResource | KubernetesResource[]) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    const resources = Array.isArray(resource) ? resource : [resource];
    applicationModel.spec.componentTemplates.push(...resources);
    this.applicationModelSubject$.next(applicationModel);
  }

  removeResource(resource: KubernetesResource) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    remove(
      applicationModel.spec.componentTemplates,
      c =>
        c.kind === resource.kind && c.metadata.name === resource.metadata.name,
    );
    this.applicationModelSubject$.next(applicationModel);
  }

  checkResourceName(
    resource: KubernetesResource,
    original?: {
      name: string;
      kind: string;
    },
  ) {
    const distinct = (item: KubernetesResource) => {
      return (
        item.metadata.name !== resource.metadata.name ||
        item.kind !== resource.kind
      );
    };
    const resources = this.applicationModel.spec.componentTemplates;
    if (original) {
      return (
        (resource.kind === original.kind &&
          resource.metadata.name === original.name) ||
        all(distinct)(resources)
      );
    } else {
      return all(distinct)(resources);
    }
  }

  // ***** Utility function ***** //
  generateApplicationPayload(
    formModel: ApplicationFormModel,
    baseParams: WorkspaceBaseParams,
  ): ApplicationFormModel {
    const payload = cloneDeep(formModel);
    payload.spec.componentTemplates
      .filter(c => PodControllerKinds.includes(c.kind))
      .forEach((controller: Workload) => {
        const labelsPath = ['spec', 'template', METADATA, LABELS];
        // 设置 podTemplate labels:  project.alauda.io/name={project-name}
        const projectPath = [
          ...labelsPath,
          `project.${this.labelBaseDomain}/name`,
        ];
        if (!get(controller, projectPath)) {
          set(controller, projectPath, baseParams.project);
        }
        // 设置 podTemplate labels:  app.alauda.io/name={app-name}.{namespace-name}
        const appPath = [...labelsPath, `app.${this.labelBaseDomain}/name`];
        if (!get(controller, appPath)) {
          set(
            controller,
            appPath,
            `${payload.metadata.name}.${payload.metadata.namespace}`,
          );
        }
        // 设置 workload 的 selector, selector 在更新 workload 的时候不允许修改否则会报错
        // 忽略为 service-mesh 定制的 label，忽略 app.alauda.io/name
        if (!controller.spec.selector) {
          const labels = omit(
            ['version', 'app', `app.${this.labelBaseDomain}/name`],
            {
              ...controller.spec.template.metadata.labels,
            },
          );
          controller.spec.selector = {
            matchLabels: labels,
          };
        }
      });
    return payload;
  }
}
