import {
  AuthorizationStateService,
  K8sApiService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, first, map, tap, timeout } from 'rxjs/operators';

import { UpdateImageTagDialogComponent } from 'app/features-shared/app/dialog/update-image-tag/component';
import { UpdateResourceSizeDialogComponent } from 'app/features-shared/app/dialog/update-resource-size/component';
import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  AccountInfo,
  Application,
  ApplicationAddress,
  ApplicationFormModel,
  CronJob,
  RESOURCE_TYPES,
  Workload,
  WorkloadAddress,
  WorkspaceBaseParams,
} from 'app/typings';

@Injectable({
  providedIn: AppSharedModule,
})
export class AppUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly acpApi: AcpApiService,
    private readonly translate: TranslateService,
    private readonly authService: AuthorizationStateService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
  ) {}

  updateImageTag(params: {
    podController?: Workload;
    cronJob?: CronJob;
    containerName: string;
    baseParams: WorkspaceBaseParams;
  }) {
    const dialogRef = this.dialog.open(UpdateImageTagDialogComponent, {
      size: DialogSize.Big,
      data: params,
    });
    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  updateResourceSize(params: {
    podController?: Workload;
    cronJob?: CronJob;
    containerName: string;
    baseParams: WorkspaceBaseParams;
  }) {
    const dialogRef = this.dialog.open(UpdateResourceSizeDialogComponent, {
      size: DialogSize.Big,
      data: params,
    });
    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  rollback(params: {
    cluster: string;
    namespace: string;
    revision: number;
    app: string;
  }) {
    return this.dialog.confirm({
      title: this.translate.get('application_rollback_confirm_title', {
        revision: params.revision,
      }),
      confirmType: ConfirmType.Primary,
      confirmText: this.translate.get('rollback'),
      cancelText: this.translate.get('cancel'),
      beforeConfirm: (resolve, reject) => {
        this.authService
          .getTokenPayload<AccountInfo>()
          .subscribe(({ email }) => {
            this.acpApi
              .rollbackApplication({
                cluster: params.cluster,
                namespace: params.namespace,
                revision: params.revision,
                app: params.app,
                user: email,
              })
              .subscribe(() => {
                this.message.success(
                  this.translate.get('application_rollback_successfully'),
                );
                resolve();
              }, reject);
          });
      },
    });
  }

  createApplication(params: {
    payload: ApplicationFormModel;
    cluster: string;
  }) {
    return this.acpApi
      .createApplication({
        payload: params.payload,
        cluster: params.cluster,
      })
      .subscribe(() => {
        this.message.success(
          this.translate.get('app_create_success', {
            appName: params.payload.metadata.name,
          }),
        );
      });
  }

  updateApplication(params: {
    payload: ApplicationFormModel;
    cluster: string;
  }) {
    return (
      this.acpApi
        .updateApplication({
          payload: params.payload,
          cluster: params.cluster,
        })
        // eslint-disable-next-line sonarjs/no-identical-functions
        .subscribe(() => {
          this.message.success(
            this.translate.get('app_create_success', {
              appName: params.payload.metadata.name,
            }),
          );
        })
    );
  }

  deleteApplication(params: { cluster: string; resource: Application }) {
    return this.dialog
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('app_delete_confirm', {
          app_name: params.resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.APPLICATION,
              cluster: params.cluster,
              resource: params.resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.message.success(
          this.translate.get('app_delete_success', {
            appName: params.resource.metadata.name,
          }),
        );
      });
  }

  getApplicationAddress(params: {
    cluster: string;
    namespace: string;
    app: string;
  }) {
    return this.acpApi
      .getApplicationAddress({
        cluster: params.cluster,
        namespace: params.namespace,
        app: params.app,
      })
      .pipe(
        map((res: ApplicationAddress) => {
          return Object.entries(res.workloads || {}).reduce(
            (obj, [resourceName, serviceMap]) => {
              obj[resourceName] = Object.keys(serviceMap).reduce<string[]>(
                (arr, key) => {
                  return arr.concat(
                    (serviceMap[key] || [])
                      .filter((addrObj: WorkloadAddress) => !!addrObj.host)
                      .map(
                        (addrObj: WorkloadAddress) =>
                          `${addrObj.protocol}://${addrObj.host}:${
                            addrObj.port
                          }${addrObj.url || ''}`,
                      ),
                  );
                },
                [],
              );
              return obj;
            },
            {} as Record<string, string[]>,
          );
        }),
        timeout(10000),
        catchError(() => of({} as Record<string, string[]>)),
      );
  }

  exportResources(params: {
    cluster: string;
    namespace: string;
    app: string;
    components: KubernetesResource[];
  }) {
    return this.acpApi.manageResources(params, 'export');
  }

  importResources(params: {
    cluster: string;
    namespace: string;
    app: string;
    components: KubernetesResource[];
  }) {
    return this.acpApi.manageResources(params, 'import');
  }
}
