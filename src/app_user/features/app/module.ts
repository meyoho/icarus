import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/features-shared/alarm/shared.module';
import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { PodSharedModule } from 'app/features-shared/pod/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { IngressSharedModule } from '../ingress/shared.module';

import { AppAddressComponent } from './app-address/component';
import { AppDataService } from './app-data.service';
import { AppUtilService } from './app-util.service';
import { ApplicationCreateAddComponentPageComponent } from './create/add-component/component';
import { ApplicationCreateComponent } from './create/component';
import { ApplicationCreatePreviewPageComponent } from './create/preview/component';
import { ApplicationCreatePrimaryPageComponent } from './create/primary/component';
import { ApplicationCreateUpdateComponentPageComponent } from './create/update-component/component';
import { AppYamlPageComponent } from './create/yaml-page/component';
import { ApplicationDetailEventComponent } from './detail/event/component';
import { FederalizationDialogComponent } from './detail/federalization-dialog/component';
import { ApplicationDetailHeaderComponent } from './detail/header/component';
import { ApplicationHistoryComponent } from './detail/history/component';
import { ApplicationDetailComponent } from './detail/main/component';
import { ApplicationMonitorChartComponent } from './detail/monitor/chart/component';
import { ApplicationMonitorComponent } from './detail/monitor/component';
import { ApplicationDetailPageComponent } from './detail/page.component';
import { ApplicationResourcesComponent } from './detail/resource/component';
import { ApplicationResourcesManageComponent } from './detail/resources-manage/component';
import { VersionSnapShotComponent } from './detail/version-snapshot/component';
import { ApplicationWorkloadsComponent } from './detail/workloads/component';
import { IngressFormDialogComponent } from './dialog/add-ingress/dialog';
import { AddResourceComponent } from './dialog/add-resource/component';
import { IngressesFormComponent } from './form/ingresses-form/component';
import { ApplicationMetadataFormComponent } from './form/metadata-form/component';
import { PodControllerFormComponent } from './form/pod-controller-form/component';
import { UpdateStrategyFormComponent } from './form/pod-controller-form/strategy-form/component';
import { ApplicationCreatePreviewFormComponent } from './form/preview-form/component';
import { ServicesFormComponent } from './form/services-form/component';
import { ApplicationHistoryDetailComponent } from './history-detail/component';
import { ApplicationListPageComponent } from './list/component';
import { IngressPreviewCardComponent } from './preview-card/ingress/component';
import { IngressPodControllerCardComponent } from './preview-card/pod-controller/component';
import { ServicePreviewCardComponent } from './preview-card/service/component';
import { ApplicationRoutingModule } from './routing.module';
import { AppTopologyComponent } from './topology/component';
import { D3Service } from './topology/d3.service';
import { ArcLinkVisualComponent } from './topology/visuals/arc-link/component';
import { DraggableDirective } from './topology/visuals/draggable.directive';
import { GraphComponent } from './topology/visuals/graph/component';
import { GraphLegendComponent } from './topology/visuals/legend/component';
import { NodeVisualComponent } from './topology/visuals/node/component';
import { TopologyWorkloadStatusComponent } from './topology/visuals/workload-status/component';
import { ZoomableDirective } from './topology/visuals/zoomable.directive';
import { ApplicationUpdateComponent } from './update/component';
import { ApplicationUpdatePreviewPageComponent } from './update/preview/component';

const TOPOLOGY_COMPONENTS = [
  NodeVisualComponent,
  ArcLinkVisualComponent,
  GraphComponent,
  DraggableDirective,
  TopologyWorkloadStatusComponent,
  GraphLegendComponent,
  ZoomableDirective,
  AppTopologyComponent,
];

const FORM_COMPONENTS = [
  ApplicationCreatePreviewFormComponent,
  PodControllerFormComponent,
  UpdateStrategyFormComponent,
  ApplicationMetadataFormComponent,
  ServicesFormComponent,
  IngressesFormComponent,
  IngressFormDialogComponent,
];

const DETAIL_COMPONENTS = [
  ApplicationDetailComponent,
  ApplicationDetailEventComponent,
  ApplicationDetailHeaderComponent,
  ApplicationMonitorComponent,
  ApplicationDetailPageComponent,
  ApplicationResourcesComponent,
  ApplicationWorkloadsComponent,
  ApplicationResourcesManageComponent,
  ApplicationMonitorChartComponent,
  IngressPodControllerCardComponent,
  ServicePreviewCardComponent,
  IngressPreviewCardComponent,
  ApplicationHistoryComponent,
  ApplicationHistoryDetailComponent,
];

@NgModule({
  imports: [
    SharedModule,
    AlarmSharedModule,
    EventSharedModule,
    IngressSharedModule,
    AppSharedModule,
    ImageSharedModule,
    ExecSharedModule,
    PodSharedModule,
    ApplicationRoutingModule,
  ],
  declarations: [
    ApplicationListPageComponent,
    ApplicationCreateComponent,
    ApplicationUpdateComponent,
    ApplicationUpdatePreviewPageComponent,
    ApplicationCreatePrimaryPageComponent,
    ApplicationCreateAddComponentPageComponent,
    ApplicationCreateUpdateComponentPageComponent,
    ApplicationCreatePreviewPageComponent,
    ApplicationResourcesManageComponent,
    VersionSnapShotComponent,
    AddResourceComponent,
    AppYamlPageComponent,
    AppAddressComponent,
    FederalizationDialogComponent,
    ...FORM_COMPONENTS,
    ...DETAIL_COMPONENTS,
    ...TOPOLOGY_COMPONENTS,
  ],
  providers: [D3Service, AppDataService, AppUtilService],
})
export class ApplicationModule {}
