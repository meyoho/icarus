import { KubernetesResource, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
} from '@angular/core';
import { safeDump, safeLoad } from 'js-yaml';

import { createActions, yamlWriteOptions } from 'app/utils';

import { AppDataService } from '../../app-data.service';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddResourceComponent implements OnInit {
  @Output()
  close = new EventEmitter<KubernetesResource>();

  yaml: string;
  originalYaml: string;
  isUpdate: boolean;

  editorActions = createActions;
  editorOptions = yamlWriteOptions;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly data: {
      appDataService: AppDataService;
      resource?: KubernetesResource;
    },
    private readonly notificationService: NotificationService,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data && this.data.resource) {
      this.isUpdate = true;
      this.originalYaml = this.yaml = safeDump(this.data.resource);
    }
  }

  cancel() {
    this.close.next();
  }

  confirm() {
    let resource;
    try {
      resource = safeLoad(this.yaml);
    } catch (err) {
      return this.notificationService.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
    }
    if (!resource.kind || !resource.metadata || !resource.metadata.name) {
      return this.messageService.error(
        this.translate.get('yaml_format_error_message'),
      );
    }
    if (!this.checkResourceName(resource)) {
      return this.messageService.error(
        this.translate.get('resource_name_conflict'),
      );
    }
    this.close.next(resource);
  }

  private checkResourceName(resource: KubernetesResource) {
    let original;
    if (this.data.resource) {
      original = {
        name: this.data.resource.metadata.name,
        kind: this.data.resource.kind,
      };
    }
    return this.data.appDataService.checkResourceName(resource, original);
  }
}
