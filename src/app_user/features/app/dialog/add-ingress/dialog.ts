import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Ingress, Service, WorkspaceBaseParams } from 'app/typings';
import { getDefaultIngress } from 'app_user/features/ingress/util';

@Component({
  templateUrl: 'dialog.html',
})
export class IngressFormDialogComponent {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  ingress: Ingress;
  services: Service[];
  baseParams: WorkspaceBaseParams;
  close = new EventEmitter<Ingress>();

  isUpdate: boolean;
  isUpdateApp: boolean;

  constructor(
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    @Inject(DIALOG_DATA)
    private readonly dialogData: {
      baseParams: WorkspaceBaseParams;
      defaultName: string;
      data?: Ingress;
      services: Service[];
      isUpdateApp: boolean;
      existedNames?: string[];
    },
  ) {
    this.baseParams = dialogData.baseParams;
    this.ingress =
      dialogData.data ||
      getDefaultIngress(
        dialogData.baseParams.namespace,
        dialogData.defaultName || '',
      );
    this.isUpdate = !!dialogData.data;
    this.isUpdateApp = !!dialogData.isUpdateApp;
    this.services = dialogData.services || [];
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    if (
      !this.isUpdateApp &&
      this.dialogData.existedNames &&
      this.dialogData.existedNames.includes(this.ingress.metadata.name)
    ) {
      this.message.error(this.translate.get('resource_name_already_existed'));
      return;
    }
    this.close.next(this.ingress);
  }

  cancel() {
    this.close.next();
  }
}
