let index = 1;

export interface WorkloadStatus {
  desired?: number;
  current?: number;
  status?: string;
}

export class Node implements d3.SimulationNodeDatum {
  // optional - defining optional implementation properties - required for relevant typing assistance
  index?: number;
  linkCount = 0;

  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  uuid: string;
  name: string;
  kind: string;
  isRef?: boolean;

  // workload status
  status?: string;
  current?: number;
  desired?: number;
  containers?: Array<{
    image?: string;
    cpu?: string;
    memory?: string;
  }>;

  constructor(params: {
    uuid?: string;
    name?: string;
    kind?: string;
    isRef?: boolean;
  }) {
    this.index = index++;
    this.uuid = params.uuid;
    this.name = params.name;
    this.kind = params.kind;
    this.isRef = params.isRef;
  }

  updateStatus(status: WorkloadStatus) {
    this.status = status.status;
    this.current = status.current;
    this.desired = status.desired;
  }

  get r() {
    return 20;
  }
}
