import { KubernetesResource, noop } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  ViewChild,
} from '@angular/core';
import { get, remove } from 'lodash-es';
import { finalize } from 'rxjs/operators';

import { PodControllerKinds } from 'app/features-shared/app/utils';
import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  ApplicationStatus,
  Container,
  TopologyEdge,
  TopologyResponse,
} from 'app/typings';

import { getApplicationFromResources } from '../util';

import { D3Service } from './d3.service';
import { Link, Node } from './models';
import { GraphComponent } from './visuals/graph/component';

@Component({
  selector: 'rc-app-topology',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./style.scss'],
})
export class AppTopologyComponent implements OnChanges {
  @ViewChild('topology', { static: true })
  graphComponent: GraphComponent;

  @Input()
  cluster: string;

  @Input()
  app: KubernetesResource[];

  nodes: Node[] = [];
  links: Link[] = [];
  loading: boolean;
  initialized: boolean;

  constructor(
    public d3Service: D3Service,
    private readonly acpApi: AcpApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnChanges() {
    if (this.cluster && this.app && !this.initialized && !this.loading) {
      this.refresh();
    }
  }

  refresh() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    const application = getApplicationFromResources(this.app);
    this.acpApi
      .getTopologyByResource({
        cluster: this.cluster,
        namespace: application.metadata.namespace,
        name: application.metadata.name,
        kind: 'Application',
      })
      .pipe(
        finalize(() => {
          this.loading = false;
          this.initialized = true;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(topology => {
        this.initNodesAndLinks(this.app, topology);
        if (application.status) {
          this.updateNodesStatus(application.status);
        }
      }, noop);
  }

  initNodesAndLinks(app: KubernetesResource[], topology: TopologyResponse) {
    this.nodes = this.getNodesFromResourcesAndTopology(app, topology.nodes);
    this.links = this.getLinksFromEdges(topology.edges, topology.nodes);
  }

  // 暂时没有用到，实时更新Nodes和Links
  updateNodesAndLinks(app: KubernetesResource[], topology: TopologyResponse) {
    const nodes = this.getNodesFromResourcesAndTopology(app, topology.nodes);
    remove(
      this.nodes,
      node => !Object.keys(topology.nodes).find(uuid => uuid === node.uuid),
    );
    nodes.forEach(_node => {
      if (!this.nodes.find(node => node.uuid === _node.uuid)) {
        this.nodes.push(_node);
      }
    });
    const links = this.getLinksFromEdges(topology.edges, topology.nodes);
    remove(this.links, link =>
      links.find(
        newLink =>
          newLink.source !== link.source || newLink.target !== link.target,
      ),
    );
    links.forEach(newLink => {
      if (
        !this.links.find(
          link =>
            newLink.source === link.source && newLink.target === link.target,
        )
      ) {
        this.links.push(newLink);
      }
    });
    this.nodes = [...this.nodes];
    this.links = [...this.links];
  }

  updateNodesStatus(status: ApplicationStatus) {
    // API 可能返回 null
    const workloads = get(status, ['workloadsStatus', 'workloads']) || [];
    workloads.forEach(workload => {
      const node = this.nodes.find(
        n => n.kind === workload.kind.toLowerCase() && n.name === workload.name,
      );
      if (node) {
        node.updateStatus(workload);
      }
    });
  }

  private getNodesFromResourcesAndTopology(
    app: KubernetesResource[],
    nodes: {
      [key: string]: KubernetesResource;
    },
  ) {
    const resources = app.map(data => ({
      name: data.metadata.name,
      kind: data.kind.toLowerCase(),
      data,
    }));
    return Object.values(nodes).map(res => {
      const { name, uid: uuid } = res.metadata;
      const kind = res.kind.toLowerCase();
      let isRef: boolean;
      const resource = resources.find(r => r.name === name && r.kind === kind);
      if (kind !== 'application') {
        isRef = !resource;
      }
      const node = new Node({
        uuid,
        name,
        kind,
        isRef,
      });
      if (PodControllerKinds.includes(res.kind)) {
        node.containers = get(
          resource,
          'data.spec.template.spec.containers',
          [],
        ).map((container: Container) => {
          const image = container.image;
          const [, ...rest] = image.split('/');
          const cpu = get(container, 'resources.limits.cpu', '');
          const memory = get(container, 'resources.limits.memory', '');
          return {
            image: rest.join('/'),
            cpu,
            memory,
          };
        });
      }
      return node;
    });
  }

  private getLinksFromEdges(
    edges: TopologyEdge[],
    nodes: {
      [key: string]: KubernetesResource;
    },
  ): Link[] {
    return edges
      .filter(edge => edge.from !== edge.to)
      .map(edge => {
        const link = new Link(
          edge.from,
          edge.to,
          edge.type.toLowerCase() === 'reference',
        );
        const source = nodes[edge.from];
        const target = nodes[edge.to];
        if (
          source.kind.toLowerCase() === 'application' &&
          !PodControllerKinds.includes(target.kind)
        ) {
          // 1. 先找到 source是 application， target不是 Workload 的 edge
          // 2. 若还存在 (source是 Workload 而 target相同 的 edge) 或
          //  （target是 Workload 而 source相同 的 edge），则过滤掉(1)中这条 edge
          // TODO: 暂时显示所有关联关系，参考 http://jira.alaudatech.com/browse/ACP-527
          // const existingEdge = edges.find((e: TopologyEdge) => {
          //   const s = nodes[e.from];
          //   const t = nodes[e.to];
          //   if (
          //     (PodControllerKinds.includes(s.kind) && e.to === edge.to) ||
          //     (PodControllerKinds.includes(t.kind) && e.from === edge.to)
          //   ) {
          //     return true;
          //   }
          //   return false;
          // });
          // if (existingEdge) {
          //   link.invisible = true;
          // }
        }
        return link;
      });
  }
}
