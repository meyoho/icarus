import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';

import { Graph, Link, Node } from './models';

@Injectable()
export class D3Service {
  /** This service will provide methods to enable user interaction with elements
   * while maintaining the d3 simulations physics
   */

  /** A method to bind a pan and zoom behaviour to an svg element */
  applyZoomableBehaviour(
    svgElement: ElementRef<SVGElement>,
    containerElement: Element,
  ) {
    const container = d3.select(containerElement);
    d3.select(svgElement.nativeElement).call(
      d3
        .zoom()
        .scaleExtent([0.5, 2])
        .on('zoom', () => {
          const { transform } = d3.event;
          container.attr(
            'transform',
            'translate(' +
              transform.x +
              ',' +
              transform.y +
              ') scale(' +
              transform.k +
              ')',
          );
        }),
    );
  }

  applyDraggableBehaviour(element: Element, node: Node, graph: Graph) {
    const d3element = d3.select(element);

    function started() {
      d3.event.sourceEvent.stopPropagation();

      if (!d3.event.active) {
        graph.simulation.alphaTarget(0.3).restart();
      }

      d3.event.on('drag', dragged).on('end', ended);

      function dragged() {
        node.fx = d3.event.x;
        node.fy = d3.event.y;
      }

      function ended() {
        if (!d3.event.active) {
          graph.simulation.alphaTarget(0);
        }

        node.fx = null;
        node.fy = null;
      }
    }

    d3element.call(d3.drag().on('start', started));
  }

  getGraph(
    options: { width: number; height: number },
    nodes?: Node[],
    links?: Link[],
  ) {
    return new Graph(options, nodes, links);
  }
}
