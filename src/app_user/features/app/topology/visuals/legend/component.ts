import { Component } from '@angular/core';

@Component({
  selector: 'rc-topology-graph-legend',
  templateUrl: 'template.html',
  styleUrls: ['./style.scss'],
})
export class GraphLegendComponent {}
