import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from '@angular/core';

import { Node } from '../../models';

const STATUS_TYPE_MAP = {
  Running: 'success',
  Stopped: 'info',
  Pending: 'primary',
} as const;

@Component({
  // tslint:disable-next-line
  selector: 'g[rc-topology-node]',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class NodeVisualComponent {
  @Input('rc-topology-node')
  node: Node;

  @Output()
  nodeFocused: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  nodeBlured: EventEmitter<void> = new EventEmitter<void>();

  statusTypeMap = STATUS_TYPE_MAP;

  @HostListener('mouseenter')
  focus() {
    this.nodeFocused.next();
  }

  @HostListener('mouseleave')
  blur() {
    this.nodeBlured.next();
  }

  getWorkloadClass() {
    if (this.node.status) {
      return `node__workload node__workload--${
        this.statusTypeMap[this.node.status as keyof typeof STATUS_TYPE_MAP]
      }`;
    } else {
      return 'node__workload';
    }
  }

  get nodeType() {
    if (!this.node) {
      return;
    }
    if (!this.node.uuid) {
      return 0;
    } else {
      switch (this.node.kind) {
        case 'application':
          return 1;
        case 'deployment':
        case 'statefulset':
        case 'daemonset':
          return 2;
        default:
          return 3;
      }
    }
  }

  getWorkloadStatusText(node: Node) {
    return node.current !== undefined && node.desired !== undefined
      ? `${node.current}/${node.desired}`
      : '';
  }
}
