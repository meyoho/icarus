import { Component, Input } from '@angular/core';

import { Link, Node } from '../../models';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'g[rc-topology-arc-link]',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ArcLinkVisualComponent {
  @Input('rc-topology-arc-link')
  link: Link;

  getPath() {
    const target = this.link.target as Node;
    const source = this.link.source as Node;
    const dx = target.x - source.x;
    const dy = target.y - source.y;
    const dr = Math.sqrt(dx * dx + dy * dy);
    return (
      'M' +
      source.x +
      ',' +
      source.y +
      'A' +
      dr +
      ',' +
      dr +
      ' 0 0,1 ' +
      target.x +
      ',' +
      target.y
    );
  }
}
