import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { D3Service } from '../d3.service';

@Directive({
  selector: '[rcZoomableOf]',
})
export class ZoomableDirective implements OnInit {
  @Input('rcZoomableOf')
  zoomableOf: ElementRef<SVGElement>;

  constructor(
    private readonly topolopy: D3Service,
    private readonly elementRef: ElementRef,
  ) {}

  ngOnInit() {
    this.topolopy.applyZoomableBehaviour(
      this.zoomableOf,
      this.elementRef.nativeElement,
    );
  }
}
