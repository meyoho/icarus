import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { D3Service } from '../d3.service';
import { Graph, Node } from '../models';

@Directive({
  selector: '[rcDraggableNode]',
})
export class DraggableDirective implements OnInit {
  @Input('rcDraggableNode')
  draggableNode: Node;

  @Input('rcDraggableInGraph')
  draggableInGraph: Graph;

  constructor(
    private readonly topology: D3Service,
    private readonly elementRef: ElementRef,
  ) {}

  ngOnInit() {
    this.topology.applyDraggableBehaviour(
      this.elementRef.nativeElement,
      this.draggableNode,
      this.draggableInGraph,
    );
  }
}
