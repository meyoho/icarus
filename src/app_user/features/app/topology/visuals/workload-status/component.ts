import { Component, Input } from '@angular/core';

import { Node } from '../../models';

@Component({
  selector: 'rc-topology-workload-status',
  templateUrl: 'template.html',
  styleUrls: ['./style.scss'],
})
export class TopologyWorkloadStatusComponent {
  @Input()
  node: Node;
}
