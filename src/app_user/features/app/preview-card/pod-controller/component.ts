import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';

import { PodControllerKindEnum } from 'app/features-shared/app/utils';
import { Container, Deployment, Workload } from 'app/typings';

@Component({
  selector: 'rc-pod-controller-preview-card',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressPodControllerCardComponent {
  @Input()
  data: Workload;

  getContainerSize(container: Container) {
    return {
      cpu: get(container, 'resources.limits.cpu', '-'),
      memory: get(container, 'resources.limits.memory', '-'),
    };
  }

  getReplicas() {
    return (this.data as Deployment).spec.replicas;
  }

  shouldShowReplicas() {
    return this.data && this.data.kind !== PodControllerKindEnum.DaemonSet;
  }
}
