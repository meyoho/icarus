import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
} from '@angular/core';
import { get } from 'lodash-es';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, Service } from 'app/typings';
import { getServiceProtocolDisplay } from 'app_user/features/service/util';

@Component({
  selector: 'rc-service-preview-card',
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicePreviewCardComponent {
  @Input()
  service: Service;

  @Input() showKind = false;

  getServiceProtocolDisplay = getServiceProtocolDisplay;

  get isNodePort() {
    return this.service.spec.type === 'NodePort';
  }

  get width() {
    return this.isNodePort ? '20%' : '25%';
  }

  get podControllerName() {
    return get(
      this.service,
      ['spec', 'selector', `service.${this.env.LABEL_BASE_DOMAIN}/name`],
      '',
    );
  }

  constructor(@Inject(ENVIRONMENTS) private readonly env: Environments) {}
}
