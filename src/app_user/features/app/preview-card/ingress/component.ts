import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';

import { HTTPIngressPath, Ingress, IngressRule } from 'app/typings';

@Component({
  selector: 'rc-ingress-preview-card',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressPreviewCardComponent {
  @Input()
  ingress: Ingress;

  @Input() showKind = false;

  getRules(ingress: Ingress) {
    return get(ingress, ['spec', 'rules'], [] as IngressRule[]);
  }

  getPaths(rule: IngressRule) {
    return get(rule, ['http', 'paths'], [] as HTTPIngressPath[]);
  }
}
