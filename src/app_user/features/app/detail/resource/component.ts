import {
  KubernetesResource,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, Subject, combineLatest } from 'rxjs';
import { debounceTime, filter, map, startWith } from 'rxjs/operators';

import { WorkspaceBaseParams } from 'app/typings';

@Component({
  selector: 'rc-application-resources',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationResourcesComponent {
  @Input()
  resources: KubernetesResource[];

  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true, [])
  resources$: Observable<KubernetesResource[]>;

  private readonly search$ = new Subject<string>();

  filteredResources$: Observable<KubernetesResource[]>;

  constructor() {
    this.filteredResources$ = combineLatest([
      this.search$.pipe(debounceTime(200), startWith('')),
      this.resources$.pipe(filter(r => !!r)),
    ]).pipe(
      map(([key, resources]) => {
        return resources.filter(r =>
          r.metadata.name.toLowerCase().includes(key.trim()),
        );
      }),
      publishRef(),
    );
  }

  onSearchKeyChange(key: string) {
    this.search$.next(key);
  }

  trackByFn(_index: number, resource: KubernetesResource) {
    return `${resource.kind}-${resource.metadata.name}`;
  }
}
