import {
  KubernetesResource,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';
import { noop } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import {
  PodControllerKindEnum,
  PodControllerKinds,
} from 'app/features-shared/app/utils';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { FederationUtilService } from 'app/services/federation-util.service';
import { Application, Deployment, StatefulSet } from 'app/typings';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { ApplicationResourcesManageComponent } from 'app_user/features/app/detail/resources-manage/component';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { FederalizationDialogComponent } from '../federalization-dialog/component';

@Component({
  selector: 'rc-app-detail-header',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailHeaderComponent implements OnChanges {
  @Input()
  cluster: string;

  @Input()
  application: Application;

  @Input()
  resources: KubernetesResource[];

  @Input() permissions: {
    update: boolean;
    delete: boolean;
  };

  @Input() isFederated: boolean;

  @ObservableInput(true)
  application$: Observable<Application>;

  @ObservableInput(true)
  cluster$: Observable<string>;

  canStart: boolean;
  canStop: boolean;

  hasDaemonSet: boolean;

  couldFederalize$ = combineLatest([
    this.cluster$.pipe(filter(cluster => !!cluster)),
    this.application$.pipe(
      filter(app => !!app),
      map(app => app.metadata.namespace),
    ),
  ]).pipe(
    switchMap(([cluster, namespace]) =>
      this.fedUtil.isFedNamespace(cluster, namespace),
    ),
  );

  get allowUpdate() {
    return this.permissions && this.permissions.update;
  }

  get allowDelete() {
    return this.permissions && this.permissions.delete;
  }

  constructor(
    private readonly acpApi: AcpApiService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly appUtil: AppUtilService,
    private readonly detailDataService: DetailDataService,
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
    private readonly fedUtil: FederationUtilService,
  ) {}

  ngOnChanges({ resources }: SimpleChanges) {
    if (resources && resources.currentValue) {
      this.onInputChange();
    }
  }

  private onInputChange() {
    this.hasDaemonSet = this.isDaemonSetApplication(this.resources);

    const workloads = this.resources.filter(i =>
      [
        PodControllerKindEnum.Deployment,
        PodControllerKindEnum.StatefulSet,
      ].includes(i.kind as PodControllerKindEnum),
    );

    this.canStart = workloads.some(
      (i: Deployment | StatefulSet) => i.spec.replicas === 0,
    );
    this.canStop = workloads.some(
      (i: Deployment | StatefulSet) => i.spec.replicas !== 0,
    );

    this.cdr.markForCheck();
  }

  private isDaemonSetApplication(resources: KubernetesResource[]) {
    return resources
      .filter(i => PodControllerKinds.includes(i.kind))
      .every(i => i.kind === PodControllerKindEnum.DaemonSet);
  }

  update() {
    this.router.navigate(['app', 'update', this.application.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  manageResources() {
    const dialogRef = this.dialogService.open(
      ApplicationResourcesManageComponent,
      {
        data: {
          resources: this.resources,
          cluster: this.cluster,
          namespace: this.application.metadata.namespace,
          appName: this.application.metadata.name,
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.reload$.subscribe(() => {
      this.detailDataService.refreshPolling();
    });
  }

  async delete() {
    try {
      await this.appUtil.deleteApplication({
        cluster: this.cluster,
        resource: this.application,
      });
      this.jumpToListPage();
    } catch (error) {}
  }

  async start() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('start'),
        content: this.translateService.get('app_start_confirm', {
          app_name: this.application.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.acpApi
      .startApplication({
        cluster: this.cluster,
        resource: this.application,
      })
      .subscribe(() => {
        this.detailDataService.refreshPolling();
      }, noop);
  }

  async stop() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('stop'),
        content: this.translateService.get('app_stop_confirm', {
          app_name: this.application.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.acpApi
      .stopApplication({
        cluster: this.cluster,
        resource: this.application,
      })
      .subscribe(() => {
        this.detailDataService.refreshPolling();
      }, noop);
  }

  federalize() {
    this.dialogService
      .open(FederalizationDialogComponent, {
        data: {
          cluster: this.cluster,
          app: this.application,
          resources: this.resources,
        },
        size: DialogSize.Large,
      })
      .afterClosed()
      .subscribe(federalized => {
        if (federalized) {
          this.jumpToListPage();
        }
      });
  }

  private disableActions() {
    this.canStart = false;
    this.canStop = false;
  }

  private jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
