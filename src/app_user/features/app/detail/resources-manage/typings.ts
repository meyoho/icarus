import { ResourceType } from 'app/typings';

export interface ManageResourceType {
  kind: string;
  definition: ResourceType;
}
export enum SubmittingType {
  DELETING = 'deleting',
  IMPORTING = 'importing',
  NONE = 'none',
}
export enum ManageResourcesPageType {
  LIST = 'list',
  DETAIL = 'detail',
}
