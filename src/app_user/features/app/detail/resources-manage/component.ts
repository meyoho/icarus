import {
  K8sApiService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { safeDump } from 'js-yaml';
import { cloneDeep, groupBy } from 'lodash-es';
import { BehaviorSubject, Subject, combineLatest } from 'rxjs';
import { finalize, map, startWith, switchMap, takeUntil } from 'rxjs/operators';

import { RESOURCE_TYPES } from 'app/typings';
import { viewActions, yamlReadOptions } from 'app/utils';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import {
  ManageResourceType,
  ManageResourcesPageType,
  SubmittingType,
} from 'app_user/features/app/detail/resources-manage/typings';

// 列表中展示的优先级与本数组index相同
const MANAGE_RESOURCE_TYPES: ManageResourceType[] = [
  { kind: 'Deployment', definition: RESOURCE_TYPES.DEPLOYMENT },
  { kind: 'DaemonSet', definition: RESOURCE_TYPES.DAEMONSET },
  { kind: 'Job', definition: RESOURCE_TYPES.JOB },
  { kind: 'CronJob', definition: RESOURCE_TYPES.CRON_JOB },
  { kind: 'Service', definition: RESOURCE_TYPES.SERVICE },
  { kind: 'Ingress', definition: RESOURCE_TYPES.INGRESS },
  { kind: 'PersistentVolumeClaim', definition: RESOURCE_TYPES.PVC },
  { kind: 'ConfigMap', definition: RESOURCE_TYPES.CONFIG_MAP },
  { kind: 'Secret', definition: RESOURCE_TYPES.SECRET },
];
const DEFAULT_MANAGE_RESOURCE_TYPE = MANAGE_RESOURCE_TYPES[0];

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationResourcesManageComponent implements OnInit, OnDestroy {
  resources: KubernetesResource[];
  cluster: string;
  namespace: string;
  appName: string;

  groupSort = groupSort;
  resourceType = DEFAULT_MANAGE_RESOURCE_TYPE;
  resourceSelect: KubernetesResource = null;
  partialResourceList: KubernetesResource[];

  destroy$$ = new Subject<void>();

  // yaml options
  codeEditorOptions = yamlReadOptions;
  actionsConfigView = viewActions;

  submitting = SubmittingType.NONE;
  ManageResourcesPageType = ManageResourcesPageType;
  SubmittingType = SubmittingType;
  MANAGE_RESOURCE_TYPES = MANAGE_RESOURCE_TYPES;

  // 用于切换弹窗内视图类型
  pageType = ManageResourcesPageType.LIST;

  // 当前待查看资源
  resourceView: KubernetesResource = null;

  // 触发外部详情轮询与可选列表的更新
  reload$ = new Subject<void>();

  // 当前选中的资源类型，初始化为DaemonSet
  chosenResourceType$ = new BehaviorSubject(DEFAULT_MANAGE_RESOURCE_TYPE);

  // 跟随选中资源类型变化
  partialResourceList$ = combineLatest([
    this.chosenResourceType$,
    this.reload$.pipe(startWith(null as void)),
  ]).pipe(
    switchMap(([type]) =>
      this.k8sApi.getResourceList({
        type: type.definition,
        cluster: this.cluster,
        namespace: this.namespace,
      }),
    ),
    map(({ items }) => items.filter(item => !item.metadata.ownerReferences)),
    takeUntil(this.destroy$$),
  );

  ngOnInit() {
    this.partialResourceList$.subscribe(items => {
      this.partialResourceList = items;
    });
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  constructor(
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      resources: KubernetesResource[];
      cluster: string;
      namespace: string;
      appName: string;
    },
    private readonly k8sApi: K8sApiService,
    private readonly appUtil: AppUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    this.resources = this.modalData.resources;
    this.cluster = this.modalData.cluster;
    this.namespace = this.modalData.namespace;
    this.appName = this.modalData.appName;
    this.importResource = this.importResource.bind(this);
    this.removeResource = this.removeResource.bind(this);
  }

  onTypeChange(type: ManageResourceType) {
    this.resourceSelect = null;
    this.chosenResourceType$.next(type);
    this.cdr.markForCheck();
  }

  trackByFn(_index: number, resource: KubernetesResource) {
    return `${resource.kind}-${resource.metadata.name}`;
  }

  importResource() {
    this.submitting = SubmittingType.IMPORTING;
    // import resource
    this.appUtil
      .importResources({
        cluster: this.cluster,
        namespace: this.namespace,
        app: this.appName,
        components: [this.resourceSelect],
      })
      .pipe(
        finalize(() => {
          this.submitting = SubmittingType.NONE;
        }),
      )
      .subscribe(
        () => {
          this.message.success(this.translate.get('import_successfully'));
          this.resources = [...this.resources, this.resourceSelect];
          // clear select
          this.resourceSelect = null;
          // update outside
          this.reload$.next();
          this.cdr.markForCheck();
        },
        () => {
          this.message.error(this.translate.get('import_failed'));
        },
      );
  }

  viewYaml(resource: KubernetesResource) {
    this.pageType = ManageResourcesPageType.DETAIL;
    this.resourceView = resource;
  }

  removeResource(resource: KubernetesResource, closeDetail = false) {
    this.submitting = SubmittingType.DELETING;
    // export resource
    this.appUtil
      .exportResources({
        cluster: this.cluster,
        namespace: this.namespace,
        app: this.appName,
        components: [resource],
      })
      .pipe(
        finalize(() => {
          this.submitting = SubmittingType.NONE;
        }),
      )
      .subscribe(
        () => {
          this.message.success(this.translate.get('export_successfully'));
          // update outside
          this.reload$.next();
          this.resources = this.resources.filter(
            resourceItem => resourceItem !== resource,
          );
          this.cdr.markForCheck();
          if (closeDetail) {
            this.closeYaml();
          }
        },
        () => {
          this.message.error(this.translate.get('export_failed'));
        },
      );
  }

  formatResourceToYaml(resource: KubernetesResource) {
    try {
      const resourceBak = cloneDeep(resource);
      return safeDump(resourceBak);
    } catch (err) {}
  }

  closeYaml() {
    this.pageType = ManageResourcesPageType.LIST;
    this.resourceView = null;
  }
}

// 分组排序，根据资源类型进行分组，并按照MANAGE_RESOURCE_TYPES定义的顺序进行排序
export function groupSort(resources: KubernetesResource[]) {
  const miscObj = groupBy(resources, 'kind');
  const flatResources: KubernetesResource[] = [];
  for (let k = 0, len = MANAGE_RESOURCE_TYPES.length; k < len; k++) {
    const currType = MANAGE_RESOURCE_TYPES[k];
    if (Array.isArray(miscObj[currType.kind])) {
      flatResources.push(...miscObj[currType.kind]);
    }
  }
  return flatResources;
}
