import {
  ANNOTATIONS,
  K8sApiService,
  METADATA,
  NAME,
  ObservableInput,
  TranslateService,
  matchLabelsToString,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { state, style, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  pluck,
  switchMap,
  tap,
} from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  Application,
  ApplicationHistory,
  ApplicationHistoryDiff,
  RESOURCE_TYPES,
} from 'app/typings';
import { VersionSnapShotComponent } from 'app_user/features/app/detail/version-snapshot/component';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AppUtilService } from '../../app-util.service';
import { DetailDataService } from '../../detail-data.service';

enum DiffType {
  ROLLBACK_FROM = 'rollbackFrom',
  HAS_DIFF = 'hasDiff',
  NO_DIFF = 'noDiff',
}

@Component({
  selector: 'rc-application-history',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('display', [
      state('show', style({ display: 'block' })),
      state('hide', style({ display: 'none' })),
    ]),
  ],
})
export class ApplicationHistoryComponent {
  @Input()
  cluster: string;

  @Input()
  application: Application;

  @Input() isFederated: boolean;

  @Input()
  canUpdateApp: boolean;

  @ObservableInput(true)
  application$: Observable<Application>;

  latestRevision: string;
  DiffType = DiffType;
  rowExpanded: Record<number, boolean> = {};

  appHistory$: Observable<ApplicationHistory[]>;
  constructor(
    private readonly router: Router,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly translateService: TranslateService,
    private readonly appUtilService: AppUtilService,
    private readonly detailDataService: DetailDataService,
    private readonly k8sApi: K8sApiService,
    private readonly utilService: K8sUtilService,
    private readonly dialogService: DialogService,
  ) {
    const revisionKey = this.utilService.normalizeType('revision', 'app');
    this.appHistory$ = this.application$.pipe(
      filter(r => !!r),
      distinctUntilChanged((a, b) => {
        return (
          a.metadata.name === b.metadata.name &&
          get(a, [METADATA, ANNOTATIONS, revisionKey]) ===
            get(b, [METADATA, ANNOTATIONS, revisionKey])
        );
      }),
      tap(application => {
        this.latestRevision = get(application, [
          METADATA,
          ANNOTATIONS,
          revisionKey,
        ]);
      }),
      switchMap(application =>
        this.fetchHistory({
          namespace: application.metadata.namespace,
          app: application.metadata.name,
        }),
      ),
      publishRef(),
    );
  }

  view(item: ApplicationHistory) {
    this.router.navigate(
      [
        'app',
        'version',
        this.application.metadata.name,
        item.metadata.name,
        this.latestRevision,
      ],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  create() {
    const dialogRef = this.dialogService.open(VersionSnapShotComponent, {
      data: {
        cluster: this.cluster,
        namespace: this.application.metadata.namespace,
        appName: this.application.metadata.name,
      },
    });
    dialogRef.componentInstance.confirmed.subscribe(confirmed => {
      dialogRef.close();
      if (confirmed) {
        this.detailDataService.refreshPolling();
      }
    });
  }

  getRollbackFrom(item: ApplicationHistory): string {
    return item.metadata.annotations
      ? item.metadata.annotations[
          this.utilService.normalizeType('rollback-from', 'app')
        ]
      : null;
  }

  getDiffType(item: ApplicationHistory): DiffType {
    return this.getRollbackFrom(item)
      ? DiffType.ROLLBACK_FROM
      : item.spec.resourceDiffs
      ? DiffType.HAS_DIFF
      : DiffType.NO_DIFF;
  }

  getDiffs(diffData: ApplicationHistoryDiff) {
    return Object.keys(diffData).reduce(
      (acc, cur) =>
        acc.concat(
          diffData[cur as keyof ApplicationHistoryDiff].map(
            (i: { kind?: string; name?: string }) =>
              this.translateService.get('application_diff_tip', {
                action: this.translateService.get(cur),
                kind: i.kind,
                name: i.name,
              }),
          ),
        ),
      [],
    );
  }

  rollback(revision: number) {
    this.appUtilService
      .rollback({
        cluster: this.cluster,
        namespace: this.application.metadata.namespace,
        revision,
        app: this.application.metadata.name,
      })
      .then(() => {
        this.detailDataService.refreshPolling();
      })
      .catch(noop);
  }

  toggleRow(index: number) {
    this.rowExpanded = {
      ...this.rowExpanded,
      [index]: !this.rowExpanded[index],
    };
  }

  private fetchHistory(params: {
    namespace: string;
    app: string;
  }): Observable<ApplicationHistory[]> {
    return this.k8sApi
      .getResourceList<ApplicationHistory>({
        type: RESOURCE_TYPES.APPLICATION_HISTORY,
        cluster: this.cluster,
        namespace: this.application.metadata.namespace,
        queryParams: {
          labelSelector: matchLabelsToString({
            [this.utilService.normalizeType(
              NAME,
              'app',
            )]: `${params.app}.${params.namespace}`,
          }),
        },
      })
      .pipe(
        pluck('items'),
        map(results =>
          [...results].sort((a, b) => b.spec.revision - a.spec.revision),
        ),
        catchError(() => of([] as ApplicationHistory[])),
      );
  }
}
