import { KubernetesResource } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { AcpApiService } from 'app/services/api/acp-api.service';
import { Application } from 'app/typings';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FederalizationDialogComponent {
  cluster: string;
  app: Application;
  resources: KubernetesResource[];

  loading = false;

  constructor(
    @Inject(DIALOG_DATA)
    data: {
      cluster: string;
      app: Application;
      resources: KubernetesResource[];
    },
    private readonly dialogRef: DialogRef,
    private readonly acpApi: AcpApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly sanitizer: DomSanitizer,
  ) {
    this.cluster = data.cluster;
    this.app = data.app;
    this.resources = data.resources;
  }

  federalize() {
    this.loading = true;
    this.acpApi
      .federalizeApplication({
        cluster: this.cluster,
        namespace: this.app.metadata.namespace,
        name: this.app.metadata.name,
      })
      .subscribe(
        () => {
          this.dialogRef.close(true);
          this.loading = false;
          this.cdr.markForCheck();
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }

  safeHTML = (value: string) => {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  };
}
