import {
  K8sApiService,
  KubernetesResource,
  ObservableInput,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { find, get } from 'lodash-es';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  delay,
  first,
  map,
  pluck,
  startWith,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { getResourceRequirementsAsLabels } from 'app/features-shared/app/utils';
import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import { getPodShortName } from 'app/features-shared/pod/util';
import { parseToWorkloadStatus } from 'app/features-shared/workload/util';
import { TerminalService } from 'app/services/api/terminal.service';
import {
  ApplicationStatus,
  Container,
  Pod,
  RESOURCE_TYPES,
  ResourceType,
  Workload,
  WorkspaceDetailParams,
} from 'app/typings';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { ResourceKind } from 'app_user/features/app/detail/main/component';
import { getContainerResourceLimit } from 'app_user/features/pod/util';

@Component({
  selector: 'rc-application-workloads',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationWorkloadsComponent implements OnInit, OnDestroy {
  @Input()
  kind: ResourceKind;

  @Input()
  resources: Workload[];

  @Input()
  detailParams: WorkspaceDetailParams;

  @Input() appStatus: ApplicationStatus;

  // 一般情况下，有 application 的 update 权限，应该同时有 workload 的 update 权限，这里不做严格区分
  @Input() permissions: {
    update: boolean;
  };

  @ObservableInput(true, [])
  resources$: Observable<Workload[]>;

  private readonly search$ = new Subject<string>();
  private readonly updateReplicas$ = new Subject<{
    replicas: number;
    resource: Workload;
  }>();

  private readonly onDestroy$ = new Subject<void>();

  filteredResources$: Observable<Workload[]>;

  pods: Pod[] = [];
  podsLoading: boolean;
  podsLoaded: boolean;

  getResourceRequirementsAsLabels = getResourceRequirementsAsLabels;
  getContainerResourceLimit = getContainerResourceLimit;
  parseToWorkloadStatus = parseToWorkloadStatus;

  getPodShortName = getPodShortName;

  execPermissions$: Observable<Record<string, boolean>>;

  constructor(
    public translate: TranslateService,
    private readonly terminal: TerminalService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly appUtilService: AppUtilService,
    private readonly detailDataService: DetailDataService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {
    this.filteredResources$ = combineLatest([
      this.search$.pipe(debounceTime(200), startWith('')),
      this.resources$,
    ]).pipe(
      map(([key, resources]) => {
        return resources.filter(r =>
          r.metadata.name.toLowerCase().includes(key.trim()),
        );
      }),
    );
  }

  get isUpdateAllowed() {
    return this.permissions && this.permissions.update;
  }

  ngOnInit() {
    this.updateReplicas$
      .pipe(
        switchMap(({ replicas, resource }) =>
          of({ replicas, resource }).pipe(delay(500)),
        ),
        switchMap(({ replicas, resource }) => {
          return this.k8sApi.patchResource({
            type:
              resource.kind === 'Deployment'
                ? RESOURCE_TYPES.DEPLOYMENT
                : RESOURCE_TYPES.STATEFULSET,
            cluster: this.detailParams.cluster,
            resource,
            part: { spec: { replicas } },
          });
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => this.detailDataService.refreshPolling());

    this.execPermissions$ = this.terminal.getPermissions({
      cluster: this.detailParams.cluster,
      namespace: this.detailParams.namespace,
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next(null);
  }

  onSearchKeyChange(key: string) {
    this.search$.next(key);
  }

  onMenuShow(resource: Workload) {
    this.getPods(resource);
  }

  onMenuHide() {
    this.podsLoaded = false;
  }

  trackByFn(_index: number, resource: KubernetesResource) {
    return `${resource.kind}-${resource.metadata.name}`;
  }

  trackByContainerFn(_index: number) {
    return _index;
  }

  getWorkloadContainers(c: Workload): Container[] {
    return get(c, ['spec', 'template', 'spec', 'containers'], []);
  }

  canEXEC(pod: Pod, container: Container): boolean {
    const podStatus = get(pod, 'status.containerStatuses', null);
    if (!podStatus) {
      return false;
    }
    return !!get(
      find(podStatus, co => co.name === container.name),
      'state.running',
    );
  }

  canNotEXECAll(pods: Pod[], container: Container): boolean {
    return pods.every(pod => !this.canEXEC(pod, container));
  }

  openTerminal(
    pods: Pod[],
    container: Container,
    resource: Workload,
    selected?: Pod,
  ) {
    const dialogRef = this.dialogService.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.detailParams.cluster,
        namespace: this.detailParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (data) {
        const podNames = pods
          // eslint-disable-next-line sonarjs/no-duplicate-string
          .map(pod => get(pod, 'metadata.name', ''))
          .join(';');
        const selectedName = get(selected, 'metadata.name', '');
        const containerName = container.name;
        this.terminal.openTerminal({
          app: this.detailParams.name,
          ctl: resource.metadata.name,
          pods: podNames,
          selectedPod: selectedName || '',
          container: containerName,
          namespace: this.detailParams.namespace,
          cluster: this.detailParams.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  openLog(pod: Pod, container: Container) {
    const podName = get(pod, 'metadata.name', '');
    const containerName = container.name;

    return this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 5,
        pod: podName,
        container: containerName,
      },
    });
  }

  getPodControllerUrl(c: Workload, params: WorkspaceDetailParams) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { name, ...baseParams } = params;
    return [
      '/workspace',
      baseParams,
      c.kind.toLowerCase(),
      'detail',
      c.metadata.name,
    ];
  }

  updateReplicas(replicas: number, resource: KubernetesResource) {
    this.updateReplicas$.next({
      replicas,
      resource,
    });
  }

  updateImageTag(containerName: string, resource: KubernetesResource) {
    this.appUtilService
      .updateImageTag({
        podController: resource,
        containerName,
        baseParams: {
          cluster: this.detailParams.cluster,
          namespace: this.detailParams.namespace,
          project: this.detailParams.project,
        },
      })
      .subscribe(res => {
        if (res) {
          this.message.success(this.translate.get('update_success'));
          this.detailDataService.refreshPolling();
        }
      });
  }

  updateResourceSize(containerName: string, resource: KubernetesResource) {
    this.appUtilService
      .updateResourceSize({
        podController: resource,
        containerName,
        baseParams: {
          cluster: this.detailParams.cluster,
          namespace: this.detailParams.namespace,
          project: this.detailParams.project,
        },
      })
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(res => {
        if (res) {
          this.message.success(this.translate.get('update_success'));
          this.detailDataService.refreshPolling();
        }
      });
  }

  private getPods(resource: Workload) {
    const labelSelector = matchLabelsToString(
      get(resource, 'spec.selector.matchLabels', {}),
    );
    this.podsLoading = true;
    this.k8sApi
      .getResourceList<Pod>({
        type: RESOURCE_TYPES.POD,
        cluster: this.detailParams.cluster,
        namespace: this.detailParams.namespace,
        queryParams: {
          labelSelector,
        },
      })
      .pipe(
        pluck('items'),
        catchError(() => of([] as Pod[])),
      )
      .subscribe(pods => {
        this.pods = pods;
        this.podsLoaded = true;
        this.podsLoading = false;
        this.cdr.markForCheck();
      });
  }
}
