import { KubernetesResource } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { get } from 'lodash-es';

@Component({
  selector: 'rc-application-event',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailEventComponent implements OnChanges {
  @Input()
  appData: KubernetesResource[];

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  names: string;

  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      const names = new Set(this.appData.map(el => get(el, 'metadata.name')));
      this.names = [...names].join(',');
      this.empty = !this.appData.length;
      this.initialized = true;
    }
  }
}
