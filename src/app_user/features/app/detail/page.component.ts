import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  isAllowed,
  publishRef,
} from '@alauda/common-snippet';
import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import equals from 'ramda/es/equals';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  Application,
  ClusterFeature,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/typings';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { getApplicationFromResources } from '../util';

@Component({
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [DetailDataService],
})
export class ApplicationDetailPageComponent implements OnDestroy {
  constructor(
    private readonly router: Router,
    private readonly acpApi: AcpApiService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly detailDataService: DetailDataService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.detailDataService.startPolling();
    this.detailDataService.polling$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.dataManager.reload();
      });
  }

  private readonly onDestroy$ = new Subject<void>();

  baseParams = this.workspaceComponent.baseParamsSnapshot;
  queryParams$ = this.activatedRoute.queryParams;
  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get('app'))),
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{ name: string; cluster: string; namespace: string }>(
      equals,
    ),
    publishRef(),
  );

  selectedTabIndex$: Observable<number> = this.queryParams$.pipe(
    map(queryParams => parseInt(queryParams.tabIndex, 10) || 0),
    startWith(0),
  );

  showMetricTab$ = this.k8sApi
    .getResourceList<ClusterFeature>({
      type: RESOURCE_TYPES.FEATURE,
      cluster: this.baseParams.cluster,
      queryParams: {
        labelSelector: 'instanceType=prometheus',
      },
    })
    .pipe(
      map(features => {
        return !!features.items.length;
      }),
      startWith(true),
      catchError(() => of(false)),
      publishRef(),
    );

  logInitialState$ = this.queryParams$.pipe(
    map(queryParams => ({
      pod: queryParams.pod,
      container: queryParams.container,
    })),
  );

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.APPLICATION,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
      }),
    ),
    isAllowed(),
  );

  dataManager = new AsyncDataLoader<{
    resource: KubernetesResource[];
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.acpApi.getApplicationYaml({
          cluster: params.cluster,
          namespace: params.namespace,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  getApplication = getApplicationFromResources;
  isFederated = (application: Application) => {
    return this.k8sUtil.isFederatedResource(application);
  };

  viewLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 5,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  onTabIndexChange(index: number) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: index,
      },
      queryParamsHandling: 'merge',
    });
    if (index !== 0) {
      this.detailDataService.suspendPolling();
    } else {
      this.detailDataService.resumePolling();
    }
  }

  back() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  ngOnDestroy() {
    this.detailDataService.stopPolling();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getApplicationStatus(resources: KubernetesResource[]) {
    const application = getApplicationFromResources(resources);
    return application.status;
  }

  getApplicationName(resources: KubernetesResource[]) {
    const application = getApplicationFromResources(resources);
    return application.metadata.name;
  }

  getResources(resources: KubernetesResource[]) {
    return resources
      .filter(r => r.kind !== 'Application')
      .sort((r1, r2) => (r1.metadata.name > r2.metadata.name ? 1 : -1));
  }

  getPodSelector(resources: KubernetesResource[]) {
    const application = getApplicationFromResources(resources);
    return get(application, ['spec', 'selector', 'matchLabels'], {});
  }
}
