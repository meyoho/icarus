import {
  KubernetesResource,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { groupBy, uniq } from 'lodash-es';
import { Observable } from 'rxjs';
import { filter, map, startWith, switchMap } from 'rxjs/operators';

import { PodControllerKinds } from 'app/features-shared/app/utils';
import { Application, Workload, WorkspaceBaseParams } from 'app/typings';

import { AppUtilService } from '../../app-util.service';

export enum ResourceKind {
  Application = 'Application',
  Deployment = 'Deployment',
  Daemonset = 'Daemonset',
  Statefulset = 'Statefulset',
  Other = 'Other',
}

@Component({
  selector: 'rc-app-detail-main',
  styles: [
    `
      aui-card {
        margin-bottom: 16px;
      }
    `,
  ],
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailComponent {
  @Input() resources: KubernetesResource[];
  @Input()
  application: Application;

  @ObservableInput(true)
  application$: Observable<Application>;

  @Input() baseParams: WorkspaceBaseParams;
  @Input() permissions: {
    update: boolean;
    delete: boolean;
  };

  @Input()
  isFederated: boolean;

  @ObservableInput(true)
  resources$: Observable<KubernetesResource[]>;

  addresses$ = this.application$.pipe(
    filter(r => !!r),
    switchMap(r =>
      this.appUtil
        .getApplicationAddress({
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          app: r.metadata.name,
        })
        .pipe(
          map(res => {
            const addresses = Object.keys(res).reduce<string[]>(
              (arr, key) => arr.concat(res[key]),
              [],
            );
            return uniq(addresses);
          }),
        ),
    ),
    startWith([]),
    publishRef(),
  );

  groupedWorkloads$: Observable<Record<string, Workload[]>>;
  commonResources$: Observable<KubernetesResource[]>;

  allResources: Array<{ kind: ResourceKind; data: KubernetesResource[] }>;

  constructor(private readonly appUtil: AppUtilService) {
    this.groupedWorkloads$ = this.resources$.pipe(
      filter(r => !!r),
      map(resources =>
        groupBy(
          resources.filter(r => PodControllerKinds.includes(r.kind)),
          r => r.kind,
        ),
      ),
    );

    this.commonResources$ = this.resources$.pipe(
      filter(r => !!r),
      map(resources =>
        resources
          .filter(r => !PodControllerKinds.includes(r.kind))
          .sort((a, b) => (a.kind > b.kind ? 1 : -1)),
      ),
    );
  }

  trackByFn(_index: number, resource: { key: string }) {
    return resource.key;
  }
}
