import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AcpApiService } from 'app/services/api/acp-api.service';

interface SnapshotModel {
  comment: string;
}
@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VersionSnapShotComponent {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  model: SnapshotModel = {
    comment: '',
  };

  cluster: string;
  namespace: string;
  appName: string;
  confirmed = new Subject<boolean>();
  confirming = false;
  constructor(
    @Inject(DIALOG_DATA)
    modelData: {
      cluster: string;
      namespace: string;
      appName: string;
    },
    private readonly acpApi: AcpApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    this.cluster = modelData.cluster;
    this.namespace = modelData.namespace;
    this.appName = modelData.appName;
  }

  confirm() {
    this.form.onSubmit(null);
    this.confirming = true;
    this.acpApi
      .createSnapshot({
        app: this.appName,
        cluster: this.cluster,
        namespace: this.namespace,
        cause: this.model.comment,
      })
      .pipe(
        finalize(() => {
          this.confirming = false;
        }),
      )
      .subscribe(
        () => {
          this.message.success(this.translate.get('create_success'));
          this.confirmed.next(true);
        },
        () => {
          this.message.error(this.translate.get('create_failed'));
          this.confirmed.next(false);
        },
      );
  }

  cancel() {
    this.confirmed.next(false);
  }
}
