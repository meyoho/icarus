import {
  KubernetesResource,
  NAME,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import Highcharts, { SeriesLineOptions } from 'highcharts';
import { get } from 'lodash-es';
import { v4 } from 'uuid';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  QueryMetric,
  QueryMetrics,
  TIME_STAMP_OPTIONS,
  getMetricNumericOptions,
  getMetricPercentOptions,
  parseMetricsResponse,
} from 'app/features-shared/app/utils';
import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

@Component({
  selector: 'rc-application-monitor-chart',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMonitorChartComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  appName: string;

  @Input()
  resources: KubernetesResource[];

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  queryElements: any[];
  clusterName: string;
  namespaceName: string;
  private intervalTimer: number;

  aggregators = AGGREGATORS;
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });

  selectedAggregator = this.aggregators[0].key;
  Highcharts = Highcharts;
  cpuChartOptions: Highcharts.Options = getMetricPercentOptions();
  memChartOptions: Highcharts.Options = getMetricPercentOptions();
  sentBytesChartOptions: Highcharts.Options = getMetricNumericOptions();
  receivedBytesChartOptions: Highcharts.Options = getMetricNumericOptions();
  queryMetrics = QueryMetrics;
  queryIdMap = new Map<string, QueryMetric>();

  constructor(
    private readonly cdr: ChangeDetectorRef,
    auiMessageService: MessageService,
    translateService: TranslateService,
    private readonly metricService: MetricService,
  ) {
    super(auiMessageService, translateService);
  }

  ngOnInit() {
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    this.sentBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.receivedBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.clusterName = this.cluster;
    this.namespaceName = this.namespace;
    this.queryElements = this.resources.map(el => ({
      componentName: get(el, 'metadata.name'),
      componentType: get(el, 'kind'),
    }));
    this.resetTimeRange();
    this.loadCharts();
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.chartLoading = true;
          this.cdr.markForCheck();
          this.loadCharts();
        }
      }
    }, 60000);
  }

  loadCharts() {
    const args = {
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 1800) {
      this.step = 60;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.endTime = args.end;
    const params: MetricQuery = {
      start,
      end: args.end,
      step: this.step,
    };
    params.queries = this.queryElements.flatMap(el =>
      this.queryMetrics.map(query => {
        const uuid = v4();
        this.queryIdMap.set(uuid, {
          ...query,
          componentName: el.componentName,
        });
        return {
          aggregator: args.aggregator,
          id: uuid,
          labels: [
            {
              type: 'EQUAL',
              name: '__name__',
              value: `workload.${query.metric}`,
            },
            {
              type: 'EQUAL',
              name: NAMESPACE,
              value: this.namespaceName,
            },
            {
              type: 'EQUAL',
              name: 'kind',
              value: el.componentType,
            },
            {
              type: 'EQUAL',
              name: NAME,
              value: el.componentName,
            },
          ],
        } as MetricQueries;
      }),
    );

    this.metricService
      .queryMetrics(this.clusterName, params)
      .then((results: Metric[]) => {
        const chartData = new Map<string, Metric[]>([
          ['cpu.utilization', []],
          ['memory.utilization', []],
          ['network.transmit_bytes', []],
          ['network.receive_bytes', []],
        ]);
        results.forEach(result => {
          const { metric } = this.queryIdMap.get(result.metric.__query_id__);
          chartData.set(metric, [result, ...chartData.get(metric)]);
        });
        chartData.forEach(values => {
          if (values.length > 0) {
            const { series, unit } = this.queryIdMap.get(
              values[0].metric.__query_id__,
            );
            values.sort((a, b) => {
              const mA = this.queryIdMap.get(a.metric.__query_id__);
              const mB = this.queryIdMap.get(b.metric.__query_id__);
              return mA.componentName.localeCompare(mB.componentName);
            });
            if (unit) {
              this[series].series = parseMetricsResponse(
                values,
                this.getName.bind(this),
                this.endTime,
                this.step,
                100,
              ) as SeriesLineOptions[];
            } else {
              this[series].series = parseMetricsResponse(
                values,
                this.getName.bind(this),
                this.endTime,
                this.step,
              ) as SeriesLineOptions[];
            }
          }
        });
      })
      .catch(error => {
        this.cpuChartOptions.series = [];
        this.memChartOptions.series = [];
        this.sentBytesChartOptions.series = [];
        this.receivedBytesChartOptions.series = [];
        throw error;
      })
      .finally(() => {
        this.chartLoading = false;
        this.cdr.markForCheck();
      });
  }

  private getName(el: Metric) {
    const { componentName } = this.queryIdMap.get(el.metric.__query_id__);
    return componentName;
  }
}
