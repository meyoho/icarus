import { KubernetesResource } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { PodControllerKinds } from 'app/features-shared/app/utils';

@Component({
  selector: 'rc-application-monitor',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMonitorComponent implements OnChanges {
  @Input()
  resources: KubernetesResource[];

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  initialized = false;
  empty = false;

  applications: KubernetesResource[];

  ngOnChanges({ resources }: SimpleChanges) {
    if (resources && resources.currentValue) {
      this.resources = this.resources.filter(el =>
        PodControllerKinds.includes(el.kind),
      );
      this.empty = !this.resources.length;
      this.initialized = true;
    }
  }
}
