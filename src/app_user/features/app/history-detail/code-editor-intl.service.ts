import { CodeEditorIntl } from '@alauda/code-editor';
import { TranslateService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

@Injectable()
export class HistoryDetailCodeEditorIntlService extends CodeEditorIntl {
  constructor(private readonly translate: TranslateService) {
    super();
    this.translate.locale$.subscribe(() => {
      this.setLabels();
    });
  }

  setLabels() {
    this.copyLabel = this.translate.get('copy');
    this.copiedLabel = this.translate.get('copy_clipboard_success');
    this.readonlyLabel = this.translate.get('code_editor_readonly');
    this.readwriteLabel = this.translate.get('code_editor_writable');
    this.clearLabel = this.translate.get('code_editor_clear');
    this.recoverLabel = this.translate.get('code_editor_recover');
    this.findLabel = this.translate.get('code_editor_find');
    this.formatLabel = this.translate.get('code_editor_format');
    this.foldLabel = this.translate.get('code_editor_fold');
    this.exportLabel = this.translate.get('code_editor_export');
    this.importLabel = this.translate.get('code_editor_import');
    this.lightThemeLabel = this.translate.get('code_editor_light_theme');
    this.darkThemeLabel = this.translate.get('code_editor_dark_theme');
    this.showDiffLabel = this.translate.get(
      'application_code_editor_show_diff_label',
    );
    this.fullscreenLabel = this.translate.get('code_editor_fullscreen');
    this.exitFullscreenLabel = this.translate.get(
      'code_editor_exit_fullscreen',
    );
  }
}
