import { CodeEditorIntl } from '@alauda/code-editor';
import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  noop,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { ApplicationHistory, RESOURCE_TYPES } from 'app/typings';
import { updateActions, yamlReadOptions } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AppUtilService } from '../app-util.service';

import { HistoryDetailCodeEditorIntlService } from './code-editor-intl.service';

@Component({
  selector: 'rc-application-history-detail',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CodeEditorIntl,
      useClass: HistoryDetailCodeEditorIntlService,
    },
  ],
})
export class ApplicationHistoryDetailComponent implements OnInit {
  revision: number;

  codeEditorOptions = yamlReadOptions;
  codeEditorUpdateActions = updateActions;

  history: ApplicationHistory;
  canUpdate: boolean;
  latestYaml = '';
  yaml = '';

  isCurrentVersion: boolean;
  initialized: boolean;

  baseParams = this.workspaceComponent.baseParamsSnapshot;
  appName = this.activatedRoute.snapshot.params.app;
  versionName = this.activatedRoute.snapshot.params.name;
  latestRevision = +this.activatedRoute.snapshot.params.latestRevision;

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.APPLICATION,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: [K8sResourceAction.UPDATE],
  });

  constructor(
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly activatedRoute: ActivatedRoute,
    private readonly appUtilService: AppUtilService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService,
  ) {}

  ngOnInit() {
    this.fetchDetail();
  }

  private fetchDetail() {
    this.k8sApi
      .getResource<ApplicationHistory>({
        type: RESOURCE_TYPES.APPLICATION_HISTORY,
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        name: this.versionName,
      })
      .pipe(
        finalize(() => {
          this.initialized = true;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(res => {
        this.history = res;
        this.revision = this.history.spec.revision;
        this.isCurrentVersion = this.latestRevision === this.revision;
        if (!this.isCurrentVersion) {
          this.fetchLatestYaml();
        }
        this.codeEditorUpdateActions = Object.assign(
          {},
          this.codeEditorUpdateActions,
          {
            diffMode: !this.isCurrentVersion,
          },
        );
        this.yaml = this.history.spec.yaml;
      }, noop);
  }

  private fetchLatestYaml() {
    this.k8sApi
      .getResource<ApplicationHistory>({
        type: RESOURCE_TYPES.APPLICATION_HISTORY,
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        name: this.appName + '-' + this.latestRevision,
      })
      .subscribe(res => {
        this.latestYaml = res.spec.yaml;
      }, noop);
  }

  rollback() {
    this.appUtilService
      .rollback({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        revision: this.revision,
        app: this.appName,
      })
      .then(() => {
        this.jumpToDetailPage();
      })
      .catch(noop);
  }

  jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  jumpToDetailPage() {
    this.router.navigate(['app', 'detail', this.appName], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  backToHistoryListPage() {
    this.router.navigate(['app', 'detail', this.appName], {
      queryParams: {
        tabIndex: 4,
      },
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
