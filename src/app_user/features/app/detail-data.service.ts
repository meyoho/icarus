import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, merge, timer } from 'rxjs';
import { switchMap, takeUntil, takeWhile } from 'rxjs/operators';

@Injectable()
export class DetailDataService {
  defaultPollingInterval = 15000;
  polling$: Observable<number>;

  private readonly stopPolling$ = new Subject<void>();
  private readonly refreshSub$ = new BehaviorSubject<null>(null);

  private suspended: boolean;

  startPolling(pollingInterval?: number) {
    const interval = pollingInterval || this.defaultPollingInterval;
    const timer$ = this.refreshSub$.pipe(
      switchMap(() =>
        timer(interval, interval).pipe(takeWhile(() => !this.suspended)),
      ),
      takeUntil(this.stopPolling$),
    );

    this.polling$ = merge(this.refreshSub$, timer$).pipe(
      takeUntil(this.stopPolling$),
    );
    this.refreshSub$.next(null);
  }

  stopPolling() {
    this.stopPolling$.next();
  }

  refreshPolling() {
    this.refreshSub$.next(null);
  }

  suspendPolling() {
    this.suspended = true;
  }

  resumePolling() {
    this.suspended = false;
    this.refreshPolling();
  }
}
