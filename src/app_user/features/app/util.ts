import {
  KubernetesResource,
  KubernetesResourceList,
  LABELS,
  METADATA,
  StringMap,
} from '@alauda/common-snippet';
import { safeLoadAll } from 'js-yaml';
import { get, set, uniq } from 'lodash-es';
import { dissocPath } from 'ramda';

import {
  PodControllerKindEnum,
  PodControllerRollingUpdateType,
  PodControllerUpdateStrategyType,
  getDefaultContainerConfig,
} from 'app/features-shared/app/utils';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import {
  Application,
  ApplicationFormModel,
  ApplicationTypeMeta,
  HTTPIngressPath,
  Ingress,
  IngressRule,
  RESOURCE_DEFINITIONS,
  ResourceType,
  SERVICE_PORT_PROTOCOLS,
  Service,
  TApp,
  Workload,
} from 'app/typings';
import { STATUS } from 'app/utils';

import { getDefaultService } from '../service/util';

// 规则: 非[a-z0-9-]的替换为_，多个_合并为1个
// 防止xx?_?xx的形式，分为2次替换
// 1. 非a-z/0-9/-替换为- 2. 多个连续的-合并为- 3.去除开头和结尾的-
export function adaptRepoNameToContainerName(repoName: string) {
  return repoName
    .replace(/[^\da-z-]/g, '-') // 非法输入转为-
    .replace(/-+/g, '-') // 合并多个-
    .replace(/^-|-$/g, ''); // 去除前后缀 -
}

export function getDefaultPodController(params: {
  imageSelectionParams: RcImageSelection;
  namespace: string;
  defaultLimit: StringMap;
  kind: 'TApp';
}): TApp;
export function getDefaultPodController(params: {
  imageSelectionParams: RcImageSelection;
  namespace: string;
  defaultLimit: StringMap;
  kind: string;
}): Workload;
export function getDefaultPodController({
  imageSelectionParams,
  namespace,
  defaultLimit,
  kind,
}: {
  imageSelectionParams: RcImageSelection;
  namespace: string;
  defaultLimit: StringMap;
  kind: string;
}) {
  const {
    repository_name: repositoryName,
    full_image_name: fullImageName,
    tag: imageTag,
    secret: imageSecret,
  } = imageSelectionParams;
  const name = adaptRepoNameToContainerName(repositoryName || '');
  const container = getDefaultContainerConfig({
    name,
    image: `${fullImageName}:${imageTag}`,
    defaultLimit,
  });
  const podController = {
    apiVersion: getApiDefinition(kind),
    kind,
    metadata: {
      name,
      namespace,
    },
    spec: {
      replicas: 1,
      template: {
        spec: {
          containers: [container],
        },
      },
    },
  };
  if (imageSecret) {
    set(podController, 'spec.template.spec.imagePullSecrets', [
      { name: imageSecret },
    ]);
  }
  return podController;
}

function getApiDefinition(kind: string) {
  const definition = RESOURCE_DEFINITIONS[
    kind.toUpperCase() as ResourceType
  ] as { apiGroup: string; apiVersion: string };
  return `${definition.apiGroup || 'apps'}/${definition.apiVersion || 'v1'}`;
}

export function getBindingServicesByPodController(params: {
  services: Service[];
  podController: Workload;
  labelBaseDomain?: string;
}) {
  const labelBaseDomain = params.labelBaseDomain || 'alauda.io';
  const name = params.podController.metadata.name;
  const kind = params.podController.kind;
  return params.services.map(item => {
    const service = { ...item };
    if (!service.spec.selector) {
      service.spec.selector = {};
    }
    service.spec.selector[
      `service.${labelBaseDomain}/name`
    ] = `${kind.toLowerCase()}-${name}`;
    return service;
  });
}

export function getDefaultServiceFromImagePorts(
  params: {
    name: string;
    ports?: number[];
    namespace: string;
  } = {
    name: '',
    ports: [],
    namespace: '',
  },
): Service {
  if (params.ports && params.ports.length > 0) {
    const service = getDefaultService(params.namespace);
    service.metadata.name = params.name;
    service.metadata.namespace = params.namespace;
    service.spec.ports = uniq(params.ports).map(port => getServicePort(port));
    return service;
  } else {
    return null;
  }
}

function getServicePort(port: number) {
  return {
    name: `${port}-${port}`,
    port,
    protocol: SERVICE_PORT_PROTOCOLS[0],
    targetPort: port,
  };
}

export function checkImageSelectionParam(params: RcImageSelection) {
  return (
    params.full_image_name && params.repository_name && params.registry_endpoint
  );
}

export function getPodControllerUpdateStrategy(
  rawK8s: Workload,
): {
  updateStrategy: PodControllerUpdateStrategyType;
  rollingUpdateStrategy: string;
} {
  if (!rawK8s) {
    return {
      updateStrategy: null,
      rollingUpdateStrategy: null,
    };
  }
  return {
    updateStrategy:
      rawK8s.kind === PodControllerKindEnum.Deployment
        ? get(rawK8s, 'spec.strategy.type')
        : get(rawK8s, 'spec.updateStrategy.type'),
    rollingUpdateStrategy: getRollingUpdateStrategy(
      rawK8s.kind === PodControllerKindEnum.Deployment
        ? get(rawK8s, 'spec.strategy.rollingUpdate', null)
        : get(rawK8s, 'spec.updateStrategy.rollingUpdate', null),
    ),
  };
}

function getRollingUpdateStrategy(
  rollingUpdate: PodControllerRollingUpdateType,
): string {
  if (!rollingUpdate) {
    return null;
  }
  return Object.keys(rollingUpdate).reduce(
    (accum, key) =>
      accum.concat(
        `${key}: ${
          rollingUpdate[key as keyof PodControllerRollingUpdateType]
        } `,
      ),
    '',
  );
}

export function removeDirtyDataBeforeUpdate(resource: KubernetesResource) {
  return dissocPath([STATUS], resource);
}

export function getResourcesFromYaml<
  T extends KubernetesResource = KubernetesResource
>(yaml: string, namespace: string) {
  let kubernetes = safeLoadAll(yaml)
    .map(item => (item === 'undefined' ? undefined : item))
    .filter(item => !!item);
  if (!kubernetes || typeof kubernetes === 'string') {
    kubernetes = [];
  }
  kubernetes.forEach((resource: T | KubernetesResourceList<T>) => {
    if (resource.kind === 'List') {
      (resource as KubernetesResourceList<T>).items.forEach(resourceItem => {
        updateResourceNamespace(resourceItem, namespace);
      });
    } else {
      updateResourceNamespace(resource as T, namespace);
    }
  });
  return kubernetes;
}

function updateResourceNamespace(
  resource: KubernetesResource,
  namespace: string,
) {
  if (!resource.metadata) {
    resource.metadata = {};
  }
  resource.metadata.namespace = namespace;
}

export function generateApplicationModel(
  kubernetes: KubernetesResource[],
): ApplicationFormModel {
  const application = kubernetes.find(
    item => item.kind === 'Application',
  ) as Application;
  const resources = kubernetes.filter(item => item.kind !== 'Application');
  const crd = kubernetes.find(
    item => item.kind === 'Application',
  ) as Application;
  return Object.assign({}, ApplicationTypeMeta, {
    metadata: crd.metadata,
    spec: {
      selector: application.spec.selector,
      componentTemplates: resources,
    },
  });
}

export function getUserAndBuiltInLabelsOrAnnotations(
  data: StringMap,
  labelBaseDomain: string,
) {
  return Object.keys(data).reduce(
    (acc, key) => {
      return key.includes(labelBaseDomain)
        ? {
            ...acc,
            builtIn: {
              ...acc.builtIn,
              [key]: data[key],
            },
          }
        : {
            ...acc,
            user: {
              ...acc.user,
              [key]: data[key],
            },
          };
    },
    {
      builtIn: {},
      user: {},
    },
  );
}

export function checkServiceAgainstPodController(
  service: Service,
  podController: Workload,
) {
  const selector = get(service, ['spec', 'selector']) || {};
  const labels =
    get(podController, ['spec', 'template', METADATA, LABELS]) || {};
  const selectorKeys = Object.keys(selector);
  return (
    selectorKeys.length &&
    selectorKeys.every(key => selector[key] === labels[key])
  );
}

export function checkIngressAgainstServices(
  ingress: Ingress,
  services: Service[],
) {
  const serviceNames = new Set();
  const defaultServiceName = get(ingress, ['spec', 'backend', 'serviceName']);
  if (defaultServiceName) {
    serviceNames.add(defaultServiceName);
  }
  get(ingress, ['spec', 'rules'], []).forEach((rule: IngressRule) => {
    get(rule, ['http', 'paths'], []).forEach((path: HTTPIngressPath) => {
      if (path.backend && path.backend.serviceName) {
        serviceNames.add(path.backend.serviceName);
      }
    });
  });
  return services
    .map(s => s.metadata.name)
    .some(name => serviceNames.has(name));
}

export function getApplicationFromResources(resources: KubernetesResource[]) {
  return resources.find(r => r.kind === 'Application') as Application;
}
