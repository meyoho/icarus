import { TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Injector,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  ApplicationFormModel,
  Environments,
  WorkspaceBaseParams,
} from 'app/typings';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AppSubmitOption } from '../form/preview-form/component';

export abstract class BaseAppCreatePage implements OnDestroy {
  onDestroy$: Subject<void> = new Subject<void>();
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  protected router: Router;
  protected activatedRoute: ActivatedRoute;
  protected env: Environments;
  protected workspaceComponent: WorkspaceComponent;
  protected appDataService: AppDataService;
  protected appUtilService: AppUtilService;
  protected appSharedService: AppSharedService;
  protected cdr: ChangeDetectorRef;
  protected acpApi: AcpApiService;
  protected translate: TranslateService;
  protected message: MessageService;

  baseParams: WorkspaceBaseParams;
  submitting: boolean;
  // to be initialized by subclass
  application: ApplicationFormModel;

  constructor(public injector: Injector) {
    // tslint:disable-next-line: deprecation
    this.cdr = this.injector.get(ChangeDetectorRef);
    this.router = this.injector.get(Router);
    this.activatedRoute = this.injector.get(ActivatedRoute);
    this.env = this.injector.get(ENVIRONMENTS);
    this.workspaceComponent = this.injector.get(WorkspaceComponent);
    this.appDataService = this.injector.get(AppDataService);
    this.appUtilService = this.injector.get(AppUtilService);
    this.appSharedService = this.injector.get(AppSharedService);
    this.acpApi = this.injector.get(AcpApiService);
    this.translate = this.injector.get(TranslateService);
    this.message = this.injector.get(MessageService);
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel(isUpdate = false) {
    if (isUpdate) {
      this.router.navigate(['app', 'detail', this.application.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } else {
      this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  protected checkFormValid() {
    this.form.onSubmit(null);
    return this.form.valid;
  }

  protected submit(isUpdate = false, submitOption?: AppSubmitOption) {
    const payload = this.appDataService.generateApplicationPayload(
      this.appDataService.applicationModel || this.application,
      this.baseParams,
    );
    this.submitting = true;
    const params = {
      payload,
      cluster: this.baseParams.cluster,
      submitOption,
    };
    this.acpApi[isUpdate ? 'updateApplication' : 'createApplication'](params)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translate.get(
            isUpdate ? 'app_update_success' : 'app_create_success',
            {
              appName: params.payload.metadata.name,
            },
          ),
        );
        this.redirectAfterSubmit(payload);
      });
  }

  private redirectAfterSubmit(res: ApplicationFormModel) {
    if (res) {
      this.router.navigate(['app', 'detail', res.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
