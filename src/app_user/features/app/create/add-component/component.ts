import { TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, Ingress, Service, Workload } from 'app/typings';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import {
  checkImageSelectionParam,
  getBindingServicesByPodController,
  getDefaultPodController,
} from '../../util';
@Component({
  templateUrl: './template.html',
  styles: [
    `
      rc-pod-controller-form,
      rc-services-form {
        margin-bottom: 16px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreateAddComponentPageComponent implements OnInit {
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  appName: string;
  isUpdatingApp: boolean; // indicates whether user is in a process of updating an app or not

  formModel: Workload;
  services: Service[] = [];
  ingresses: Ingress[] = [];

  existedServiceNames = this.appDataService.applicationModel.spec.componentTemplates
    .filter(r => r.kind === 'Service')
    .map(r => r.metadata.name);

  existedIngressNames = this.appDataService.applicationModel.spec.componentTemplates
    .filter(r => r.kind === 'Ingress')
    .map(r => r.metadata.name);

  baseParams = this.workspaceComponent.baseParamsSnapshot;

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly router: Router,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly appDataService: AppDataService,
    private readonly appSharedService: AppSharedService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (!this.appDataService.applicationModel) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
    this.isUpdatingApp = !!this.appDataService.updateAppName;
    this.appName = this.appDataService.applicationModel.metadata.name;

    const queryParams = this.activatedRoute.snapshot.queryParams;

    if (!checkImageSelectionParam(queryParams as RcImageSelection)) {
      return this.back();
    }

    this.appSharedService
      .getDefaultLimitResources(
        {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
        },
        true,
      )
      .subscribe(defaultLimit => {
        this.formModel = getDefaultPodController({
          imageSelectionParams: queryParams as RcImageSelection,
          namespace: this.baseParams.namespace,
          defaultLimit,
          kind: 'Deployment',
        });
        this.cdr.markForCheck();
      });
  }

  save() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }

    if (!this.appDataService.checkResourceName(this.formModel)) {
      return this.messageService.error(
        this.translateService.get('pod_controller_name_existed'),
      );
    }
    this.appDataService.addResource(this.formModel);
    this.addResources();
    this.back();
  }

  back() {
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  private addResources() {
    if (this.services.length > 0) {
      const services = getBindingServicesByPodController({
        services: this.services,
        podController: this.formModel,
        labelBaseDomain: this.env.LABEL_BASE_DOMAIN,
      });
      this.appDataService.addResource(services);
    }
    if (this.ingresses.length > 0) {
      this.appDataService.addResource(this.ingresses);
    }
  }
}
