import { TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';
import { take } from 'rxjs/operators';

import { Workload, WorkspaceBaseParams } from 'app/typings';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreateUpdateComponentPageComponent implements OnInit {
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  isUpdatingApp: boolean;
  appName: string;
  formModel: Workload;
  baseParams: WorkspaceBaseParams;
  private original: {
    name: string;
    kind: string;
  };

  constructor(
    private readonly router: Router,
    private readonly messageService: MessageService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly appDataService: AppDataService,
    private readonly translateService: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.isUpdatingApp = !!this.appDataService.updateAppName;

    if (!this.isUpdatingApp && !this.appDataService.applicationModel) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }

    this.appDataService.applicationModel$
      .pipe(take(1))
      .subscribe(application => {
        const formModel = application.spec.componentTemplates.find(
          (item: Workload) => {
            return (
              item.kind.toLowerCase() === params.kind &&
              item.metadata.name === params.name
            );
          },
        );
        this.appName = application.metadata.name;
        this.formModel = cloneDeep(formModel);
        this.original = {
          name: formModel.metadata.name,
          kind: formModel.kind,
        };
        this.cdr.markForCheck();
      });
  }

  save() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    if (!this.appDataService.checkResourceName(this.formModel, this.original)) {
      return this.messageService.error(
        this.translateService.get('pod_controller_name_existed'),
      );
    }
    this.appDataService.updateResource(this.formModel, this.original);
    this.back();
  }

  back() {
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }
}
