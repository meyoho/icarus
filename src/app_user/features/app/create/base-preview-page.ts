import { Injector } from '@angular/core';

import { RcImageSelection } from 'app/features-shared/image/image.type';

import { AppSubmitOption } from '../form/preview-form/component';

import { BaseAppCreatePage } from './base-app-create-page';
export class BasePreviewPage extends BaseAppCreatePage {
  isUpdate = false;

  constructor(public injector: Injector) {
    super(injector);
  }

  onAddComponent(params: RcImageSelection) {
    this.router.navigate(['add-component'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: params,
    });
  }

  onUpdateComponent(params: { name: string; kind: string }) {
    this.router.navigate(['update-component', params.kind, params.name], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  onCreate(submitOption?: AppSubmitOption) {
    this.submit(this.isUpdate, submitOption);
  }

  onCancel() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
