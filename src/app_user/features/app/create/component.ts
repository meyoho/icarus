import { Component } from '@angular/core';

import { AppDataService } from '../app-data.service';

@Component({
  template: '<router-outlet></router-outlet>',
  providers: [AppDataService],
})
export class ApplicationCreateComponent {}
