import { ObjectMeta } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { RcImageSelection } from 'app/features-shared/image/image.type';
import { ApplicationTypeMeta, Ingress, Service, Workload } from 'app/typings';

import {
  checkImageSelectionParam,
  getBindingServicesByPodController,
  getDefaultPodController,
} from '../../util';
import { BaseAppCreatePage } from '../base-app-create-page';

@Component({
  templateUrl: 'template.html',
  styles: [
    `
      aui-card,
      rc-pod-controller-form,
      rc-services-form {
        margin-bottom: 16px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreatePrimaryPageComponent extends BaseAppCreatePage
  implements OnInit, OnDestroy {
  metadata: ObjectMeta;
  podController: Workload;
  services: Service[] = [];
  ingresses: Ingress[] = [];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (!checkImageSelectionParam(queryParams as RcImageSelection)) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }

    this.appSharedService
      .getDefaultLimitResources({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      })
      .subscribe(defaultLimit => {
        this.podController = getDefaultPodController({
          imageSelectionParams: queryParams,
          namespace: this.baseParams.namespace,
          defaultLimit,
          kind: 'Deployment',
        });
        this.cdr.markForCheck();
      });
  }

  confirm() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.submit();
  }

  preview() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  getAppName() {
    return this.metadata ? this.metadata.name : '';
  }

  addComponent() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((params: RcImageSelection) => {
        if (params) {
          this.router.navigate(['add-component'], {
            relativeTo: this.activatedRoute.parent,
            queryParams: params,
          });
        }
      });
  }

  private updateServicesSelector() {
    if (this.services.length > 0) {
      this.services = getBindingServicesByPodController({
        services: this.services,
        podController: this.podController,
        labelBaseDomain: this.env.LABEL_BASE_DOMAIN,
      });
    }
  }

  private updateApplicationModel() {
    this.updateServicesSelector();
    this.appDataService.setApplicationFormModel(
      Object.assign({}, ApplicationTypeMeta, {
        metadata: this.metadata,
        spec: {
          componentTemplates: [
            this.podController,
            ...this.services,
            ...this.ingresses,
          ],
        },
      }),
    );
  }
}
