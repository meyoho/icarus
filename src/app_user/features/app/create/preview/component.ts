import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';

import { BasePreviewPage } from '../base-preview-page';
@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreatePreviewPageComponent extends BasePreviewPage
  implements OnInit {
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    if (!this.appDataService.applicationModel) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
