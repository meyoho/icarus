import { KubernetesResource, ObjectMeta } from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  TemplateRef,
} from '@angular/core';
import { safeDump, safeLoadAll } from 'js-yaml';

import { ApplicationTypeMeta } from 'app/typings';
import { createActions, viewActions, yamlWriteOptions } from 'app/utils';

import { getResourcesFromYaml } from '../../util';
import { BaseAppCreatePage } from '../base-app-create-page';

import { demo } from './demo';
@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppYamlPageComponent extends BaseAppCreatePage {
  yaml = '';
  yamlDemo: string;
  codeEditorOptions = yamlWriteOptions;
  actionsConfigView = viewActions;
  actionsConfigCreate = createActions;

  metadata: ObjectMeta;

  constructor(
    injector: Injector,
    private readonly notification: NotificationService,
    private readonly dialogService: DialogService,
  ) {
    super(injector);
    this.initYamlDemo();
  }

  initYamlDemo() {
    const resources = safeLoadAll(demo).filter(r => !!r);
    resources.forEach((r: KubernetesResource) => {
      r.metadata.namespace = this.baseParams.namespace;
    });
    this.yamlDemo = resources
      .map(r => safeDump(r, { sortKeys: true }))
      .join('---\r\n');
  }

  confirm() {
    if (!this.checkFormValid()) {
      return;
    }
    if (this.yamlToFormModel()) {
      this.submit();
    }
  }

  cancel() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  useDemoTemplate() {
    this.yaml = this.yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private yamlToFormModel() {
    try {
      const resources = getResourcesFromYaml(
        this.yaml,
        this.baseParams.namespace,
      );
      this.application = Object.assign({}, ApplicationTypeMeta, {
        metadata: this.metadata,
        spec: {
          componentTemplates: resources,
        },
      });
    } catch (err) {
      this.notification.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return false;
    }
    return true;
  }
}
