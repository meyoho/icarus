import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { Application, RESOURCE_TYPES } from 'app/typings';
import { ACTION, STATUS } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AppUtilService } from '../app-util.service';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationListPageComponent {
  list: K8SResourceList<Application>;

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.APPLICATION,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  columns = [NAME, STATUS, 'created_time', ACTION];

  baseParams = this.workspaceComponent.baseParamsSnapshot;

  project = this.baseParams.project;
  cluster = this.baseParams.cluster;
  namespace = this.baseParams.namespace;

  constructor(
    private readonly appUtilService: AppUtilService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly appSharedService: AppSharedService,
    private readonly k8sPermission: K8sPermissionService,
    public router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.list = new K8SResourceList({
      fetcher: queryParams =>
        this.k8sApi.getResourceList({
          type: RESOURCE_TYPES.APPLICATION,
          cluster: this.workspaceComponent.baseParamsSnapshot.cluster,
          namespace: this.workspaceComponent.baseParamsSnapshot.namespace,
          queryParams,
        }),
      activatedRoute: this.activatedRoute,
    });
  }

  isFederated = (resource: Application) => {
    return this.k8sUtil.isFederatedResource(resource);
  };

  trackByFn = (_: number, app: Application) => {
    return app.metadata.name;
  };

  searchByName(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  create() {
    this.appSharedService
      .selectImage(this.baseParams)
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['app', 'create'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
            queryParams: res,
          });
        }
      });
  }

  createFromYaml() {
    this.router.navigate(['app', 'create_from_yaml'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update(app: Application) {
    const name = app.metadata.name;
    this.router.navigate(['app', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteApp(app: Application) {
    try {
      await this.appUtilService.deleteApplication({
        cluster: this.cluster,
        resource: app,
      });
      this.list.delete(app);
      this.cdr.markForCheck();
    } catch (error) {}
  }
}
