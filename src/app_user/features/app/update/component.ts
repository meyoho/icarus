import { TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AppDataService } from '../app-data.service';

@Component({
  template: '<router-outlet></router-outlet>',
  providers: [AppDataService],
})
export class ApplicationUpdateComponent implements OnInit {
  constructor(
    private readonly appDataService: AppDataService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly translate: TranslateService,
    private readonly router: Router,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.appDataService.initFetchRequest(
      baseParams.cluster,
      baseParams.namespace,
      params.name,
    );
    this.appDataService.resourceNotFound$.pipe(take(1)).subscribe(res => {
      if (res) {
        this.messageService.error(this.translate.get('resource_not_exist'));
        this.router.navigate(['app'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      }
    });
  }
}
