import { Component, Injector, OnInit } from '@angular/core';

import { BasePreviewPage } from '../../create/base-preview-page';
@Component({
  templateUrl: './template.html',
})
export class ApplicationUpdatePreviewPageComponent extends BasePreviewPage
  implements OnInit {
  appName: string;
  constructor(public injector: Injector) {
    super(injector);
    this.isUpdate = true;
  }

  ngOnInit() {
    this.appName = this.appDataService.updateAppName;
  }
}
