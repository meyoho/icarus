import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  NAME,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, noop, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  switchMap,
} from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import {
  HelmRequest,
  HelmRequestPhaseEnum,
  HelmRequestStatusColorMapper,
  HelmRequestStatusIconMapper,
  RESOURCE_TYPES,
} from 'app/typings';
import { getHelmRequestStatus } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { HelmRequestUtilService } from '../hr.util.service';

interface HrAndResource {
  hr: HelmRequest;
  resources: KubernetesResource[];
}

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmRequestDetailComponent {
  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly util: HelmRequestUtilService,
    private readonly catalogApi: CatalogApiService,
    private readonly translate: TranslateService,
  ) {}

  getHelmRequestStatus = getHelmRequestStatus;
  HelmRequestStatusColorMapper = HelmRequestStatusColorMapper;
  HelmRequestStatusIconMapper = HelmRequestStatusIconMapper;

  baseParams = this.workspaceComponent.baseParamsSnapshot;
  params$ = this.activatedRoute.paramMap
    .pipe(map(params => params.get(NAME)))
    .pipe(
      distinctUntilChanged(),
      map(name => ({ name })),
      publishRef(),
    );

  permissions$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
      }),
    ),
  );

  dataManager = new AsyncDataLoader<{
    resource: HrAndResource;
    permissions: { update: boolean; delete: boolean };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi
          .watchResource<HelmRequest>({
            type: RESOURCE_TYPES.HELM_REQUEST,
            namespace: this.baseParams.namespace,
            cluster: this.baseParams.cluster,
            name: params.name,
          })
          .pipe(
            switchMap(hr => {
              if (!hr) {
                return of(null);
              }
              if (hr.status?.phase === HelmRequestPhaseEnum.Synced) {
                return this.catalogApi
                  .getHelmRequestResources({
                    cluster: this.baseParams.cluster,
                    namespace: this.baseParams.namespace,
                    hrName: hr.metadata.name,
                  })
                  .pipe(
                    map(res => res.resources),
                    catchError(() => of([])),
                    map(resources => ({
                      hr,
                      resources,
                    })),
                  );
              } else {
                return of({
                  hr,
                  resources: [],
                });
              }
            }),
          ),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) =>
          resource
            ? {
                resource,
                permissions,
              }
            : null,
        ),
      );
    },
  });

  update(item: HelmRequest) {
    this.router.navigate(['hr', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(item: HelmRequest) {
    this.util
      .delete(item, this.workspaceComponent.baseParamsSnapshot.cluster)
      .then(this.jumpToListPage)
      .catch(noop);
  }

  jumpToListPage = () => {
    this.router.navigate(['hr'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };

  getDeployNotes = (resource: HelmRequest) => {
    return resource.status && resource.status.notes
      ? resource.status.notes
      : this.translate.get('no_deploy_info');
  };

  shouldShowAppLink = (resource: HelmRequest) => {
    return (
      this.util.isDeployApplicationEnabled(resource) &&
      resource.status &&
      resource.status.phase === HelmRequestPhaseEnum.Synced
    );
  };
}
