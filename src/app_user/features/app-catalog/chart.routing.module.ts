import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChartDetailComponent } from './chart-detail/component';
import { ChartListComponent } from './chart-list/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ChartListComponent,
  },
  {
    path: 'detail/:name/:version',
    component: ChartDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartRoutingModule {}
