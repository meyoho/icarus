import { Injectable } from '@angular/core';
import { safeLoad } from 'js-yaml';
import { flatten } from 'lodash-es';
import { compose, load, serialize } from 'yaml-js';

import { TRUE } from 'app/utils';

export enum YamlEntryTypeEnum {
  number = 'number',
  string = 'string',
  null = 'null',
  object = 'object',
  boolean = 'boolean',
  objectArray = 'object array',
  array = 'array',
}

/**
 * One entry of flatten result of a yaml string.
 */
export interface YamlEntry {
  /**
   * Path in array format.
   */
  paths: string[];

  /**
   * Original type of the entry. Will be used to recover from inputs (which
   * will always be string).
   */
  type: YamlEntryTypeEnum;

  /**
   * Value to the path.
   */
  value: string;

  /**
   * The comment before this entry.
   */
  preComment: string;

  /**
   * The inline comment following this entry.
   */
  inlineComment: string;

  /**
   * The string content after this entry. Only for the last entry.
   */
  afterContent?: string;
}

interface YjsMark {
  buffer: string;
  pointer: number;
}

type YjsNodeValue =
  | string
  | number
  | boolean
  | YjsNode[]
  | Array<[YjsKeyNode, YjsNode]>;

interface YjsNode {
  value: YjsNodeValue;
  id: string;
  start_mark: YjsMark;
  end_mark: YjsMark;
  tag: string;
}

interface YjsMappingNode extends YjsNode {
  value: Array<[YjsKeyNode, YjsNode]>;
  id: 'mapping';
}

interface YjsScalarNode extends YjsNode {
  value: string | number | boolean;
  id: 'scalar';
}

interface YjsSequenceNode extends YjsNode {
  value: Array<YjsScalarNode | YjsMappingNode>;
  id: 'sequence';
}

interface YjsKeyNode extends YjsScalarNode {
  value: string;
  id: 'scalar';
}

interface ScalarNodeWithPath {
  paths: string[];
  node: YjsScalarNode | YjsSequenceNode;
}

/**
 * A service to parse a yaml with comments.
 */
@Injectable()
export class YamlParserService {
  /**
   * Flatten a yaml to an array.
   * Each entry of the output array is a key-value field to the original yaml.
   * Each entry will also tries to find the most related comment to be used as a hint.
   *
   * @param yaml the input yaml
   */
  parse(yaml: string): YamlEntry[] {
    const rootNode = compose(yaml);
    if (rootNode) {
      return this.parseFlattenedTree(this.flattenTree(rootNode));
    } else {
      return [];
    }
  }

  /**
   * Flatten a YAML token tree to an array of ScalarNodeWithPath.
   * This is to prepare the final result by interpolating nodes pairwise.
   *
   * @param tree node to be flatten
   * @param prefix the prefix to the node
   */
  private flattenTree(
    tree: YjsNode,
    prefix: string[] = [],
  ): ScalarNodeWithPath[] {
    let result: ScalarNodeWithPath[] = [];
    switch (tree.id) {
      // Flatten mapping:
      case 'mapping': {
        const treeNode = tree as YjsMappingNode;
        const buffer = treeNode.start_mark.buffer;
        // For mapping node '{}', we transform it as a scalar node.
        if (
          buffer[treeNode.start_mark.pointer] === '{' &&
          buffer[treeNode.end_mark.pointer - 1] === '}'
        ) {
          return [
            {
              paths: [...prefix],
              node: {
                ...treeNode,
                id: 'scalar',
                value: buffer.slice(
                  treeNode.start_mark.pointer,
                  treeNode.end_mark.pointer,
                ),
              },
            },
          ];
        } else {
          const flattenedMap = treeNode.value.map(([keyNode, valueNode]) => {
            return this.flattenTree(valueNode, [...prefix, keyNode.value]);
          });
          result = [...result, ...flatten(flattenedMap)];
        }
        break;
      }
      // Flatten sequence. In the final result, array will be treated as a single node.
      case 'sequence': {
        const sequenceNode = tree as YjsSequenceNode;
        return [
          {
            paths: [...prefix],
            node: sequenceNode,
          },
        ];
      }
      case 'scalar': {
        const scalarNode = tree as YjsScalarNode;
        return [
          {
            paths: [...prefix],
            node: scalarNode,
          },
        ];
      }
      default:
        break;
    }

    return result;
  }

  /**
   * Parse the flattened tree nodes and apply comments.
   */
  private parseFlattenedTree(flattenedTree: ScalarNodeWithPath[]): YamlEntry[] {
    return flattenedTree.reduce((accum, nodeWithPath, index) => {
      const currNode = nodeWithPath.node;
      const prevNode = index > 0 ? flattenedTree[index - 1].node : undefined;

      const [inlineIndexStart, inlineIndexEnd] = this.getInlineCommentIndex(
        currNode,
      );

      const [, prevLineEndIndex] = this.getInlineCommentIndex(prevNode);

      const inlineComment = this.parseCommentFromRange(
        currNode,
        inlineIndexStart,
        inlineIndexEnd,
      );
      const preComment = this.getPreComment(currNode, prevLineEndIndex);

      const nodeValue = this.getNodeValue(currNode);
      const entry = {
        paths: [...nodeWithPath.paths],
        value: nodeValue,
        type: this.getNodeType(currNode),
        preComment,
        inlineComment,
        afterContent:
          index === flattenedTree.length - 1
            ? currNode.end_mark.buffer
                .slice(Math.max(0, currNode.end_mark.pointer))
                .replace('\u0000', '')
            : undefined,
      };

      return [...accum, entry];
    }, []);
  }

  /**
   * Get pre comment of a node.
   */
  private getPreComment(currNode: YjsNode, startIndex: number) {
    const endIndex = currNode.start_mark.pointer;
    return this.parseCommentFromRange(currNode, startIndex, endIndex);
  }

  private parseCommentFromRange(
    node: YjsNode,
    startIndex: number,
    endIndex: number,
  ) {
    const comments = node.start_mark.buffer
      .slice(startIndex, endIndex)
      .match(/#.*$/gm);

    return comments ? comments.map(comment => comment.trim()).join('\n') : '';
  }

  /* Find the inline comment indices */
  private getInlineCommentIndex(node: YjsNode) {
    if (!node) {
      return [0, 0];
    }

    let endIndex = -1;

    if (node.id === 'sequence') {
      const sequenceNode = node as YjsSequenceNode;
      if (sequenceNode.value.length === 0) {
        return [node.start_mark.pointer, node.end_mark.pointer];
      }
      const lastSubNode = sequenceNode.value[sequenceNode.value.length - 1];
      if (lastSubNode) {
        endIndex = lastSubNode.end_mark.buffer.indexOf(
          '\n',
          lastSubNode.end_mark.pointer,
        );
      }
      endIndex =
        endIndex === -1 && lastSubNode
          ? lastSubNode.end_mark.pointer
          : endIndex;
      return [node.start_mark.pointer, endIndex];
    }

    let startIndex = node.end_mark.buffer.indexOf('#', node.end_mark.pointer);
    endIndex = node.end_mark.buffer.indexOf('\n', node.end_mark.pointer);
    endIndex = endIndex === -1 && node ? node.end_mark.pointer : endIndex;
    startIndex =
      startIndex === -1 || startIndex > endIndex ? endIndex : startIndex;
    return [startIndex, endIndex];
  }

  private getNodeType(currNode: YjsScalarNode | YjsSequenceNode) {
    return currNode.id === 'sequence'
      ? this.getArrayType(currNode)
      : this.tagTypeToJsType(currNode.tag);
  }

  private getArrayType(currNode: YjsScalarNode | YjsSequenceNode) {
    const val: unknown[] = load(serialize(currNode));
    return val.some(v => typeof v === 'object')
      ? YamlEntryTypeEnum.objectArray
      : YamlEntryTypeEnum.array;
  }

  /**
   * Cast yaml tags to json types.
   */
  private tagTypeToJsType(tag: string): string {
    if (['tag:yaml.org,2002:int', 'tag:yaml.org,2002:float'].includes(tag)) {
      return YamlEntryTypeEnum.number;
    } else if (tag === 'tag:yaml.org,2002:map') {
      return YamlEntryTypeEnum.object;
    } else if (tag === 'tag:yaml.org,2002:bool') {
      return YamlEntryTypeEnum.boolean;
    } else if (tag === 'tag:yaml.org,2002:null') {
      return YamlEntryTypeEnum.null;
    } else {
      return YamlEntryTypeEnum.string;
    }
  }

  private getNodeValue(currNode: YjsScalarNode | YjsSequenceNode) {
    const type = this.getNodeType(currNode);
    switch (type) {
      case YamlEntryTypeEnum.object: {
        const obj = safeLoad(currNode.value.toString());
        if (obj) {
          Object.keys(obj).forEach(key => {
            if (obj[key] === null) {
              obj[key] = '';
            }
          });
        }
        return obj;
      }
      case YamlEntryTypeEnum.null:
        return '';
      case YamlEntryTypeEnum.array:
      case YamlEntryTypeEnum.objectArray:
        return load(serialize(currNode));
      case YamlEntryTypeEnum.boolean:
        return currNode.value.toString() === TRUE;
      case YamlEntryTypeEnum.number:
        return parseFloat(currNode.value.toString());
      default:
        return currNode.value.toString();
    }
  }
}
