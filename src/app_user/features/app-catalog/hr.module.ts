import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { HelmRequestCreateComponent } from './hr-create/component';
import { HelmRequestDetailComponent } from './hr-detail/component';
import { HelmRequestFormComponent } from './hr-form/component';
import { HelmRequestListComponent } from './hr-list/component';
import { HelmRequestUpdateComponent } from './hr-update/component';
import { HelmRequestValuesFormComponent } from './hr-values-form/component';
import { HelmRequestValuesComponent } from './hr-values/component';
import { ObjectJsonArrayFormComponent } from './hr-values/object-json-array-form/component';
import { HelmRequestRoutingModule } from './hr.routing.module';
import { HelmRequestUtilService } from './hr.util.service';
import { YamlParserService } from './yaml-parser.service';

@NgModule({
  imports: [SharedModule, EventSharedModule, HelmRequestRoutingModule],
  exports: [],
  declarations: [
    ObjectJsonArrayFormComponent,
    HelmRequestValuesFormComponent,
    HelmRequestValuesComponent,
    HelmRequestFormComponent,
    HelmRequestCreateComponent,
    HelmRequestUpdateComponent,
    HelmRequestListComponent,
    HelmRequestDetailComponent,
  ],
  providers: [HelmRequestUtilService, YamlParserService],
})
export class HelmRequestModule {}
