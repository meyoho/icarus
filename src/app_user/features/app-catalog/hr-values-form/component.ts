import {
  Callback,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { safeDump, safeLoad } from 'js-yaml';
import { Observable, Subscription, combineLatest, of } from 'rxjs';
import { catchError, filter, finalize, map } from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import { createActions, yamlWriteOptions } from 'app/utils';

@Component({
  selector: 'rc-helm-request-values-form',
  templateUrl: 'template.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HelmRequestValuesFormComponent),
      multi: true,
    },
  ],
})
export class HelmRequestValuesFormComponent
  implements ControlValueAccessor, OnInit, OnDestroy, AfterViewInit {
  @ViewChild(FormGroupDirective, { static: false })
  ngFormGroupDirective: FormGroupDirective;

  @Input() chartName: string;
  @ObservableInput(true) chartName$: Observable<string>;

  @Input() chartVersion: string;
  @ObservableInput(true) chartVersion$: Observable<string>;

  onCvaChange: Callback;
  onCvaTouched: Callback;

  form: FormGroup;
  mode = 'form';
  configLoading = false;
  configYaml: string;
  valuesYaml: string;
  editorOptions = yamlWriteOptions;
  editorActions = createActions;

  private parentFormSub: Subscription;

  registerOnChange(fn: (value: Record<string, unknown>) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(values: Record<string, unknown>) {
    if (values) {
      this.setUpFormValues(values);
    }
  }

  constructor(
    private readonly fb: FormBuilder,
    private readonly cdr: ChangeDetectorRef,
    private readonly catalogApi: CatalogApiService,
    private readonly injector: Injector,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    this.form = this.fb.group({
      values: this.fb.control(null),
      yaml: this.fb.control(''),
    });
  }

  ngAfterViewInit() {
    this.setUpFormSubscription();
    this.setupSubmitEvent();
  }

  ngOnInit() {
    combineLatest([
      this.chartName$.pipe(filter(n => !!n)),
      this.chartVersion$.pipe(filter(n => !!n)),
    ]).subscribe(([_name, _version]) => {
      this.getChartValues();
    });
  }

  ngOnDestroy() {
    if (this.parentFormSub) {
      this.parentFormSub.unsubscribe();
    }
  }

  modeChange(mode: string) {
    if (mode === 'yaml') {
      // values formControl 初始状态，未修改时，没有 value，需要触发一次提交，覆盖掉 原始yaml 里存在的 null 值
      if (!this.form.get('values').value) {
        this.ngFormGroupDirective.onSubmit(null);
        setTimeout(() => {
          this.setYamlByForm();
        });
      } else {
        this.setYamlByForm();
      }
    } else {
      this.setFormByYaml();
    }
  }

  private setYamlByForm() {
    this.form
      .get('yaml')
      .setValue(safeDump(this.form.get('values').value), { emitEvent: false });
  }

  private setFormByYaml() {
    const valuesYaml = safeDump(this.getEditorValue());
    if (valuesYaml) {
      this.valuesYaml = valuesYaml;
      this.form
        .get('values')
        .setValue(safeLoad(valuesYaml), { emitEvent: false });
    }
  }

  private getFormValue() {
    if (this.mode === 'form') {
      return this.form.get('values').value;
    } else {
      return this.getEditorValue();
    }
  }

  private getEditorValue() {
    let values;
    try {
      values = safeLoad(this.form.get('yaml').value);
    } catch (err) {
      this.message.error(this.translate.get('failed_to_parse_yaml'));
    }
    return values;
  }

  private setUpFormValues(values: Record<string, unknown>) {
    this.valuesYaml = safeDump(values);
    this.form.get('values').setValue(values);
    this.form.get('yaml').setValue(this.valuesYaml);
    this.cdr.markForCheck();
  }

  private getChartValues() {
    this.configLoading = true;
    this.catalogApi
      .getChartFiles({
        chartName: this.chartName,
        chartVersion: this.chartVersion,
      })
      .pipe(
        map(r => r.files['values.yaml']),
        catchError(() => of('{}')),
        finalize(() => {
          this.configLoading = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(configYaml => {
        if (configYaml) {
          const config = safeLoad(configYaml, {
            json: true,
          });

          if (!this.form.get('yaml').value) {
            // omit comment nodes
            this.form.get('yaml').setValue(safeDump(config));
          }
          this.configYaml = safeDump(config);
          this.cdr.markForCheck();
        }
      });
  }

  private setUpFormSubscription() {
    this.form.valueChanges.subscribe(() => {
      this.onCvaChange(this.getFormValue());
    });
  }

  private setupSubmitEvent() {
    const parentForm = this.injector.get(FormGroupDirective);
    if (parentForm) {
      this.parentFormSub = parentForm.ngSubmit.subscribe((event: Event) => {
        if (this.ngFormGroupDirective) {
          this.ngFormGroupDirective.onSubmit(event);
        }
        this.form.updateValueAndValidity();
        this.onCvaChange(this.getFormValue());
        this.cdr.markForCheck();
      });
    }
  }
}
