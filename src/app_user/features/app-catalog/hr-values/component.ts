import { Callback, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { safeLoad } from 'js-yaml';
import { merge, set } from 'lodash-es';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { dump } from 'yaml-js';

import { YamlEntry, YamlParserService } from '../yaml-parser.service';

type ExtendedYamlEntry = YamlEntry & { path?: string };

@Component({
  selector: 'rc-helm-request-values',
  templateUrl: 'template.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HelmRequestValuesComponent),
      multi: true,
    },
  ],
})
export class HelmRequestValuesComponent
  implements ControlValueAccessor, OnChanges, OnDestroy, AfterViewInit {
  @ViewChild(FormGroupDirective, { static: false })
  ngFormGroupDirective: FormGroupDirective;

  @Input() configYaml: string;

  yamlEntries: ExtendedYamlEntry[];
  form: FormGroup = this.formBuilder.group({});

  onCvaChange: Callback;
  onCvaTouched: Callback;

  private readonly formSub: Subscription;
  private parentFormSub: Subscription;
  private values: Record<string, unknown>;

  registerOnChange(fn: (value: Record<string, unknown>) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(resource: Record<string, unknown>) {
    if (resource) {
      this.values = resource;
      if (this.configYaml) {
        this.rebuildForm(true);
      }
      this.cdr.markForCheck();
    }
  }

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly yamlParser: YamlParserService,
    private readonly message: MessageService,
    private readonly formBuilder: FormBuilder,
    private readonly translate: TranslateService,
    private readonly injector: Injector,
  ) {}

  ngOnChanges({ configYaml }: SimpleChanges) {
    if (configYaml && configYaml.currentValue) {
      // if previousValues is `undefined`, rebuilding shoud consider original resource if exists
      this.rebuildForm(!configYaml.previousValue);
    }
  }

  ngOnDestroy() {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
    if (this.parentFormSub) {
      this.parentFormSub.unsubscribe();
    }
  }

  ngAfterViewInit() {
    this.setupSubmitEvent();
  }

  entryTrackBy(_: number, entry: ExtendedYamlEntry) {
    return entry.paths;
  }

  isEntryMultiline(entry: ExtendedYamlEntry) {
    return typeof entry.value === 'string' && entry.value.includes('\n');
  }

  private rebuildForm(mergeValues: boolean) {
    try {
      let formConfig = safeLoad(this.configYaml);
      if (mergeValues && this.values) {
        formConfig = merge(formConfig, this.values);
      }
      // Use yaml-js 'dump' to make sure the last level is 'flow style'. https://yaml.org/spec/1.2/spec.html#Flow
      this.yamlEntries = this.yamlParser.parse(dump(formConfig));
      // Enhance the path with display format
      this.yamlEntries.forEach(entry => (entry.path = entry.paths.join('.')));
      this.form = this.formBuilder.group(
        this.yamlEntries.reduce((accum, entry) => {
          const control = new FormControl(entry.value);
          return { ...accum, [entry.path]: control };
        }, {}),
      );
      this.setUpFormSubscription();
    } catch (err) {
      this.message.error(this.translate.get('failed_to_parse_yaml'));
    }
  }

  private setUpFormSubscription() {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
    this.form.valueChanges.pipe(debounceTime(300)).subscribe(() => {
      this.onCvaChange(this.dump());
    });
  }

  private dump() {
    const json = safeLoad(this.configYaml);
    Object.entries(this.form.value).forEach(
      ([path, value]: [string, string]) => {
        set(
          json,
          this.yamlEntries.find(entry => entry.path === path).paths,
          value,
        );
      },
    );
    return json;
  }

  private setupSubmitEvent() {
    const parentForm = this.injector.get(FormGroupDirective);
    if (parentForm) {
      this.parentFormSub = parentForm.ngSubmit.subscribe((event: Event) => {
        if (this.ngFormGroupDirective) {
          this.ngFormGroupDirective.onSubmit(event);
        }
        this.form.updateValueAndValidity();
        this.onCvaChange(this.dump());
        this.cdr.markForCheck();
      });
    }
  }
}
