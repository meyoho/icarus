import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormArray } from '@angular/forms';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

import { rowBackgroundColorFn } from 'app/utils';

@Component({
  selector: 'rc-object-json-array-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ObjectJsonArrayFormComponent
  extends BaseResourceFormComponent<unknown[], string[]>
  implements OnInit {
  @Input()
  resourceName: string;

  form: FormArray;

  constructor(public injector: Injector) {
    super(injector);
  }

  rowBackgroundColorFn = rowBackgroundColorFn;

  getResourceMergeStrategy() {
    return false;
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): string[] {
    return [];
  }

  adaptResourceModel(resource: unknown[]): string[] {
    return resource ? resource.map(val => JSON.stringify(val, null, 2)) : [];
  }

  adaptFormModel(formModel: string[]): unknown[] {
    return formModel.map(json => {
      try {
        return JSON.parse(json);
      } catch (err) {
        return '{}';
      }
    });
  }

  add(index = this.form.length) {
    this.form.insert(index, this.getOnFormArrayResizeFn()());
    this.cdr.markForCheck();
  }

  remove(index: number) {
    this.form.removeAt(index);
    this.cdr.markForCheck();
  }

  getOnFormArrayResizeFn() {
    return () => this.fb.control('{}');
  }
}
