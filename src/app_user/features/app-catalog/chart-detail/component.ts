import {
  AsyncDataLoader,
  DESCRIPTION,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  LABELS,
  METADATA,
  NAME,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get, isEqual } from 'lodash-es';
import marked from 'marked';
import { combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  pluck,
  switchMap,
} from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import { Chart, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { getChartLatestVersion } from '../util';
@Component({
  templateUrl: 'template.html',
})
export class ChartDetailComponent {
  baseParams = this.workspaceComponent.baseParamsSnapshot;

  params$ = this.activatedRoute.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
      version: params.get('version'),
    })),
    distinctUntilChanged(isEqual),
    publishRef(),
  );

  readme$ = this.params$.pipe(
    switchMap(params =>
      this.catalogApi
        .getChartFiles({
          chartName: params.name,
          chartVersion: params.version,
        })
        .pipe(
          map(r => r.files['README.md']),
          map(r => marked(r)),
          catchError(() => of('')),
        ),
    ),
  );

  chart$ = this.params$.pipe(
    switchMap(params =>
      this.catalogApi
        .getChartList({
          appCluster: this.baseParams.cluster,
          appNamespace: this.baseParams.namespace,
          fieldSelector: matchLabelsToString({
            'metadata.name': params.name,
          }),
        })
        .pipe(
          pluck('items'),
          map(items => items[0]),
          catchError(() => of(null)),
          publishRef(),
        ),
    ),
  );

  allowCreate$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: K8sResourceAction.CREATE,
      }),
    ),
  );

  dataManager = new AsyncDataLoader<{
    resource: Chart;
    readme: string;
    chart: Chart;
    allowCreate: boolean;
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<Chart>({
          type: RESOURCE_TYPES.CHART,
          namespaced: true,
          name: params.name,
        }),
        this.readme$,
        this.chart$,
        this.allowCreate$,
      ]).pipe(
        map(([resource, readme, chart, allowCreate]) => ({
          resource,
          readme,
          chart,
          allowCreate,
        })),
      );
    },
  });

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly catalogApi: CatalogApiService,
  ) {}

  getChartLatestVersion = getChartLatestVersion;

  createApp(chartName: string) {
    this.router.navigate(['apply', chartName], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  getChartName(chart: Chart) {
    const name = chart.metadata.name;
    return name.includes('.') ? name.split('.')[0] : name;
  }

  getChartRepo(chart: Chart) {
    return get(chart, [METADATA, LABELS, 'repo']) || '';
  }

  getDescription(chart: Chart) {
    return get(chart, ['spec', 'versions', '0', DESCRIPTION]) || '';
  }

  getChartAppName(chart: Chart) {
    return chart ? chart.spec.appName : null;
  }

  openHrDetail(chart: Chart) {
    this.router.navigate(['hr', 'detail', chart.spec.appName], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  createHelmRequest(chart: Chart) {
    this.router.navigate(['hr', 'deploy'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
      queryParams: {
        chart: chart.metadata.name,
        version: getChartLatestVersion(chart),
      },
    });
  }

  jumpToListPage = () => {
    this.router.navigate(['hr'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };
}
