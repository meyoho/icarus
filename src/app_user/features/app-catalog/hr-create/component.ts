import {
  K8sApiService,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, map } from 'rxjs/operators';

import { HelmRequest, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmRequestCreateComponent {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  submitting: boolean;
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  resource: HelmRequest;
  chartName$ = this.activatedRoute.queryParams.pipe(
    map(params => params.chart),
    publishRef(),
  );

  chartVersion$ = this.activatedRoute.queryParams.pipe(
    map(params => params.version),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly router: Router,
    private readonly location: Location,
  ) {}

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;

    const params = {
      type: RESOURCE_TYPES.HELM_REQUEST,
      resource: this.resource,
      cluster: this.baseParams.cluster,
    };
    this.k8sApi
      .postResource(params)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(this.translate.get('create_success'));
        this.router.navigate(['hr', 'detail', this.resource.metadata.name], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      });
  }

  cancel() {
    this.location.back();
  }
}
