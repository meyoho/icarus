import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ChartSharedModule } from 'app_admin/features/chartrepo/shared.module';

import { ChartDetailComponent } from './chart-detail/component';
import { ChartListComponent } from './chart-list/component';
import { ChartRoutingModule } from './chart.routing.module';

@NgModule({
  imports: [SharedModule, ChartSharedModule, ChartRoutingModule],
  exports: [],
  declarations: [ChartListComponent, ChartDetailComponent],
  providers: [],
})
export class ChartModule {}
