import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TOKEN_BASE_DOMAIN,
  matchExpressionsToString,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, ReplaySubject, Subject, merge, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  pluck,
  scan,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { CatalogApiService } from 'app/services/api/catalog-api.service';
import { Chart, ChartRepo, RESOURCE_TYPES } from 'app/typings';
import { ASSIGN_ALL } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { getChartLatestVersion } from '../util';

type ChartsScanner = (state: ChartsState) => ChartsState;
interface ChartsState {
  charts: Chart[];
  filterKey: string;
  loading: boolean;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartListComponent {
  baseParams = this.workspaceComponent.baseParamsSnapshot;

  selectedRepoName: string;

  allowCreate$ = this.workspaceComponent.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: K8sResourceAction.CREATE,
      }),
    ),
    publishRef(),
  );

  repos$: Observable<ChartRepo[]> = this.k8sApi
    .getGlobalResourceList<ChartRepo>({
      namespaced: true,
      type: RESOURCE_TYPES.CHART_REPO,
      queryParams: {
        labelSelector: matchExpressionsToString([
          {
            key: `${this.baseDomain}/project`,
            operator: 'in',
            values: [this.baseParams.project, ASSIGN_ALL],
          },
        ]),
      },
    })
    .pipe(
      pluck('items'),
      catchError(() => of([])),
      tap(items => {
        if (items.length > 0) {
          this.repoSelect(items[0]);
        }
        this.cdr.markForCheck();
      }),
      publishRef(),
    );

  initialized$ = this.repos$.pipe(map(repos => !!repos));
  repoLength$ = this.repos$.pipe(map(repos => repos.length));

  repoSelect$ = new ReplaySubject<string>(1);
  filterChange$ = new Subject<ChartsScanner>();
  chartsState$ = merge(
    this.repoSelect$.pipe(
      switchMap(repo =>
        this.getChartsScannerByRepoName$(repo).pipe(
          startWith((state: ChartsState) => ({
            ...state,
            loading: true,
          })),
          debounceTime(0),
        ),
      ),
    ),
    this.filterChange$,
  ).pipe(
    scan((state: ChartsState, action: ChartsScanner) => action(state), {
      charts: [],
      filterKey: '',
      loading: false,
    }),
    publishRef(),
  );

  charts$ = this.chartsState$.pipe(
    map(state => {
      return state.filterKey
        ? state.charts.filter(c => c.metadata.name.includes(state.filterKey))
        : state.charts;
    }),
  );

  chartsLength$ = this.chartsState$.pipe(map(state => state.charts.length));
  chartsLoading$ = this.chartsState$.pipe(map(state => state.loading));

  constructor(
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly k8sApi: K8sApiService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly catalogApi: CatalogApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  getChartsScannerByRepoName$(repo: string) {
    return this.catalogApi
      .getChartList({
        appCluster: this.baseParams.cluster,
        appNamespace: this.baseParams.namespace,
        labelSelector: matchLabelsToString({
          repo,
        }),
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
        map(charts => (state: ChartsState) => ({
          ...state,
          charts,
          loading: false,
        })),
      );
  }

  filterKeyChange(filterKey: string) {
    this.filterChange$.next((state: ChartsState) => {
      return {
        ...state,
        filterKey,
      };
    });
  }

  repoSelect(repo: ChartRepo) {
    this.selectedRepoName = repo.metadata.name;
    this.repoSelect$.next(repo.metadata.name);
  }

  createApp(chart: Chart) {
    const version = getChartLatestVersion(chart);
    this.router.navigate(['hr', 'deploy'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
      queryParams: {
        chart: chart.metadata.name,
        version,
      },
    });
  }

  onOpenHrDetail(name: string) {
    this.router.navigate(['hr', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  showDetail(chart: Chart) {
    this.router.navigate(
      ['detail', chart.metadata.name, getChartLatestVersion(chart)],
      {
        relativeTo: this.activatedRoute,
      },
    );
  }

  trackByRepoUid(_index: number, repo: ChartRepo) {
    return repo.metadata.uid;
  }

  trackByChartUid(_index: number, chart: Chart) {
    return chart.metadata.uid;
  }

  getButtonType(item: ChartRepo) {
    return item.metadata.name === this.selectedRepoName ? 'primary' : 'text';
  }
}
