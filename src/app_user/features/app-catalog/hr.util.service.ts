import {
  ANNOTATIONS,
  K8sApiService,
  METADATA,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { get } from 'lodash-es';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { HelmRequest, RESOURCE_TYPES } from 'app/typings';
import { TRUE } from 'app/utils';

@Injectable()
export class HelmRequestUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  delete(resource: HelmRequest, cluster: string) {
    return this.dialog
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get(
          this.isDeployApplicationEnabled(resource)
            ? 'delete_helmrequest_with_app_confirm'
            : 'delete_helmrequest_confirm',
          {
            name: resource.metadata.name,
          },
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.HELM_REQUEST,
              cluster,
              resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.message.success(this.translate.get('delete_success'));
      });
  }

  isDeployApplicationEnabled(resource: HelmRequest) {
    return (
      get(resource, [
        METADATA,
        ANNOTATIONS,
        this.k8sUtil.normalizeType('deploy-application', 'helmrequests'),
      ]) === TRUE
    );
  }
}
