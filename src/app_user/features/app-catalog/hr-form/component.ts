import {
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import {
  Chart,
  ChartVersion,
  HelmRequest,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
  getYamlApiVersion,
} from 'app/typings';
import { FALSE, K8S_RESOURCE_NAME_BASE, TRUE } from 'app/utils';

import { HelmRequestUtilService } from '../hr.util.service';
import { formatChartName } from '../util';

interface HelmRequestFormModel {
  name?: string;
  displayName?: string;
  chartVersion?: string;
  values?: Record<string, unknown>;
  resourceVersion?: string;
  deployApplication?: boolean;
}

@Component({
  selector: 'rc-helm-request-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmRequestFormComponent extends BaseResourceFormGroupComponent<
  HelmRequest,
  HelmRequestFormModel
> {
  @Input() isUpdate = false;
  @Input() baseParams: WorkspaceBaseParams;
  @Input() chartVersion: string;
  @Input() chartName: string;
  @ObservableInput(true) chartName$: Observable<string>;
  versions$ = this.chartName$.pipe(
    filter(c => !!c),
    switchMap(name =>
      this.k8sApi.getGlobalResource<Chart>({
        type: RESOURCE_TYPES.CHART,
        namespaced: true,
        name,
      }),
    ),
    map(chart => chart.spec.versions || []),
    publishRef(),
  );

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly hrUtil: HelmRequestUtilService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      name: this.fb.control('', [
        Validators.pattern(this.resourceNamePattern.pattern),
      ]),
      displayName: this.fb.control(''),
      chart: this.fb.control(''),
      chartVersion: this.fb.control(''),
      values: this.fb.control(null),
      resourceVersion: this.fb.control(''),
      deployApplication: this.fb.control(''),
    });
  }

  getDefaultFormModel() {
    return {
      chartVersion: this.chartVersion,
    };
  }

  adaptFormModel(formModel: HelmRequestFormModel) {
    const applicationKey = this.k8sUtil.normalizeType(
      'deploy-application',
      'helmrequests',
    );
    const resource: HelmRequest = {
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.HELM_REQUEST),
      kind: 'HelmRequest',
      metadata: {
        name: formModel.name,
        namespace: this.baseParams.namespace,
        annotations: {
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: formModel.displayName,
        },
      },
      spec: {
        chart: formatChartName(this.chartName),
        clusterName: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        values: formModel.values,
        version: formModel.chartVersion,
      },
    };
    if (formModel.resourceVersion) {
      resource.metadata.resourceVersion = formModel.resourceVersion;
    }
    if (formModel.deployApplication) {
      resource.metadata.annotations[applicationKey] = TRUE;
    } else {
      resource.metadata.annotations[applicationKey] = FALSE;
    }
    return resource;
  }

  adaptResourceModel(resource: HelmRequest): HelmRequestFormModel {
    return resource
      ? {
          name: resource.metadata.name,
          displayName: this.k8sUtil.getDisplayName(resource),
          chartVersion: resource.spec.version,
          values: resource.spec.values,
          resourceVersion: resource.metadata.resourceVersion,
          deployApplication: this.hrUtil.isDeployApplicationEnabled(resource),
        }
      : {};
  }

  trackByVersion(_index: number, version: ChartVersion) {
    return version.version;
  }
}
