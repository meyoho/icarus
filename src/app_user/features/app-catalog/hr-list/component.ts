import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, noop } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import {
  HelmRequest,
  HelmRequestStatusColorMapper,
  HelmRequestStatusIconMapper,
  RESOURCE_TYPES,
  WorkspaceListParams,
} from 'app/typings';
import { ACTION, STATUS, getHelmRequestStatus } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { HelmRequestUtilService } from '../hr.util.service';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmRequestListComponent {
  getHelmRequestStatus = getHelmRequestStatus;
  HelmRequestStatusColorMapper = HelmRequestStatusColorMapper;
  HelmRequestStatusIconMapper = HelmRequestStatusIconMapper;

  columns = [NAME, STATUS, 'app_template', 'created_time', ACTION];
  baseParams$ = this.workspaceComponent.baseParams;
  fetchParams$ = combineLatest([
    this.activatedRoute.queryParams,
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([queryParams, params]) => Object.assign({}, queryParams, params)),
  );

  permissions$ = this.baseParams$.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
    publishRef(),
  );

  list = new K8SResourceList<HelmRequest>({
    fetcher: ({ cluster, namespace, ...queryParams }: WorkspaceListParams) => {
      return this.k8sApiService.getResourceList<HelmRequest>({
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster,
        namespace,
        queryParams,
      });
    },
    fetchParams$: this.fetchParams$,
    limit: 20,
    watcher: seed =>
      this.k8sApiService.watchResourceChange(seed, {
        type: RESOURCE_TYPES.HELM_REQUEST,
        cluster: this.workspaceComponent.baseParamsSnapshot.cluster,
        namespace: this.workspaceComponent.baseParamsSnapshot.namespace,
      }),
  });

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApiService: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly util: HelmRequestUtilService,
  ) {}

  update(item: HelmRequest) {
    this.router.navigate(['hr', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(item: HelmRequest) {
    this.util
      .delete(item, this.workspaceComponent.baseParamsSnapshot.cluster)
      .then(() => {
        this.list.delete(item);
      })
      .catch(noop);
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }
}
