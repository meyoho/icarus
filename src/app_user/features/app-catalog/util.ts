import { get } from 'lodash-es';

import { Chart } from 'app/typings';

export function getChartLatestVersion(chart: Chart) {
  return get(chart, ['spec', 'versions', '0', 'version']) || '';
}

export function formatChartName(chartName: string) {
  const [chart, repo] = chartName.split('.');
  return `${repo}/${chart}`;
}

export function getChartName(formattedChartName: string) {
  const [repo, chart] = formattedChartName.split('/');
  return `${chart}.${repo}`;
}
