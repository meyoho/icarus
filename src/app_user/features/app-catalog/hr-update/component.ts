import {
  AsyncDataLoader,
  K8sApiService,
  NAME,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, finalize, map, tap } from 'rxjs/operators';

import { HelmRequest, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { getChartName } from '../util';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelmRequestUpdateComponent {
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  resource: HelmRequest;
  submitting = false;
  baseParams = this.workspaceComponent.baseParamsSnapshot;
  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly location: Location,
  ) {}

  params$ = this.activatedRoute.paramMap
    .pipe(map(params => params.get(NAME)))
    .pipe(
      distinctUntilChanged(),
      map(name => ({ name })),
      publishRef(),
    );

  dataManager = new AsyncDataLoader<{
    resource: HelmRequest;
  }>({
    params$: this.params$,
    fetcher: params => {
      return this.k8sApi
        .getResource<HelmRequest>({
          type: RESOURCE_TYPES.HELM_REQUEST,
          namespace: this.baseParams.namespace,
          cluster: this.baseParams.cluster,
          name: params.name,
        })
        .pipe(
          tap(resource => {
            this.resource = resource;
            this.cdr.markForCheck();
          }),
          map(resource => ({
            resource,
          })),
        );
    },
  });

  getChartName = getChartName;

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }

    this.submitting = true;

    const params = {
      type: RESOURCE_TYPES.HELM_REQUEST,
      resource: this.resource,
      cluster: this.baseParams.cluster,
    };
    this.k8sApi
      .putResource(params)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(this.translate.get('update_success'));
        this.router.navigate(['hr', 'detail', this.resource.metadata.name], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      });
  }

  cancel() {
    this.location.back();
  }

  jumpToListPage = () => {
    this.router.navigate(['hr'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };
}
