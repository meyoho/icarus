import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HelmRequestCreateComponent } from './hr-create/component';
import { HelmRequestDetailComponent } from './hr-detail/component';
import { HelmRequestListComponent } from './hr-list/component';
import { HelmRequestUpdateComponent } from './hr-update/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HelmRequestListComponent,
  },
  {
    path: 'deploy',
    component: HelmRequestCreateComponent,
  },
  {
    path: 'update/:name',
    component: HelmRequestUpdateComponent,
  },
  {
    path: 'detail/:name',
    component: HelmRequestDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HelmRequestRoutingModule {}
