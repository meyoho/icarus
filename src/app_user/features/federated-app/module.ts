import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { WorkloadSharedModule } from 'app/features-shared/workload/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { ConfigMapSharedModule } from '../configmap/shared.module';
import { SecretSharedModule } from '../configsecret/shared.module';

import { CreatePageComponent } from './create/component';
import { DetailPageComponent } from './detail/component';
import { AddConfigComponent } from './internal-shared/add-config/component';
import { AddOverrideComponent } from './internal-shared/add-override/component';
import { AddWorkloadComponent } from './internal-shared/add-workload/component';
import { FormContainerComponent } from './internal-shared/form-container/component';
import { FormComponent } from './internal-shared/form/component';
import { OverrideListComponent } from './internal-shared/override-list/component';
import {
  OverridesDuplicatedValidatorDirective,
  OverridesFormComponent,
} from './internal-shared/overrides-form/component';
import { ResourceListComponent } from './internal-shared/resource-list/component';
import { UpdateConfigComponent } from './internal-shared/update-config/component';
import { UpdateOverrideComponent } from './internal-shared/update-override/component';
import { UpdateWorkloadComponent } from './internal-shared/update-workload/component';
import { ListPageComponent } from './list/component';
import { FederatedAppRoutingModule } from './routing.module';
import { UpdatePageComponent } from './update/component';

@NgModule({
  imports: [
    FederatedAppRoutingModule,
    SharedModule,
    AppSharedModule,
    ImageSharedModule,
    WorkloadSharedModule,
    ConfigMapSharedModule,
    SecretSharedModule,
  ],
  declarations: [
    ListPageComponent,
    CreatePageComponent,
    UpdatePageComponent,
    DetailPageComponent,
    FormComponent,
    FormContainerComponent,
    ResourceListComponent,
    AddWorkloadComponent,
    UpdateWorkloadComponent,
    OverrideListComponent,
    AddOverrideComponent,
    UpdateOverrideComponent,
    OverridesFormComponent,
    AddConfigComponent,
    UpdateConfigComponent,
    OverridesDuplicatedValidatorDirective,
  ],
})
export class FederatedAppModule {}
