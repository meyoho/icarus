import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  KubernetesResource,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { equals, pathOr } from 'ramda';
import { EMPTY, Observable, Subject, combineLatest, from, merge } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  mergeMap,
  scan,
  startWith,
  tap,
} from 'rxjs/operators';

import {
  FederatedAppApiService,
  FederatedAppTemplatesResource,
  SpecificFederatedAppParams,
} from 'app/services/api/federated-app.service';
import { Application, RESOURCE_TYPES } from 'app/typings';
import { STATUS } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { recoverToAppTemplate } from '../update/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPageComponent {
  dataManager: AsyncDataLoader<
    {
      resource: FederatedAppTemplatesResource;
      permissions: Record<string, boolean>;
    },
    SpecificFederatedAppParams
  >;

  baseParams = this.workspace.baseParamsSnapshot;

  params$ = combineLatest([
    this.workspace.baseParams,
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
  ]).pipe(
    map(([{ cluster, namespace }, name]) => ({ name, cluster, namespace })),
    distinctUntilChanged<SpecificFederatedAppParams>(equals),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.FEDERATED_APPLICATION,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    private readonly federatedAppApi: FederatedAppApiService,
    private readonly workspace: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
  ) {
    this.dataManager = new AsyncDataLoader({
      params$: this.params$,
      fetcher: params => {
        return combineLatest([
          this.federatedAppApi.getFederatedAppResource(params),
          this.permissions$,
        ]).pipe(
          map(([resource, permissions]) => ({
            resource: recoverToAppTemplate(resource.spec.components, true),
            permissions,
          })),
        );
      },
    });
  }

  getApplications: (
    r: FederatedAppTemplatesResource,
  ) => Observable<Record<string, Application>> = resource => {
    const name = resource.metadata.name;
    const clusters = pathOr([], [STATUS, 'clusters'], resource).map(
      c => c.name,
    );
    const maxCall = 5;
    const lead = clusters.slice(0, maxCall);
    const rest = clusters.slice(maxCall);
    const request$$ = new Subject<string>();

    return merge(from(lead), request$$).pipe(
      mergeMap(cluster => {
        return this.k8sApi
          .getResource<Application>({
            type: RESOURCE_TYPES.APPLICATION,
            namespace: this.baseParams.namespace,
            cluster,
            name,
          })
          .pipe(
            map(res => ({ [cluster]: res })),
            tap(() => {
              if (rest.length > 0) {
                request$$.next(rest.shift());
              }
            }),
            catchError(() => {
              if (rest.length > 0) {
                request$$.next(rest.shift());
              }
              return EMPTY;
            }),
          );
      }),
      scan(
        (acc, curr) => ({ ...acc, ...curr }),
        {} as Record<string, Application>,
      ),
      startWith(clusters.reduce((acc, curr) => ({ ...acc, [curr]: null }), {})),
      publishRef(),
    );
  };

  jumpToListPage() {
    this.router.navigate(['federated_app'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  update(resource: KubernetesResource) {
    this.router.navigate(['federated_app', 'update', resource.metadata.name], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  delete(resource: KubernetesResource) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_federated_application_confirm', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.FEDERATED_APPLICATION,
              cluster: this.baseParams.cluster,
              resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.jumpToListPage();
      })
      .catch(noop);
  }
}
