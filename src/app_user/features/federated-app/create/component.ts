import { DISPLAY_NAME } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { FederatedAppTemplatesResource } from 'app/services/api/federated-app.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, RESOURCE_DEFINITIONS } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

const federatedAppDefinition = RESOURCE_DEFINITIONS.FEDERATED_APPLICATION;
const appDefinition = RESOURCE_DEFINITIONS.APPLICATION;

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreatePageComponent {
  private readonly namespace = this.workspace.baseParamsSnapshot.namespace;

  initResource: FederatedAppTemplatesResource = {
    kind: 'FederatedApplication',
    apiVersion: `${federatedAppDefinition.apiGroup}/${federatedAppDefinition.apiVersion}`,
    metadata: {
      name: '',
      namespace: this.namespace,
      annotations: {
        [`${this.env.LABEL_BASE_DOMAIN}/${DISPLAY_NAME}`]: '',
      },
    },
    spec: {
      template: {
        kind: 'Application',
        apiVersion: `${appDefinition.apiGroup}/${appDefinition.apiVersion}`,
        metadata: {
          name: '',
          namespace: this.namespace,
        },
        spec: {
          componentTemplates: [],
        },
      },
      overrides: [],
      placement: {
        clusterSelector: {},
      },
    },
  };

  constructor(
    private readonly workspace: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {}
}
