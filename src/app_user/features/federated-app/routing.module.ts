import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreatePageComponent } from './create/component';
import { DetailPageComponent } from './detail/component';
import { ListPageComponent } from './list/component';
import { UpdatePageComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ListPageComponent,
  },
  {
    path: 'create',
    component: CreatePageComponent,
  },
  {
    path: 'update/:name',
    component: UpdatePageComponent,
  },
  {
    path: 'detail/:name',
    component: DetailPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class FederatedAppRoutingModule {}
