import { DISPLAY_NAME, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import { CommonFormControl } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator,
} from '@angular/forms';
import { identity } from 'ramda';

import {
  ClusterOverride,
  ClusterOverrideOp,
  FederatedAppOverride,
} from 'app/services/api/federated-app.service';
import { Workload } from 'app/typings';

export const CONTAINER_INDEX_PLACEMENT = '~~container-index~~';
export const CONTAINER_OVERRIDE_PREFIX = '/spec/template/spec/containers';

@Component({
  selector: 'rc-fa-overrides-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OverridesFormComponent),
      multi: true,
    },
  ],
})
export class OverridesFormComponent extends CommonFormControl<FlatOverride[]> {
  @Input()
  clusters: string[] = [];

  @Input()
  resourceKind: string;

  paramsByKind: Record<string, OverrideParam[]>;
  paramsByPath: Record<string, OverrideParam>;

  constructor(
    @Inject(TOKEN_BASE_DOMAIN) baseDomain: string,
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);
    const overrideParams = getOverrideParams({ baseDomain });
    const workloadParams = overrideParams.filter(v => v.type === 'workload');
    this.paramsByKind = {
      FederatedApplication: overrideParams.filter(
        v => v.type === 'application',
      ),
      Deployment: workloadParams,
      StatefulSet: workloadParams,
    };

    this.paramsByPath = overrideParams.reduce((acc, curr) => {
      return { ...acc, [curr.path]: curr };
    }, {} as Record<string, OverrideParam>);
  }

  uiValue = (value: any, path: string) => {
    if (this.paramsByPath[path]) {
      const v = this.paramsByPath[path].uiValue(value);
      return v ?? '';
    } else {
      return value;
    }
  };

  formatValue = (value: any, path: string) => {
    if (this.paramsByPath[path]) {
      return this.paramsByPath[path].formatValue(value);
    } else {
      return value;
    }
  };

  writeValue(value: FlatOverride[]) {
    this.value$$.next(value || []);
  }

  add() {
    this.emitValueChange(
      this.snapshot.value.concat({
        cluster: '',
        op: ClusterOverrideOp.Replace,
        path: '',
        value: '',
      }),
    );
  }

  remove(index: number) {
    this.emitValueChange(this.snapshot.value.filter((_, i) => i !== index));
  }

  updateItem(override: FlatOverride, index: number) {
    this.emitValueChange(
      this.snapshot.value.map((v, i) => {
        if (i !== index) {
          return v;
        } else {
          return { ...v, ...override };
        }
      }),
    );
  }

  trackByPath(item: FlatOverride) {
    return item.path;
  }
}

@Directive({
  selector:
    // tslint:disable-next-line: directive-selector
    'rc-fa-overrides-form[ngModel][duplicated],rc-fa-overrides-form[formControl][duplicated],rc-fa-overrides-form[formControlName][duplicated]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => OverridesDuplicatedValidatorDirective),
      multi: true,
    },
  ],
})
export class OverridesDuplicatedValidatorDirective implements Validator {
  validate(control: AbstractControl) {
    const overrides: FlatOverride[] = control.value;

    const duplicated = (overrides ?? []).filter((item, _, arr) => {
      return (
        item.cluster &&
        item.path &&
        arr.filter(v => {
          return v.cluster === item.cluster && v.path === item.path;
        }).length > 1
      );
    });

    return duplicated.length === 0
      ? null
      : {
          duplicated,
        };
  }
}

export interface FlatOverride extends ClusterOverride {
  cluster: string;
}

export function flatOverrides(value: FederatedAppOverride[]): FlatOverride[] {
  return value.reduce((acc, { clusterName, clusterOverrides }) => {
    const flatten = clusterOverrides
      .filter(
        ({ path }) =>
          !path.startsWith(CONTAINER_OVERRIDE_PREFIX) ||
          path.startsWith(`${CONTAINER_OVERRIDE_PREFIX}/0/`),
      )
      .map(item => {
        const path = item.path.startsWith(`${CONTAINER_OVERRIDE_PREFIX}`)
          ? item.path.replace(
              `${CONTAINER_OVERRIDE_PREFIX}/0/`,
              `${CONTAINER_OVERRIDE_PREFIX}/${CONTAINER_INDEX_PLACEMENT}/`,
            )
          : item.path;
        return {
          ...item,
          cluster: clusterName,
          path,
        };
      });
    return acc.concat(flatten);
  }, []);
}

export function wrapOverrides(value: FlatOverride[]) {
  return value.reduce((acc, { cluster, op, path, value }) => {
    if (!cluster || !path) {
      return acc;
    }
    const exist = acc.find(item => item.clusterName === cluster);
    const override = { op, path, value };
    if (exist) {
      exist.clusterOverrides = exist.clusterOverrides.concat(override);
      return acc;
    } else {
      return acc.concat({
        clusterName: cluster,
        clusterOverrides: [override],
      });
    }
  }, [] as FederatedAppOverride[]);
}

export function fillContainerPlacement(
  resource: Workload,
  overrides: FlatOverride[],
) {
  const num = resource.spec.template.spec.containers.length;
  return overrides.reduce((acc, curr) => {
    if (!curr.path.includes(CONTAINER_INDEX_PLACEMENT)) {
      return acc.concat(curr);
    } else {
      const expanded = new Array(num)
        .fill(curr)
        .map((item: FlatOverride, index) => ({
          ...item,
          path: item.path.replace(CONTAINER_INDEX_PLACEMENT, `${index}`),
        }));
      return acc.concat(expanded);
    }
  }, [] as FlatOverride[]);
}

export interface OverrideParam {
  type: string;
  label: string;
  op: ClusterOverrideOp;
  path: string;
  valueType: string;
  uiValue: (v: any) => any;
  formatValue: (v: any) => any;
}

export function getOverrideParams({
  baseDomain,
}: {
  baseDomain: string;
}): OverrideParam[] {
  return [
    {
      type: 'application',
      label: 'display_name',
      op: ClusterOverrideOp.Add,
      path: `/metadata/annotations`,
      valueType: 'text',
      formatValue: (v: string) => ({ [`${baseDomain}/${DISPLAY_NAME}`]: v }),
      uiValue: (v: Record<string, string>) =>
        v[`${baseDomain}/${DISPLAY_NAME}`],
    },
    {
      type: 'workload',
      label: 'service_replicas',
      op: ClusterOverrideOp.Replace,
      path: '/spec/replicas',
      valueType: 'number',
      formatValue: (v: string) => {
        const p = parseInt(v);
        return isNaN(p) ? 0 : p;
      },
      uiValue: identity,
    },
    {
      type: 'workload',
      label: 'resource_limits_cpu',
      op: ClusterOverrideOp.Replace,
      path: `${CONTAINER_OVERRIDE_PREFIX}/${CONTAINER_INDEX_PLACEMENT}/resources/limits/cpu`,
      valueType: 'text',
      formatValue: identity,
      uiValue: identity,
    },
    {
      type: 'workload',
      label: 'resource_limits_mem',
      op: ClusterOverrideOp.Replace,
      path: `${CONTAINER_OVERRIDE_PREFIX}/${CONTAINER_INDEX_PLACEMENT}/resources/limits/memory`,
      valueType: 'text',
      formatValue: identity,
      uiValue: identity,
    },
  ];
}
