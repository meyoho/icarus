import { DISPLAY_NAME } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { FederatedAppTemplatesResource } from 'app/services/api/federated-app.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

@Component({
  selector: 'rc-federated-app-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent extends BaseResourceFormGroupComponent<
  FederatedAppTemplatesResource,
  FederatedAppTemplatesResource
> {
  @Input()
  isUpdate = false;

  @Input()
  cluster: string;

  @Input()
  namespace: string;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  displayNameKey = `${this.env.LABEL_BASE_DOMAIN}/${DISPLAY_NAME}`;

  constructor(
    injector: Injector,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control('', [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ]),
        annotations: this.fb.group({
          [this.displayNameKey]: this.fb.control(''),
        }),
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }
}
