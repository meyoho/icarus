import { TOKEN_BASE_DOMAIN, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { POD_SPEC_FORM_CONFIG } from 'app/features-shared/app/tokens';
import { ConfigMap, Secret, Workload } from 'app/typings';

import { podSpecFormConfig } from '../../util';
import { initMatchLabels } from '../add-workload/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../add-workload/styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: POD_SPEC_FORM_CONFIG,
      useValue: podSpecFormConfig,
    },
  ],
})
export class UpdateWorkloadComponent {
  @ViewChild('resourceForm', { static: false })
  private readonly ngForm: NgForm;

  workloadType: string;
  resource: Workload;

  cluster: string;
  namespace: string;
  project: string;
  existingNames: string[];
  otherWorkloads: Workload[];
  configMapOptions: ConfigMap[];
  secretOptions: Secret[];
  hasBeCreated: boolean;

  constructor(
    @Inject(DIALOG_DATA) data: UpdateWorkloadDialogData,
    private readonly dialogRef: DialogRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    this.resource = data.resource;
    this.workloadType = data.resource.kind.toUpperCase();
    this.cluster = data.baseParams.cluster;
    this.namespace = data.baseParams.namespace;
    this.project = data.baseParams.project;
    this.existingNames = data.existingNames;
    this.otherWorkloads = data.otherWorkloads;
    this.configMapOptions = data.configMapOptions;
    this.secretOptions = data.secretOptions;
    this.hasBeCreated = data.hasBeCreated;
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    if (this.existingNames.includes(this.resource.metadata.name)) {
      this.message.error(this.translate.get('name_existed'));
      return;
    }

    this.dialogRef.close(
      this.hasBeCreated
        ? this.resource
        : initMatchLabels(this.baseDomain, this.resource),
    );
  }
}

export interface UpdateWorkloadDialogData {
  resource: Workload;
  baseParams: {
    cluster: string;
    namespace: string;
    project: string;
  };
  existingNames: string[];
  otherWorkloads: Workload[];
  configMapOptions: ConfigMap[];
  secretOptions: Secret[];
  hasBeCreated: boolean;
}
