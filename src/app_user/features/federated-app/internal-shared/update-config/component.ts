import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ConfigMap, Secret } from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateConfigComponent {
  @ViewChild('resourceForm', { static: false })
  private readonly ngForm: NgForm;

  type: string;
  resource: ConfigMap | Secret;
  hasBeCreated: boolean;

  constructor(
    @Inject(DIALOG_DATA)
    { resource, hasBeCreated }: UpdateConfigDialogData,
    private readonly dialogRef: DialogRef,
  ) {
    this.type = resource.kind.toLowerCase();
    this.resource = resource;
    this.hasBeCreated = hasBeCreated;
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    this.dialogRef.close(this.resource);
  }
}

export interface UpdateConfigDialogData {
  resource: Secret | ConfigMap;
  hasBeCreated: boolean;
}
