import {
  K8sApiService,
  KubernetesResource,
  TOKEN_BASE_DOMAIN,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { FederatedAppComponent } from 'app/services/api/federated-app.service';
import { WorkspaceBaseParams } from 'app/typings';

import { getFederatedClusters$ } from '../add-override/component';
import {
  FlatOverride,
  fillContainerPlacement,
  flatOverrides,
  wrapOverrides,
} from '../overrides-form/component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateOverrideComponent {
  @ViewChild('ngForm', { static: false })
  private readonly ngForm: NgForm;

  overrides: FlatOverride[] = [];

  resource: KubernetesResource;

  clusters$: Observable<string[]>;

  constructor(
    @Inject(DIALOG_DATA) data: UpdateOverrideDialogData,
    private readonly dialogRef: DialogRef,
    k8sApi: K8sApiService,
    @Inject(TOKEN_BASE_DOMAIN) baseDomain: string,
  ) {
    this.resource = data.component.resource;
    this.overrides = flatOverrides(data.component.overrides);

    this.clusters$ = getFederatedClusters$(
      k8sApi,
      data.baseParams.cluster,
      baseDomain,
    );
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    if (this.resource.kind === 'FederatedApplication') {
      this.dialogRef.close({
        resource: this.resource,
        overrides: wrapOverrides(this.overrides),
      });
    } else {
      this.dialogRef.close({
        resource: this.resource,
        overrides: wrapOverrides(
          fillContainerPlacement(this.resource, this.overrides),
        ),
      });
    }
  }
}

export interface UpdateOverrideDialogData {
  component: FederatedAppComponent;
  baseParams: WorkspaceBaseParams;
}
