import {
  KubernetesResource,
  LABELS,
  METADATA,
  NAME,
  TOKEN_BASE_DOMAIN,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
} from '@angular/core';
import { Router } from '@angular/router';
import { assocPath, compose, converge, identity, pathOr } from 'ramda';
import { first, switchMap } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { AddServiceFormDialogComponent } from 'app/features-shared/app/dialog/add-service/dialog';
import { PodControllerKinds } from 'app/features-shared/app/utils';
import {
  FederatedAppApiService,
  FederatedAppComponent,
  FederatedAppOverride,
  FederatedAppTemplatesResource,
} from 'app/services/api/federated-app.service';
import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import { RESOURCE_TYPES, Service, Workload, WorkloadType } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import {
  AddConfigComponent,
  AddConfigDialogData,
} from '../add-config/component';
import {
  AddOverrideComponent,
  AddOverrideDialogData,
} from '../add-override/component';
import {
  AddWorkloadComponent,
  AddWorkloadDialogData,
} from '../add-workload/component';
import {
  UpdateConfigComponent,
  UpdateConfigDialogData,
} from '../update-config/component';
import {
  UpdateOverrideComponent,
  UpdateOverrideDialogData,
} from '../update-override/component';
import {
  UpdateWorkloadComponent,
  UpdateWorkloadDialogData,
} from '../update-workload/component';

const coverPath = (from: string[], path: string[]) =>
  converge(assocPath(path), [pathOr('', from), identity]);

@Component({
  selector: 'rc-federated-app-form-container',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormContainerComponent extends BaseFormContainer<
  FederatedAppTemplatesResource
> {
  baseParams = this.workspace.baseParamsSnapshot;
  private readonly templatesPath = [
    'spec',
    'template',
    'spec',
    'componentTemplates',
  ];

  private readonly copyName = coverPath(
    [METADATA, NAME],
    ['spec', 'template', METADATA, NAME],
  );

  private readonly setPodLabels = (resource: FederatedAppTemplatesResource) => {
    const templatesPath = ['spec', 'template', 'spec', 'componentTemplates'];
    const podPath = ['resource', 'spec', 'template'];
    const templates = pathOr([], templatesPath, resource).map(
      (component: FederatedAppComponent) => {
        if (!pathOr(null, podPath, component)) {
          return component;
        } else {
          return assocPath(
            [...podPath, METADATA, LABELS, `app.${this.baseDomain}/${NAME}`],
            `${resource.metadata.name}.${resource.metadata.namespace}`,
            component,
          );
        }
      },
    );
    return assocPath(templatesPath, templates, resource);
  };

  protected fillResource = compose(this.setPodLabels, this.copyName);

  constructor(
    injector: Injector,
    private readonly workspace: WorkspaceComponent,
    private readonly federatedAppAPi: FederatedAppApiService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly appSharedService: AppSharedService,
    private readonly dialog: DialogService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  submit() {
    if (!this.checkForm()) {
      return;
    }

    this.submitting = true;

    (this.isUpdate
      ? this.federatedAppAPi.putFederatedApp(
          {
            cluster: this.baseParams.cluster,
            namespace: this.baseParams.namespace,
            name: this.resource.metadata.name,
          },
          this.resource,
        )
      : this.federatedAppAPi.postFederatedApp(
          {
            cluster: this.baseParams.cluster,
            namespace: this.baseParams.namespace,
          },
          this.resource,
        )
    ).subscribe(
      () => {
        this.router.navigate(
          ['federated_app', 'detail', this.resource.metadata.name],
          {
            relativeTo: this.workspace.baseActivatedRoute,
            replaceUrl: true,
          },
        );
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  addComponent(type: string) {
    switch (type) {
      case 'deployment':
        this.addWorkload(RESOURCE_TYPES.DEPLOYMENT);
        break;
      case 'statefulset':
        this.addWorkload(RESOURCE_TYPES.STATEFULSET);
        break;
      case 'service':
        this.addService();
        break;
      case 'configmap':
        this.addConfig('ConfigMap');
        break;
      case 'secret':
        this.addConfig('Secret');
        break;
    }
  }

  updateComponent(target: KubernetesResource) {
    switch (target.kind.toLowerCase()) {
      case 'deployment':
      case 'statefulset':
        this.updateWorkload(target);
        break;
      case 'service':
        this.updateService(target);
        break;
      case 'configmap':
      case 'secret':
        this.updateConfig(target);
        break;
    }
  }

  deleteComponent(target: KubernetesResource) {
    this.replaceComponents(
      this.getComponents().filter(
        ({ resource }) =>
          resource.kind !== target.kind ||
          resource.metadata.name !== target.metadata.name,
      ),
    );
  }

  addOverride() {
    this.dialog
      .open<AddOverrideComponent, AddOverrideDialogData>(AddOverrideComponent, {
        size: DialogSize.Large,
        data: {
          resource: this.form.get('resource').value,
          baseParams: this.baseParams,
        },
      })
      .afterClosed()
      .subscribe(
        (result: {
          resource: KubernetesResource;
          overrides: FederatedAppOverride[];
        }) => {
          if (result) {
            this.replaceOverrides(result);
            this.cdr.markForCheck();
          }
        },
      );
  }

  updateOverride(component: FederatedAppComponent) {
    this.dialog
      .open<UpdateOverrideComponent, UpdateOverrideDialogData>(
        UpdateOverrideComponent,
        {
          size: DialogSize.Large,
          data: {
            component,
            baseParams: this.baseParams,
          },
        },
      )
      .afterClosed()
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(result => {
        if (result) {
          this.replaceOverrides(result);
          this.cdr.markForCheck();
        }
      });
  }

  deleteOverride(component: FederatedAppComponent) {
    this.replaceOverrides({ resource: component.resource, overrides: [] });
  }

  private addWorkload(workloadType: WorkloadType) {
    if (!this.checkForm()) {
      return;
    }
    this.appSharedService
      .selectImage(this.baseParams)
      .pipe(
        switchMap(imageSelection => {
          return this.dialog
            .open<AddWorkloadComponent, AddWorkloadDialogData>(
              AddWorkloadComponent,
              {
                size: DialogSize.Large,
                data: {
                  workloadType,
                  imageSelection,
                  baseParams: {
                    ...this.baseParams,
                    appName: this.resource.metadata.name,
                  },
                  existingNames: this.getComponentNamesByKind(workloadType),
                  otherWorkloads: this.getWorkloads(),
                  configMapOptions: this.getResourcesByKind('ConfigMap'),
                  secretOptions: this.getResourcesByKind('Secret'),
                },
              },
            )
            .afterClosed();
        }),
      )
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(result => {
        if (result) {
          this.unshiftComponent(result);
          this.cdr.markForCheck();
        }
      });
  }

  private updateWorkload(resource: KubernetesResource) {
    this.dialog
      .open<UpdateWorkloadComponent, UpdateWorkloadDialogData>(
        UpdateWorkloadComponent,
        {
          size: DialogSize.Large,
          data: {
            resource,
            baseParams: this.baseParams,
            existingNames: this.getComponentNamesByKind(
              resource.kind,
              resource.metadata.name,
            ),
            otherWorkloads: this.getWorkloads().filter(
              r => r.metadata.name !== resource.metadata.name,
            ),
            configMapOptions: this.getResourcesByKind('ConfigMap'),
            secretOptions: this.getResourcesByKind('Secret'),
            hasBeCreated: this.isUpdate,
          },
        },
      )
      .afterClosed()
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(result => {
        if (result) {
          this.replaceComponent(resource.metadata.name, result);
          this.cdr.markForCheck();
        }
      });
  }

  private addService() {
    const dialogRef = this.dialog.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        namespace: this.baseParams.namespace,
        workloads: this.getWorkloads(),
        existedNames: this.getResourcesByKind('Service').map(
          c => c.metadata.name,
        ),
        disableHeadless: true,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((result: Service) => {
        dialogRef.close();
        if (result) {
          this.unshiftComponent(result);
          this.cdr.markForCheck();
        }
      });
  }

  private updateService(service: Service) {
    const dialogRef = this.dialog.open(AddServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...service },
        namespace: this.baseParams.namespace,
        workloads: this.getWorkloads(),
        existedNames: this.getResourcesByKind('Service')
          .filter(r => r.metadata.name !== service.metadata.name)
          .map(c => c.metadata.name),
        isUpdateApp: this.isUpdate,
        disableHeadless: true,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((result: Service) => {
        dialogRef.close();
        if (result) {
          this.replaceComponent(service.metadata.name, result);
          this.cdr.markForCheck();
        }
      });
  }

  private addConfig(kind: string) {
    this.dialog
      .open<AddConfigComponent, AddConfigDialogData>(AddConfigComponent, {
        size: DialogSize.Large,
        data: {
          baseParams: this.baseParams,
          kind,
        },
      })
      .afterClosed()
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(result => {
        if (result) {
          this.unshiftComponent(result);
          this.cdr.markForCheck();
        }
      });
  }

  private updateConfig(resource: KubernetesResource) {
    this.dialog
      .open<UpdateConfigComponent, UpdateConfigDialogData>(
        UpdateConfigComponent,
        {
          size: DialogSize.Large,
          data: { resource, hasBeCreated: this.isUpdate },
        },
      )
      .afterClosed()
      // eslint-disable-next-line sonarjs/no-identical-functions
      .subscribe(result => {
        if (result) {
          this.replaceComponent(resource.metadata.name, result);
          this.cdr.markForCheck();
        }
      });
  }

  private getWorkloads() {
    return this.getComponents()
      .filter(c => PodControllerKinds.includes(c.resource.kind))
      .map(c => c.resource) as Workload[];
  }

  private getResourcesByKind(kind: 'Service' | 'ConfigMap' | 'Secret') {
    return this.getComponents()
      .filter(c => c.resource.kind === kind)
      .map(c => c.resource);
  }

  private unshiftComponent(resource: KubernetesResource) {
    const component: FederatedAppComponent = { resource, overrides: [] };
    this.replaceComponents([component].concat(this.getComponents()));
  }

  private replaceComponent(target: string, resource: KubernetesResource) {
    const targetKind = resource.kind;
    const newComponents = this.getComponents().map(component => {
      const kind = component.resource.kind;
      const name = component.resource.metadata.name;
      if (kind === targetKind && name === target) {
        return {
          resource,
          overrides: [],
        };
      } else {
        return component;
      }
    });
    this.replaceComponents(newComponents);
  }

  private getComponents(): FederatedAppComponent[] {
    return pathOr([], this.templatesPath, this.form.get('resource').value);
  }

  private getComponentNamesByKind(kind: string, exclude = '') {
    return this.getComponents()
      .filter(
        ({ resource }) =>
          resource.kind.toLowerCase() === kind.toLowerCase() &&
          resource.metadata.name !== exclude,
      )
      .map(({ resource }) => resource.metadata.name);
  }

  private replaceComponents(templates: FederatedAppComponent[]) {
    this.updateResource(
      assocPath(this.templatesPath, templates, this.form.get('resource').value),
    );
  }

  private replaceOverrides(component: FederatedAppComponent) {
    if (component.resource.kind === 'FederatedApplication') {
      this.updateResource(
        assocPath(
          ['spec', 'overrides'],
          component.overrides,
          this.form.get('resource').value,
        ),
      );
    } else {
      this.replaceComponentOverrides(component.resource, component.overrides);
    }
  }

  private findComponentIndex(target: KubernetesResource) {
    return this.getComponents().findIndex(
      ({ resource }) =>
        resource.kind === target.kind &&
        resource.metadata.name === target.metadata.name,
    );
  }

  private replaceComponentOverrides(
    resource: KubernetesResource,
    overrides: FederatedAppOverride[],
  ) {
    this.updateResource(
      assocPath(
        [
          'spec',
          'template',
          'spec',
          'componentTemplates',
          this.findComponentIndex(resource),
          'overrides',
        ],
        overrides,
        this.form.get('resource').value,
      ),
    );
  }
}
