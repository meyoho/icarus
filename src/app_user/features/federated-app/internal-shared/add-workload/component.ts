import {
  LABELS,
  METADATA,
  NAME,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { assocPath } from 'ramda';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { AppSharedService } from 'app/features-shared/app/app-shared.service';
import { POD_SPEC_FORM_CONFIG } from 'app/features-shared/app/tokens';
import { RcImageSelection } from 'app/features-shared/image/image.type';
import {
  copyMatchLabels,
  getWorkloadKind,
} from 'app/features-shared/workload/util';
import {
  ConfigMap,
  Deployment,
  Secret,
  Workload,
  WorkloadType,
} from 'app/typings';
import { getDefaultPodController } from 'app_user/features/app/util';

import { podSpecFormConfig } from '../../util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: POD_SPEC_FORM_CONFIG,
      useValue: podSpecFormConfig,
    },
  ],
})
export class AddWorkloadComponent {
  @ViewChild('resourceForm', { static: false })
  private readonly ngForm: NgForm;

  workloadType: WorkloadType;
  resource: Deployment;

  cluster: string;
  namespace: string;
  project: string;
  appName: string;
  existingNames: string[];
  otherWorkloads: Workload[];
  configMapOptions: ConfigMap[];
  secretOptions: Secret[];

  constructor(
    @Inject(DIALOG_DATA)
    data: AddWorkloadDialogData,
    private readonly appSharedService: AppSharedService,
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogRef: DialogRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    const { baseParams, imageSelection } = data;
    this.cluster = baseParams.cluster;
    this.namespace = baseParams.namespace;
    this.project = baseParams.project;
    this.appName = baseParams.appName;
    this.workloadType = data.workloadType;
    this.existingNames = data.existingNames;
    this.otherWorkloads = data.otherWorkloads;
    this.configMapOptions = data.configMapOptions;
    this.secretOptions = data.secretOptions;

    this.appSharedService
      .getDefaultLimitResources({
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
      })
      .pipe(
        catchError(() => of({})),
        map(defaultLimit => {
          return getDefaultPodController({
            imageSelectionParams: imageSelection,
            namespace: this.namespace,
            defaultLimit,
            kind: getWorkloadKind(this.workloadType),
          });
        }),
      )
      .subscribe(res => {
        this.resource = res;
        this.cdr.markForCheck();
      });
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    const name = this.appName
      ? `${this.appName}-${this.resource.metadata.name}`
      : this.resource.metadata.name;

    if (this.existingNames.includes(name)) {
      this.message.error(this.translate.get('name_existed'));
      return;
    }

    this.dialogRef.close(
      initMatchLabels(
        this.baseDomain,
        assocPath([METADATA, NAME], name, this.resource),
      ),
    );
  }
}

export interface AddWorkloadDialogData {
  imageSelection: RcImageSelection;
  baseParams: {
    cluster: string;
    namespace: string;
    project: string;
    appName: string;
  };
  existingNames: string[];
  workloadType: WorkloadType;
  otherWorkloads: Workload[];
  configMapOptions: ConfigMap[];
  secretOptions: Secret[];
}

export function initMatchLabels(baseDomain: string, resource: Workload) {
  const key = `service.${baseDomain}/name`;
  return copyMatchLabels(baseDomain)(
    assocPath(
      ['spec', 'template', METADATA, LABELS, key],
      resource.kind.toLowerCase() + '-' + resource.metadata.name,
      resource,
    ),
  );
}
