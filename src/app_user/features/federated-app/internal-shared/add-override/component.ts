import { K8sApiService, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

import {
  FederatedAppComponent,
  FederatedAppTemplatesResource,
} from 'app/services/api/federated-app.service';
import {
  Cluster,
  ClusterFed,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';

import {
  FlatOverride,
  fillContainerPlacement,
  flatOverrides,
  wrapOverrides,
} from '../overrides-form/component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddOverrideComponent {
  @ViewChild('ngForm', { static: false })
  private readonly ngForm: NgForm;

  resourceKinds = ['FederatedApplication', 'Deployment', 'StatefulSet'];
  resourceKind = 'FederatedApplication';

  resource: FederatedAppTemplatesResource;

  components: FederatedAppComponent[];
  component: FederatedAppComponent;

  overrides: FlatOverride[] = [];

  clusters$: Observable<string[]>;

  constructor(
    @Inject(DIALOG_DATA) data: AddOverrideDialogData,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly dialogRef: DialogRef,
    k8sApi: K8sApiService,
  ) {
    this.components = data.resource.spec.template.spec.componentTemplates;
    this.resource = data.resource;
    this.overrides = flatOverrides(this.resource.spec.overrides);

    this.clusters$ = getFederatedClusters$(
      k8sApi,
      data.baseParams.cluster,
      this.baseDomain,
    );
  }

  filterComponents(components: FederatedAppComponent[], resourceKind: string) {
    return components.filter(({ resource }) => resource.kind === resourceKind);
  }

  resourceKindChanged(type: string) {
    this.component = null;
    if (type === 'FederatedApplication') {
      this.overrides = flatOverrides(this.resource.spec.overrides || []);
    } else {
      this.overrides = [];
    }
  }

  componentChange(component: FederatedAppComponent) {
    this.overrides = flatOverrides(component.overrides || []);
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    if (this.resourceKind === 'FederatedApplication') {
      this.dialogRef.close({
        resource: this.resource,
        overrides: wrapOverrides(this.overrides),
      });
    } else {
      this.dialogRef.close({
        resource: this.component.resource,
        overrides: wrapOverrides(
          fillContainerPlacement(this.component.resource, this.overrides),
        ),
      });
    }
  }
}

export interface AddOverrideDialogData {
  resource: FederatedAppTemplatesResource;
  baseParams: WorkspaceBaseParams;
}

export function getFederatedClusters$(
  k8sApi: K8sApiService,
  name: string,
  baseDomain: string,
) {
  return k8sApi
    .getGlobalResource<Cluster>({
      type: RESOURCE_TYPES.CLUSTER,
      namespaced: true,
      name,
    })
    .pipe(
      switchMap(cluster => {
        const fedCluster =
          cluster.metadata.labels[`federation.${baseDomain}/name`];
        return k8sApi.getGlobalResource<ClusterFed>({
          type: RESOURCE_TYPES.CLUSTER_FED,
          name: fedCluster,
        });
      }),
      map(fed => fed.spec.clusters.map(item => item.name)),
      catchError(() => of([])),
      publishReplay(1),
      refCount(),
    );
}
