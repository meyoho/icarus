import { DESCRIPTION } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  ConfigMap,
  Environments,
  Secret,
  SecretType,
  WorkspaceBaseParams,
} from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddConfigComponent {
  @ViewChild('resourceForm', { static: false })
  private readonly ngForm: NgForm;

  type: string;
  resource: ConfigMap | Secret;

  constructor(
    @Inject(DIALOG_DATA)
    data: AddConfigDialogData,
    private readonly dialogRef: DialogRef,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    this.type = data.kind.toLowerCase();
    this.resource = {
      apiVersion: 'v1',
      kind: data.kind,
      metadata: {
        name: '',
        namespace: data.baseParams.namespace,
        annotations: {
          [`${this.env.LABEL_BASE_DOMAIN}/${DESCRIPTION}`]: '',
        },
      },
      data: {},
    };
    if (data.kind === 'Secret') {
      (this.resource as Secret).type = SecretType.Opaque;
    }
  }

  confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    this.dialogRef.close(this.resource);
  }
}

export interface AddConfigDialogData {
  baseParams: WorkspaceBaseParams;
  kind: string;
}
