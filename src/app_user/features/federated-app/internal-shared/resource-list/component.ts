import { KubernetesResource, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { get } from 'lodash-es';

import { FederatedAppComponent } from 'app/services/api/federated-app.service';
import { Service } from 'app/typings';
import { SecretUtilService } from 'app_user/features/configsecret/util.service';

@Component({
  selector: 'rc-fa-resource-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListComponent {
  @Input()
  editable = false;

  @Input()
  components: FederatedAppComponent[] = [];

  @Output()
  add = new EventEmitter<string>();

  @Output()
  update = new EventEmitter<KubernetesResource>();

  @Output()
  delete = new EventEmitter<KubernetesResource>();

  getSecretDisplayType = (value: string) => {
    if (value) {
      return this.secretUtil.translateSecretType(value);
    } else {
      return '';
    }
  };

  constructor(
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly secretUtil: SecretUtilService,
  ) {}

  filterResource = (
    components: FederatedAppComponent[],
    category: 'workload' | 'service' | 'config',
  ) => {
    switch (category) {
      case 'workload':
        return this.filterByKind(components, ['Deployment', 'StatefulSet']);
      case 'service':
        return this.filterByKind(components, ['Service']);
      case 'config':
        return this.filterByKind(components, ['ConfigMap', 'Secret']);
    }
  };

  getTargetWorkloadSelector = (service: Service) => {
    return get(
      service,
      ['spec', 'selector', `service.${this.baseDomain}/name`],
      '',
    );
  };

  getTargetWorkloadName(selector: string) {
    return selector
      ? selector.slice(Math.max(0, selector.indexOf('-') + 1))
      : '';
  }

  getTargetWorkloadType(selector: string) {
    return selector ? selector.split('-')[0] : '';
  }

  private filterByKind(components: FederatedAppComponent[], kinds: string[]) {
    return components
      .filter(item => kinds.includes(item.resource.kind))
      .map(item => item.resource)
      .sort((a, b) => a.kind.localeCompare(b.kind));
  }
}
