import { DISPLAY_NAME, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { safeDump } from 'js-yaml';

import { FederatedAppTemplatesResource } from 'app/services/api/federated-app.service';

import {
  OverrideParam,
  flatOverrides,
  getOverrideParams,
} from '../overrides-form/component';

@Component({
  selector: 'rc-fa-override-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverrideListComponent {
  @Input() editable = false;
  @Input() resource: FederatedAppTemplatesResource;

  @Output() add = new EventEmitter<void>();
  @Output() update = new EventEmitter<void>();
  @Output() delete = new EventEmitter<void>();

  flatOverrides = flatOverrides;

  pathList: OverrideParam[];

  displayNameKey: string;

  constructor(@Inject(TOKEN_BASE_DOMAIN) baseDomain: string) {
    this.pathList = getOverrideParams({ baseDomain });
    this.displayNameKey = `${baseDomain}/${DISPLAY_NAME}`;
  }

  get hasOverrides() {
    return (
      (this.resource.spec.overrides && this.resource.spec.overrides.length) ||
      this.resource.spec.template.spec.componentTemplates.reduce(
        (acc, curr) => {
          return acc + ((curr.overrides && curr.overrides.length) || 0);
        },
        0,
      )
    );
  }

  pathToParam = (path: string) => {
    const param = this.pathList.find(v => v.path === path);

    if (param) {
      return param.label;
    } else {
      return path;
    }
  };

  isPlain(v: unknown) {
    return !(v !== null && typeof v === 'object');
  }

  getYaml(v: unknown) {
    return safeDump(v).trim();
  }
}
