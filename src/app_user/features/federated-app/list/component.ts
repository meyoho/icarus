import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  ResourceListParams,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FederatedApp } from 'app/services/api/federated-app.service';
import { FederationUtilService } from 'app/services/federation-util.service';
import { RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {
  baseParams = this.workspace.baseParamsSnapshot;
  list: K8SResourceList<FederatedApp>;

  isMasterFedContext$ = this.fedUtil.isMasterFedContext(
    this.baseParams.cluster,
    this.baseParams.namespace,
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.FEDERATED_APPLICATION,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly workspace: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly fedUtil: FederationUtilService,
  ) {
    this.list = new K8SResourceList({
      fetcher: (queryParams: ResourceListParams) =>
        this.k8sApi.getResourceList({
          type: RESOURCE_TYPES.FEDERATED_APPLICATION,
          cluster: workspace.baseParamsSnapshot.cluster,
          namespace: workspace.baseParamsSnapshot.namespace,
          queryParams,
        }),
      activatedRoute,
    });
  }

  create() {
    this.router.navigate(['create'], {
      relativeTo: this.activatedRoute,
    });
  }

  update(data: FederatedApp) {
    this.router.navigate(['update', data.metadata.name], {
      relativeTo: this.activatedRoute,
    });
  }

  delete(resource: FederatedApp) {
    const name = resource.metadata.name;
    this.dialog
      .confirm({
        title: this.translate.get('delete_federated_application_confirm', {
          name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.FEDERATED_APPLICATION,
              cluster: this.baseParams.cluster,
              resource,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(() => {
        this.list.delete(resource);
        setTimeout(() => {
          this.list.reload();
        }, 5000);
      })
      .catch(noop);
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  joinClusters(data: FederatedApp) {
    if (data.status && data.status.clusters) {
      return data.status.clusters.map(v => v.name).join(', ');
    } else {
      return '';
    }
  }

  trackByName(_: number, data: FederatedApp) {
    return data.metadata.name;
  }
}
