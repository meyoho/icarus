import {
  ANNOTATIONS,
  AsyncDataLoader,
  KubernetesResource,
  LABELS,
  METADATA,
  NAME,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { snakeCase } from 'lodash-es';
import {
  assocPath,
  clone,
  compose,
  converge,
  dissoc,
  dissocPath,
  identity,
  pathOr,
} from 'ramda';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  FederatedAppApiService,
  FederatedAppTemplatesResource,
  SpecificFederatedAppParams,
} from 'app/services/api/federated-app.service';
import { RESOURCE_DEFINITIONS } from 'app/typings';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdatePageComponent {
  dataManager: AsyncDataLoader<
    FederatedAppTemplatesResource,
    SpecificFederatedAppParams
  >;

  constructor(
    federatedAppApi: FederatedAppApiService,
    activatedRoute: ActivatedRoute,
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
  ) {
    const params$ = combineLatest([
      workspace.baseParams,
      activatedRoute.paramMap.pipe(map(param => param.get(NAME))),
    ]).pipe(
      map(([{ cluster, namespace }, name]) => ({ cluster, namespace, name })),
      publishRef(),
    );
    this.dataManager = new AsyncDataLoader({
      params$,
      fetcher: params =>
        federatedAppApi
          .getFederatedAppResource(params)
          .pipe(map(res => recoverToAppTemplate(res.spec.components))),
    });
  }

  jumpToListPage() {
    this.router.navigate(['federated_app'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }
}

export function recoverToAppTemplate(
  components: KubernetesResource[],
  keepStatus = false,
): FederatedAppTemplatesResource {
  const removeDirty = keepStatus ? identity : removeDirtyDataBeforeUpdate;

  const getOverrides = compose(clone, pathOr([], ['spec', 'overrides']));

  const normalizeApp: (
    r: FederatedAppTemplatesResource,
  ) => FederatedAppTemplatesResource = compose(
    removeDirty,
    converge(assocPath(['spec', 'template']), [
      compose(removeDirty, pathOr({}, ['spec', 'template'])),
      identity,
    ]),
  );

  const normalizeComponent = compose(recoverToResourceTemplate, removeDirty);

  const federatedApp: FederatedAppTemplatesResource = normalizeApp(
    components.find(
      item => item.kind === 'FederatedApplication',
    ) as FederatedAppTemplatesResource,
  );

  const templates = components
    .filter(item => item.kind !== 'FederatedApplication')
    .map(item => ({
      resource: normalizeComponent(item),
      overrides: getOverrides(item),
    }));

  return assocPath(
    ['spec', 'template', 'spec', 'componentTemplates'],
    templates,
    federatedApp,
  );
}

export function recoverToResourceTemplate(component: KubernetesResource) {
  const kind = component.kind.replace('Federated', '');
  const def = RESOURCE_DEFINITIONS[kindDefKeyMap(kind)] as Record<
    string,
    string
  >;
  const version = def.apiVersion || 'v1';
  const apiVersion = def.apiGroup ? `${def.apiGroup}/${version}` : version;

  const dissocUseless: (r: KubernetesResource) => KubernetesResource = compose(
    dissoc(METADATA),
    dissoc(ANNOTATIONS),
    dissoc(LABELS),
  );

  return {
    apiVersion,
    kind,
    metadata: dissocPath(['finalizers'], component.metadata),
    ...dissocUseless(pathOr({}, ['spec', 'template'], component)),
  };
}

export function kindDefKeyMap(kind: string) {
  if (kind === 'StatefulSet') {
    return kind.toUpperCase() as keyof typeof RESOURCE_DEFINITIONS;
  }
  return snakeCase(kind).toUpperCase() as keyof typeof RESOURCE_DEFINITIONS;
}
