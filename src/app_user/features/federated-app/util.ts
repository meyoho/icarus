import { PodSpecFormConfig } from 'app/features-shared/app/tokens';

export const podSpecFormConfig: PodSpecFormConfig = {
  hiddenFields: ['node-selector', 'network'],
};
