import { DESCRIPTION } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, Secret, SecretType } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';
@Component({
  template:
    '<rc-configsecret-form-container [initResource]="initResource"></rc-configsecret-form-container>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigSecretCreateComponent {
  initResource: Secret;

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    workspace: WorkspaceComponent,
  ) {
    this.initResource = {
      apiVersion: 'v1',
      kind: 'Secret',
      metadata: {
        name: '',
        namespace: workspace.baseParamsSnapshot.namespace,
        annotations: {
          [`${this.env.LABEL_BASE_DOMAIN}/${DESCRIPTION}`]: '',
        },
      },
      type: SecretType.Opaque,
      data: {},
    };
  }
}
