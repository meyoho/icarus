import { NgModule } from '@angular/core';

import { ResourceReferenceSharedModule } from 'app/features-shared/resource-reference/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { AddSecretComponent } from './add-secret/component';
import { ConfigSecretCreateComponent } from './create/component';
import { SecretDataViewerComponent } from './data-viewer/component';
import { ConfigSecretDetailComponent } from './detail/component';
import { SecretFormContainerComponent } from './form-container/component';
import { ConfigSecretListComponent } from './list/component';
import { ConfigSecretRoutingModule } from './routing.module';
import { SecretSharedModule } from './shared.module';
import { ConfigSecretUpdateComponent } from './update/component';

@NgModule({
  imports: [
    SharedModule,
    ConfigSecretRoutingModule,
    SecretSharedModule,
    ResourceReferenceSharedModule,
  ],
  declarations: [
    ConfigSecretListComponent,
    ConfigSecretCreateComponent,
    ConfigSecretDetailComponent,
    ConfigSecretUpdateComponent,
    SecretDataViewerComponent,
    SecretFormContainerComponent,
    AddSecretComponent,
  ],
  entryComponents: [AddSecretComponent],
})
export class ConfigSecretModule {}
