import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { equals } from 'ramda';
import { combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  switchMap,
} from 'rxjs/operators';

import { RefsService } from 'app/features-shared/resource-reference/refs.service';
import { findRefs } from 'app/features-shared/resource-reference/utils';
import { getBelongingApp } from 'app/features-shared/workload/util';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, Secret } from 'app/typings';
import { viewActions, yamlReadOptions } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AddSecretComponent } from '../add-secret/component';
import { SecretUtilService } from '../util.service';

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigSecretDetailComponent {
  editorActions = viewActions;
  editorOptions = yamlReadOptions;

  baseParams = this.workspace.baseParamsSnapshot;
  getBelongingApp = getBelongingApp;

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspace.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    publishRef(),
  );

  dataManager = new AsyncDataLoader<Secret>({
    fetcher: params =>
      this.k8sApi.getResource({
        type: RESOURCE_TYPES.SECRET,
        cluster: params.cluster,
        namespace: params.namespace,
        name: params.name,
      }),
    params$: this.params$,
  });

  permissions$ = this.workspace.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SERVICE,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  refs$ = this.params$.pipe(
    switchMap(({ cluster, namespace, name }) => {
      return this.acpApi
        .getTopologyByResource({
          cluster,
          namespace,
          name,
          kind: 'secret',
        })
        .pipe(
          map(graph => {
            return findRefs(graph, { kind: 'Secret', name });
          }),
          catchError(() => of([])),
        );
    }),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly acpApi: AcpApiService,
    private readonly refsService: RefsService,
    private readonly secretUtilities: SecretUtilService,
    private readonly router: Router,
    private readonly workspace: WorkspaceComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
  ) {}

  getDisplayType = (value: string) => {
    return this.secretUtilities.translateSecretType(value);
  };

  back = () => {
    this.router.navigate(['secret'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  };

  getDescription(secret: Secret) {
    return this.k8sUtil.getDescription(secret);
  }

  addSecret() {
    this.dialog
      .open(AddSecretComponent, {
        size: DialogSize.Large,
        data: {
          baseParams: this.dataManager.snapshot.params,
          secret: this.dataManager.snapshot.data,
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.dataManager.mapData(() => {
            return result;
          });
        }
      });
  }

  updateSecret() {
    this.router.navigate(
      ['secret', 'update', this.dataManager.snapshot.params.name],
      {
        relativeTo: this.workspace.baseActivatedRoute,
      },
    );
  }

  deleteSecret() {
    this.refsService
      .safeDelete({
        cluster: this.workspace.baseParamsSnapshot.cluster,
        resource: this.dataManager.snapshot.data,
        handleDelete: () =>
          this.k8sApi.deleteResource({
            type: RESOURCE_TYPES.SECRET,
            cluster: this.workspace.baseParamsSnapshot.cluster,
            resource: this.dataManager.snapshot.data,
          }),
      })
      .subscribe(this.back, noop);
  }

  resourceToYaml(secret: Secret) {
    return safeDump(secret);
  }

  dataUpdated(secret: Secret) {
    this.dataManager.mapData(() => secret);
  }
}
