import { TranslateService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

@Injectable()
export class SecretUtilService {
  constructor(private readonly translate: TranslateService) {}

  translateSecretType(type: string) {
    const value = type.replace('kubernetes.io/', '');
    switch (value) {
      case 'tls':
        return 'TLS';
      case 'ssh-auth':
        return this.translate.get('secret_ssh_auth');
      case 'basic-auth':
        return this.translate.get('secret_basic_auth');
      case 'dockerconfigjson':
        return this.translate.get('secret_dockerconfigjson');
      case 'Opaque':
      default:
        return value;
    }
  }
}
