import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, combineLatest } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType, Secret } from 'app/typings';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-loading-mask [loading]="!(secret$ | async)"></rc-loading-mask>
    <rc-configsecret-form-container
      *ngIf="secret$ | async"
      [isUpdate]="true"
      [initResource]="secret$ | async"
    ></rc-configsecret-form-container>
  `,
})
export class ConfigSecretUpdateComponent {
  secret$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspaceComponent.baseParams,
  ]).pipe(
    switchMap(([params, { cluster, namespace }]) =>
      this.k8sApi
        .getResource<Secret>({
          type: RESOURCE_TYPES.SECRET,
          name: params.get(NAME),
          cluster,
          namespace,
        })
        .pipe(
          catchError(() => {
            this.router.navigate(['secret'], {
              relativeTo: this.workspaceComponent.baseActivatedRoute,
            });
            return EMPTY;
          }),
        ),
    ),
    map(removeDirtyDataBeforeUpdate),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}
}
