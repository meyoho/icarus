import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { SecretItemsFieldsetComponent } from './fieldset/component';
import { SecretFormComponent } from './form/component';
import { DockerConfigDataFormComponent } from './form/docker-config-data-form/component';
import { SecretDataFormComponent } from './form/secret-data-form/component';
import { SecretUtilService } from './util.service';

@NgModule({
  imports: [SharedModule],
  declarations: [
    SecretFormComponent,
    SecretItemsFieldsetComponent,
    SecretDataFormComponent,
    DockerConfigDataFormComponent,
  ],
  exports: [SecretFormComponent],
  providers: [SecretUtilService],
})
export class SecretSharedModule {}
