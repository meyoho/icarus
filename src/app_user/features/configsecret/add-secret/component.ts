import {
  JSONPatchOp,
  K8sApiService,
  PatchOperation,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { noop } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { RESOURCE_TYPES, Secret } from 'app/typings';
import { K8S_CONFIGMAP_KEY } from 'app/utils';
@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddSecretComponent {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  formModel = {
    key: '',
    value: '',
  };

  keyReg = K8S_CONFIGMAP_KEY;
  submitting = false;
  cluster: string;
  namespace: string;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      baseParams: {
        cluster: string;
        namespace: string;
        name: string;
      };
      secret: Secret;
    },
    private readonly k8sApi: K8sApiService,
    private readonly dialogRef: DialogRef,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;

    const resource = this.data.secret;
    const path = resource.data ? `/data/${this.formModel.key}` : '/data';
    const encodedValue = Base64.encode(this.formModel.value);
    const value = resource.data
      ? encodedValue
      : { [this.formModel.key]: encodedValue };

    this.k8sApi
      .patchResource({
        type: RESOURCE_TYPES.SECRET,
        cluster: this.data.baseParams.cluster,
        resource,
        part: [{ op: JSONPatchOp.Add, path, value }],
        operation: PatchOperation.JSON,
      })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(res => this.dialogRef.close(res), noop);
  }

  cancel() {
    this.dialogRef.close();
  }
}
