import {
  JSONPatchOp,
  K8sApiService,
  PatchOperation,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Base64 } from 'js-base64';
import { Observable, fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { RESOURCE_TYPES, Secret } from 'app/typings';

@Component({
  selector: 'rc-secret-data-viewer',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class SecretDataViewerComponent implements OnInit, OnChanges {
  @Input()
  secret: Secret;

  @Input()
  clusterName: string;

  @Input()
  canUpdate: boolean;

  @Output()
  updated = new EventEmitter<Secret>();

  @ViewChild(NgForm, { static: false })
  form: NgForm;

  pageScroll$: Observable<number>;

  secretItems: Array<Record<string, string>> = [];

  keyword = '';

  submitting = false;

  visibleMap: { [key: string]: boolean } = {};

  private dialogRef: DialogRef<TemplateRef<any>>;

  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
  ) {}

  ngOnInit(): void {
    const layoutPageEl = document.querySelector('.aui-layout__page');
    this.pageScroll$ = fromEvent(layoutPageEl, 'scroll').pipe(
      startWith({}),
      map(() => layoutPageEl.scrollTop),
    );
  }

  ngOnChanges({ secret }: SimpleChanges) {
    if (secret) {
      this.secretItems = Object.entries(
        this.secret.data || {},
      ).map(([key, value]) => ({ key, value }));
    }
  }

  toggleCollapse(key: string) {
    this.visibleMap[key] = !this.visibleMap[key];
  }

  decode(s: string): string {
    if (s) {
      return Base64.decode(s);
    } else {
      return '';
    }
  }

  filterSecrets(secrets: Array<Record<string, string>>, keyword: string) {
    return secrets.filter(({ key }) => key.includes(keyword));
  }

  updateItem(item: Record<string, string>, templateRef: TemplateRef<any>) {
    this.dialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Large,
      data: { key: item.key, value: Base64.decode(item.value) },
    });
  }

  deleteItem(item: Record<string, string>) {
    this.dialog
      .confirm({
        title: this.translate.get('delete'),
        content: this.translate.get(
          'configmap_service_delete_key_configmap_confirm',
          {
            configmap_key: item.key,
          },
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .patchResource({
              type: RESOURCE_TYPES.SECRET,
              cluster: this.clusterName,
              resource: this.secret,
              part: [{ op: JSONPatchOp.Remove, path: `/data/${item.key}` }],
              operation: PatchOperation.JSON,
            })
            .subscribe(resolve, reject);
        },
      })
      .then(result => {
        this.updated.emit(result);
      })
      .catch(noop);
  }

  confirmUpdate(key: string, newVal: string) {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchResource({
        cluster: this.clusterName,
        type: RESOURCE_TYPES.SECRET,
        resource: this.secret,
        part: [
          {
            op: JSONPatchOp.Replace,
            path: `/data/${key}`,
            value: Base64.encode(newVal),
          },
        ],
        operation: PatchOperation.JSON,
      })
      .subscribe(
        result => {
          this.submitting = false;
          this.dialogRef.close();
          this.updated.emit(result);
        },
        () => {
          this.submitting = false;
        },
      );
  }
}
