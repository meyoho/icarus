import {
  COMMON_WRITABLE_ACTIONS,
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
  noop,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { RefsService } from 'app/features-shared/resource-reference/refs.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, Secret } from 'app/typings';
import { ACTION, TYPE } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { SecretUtilService } from '../util.service';

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigSecretListComponent {
  list: K8SResourceList<Secret>;

  permissions$ = this.workspace.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SERVICE,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  columns = [NAME, TYPE, DESCRIPTION, 'created_time', ACTION];
  baseParams = this.workspace.baseParamsSnapshot;
  get descriptionKey() {
    return this.utilService.normalizeType(DESCRIPTION);
  }

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly secretUtil: SecretUtilService,
    private readonly workspace: WorkspaceComponent,
    private readonly utilService: K8sUtilService,
    private readonly refsService: RefsService,
  ) {
    this.list = new K8SResourceList({
      fetcher: queryParams =>
        this.k8sApi.getResourceList({
          type: RESOURCE_TYPES.SECRET,
          cluster: this.workspace.baseParamsSnapshot.cluster,
          namespace: this.workspace.baseParamsSnapshot.namespace,
          queryParams,
        }),
      activatedRoute: this.activatedRoute,
    });
  }

  getDisplayType(value: string) {
    return this.secretUtil.translateSecretType(value);
  }

  trackByFn(_index: number, item: Secret) {
    return item.metadata.uid;
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  create() {
    this.router.navigate(['secret', 'create'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  update(name: string) {
    this.router.navigate(['secret', 'update', name], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  delete(secret: Secret) {
    this.refsService
      .safeDelete({
        cluster: this.workspace.baseParamsSnapshot.cluster,
        resource: secret,
        handleDelete: () =>
          this.k8sApi.deleteResource({
            type: RESOURCE_TYPES.SECRET,
            cluster: this.workspace.baseParamsSnapshot.cluster,
            resource: secret,
          }),
      })
      .subscribe(() => {
        this.list.delete(secret);
      }, noop);
  }
}
