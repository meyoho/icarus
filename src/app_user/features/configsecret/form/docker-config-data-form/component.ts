import { CommonFormControl } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  QueryList,
  ViewChildren,
  forwardRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, NgControl, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'rc-docker-config-data-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DockerConfigDataFormComponent),
      multi: true,
    },
  ],
})
export class DockerConfigDataFormComponent extends CommonFormControl<string>
  implements AfterViewInit {
  @ViewChildren(NgControl)
  controls: QueryList<NgControl>;

  showPassword = false;

  constructor(private readonly injector: Injector, cdr: ChangeDetectorRef) {
    super(cdr);
  }

  writeValue(value: string) {
    this.value$$.next(value);
  }

  toFormModel(value: string) {
    const auths = JSON.parse(value || '{}').auths || {};
    const address = Object.keys(auths)[0] || '';
    const config = auths[address] || {};
    const username = config.username || '';
    const password = config.password || '';
    const email = config.email || '';

    return {
      address,
      username,
      password,
      email,
    };
  }

  emitPropertyValueChange(
    model: Record<string, string>,
    key: string,
    value: string,
  ) {
    const auth = {
      username: model.username,
      password: model.password,
      email: model.email,
    };
    if (key === 'address') {
      this.emitValueChange(
        JSON.stringify({
          auths: {
            [value]: auth,
          },
        }),
      );
    } else {
      this.emitValueChange(
        JSON.stringify({
          auths: {
            [model.address]: {
              ...auth,
              [key]: value,
            },
          },
        }),
      );
    }
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  ngAfterViewInit() {
    const selfControl = this.injector.get(NgControl);

    if (selfControl) {
      const originValidator = selfControl.control.validator;
      this.controls.changes
        .pipe(startWith(this.controls))
        .subscribe((ctrls: QueryList<NgControl>) => {
          const validator = () => {
            const errors = ctrls.filter(ctl => ctl.invalid);
            return errors.length
              ? errors.reduce(
                  (acc, curr) => ({
                    ...acc,
                    [`${selfControl.name}.${curr.name}`]: true,
                  }),
                  {},
                )
              : null;
          };

          selfControl.control.validator = Validators.compose([
            originValidator,
            validator,
          ]);
        });
    }
  }
}
