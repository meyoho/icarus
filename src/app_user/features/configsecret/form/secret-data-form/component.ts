import { CommonFormControl, NotificationService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  QueryList,
  ViewChildren,
  forwardRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, NgControl, Validators } from '@angular/forms';
import { Base64 } from 'js-base64';
import { startWith } from 'rxjs/operators';

function checkFileIsBinary(content: string) {
  return [...content].some(char => char.charCodeAt(0) > 127);
}

@Component({
  selector: 'rc-secret-data-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SecretDataFormComponent),
      multi: true,
    },
  ],
})
export class SecretDataFormComponent
  extends CommonFormControl<Record<string, string>>
  implements AfterViewInit {
  @Input()
  type: string;

  @ViewChildren(NgControl)
  controls: QueryList<NgControl>;

  showPassword = false;

  fieldSet: Record<string, string> = null;

  constructor(
    private readonly notification: NotificationService,
    private readonly injector: Injector,
    cdr: ChangeDetectorRef,
  ) {
    super(cdr);
  }

  ngAfterViewInit() {
    const selfControl = this.injector.get(NgControl);

    if (selfControl) {
      const originValidator = selfControl.control.validator;
      this.controls.changes
        .pipe(startWith(this.controls))
        .subscribe((ctrls: QueryList<NgControl>) => {
          const validator = () => {
            const errors = ctrls.filter(ctl => ctl.invalid);
            return errors.length
              ? errors.reduce(
                  (acc, curr) => ({
                    ...acc,
                    [`${selfControl.name}.${curr.name}`]: true,
                  }),
                  {},
                )
              : null;
          };

          selfControl.control.validator = Validators.compose([
            originValidator,
            validator,
          ]);
        });
    }
  }

  writeValue(value: Record<string, string>) {
    this.fieldSet = this.decodeValue(value || {});

    this.value$$.next(value || {});
  }

  emitPropertyValueChange(key: string, value: string) {
    this.emitValueChange({
      ...this.snapshot.value,
      [key]: Base64.encode(value),
    });
  }

  decodeValue(value: Record<string, string>): Record<string, string> {
    return Object.entries(value)
      .map(([key, value]) => {
        return [key, Base64.decode(value)];
      })
      .reduce((acc, [key, value]) => {
        return { ...acc, [key]: value };
      }, {});
  }

  fieldSetChange(value: Record<string, string>) {
    this.emitValueChange(
      Object.entries(value).reduce((acc, [key, value]) => {
        return { ...acc, [key]: Base64.encode(value || '') };
      }, {}),
    );
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  onImport(event: Event) {
    const input = event.target as HTMLInputElement;
    const length = input.files.length;

    let counter = 0;

    // eslint-disable-next-line unicorn/prefer-spread
    Array.from(input.files).forEach(file => {
      const reader = new FileReader();
      reader.onloadend = () => {
        if (checkFileIsBinary(reader.result as string)) {
          this.notification.error({
            content: 'File unsupported',
          });
        } else {
          this.emitPropertyValueChange('tls.crt', reader.result.toString());
          this.cdr.markForCheck();
        }
        counter++;
        if (length === counter) {
          input.value = '';
        }
      };

      reader.readAsText(file);
    });
  }
}
