import { DESCRIPTION } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
} from '@angular/core';
import { AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, Secret, SecretType } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

import { SecretUtilService } from '../util.service';

@Component({
  selector: 'rc-configsecret-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretFormComponent extends BaseResourceFormGroupComponent<
  Secret,
  Secret
> {
  @Input()
  isUpdate = false;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  descriptionKey = `${this.env.LABEL_BASE_DOMAIN}/${DESCRIPTION}`;

  constructor(
    injector: Injector,
    private readonly secretUtilities: SecretUtilService,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  getDisplayType = (value: string) => {
    return this.secretUtilities.translateSecretType(value);
  };

  createForm() {
    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control('', [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ]),
        annotations: this.fb.group({
          [this.descriptionKey]: this.fb.control(''),
        }),
      }),
      type: this.fb.control(''),
      data: this.fb.control({}, [ValueRequiredValidator]),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  typeChanged(type: SecretType) {
    this.form.get('data').setValue(this.getDataStructure(type));
  }

  private getDataStructure(type: SecretType) {
    switch (type) {
      case SecretType.TLS:
        return {
          'tls.crt': '',
          'tls.key': '',
        };
      case SecretType.SSHAuth:
        return { 'ssh-privatekey': '' };
      case SecretType.BasicAuth:
        return {
          username: '',
          password: '',
        };
      case SecretType.DockerConfigJson:
        return { '.dockerconfigjson': '' };
      default:
        return {};
    }
  }
}

function ValueRequiredValidator(
  control: AbstractControl,
): ValidationErrors | null {
  const hasEmpty = Object.values(control.value || {}).some(v => !v);
  return hasEmpty ? { value_required: true } : null;
}
