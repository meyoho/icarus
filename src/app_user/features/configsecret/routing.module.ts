import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConfigSecretCreateComponent } from './create/component';
import { ConfigSecretDetailComponent } from './detail/component';
import { ConfigSecretListComponent } from './list/component';
import { ConfigSecretUpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: ConfigSecretListComponent,
  },
  {
    path: 'create',
    component: ConfigSecretCreateComponent,
  },
  {
    path: 'update/:name',
    component: ConfigSecretUpdateComponent,
  },
  {
    path: 'detail/:name',
    component: ConfigSecretDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigSecretRoutingModule {}
