import {
  formatCPU,
  formatMemory,
} from 'app/features-shared/app/pod-template-form/resource-form/utils';
import { Container, Pod } from 'app/typings';

// 返回 CPU, Memory。若为''，则解析为无限制
export function getPodResourceLimit(
  pod: Pod,
): { cpu: number | string; memory: number | string } {
  const { containers = [], initContainers = [] } = pod.spec;
  const allContainers = initContainers.concat(...containers);
  let cpuResult = 0;
  let memoryResult = 0;
  allContainers.forEach(container => {
    // 通过强行修改yaml，删除limits的字段，可以让cpu，memory无限制
    let { cpu = '', memory = '' } = container.resources.limits || {};
    // 未限制置为 NaN，无需解析计算
    if (cpu === '' || isNaN(cpuResult)) {
      cpuResult = NaN;
    } else {
      if (!isNaN(+cpu)) {
        cpu += 'c';
      }
      cpuResult += formatCPU(cpu);
    }
    if (memory === '' || isNaN(memoryResult)) {
      memoryResult = NaN;
    } else {
      memoryResult += formatMemory(memory);
    }
  });
  return {
    cpu: isNaN(cpuResult) ? '' : cpuResult,
    memory: isNaN(memoryResult) ? '' : memoryResult,
  };
}

export function getContainerResourceLimit(
  container: Container,
): { cpu: string; memory: string } {
  const { cpu = '', memory = '' } = container.resources.limits || {};
  return { cpu, memory };
}
