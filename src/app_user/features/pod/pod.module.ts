import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { PodSharedModule } from 'app/features-shared/pod/shared.module';
import { UtilService } from 'app/features-shared/workload/util.service';
import { SharedModule } from 'app/shared/shared.module';

import { PodDetailComponent } from './detail/component';
import { PodDetailInfoComponent } from './detail/detail-info/component';
import { PodStatusListComponent } from './detail/pod-status-list/component';
import { PodListPageComponent } from './list-page/component';
import { PodRoutingModule } from './pod.routing.module';

@NgModule({
  imports: [
    PodRoutingModule,
    SharedModule,
    ExecSharedModule,
    PodSharedModule,
    AppSharedModule,
    EventSharedModule,
  ],
  providers: [UtilService],
  declarations: [
    PodListPageComponent,
    PodDetailComponent,
    PodDetailInfoComponent,
    PodStatusListComponent,
  ],
})
export class PodModule {}
