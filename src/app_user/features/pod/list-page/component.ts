import { K8SResourceList } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Pod } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <aui-inline-alert>
      {{ 'pod_introduction' | translate }}
    </aui-inline-alert>
    <rc-pod-list
      [baseParams]="baseParams"
      [embedded]="false"
      (onViewLog)="viewLog($event)"
    ></rc-pod-list>
  `,
})
export class PodListPageComponent {
  readonly baseParams = this.workspace.baseParamsSnapshot;
  list: K8SResourceList<Pod>;

  constructor(
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
  ) {}

  viewLog(params: { pod: string; container: string }) {
    const { pod, container } = params;
    this.router.navigate(['pod', 'detail', pod], {
      relativeTo: this.workspace.baseActivatedRoute,
      queryParams: {
        tabIndex: 3,
        container,
        pod,
      },
    });
  }
}
