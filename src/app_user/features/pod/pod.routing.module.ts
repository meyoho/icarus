import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PodDetailComponent } from './detail/component';
import { PodListPageComponent } from './list-page/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PodListPageComponent,
  },
  {
    path: 'detail/:name',
    pathMatch: 'full',
    component: PodDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PodRoutingModule {}
