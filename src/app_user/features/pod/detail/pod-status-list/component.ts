import { Component, Input } from '@angular/core';

import { PodCondition } from 'app/typings';

@Component({
  selector: 'rc-pod-status-list',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class PodStatusListComponent {
  @Input()
  conditions: PodCondition[];

  get sortedConditions() {
    return this.conditions.sort((a, b) =>
      a.lastTransitionTime === b.lastTransitionTime
        ? 0
        : a.lastTransitionTime > b.lastTransitionTime
        ? -1
        : 1,
    );
  }

  columns = ['type', 'status', 'reason', 'message', 'lastUpdateTime'];
}
