import {
  ObservableInput,
  StringMap,
  TranslateService,
  viewActions,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { compose } from '@ngrx/store';
import { get } from 'lodash-es';
import { pathOr } from 'ramda';
import { Observable } from 'rxjs';

import {
  FlattenAffinity,
  flatAffinity,
} from 'app/features-shared/workload/util';
import {
  Container,
  ContainerStatus,
  Pod,
  WorkspaceBaseParams,
} from 'app/typings';
import { STATUS } from 'app/utils';

import { getPodResourceLimit } from '../../util';
import { SourceLink } from '../component';

@Component({
  selector: 'rc-pod-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodDetailInfoComponent {
  @ViewChild('serviceAccountTmpl')
  serviceAccountTmpl: TemplateRef<any>;

  @Input()
  data: Pod;

  @Input()
  sourceLink: SourceLink;

  @ObservableInput(true)
  data$: Observable<Pod>;

  @Input()
  serviceAccountYaml: string;

  @Input()
  canDelete: boolean;

  @Input()
  canExec: boolean;

  @Input()
  baseParams: WorkspaceBaseParams;

  @Output()
  stateChange = new EventEmitter<{
    resource: Pod;
  } | void>();

  @Output()
  deletePod = new EventEmitter<Pod>();

  @Output()
  exec = new EventEmitter<{ pod: Pod; container: Container }>();

  @Output()
  openTerminal = new EventEmitter<{
    pods: string;
    selectedPod: string;
    container: string;
  }>();

  path = pathOr('-');
  actionsConfig = viewActions;
  getPodResourceLimit = getPodResourceLimit;

  constructor(
    private readonly dialogService: DialogService,
    public readonly translate: TranslateService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    this.formatSourceLink = this.formatSourceLink.bind(this);
  }

  getNodeSelector(data: Pod) {
    return pathOr({}, ['spec', 'nodeSelector'], data);
  }

  formatNodeSelector(selectors: StringMap) {
    return Object.keys(selectors)
      .map(key => [key, selectors[key]].join('='))
      .join('， ');
  }

  getImagePullSecrets(resource: Pod) {
    return pathOr([], ['spec', 'imagePullSecrets'], resource).map(
      ({ name }) => name,
    );
  }

  getAffinityInfo(resource: Pod): FlattenAffinity[] {
    const items = compose(
      flatAffinity,
      pathOr({}, ['spec', 'affinity']),
    )(resource);
    return items.length > 0 ? items : null;
  }

  getVolumes(resource: Pod) {
    return get(resource, ['spec', 'volumes']);
  }

  openServiceAccountYaml() {
    this.dialogService.open(this.serviceAccountTmpl, {
      size: DialogSize.Large,
    });
  }

  openLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 3,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  onOpenTerminal(params: {
    pods: string;
    selectedPod: string;
    container: string;
  }) {
    this.openTerminal.next(params);
  }

  onExec(pod: Pod, container: Container) {
    this.exec.next({ pod, container });
  }

  delete() {
    this.deletePod.next(this.data);
  }

  canExecContainer(container: Container, pod: Pod) {
    const containerStatuses = get(
      pod,
      [STATUS, 'containerStatuses'],
      [] as ContainerStatus[],
    );
    return !!get(
      containerStatuses.find(status => status.name === container.name),
      ['state', 'running'],
    );
  }

  formatSourceLink(sourceLink: SourceLink) {
    return sourceLink
      ? `${sourceLink.name} (${this.translate.get(
          sourceLink.kind.toLowerCase(),
        )})`
      : '';
  }
}
