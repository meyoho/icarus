import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  NAME,
  OwnerReference,
  TranslateService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { equals } from 'ramda';
import { Observable, combineLatest, interval, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  first,
  map,
  pluck,
  shareReplay,
  startWith,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import { MetricService } from 'app/services/api/metric.service';
import { TerminalService } from 'app/services/api/terminal.service';
import {
  Container,
  Pod,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { PodUtilService } from '../util.service';

const TRACE_SOURCE_SUPPORT_RESOURCE = new Set([
  'Deployment',
  'StatefulSet',
  'DaemonSet',
  'TApp',
  'Job',
  'CronJob',
  'ReplicaSet',
]);

export interface SourceLink {
  name?: string;
  routerLink?: any[];
  kind?: string;
}

@Component({
  selector: 'rc-pod-detail',
  templateUrl: 'template.html',
  styleUrls: ['../../../../app_user/features/app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodDetailComponent {
  baseParams: WorkspaceBaseParams;

  tabIndex$ = this.activatedRoute.queryParamMap.pipe(
    map(params => parseInt(params.get('tabIndex'), 10) || 0),
    distinctUntilChanged(),
  );

  logInitialState$ = this.activatedRoute.queryParamMap.pipe(
    map(params => ({
      pod: params.get('pod'),
      container: params.get('container'),
    })),
  );

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspace.baseParams,
  ]).pipe(
    map(([name, baseParams]) => {
      this.baseParams = baseParams;
      return {
        name,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
      };
    }),
    distinctUntilChanged<{ name: string; cluster: string; namespace: string }>(
      equals,
    ),
    publishRef(),
  );

  execPermissions$ = this.workspace.baseParams.pipe(
    switchMap(({ cluster, namespace }) =>
      this.terminal.getPermissions({
        cluster,
        namespace,
      }),
    ),
  );

  showMetricTab$ = this.workspace.baseParams.pipe(
    switchMap(({ cluster }) => this.metricService.canUseMetric(cluster)),
  );

  dataManager = new AsyncDataLoader<{
    resource: Pod;
  }>({
    params$: this.params$,
    fetcher: ({ cluster, namespace, name }) => {
      return interval(10000).pipe(
        withLatestFrom(this.tabIndex$),
        filter(([_, tabIndex]) => tabIndex === 0),
        startWith(0),
        switchMap(() =>
          this.k8sApi.getResource({
            type: RESOURCE_TYPES.POD,
            cluster,
            namespace,
            name,
          }),
        ),
        map(resource => ({
          resource,
        })),
      );
    },
  });

  deletePermission$ = combineLatest([
    this.dataManager.data$,
    this.params$,
  ]).pipe(
    switchMap(([_, { cluster, namespace }]) =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.POD,
        action: K8sResourceAction.DELETE,
        cluster,
        namespace,
      }),
    ),
  );

  serviceAccountYaml$ = combineLatest([
    this.dataManager.data$.pipe(
      distinctUntilChanged(
        (a, b) =>
          a.resource.spec.serviceAccountName ===
          b.resource.spec.serviceAccountName,
      ),
    ),
    this.workspace.baseParams,
  ]).pipe(
    switchMap(([data, { namespace, cluster }]) => {
      const { serviceAccountName } = data.resource.spec;
      if (!serviceAccountName) {
        return null;
      }
      return this.k8sApi
        .getResource({
          type: RESOURCE_TYPES.SERVICE_ACCOUNT,
          namespace,
          cluster,
          name: serviceAccountName,
        })
        .pipe(
          map(resource => safeDump(resource)),
          catchError(_ => of(null)),
        );
    }),
    shareReplay(1),
  );

  sourceLink$ = this.dataManager.data$.pipe(
    pluck('resource'),
    distinctUntilChanged(this.sameOwnerReference),
    filter(i => !!i),
    switchMap(data => this.getSource(data)),
    shareReplay(1),
  );

  matchLabelsToString = matchLabelsToString;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly terminal: TerminalService,
    private readonly podService: PodUtilService,
    private readonly dialog: DialogService,
    private readonly metricService: MetricService,
  ) {}

  tabIndexChanged(index: number) {
    const currentQueryParams = this.activatedRoute.snapshot.queryParams;
    if (index === 0 || index === 1) {
      this.dataManager.reload();
    }
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...currentQueryParams,
        tabIndex: index,
      },
    });
  }

  stateChanged(data: { resource: Pod }) {
    if (data) {
      this.dataManager.reload({ ...this.dataManager.snapshot.data, ...data });
    } else {
      this.dataManager.reload();
    }
  }

  viewLog({ pod, container }: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 3,
        pod,
        container,
      },
    });
  }

  jumpToListPage() {
    this.router.navigate(['pod'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  }

  deletePod(pod: Pod) {
    this.podService.deletePod(pod, this.baseParams.cluster).subscribe(_ => {
      this.jumpToListPage();
      this.message.success(this.translate.get('delete_success'));
    });
  }

  exec({ pod, container }: { pod: Pod; container: Container }) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      this.podService.handleExecInfo(pod, container, this.baseParams, data);
    });
  }

  openTerminal({
    pods,
    selectedPod,
    container,
  }: {
    pods: string;
    selectedPod: string;
    container: string;
  }) {
    const { namespace, cluster } = this.workspace.baseParamsSnapshot;
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (data) {
        this.terminal.openTerminal({
          pods,
          selectedPod,
          container,
          namespace,
          cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  // 仅支持 ownerReferences 为 workload, tapp
  getSource(resource: KubernetesResource): Observable<SourceLink> {
    const ownerRef = resource.metadata.ownerReferences?.[0];
    // 无 ownerRef，不显示
    if (!ownerRef) {
      return of(null);
    }

    // 不支持跳转详情页
    if (!TRACE_SOURCE_SUPPORT_RESOURCE.has(ownerRef.kind)) {
      return of({ name: ownerRef.name, kind: ownerRef.kind });
    }

    // 支持跳转详情页
    if (ownerRef.kind !== 'ReplicaSet') {
      return of({
        name: ownerRef.name,
        routerLink: this.getSourceLink(ownerRef),
        kind: ownerRef.kind,
      });
    }

    // kind 为 ReplicaSet，递归查找ownerReference
    return this.k8sApi
      .getResource({
        type: RESOURCE_TYPES.REPLICA_SET,
        name: ownerRef.name,
        namespace: this.baseParams.namespace,
        cluster: this.baseParams.cluster,
      })
      .pipe(
        switchMap(resource => {
          return this.getSource(resource);
        }),
      );
  }

  private getSourceLink(ownerRef: OwnerReference) {
    const { kind, name } = ownerRef;
    let detailPath = kind.toLowerCase();
    if (kind === 'Job') {
      detailPath = 'job_record';
    } else if (kind === 'CronJob') {
      detailPath = 'cron_job';
    }
    return ['/workspace', this.baseParams, detailPath, 'detail', name];
  }

  private sameOwnerReference(a: KubernetesResource, b: KubernetesResource) {
    const aRef = a.metadata.ownerReferences?.[0];
    const bRef = b.metadata.ownerReferences?.[0];
    return aRef && bRef && aRef.name === bRef.name && aRef.kind === bRef.kind;
  }
}
