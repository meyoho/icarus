import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-workload-detail (redirect)="jumpToListPage()"></rc-workload-detail>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPageComponent {
  constructor(
    private readonly router: Router,
    private readonly workspaceComponent: WorkspaceComponent,
  ) {}

  jumpToListPage() {
    this.router.navigate(['daemonset'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
