import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <rc-workload-update></rc-workload-update>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent {}
