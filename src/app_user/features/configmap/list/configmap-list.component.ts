import {
  COMMON_WRITABLE_ACTIONS,
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { noop } from 'rxjs';
import { map } from 'rxjs/operators';

import { RefsService } from 'app/features-shared/resource-reference/refs.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { ConfigMap, RESOURCE_TYPES } from 'app/typings';
import { ACTION } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { ConfigMapUtilService } from '../util.service';

@Component({
  templateUrl: './configmap-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigmapListComponent {
  columns = [NAME, DESCRIPTION, 'created_time', ACTION];

  get descriptionKey() {
    return this.utilService.normalizeType(DESCRIPTION);
  }

  baseParams = this.workspaceComponent.baseParamsSnapshot;

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CONFIG_MAP,
    cluster: this.baseParams.cluster,
    namespace: this.baseParams.namespace,
    action: COMMON_WRITABLE_ACTIONS,
  });

  search$ = this.route.queryParamMap.pipe(map(params => params.get('keyword')));

  list = new K8SResourceList({
    activatedRoute: this.route,
    fetcher: this.fetchConfigMaps.bind(this),
  });

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sPermission: K8sPermissionService,
    public configmapUtilities: ConfigMapUtilService,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService,
    private readonly utilService: K8sUtilService,
    private readonly refsService: RefsService,
  ) {}

  fetchConfigMaps(queryParams: ResourceListParams) {
    return this.k8sApi.getResourceList<ConfigMap>({
      type: RESOURCE_TYPES.CONFIG_MAP,
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
      queryParams,
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  trackByFn(_index: number, item: ConfigMap) {
    return item.metadata.uid;
  }

  createConfigmap() {
    this.router.navigate(['configmap', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updateConfigmap(name: string) {
    this.router.navigate(['configmap', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  deleteConfigmap(configmap: ConfigMap) {
    this.refsService
      .safeDelete({
        cluster: this.baseParams.cluster,
        resource: configmap,
        handleDelete: () =>
          this.k8sApi.deleteResource({
            type: RESOURCE_TYPES.CONFIG_MAP,
            cluster: this.baseParams.cluster,
            resource: configmap,
          }),
      })
      .subscribe(() => {
        this.list.delete(configmap);
      }, noop);
  }
}
