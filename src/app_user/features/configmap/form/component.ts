import { DESCRIPTION } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { ConfigMap, Environments } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

@Component({
  selector: 'rc-configmap-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapFormComponent extends BaseResourceFormGroupComponent<
  ConfigMap,
  ConfigMap
> {
  @Input()
  isUpdate = false;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  descriptionKey = `${this.env.LABEL_BASE_DOMAIN}/${DESCRIPTION}`;

  constructor(
    injector: Injector,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      metadata: this.fb.group({
        name: this.fb.control('', [
          Validators.required,
          Validators.maxLength(63),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ]),
        annotations: this.fb.group({
          [this.descriptionKey]: this.fb.control(''),
        }),
      }),
      data: this.fb.control({}),
    });
  }

  getDefaultFormModel() {
    return {};
  }
}
