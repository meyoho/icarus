import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, combineLatest } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ConfigMap, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-loading-mask [loading]="!(configMap$ | async)"></rc-loading-mask>
    <rc-configmap-form-container
      *ngIf="configMap$ | async"
      [isUpdate]="true"
      [initResource]="configMap$ | async"
    ></rc-configmap-form-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapUpdateComponent {
  configMap$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspaceComponent.baseParams,
  ]).pipe(
    switchMap(([params, { cluster, namespace }]) =>
      this.k8sApi
        .getResource<ConfigMap>({
          type: RESOURCE_TYPES.CONFIG_MAP,
          name: params.get(NAME),
          cluster,
          namespace,
        })
        .pipe(
          catchError(() => {
            this.router.navigate(['configmap'], {
              relativeTo: this.workspaceComponent.baseActivatedRoute,
            });
            return EMPTY;
          }),
        ),
    ),
    map(removeDirtyDataBeforeUpdate),
    publishRef(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}
}
