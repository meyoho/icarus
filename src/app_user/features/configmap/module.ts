import { NgModule } from '@angular/core';

import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { ResourceReferenceSharedModule } from 'app/features-shared/resource-reference/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { K8sConfigItemsComponent } from './config-items/k8s-config-items.component';
import { ConfigMapCreateComponent } from './create/component';
import { ConfigmapDetailComponent } from './detail/configmap-detail.component';
import { AddConfigDialogComponent } from './dialog/add-config-dialog.component';
import { ConfigMapFormContainerComponent } from './form-container/component';
import { ConfigmapListComponent } from './list/configmap-list.component';
import { ConfigmapRoutingModule } from './routing.module';
import { ConfigMapSharedModule } from './shared.module';
import { ConfigMapUpdateComponent } from './update/component';
import { ConfigMapUtilService } from './util.service';
import { ConfigmapWorkloadsComponent } from './workloads-table/component';

@NgModule({
  imports: [
    SharedModule,
    ConfigmapRoutingModule,
    ExecSharedModule,
    ConfigMapSharedModule,
    ResourceReferenceSharedModule,
  ],
  providers: [ConfigMapUtilService],
  declarations: [
    ConfigmapListComponent,
    ConfigMapCreateComponent,
    ConfigmapDetailComponent,

    ConfigMapUpdateComponent,
    K8sConfigItemsComponent,
    AddConfigDialogComponent,
    ConfigmapWorkloadsComponent,
    ConfigMapFormContainerComponent,
  ],
})
export class ConfigmapModule {}
