import { DESCRIPTION } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { ConfigMap, Environments } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template:
    '<rc-configmap-form-container [initResource]="initResource"></rc-configmap-form-container>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapCreateComponent {
  initResource: ConfigMap;

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    workspace: WorkspaceComponent,
  ) {
    this.initResource = {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      metadata: {
        name: '',
        namespace: workspace.baseParamsSnapshot.namespace,
        annotations: {
          [`${this.env.LABEL_BASE_DOMAIN}/${DESCRIPTION}`]: '',
        },
      },
      data: {},
    };
  }
}
