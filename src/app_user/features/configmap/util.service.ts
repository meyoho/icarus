import {
  JSONPatchOp,
  K8sApiService,
  PatchOperation,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { ConfigMap, RESOURCE_TYPES, ResourceType } from 'app/typings';

@Injectable()
export class ConfigMapUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}

  deleteConfigMapKey(clusterName: string, configmap: ConfigMap, key: string) {
    return this.dialog.confirm({
      title: this.translate.get('delete'),
      content: this.translate.get(
        'configmap_service_delete_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
      confirmText: this.translate.get('confirm'),
      cancelText: this.translate.get('cancel'),
      beforeConfirm: (resolve, reject) => {
        this.k8sApi
          .patchResource({
            type: RESOURCE_TYPES.CONFIG_MAP,
            cluster: clusterName,
            resource: configmap,
            part: [{ op: JSONPatchOp.Remove, path: `/data/${key}` }],
            operation: PatchOperation.JSON,
          })
          .subscribe(resolve, reject);
      },
    });
  }
}
