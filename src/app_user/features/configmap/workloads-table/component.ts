import {
  Dictionary,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  ObservableInput,
  TranslateService,
  isAllowed,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { first, map, pluck, switchMap } from 'rxjs/operators';

import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from 'app/features-shared/exec/exec-command/component';
import { findRefs } from 'app/features-shared/resource-reference/utils';
import { AcpApiService } from 'app/services/api/acp-api.service';
import { TerminalService } from 'app/services/api/terminal.service';
import {
  ConfigMap,
  Container,
  Pod,
  RESOURCE_TYPES,
  Workload,
} from 'app/typings';
import { TYPE } from 'app/utils';

interface WorkloadInfo {
  id: number;
  name: string;
  kind: string;
  containers: Container[];
  raw: Workload;
}

@Component({
  selector: 'rc-configmap-workloads',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('expand', [
      state('*', style({ height: 0 })),
      state('expanded', style({ height: '*' })),
      transition('* => expanded', [animate(250)]),
      transition('expanded => *', [animate(250)]),
    ]),
  ],
})
export class ConfigmapWorkloadsComponent {
  @ObservableInput(true)
  configmap$: Observable<ConfigMap>;

  @Input()
  configmap: ConfigMap;

  @ObservableInput(true)
  cluster$: Observable<string>;

  @Input()
  cluster: string;

  @Input() workloads: WorkloadInfo[] = [];

  params$ = combineLatest([this.cluster$, this.configmap$]).pipe(
    map(([cluster, configMap]) => ({
      cluster,
      namespace: this.k8sUtil.getNamespace(configMap),
      name: this.k8sUtil.getName(configMap),
    })),
  );

  createPermission$ = this.params$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.PODS_EXEC,
        action: K8sResourceAction.CREATE,
        cluster,
        namespace,
      }),
    ),
    isAllowed(false),
  );

  workloads$ = this.params$.pipe(
    switchMap(params =>
      this.acpApi.getTopologyByResource({
        kind: 'configmap',
        ...params,
      }),
    ),
    map(graph =>
      findRefs(graph, {
        kind: this.configmap.kind,
        name: this.configmap.metadata.name,
      }),
    ),
    map(workloads =>
      workloads.map((item, index) => ({
        id: index,
        kind: item.kind,
        name: this.k8sUtil.getName(item),
        containers: get(item, ['spec', 'template', 'spec', 'containers']),
        raw: item,
      })),
    ),
    publishRef(),
  );

  columns = [NAME, TYPE, 'detail'];

  rowExpanded: Dictionary<boolean> = {};

  constructor(
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
    private readonly terminal: TerminalService,
    private readonly translate: TranslateService,
    private readonly acpApi: AcpApiService,
  ) {}

  getPods(resource: Workload) {
    return this.k8sApi
      .getResourceList<Pod>({
        type: RESOURCE_TYPES.POD,
        cluster: this.cluster,
        namespace: this.configmap.metadata.namespace,
        queryParams: {
          labelSelector: matchLabelsToString(
            get(resource, 'spec.selector.matchLabels'),
          ),
        },
      })
      .pipe(pluck('items'));
  }

  openTerminal(pods: Pod[], container: Container, componentName: string) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        title: this.translate.get('reload_configuration'),
        cluster: this.cluster,
        namespace: this.k8sUtil.getNamespace(this.configmap),
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!data) {
        return;
      }
      this.terminal.openTerminal({
        ctl: componentName,
        pods: pods.map(pod => pod.metadata.name).join(';'),
        selectedPod: '',
        container: container.name,
        namespace: this.k8sUtil.getNamespace(this.configmap),
        cluster: this.cluster,
        user: data.user,
        command: data.command,
      });
    });
  }

  reloadConfig(item: WorkloadInfo, container: Container) {
    this.getPods(item.raw).subscribe(pods =>
      this.openTerminal(pods, container, item.name),
    );
  }

  toggleRow(id: number) {
    this.rowExpanded = {
      ...this.rowExpanded,
      [id]: !this.rowExpanded[id],
    };
  }
}
