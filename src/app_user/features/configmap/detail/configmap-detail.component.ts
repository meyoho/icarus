import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  NAME,
  isAllowed,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { equals } from 'ramda';
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { RefsService } from 'app/features-shared/resource-reference/refs.service';
import { getBelongingApp } from 'app/features-shared/workload/util';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { ConfigMap, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { viewActions, yamlReadOptions } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

import { AddConfigDialogComponent } from '../dialog/add-config-dialog.component';

export interface ConfigmapDetailParams {
  cluster: string;
  namespace: string;
  name: string;
}

@Component({
  templateUrl: './configmap-detail.component.html',
  styleUrls: ['./configmap-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigmapDetailComponent {
  editorActions = viewActions;
  editorOptions = yamlReadOptions;

  baseParams = this.workspace.baseParamsSnapshot;
  getBelongingApp = getBelongingApp;

  params$ = combineLatest([
    this.activatedRoute.paramMap.pipe(map(params => params.get(NAME))),
    this.workspace.baseParams,
  ]).pipe(
    map(([name, { cluster, namespace }]) => ({ name, cluster, namespace })),
    distinctUntilChanged<{
      name: string;
      cluster: string;
      namespace: string;
    }>(equals),
    publishRef(),
  );

  dataManager = new AsyncDataLoader<ConfigMap>({
    fetcher: params =>
      this.k8sApi.getResource({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster: params.cluster,
        namespace: params.namespace,
        name: params.name,
      }),
    params$: this.params$,
  });

  permissions$ = this.workspace.baseParams.pipe(
    switchMap(baseParams =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.SERVICE,
        cluster: baseParams.cluster,
        namespace: baseParams.namespace,
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly workspace: WorkspaceComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly refsService: RefsService,
  ) {}

  getDescription(configMap: ConfigMap) {
    return this.k8sUtil.getDescription(configMap);
  }

  resourceToYaml(configMap: ConfigMap) {
    return safeDump(configMap);
  }

  back = () => {
    this.router.navigate(['configmap'], {
      relativeTo: this.workspace.baseActivatedRoute,
    });
  };

  addConfig() {
    this.dialogService
      .open(AddConfigDialogComponent, {
        size: DialogSize.Large,
        data: {
          configmapData: this.dataManager.snapshot.data,
          baseParams: this.dataManager.snapshot.params,
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.dataManager.mapData(() => {
            return result;
          });
        }
      });
  }

  updateConfigmap() {
    this.router.navigate(
      ['configmap', 'update', this.dataManager.snapshot.params.name],
      {
        relativeTo: this.workspace.baseActivatedRoute,
      },
    );
  }

  deleteConfigmap() {
    this.refsService
      .safeDelete({
        cluster: this.workspace.baseParamsSnapshot.cluster,
        resource: this.dataManager.snapshot.data,
        handleDelete: () =>
          this.k8sApi.deleteResource({
            type: RESOURCE_TYPES.CONFIG_MAP,
            cluster: this.workspace.baseParamsSnapshot.cluster,
            resource: this.dataManager.snapshot.data,
          }),
      })
      .subscribe(this.back, noop);
  }

  dataUpdated(configMap: ConfigMap) {
    this.dataManager.mapData(() => configMap);
  }
}
