import {
  JSONPatchOp,
  K8sApiService,
  ObservableInput,
  PatchOperation,
  ValueHook,
  noop,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { ConfigMap, RESOURCE_TYPES, ResourceType } from 'app/typings';
import { ACTION } from 'app/utils';

import { ConfigMapUtilService } from '../util.service';

export interface KeyValue {
  key: string;
  value: string;
}

@Component({
  selector: 'rc-k8s-config-items',
  templateUrl: './k8s-config-items.component.html',
  styleUrls: ['./k8s-config-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sConfigItemsComponent {
  @ObservableInput(true)
  configMap$: Observable<ConfigMap>;

  @ValueHook<K8sConfigItemsComponent, 'configMap'>(function (configMap) {
    this.handleData(configMap);
  })
  @Input()
  configMap: ConfigMap;

  @Input()
  clusterName: string;

  @Input()
  canUpdate: boolean;

  @Output()
  actionComplete = new EventEmitter<ConfigMap>();

  @ViewChild(NgForm, { static: false })
  form: NgForm;

  dialogRef: DialogRef<TemplateRef<any>>;

  keyword = '';
  keyValueList: KeyValue[] = [];
  columns = ['key', 'value', ACTION];

  submitting = false;

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly configMapUtilities: ConfigMapUtilService,
    private readonly dialogService: DialogService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  handleData(res: ConfigMap) {
    this.keyValueList = Object.entries(res.data ?? {}).map(([key, value]) => ({
      key,
      value,
    }));
  }

  updateConfig(data: KeyValue, templateRef: TemplateRef<any>) {
    this.dialogRef = this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data,
    });
  }

  deleteConfigMapKey(rowData: KeyValue) {
    this.configMapUtilities
      .deleteConfigMapKey(this.clusterName, this.configMap, rowData.key)
      .then(result => this.actionComplete.next(result))
      .catch(noop);
  }

  filterItems(items: KeyValue[], keyword: string) {
    return items.filter(({ key }) => key.includes(keyword));
  }

  trackByFn(_i: number, item: KeyValue) {
    return item.key;
  }

  confirm(key: string, newVal: string) {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchResource({
        cluster: this.clusterName,
        type: RESOURCE_TYPES.CONFIG_MAP,
        resource: this.configMap,
        part: [
          { op: JSONPatchOp.Replace, path: `/data/${key}`, value: newVal },
        ],
        operation: PatchOperation.JSON,
      })
      .subscribe(
        result => {
          this.submitting = false;
          this.dialogRef.close();
          this.actionComplete.next(result);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close();
  }
}
