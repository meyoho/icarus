import { TranslateService } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';

import { BaseStringMapFormComponent, KeyValue } from 'app/abstract';
import { K8S_CONFIGMAP_KEY } from 'app/utils';

@Component({
  selector: 'rc-config-items-fieldset',
  templateUrl: './config-items-fieldset.component.html',
  styleUrls: ['./config-items-fieldset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigItemsFieldsetComponent extends BaseStringMapFormComponent {
  keyReg = K8S_CONFIGMAP_KEY;

  maxSize = 1024 * 1024;

  constructor(
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  async addRowFromFile(event: Event) {
    const target = event.target as HTMLInputElement;
    const { files } = target;
    const readFiles: Array<Promise<KeyValue>> = [];
    const errorFileNames: string[] = [];
    // eslint-disable-next-line unicorn/prefer-spread
    Array.from(files).forEach(file => {
      file.size < this.maxSize
        ? readFiles.push(this.getFileContent(file))
        : errorFileNames.push(file.name);
    });
    const readResult = await Promise.all(readFiles);
    // 导入时，去除排在前面的空白键值
    let count = this.form.controls.length;
    while (count) {
      count--;
      const control = this.form.controls[count];
      if (control && !control.value[0] && !control.value[1]) {
        this.form.removeAt(count);
      }
    }
    readResult.forEach((item: KeyValue) => {
      this.checkFileIsBinary(item[1])
        ? errorFileNames.push(item[0])
        : this.addImportItemToForm(item);
    });
    target.value = null;
    if (errorFileNames.length > 0) {
      this.notification.error({
        title: this.translate.get('error'),
        content: this.translate.get('configmap_from_file_error_content', {
          fileNames: errorFileNames.join(', '),
        }),
      });
    }

    this.cdr.markForCheck();
  }

  addImportItemToForm(item: KeyValue) {
    const control = this.createNewControl();
    control.setValue(item);
    this.form.push(control);
  }

  getFileContent(file: File): Promise<KeyValue> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.addEventListener('load', () => {
        resolve([file.name, fileReader.result as string]);
      });
      fileReader.addEventListener('error', reject);
      fileReader.readAsText(file);
    });
  }

  checkFileIsBinary(content: string) {
    return [...content].some(char => char.charCodeAt(0) > 127);
  }

  createNewControl() {
    return this.fb.array([
      [
        '',
        [
          Validators.required,
          Validators.pattern(this.keyReg.pattern),
          (control: AbstractControl) => {
            const index = this.form.controls.indexOf(control.parent);
            const previousKeys = this.getPreviousKeys(index);
            if (previousKeys.includes(control.value)) {
              return {
                duplicateKey: control.value,
              };
            } else {
              return null;
            }
          },
        ],
      ],
      [],
    ]);
  }
}
