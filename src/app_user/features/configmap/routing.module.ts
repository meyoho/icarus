import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConfigMapCreateComponent } from 'app_user/features/configmap/create/component';
import { ConfigmapDetailComponent } from 'app_user/features/configmap/detail/configmap-detail.component';
import { ConfigmapListComponent } from 'app_user/features/configmap/list/configmap-list.component';
import { ConfigMapUpdateComponent } from 'app_user/features/configmap/update/component';

const routes: Routes = [
  {
    path: '',
    component: ConfigmapListComponent,
  },
  {
    path: 'create',
    component: ConfigMapCreateComponent,
  },
  {
    path: 'update/:name',
    component: ConfigMapUpdateComponent,
  },
  {
    path: 'detail/:name',
    component: ConfigmapDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigmapRoutingModule {}
