import { K8sApiService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from '@angular/core';
import { Router } from '@angular/router';
import { identity } from 'ramda';

import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import { ConfigMap, RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  selector: 'rc-configmap-form-container',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapFormContainerComponent extends BaseFormContainer<
  ConfigMap
> {
  protected fillResource = identity;

  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService,
    private readonly workspace: WorkspaceComponent,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {
    super(injector);
  }

  submit() {
    if (!this.checkForm()) {
      return;
    }

    this.submitting = true;
    (this.isUpdate ? this.k8sApi.putResource : this.k8sApi.postResource)
      .call(this.k8sApi, {
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster: this.workspace.baseParamsSnapshot.cluster,
        resource: this.resource,
      })
      .subscribe(
        () => {
          this.router.navigate(
            ['configmap', 'detail', this.resource.metadata.name],
            {
              relativeTo: this.workspace.baseActivatedRoute,
            },
          );
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }
}
