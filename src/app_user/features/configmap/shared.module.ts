import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ConfigItemsFieldsetComponent } from './fieldset/config-items-fieldset.component';
import { ConfigMapFormComponent } from './form/component';

@NgModule({
  imports: [SharedModule],
  declarations: [ConfigMapFormComponent, ConfigItemsFieldsetComponent],
  exports: [ConfigMapFormComponent],
})
export class ConfigMapSharedModule {}
