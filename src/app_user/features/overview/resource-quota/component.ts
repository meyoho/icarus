import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ResourceQuotaItem } from 'app/typings';
import { shortNum, toUnitGi, toUnitNum } from 'app/utils';

export const RESOURCE_QUOTA_ITEMS = [
  {
    key: 'limits.cpu',
    name: 'cpu',
    unit: 'core',
    normalizer: toUnitNum,
  },
  {
    key: 'limits.memory',
    name: 'memory',
    unit: 'gi',
    normalizer: toUnitGi,
  },
  {
    key: 'requests.storage',
    name: 'storage',
    unit: 'gi',
    normalizer: toUnitGi,
  },
  {
    key: 'pods',
    name: 'pods_num',
    unit: 'ge',
    normalizer: toUnitNum,
  },
  {
    key: 'persistentvolumeclaims',
    name: 'pvc_num',
    unit: 'ge',
    normalizer: toUnitNum,
  },
];

@Component({
  selector: 'alk-overview-resource-quota',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceQuotaComponent {
  @ObservableInput()
  @Input('resourceQuota')
  resourceQuota$: Observable<{
    hard?: ResourceQuotaItem;
    used?: ResourceQuotaItem;
  }>;

  resourceQuotaItems$ = this.resourceQuota$.pipe(
    map(resourceQuota =>
      RESOURCE_QUOTA_ITEMS.filter(
        item => get(resourceQuota, ['used', item.key]) != null,
      ).map(item => {
        const used = shortNum(
          item.normalizer(get(resourceQuota, ['used', item.key])),
        );
        const total = shortNum(
          item.normalizer(get(resourceQuota, ['hard', item.key])),
        );
        const ratio = total ? +used / +total : 0.1;
        return {
          ...item,
          used: shortNum(used),
          total: shortNum(total),
          ratio,
          color: ratio > 0.9 ? '#eb6262' : ratio > 0.7 ? '#f8ac58' : '#63d283',
        };
      }),
    ),
    publishRef(),
  );
}
