import {
  API_GATEWAY,
  CodeDisplayDialogComponent,
  K8sApiService,
  K8sUtilService,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { K8sEventsResult } from 'app/features-shared/event/k8s-event-list/event.types';
import { ChartDataItem } from 'app/shared/components/donut-chart/component';
import {
  NamespaceOverview,
  RESOURCE_TYPES,
  Service,
  WorkloadStats,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

export interface Alert {
  annotations: {
    alert_current_value: string;
    alert_notifications: string;
  };
  endsAt: string;
  fingerprint: string;
  receivers: [
    {
      name: string;
    },
  ];
  startsAt: string;
  status: {
    inhibitedBy: [];
    silencedBy: [];
    state: 'active';
  };
  updatedAt: string;
  generatorURL: string;
  labels: {
    alert_cluster: string;
    alert_indicator: string;
    alert_indicator_aggregate_range: string;
    alert_indicator_alias: string;
    alert_indicator_comparison: string;
    alert_indicator_threshold: string;
    alert_involved_object_kind: string;
    alert_involved_object_name: string;
    alert_involved_object_namespace: string;
    alert_name: string;
    alert_project: string;
    alert_resource: string;
    alertname: string;
    namespace: string;
    service: string;
    severity: string;
  };
}

export const EVENT_DETAIL_LINK_MAPPER = {
  StatefulSet: 'statefulset',
  DaemonSet: 'daemonset',
  Deployment: 'deployment',
  Pod: 'pod',
  CronJob: 'cron_job',
  Job: 'job_record',
};

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverviewComponent {
  EVENT_DETAIL_LINK_MAPPER = EVENT_DETAIL_LINK_MAPPER;

  baseParams$ = this.workspace.baseParams;

  namespaceOverview$ = this.baseParams$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sApi.watchResource<NamespaceOverview>({
        type: RESOURCE_TYPES.NAMESPACE_OVERVIEW,
        cluster,
        namespace,
        name: namespace,
      }),
    ),
    startWith(null as NamespaceOverview),
    publishRef(),
  );

  appChartData$: Observable<ChartDataItem[]> = this.namespaceOverview$.pipe(
    map(namespaceOverview => {
      const application = namespaceOverview?.spec.application;
      return [
        {
          name: 'running',
          value: application?.running,
        },
        {
          name: 'partial_running',
          value: application?.partialRunning,
        },
        {
          name: 'pending',
          value: application?.pending,
        },
        {
          name: 'failed',
          value: application?.failed,
        },
        {
          name: 'stopped',
          value: application?.stopped,
        },
        {
          name: 'no_workload',
          value: application?.empty,
          type: 'empty',
        },
      ];
    }),
  );

  workloadChartsData$ = this.namespaceOverview$.pipe(
    map(namespaceOverview => {
      const workload = namespaceOverview?.spec.workload;
      return (['deployment', 'statefulset', 'daemonset'] as const).reduce(
        (acc, type) => {
          const item = workload?.[type] || ({} as WorkloadStats);
          const data = [
            {
              name: 'running',
              value: item.ready,
            },
            {
              name: 'pending',
              value: item.pending,
            },
          ];
          if (type !== 'daemonset') {
            data.push({
              name: 'stopped',
              value: item.stopped,
            });
          }
          acc.push({
            type,
            data,
            total: item.ready + item.pending + item.stopped || 0,
          });
          return acc;
        },
        [],
      );
    }),
    publishRef(),
  );

  events$ = this.baseParams$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.http.get<K8sEventsResult>(API_GATEWAY + '/v4/events', {
        params: {
          cluster,
          namespace,
          start_time: String(Date.now() / 1000 - 24 * 60 * 60),
          end_time: String(Date.now() / 1000),
          size: '100',
        },
      }),
    ),
    map(({ items }) => items?.map(({ spec }) => spec)),
    publishRef(),
  );

  alerts$ = this.baseParams$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sApi
        .getResource<Service>({
          type: RESOURCE_TYPES.SERVICE,
          cluster,
          namespace: this.globalNamespace,
          name: 'kube-prometheus-alertmanager',
        })
        .pipe(
          switchMap(service => {
            const portName = service?.spec.ports?.[0]?.name;
            return portName
              ? this.http.get<Alert[]>(
                  `${API_GATEWAY}/kubernetes/${cluster}/api/v1/namespaces/${
                    this.globalNamespace
                  }/services/${this.k8sUtil.getName(
                    service,
                  )}:${portName}/proxy/api/v2/alerts`,
                  {
                    params: {
                      filter: [
                        `alert_cluster=${cluster}`,
                        'severity=~"High|Critical"',
                        `alert_involved_object_namespace=${namespace}`,
                      ],
                    },
                  },
                )
              : EMPTY;
          }),
        ),
    ),
    publishRef(),
  );

  podErrorsColumns = ['name', 'status', 'reason'];

  eventsColumns = [
    'resource_name',
    'resource_type',
    'event_time',
    'reason',
    'message',
  ];

  constructor(
    private readonly http: HttpClient,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly workspace: WorkspaceComponent,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  sumPods(namespaceOverview: NamespaceOverview) {
    const podStats = namespaceOverview?.spec.pod.stats;
    return podStats
      ? podStats.error +
          podStats.evicted +
          podStats.pending +
          podStats.running +
          podStats.succeeded
      : 0;
  }

  showEventDetail(eventDetail: {}) {
    this.dialog.open(CodeDisplayDialogComponent, {
      size: DialogSize.Big,
      data: {
        code: JSON.stringify(eventDetail, null, 2),
        language: 'json',
        title: this.translate.get('event_detail'),
      },
    });
  }
}
