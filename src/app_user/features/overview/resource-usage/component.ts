import {
  API_GATEWAY,
  FALSE,
  NOTIFY_ON_ERROR_HEADER,
  ObservableInput,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import Highcharts from 'highcharts';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { numToStr } from 'app/utils/commons';

export const TIME_RANGES = [
  {
    type: 'last_day',
    offset: 24 * 60 * 60,
    step: 2 * 60 * 60,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 60 * 60,
    step: 14 * 60 * 60,
  },
  {
    type: 'last_1_month',
    offset: 30 * 24 * 60 * 60,
    step: 60 * 60 * 60,
  },
];

export interface MetricQueryItem {
  metric: {
    namespace?: string;
    __query_id__: string;
  };
  values: Array<[number, string]>;
}

const UNITS = {
  cpu: 'core',
  memory: 'gi',
};

@Component({
  selector: 'alk-overview-resource-usage',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceUsageComponent {
  @ObservableInput()
  @Input('cluster')
  cluster$: Observable<string>;

  @ObservableInput()
  @Input('namespace')
  namespace$: Observable<string>;

  TIME_RANGES = TIME_RANGES;

  UNITS = UNITS;

  Highcharts = Highcharts;

  physicalTimeRange$$ = new BehaviorSubject('last_day');

  appTimeRange$$ = new BehaviorSubject('last_day');

  physicalData$ = combineLatest([
    this.physicalTimeRange$$.pipe(distinctUntilChanged()),
    this.cluster$,
    this.namespace$,
  ]).pipe(
    switchMap(([timeRange, cluster, namespace]) => {
      return this.fetchData({
        timeRange,
        cluster,
        queries: [
          {
            name: 'cpu',
            expr: `sum by (namespace) (container_cpu_usage_seconds_total_irate5m{namespace="${namespace}",image!="",container_name!="POD"})`,
          },
          {
            name: 'memory',
            expr: `sum by(namespace) (container_memory_usage_bytes_without_cache{namespace="${namespace}"})`,
          },
        ],
      });
    }),
    publishRef(),
  );

  appData$ = combineLatest([
    this.appTimeRange$$.pipe(distinctUntilChanged()),
    this.cluster$,
    this.namespace$,
  ]).pipe(
    switchMap(([timeRange, cluster, namespace]) => {
      return this.fetchData({
        timeRange,
        cluster,
        queries: [
          {
            name: 'deployment',
            expr: `count(kube_deployment_created{namespace="${namespace}"})`,
          },
          {
            name: 'statefulset',
            expr: `count(kube_statefulset_created{namespace="${namespace}"})`,
          },
          {
            name: 'daemonset',
            expr: `count(kube_daemonset_created{namespace="${namespace}"})`,
          },
          {
            name: 'cron_job',
            expr: `count(kube_cronjob_created{namespace="${namespace}"})`,
          },
          {
            name: 'pod',
            expr: `count(kube_pod_created{namespace="${namespace}"})`,
          },
        ],
      });
    }),
    publishRef(),
  );

  constructor(private readonly http: HttpClient) {}

  fetchData({
    cluster,
    timeRange,
    queries,
  }: {
    cluster: string;
    timeRange: string;
    queries: Array<{
      name: string;
      expr: string;
    }>;
  }) {
    const range = TIME_RANGES.find(({ type }) => type === timeRange);
    const end = Math.ceil(Date.now() / 1000);
    const initData = queries.map(({ name }) => ({
      metric: {
        __query_id__: name,
      },
      values: this.buildEmptyValues(range),
    }));
    return this.http
      .post<MetricQueryItem[]>(
        `${API_GATEWAY}/v1/metrics/${cluster}/query_range`,
        {
          start: end - range.offset,
          end,
          step: range.step,
          queries: queries.map(({ name, expr }) => ({
            id: name,
            range: 0,
            labels: [
              {
                name: '__name__',
                value: 'custom',
              },
              {
                name: 'expr',
                value: expr + ' or on() vector(0)',
              },
            ],
          })),
        },
        {
          headers: {
            [NOTIFY_ON_ERROR_HEADER]: FALSE,
          },
        },
      )
      .pipe(
        map(items =>
          items?.length
            ? items
                .reduce<MetricQueryItem[]>((acc, item) => {
                  const type = item.metric.__query_id__;
                  if (type === 'memory') {
                    item.values = item.values.map(([x, y]) => [
                      x,
                      String(+y / 1024 / 1024 / 1024),
                    ]);
                  }
                  const exists = acc.find(i => i.metric.__query_id__ === type);
                  if (exists) {
                    exists.metric.namespace =
                      exists.metric.namespace || item.metric.namespace;
                    exists.values.push(...item.values);
                  } else {
                    acc.push(item);
                  }
                  return acc;
                }, [])
                .map(item => {
                  item.values.sort((a, b) => a[0] - b[0]);
                  return item;
                })
                .sort((a, b) => {
                  const aIndex = queries.findIndex(
                    x => x.name === a.metric.__query_id__,
                  );
                  const bIndex = queries.findIndex(
                    x => x.name === b.metric.__query_id__,
                  );
                  return aIndex - bIndex;
                })
            : initData,
        ),
        startWith(initData),
        skipError(initData),
      );
  }

  average(item: MetricQueryItem, toInt = false) {
    const { values } = item;
    const average =
      values.reduce((sum, [_, value]) => sum + +value, 0) / values.length;
    return toInt ? Math.round(average) : numToStr(+average.toFixed(2));
  }

  buildEmptyValues({ offset, step }: { offset: number; step: number }) {
    const now = Math.ceil(Date.now() / 1000);
    const values: Array<[number, number]> = [];
    let currOffset = 0;
    while (currOffset <= offset) {
      values.unshift([now - currOffset, 0]);
      currOffset += step;
    }
    return values;
  }

  getChartOptions(item: MetricQueryItem): Highcharts.Options {
    const data = item.values.map(([x, y]) => [x * 1000, +y]);
    return {
      chart: {
        backgroundColor: null,
      },
      title: {
        text: '',
      },
      legend: {
        enabled: false,
      },
      series: [
        {
          type: 'area',
          data,
          fillOpacity: 0.5,
          lineColor: '#006eff',
          marker: {
            enabled: false,
          },
        },
      ],
      tooltip: {
        xDateFormat: '%Y-%m-%d %H:%M:%S',
        pointFormat: '{point.y}',
      },
      credits: {
        enabled: false,
      },
      xAxis: {
        visible: false,
        type: 'datetime',
      },
      yAxis: {
        visible: false,
        min: 0,
        max: data.reduce((max, [_, value]) => Math.max(max, value), 0) || 1,
      },
    };
  }
}
