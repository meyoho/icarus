import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { OverviewComponent } from './component';
import { ResourceQuotaComponent } from './resource-quota/component';
import { ResourceUsageComponent } from './resource-usage/component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: OverviewComponent,
      },
    ]),
  ],
  declarations: [
    OverviewComponent,
    ResourceQuotaComponent,
    ResourceUsageComponent,
  ],
})
export class OverviewModule {}
