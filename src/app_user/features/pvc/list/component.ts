import {
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  NAME,
  ResourceListParams,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  Environments,
  PersistentVolumeClaim,
  PvcStatusColorMapper,
  PvcStatusIconMapper,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { ACTION, STATUS, getPvcStatus } from 'app/utils';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcListComponent implements OnInit, OnDestroy {
  baseParams: WorkspaceBaseParams;
  columns = [NAME, STATUS, 'connect_pv', 'size', 'created_time', ACTION];
  private readonly onDestroy$ = new Subject<void>();
  getPvcStatus = getPvcStatus;
  PvcStatusColorMapper = PvcStatusColorMapper;
  PvcStatusIconMapper = PvcStatusIconMapper;

  params$ = this.workspaceComponent.baseActivatedRoute.params.pipe(
    map(({ cluster, project, namespace }) => ({
      cluster,
      project,
      namespace,
    })),
  );

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(param => param.get('keyword')),
  );

  pvcParams$ = combineLatest([this.params$, this.searchKey$]).pipe(
    map(([params, keyword]) => ({
      ...params,
      keyword,
    })),
  );

  list = new K8SResourceList({
    fetcher: this.fetchPVCs.bind(this),
    fetchParams$: this.pvcParams$,
    watcher: seed =>
      this.params$.pipe(
        switchMap(({ cluster, namespace }) =>
          this.k8sApi.watchResourceChange(seed, {
            type: RESOURCE_TYPES.PVC,
            cluster,
            namespace,
          }),
        ),
      ),
  });

  permissions$ = this.params$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.PVC,
        cluster,
        action: COMMON_WRITABLE_ACTIONS,
        namespace,
      }),
    ),
  );

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {}

  fetchPVCs({
    cluster,
    search: _search,
    namespace,
    ...queryParams
  }: ResourceListParams) {
    return this.k8sApi.getResourceList<PersistentVolumeClaim>({
      type: RESOURCE_TYPES.PVC,
      cluster,
      namespace,
      queryParams,
    });
  }

  ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.list.destroy();
  }

  createPvc() {
    this.router.navigate(['pvc', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updatePvc(item: PersistentVolumeClaim) {
    this.router.navigate(['pvc', 'update', item.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  searchByName(keyword: string) {
    return this.router.navigate(['.'], {
      queryParams: { keyword },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }
}
