import { Component } from '@angular/core';
import { map } from 'rxjs/operators';

import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-pvc-form-container
      [baseParams]="workspace.baseParams | async"
      [initResource]="initResource$ | async"
    ></rc-pvc-form-container>
  `,
})
export class PvcCreateComponent {
  initResource$ = this.workspace.baseParams.pipe(
    map(baseParams => ({
      apiVersion: 'v1',
      kind: 'PersistentVolumeClaim',
      metadata: {
        name: '',
        namespace: baseParams.namespace,
        annotations: {},
        labels: {},
      },
      spec: {
        storageClassName: null,
        accessModes: ['ReadWriteOnce'],
        resources: {
          requests: {
            storage: '',
          },
        },
        selector: {
          matchLabels: {},
        },
      },
    })),
  );

  constructor(public workspace: WorkspaceComponent) {}
}
