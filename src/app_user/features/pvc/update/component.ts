import { K8sApiService, NAME } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, combineLatest } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { RESOURCE_TYPES } from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  template: `
    <rc-pvc-form-container
      *ngIf="data$ | async as data"
      [isUpdate]="true"
      [initResource]="data"
      [baseParams]="workspace.baseParams | async"
    ></rc-pvc-form-container>
  `,
})
export class PvcUpdateComponent {
  data$ = combineLatest([
    this.activatedRoute.paramMap,
    this.workspace.baseParams,
  ]).pipe(
    switchMap(([selfParam, { cluster, namespace }]) =>
      this.k8sApi.getResource({
        type: RESOURCE_TYPES.PVC,
        name: selfParam.get(NAME),
        cluster,
        namespace,
      }),
    ),
    catchError(err => {
      this.auiNotificationService.error(err);
      this.router.navigate(['pvc'], {
        relativeTo: this.workspace.baseActivatedRoute,
      });
      return EMPTY;
    }),
  );

  constructor(
    public readonly workspace: WorkspaceComponent,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly auiNotificationService: NotificationService,
  ) {}
}
