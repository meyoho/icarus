import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, combineLatest, forkJoin } from 'rxjs';
import { map, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';

import { PodControllerKinds } from 'app/features-shared/app/utils';
import { getBelongingApp } from 'app/features-shared/workload/util';
import { AcpApiService } from 'app/services/api/acp-api.service';
import {
  PersistentVolumeClaim,
  PvcStatusColorMapper,
  PvcStatusIconMapper,
  RESOURCE_TYPES,
  Workload,
  WorkspaceBaseParams,
} from 'app/typings';
import { TYPE, getPvcStatus } from 'app/utils';
import { ACCESS_MODES } from 'app_admin/features/storage/storage.constant';
import { PVCUtilService } from 'app_user/features/pvc/util.service';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../app/detail-styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcDetailComponent implements OnDestroy {
  baseParams: WorkspaceBaseParams;
  referencedResources: Workload[] = [];
  accessModes = ACCESS_MODES;
  getPvcStatus = getPvcStatus;
  getBelongingApp = getBelongingApp;
  PvcStatusColorMapper = PvcStatusColorMapper;
  PvcStatusIconMapper = PvcStatusIconMapper;
  resourceColumns = [NAME, TYPE];

  destroy$ = new Subject<void>();

  params$ = combineLatest([
    this.activatedRoute.params,
    this.workspaceComponent.baseParams,
  ]).pipe(
    map(([{ name }, baseParams]) => {
      this.baseParams = baseParams;
      return {
        name,
        ...baseParams,
      };
    }),
    shareReplay(1),
    takeUntil(this.destroy$),
  );

  permissions$ = this.params$.pipe(
    switchMap(param =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.PVC,
        cluster: param.cluster,
        action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
        namespace: param.namespace,
      }),
    ),
    takeUntil(this.destroy$),
    shareReplay(1),
  );

  dataManager = new AsyncDataLoader<{
    resource: PersistentVolumeClaim;
    permissions: { update: boolean; delete: boolean };
  }>({
    params$: this.params$,
    fetcher: this.dataFetcher.bind(this),
  });

  constructor(
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly acpApi: AcpApiService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly pvcUtil: PVCUtilService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  private dataFetcher(params: Params) {
    return combineLatest([this.fetchResource(params), this.permissions$]).pipe(
      map(([resource, permissions]) => ({
        resource,
        permissions,
      })),
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private fetchResource({ cluster, namespace, name }: Params) {
    return forkJoin([
      this.k8sApi.getResource<PersistentVolumeClaim>({
        type: RESOURCE_TYPES.PVC,
        cluster,
        namespace,
        name,
      }),
      this.acpApi.getTopologyByResource({
        cluster,
        namespace,
        kind: 'persistentvolumeclaim',
        name,
      }),
    ]).pipe(
      tap(([, topology]) => {
        this.referencedResources = Object.values(topology.nodes).filter(r =>
          PodControllerKinds.includes(r.kind),
        );
      }),
      map(([data]) => data),
    );
  }

  jumpToListPage = () => {
    this.router.navigate(['pvc'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  };

  update(name: string) {
    this.router.navigate(['pvc', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  delete(pvc: PersistentVolumeClaim) {
    const { cluster } = this.workspaceComponent.baseParamsSnapshot;
    this.pvcUtil.delete(pvc, cluster).subscribe(() => {
      this.message.success(this.translate.get('pvc_delete_success'));
      this.jumpToListPage();
    });
  }
}
