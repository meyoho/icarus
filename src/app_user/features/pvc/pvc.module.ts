import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { PvcCreateComponent } from 'app_user/features/pvc/create/component';
import { PvcDetailComponent } from 'app_user/features/pvc/detail/component';
import { PvcFormContainerComponent } from 'app_user/features/pvc/form-container/component';
import { PvcFormComponent } from 'app_user/features/pvc/form/component';
import { PvcListComponent } from 'app_user/features/pvc/list/component';
import { PvcRoutingModule } from 'app_user/features/pvc/pvc.routing.module';
import { PvcUpdateComponent } from 'app_user/features/pvc/update/component';

import { PVCUtilService } from './util.service';

@NgModule({
  imports: [SharedModule, EventSharedModule, PvcRoutingModule],
  declarations: [
    PvcListComponent,
    PvcFormComponent,
    PvcFormContainerComponent,
    PvcCreateComponent,
    PvcUpdateComponent,
    PvcDetailComponent,
  ],
  providers: [PVCUtilService],
})
export class PvcModule {}
