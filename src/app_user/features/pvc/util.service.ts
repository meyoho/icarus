import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { concatMapTo } from 'rxjs/operators';

import { PersistentVolumeClaim, RESOURCE_TYPES } from 'app/typings';
import { catchPromise } from 'app/utils';

@Injectable()
export class PVCUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
  ) {}

  delete(resource: PersistentVolumeClaim, cluster: string) {
    return catchPromise(
      this.dialog.confirm({
        title: this.translate.get('delete_pvc_title'),
        content: this.translate.get('delete_pvc_content', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      concatMapTo(
        this.k8sApi.deleteResource({
          type: RESOURCE_TYPES.PVC,
          cluster,
          resource,
        }),
      ),
    );
  }
}
