import {
  K8sApiService,
  METADATA,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep, set } from 'lodash-es';
import { identity } from 'ramda';
import { finalize } from 'rxjs/operators';

import { BaseFormContainer } from 'app/shared/abstract/base-form-container';
import {
  PersistentVolumeClaim,
  RESOURCE_TYPES,
  WorkspaceBaseParams,
} from 'app/typings';
import { WorkspaceComponent } from 'app_user/workspace/workspace.component';

const RESOURCE_CREATE_TIP = 'pvc_create_success';
const RESOURCE_UPDATE_TIP = 'pvc_update_success';

@Component({
  selector: 'rc-pvc-form-container',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcFormContainerComponent
  extends BaseFormContainer<PersistentVolumeClaim>
  implements OnInit {
  @Input()
  baseParams: WorkspaceBaseParams;

  clusters: string[];

  initialized = false;

  constructor(
    injector: Injector,
    private readonly workspaceComponent: WorkspaceComponent,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly translateService: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  jumpToDetailPage(name: string) {
    this.router.navigate(['pvc/detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  submit() {
    if (this.checkForm()) {
      const payload = cloneDeep(this.resource);
      this._confirm(payload);
    }
  }

  protected fillResource = identity;

  private _confirm(payload: PersistentVolumeClaim) {
    if (this.isUpdate) {
      delete payload.metadata.resourceVersion;
    }
    set(payload, [METADATA, NAMESPACE], this.baseParams.namespace);
    const { name } = payload.metadata;
    this.k8sApi[this.isUpdate ? 'putResource' : 'postResource']({
      cluster: this.baseParams.cluster,
      type: RESOURCE_TYPES.PVC,
      resource: payload,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(
          this.translateService.get(
            this.isUpdate ? RESOURCE_UPDATE_TIP : RESOURCE_CREATE_TIP,
            {
              name,
            },
          ),
        );
        this.jumpToDetailPage(name);
      });
  }
}
