import {
  K8sApiService,
  KubernetesResource,
  NAME,
  PROJECT,
  StringMap,
  TranslateService,
  matchExpressionsToString,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  PersistentVolumeClaim,
  PersistentVolumeClaimSpec,
  RESOURCE_TYPES,
  StorageClass,
  WorkspaceBaseParams,
} from 'app/typings';
import {
  ASSIGN_ALL,
  K8S_RESOURCE_NAME_BASE,
  POSITIVE_INT_PATTERN,
  yamlWriteOptions,
} from 'app/utils';
import { ACCESS_MODES } from 'app_admin/features/storage/storage.constant';

interface PersistentVolumeClaimSpecFormModel {
  storageClassName?: string;
  volumeName?: string;
  resources: {
    requests: { storage: string };
  };
  selector?: {
    matchLabels: StringMap;
  };
  accessModes: string;
}

interface PersistentVolumeClaimFormModel extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeClaimSpecFormModel;
  status?: {
    phase: string;
  };
}

@Component({
  selector: 'rc-pvc-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  baseParams: WorkspaceBaseParams;

  @Input()
  isUpdate: boolean;

  storageClassNameList: Array<{
    key: string;
    value: string;
  }> = [];

  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  accessModes = Object.keys(ACCESS_MODES);
  accessModesMap = ACCESS_MODES;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  unit = 'Gi';
  codeEditorOptions = yamlWriteOptions;

  constructor(
    injector: Injector,
    private readonly translateService: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.storageClassNameList = [
      {
        key: this.translateService.get('storageclass_nonuse'),
        value: 'no_use',
      },
      {
        key: this.translateService.get('storageclass_use_default'),
        value: 'use_default',
      },
    ];
    this.getStorageClass();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.resourceNameReg.pattern),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      storageClassName: this.fb.control(''),
      accessModes: this.fb.control([]),
      resources: this.fb.group({
        requests: this.fb.group({
          storage: this.fb.control(''),
        }),
      }),
      selector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getDefaultFormModel(): PersistentVolumeClaimFormModel {
    return {
      apiVersion: 'v1',
      kind: 'PersistentVolumeClaim',
      metadata: {
        name: '',
        namespace: this.baseParams.namespace,
        annotations: {},
        labels: {},
      },
      spec: {
        storageClassName: 'no_use',
        accessModes: 'ReadWriteOnce',
        resources: {
          requests: {
            storage: '',
          },
        },
        selector: {
          matchLabels: {},
        },
      },
    };
  }

  adaptResourceModel(
    resource: PersistentVolumeClaim,
  ): PersistentVolumeClaimFormModel {
    if (resource && resource.spec) {
      const spec: PersistentVolumeClaimSpecFormModel = {
        ...resource.spec,
        accessModes: resource.spec.accessModes[0] || 'ReadWriteOnce',
        resources: {
          requests: {
            storage: resource.spec.resources.requests.storage,
          },
        },
      };
      if (!spec.storageClassName) {
        switch (spec.storageClassName) {
          case '':
            spec.storageClassName = 'no_use';
            break;
          case null:
            spec.storageClassName = 'use_default';
            break;
        }
      }
      if (spec.resources.requests.storage) {
        const res = spec.resources.requests.storage.match(
          /[A-Za-z]+|\d+(?:\.\d+|)/g,
        );
        spec.resources.requests.storage = res[0] || '';
        this.unit = res[1] || 'Gi';
      }
      return { ...resource, spec };
    }
  }

  adaptFormModel(
    formModel: PersistentVolumeClaimFormModel,
  ): PersistentVolumeClaim {
    const spec: PersistentVolumeClaimSpec = {
      ...formModel.spec,
      accessModes: [formModel.spec.accessModes],
      resources: {
        requests: {
          storage: formModel.spec.resources.requests.storage,
        },
      },
    };
    if (
      spec.selector &&
      !(
        spec.selector.matchLabels &&
        Object.keys(spec.selector.matchLabels).length > 0
      )
    ) {
      delete spec.selector;
    }
    switch (spec.storageClassName) {
      case 'no_use':
        spec.storageClassName = '';
        break;
      case 'use_default':
        spec.storageClassName = null;
        break;
    }
    if (spec.resources.requests.storage) {
      spec.resources.requests.storage = `${spec.resources.requests.storage}${this.unit}`;
    }
    return { ...formModel, spec };
  }

  getStorageClass() {
    this.k8sApi
      .getResourceList({
        type: RESOURCE_TYPES.STORAGE_CLASS,
        cluster: this.baseParams.cluster,
        queryParams: {
          labelSelector: matchExpressionsToString([
            {
              key: this.k8sUtil.normalizeType(NAME, PROJECT),
              operator: 'in',
              values: [ASSIGN_ALL, this.baseParams.project],
            },
          ]),
        },
      })
      .subscribe(storageClass => {
        storageClass.items.forEach((item: StorageClass) => {
          this.storageClassNameList.push({
            key: item.metadata.name,
            value: item.metadata.name,
          });
        });
        this.cdr.markForCheck();
      });
  }
}
