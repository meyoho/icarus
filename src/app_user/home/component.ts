import {
  K8sApiService,
  NAME,
  NAMESPACE,
  Reason,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { get } from 'lodash-es';
import { Observable, ReplaySubject, Subject, combineLatest, merge } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { GlobalPermissionService } from 'app/services/global-permission.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { BaseLayoutComponent } from 'app/shared/layout/base-layout-component';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as namespaceActions from 'app/store/actions/namespaces.actions';
import * as projectActions from 'app/store/actions/projects.actions';
import { ProjectsFacadeService } from 'app/store/facades';
import { NamespacesFacadeService } from 'app/store/facades/namespaces.facade';
import * as fromNamespaces from 'app/store/selectors/namespaces.selectors';
import {
  Cluster,
  Environments,
  Namespace,
  Project,
  ProjectCluster,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/typings';
import { NamespaceStatusEnum } from 'app_user/home/types';

interface ClusterOption {
  project_name: string;
  name: string;
  display_name: string;
  isFederated?: boolean;
  isMaster?: boolean;
}
interface NamespaceOption {
  name: string;
  display_name?: string;
  created_time: string;
  status: string;
  isFederated?: boolean;
  cluster: ClusterOption;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['../../app/shared/layout/layout.common.scss', 'styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent extends BaseLayoutComponent
  implements OnInit, OnDestroy {
  project$: Observable<Project>;
  private readonly clusterSelect$ = new ReplaySubject<ClusterOption>(1);
  private projectName: string;

  navConfigAddress = '';
  columns = [NAME, 'created_time'];
  currentCluster: ClusterOption;
  projectName$: Observable<string>;
  projectsLength$: Observable<number>;

  clusters$: Observable<ClusterOption[]>;
  clustersLength$: Observable<number>;

  namespacesLength$: Observable<number>;
  filteredNamespaces$: Observable<NamespaceOption[]>;
  search$ = new Subject<string>();
  validatingNamespaceName: string;

  reason = Reason;

  constructor(
    injector: Injector,
    private readonly cdr: ChangeDetectorRef,
    private readonly projectsFacade: ProjectsFacadeService,
    private readonly namespacesFacade: NamespacesFacadeService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    public globalPermission: GlobalPermissionService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.projectsFacade.dispatch(new projectActions.LoadProjects());
    this.initObservables();
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.project) {
        window.localStorage.setItem('project', params.project);
      }
    });
    this.translate.locale$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.title.setTitle(this.translate.get(NAMESPACE));
    });
  }

  nameCompare(ns1: NamespaceOption, ns2: NamespaceOption) {
    return ns1.name.localeCompare(ns2.name);
  }

  trackByClusterName(_index: number, cluster: any) {
    return cluster.name;
  }

  trackByNamespaceUuid(_index: number, namespace: NamespaceOption) {
    return `${namespace.cluster.name}-${namespace.name}`;
  }

  selectProject(project: string) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        project,
      },
    });
  }

  selectCluster(cluster: ClusterOption) {
    if (this.currentCluster && this.currentCluster.name === cluster.name) {
      return;
    }
    this.namespacesFacade.dispatch(
      new namespaceActions.GetNamespacesByClusterAndProject({
        cluster: cluster.name,
        project: cluster.project_name,
      }),
    );
    this.clusterSelect$.next(cluster);
    this.currentCluster = cluster;
    this.router.navigate(['.'], {
      queryParams: {
        cluster: cluster.name,
      },
      queryParamsHandling: 'merge',
    });
  }

  getNamespaceOptionLabel(namespace: NamespaceOption) {
    return `${namespace.cluster.name}.${namespace.name}`;
  }

  selectNamespace(namespace: NamespaceOption) {
    if (
      this.validatingNamespaceName === this.getNamespaceOptionLabel(namespace)
    ) {
      return;
    }
    this.validatingNamespaceName = this.getNamespaceOptionLabel(namespace);
    this.k8sApi
      .getResource<Namespace>({
        type: RESOURCE_TYPES.NAMESPACE,
        name: namespace.name,
        cluster: namespace.cluster.name,
      })
      .pipe(
        finalize(() => {
          this.validatingNamespaceName = '';
          this.cdr.markForCheck();
        }),
      )
      .subscribe(
        resource => {
          if (
            get(resource, 'status.phase', '').toLowerCase() ===
            NamespaceStatusEnum.TERMINATING
          ) {
            this.namespacesFacade.dispatch(
              new namespaceActions.UpdateNamespaceByClusterAndProject({
                cluster: namespace.cluster.name,
                project: this.projectName,
                namespace: resource,
              }),
            );
          } else {
            this._jumpToWorkspace(namespace);
          }
        },
        () => this._jumpToWorkspace(namespace),
      );
  }

  isTerminating(item: NamespaceOption) {
    return (
      item.status &&
      item.status.toLowerCase() === NamespaceStatusEnum.TERMINATING
    );
  }

  private _jumpToWorkspace(namespace: NamespaceOption) {
    this.router.navigate([
      '/workspace',
      {
        project: this.projectName,
        cluster: namespace.cluster.name,
        namespace: namespace.name,
      },
    ]);
  }

  private initObservables() {
    this.projectsLength$ = this.projectsFacade.projects$.pipe(
      map(projects => projects.length),
    );

    this.projectName$ = this.activatedRoute.queryParams.pipe(
      map(params => {
        return params.project;
      }),
      distinctUntilChanged(),
      tap(name => (this.projectName = name)),
    );

    this.project$ = this.projectName$.pipe(
      switchMap((name: string) =>
        this.projectsFacade.selectProjectByName$(name),
      ),
    );

    this.clusters$ = combineLatest([
      this.project$,
      this.k8sApi
        .getGlobalResourceList<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          namespaced: true,
        })
        .pipe(map(res => res.items)),
    ]).pipe(
      map(([project, clusters]) => {
        const clusterRefs = get(project, 'spec.clusters', []);
        return clusters
          .filter(c => {
            return clusterRefs.find(
              (ref: ProjectCluster) => ref.name === c.metadata.name,
            );
          })
          .map(cluster => ({
            project_name: project.metadata.name,
            name: cluster.metadata.name,
            display_name: this.k8sUtil.getDisplayName(cluster),
            isFederated: !!this.k8sUtil.getLabel(cluster, NAME, 'federation'),
            isMaster:
              this.k8sUtil.getLabel(cluster, 'hostname', 'federation') ===
              cluster.metadata.name,
          }));
      }),
      publishRef(),
    );

    this.clustersLength$ = this.clusters$.pipe(map(r => r.length));

    this.projectName$.subscribe(() => {
      this.currentCluster = null;
    });

    this.clusters$.pipe(takeUntil(this.onDestroy$)).subscribe(options => {
      if (options.length) {
        const option = options.find(
          option =>
            option.name ===
            this.activatedRoute.snapshot.queryParamMap.get('cluster'),
        );
        this.selectCluster(option || options[0]);
      }
    });

    const namespaces$ = merge(
      this.project$.pipe(map(() => [])),
      this.clusterSelect$.pipe(
        switchMap(option => {
          return this.namespacesFacade
            .getNamespacesByProjectAndCluster$(option.project_name, option.name)
            .pipe(
              filter(r => !!r),
              map(entities => {
                return entities.map(entity =>
                  this.getNamespaceOption(entity, option),
                );
              }),
            );
        }),
        publishRef(),
      ),
    ).pipe(publishRef());

    this.namespacesLength$ = namespaces$.pipe(
      map(namespaces => namespaces.length),
      publishRef(),
    );

    this.filteredNamespaces$ = combineLatest([
      namespaces$,
      this.search$.pipe(startWith('')),
    ]).pipe(
      map(([namespaces, search]) =>
        search
          ? namespaces
              .filter((item: any) => item.name.includes(search.trim()))
              .sort(this.nameCompare)
          : namespaces,
      ),
      tap(() => {
        this.cdr.markForCheck();
      }),
    );
  }

  private getNamespaceOption(
    entity: fromNamespaces.NamespaceEntity,
    cluster: ClusterOption,
  ): NamespaceOption {
    return {
      name: entity.namespace.metadata.name,
      created_time: entity.namespace.metadata.creationTimestamp,
      status: entity.namespace.status.phase,
      display_name: this.k8sUtil.getDisplayName(entity.namespace),
      isFederated: this.k8sUtil.isFederatedResource(entity.namespace),
      cluster,
    };
  }

  buildUrl(url: string) {
    return url;
  }
}
