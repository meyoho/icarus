import { CommonLayoutModule } from '@alauda/common-snippet';
import { PageModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { HomeComponent } from './component';
import { HomeRoutingModule } from './routing.module';

@NgModule({
  imports: [
    SharedModule,
    SharedLayoutModule,
    PageModule,
    RouterModule,
    PortalModule,
    CommonLayoutModule,
    HomeRoutingModule,
  ],
  declarations: [HomeComponent],
})
export class HomeModule {}
