export interface QuotaRange {
  cpu?: {
    max: number;
    used: number;
  };
  memory?: {
    max: number;
    used: number;
  };
  storage?: {
    max: number;
    used: number;
  };
  pvc_num?: {
    max: number;
    used: number;
  };
  pods?: {
    max: number;
    used: number;
  };
}

export enum QuotaConfigEnum {
  CPU = 'requests.cpu',
  MEMORY = 'requests.memory',
  STORAGE = 'requests.storage',
  PVC_NUM = 'persistentvolumeclaims',
  pods = 'pods',
  CPU_REQUESTS = 'requests.cpu',
  CPU_LIMITS = 'limits.cpu',
  MEMORY_REQUESTS = 'requests.memory',
  MEMORY_LIMITS = 'limits.memory',
}
export enum NamespaceStatusEnum {
  ACTIVE = 'active',
  CREATING = 'creating',
  TERMINATING = 'terminating',
  ERROR = 'error',
  UNKNOWN = 'unknown',
}
