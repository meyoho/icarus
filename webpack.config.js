const customWebpack = require('@alauda/custom-webpack');

module.exports = config => {
  Object.assign(customWebpack(config).resolve.alias, {
    moment$: 'dayjs/esm',
    'moment-timezone$': require.resolve('./src/app/timezone'),
  });
  return config;
};
